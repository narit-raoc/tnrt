# (SS. 03/2023) RaDecToAzEl.cpp from Katja@OHB sent by email on 2023/03/01
# Purpose: Change ACU to fix issue 552 about UT1 day discontinuity
# This file is a python translation of RaDecToAzEl.cpp so we can simulate and debug
# using convenient python scripts.
import numpy as np
from numpy import sin as SIN
from numpy import cos as COS
from numpy import arcsin as ASIN
from numpy import arccos as ACOS
from numpy import floor as FLOOR

from astropy import units
from astropy.coordinates import Longitude

def get_sidereal_day_per_solar_day(obstime_utc_mjd):
    # Use astropy to calculate ratio between sidereal day and solar day.
    t1 = obstime_utc_mjd
    t2 = obstime_utc_mjd + 1 * units.day
    sidereal_time1 = t1.sidereal_time("apparent", longitude="greenwich")
    sidereal_time2 = t2.sidereal_time("apparent", longitude="greenwich")
    # One solar day (86400 seconds) is longer than 1 sidereal day (86164.0905 seconds),
    # so the hourangle has already wrapped around 24 when we subtract the sidereal times.
    # Add 24 hours to account for the full rotation.
    sidereal_hourangles_per_solar_day = (sidereal_time2 - sidereal_time1) + 24 * units.hourangle
    sidereal_day_per_solar_day = sidereal_hourangles_per_solar_day.hourangle / 24.0
    return(sidereal_day_per_solar_day)

class gvl_const:
    PI = np.pi
    SEC_PER_DAY = 86400

class Status:
    def __init__(self, length):
        self.error = np.zeros(length)

def FB_RaDec2AzEl(
	mjd,            # actual modified julien day            ndarray
	mjdTimeOfStart, # time of program track start [mjd]     scalar
	dut1,           # milliseconds                          scalar
	gst0,           # milliseconds                          scalar
	ra_in,          # degree                                ndarray
	dec_in,         # degree                                ndarray
	long_in,        # degree                                scalar
	lat_in,         # degree                                scalar
    verbose=False   # {True | False} print debug information
    ):
    """
    Reproduce ACU algorithm
    """
    
    if verbose is True:
        print("* Input to FB_RaDec2AzEl ...")
        print("* mjd: type={}, unit=days, value={}, ".format(type(mjd), mjd))
        print("* mjdTimeOfStart: type={}, unit=days, value={}, ".format(type(mjdTimeOfStart), mjdTimeOfStart))
        print("* dut1: type={}, unit=[ms], value={}".format(type(dut1), dut1))
        print("* gst0: type={}. unit=[ms], value={}".format(type(gst0), gst0))
        print("* ra_in: type={}, unit=[deg], value={}".format(type(ra_in), ra_in))
        print("* dec_in: type={}, unit=[deg], value={}".format(type(dec_in), dec_in))
        print("* long_in: type={}, unt=[deg], value={}".format(type(long_in), long_in))
        print("* lat_in: type={}, unit=[deg], value={}".format(type(lat_in),lat_in))

    npts = len(mjd)
    stSts = Status(npts)

    # Set input data to internal data
    ra = ra_in
    dec = dec_in
    long = long_in
    lat = lat_in

    # Convert units
    dut1 = dut1 / 1000.0    # [msec] to [sec]
    gst0 = gst0 / 1000.0    # [msec] to [sec]

    # Calculate the number seconds between actual time of ACU tracking and
    # the integer UT1 day when trackig started (must be the same UT1 day that was
    # used to calculate gst0)
    
    # NOTE: We must shift the time of program tracking start from UTC to UT1 **BEFORE**
    # we use FLOOR to find the integer day
    mjdTimeOfStart_ut1 = mjdTimeOfStart + (dut1 / gvl_const.SEC_PER_DAY)
    mjdDayOfStart_ut1 = FLOOR(mjdTimeOfStart_ut1)

    mjdActual_ut1 = mjd + (dut1 / gvl_const.SEC_PER_DAY)
    
    seconds_since_tracking_start = (mjdActual_ut1 - mjdDayOfStart_ut1) * gvl_const.SEC_PER_DAY
    
    # Calculate Greenwich Sideral Time at the time of ACU tracking
    gst = gst0 + seconds_since_tracking_start * 1.00273790935
    
    # Wrap gst at 0 to 86400 seconds (or NOT!)
    # Note: Use numpy conditional to use vectorized operation on entire array
    # condition_wrap_86400_down = np.array(gst > 86400.0, dtype=bool)
    # index_of_wrap_86400_down = np.where(condition_wrap_86400_down)
    # gst[index_of_wrap_86400_down] = gst[index_of_wrap_86400_down] % 86400.0

    # condition_wrap_86400_up = np.array(gst < 0.0, dtype=bool)
    # index_of_wrap_86400_up = np.where(condition_wrap_86400_up)
    # gst[index_of_wrap_86400_up] = gst[index_of_wrap_86400_up] + 86400.0

    # Convert units
    gst = gst / 3600.0  # [sec] to [hours]
    long = long / 15.0  # [deg] to [hours]
    ra = ra / 15.0      # [deg] to [hours]

    lst = gst + long    # Local Sidereal Time at location Longitude [hours]
    h = lst - ra        # Local Hour Angle of object [hours]

    # Convert units
    h = h * gvl_const.PI / 12.0         # [hours] to [radian]
    dec = dec * gvl_const.PI / 180.0    # [deg] to [radian]
    ra = ra * gvl_const.PI / 12.0       # [hours] to [radian]
    lat = lat * gvl_const.PI / 180.0    # [deg] to [radian]
   
    # sin and cos of geometry parameters for convenience
    u = SIN(lat)    # scalar
    v = SIN(dec)    # array of length npts
    w = COS(lat)    # scalar
    x = COS(dec)    # array of length npts
    y = COS(h)      # array of length npts
    
    # calculate azimuth and elevation
    # sinAlt := ( SIN(lat) * SIN(dec) + COS(lat) * COS(dec) * COS(h));
    sinAlt = u * v + w * x * y
    
    # Note: Use numpy conditional to use vectorized operation on entire array
    c1 = np.array((sinAlt < -1), dtype=bool)
    c2 = np.array((sinAlt > 1), dtype=bool)
    condition_error1 = np.logical_or(c1, c2)
    index_of_error1 = np.where(condition_error1)
    k = np.where(np.logical_not(condition_error1))

    # Array indices that have error condition, set the error status and set el=0
    el = np.zeros(npts)
    stSts.error[index_of_error1] = 1        
    el[index_of_error1] = 0
    
    # Array indices that do not have error condition, set values following algorithm
    altRad = np.zeros(npts)
    altRad[k] = ASIN(sinAlt[k])
    el[k] = altRad[k]
    
    cosAlt = COS(altRad)
    
    # az := ACOS((SIN(dec) - SIN(lat)*sinAlt) / (COS(lat)* cosAlt));
    # Note: Use numpy conditional to use vectorized operation on entire array
    condition_error2 = np.array((w * cosAlt)==0, dtype=bool)
    
    z = np.zeros(npts)
    az = np.zeros(npts)
    # Array indices that have error condition, set the error status and set az =0
    index_of_error2 = np.where(condition_error2)
    stSts.error[index_of_error2] = 2
    az[index_of_error1] = 0
    
    # Array indices that do not have error condition, set values following algorithm
    k = np.where(np.logical_not(condition_error2))
    z[k] = (v[k] - u * sinAlt[k]) / (w * cosAlt[k]) # remove LIMIT function
    az[k] = ACOS(z[k])    # [radian]

    # Resolve ambiguity in arccos function
    # Note: Use numpy conditional to use vectorized operation on entire array
    wrap_condition = np.array(SIN(h) > 0.0, dtype=bool)
    index_to_wrap = np.where(wrap_condition)
    az[index_to_wrap] =  2 * gvl_const.PI - az[index_to_wrap]

    # Convert units
    p_azimuth = az * 180.0 / gvl_const.PI   # [radian] to [deg]
    p_elevation = el * 180.0 / gvl_const.PI # [radian] to [deg]
       
    return(p_azimuth, p_elevation, gst)



""" (* Decleration Part *)
FUNCTION_BLOCK FB_RaDec2AzEl_OAN_Test
VAR_INPUT
	mjd				:LREAL;		// actual modified julien day
	mjdDayOfStart	:LREAL;		// day of starttime [mjd]
	dut1			:LREAL;		// milliseconds
	gst0			:LREAL;		// milliseconds 
	ra_in			:LREAL;		// degree
	dec_in			:LREAL;		// degree
	long_in			:LREAL;		// degree
	lat_in			:LREAL;		// degree
END_VAR
VAR_OUTPUT
	stSts			:ST_StarTrackStatus;
END_VAR
VAR
	utc				:LREAL;	// seconds since midnight of actual track -> can be more than 86400
	ra				:LREAL;
	dec				:LREAL;
	az				:LREAL;
	el				:LREAL;
	long			:LREAL;
	lat				:LREAL;
	gst 			:LREAL;	(* Greenwhich sidereal time *)
	lst				:LREAL; (* Local sidereal time *)
	h	 			:LREAL;	(* hour angle *)
	sinAlt  		:LREAL;
	altRad  		:LREAL;
	cosAlt  		:LREAL;
	u,v,w,x,y,z   	:LREAL;
	ut1_now			:LREAL;
	ut1_start		:LREAL;
END_VAR


(* Execution Part *)
// Set input data to internal data
ra		:= ra_in;
dec		:= dec_in;
long	:= long_in;
lat		:= lat_in;

utc	:= (mjd - mjdDayOfStart) * gvl_const.SEC_PER_DAY;

(* Greenwhich sidereal time  in seconds*)
dut1 		:= dut1/1000;
gst0 		:= gst0/1000;

gst  := gst0 + (utc + dut1)*1.00273790935;

(*
IF gst > 86400.0 THEN
	gst := LMOD(gst, 86400.0);
END_IF

IF(gst < 0.0)THEN
	gst := gst + 86400.0;
END_IF
*)

gst 	:= gst/3600; (* gst from [sec] to [hours] *)
long 	:= long/15.0; (* long from [deg] to [hours] *)
ra 		:= ra/15.0;	  (* RA from [deg] to [hours] *)

lst 	:= gst + long; (* local sidereal time *)

h 		:= lst - ra;		(* hour angle *)

h 		:= h * gvl_const.PI/12.0;
dec 	:= dec * gvl_const.PI/180.0;
ra 		:= ra * gvl_const.PI/12.0;
lat 	:= lat * gvl_const.PI/180.0;

u := SIN(lat);
v := SIN(dec);
w := COS(lat);
x := COS(dec);
y := COS(h);

(* calculate azimuth and elevation in arcsec *)
(* sinAlt := ( SIN(lat) * SIN(dec) + COS(lat) * COS(dec) * COS(h)); *)
sinAlt := u * v + w * x * y;
IF sinAlt < -1 OR sinAlt > 1 THEN
	stSts.error := 1;
	el			:= 0;
ELSE
	altRad := ASIN(sinAlt);
	el := altRad;
END_IF
cosAlt := COS(altRad);


(* az := ACOS((SIN(dec) - SIN(lat)*sinAlt) / (COS(lat)* cosAlt)); *)
IF (w * cosAlt) = 0 THEN
	stSts.error := 2;
	az 			:= 0;
ELSE
	z := LIMIT(-1,(v - u *sinAlt) / (w * cosAlt),1);
	az := ACOS(z);

	IF SIN(h) > 0.0 THEN
		az :=  2 * gvl_const.PI - az;
	END_IF
END_IF


// Status 
stSts.p_azimuth 	:= az * 180/gvl_const.PI;
stSts.p_elevation 	:= el * 180/gvl_const.PI;
stSts.ra			:= ra_in;
stSts.dec			:= dec_in;
stSts.epoch			:= 0;
 """