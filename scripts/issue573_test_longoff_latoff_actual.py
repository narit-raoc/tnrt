# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 100 * units.deg
el = 45 * units.deg
logger.debug(
    "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0
send_icrs_to_acu = False

#----------------------------------------------------------

# Schedule params
project_id = 'no_projid'
obs_id = 'no_obsid'

# Scantype Parameters for cross scan - use for all scans
arm_length = 7200
time_per_arm = 30
scantype_params = ScantypeParamsCross(arm_length, time_per_arm)

# Frontend, Backend.
frontend_params = FrontendParamsK()
backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=250000)

# clear offsets and pointing corrections
clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# -------------------------------------------------------
# [scan id = 1] No offsets or pointing corrections - Tracking TLE
# -------------------------------------------------------
# schedule_params = ScheduleParams(project_id, obs_id, scan_id=1)
# tle_data = get_TLE(38098)
# tracking_params = TrackingParamsTLE(tle_data.name, tle_data.line1, tle_data.line2)
# add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params)
# log(run_queue())
# -------------------------------------------------------
# [scan id = 2] No offsets or pointing corrections - Tracking EQ (RADEC)
# -------------------------------------------------------
schedule_params = ScheduleParams(project_id, obs_id, scan_id=2)
tracking_params = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    0,
    0,
)
# add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params)
# log(run_queue())

# -------------------------------------------------------
# [scan id = 3] Axis position offsets
# -------------------------------------------------------
set_axis_position_offsets(1*3600, 2*3600)
schedule_params = ScheduleParams(project_id, obs_id, scan_id=3)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params)
log(run_queue())
clear_axis_position_offsets()

# # -------------------------------------------------------
# # [scan id = 4] Pointing Model
# # -------------------------------------------------------
# set_pmodel(p1=3*3600, p7=4*3600)
# schedule_params = ScheduleParams(project_id, obs_id, scan_id=4)
# add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params)
# log(run_queue())
# clear_pmodel()

# # # -------------------------------------------------------
# # # [scan id = 5] User Pointing Correction
# # # -------------------------------------------------------
# schedule_params = ScheduleParams(project_id, obs_id, scan_id=5)
# tracking_params = TrackingParamsEQ(
#     ra_catalog_icrs,
#     dec_catalog_icrs,
#     pm_ra,
#     pm_dec,
#     parallax,
#     radial_velocity,
#     send_icrs_to_acu,
#     5*3600,
#     6*3600,
# )
# add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params)

# log(run_queue())

# clear offsets and pointing corrections
clear_pmodel()
clear_axis_position_offsets()
clear_refraction()
