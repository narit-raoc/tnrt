delay_until_scheduled_start = 5
project_id = "test script"
obs_id = "SS"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    start_mjd=start_mjd,
    source_name=source_name,
    line_name=line_name,
)


arm_length = 7200
time_per_arm = 30
scantype_params_cross = ScantypeParamsCross(arm_length, time_per_arm)

# Tracking Horizontal
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

#  For L-band provision
# backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer")
# backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer", integration_time=1, freq_res=4000)

#  For K-band provision
backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_K")

frontend_params = FrontendParamsK()

add_scan(
    schedule_params, 
    scantype_params_cross, 
    tracking_params, 
    backend_params=backend_params,
    frontend_params=frontend_params,
)
results = run_queue()

results = stop_queue()
