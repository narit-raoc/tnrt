import argparse
import sys
from argparse import RawTextHelpFormatter
import DataAggregatorDefaults as config
from pymongo import MongoClient
import json
import pprint

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="input file name", formatter_class=RawTextHelpFormatter
)
parser.add_argument("filename", help="FITS file. Only name not include the full path")

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

db_client = MongoClient(
    config.DB_URL,
    username=config.DB_USER,
    password=config.DB_PASSWORD,
)

db = db_client[config.DB_NAME]
collection = db[config.DB_COLLECTION]

result = collection.find_one({"fitInfo.fileName": args.filename})

pp = pprint.PrettyPrinter(depth=4)
pp.pprint(result)
