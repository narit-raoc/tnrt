# -*- coding: utf-8 -*-
import argparse
from argparse import RawTextHelpFormatter
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

from astropy import units
from astropy.coordinates import EarthLocation
from astropy.time import Time

from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

from coordinate_utility import icrs_altaz_astropy

# Constant
SECONDS_PER_DAY = 86400
MILLISECONDS_PER_DAY = 1000 * SECONDS_PER_DAY

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
# white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color1 = "b"
    color2 = "r"
    color3 = "g"
    color4 = "k"
else:
    # Use CMY color for dark background
    color1 = "y"
    color2 = "m"
    color3 = "c"
    color4 = "r"

symbol1 = "o"

size1 = 18
size2 = 12
size3  = 6

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Simulate AZ, EL tracking solution for RA, DEC coordinates using various algorithms",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "-n",
    "--npoints",
    type=int,
    default=50,
    help="number of data items to create",
)
parser.add_argument(
    "-u",
    "--duration",
    type=float,
    default=300.0,
    help="time duration of the tracking table [seconds]",
)
parser.add_argument(
    "-r",
    "--ra_icrs",
    type=float,
    default=299.8681523682083,
    help="Right Ascension from catalog in ICRS coordinate system",
)
parser.add_argument(
    "-d",
    "--dec_icrs",
    type=float,
    default=40.73391589791667,
    help="Declination from catalog ICRS coordinate system",
)

parser.add_argument(
    "-s",
    "--pt_starttime",
    type=str,
    default=0,
    help="start time ISO-T string",
)
parser.add_argument(
    "-o",
    "--sim_time_offset",
    type=float,
    default=0,
    help="Time offset from pt_starttime to of the first simulated timestamp.  Can be (-) or (+)",
)

# Read command line arguments into argparse structure
args = parser.parse_args()
print("")
print("------------------------------------------------------------------------")
print("Inputs to Coordinate Generator")
print("------------------------------------------------------------------------")
print("Npoints [integer]: {}".format(args.npoints))
print("Duration [s]: {} ".format(args.duration))
print("RA catalog ICRS [deg]: {}".format(args.ra_icrs))
print("DEC catalog ICRS [deg]: {}".format(args.dec_icrs))
print("Time offset from now of start time [s]: {}".format(args.sim_time_offset))

# Define the EarthLocation at TNRT site
print("")
print("------------------------------------------------------------------------")
print("Site Location (constant)")
print("------------------------------------------------------------------------")
loc = EarthLocation(
    EarthLocation.from_geodetic(
        lon=99.216805 * units.deg,
        lat=18.864348 * units.deg,
        height=403.625 * units.m,
        ellipsoid="WGS84",
    )
)

print("Site Location Latitude [deg]: {}".format(loc.lat.value))
print("Site Location Longitude [deg]: {}".format(loc.lon.value))
print("Site Location Height [m]: {}".format(loc.height.value))

print("")
print("------------------------------------------------------------------------")
print("Generate time steps")
print("------------------------------------------------------------------------")
# If program track start time is not selected, use current time now
# Else parse the ISO time string and create a time object from that.
if args.pt_starttime == 0:
    starttime_tracking = Time.now()
else:
    starttime_tracking = Time(args.pt_starttime, format="isot")

# Simulated time steps are relative to the program track start time 
# (which could be now or specified by parameter).
start_timesteps_mjd = (starttime_tracking + args.sim_time_offset * units.s).mjd
end_mjd = start_timesteps_mjd + args.duration / SECONDS_PER_DAY
(tt_mjd, tstep_days) = np.linspace(
    start_timesteps_mjd, end_mjd, args.npoints, endpoint=True, retstep=True
)
time_utc = Time(tt_mjd, format="mjd")

print("Start time now MJD: {}".format(start_timesteps_mjd))
print("End time MJD: {}".format(start_timesteps_mjd))
print("Time increment [s]: {} ".format(tstep_days * SECONDS_PER_DAY))
print("Time UTC MJD: {}".format(time_utc))

# Create Astropy "smart" angle objects to simplify my code.  Accept input as 
ra_icrs = Longitude(args.ra_icrs, unit=units.deg)
dec_icrs = Latitude(args.dec_icrs, unit=units.deg)

# # Transform the coorinates using 3 different methods
print("")
print("------------------------------------------------------------------------")
print("Astropy transform ICRS -> Observed AltAz ...")
print("------------------------------------------------------------------------")
(az_astropy, alt_astropy) = icrs_altaz_astropy(ra_icrs, dec_icrs, loc, time_utc)

print("AZ: {}".format(az_astropy))
print("")
print("EL: {}".format(alt_astropy))

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
grid_opacity = 0.7
app = QtWidgets.QApplication([])
str_title = "<font>Tracking Simulation: ra_icrs: {} [deg], dec_icrs: {} [deg]<br>npoints: {}, duration: {} [s], sim_time_offset: {} [s], pt_start_time: {} [UTC]</font>".format(
    args.ra_icrs, args.dec_icrs,
    args.npoints, args.duration, args.sim_time_offset, starttime_tracking.isot,
)

# Time axis data
time_unix = time_utc.unix

# -----------------------------------------------------------------------------
# Graph window 1
# -----------------------------------------------------------------------------
glw_abs = pg.GraphicsLayoutWidget(show=True)
glw_abs.resize(1024, 768)
glw_abs.setWindowTitle("Tracking Simulation")
pg.setConfigOptions(antialias=True)

pi_az_time = glw_abs.addPlot(0, 0, title=str_title)
pi_az_time.addLegend()

pi_az_time.plot(time_unix, az_astropy, name="Astropy ICRS to AltAz", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="AZ", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot EL vs Time
pi_el_time = glw_abs.addPlot(1, 0)
pi_el_time.addLegend()
pi_el_time.plot(time_unix, alt_astropy, name="Astropy ICRS to AltAz", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="EL", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_el_time.showGrid(True, True, alpha=grid_opacity)


# Start the Qt event loop and block this script until user closes the GUI window
app.exec_()
