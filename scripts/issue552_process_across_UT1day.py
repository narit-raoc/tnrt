# -*- coding: utf-8 -*-
import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore

from astropy.io import fits
from astropy import units
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.time import TimeDelta

from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

from coordinate_utility import icrs_altaz_astropy
from RaDecToAzEl import FB_RaDec2AzEl

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
#white_background = True

size1 = 18
size2 = 12
size3  = 6

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color1 = "b"
    color2 = "r"
    color3 = "g"
else:
    # Use CMY color for dark background
    color1 = "y"
    color2 = "m"
    color3 = "c"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# Constant
MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24
ARCSEC_PER_DEG = 3600

# Read important header information and add units
ra_catalog = Longitude(hdul["SCAN-MBFITS"].header["BLONGOBJ"], unit=units.deg)
dec_catalog = Latitude(hdul["SCAN-MBFITS"].header["BLATOBJ"], unit=units.deg)
print("SCAN Header: RA ICRS: {}".format(ra_catalog))
print("SCAN Header: DEC ICRS: {}".format(dec_catalog))

loc_scan_header = EarthLocation(
    EarthLocation.from_geodetic(
        lon=hdul["SCAN-MBFITS"].header["SITELONG"] * units.deg,
        lat=hdul["SCAN-MBFITS"].header["SITELAT"] * units.deg,
        height=hdul["SCAN-MBFITS"].header["SITEELEV"] * units.m,
        ellipsoid="WGS84",
    )
)

print("SCAN Header: Site Location Longitude: {}".format(loc_scan_header.lon))
print("SCAN Header: Site Location Latitude: {}".format(loc_scan_header.lat))
print("SCAN Header: Site Location Height: {}".format(loc_scan_header.height))

# Select in index to use in this script and print in debug messages.
# In the case of this test the values are constant throughout the entire array.
# However, in a very long observation, the value of dut1, ra_geocentric_apparent,
# and dec_geocentric_apparent can change by a small amount.
debug_index = 0

loc_acu = EarthLocation(
    EarthLocation.from_geodetic(
        lon=hdul["MONITOR-ANT-TRACKING"].data["antenna_longitude"][debug_index]
        * units.deg,
        lat=hdul["MONITOR-ANT-TRACKING"].data["antenna_latitude"][debug_index]
        * units.deg,
        height=hdul["MONITOR-ANT-TRACKING"].data["antenna_altitude"][debug_index]
        * units.m,
        ellipsoid="WGS84",
    )
)

dut1_acu = TimeDelta(
    hdul["MONITOR-ANT-TRACKING"].data["dut1"][debug_index] * units.ms, format="sec"
)
dut1_acu_ms = int(np.round(dut1_acu.value*1000))

GST0hUT1_ms = hdul["MONITOR-ANT-TRACKING"].data["gst0hut"][debug_index]
GST0hUT1 = Longitude((GST0hUT1_ms / MILLISECONDS_PER_DAY) * 24.0, unit=units.hourangle)

ra_acu_tracktable = Longitude(
    hdul["MONITOR-ANT-TRACKING-OBJECT"].data["pt_table11"], unit=units.deg
)
dec_acu_tracktable = Latitude(
    hdul["MONITOR-ANT-TRACKING-OBJECT"].data["pt_table12"], unit=units.deg
)

program_track_starttime_utc = Time(hdul["MONITOR-ANT-TRACKING-OBJECT"].data['pt_start_time'][debug_index],
                               format="mjd",
                               scale="utc",
                               location=loc_acu)

program_track_starttime_ut1 = (program_track_starttime_utc+dut1_acu).mjd
program_track_startday = program_track_starttime_ut1.astype(int)

print("ACU: Site Location Longitude: {}".format(loc_acu.lon))
print("ACU: Site Location Latitude: {}".format(loc_acu.lat))
print("ACU: Site Location Height: {}".format(loc_acu.height))
print("ACU: RA (Geocentric Apparent): {}".format(ra_acu_tracktable))
print("ACU: DEC (Geocentric Apparent): {}".format(dec_acu_tracktable))
print("ACU: DUT1 (UT1-UTC) ms : {}".format(dut1_acu_ms))
print("ACU: GST0hUT1: {} ms".format(GST0hUT1_ms))
print("ACU: GST0hUT1: {}".format(GST0hUT1))
print("ACU: Program Track Start Day (MJD): {}".format(program_track_startday))

# Select the data
time_acu = (
    Time(
        hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
        format="mjd",
        scale="utc",
        location=loc_acu,
    )
)


startday_0hUT1 = Time(program_track_startday, format="mjd", scale="ut1")
GST0hUT1_calc = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")
GST0hUT1_calc_ms = int(np.round(86400000 * (GST0hUT1_calc.hourangle / 24.0)))
dut1_calc = program_track_starttime_utc.delta_ut1_utc
dut1_calc_ms = int(np.round(1000*dut1_calc))

print("CHECK: GST0hUT1 at ACU program track start day {}: {}".format(program_track_startday, GST0hUT1_calc))
print("CHECK: GST0hUT1_ms at ACU program track start day {}: {}".format(program_track_startday, GST0hUT1_calc_ms))
print("CHECK: DUT1 at program ACU track start time {} [s]: {}".format(program_track_starttime_utc, dut1_calc))
print("CHECK: DUT1_ms at ACU program track start time {}: {} [ms]".format(program_track_starttime_utc, dut1_calc_ms))

str_gst_label_acu = "GST0hUT1 used by ACU: {}".format(GST0hUT1_ms)
str_gst_label_calc = "GST0hUT1 at ACU program track start day {}: {}".format(program_track_startday, GST0hUT1_calc_ms)

print("------------- debug scalar ------------------")
print(type(time_acu.mjd))
print(type(program_track_starttime_utc.mjd))
print(type(dut1_acu_ms))
print(type(GST0hUT1_ms))
print(type(ra_acu_tracktable.deg))
print(type(dec_acu_tracktable.deg))
print(type(loc_acu.lon.deg))
print(type(loc_acu.lat.deg))

print("RA / DEC Geocentric Apparent -> Observed AltAz (reproduce ACU algorithm)")
(sidereal_az, sidereal_el, gst) = FB_RaDec2AzEl(
    time_acu.mjd,                       # ndarray
    program_track_starttime_utc.mjd,    # scalar
    dut1_acu_ms,                        # scalar
    GST0hUT1_ms,                        # scalar
    ra_acu_tracktable.deg,              # ndarray
    dec_acu_tracktable.deg,             # ndarray
    loc_acu.lon.deg,                    # scalar
    loc_acu.lat.deg,                    # scalar
    True
)

print("Astropy transform ICRS -> Observed AltAz ...")
(astropy_az, astropy_el) = icrs_altaz_astropy(
    ra_catalog, dec_catalog, loc_scan_header, time_acu
)
print("Done")

acu_program_track_az = hdul['MONITOR-ANT-TRACKING'].data['prog_track_az']
acu_program_track_el = hdul['MONITOR-ANT-TRACKING'].data['prog_track_el']

d_acu_astropy_az = (acu_program_track_az - astropy_az) * ARCSEC_PER_DEG
d_acu_astropy_el = (acu_program_track_el - astropy_el) * ARCSEC_PER_DEG

d_acu_sidereal_az = (acu_program_track_az - sidereal_az) * ARCSEC_PER_DEG
d_acu_sidereal_el = (acu_program_track_el - sidereal_el) * ARCSEC_PER_DEG

# Time axis data
time_unix = time_acu.ut1.unix

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "File {}".format(os.path.basename(args.filename))
grid_opacity = 0.7
app = QtGui.QApplication([])

# -----------------------------------------------------------------------------
# Graph window 1 - Absolute Values
# -----------------------------------------------------------------------------
glw_time = pg.GraphicsLayoutWidget(show=True)
glw_time.resize(1024, 768)
glw_time.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Plot AZ vs Time
title_with_gst = "<font>{}<br>{}<br>{}</font>".format(str_title, str_gst_label_acu, str_gst_label_calc)
pi_az_time = glw_time.addPlot(0, 0, title=title_with_gst)
pi_az_time.addLegend()
pi_az_time.plot(time_unix, acu_program_track_az, name="ACU prog track", pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size1)
pi_az_time.plot(time_unix, sidereal_az, name="Python FB_RaDec2AzEl", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_az_time.plot(time_unix, astropy_az, name="Astropy ICRS->AltAz", pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="AZ", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UT1")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot EL vs Time
pi_el_time = glw_time.addPlot(1, 0)
pi_el_time.addLegend()
pi_el_time.plot(time_unix, acu_program_track_el, name="ACU prog track", pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size1)
pi_el_time.plot(time_unix, sidereal_el, name="Python FB_RaDec2AzEl", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_el_time.plot(time_unix, astropy_el, name="Astropy ICRS->AltAz", pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="EL", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UT1")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_el_time.showGrid(True, True, alpha=grid_opacity)

# -----------------------------------------------------------------------------
# Graph window 2 - Differences
# -----------------------------------------------------------------------------
glw_diff = pg.GraphicsLayoutWidget(show=True)
glw_diff.resize(1024, 768)
glw_diff.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Plot diff AZ vs Time
pi_d_az_time = glw_diff.addPlot(0, 0, title=str_title)
pi_d_az_time.addLegend()
pi_d_az_time.plot(time_unix, d_acu_sidereal_az, name="ACU - Python FB_RaDec2AzEl", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_d_az_time.plot(time_unix, d_acu_astropy_az, name="ACU - Astropy", pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="AZ", units="arcsec")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UT1")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_d_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_d_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot diff EL vs Time
pi_d_el_time = glw_diff.addPlot(1, 0)
pi_d_el_time.addLegend()
pi_d_el_time.plot(time_unix, d_acu_sidereal_el, name="ACU - Python FB_RaDec2AzEl", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_d_el_time.plot(time_unix, d_acu_astropy_el, name="ACU - Astropy", pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="EL", units="arcsec")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UT1")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_d_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_d_el_time.showGrid(True, True, alpha=grid_opacity)

# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()
