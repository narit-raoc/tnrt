delay_until_scheduled_start = 5
project_id = "test script"
obs_id = "Test Scan With Backend"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    start_mjd=start_mjd,
    source_name=source_name,
    line_name=line_name,
)

# # Scan On Source
# duration = 60  # second
# scantype_params = ScantypeParamsOnSource(duration)

# Scan Raster 3x3
xlen = 7200
xstep = xlen / 2
ylen = 7200
ystep = ylen / 2
time_per_point = 8
zigzag = True
primary_axis_enum = "X"
coord_system_enum = "HO"

scantype_params_raster = ScantypeParamsRaster(
    xlen,
    xstep,
    ylen,
    ystep,
    time_per_point,
    zigzag,
    primary_axis_enum,
    coord_system_enum,
)

arm_length = 7200
time_per_arm = 30
scantype_params_cross = ScantypeParamsCross(arm_length, time_per_arm)

# # Tracking Solar System
# tracking_params = TrackingParamsSS("sun")

# Tracking Horizontal
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

# Tracking Equatorial
# ra = 141.8958
# dec = -8.6569
# tracking_params = TrackingParamsEQ(ra, dec)


# Specify parameters
# Note (SS 11/2021): provision name "TNRT_daulpol_spectrometer" doesn't affect the behavior at all.
# Perhaps in the future, we will choose the number of sections / polarizations based on this
# provision name to make a more accurate simulation
backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=250000)
# frontend_params = FrontendParamsK()
add_scan(
    schedule_params,
    scantype_params_cross,
    tracking_params,
    backend_params=backend_params,
    # frontend_params=frontend_params
)
add_scan(
    schedule_params,
    scantype_params_raster,
    tracking_params,
    backend_params=backend_params,
    # frontend_params=frontend_params
)

# Default parameters
# backend_params = BackendParamsMock()
# add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)


results = run_queue()
