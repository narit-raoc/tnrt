# Clear pointing model that is stored in from ACU
clear_pmodel()

# Basic schedule parameters (run ASAP)
project_id = "az_limit_wrap"
obs_id = "SS"
schedule_params = ScheduleParams(project_id, obs_id)

# --------------------------------------
# Design the scan 1 and add to the queue
# --------------------------------------
# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 1 * units.deg
el = 45 * units.deg
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

# Specify duration
duration = 60*60
scantype_params = ScantypeParamsOnSource(duration)

add_scan(schedule_params, scantype_params, tracking_params)

# --------------------------------------
# Design the scan 2 and add to the queue
# --------------------------------------

# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 350 * units.deg
el = 45 * units.deg
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

# Specify duration
duration = 30
scantype_params = ScantypeParamsOnSource(duration)

add_scan(schedule_params, scantype_params, tracking_params)

# --------------------------------------
# run queue
# --------------------------------------

results = run_queue()
log("all results: {}".format(results))
