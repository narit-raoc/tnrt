import pyclassfiller
from pyclassfiller import code
import DataFormatGildas
from astropy.coordinates import EarthLocation
from astropy.time import Time
import astropy.units as units
from glob import glob

from astropy.io import fits
import astropy.constants as constants
import numpy as np
import os
import sys
import argparse
from argparse import RawTextHelpFormatter

import DataAggregatorDefaults

class Gildas:
    def __init__(self):
        pass

    def create(self, filename, freq_index, skip_spe=True):
        self.freq_index = freq_index
        self.skip_spe = skip_spe
        self.filename = filename

        self.fid = open(self.filename, "rb")
        self.hdul = fits.open(self.fid)
        self.scan_mbfits_header = self.hdul['SCAN-MBFITS'].header

        self.prepare_file()
        self.compose_common_header()
        self.compose_subscan()
        self.cleanup()

    def prepare_file(self):
        ## settings
        # Allow appending file if file exists
        allow_file_exists = True

        # If overwrite_mode == True, overwrite file.  same as file open mode 'w'.
        # If overwrite_mode == False, append to file.  same as file open mode 'a'
        overwrite_mode = False

        # Maximum number of scans in the file (obsolete)
        max_scans = 999999

        # enforce each scan to be unique in the file
        enforce_unique_scans = True

        ## prepare fileout
        self.fileout = pyclassfiller.ClassFileOut()
        fileout_name = self.filename.split('/')[-1].split('.')[0]

        ## calculate frequency axis
        fs = 2e9
        self.ff = np.linspace(
            fs / 2,
            fs,
            int(self.hdul['ARRAYDATA-MBFITS'].data['DATA'].shape[2]), # FIXME: int(self.hdul['ARRAYDATA-MBFITS'].header['CHANNELS']), 
            endpoint=False,
        )


        self.fileout_path = '{}/gildas/{}_{}kHz_{}kHz.40m'.format(
            os.getenv('DATA_DIR'),
            fileout_name,
            str(int(np.round(self.ff[self.freq_index[0]]) / 1e3)),
            str(int(np.round(self.ff[self.freq_index[1]]) / 1e3)),
        )

        if os.path.exists(self.fileout_path):
            os.remove(self.fileout_path)

        if not os.path.exists(os.getenv('DATA_DIR')+"/gildas"):
            os.mkdir(os.getenv('DATA_DIR')+"/gildas")

        self.fileout.open(
            file= self.fileout_path,
            new=allow_file_exists,
            over=overwrite_mode,
            size=max_scans,
            single=enforce_unique_scans,
        )

        # Create a python CLASS observation data structure to populate.
        # pyclassfiller knows how to write this structure to the actual file.
        self.obs = pyclassfiller.ClassObservation()

        # Observation number [int32][no unit]
        self.obs.head.gen.num = DataFormatGildas.defaults.head.gen.num

        # Version number [int32][no unit]
        self.obs.head.gen.ver = DataFormatGildas.defaults.head.gen.ver
        # Enable sections of file
        self.obs.head.presec[:] = False  # Disable all sections except...
        self.obs.head.presec[code.sec.gen] = True  # General
        self.obs.head.presec[code.sec.pos] = True  # Position
        self.obs.head.presec[code.sec.spe] = True  # Spectroscopy
        self.obs.head.presec[code.sec.dri] = True  # Continuum drifts
        self.obs.head.presec[code.sec.cal] = True  # Calibration

    def compose_common_header(self):
        # ------------------
        # General Parameters
        # ------------------
        # Integer date of observation [int32], [MJD - 60549].
        self.obs.head.gen.dobs = int(self.scan_mbfits_header['MJD']) - DataFormatGildas.GAG_MJD_OFFSET
        
        # Integer date of reduction [int32] [MJD - 60549].
        self.obs.head.gen.dred = int(self.scan_mbfits_header['MJD']) - DataFormatGildas.GAG_MJD_OFFSET
        
        # Quality of data [code]  Ignore
        self.obs.head.gen.qual = code.qual.unknown

        # Scan number [int32][no unit]
        self.obs.head.gen.scan = int(self.scan_mbfits_header['SCANNUM'])

        # X Unit. [code]
        # (If X coodinates section is present) {.velo, .freq, .wave}
        self.obs.head.gen.xunit = code.xunit.freq

        # --------------------
        # Position Information
        # --------------------
        # Source name char[12]
        self.obs.head.pos.sourc = self.scan_mbfits_header['OBJECT']

        # Coordinate System [code] {.unk, .equ, .gal, .hor}
        try:
            self.obs.head.pos.system = DataFormatGildas.map_code_coord[self.scan_mbfits_header['BLNGTYPE'].upper()]
        except KeyError:
            print("WARNING: Using default value of head.pos.system = %d"% DataFormatGildas.defaults.head.pos.system)
            self.obs.head.pos.system = DataFormatGildas.defaults.head.pos.system

        # Equinox of coordinates [float32][unit = year]
        self.obs.head.pos.equinox = self.scan_mbfits_header['EQUINOX']

        # Projection system [code]
        self.obs.head.pos.proj = code.proj.none

        # Lambda : Longitude of source [float64][unit = radian]
        self.obs.head.pos.lam = (self.scan_mbfits_header['BLONGOBJ'] * DataFormatGildas.RADIANS_PER_DEGREE)

        # Beta : Latitude of source [float64][unit = radian]
        self.obs.head.pos.bet = (self.scan_mbfits_header['BLATOBJ'] * DataFormatGildas.RADIANS_PER_DEGREE)

        # Projection angle [float64][unit = radian]
        self.obs.head.pos.projang = DataFormatGildas.defaults.head.pos.projang

        # Offset in longitude [float32][unit = radian]
        self.obs.head.pos.lamof = (self.scan_mbfits_header['PATLONG'] * DataFormatGildas.RADIANS_PER_DEGREE)

        # Offset in latitude [float32][unit = radian]
        self.obs.head.pos.betof = (self.scan_mbfits_header['PATLAT'] * DataFormatGildas.RADIANS_PER_DEGREE)

        self.site_location = EarthLocation(
            EarthLocation.from_geodetic(
                lon=self.scan_mbfits_header['SITELONG'] * units.deg,
                lat=self.scan_mbfits_header['SITELAT'] * units.deg,
                height=self.scan_mbfits_header['SITEELEV'] * units.m,
                ellipsoid="WGS84",
            )
        )

    def compose_subscan(self):
        nsubs = self.scan_mbfits_header['NSUBS']
        arraydata_start_index = 3
        datapar_start_index = 4

        for index in range(nsubs):
            print('------------Subscan {}------------'.format(index))
            arraydata_index = 2 * index + arraydata_start_index
            arraydata_mbfits = self.hdul[arraydata_index].data
            arraydata_mbfits_header = self.hdul[arraydata_index].header

            datapar_index = 2 * index + datapar_start_index
            datapar_mbfits = self.hdul[datapar_index].data
            datapar_mbfits_header = self.hdul[datapar_index].header

            time_apy = Time(
                datapar_mbfits_header['DATE-OBS'],
                location=self.site_location,
                format="isot",
            )

            # UT of observation [float64][unit = radian]
            # Timestamp comes from the first integration returned from the receiver during this subscan
            self.obs.head.gen.ut = (2 * np.pi * ((time_apy.ut1.mjd) % 1))  # fractional days  * 2pi
            print("Calculate subscan start time UT [radians] = %f" % self.obs.head.gen.ut)

            # LST of observation [float64][unit = radian]
            # Timestamp comes from the first integration returned from the receiver during this subscan
            self.obs.head.gen.st = time_apy.sidereal_time("apparent").radian
            print( "Calculate subscan start time LST [radians] = %f" % self.obs.head.gen.st)

            # Subscan number [int32][no unit]
            self.obs.head.gen.subscan = arraydata_mbfits_header['SUBSNUM']
            
            # Opacity [float32][unit = neper]
            # TODO - get this from MBFITS monitor point TAU_FEBE
            self.obs.head.gen.tau = DataFormatGildas.defaults.head.gen.tau
            
            # System temperature [float32][unit = Kelvin]
            # TODO - get this from MBFITS monitor point TSYS_FEBE
            self.obs.head.gen.tsys = DataFormatGildas.defaults.head.gen.tsys
            
            if not self.skip_spe:
                self.write_spectrum(datapar_mbfits, datapar_mbfits_header, arraydata_mbfits, arraydata_mbfits_header)
            
            self.write_continuum(datapar_mbfits, datapar_mbfits_header, arraydata_mbfits, arraydata_mbfits_header)

    def write_spectrum(self, datapar_mbfits, datapar_mbfits_header, arraydata_mbfits, arraydata_mbfits_header):
        # Set type spectrum
        self.obs.head.gen.kind = code.kind.spec

        # Spectral line name char[12]
        self.obs.head.spe.line = arraydata_mbfits_header['MOLECULE']

        # Rest frequency [float64][unit = MHz].  MBFITS has unit of Hz.
        self.obs.head.spe.restf = arraydata_mbfits_header['RESTFREQ'] / 1e6
        
        # Number of channels [int32][no unit]
        # In this example, hardcode 1 because it is a continuum detector.  In the real
        # data pipeline, this will come from an array dimension
        self.obs.head.spe.nchan = int(arraydata_mbfits_header['CHANNELS'])

        # Reference channels [float32][no unit]
        self.obs.head.spe.rchan = np.round(self.obs.head.spe.nchan / 2)


        # Frequency resolution [float32][unit = MHz]
        # TODO
        self.obs.head.spe.fres = arraydata_mbfits_header['1CDLT2S'] / 1e6

        # Velocity resolution [float32][unit = km/s]
        # FIXME
        if self.obs.head.spe.restf == 0:
            self.obs.head.spe.vres = DataFormatGildas.defaults.head.spe.vres
        else:
            self.obs.head.spe.vres = (
                -1
                * constants.c.value
                * self.obs.head.spe.fres
                / self.obs.head.spe.restf
                / 1e3
            )

        # Velocity at reference channel [float32][unit = km/s]
        # TODO
        self.obs.head.spe.voff = DataFormatGildas.defaults.head.spe.voff

        # Blanking value [float32][unit?]
        self.obs.head.spe.bad = DataFormatGildas.defaults.head.spe.bad

        # Image frequency [float64][unit = MHz]
        # TODO
        self.obs.head.spe.image = DataFormatGildas.defaults.head.spe.image

        # Type of velocity [code] {unk, lsr, helio, obs, earth, auto}
        # TODO
        self.obs.head.spe.vtype = DataFormatGildas.defaults.head.spe.vtype

        # Velocity convention [code] {unk, rad, opt, thirtym}
        # Use rad = radio (not optical or 30m IRAM convention .. whatever that means).
        self.obs.head.spe.vconv = DataFormatGildas.defaults.head.spe.vconv

        # Doppler correction -V/c (CLASS convention) [float64][unit = km/s]
        # TODO
        self.obs.head.spe.doppler = DataFormatGildas.defaults.head.spe.doppler
        
        for time_index in range(arraydata_mbfits['DATA'].shape[0]):
            # Update longitude and latitude offset for this integration.
            
            # Offset in longitude [float32][unit = radian]
            self.obs.head.pos.lamof = datapar_mbfits[time_index]["LONGOFF"] * DataFormatGildas.RADIANS_PER_DEGREE
           
            # Offset in latitude [float32][unit = radian]
            self.obs.head.pos.betof = datapar_mbfits[time_index]["LATOFF"] * DataFormatGildas.RADIANS_PER_DEGREE
            
            for polarization in range(arraydata_mbfits['DATA'].shape[1]):
                self.obs.head.gen.teles = "{}-{}-{}-{}".format(
                    self.scan_mbfits_header['TELESCOP'],
                    "L", #TODO: get from FEBE
                    DataAggregatorDefaults.MAP_INDEX_TO_POLARIZATION[polarization],
                    DataAggregatorDefaults.MAP_INDEX_TO_NOISE[polarization]
                )
                # Write the data
                self.obs.datay = np.array(arraydata_mbfits['DATA'][time_index, polarization, :], dtype=np.float32)
                self.obs.write()


    def write_continuum(self, datapar_mbfits, datapar_mbfits_header, arraydata_mbfits, arraydata_mbfits_header ):

        (freq_min, freq_max) = self.freq_index
        self.obs.head.gen.kind = code.kind.cont

        # Rest frequency [float64][unit = MHz]
        # FIXME
        self.obs.head.dri.freq = (self.ff[freq_min] + self.ff[freq_max]) / 2 / 1e6 # arraydata_mbfits_header['RESTFREQ'] / 1e6

        # Bandwidth [float32][unit = MHz]
        total_bandwidth = 1e9 # FIXME 1e9 is fixed to Lband use other number for other band (get from header)
        total_channel = len(self.ff)
        n_channel = freq_max - freq_min
        self.obs.head.dri.width = (n_channel+1) * total_bandwidth / total_channel / 1e6

        # Time at reference [float32][?]
        # FIXME
        self.obs.head.dri.tref = DataFormatGildas.defaults.head.dri.tref

        # Angular offset at reference [float32][unit = radian]
        # We are using this continuum drift to calculate offsets, so don't add more here
        self.obs.head.dri.aref = DataFormatGildas.defaults.head.dri.aref

        # Position angle of drift [float32][unit = radian]
        try:
            self.obs.head.dri.apos = DataFormatGildas.map_apos[
                datapar_mbfits_header['SCANDIR'].upper()
            ]
        except KeyError:
            print(
                "WARNING: Using default value of head.dri.apos = %f"
                % DataFormatGildas.defaults.head.dri.apos
            )
            self.obs.head.dri.apos = DataFormatGildas.defaults.head.dri.apos

        # Time resolution [float32][unit = seconds]
        self.obs.head.dri.tres = self.obs.head.gen.time

        # Blanking value [float32]
        self.obs.head.dri.bad = DataFormatGildas.defaults.head.dri.bad

        # Type of offsets [code]
        try:
            self.obs.head.dri.ctype = DataFormatGildas.map_code_coord[
                datapar_mbfits_header['NLNGTYPE'].upper()
            ]
        except KeyError:
            print(
                "WARNING: Using default value of head.dri.ctype = %f"
                % DataFormatGildas.defaults.head.dri.ctype
            )
            self.obs.head.dri.ctype = DataFormatGildas.defaults.head.dri.ctype

        # Image frequency [float64][unit = MHz]
        self.obs.head.dri.cimag = DataFormatGildas.defaults.head.dri.cimag

        # Collimation error Az [float32]
        # TODO
        self.obs.head.dri.colla = self.scan_mbfits_header['PDELTACA']

        # Collimation error El [float32]
        # TODO
        self.obs.head.dri.colle = self.scan_mbfits_header['PDELTAIE']
        

        # Integration Time [float32][unit = second]
        # Use the integration time from the first row of the DATAPAR table.  Perhaps it could
        # change during the subscan, but we have to assume that the integration time does not
        # change so we can choose 1 number to prepare the Gildas header time resolution.
   
        self.obs.head.gen.time = datapar_mbfits["INTEGTIM"][0]
        print("Found integration time time = %f" % self.obs.head.gen.time)

        # Number of data points (angles) [int32][no unit]
        self.obs.head.dri.npoin = arraydata_mbfits.shape[0]
        print("Found number of points npoin = %d" % self.obs.head.dri.npoin)

        # Reference point [float32][no unit]
        # If number of points in drift is an odd number, use the center point.
        # else, create a reference point between the 2 center points.
        # This method assumes that the scan is designed to see the radio source in the center
        # after accounting for pre-programmed longitude (AZ, RA) and latitude (EL, DEC) offsets.

        # TODO improve this function to look at the offset angles in DATAPAR instead of than assume
        # that reference angle is exactly in the center of the subscan (typical)
        self.obs.head.dri.rpoin = np.round(self.obs.head.dri.npoin / 2)

        self.obs.head.dri.aref = datapar_mbfits[int(self.obs.head.dri.rpoin)]['LONGOFF'] *  DataFormatGildas.RADIANS_PER_DEGREE
        print("Angle Reference (aref) = {} [deg]".format(self.obs.head.dri.aref * DataFormatGildas.DEGREES_PER_RADIAN))
        
        # Angular resolution [float32][unit = radian]
        # convert arcseconds to radians

        # Choose list of angles to use to calculate the angular resolution. (2 choices)
        # Use the MBFITS keywords from DATAPAR_HEADER.SCANDIR to determine if it is a longitude drift (x)
        # or latitude drift (y).  lookup from the dictionary is the MBFITS keyword to get the list of angles
        # from DATAPAR.<keyword>
        try:
            # Select the list of angles from DATAPAR (2 choices)
            scandir = datapar_mbfits_header['SCANDIR'].upper()
            print("Select angle list. scan direction from DATAPAR_MBFITS_HEADER: %s" % scandir)

            angle_key = DataFormatGildas.map_angle_key[scandir]
            print("Selected angle key: %s" % angle_key)

            angle_list = datapar_mbfits[angle_key]

            # Calculate angular resolution.
            # calculate from -10% to +10% near the center of the subscan because we believe the antenna
            # moves at constant velocity.
            angle_index_min = int(
                self.obs.head.dri.rpoin - 0.1 * self.obs.head.dri.npoin
            )
            angle_index_max = int(
                self.obs.head.dri.rpoin + 0.1 * self.obs.head.dri.npoin
            )
            print(
                "Calculate angular resolution from index %d to %d of total %d"
                % (angle_index_min, angle_index_max, self.obs.head.dri.npoin)
            )

            angle_list_centered = angle_list[angle_index_min:angle_index_max]
            self.obs.head.dri.ares = (np.pi / 180) * (np.mean(np.diff(angle_list_centered)))
            print(
                "Calculated angular resolution = %f [rad], %f [arcsec]"
                % (self.obs.head.dri.ares, self.obs.head.dri.ares * 3600 * 180 / np.pi)
            )
        except Exception as e:
            print(
                "WARNING: Using default value of head.dri.ares = %f"
                % DataFormatGildas.defaults.head.dri.ares
            )
            self.obs.head.dri.ares = DataFormatGildas.defaults.head.dri.ares

        for pol in range(arraydata_mbfits['DATA'].shape[1]):
            self.obs.head.gen.teles = "{}-{}-{}-{}".format(
                self.scan_mbfits_header['TELESCOP'],
                "L", #FIXME: get from FEBE
                DataAggregatorDefaults.MAP_INDEX_TO_POLARIZATION[pol],
                DataAggregatorDefaults.MAP_INDEX_TO_NOISE[pol]
            )
            try:
                # Write the data
                self.obs.datay = np.sum(
                    arraydata_mbfits['DATA'][:, pol, freq_min:freq_max + 1], 
                    1,  # axis 1 (freq) pol dimension is lost because we already choose the index of polarization (singleton dimension)
                    dtype=np.float32
                )
                self.obs.write()
            except IndexError as e:
                print(e)
                self.cleanup()
                os.remove(self.fileout_path)
                print('Failed to create gildas from {}'.format(self.filename))
                print('Deleting {}'.format(self.fileout_path))
                raise e

    def cleanup(self):
        self.fid.close()
        self.fileout.close()
        

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Reads FITS file written by PipelineMBfits and diplays some graphs",
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "filename", 
        help="FITS file with format specified by AntennaDataFormat.py",
    )
    parser.add_argument(
        "min_freq_index",
        help="min frequency index interested", 
        type=int,
    )
    parser.add_argument(
        "max_freq_index",
        help="max frequency index interested", 
        type=int,
    )

    # If no command line arguments are avaiable, show help message and exit
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        exit()

    args = parser.parse_args()
    print(args.filename)
    print("Select frequency index {} -> {}".format(args.min_freq_index, args.max_freq_index))
    files = glob(args.filename)
    error_files = []
    for file in files:
        print('===========================================')
        print('creating gildas file from {}'.format(file))
        print('===========================================')

        try:
            gildas = Gildas()
            gildas.create(
                filename=file,
                freq_index=(args.min_freq_index,args.max_freq_index),
                skip_spe=True
            )
        except Exception as e:
            print(e)
            error_files.append(file)
    print('--------------------------------------------')
    print('Finish converting {} files'.format(len(files)))
    print('Successful {} files'.format(len(files) - len(error_files)))
    print('unsuccessful {} files: {}'.format(len(error_files), error_files))
    print('--------------------------------------------')