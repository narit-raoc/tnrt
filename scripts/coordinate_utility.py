import numpy as np
from astropy import units
from astropy.coordinates import AltAz
from astropy.coordinates import ICRS
from astropy.coordinates import Longitude
from astropy.coordinates import Latitude
from astropy.coordinates import SkyCoord
from astropy.utils import iers
from astropy.time import TimeDelta
from astropy.time import Time

SECONDS_PER_DAY = 86400
MILLISECONDS_PER_DAY = 1000 * SECONDS_PER_DAY

try:
    # For Python < 3.7, import erfa from astropy
    from astropy import _erfa as erfa
except:
    # Python >= 3.7, import erfa from pyerfa package
    # https://pypi.org/project/pyerfa/
    import erfa



def icrs_altaz_astropy(ra_icrs, dec_icrs, location_apy, time_apy):
    """
    Convert to AltAz using Astropy transform
    """
    frame_AltAz = AltAz(location=location_apy, obstime=time_apy)
    coord_icrs = ICRS(ra=ra_icrs, dec=dec_icrs)
    coord_altaz = coord_icrs.transform_to(frame_AltAz)
    return (coord_altaz.az.deg, coord_altaz.alt.deg)

def altaz_icrs_astropy(az, el, location_apy, time_apy):
    """
    Convert AltAz to ICRS using Astropy transform
    """
    coord_altaz = SkyCoord(
        frame="altaz",
        az=az,
        alt=el,
        obstime=time_apy,
        location=location_apy,
    )

    coord_icrs = coord_altaz.transform_to("icrs")

    return (coord_icrs.ra.deg, coord_icrs.dec.deg)

def icrs_altaz_iau(ra_icrs, dec_icrs, location_apy, time_apy, dut1_apy, pm_x, pm_y):
    """
    Convert to AltAz observed coordinates using SOFA model
    """
    # Note: assume proper motion, parallax, and radial velocity are 0
    # while debugging ACU tracking.  Fine-tune later when we are actually
    # searching for stars on sky.
    rc = ra_icrs.radian  # ICRS [ α, δ ] at J2000.0 (radians, Note 1)
    dc = dec_icrs.radian
    pr = 0  # RA proper motion (radians/year; Note 2)
    pd = 0  # Dec proper motion (radians/year)
    px = 0  # parallax (arcsec)
    rv = 0  # radial velocity (km/s, positive if receding)

    date1 = time_apy.jd1  # UTC as a 2-part. . .
    date2 = time_apy.jd2  # . . . quasi Julian Date (Notes 3,4)
    dut1 = dut1_apy.sec  # UT1−UTC (seconds, Note 5)

    elong = location_apy.lon.radian  # longitude (radians, east-positive, Note 6)
    phi = location_apy.lat.radian  # geodetic latitude (radians, Note 6)
    hm = location_apy.height.value  # height above ellipsoid (m, geodetic Notes 6,8)

    # polar motion coordinates (radians, Note 7)
    xp = (np.pi / 180) * (pm_x.value / 3600)
    yp = (np.pi / 180) * (pm_y.value / 3600)

    phpa = 0  # pressure at the observer (hPa ≡ mB, Note 8
    tc = 0  # ambient temperature at the observer (deg C)
    rh = 0  # relative humidity at the observer (range 0-1)
    wl = 1000  # wavelength (micrometers, Note 9)

    # Use IAU SOFA model to transform to AltAz observed
    # function name tco13 -> [transform][catalog] to [observed] model [2013]
    # Returns
    # -------
    # aob   observed azimuth (radians: N=0 ◦ , E=90 ◦ )
    # zob   observed zenith distance (radians)
    # hob   observed hour angle (radians)
    # dob   observed declination (radians)
    # rob   observed right ascension (CIO-based, radians)
    # eo    equation of the origins (ERA−GST)

    (aob, zob, hob, dob, rob, eo) = erfa.atco13(
        rc,
        dc,
        pr,
        pd,
        px,
        rv,
        date1,
        date2,
        dut1,
        elong,
        phi,
        hm,
        xp,
        yp,
        phpa,
        tc,
        rh,
        wl,
    )

    az = (aob * units.radian).to(units.deg).value

    # Result from IAU SOFA model is Zenith angle.  We want Alt/EL
    alt = ((90 * units.deg) - (zob * units.radian).to(units.deg)).value
    return (az, alt)

def icrs_to_geocentric_apparent_iau(ra_icrs, dec_icrs, time_apy):
    """
    Follow this example to get geocentric apparent RA, DEC coordinates
    Follow example in document http://www.iausofa.org/2019_0722_C/sofa/sofa_ast_c.pdf
    """
    # Convert units to use wth IAU models and get the raw value from the "smart" Quantity unit objects.
    rc = ra_icrs.radian  # ICRS [ α, δ ] at J2000.0 (radians, Note 1)
    dc = dec_icrs.radian  # RA proper motion (radians/year; Note 2)
    pr = 0  # RA proper motion (radians/year; Note 2)
    pd = 0  # Dec proper motion (radians/year)
    px = 0  # parallax (arcsec)
    rv = 0  # radial velocity (km/s, positive if receding)

    date1 = time_apy.jd1  # UTC as a 2-part. . .
    date2 = time_apy.jd2  # . . . quasi Julian Date (Notes 3,4)

    (ri, di, eo) = erfa.atci13(rc, dc, pr, pd, px, rv, date1, date2)

    # Convert scalar result to Quantity object with units
    # Rotate the right ascension from ERA (Earth Rotation Angle) origin to
    # GST (Greenwich Sidereal Time) origin using EO (Equation of the Origins)
    # erfa.anp wraps angle to value within range [0 .. 2 * pi]
    ra_geocentric_apparent = Longitude((erfa.anp(ri - eo)) * units.radian)
    dec_geocentric_apparent = Latitude(di * units.radian)

    # Convert to degrees and return data to use as input to ACU tracking generator
    return (
        ra_geocentric_apparent.to(units.deg),
        dec_geocentric_apparent.to(units.deg),
    )

def get_dut1(time_apy):
    iers_table = iers.IERS_Auto.open()
    dut1_apy = TimeDelta(iers_table.ut1_utc(time_apy.jd), format="sec")
    return(int(1000 * dut1_apy.sec))

def get_gst0hut1(location_apy, time_apy):
    # Convert UTC time to UT1, then get integer part of the UT1 day.
    # Note:
    # MJD increments the day number +1 at midnight (0h)
    # JD increments the day number at noon (12h)
    # UT1 day increment follows the same convention as MJD. Therefore we must use integer
    # of the MJD, *not* the JD (offset by 12 hours)
    starttime_ut1 = time_apy.ut1
    starttime_ut1_integer_day = int(starttime_ut1.mjd)

    startday_0hUT1 = Time(starttime_ut1_integer_day, format="mjd", scale="ut1")
    GST0hUT1 = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")

    # Create a new astropy Time object of fractional day on UT1 time scale
    return(int(MILLISECONDS_PER_DAY * (GST0hUT1.hourangle / 24.0)))