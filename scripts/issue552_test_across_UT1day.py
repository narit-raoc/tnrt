# Scan duration.  Use the on source scan duration to calculate schedule times for 3 scans
# - before UT1 day boundary
# - while crossing the UT1 day boundary
# - after UT1 day boundary
# The time on my local computer OS (scan schedule) and ACU will be fake anyway,
# so we can choose any day boundary that we want.   So, choose the first day boundary
# in the future and schedule the scans to be before, during, and after that boundary.
subscan_duration = 120
time_gap = 30

# Get timestamp MJD now and round to end of day (<24 hours in the future)
# Center the subscan duration on the UT1 Day boundary.
starttime_during = Time(np.ceil(Time.now().ut1.mjd), format="mjd") - (subscan_duration / 2) * units.s
#starttime_during = Time.now() + 30 * units.s
starttime_before = starttime_during - (subscan_duration * units.s) - (time_gap * units.s)
starttime_after = starttime_during + (subscan_duration * units.s) + (time_gap * units.s)

log("Scan (before UT1 day change) MJD: {}, ISO: {}".format(starttime_before.mjd, starttime_before.iso))
log("Scan (during UT1 day change) MJD: {}, ISO: {}".format(starttime_during.mjd, starttime_during.iso))
log("Scan (after UT1 day change) MJD: {}, ISO: {}".format(starttime_after.mjd, starttime_after.iso))

# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 180 * units.deg
el = 45 * units.deg
logger.debug(
    "Choose a coordinate above the horizon now in the middle of AZ range (no cable wrap). (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=starttime_during,
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0
send_icrs_to_acu = False
tracking_offset_az = 0.0
tracking_offset_el = 0.0

project_id = "issue551_track_across_UT1day"
obs_id = "SS"

source_name = "across_UT1"
line_name = "no_line"

scantype_params = ScantypeParamsOnSource(subscan_duration)

tracking_params = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    tracking_offset_az,
    tracking_offset_el,
)

schedule_params_before = ScheduleParams(project_id, obs_id, start_mjd=starttime_before.mjd)
schedule_params_during = ScheduleParams(project_id, obs_id, start_mjd=starttime_during.mjd)
schedule_params_after = ScheduleParams(project_id, obs_id, start_mjd=starttime_after.mjd)

# Create the scan and add it to the queue
add_scan(schedule_params_before, scantype_params, tracking_params)
add_scan(schedule_params_during, scantype_params, tracking_params)
add_scan(schedule_params_after, scantype_params, tracking_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
