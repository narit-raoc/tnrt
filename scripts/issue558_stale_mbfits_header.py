# Scan duration.  Use the on source scan duration to calculate schedule times for 3 scans
# - before UT1 day boundary
# - while crossing the UT1 day boundary
# - after UT1 day boundary
# The time on my local computer OS (scan schedule) and ACU will be fake anyway,
# so we can choose any day boundary that we want.   So, choose the first day boundary
# in the future and schedule the scans to be before, during, and after that boundary.

starttime = Time.now()

# Schedule
project_id = "issue551_track_across_UT1day"
obs_id = "SS"
line_name = "no_line"

# Scantype
subscan_duration = 5
scantype_params = ScantypeParamsOnSource(subscan_duration)

# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az_list = [100 * units.deg, 50 * units.deg, 25 * units.deg]
el_list = [60 * units.deg, 45 * units.deg, 30 * units.deg]
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0
send_icrs_to_acu = False
tracking_offset_az = 0.0
tracking_offset_el = 0.0

scan_id = 800

for (az, el) in zip(az_list, el_list):
    logger.debug(
        "Choose a coordinate above the horizon now in the middle of AZ range (no cable wrap). (AZ, EL) = ({}, {}) [deg]".format(
            az.value, el.value
        )
    )

    # Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
    coord_altaz = SkyCoord(
        frame="altaz",
        az=az,
        alt=el,
        obstime=starttime,
        location=get_site_location(),
    )
    coord_icrs = coord_altaz.transform_to("icrs")
    logger.debug(
        "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
            coord_icrs.ra.deg, coord_icrs.dec.deg
        )
    )

    # Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
    ra_catalog_icrs = coord_icrs.ra.deg
    dec_catalog_icrs = coord_icrs.dec.deg

    # Tracking
    tracking_params = TrackingParamsEQ(
        ra_catalog_icrs,
        dec_catalog_icrs,
        pm_ra,
        pm_dec,
        parallax,
        radial_velocity,
        send_icrs_to_acu,
        tracking_offset_az,
        tracking_offset_el)
    
    # Schedule
    schedule_params = ScheduleParams(project_id, obs_id, scan_id, start_mjd=starttime.mjd, source_name="az{}_el{}".format(az, el))
    scan_id = scan_id + 1

    # Add to queue
    add_scan(schedule_params, scantype_params, tracking_params)


# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)


# read results from file and check if the data matches what we expect.
from astropy.io import fits
for result in results:
    hdul = fits.open(results[result]["filenames"][0])
    log("Read SCAN-MBFITS header")
    log("SCANNUM: {}, BLONGOBJ: {}, BLATOBJ: {}".format(
        hdul["SCAN-MBFITS"].header["SCANNUM"],
        hdul["SCAN-MBFITS"].header["BLONGOBJ"], 
        hdul["SCAN-MBFITS"].header["BLATOBJ"]))
    hdul.close()