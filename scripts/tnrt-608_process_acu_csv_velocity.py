# Built in Python modules
import logging
import argparse
import os

# from pip
import coloredlogs
import numpy as np
from scipy.interpolate import interp1d
from astropy.time import Time
from astropy.io import fits
from astropy import units as u
from astropy.coordinates import EarthLocation
from astropy.coordinates import AltAz
from astropy.coordinates import ICRS

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets


def configure_logger(logger, level):
    logger.setLevel(level)
    fmt_scrn = "%(asctime)s [%(levelname)s] %(funcName)s(): %(message)s"
    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red", "bright": True},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta", "bright": True, "bold": True},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }

    formatter_screen = logging.Formatter(fmt=fmt_scrn)
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)
    handler_screen.setLevel(logging.DEBUG)
    logger.addHandler(handler_screen)


class AcuCompareTool:
    def __init__(self, loglevel):       
        self.logger = logging.getLogger(self.__class__.__name__)
        configure_logger(self.logger, loglevel)
        self.logger.debug("Started log for {}".format(self.logger.name))
    
    def process_acu_csv(self, acu_csv_filename):
        self.acu_csv_filename = acu_csv_filename
        self.logger.info("Select ACU file: {}".format(self.acu_csv_filename))
        
        self.acu_data = self.load_acu_csv()
        self.logger.debug("shape {}, dtype: {}".format(self.acu_data.shape, self.acu_data.dtype))
        
        self.indexof_tracking_enabled = np.where(self.acu_data['tracking_run_bit']==1)
        self.indexof_tracking_disabled = np.where(self.acu_data['tracking_run_bit']==0)        
        self.logger.debug("tracking_run_bit: {}, self.indexof_tracking_enabled: {}".format(self.acu_data['tracking_run_bit'], self.indexof_tracking_enabled))

        self.tt_acu = Time(self.acu_data['actual_time'], format="mjd")
        self.tt_acu_unix = self.tt_acu.unix # for convenience not to convert many times
    
    def load_acu_csv(self):
        dtype_row = np.dtype(
            [
                ("actual_time", np.float64),
                ("prog_track_az", np.float64),
                ("prog_track_el", np.float64),
                ("prog_offset_az", np.float64),
                ("prog_offset_el", np.float64),
                ("desired_az", np.float64),
                ("desired_el", np.float64),
                ("encoder_position_az", np.float64),
                ("encoder_position_el", np.float64),
                ("pointing_correction_az", np.float64),
                ("pointing_correction_el", np.float64),
                ("actual_position_az", np.float64),
                ("actual_position_el", np.float64),
                ("safety_device_errors", np.uint),
                ("tracking_bit_status", np.uint),
                ("tracking_run_bit", np.uint),
                ("prog_track_bs", np.uint),
                ("prog_offset_bs", np.uint),
                ("trajectory_generator_acceleration_az", np.float64),
                ("trajectory_generator_velocity_az", np.float64),
                ("desired_velocity_az", np.float64),
                ("current_velocity_az", np.float64),
                ("position_deviation", np.float64),
                ("filtered_position_deviation", np.float64),
            ]
        )

        self.logger.info("Loading ACU data from {} ...".format(self.acu_csv_filename))
        data = np.loadtxt(
            self.acu_csv_filename, dtype=dtype_row, delimiter=",", skiprows=1
        )
        self.logger.info("Done")
        
        return data
    
    def preview(self):
        app = QtWidgets.QApplication([])
        # NOTE: Default is dark background and CMY colors. Set white_background=True
        # if you want white background and RGB colors.
        #white_background = False
        white_background = False

        if white_background is True:
            pg.setConfigOption("background", "w")
            pg.setConfigOption("foreground", "k")

            # Use RGB color for white background
            color1 = "b"
            color2 = "r"
            color3 = "g"
            color4 = "m"
            color5 = "k"
        else:
            # Use CMY color for dark background
            color1 = "y"
            color2 = "m"
            color3 = "c"
            color4 = "b"
            color5 = "r"

        symbol1 = "o"
        symbol2= "o"
        symbol3 = "d"

        size1 = 18
        size2 = 12
        size3 = 6

        grid_opacity = 0.7

        ARCSEC_PER_DEG = 3600

        filename_display = os.path.basename(self.acu_csv_filename)
        # -----------------------------------------------------------------------------
        # window 1. ACU time, prog track AZ, actual AZ, mbfits AZ, astropy AZ
        # -----------------------------------------------------------------------------
        glw_tracking_az = pg.GraphicsLayoutWidget(show=True)
        glw_tracking_az.resize(1600, 1200)
        glw_tracking_az.setWindowTitle("{}_az_time".format(filename_display))
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem("{} AZIMUTH".format(filename_display))

        glw_tracking_az.addItem(label_config_summary, 0, 0)

        pi_az_time = glw_tracking_az.addPlot(1, 0, title=None)
        pi_az_time.addLegend()
        pi_az_time.plot(
            self.tt_acu_unix[self.indexof_tracking_enabled],
            self.acu_data['prog_track_az'][self.indexof_tracking_enabled],
            name="ACU Program Track AZ (algorithm) - TRACKING ENABLED",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_az_time.plot(
            self.tt_acu_unix[self.indexof_tracking_enabled],
            self.acu_data['actual_position_az'][self.indexof_tracking_enabled],
            name="ACU Actual Position AZ - TRACKING ENABLED",
            pen=None,
            symbol=symbol2,
            symbolBrush=None,
            symbolPen=color2,
            symbolSize=size2,
        )

        pi_az_time.plot(
            self.tt_acu_unix[self.indexof_tracking_disabled],
            self.acu_data['prog_track_az'][self.indexof_tracking_disabled],
            name="ACU Program Track AZ (algorithm) - TRACKING DISABLED",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size1,
        )
        pi_az_time.plot(
            self.tt_acu_unix[self.indexof_tracking_disabled],
            self.acu_data['actual_position_az'][self.indexof_tracking_disabled],
            name="ACU Actual Position AZ - TRACKING DISABLED",
            pen=None,
            symbol=symbol2,
            symbolBrush=None,
            symbolPen=color4,
            symbolSize=size2,
        )
        ax_L = pg.AxisItem(orientation="left", text="Azimuth", units="deg")
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_az_time.showGrid(True, True, alpha=grid_opacity)
                

        # -----------------------------------------------------------------------------
        # window 2. ACU time, prog track EL, actual EL, mbfits EL, astropy EL
        # -----------------------------------------------------------------------------
        glw_tracking_el = pg.GraphicsLayoutWidget(show=True)
        glw_tracking_el.resize(1600, 1200)
        glw_tracking_el.setWindowTitle("{}_el_time".format(filename_display))
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem("{} ELEVATION".format(filename_display))

        glw_tracking_el.addItem(label_config_summary, 0, 0)

        pi_el_time = glw_tracking_el.addPlot(1, 0)
        pi_el_time.addLegend()
        pi_el_time.plot(
            self.tt_acu_unix[self.indexof_tracking_enabled],
            self.acu_data['prog_track_el'][self.indexof_tracking_enabled],
            name="ACU Program Track EL (algorithm) - TRACKING ENABLED",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_el_time.plot(
            self.tt_acu_unix[self.indexof_tracking_enabled],
            self.acu_data['actual_position_el'][self.indexof_tracking_enabled],
            name="ACU Actual Position EL - TRACKING DISABLED",
            pen=None,
            symbol=symbol2,
            symbolBrush=None,
            symbolPen=color2,
            symbolSize=size2,
        )

        pi_el_time.plot(
            self.tt_acu_unix[self.indexof_tracking_disabled],
            self.acu_data['prog_track_el'][self.indexof_tracking_disabled],
            name="ACU Program Track EL (algorithm) - TRACKING ENABLED",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size1,
        )
        pi_el_time.plot(
            self.tt_acu_unix[self.indexof_tracking_disabled],
            self.acu_data['actual_position_el'][self.indexof_tracking_disabled],
            name="ACU Actual Position EL - TRACKING DISABLED",
            pen=None,
            symbol=symbol2,
            symbolBrush=None,
            symbolPen=color4,
            symbolSize=size2,
        )


        ax_L = pg.AxisItem(orientation="left", text="Elevation", units="deg")
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_el_time.showGrid(True, True, alpha=grid_opacity)

        # -----------------------------------------------------------------------------
        # window 3. AZ velocity
        # -----------------------------------------------------------------------------
        glw_velocity = pg.GraphicsLayoutWidget(show=True)
        glw_velocity.resize(1600, 1200)
        glw_velocity.setWindowTitle("{}_velocity".format(filename_display))
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem("{}".format(filename_display))

        glw_velocity.addItem(label_config_summary, 0, 0)

        pi_v_az = glw_velocity.addPlot(1, 0, title=None)
        pi_v_az.addLegend()
        pi_v_az.plot(
            self.tt_acu_unix,
            self.acu_data['trajectory_generator_velocity_az'],
            name="trajectory_generator_velocity_az",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_v_az.plot(
            self.tt_acu_unix,
            self.acu_data['desired_velocity_az'],
            name="desired_velocity_az",
            pen=None,
            symbol=symbol2,
            symbolBrush=None,
            symbolPen=color2,
            symbolSize=size1,
        )
        pi_v_az.plot(
            self.tt_acu_unix,
            self.acu_data['current_velocity_az'],
            name="current_velocity_az",
            pen=None,
            symbol=symbol3,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size1,
        )
        
        ax_L = pg.AxisItem(orientation="left", text="Azimuth Velocity", units="deg / s")
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_v_az.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_v_az.showGrid(True, True, alpha=grid_opacity)

        # -----------------------------------------------------------------------------
        # window 4. difference in velocities
        # -----------------------------------------------------------------------------
        glw_dvelocity = pg.GraphicsLayoutWidget(show=True)
        glw_dvelocity.resize(1600, 1200)
        glw_dvelocity.setWindowTitle("{}_velocity".format(filename_display))
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem("{}".format(filename_display))

        glw_dvelocity.addItem(label_config_summary, 0, 0)

        pi_dv_az = glw_dvelocity.addPlot(1, 0, title=None)
        pi_dv_az.addLegend()
        pi_dv_az.plot(
            self.tt_acu_unix,
            self.acu_data['desired_velocity_az'] - self.acu_data['trajectory_generator_velocity_az'],
            name="desired_velocity_az - trajectory_generator_velocity_az",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_dv_az.plot(
            self.tt_acu_unix,
            self.acu_data['current_velocity_az'] - self.acu_data['desired_velocity_az'],
            name="current_velocity_az - desired_velocity_az",
            pen=None,
            symbol=symbol2,
            symbolBrush=None,
            symbolPen=color2,
            symbolSize=size1,
        )
        pi_dv_az.plot(
            self.tt_acu_unix,
            self.acu_data['current_velocity_az'] - self.acu_data['trajectory_generator_velocity_az'],
            name="current_velocity_az - trajectory_generator_velocity_az",
            pen=None,
            symbol=symbol3,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size1,
        )
        
        ax_L = pg.AxisItem(orientation="left", text="Azimuth Velocity Deviation", units="deg / s")
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_dv_az.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_dv_az.showGrid(True, True, alpha=grid_opacity)

        # ------------------------------------------------------------------------------
        # Start the Qt event loop and block this script until user closes the GUI window
        app.exec_()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="python process_pointing_gap [ACU csv filename] [list of .mbfits filenames]"
    )
    parser.add_argument(
        dest="acu_csv_filename",
        type=str,
        help="ACU .csv file",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        default=False,
        action="store_true",
        help="Enable verbose logging (python logging level DEBUG. default=INFO)",
    )

    # Read command line arguments into argparse structure
    args = parser.parse_args()
    if args.verbose == True:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    logger = logging.getLogger(__name__)
    configure_logger(logger, loglevel)

    compare_tool = AcuCompareTool(loglevel)
    
    try:
        compare_tool.process_acu_csv(args.acu_csv_filename)
    except (FileNotFoundError, OSError) as e:
        logger.error(
            "{} not found in PATH. Current directory is {}".format(
                args.acu_csv_filename, os.getcwd()
            )
        )
    except (ValueError) as e:
        logger.error("ACU file not specified.  Do not load ACU .csv data")
    
    
    compare_tool.preview()
