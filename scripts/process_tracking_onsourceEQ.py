# -*- coding: utf-8 -*-
import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui

from astropy.io import fits
from astropy import units
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.time import TimeDelta
from astropy.utils import iers

from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

from coordinate_utility import icrs_altaz_astropy
from coordinate_utility import icrs_altaz_iau
from RaDecToAzEl import FB_RaDec2AzEl

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
# white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color_acu = "k"
    color_iau_no_polar_motion = "b"
    color_iau_polar_motion = "m"
    color_astropy = "r"
    color_manual = "g"
else:
    # Use CMY color for dark background
    color_acu = "b"
    color_iau_no_polar_motion = "y"
    color_iau_polar_motion = "b"
    color_astropy = "c"
    color_manual = "m"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# Constant
MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24

# Read important header information and add units
ra_catalog = Longitude(hdul["SCAN-MBFITS"].header["BLONGOBJ"], unit=units.deg)
dec_catalog = Latitude(hdul["SCAN-MBFITS"].header["BLATOBJ"], unit=units.deg)
print("SCAN Header: RA ICRS: {}".format(ra_catalog))
print("SCAN Header: DEC ICRS: {}".format(dec_catalog))

loc_scan_header = EarthLocation(
    EarthLocation.from_geodetic(
        lon=hdul["SCAN-MBFITS"].header["SITELONG"] * units.deg,
        lat=hdul["SCAN-MBFITS"].header["SITELAT"] * units.deg,
        height=hdul["SCAN-MBFITS"].header["SITEELEV"] * units.m,
        ellipsoid="WGS84",
    )
)

print("SCAN Header: Site Location Longitude: {}".format(loc_scan_header.lon))
print("SCAN Header: Site Location Latitude: {}".format(loc_scan_header.lat))
print("SCAN Header: Site Location Height: {}".format(loc_scan_header.height))

# Select in index to use in this script and print in debug messages.
# In the case of this test the values are constant throughout the entire array.
# However, in a very long observation, the value of dut1, ra_geocentric_apparent,
# and dec_geocentric_apparent can change by a small amount.
debug_index = 0

loc_acu = EarthLocation(
    EarthLocation.from_geodetic(
        lon=hdul["MONITOR-ANT-TRACKING"].data["antenna_longitude"][debug_index]
        * units.deg,
        lat=hdul["MONITOR-ANT-TRACKING"].data["antenna_latitude"][debug_index]
        * units.deg,
        height=hdul["MONITOR-ANT-TRACKING"].data["antenna_altitude"][debug_index]
        * units.m,
        ellipsoid="WGS84",
    )
)

dut1_acu = TimeDelta(
    hdul["MONITOR-ANT-TRACKING"].data["dut1"][debug_index] * units.ms, format="sec"
)
dut1_acu_ms = int(np.round(dut1_acu.value*1000))

GST0hUT1_ms = hdul["MONITOR-ANT-TRACKING"].data["gst0hut"][debug_index]
GST0hUT1 = Longitude((GST0hUT1_ms / MILLISECONDS_PER_DAY) * 24.0, unit=units.hourangle)

ra_acu_tracktable = Longitude(
    hdul["MONITOR-ANT-TRACKING-OBJECT"].data["pt_table11"], unit=units.deg
)
dec_acu_tracktable = Latitude(
    hdul["MONITOR-ANT-TRACKING-OBJECT"].data["pt_table12"], unit=units.deg
)

program_track_starttime_utc = Time(hdul["MONITOR-ANT-TRACKING-OBJECT"].data['pt_start_time'][debug_index],
                               format="mjd",
                               scale="utc",
                               location=loc_acu)

print("ACU: Site Location Longitude: {}".format(loc_acu.lon))
print("ACU: Site Location Latitude: {}".format(loc_acu.lat))
print("ACU: Site Location Height: {}".format(loc_acu.height))
print("ACU: DUT1 (UT1-UTC) : {}".format(dut1_acu))
print("ACU: GST0hUT1: {} ms".format(GST0hUT1_ms))
print("ACU: GST0hUT1: {}".format(GST0hUT1))
print("ACU: RA (system?): {}".format(ra_acu_tracktable))
print("ACU: DEC (system?): {}".format(dec_acu_tracktable))

# -----------------------------------------------------------------------------
# Select data while tracking on the object
# -----------------------------------------------------------------------------
# Find the section of the data file where the system is tracking (not slew to target).
# Use only this data for the remaining graphs.
AZ = 0
EL = 1
az_error = 3600 * (
    hdul["MONITOR-ANT-AXIS"].data["desired_position"][:, AZ]
    - hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, AZ]
)
el_error = 3600 * (
    hdul["MONITOR-ANT-AXIS"].data["desired_position"][:, EL]
    - hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL]
)
az_error_onsky = az_error * np.cos(
    hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL] * np.pi / 180.0
)
error_abs = np.sqrt(az_error_onsky**2 + el_error**2)
tolerance = 3600 * 10 # show all data that is within 10 degrees of tracking
#tolerance =  10 # filter the data down to display only when tracking within 10 arcsec
condition1 = np.array(error_abs < tolerance, dtype=bool)

# Second condition is when Program Track is "running" state
PTS_RUNNING = 1 << 4  # From ICD document.  bit 4 indicates program track running.
condition2 = np.array(
    (hdul["MONITOR-ANT-TRACKING"].data["prog_track_bs"] & PTS_RUNNING) > 0,
    dtype=bool,
)

index_to_keep = np.where(condition1 & condition2)

# processing_time_offset = 0.08783833660129127 * units.s
processing_time_offset = 0 * units.s

# Select the data
time_acu = (
    Time(
        hdul["MONITOR-ANT-GENERAL"].data["actual_time"][index_to_keep],
        format="mjd",
        scale="utc",
        location=loc_acu,
    )
    + processing_time_offset
)

starttime_ut1_integer_day = int(time_acu[0].ut1.mjd)
startday_0hUT1 = Time(starttime_ut1_integer_day, format="mjd", scale="ut1")
GST0hUT1_apy = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")
print("Sanity Check: Astropy: GST0hUT1: {}".format(GST0hUT1_apy))

# Get polar motion from IERS at the start time of tracking
iers_table = iers.IERS_Auto.open()
(pm_x, pm_y) = iers_table.pm_xy(Time.now().jd)  # arcsec
print("IERS: Polar motion x:{} arcsec".format(pm_x.value))
print("IERS: Polar motion y:{} arcsec".format(pm_y.value))
# xp = 8.369384499848348e-07
# yp = 2.348998103817482e-06

# Transform the coorinates using 3 different methods
print("Astropy transform ICRS -> Observed AltAz ...")
(az_astropy, alt_astropy) = icrs_altaz_astropy(
    ra_catalog, dec_catalog, loc_scan_header, time_acu
)
print("Done")
# dut1 is not in the scan header anymore. read it from ACU data
dut1_scan_header = TimeDelta(dut1_acu_ms * units.ms, format="sec")

print("SCAN Header: DUT1 (UT1-UTC) [s]: {}".format(dut1_scan_header))

print("IAU SOFA transform ICRS -> Observed AltAz (atco13) no polar motion ...")
(az_iau_no_polar_motion, alt_iau_no_polar_motion) = icrs_altaz_iau(
    ra_catalog,
    dec_catalog,
    loc_scan_header,
    time_acu,
    dut1_scan_header,
    units.quantity.Quantity(0),
    units.quantity.Quantity(0),
)
print("Done")

print("IAU SOFA transform ICRS -> Observed AltAz (atco13) including polar motion ...")
(az_iau_polar_motion, alt_iau_polar_motion) = icrs_altaz_iau(
    ra_catalog,
    dec_catalog,
    loc_scan_header,
    time_acu,
    dut1_scan_header,
    pm_x,
    pm_y,
)
print("Done")

print("RA / DEC (??? system) -> Observed AltAz (reproduce ACU algorithm)")
(az_manual, alt_manual, gst) = FB_RaDec2AzEl(
    time_acu.mjd,                       # ndarray
    program_track_starttime_utc.mjd,    # scalar
    dut1_acu_ms,                        # scalar
    GST0hUT1_ms,                        # scalar
    ra_acu_tracktable.deg,              # ndarray
    dec_acu_tracktable.deg,             # ndarray
    loc_acu.lon.deg,                    # scalar
    loc_acu.lat.deg,                    # scalar
    False
)

# Read the AZ and EL from ACU status message.
# Assume that all pointing corrections are 0.
# If any of the pointing corrections are non-zero, the value we read from the
# encoder will not align with the Astropy or IAU result.
az_acu = hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, 0][index_to_keep]
alt_acu = hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, 1][index_to_keep]

# -----------------------------------------------------------------------------
# Calculate Differences between solutions in arcseconds
# -----------------------------------------------------------------------------
# Between IAU SOFA model and ACU without adjustment of polar motion
d_az_iau_no_polar_motion_acu = 3600 * (az_iau_no_polar_motion - az_acu)
d_el_iau_no_polar_motion_acu = 3600 * (alt_iau_no_polar_motion - alt_acu)

# Between IAU SOFA model and ACU including adjustment of polar motion
d_az_iau_polar_motion_acu = 3600 * (az_iau_polar_motion - az_acu)
d_el_iau_polar_motion_acu = 3600 * (alt_iau_polar_motion - alt_acu)

# Between manual calculation and ACU
d_az_manual_acu = 3600 * (az_manual - az_acu)
d_el_manual_acu = 3600 * (alt_manual - alt_acu)

# Between Astropy SkyCoord transform and ACU
d_az_astropy_acu = 3600 * (az_astropy - az_acu)
d_el_astropy_acu = 3600 * (alt_astropy - alt_acu)

# Time axis data
tt = (time_acu - time_acu[0]).sec
time_unix = time_acu.unix

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "File {}. Procssing Time Offset: {}".format(
    os.path.basename(args.filename), processing_time_offset
)
grid_opacity = 0.7
app = QtGui.QApplication([])

# -----------------------------------------------------------------------------
# Graph window `1
# -----------------------------------------------------------------------------
win1 = pg.GraphicsLayoutWidget(show=True)
win1.resize(1024, 768)
win1.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Create the PlotItem
p1 = win1.addPlot(title=str_title)

# Some config before we add the lines
p1.setAspectLocked(True)
p1.addLegend()

line11 = p1.plot(
    az_acu, alt_acu, name="ACU Encoder", pen=color_acu, symbol="+", symbolPen=color_acu
)
line12 = p1.plot(
    az_iau_no_polar_motion,
    alt_iau_no_polar_motion,
    name="IAU SOFA atco13",
    pen=color_iau_no_polar_motion,
    symbol="x",
    symbolPen=color_iau_no_polar_motion,
)
line13 = p1.plot(
    az_manual,
    alt_manual,
    name="Manual",
    pen=color_manual,
    symbol="d",
    symbolPen=color_manual,
)
line14 = p1.plot(
    az_astropy,
    alt_astropy,
    name="Astropy",
    pen=color_astropy,
    symbol="o",
    symbolPen=color_astropy,
)

# Some config after we add the lines
axis_bottom1 = pg.AxisItem(orientation="bottom", text="AZ", units="deg")
axis_bottom1.showLabel(True)

axis_left1 = pg.AxisItem(orientation="left", text="EL", units="deg")
axis_left1.showLabel(True)

p1.setAxisItems(
    axisItems={
        "bottom": axis_bottom1,
        "left": axis_left1,
    }
)

p1.showGrid(True, True, alpha=grid_opacity)

# -----------------------------------------------------------------------------
# Graph window `2
# -----------------------------------------------------------------------------
win2 = pg.GraphicsLayoutWidget(show=True)
win2.resize(1024, 768)
win2.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))

# Create the PlotItem
p2a = win2.addPlot(title=str_title)
win2.nextRow()
p2b = win2.addPlot()

# Some config before we add the lines
p2a.addLegend()
p2b.addLegend()


line2a1 = p2a.plot(
    time_unix,
    az_acu,
    name="ACU Encoder",
    pen=color_acu,
    symbol="+",
    symbolPen=color_acu,
)
line2a2 = p2a.plot(
    time_unix,
    az_iau_no_polar_motion,
    name="IAU SOFA atco13",
    pen=color_iau_no_polar_motion,
    symbol="x",
    symbolPen=color_iau_no_polar_motion,
)
line2a3 = p2a.plot(
    time_unix,
    az_manual,
    name="Manual",
    pen=color_manual,
    symbol="d",
    symbolPen=color_manual,
)
line2a4 = p2a.plot(
    time_unix,
    az_astropy,
    name="Astropy",
    pen=color_astropy,
    symbol="o",
    symbolPen=color_astropy,
)

line2b1 = p2b.plot(
    time_unix,
    alt_acu,
    name="ACU Encoder",
    pen=color_acu,
    symbol="+",
    symbolPen=color_acu,
)
line2b2 = p2b.plot(
    time_unix,
    alt_iau_no_polar_motion,
    name="IAU SOFA atco13 (no polar motion)",
    pen=color_iau_no_polar_motion,
    symbol="x",
    symbolPen=color_iau_no_polar_motion,
)
line2b3 = p2b.plot(
    time_unix,
    alt_manual,
    name="Manual",
    pen=color_manual,
    symbol="d",
    symbolPen=color_manual,
)
line2b4 = p2b.plot(
    time_unix,
    alt_astropy,
    name="Astropy",
    pen=color_astropy,
    symbol="o",
    symbolPen=color_astropy,
)

# Some config after we add the lines
axis_bottom2a = pg.DateAxisItem(
    orientation="bottom", utcOffset=0, text="Universal Time Coordinated (UTC)"
)
# = pg.AxisItem(orientation="bottom", text="Time after start", units="s")
axis_bottom2a.showLabel(True)

axis_left2a = pg.AxisItem(orientation="left", text="AZ", units="deg")
axis_left2a.showLabel(True)

# axis_bottom2b = pg.AxisItem(orientation="bottom", text="Time after start", units="s")
axis_bottom2b = pg.DateAxisItem(
    orientation="bottom", utcOffset=0, text="Universal Time Coordinated (UTC)"
)
axis_bottom2b.showLabel(True)

axis_left2b = pg.AxisItem(orientation="left", text="EL", units="deg")
axis_left2b.showLabel(True)

p2a.setAxisItems(
    axisItems={
        "bottom": axis_bottom2a,
        "left": axis_left2a,
    }
)

p2b.setAxisItems(
    axisItems={
        "bottom": axis_bottom2b,
        "left": axis_left2b,
    }
)

p2a.showGrid(True, True, alpha=grid_opacity)
p2b.showGrid(True, True, alpha=grid_opacity)

# -----------------------------------------------------------------------------
# Graph window `3
# -----------------------------------------------------------------------------
win3 = pg.GraphicsLayoutWidget(show=True)
win3.resize(1024, 768)
win3.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))

# Create the PlotItem
p3 = win3.addPlot(title=str_title)

# Configuration before we add data
p3.setAspectLocked(True)
p3.addLegend()

line31 = p3.plot(
    d_az_iau_no_polar_motion_acu,
    d_el_iau_no_polar_motion_acu,
    name="IAU SOFA atco13(not including polar motion) - ACU",
    pen=None,
    symbol="+",
    symbolPen=color_iau_no_polar_motion,
)
line32 = p3.plot(
    d_az_iau_polar_motion_acu,
    d_el_iau_polar_motion_acu,
    name="IAU SOFA atco13(including polar motion) - ACU",
    pen=None,
    symbol="+",
    symbolPen=color_iau_polar_motion,
)
line33 = p3.plot(
    d_az_manual_acu,
    d_el_manual_acu,
    name="Manual - ACU",
    pen=None,
    symbol="+",
    symbolPen=color_manual,
)
line34 = p3.plot(
    d_az_astropy_acu,
    d_el_astropy_acu,
    name="Astropy - ACU",
    pen=None,
    symbol="+",
    symbolPen=color_astropy,
)

# Some config after we add the lines
axis_bottom3 = pg.AxisItem(orientation="bottom", text="difference AZ", units="arcsec")
axis_bottom3.showLabel(True)

axis_left3 = pg.AxisItem(orientation="left", text="difference EL", units="arcsec")
axis_left3.showLabel(True)

p3.setAxisItems(
    axisItems={
        "bottom": axis_bottom3,
        "left": axis_left3,
    }
)

p3.showGrid(True, True, alpha=grid_opacity)

# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()
