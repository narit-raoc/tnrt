# Schedule
project_id = "BackendEDD_headers"
obs_id = "SS"
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    source_name=source_name,
    line_name=line_name,
)

# Scan Type
duration = 10  # seconds.  If using MockAcu, this number doesn't change anything because tracking is "already arrived"
scantype_params = ScantypeParamsOnSource(duration)

# Tracking
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

# TCS Data pipelines.  
# Note: When BackendEDD is configured for pulsar, vlbi
# "spectrum_preview" doesn't have data.  no effect
# "mbfits" doesn't have spectrum data, but should still have ACU data in the MONITOR-ANT-xxxxx tables
data_params = DataParams(["mbfits"])

# Frontend configuration
# Select L-band receiver.  
# Currently (05/2024), we don't have any config parameters for the L-band frontend, but soon will have the option auto-enable LNA.
# frontend_params = FrontendParamsL()
frontend_params = None

# Use BackendMockEDD to simulate data
backend_params_spectrometer = BackendParamsEDD("TNRT_dualpol_spectrometer_L", mock=True, freq_res=244140.625, integration_time=2.0)

# Add some scans
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params_spectrometer, data_params)

results = run_queue()