# Schedule parameters
project_id = 'test_stowpin'
obs_id = 'ss'

# Scan type parameters.
# Typically, a manual scan we must stop manually by command stop_queue().
# We have option to set the time for the scan to automatically timeout and stop.
# If we don't set the duration, it will automatically stop after 60 minutes 
# (file size grows approximately 1 MB per minute)
duration = 300 # seconds

schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsManual(duration)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params)

# Run all scans in the queue.  Use blocking=False to add manual commands
# and manual stop scan after your commands are finished.

# Note, when using ScantypeParamsManual, the ACU tracking generator is not active
# therefore some offsets such as pointing model and refraction do not affect the result.
# These parameters are used by the tracking trajectory generator.
results = run_queue(blocking=False)

# -------------------------------------------------------------------------------
# Do something interesting (that doesn't depend on tracking trajectory generator)

# Go to AZ EL near the stow position.  This function has blocking flow control, so we
# don't have to guess.
goto_azel(40, 40)

# Use convenient nash functions to stow AZ and EL axes
# NOTE: These stow pin functions don't have smart flow control, so we have to wait
# in this script.
stow()

# TODO stow THU / GRS stow pins after we have a function for this.
log('stow THU, GRS')
session.antenna.objref.driveToStow(tnrtAntennaMod.THU, tnrtAntennaMod.stowPositionNearest, 0.5)
session.antenna.objref.driveToStow(tnrtAntennaMod.GRS, tnrtAntennaMod.stowPositionNearest, 0.5)

# Wait and record data that includes time stamps from ACU and TCS computer
waitrel(10)

# Unstow AZ , EL
unstow()

# TODO unstow THU, GRS
log('unstow THU, GRS')
session.antenna.objref.unstow(tnrtAntennaMod.THU, tnrtAntennaMod.stowpinBoth)
session.antenna.objref.unstow(tnrtAntennaMod.GRS, tnrtAntennaMod.stowpin1)


# -------------------------------------------------------------------------------
# Finished manual commands.  Now manual stop the scan.
# If you don't stop a non-blocking scan now, 
# it will timeout after `duration` seconds.
results = stop_queue()
