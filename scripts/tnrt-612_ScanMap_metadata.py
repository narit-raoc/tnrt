# --------
# Schedule
# --------
project_id = "no_projid"
obs_id = "no_obsid"

schedule_params = ScheduleParams(project_id, obs_id, source_name="sim_source", line_name="sim_line")

# --------
# Tracking
# --------
az = 150
el = 60
tracking_params = TrackingParamsHO(az, el)

# --------
# Backend
# --------
backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=250000)

# ---------------------------------------
# Scan type - Map, YES center cal, NO zigzag (same as Holography obs)
# ---------------------------------------
line_length = 7200
time_per_line = 10
nlines = 10
spacing = 720
axis = "x"
zigzag = False
coord_system = "HO"
subscans_per_cal = 1
time_cal0 = 30
time_per_cal = 5

scantype_params = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
    zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

# Run all scans in the queue.  show results JSON after queue is finished.
results = run_queue()

log("all results: {}".format(results))
