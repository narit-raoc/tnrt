import numpy as np
import json
from astropy import units as u
from astropy.coordinates import EarthLocation
from astropy.coordinates import Longitude
from astropy.coordinates import Latitude
from astropy.time import Time

# import local modules from current path (tnrt/scripts/)
import coordinate_utility as cu
from RaDecToAzEl import FB_RaDec2AzEl

# -------------------------------------------
# Specify site Location
# -------------------------------------------
lon = 99.216805
lat = 18.864348
height = 403.625

loc = EarthLocation(
    EarthLocation.from_geodetic(
        lon=lon * u.deg,
        lat=lat * u.deg,
        height=height * u.m,
        ellipsoid="WGS84",
    )
)

# -------------------------------------------
# Specify program track start position AZ. EL angles
# -------------------------------------------
# Shortcut algorithm maps [0 .. +60] to [+360 .. +420]
# Shortcut algorithm maps [+300 .. +360] to [-60 .. 0]

# Track object at 90, then load next object that starts at 330 
# - confirm that AZ slews (-) and start tracking at -30 instead of +330

# Track object at 270, then load next object that starts at 30
# - confirm that AZ slews (+) and start tracking at +390 instead of +30

az_start_position = np.array([0, 30, 90, 270, 330]) * u.deg

# Broadcast EL to make an array same size as az, but constant value 45
el_start_position = np.full(len(az_start_position), 45) * u.deg

# -------------------------------------------
# Calculate ICRS catalog RA, DEC
# -------------------------------------------
# Transform the az, EL coordinate to ICRS that will be used to configure the tracking.
# and position the antenna at the AZ, EL tracking start positions already specified.
# Use only the start time (now) to create the catalog coordinate
tracking_starttime_apy = Time(60152, format="mjd", scale="utc")
(ra_icrs, dec_icrs) = cu.altaz_icrs_astropy(az_start_position, el_start_position, loc, tracking_starttime_apy)

# -------------------------------------------
# Get DUT1 for tracking start time
# Calculate GST0hUT1 for TNRT site and tracking start time
# -------------------------------------------
dUT1_ms = cu.get_dut1(tracking_starttime_apy)
GST0hUT1_ms = cu.get_gst0hut1(loc, tracking_starttime_apy)

# -------------------------------------------
# Specify time
# -------------------------------------------
# Generate time steps for program track table - 24 hours of data
# (same as TNRT current software)
duration_1day = 86400
n_steps = 50
time_ndarray = tracking_starttime_apy + np.linspace(0, duration_1day, n_steps) * u.second

print("location: {}".format(loc.geodetic))
print("starttime MJD, data [day]: {}".format(tracking_starttime_apy.mjd))
print("az shape: {}, data [deg]: {}".format(az_start_position.shape, az_start_position.value))
print("el_start_position shape: {}, data [deg]: {}".format(el_start_position.shape, el_start_position.value))
print("ra_icrs shape: {}, data [deg]: {}".format(ra_icrs.shape, ra_icrs))
print("dec_icrs shape: {}, data [deg]: {}".format(ra_icrs.shape, dec_icrs))
print("dUT1_ms [ms]: {}".format(dUT1_ms))
print("GST0hUT1_ms [ms]: {}".format(GST0hUT1_ms))

for (az, el, rc, dc) in zip(az_start_position.value, el_start_position.value, ra_icrs, dec_icrs):
    # Transform ICRS RA DEC to Geocentric Apparent so we can use the sidereal time method.
    # Using the legacy ACU tracking algorithm, this step is required by the TCS user
    # before we load RA DEC into the ACU tracking table.
    
    # Prepare Astropy Angle objects from scalar r,d
    r = Longitude(rc, unit=u.deg)
    d = Latitude(dc, unit=u.deg)
    (ra_geocentric_apparent, de_geocentric_apparent) = cu.icrs_to_geocentric_apparent_iau(r, d, time_ndarray)
    
    # Use the python translation of ACU tracking algorithm
    # Note: this algorithm does not use any Astropy data types
    # only scalar values and arrays of scalar values
    (az_acu, el_acu, gst) = FB_RaDec2AzEl(
        time_ndarray.mjd,
        tracking_starttime_apy.mjd,
        dUT1_ms,
        GST0hUT1_ms,
        ra_geocentric_apparent.deg,
        de_geocentric_apparent.deg,
        loc.lon.deg,
        loc.lat.deg,
        False,
        )

    # For confidence, compare ACU algorithm result with Astropy transform
    (az_astropy, el_astropy) = cu.icrs_altaz_astropy(r, d, loc, time_ndarray)

    # Calculate difference between algorithms for confidence check
    d_az_astropy_acu = az_acu - az_astropy
    d_el_astropy_acu = el_acu - el_astropy

    print("\naz_start_position: {}".format(az))
    print("ra_icrs [deg]: {}, ra_geocentric_apparent [deg]: {}".format(r, ra_geocentric_apparent))
    print("de_icrs [deg]: {}, de_geocentric_apparent [deg]: {}".format(d, de_geocentric_apparent))
    print("az_acu [deg]: {}".format(az_acu))
    print("el_acu [deg]: {}".format(el_acu))
    print("az_acu_astropy [deg]: {}".format(az_astropy))
    print("el_acu_astropy [deg]: {}".format(el_astropy))
    print("d_az_astropy_acu [arcsec]: {}".format(d_az_astropy_acu))
    print("d_el_astropy_acu [arcsec]: {}".format(d_el_astropy_acu))
    
    # Write the file
    filename = "startpos_az{:03d}_el{:03d}".format(int(az), int(el))
    print("filename: {}".format(filename))
    
    header_dict = {"lon" : lon,
                   "lat" : lat,
                    "height" : height,
                    "startpos_az" : az,
                    "startpos_el" : el,
                    "starttime_mjd" : tracking_starttime_apy.mjd,
                    "dUT1_ms" : dUT1_ms,
                    "GST0hUT1_ms" : GST0hUT1_ms,
                    "ra_icrs" : rc,
                    "dec_icrs" : dc}
    print(header_dict)
    
    with open(filename+".json", "w") as outfile:
        json.dump(header_dict, outfile)

    out_array = np.stack([
        time_ndarray.mjd, 
        ra_geocentric_apparent.deg,
        de_geocentric_apparent.deg,
        az_acu,
        el_acu,
        az_astropy,
        el_astropy,
        d_az_astropy_acu,
        d_el_astropy_acu
        ], axis=1)
    print("out_array shape: {}".format(out_array.shape))
    
    np.savetxt(filename+".csv", 
            out_array, 
            delimiter=",", 
            header="mjd,ra_geocentric_apparent,dec_geocentric_apparent,az_acu,el_acu,az_astropy,el_astropy,d_az_astropy_acu,d_el_astropy_acu",
            fmt="%f")
