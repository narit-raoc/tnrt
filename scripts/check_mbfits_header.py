import argparse
from argparse import RawTextHelpFormatter
import sys
import numpy as np
import os

from astropy.io import fits

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument("filename", help="FITS file with format specified by DataFormatMbfits.py")

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()


# Expected header
scan_mbfits_header = hdul['SCAN-MBFITS'].header
print('==================')
print('Basic information about scan')
print('==================')
print('TELESCOP:', scan_mbfits_header['TELESCOP'])
print('SITELONG:', scan_mbfits_header['SITELONG'])
print('SITELAT:', scan_mbfits_header['SITELAT'])
print('SITEELEV:', scan_mbfits_header['SITEELEV'])
print('DIAMETER:', scan_mbfits_header['DIAMETER'])
print('PROJID:', scan_mbfits_header['PROJID'])
print('OBSID:', scan_mbfits_header['OBSID'])
print('SCANNUM:', scan_mbfits_header['SCANNUM'])
print('TIMESYS:', scan_mbfits_header['TIMESYS'])
print('DATE-OBS:', scan_mbfits_header['DATE-OBS'])
print('MJD:', scan_mbfits_header['MJD'])
print('NSUBS:', scan_mbfits_header['NSUBS'])
print('OBJECT:', scan_mbfits_header['OBJECT'])

print()

print('==================')
print('scan settings')
print('==================')
print('RADESYS:', scan_mbfits_header['RADESYS'])
print('EQUINOX:', scan_mbfits_header['EQUINOX'])
print('BLNGTYPE:', scan_mbfits_header['BLNGTYPE'])
print('BLATTYPE:', scan_mbfits_header['BLATTYPE'])
print('NLNGTYPE:', scan_mbfits_header['NLNGTYPE'])
print('NLATTYPE:', scan_mbfits_header['NLATTYPE'])
print('BLONGOBJ:', scan_mbfits_header['BLONGOBJ'])
print('BLATOBJ:', scan_mbfits_header['BLATOBJ'])
print('MOVEFRAM:', scan_mbfits_header['MOVEFRAM'])
print('SCANTYPE:', scan_mbfits_header['SCANTYPE'])
print('SCANMODE:', scan_mbfits_header['SCANMODE'])
print('SCANGEOM:', scan_mbfits_header['SCANGEOM'])
print('SCANLINE:', scan_mbfits_header['SCANLINE'])
print('SCANRPTS:', scan_mbfits_header['SCANRPTS'])
print('SCANLEN:', scan_mbfits_header['SCANLEN'])
print('SCANTIME:', scan_mbfits_header['SCANTIME'])

print()

# for i in range(scan_mbfits_header['NSUBS']):
arraydata_mbfits_header = hdul['ARRAYDATA-MBFITS'].header
print('==================')
print('subscan ARRAYDATA (first subscan)')
print('==================')
print('FEBE:', arraydata_mbfits_header['FEBE'])
print('CHANNELS:', arraydata_mbfits_header['CHANNELS'])
print('NUSEFEED:', arraydata_mbfits_header['NUSEFEED'])
print('FREQRES:', arraydata_mbfits_header['FREQRES'])
print('MOLECULE:', arraydata_mbfits_header['MOLECULE'])
print('freq axis')
print('--------')
print('1CRPX2F:', arraydata_mbfits_header['1CRPX2F'], '(Ref. channel)')
print('1CDLT2F:', arraydata_mbfits_header['1CDLT2F'], '(Channel separation [Hz])')
print('1CUNI2F:', arraydata_mbfits_header['1CUNI2F'], '(Unit)')
print('1CRVL2F:', arraydata_mbfits_header['1CRVL2F'], '(Frequency at ref. channel)')

print('==================')
print('subscan DATAPAR summary(all subscans)')
print('==================')

print("DATE-OBS:\t\t\tSUBSNUM\tSUBSTYPE\tNLNGTYPE\tNLATTYPE")
for hdu in hdul:
    if hdu.name == "DATAPAR-MBFITS":
        # print("name is 'DATAPAR-MBFITS'. Append to datapar_list")
        print("{}\t\t{}\t'{}'\t\t'{}'\t'{}'".format(
            hdu.header["DATE-OBS"],
            hdu.header["SUBSNUM"],
            hdu.header["SUBSTYPE"],
            hdu.header["NLNGTYPE"],
            hdu.header["NLATTYPE"]))

    