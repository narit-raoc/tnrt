# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 150 * units.deg
el = 60 * units.deg
logger.debug(
    "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0

send_icrs_to_acu = False
tracking_offset_az = 0.0
tracking_offset_el = 0.0

delay_until_scheduled_start = 0

project_id = "no_projid"
obs_id = "no_obsid"

source_name = "no_source"
line_name = "no_line"

duration = 10

# schedule_params = ScheduleParams(project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name)
schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsOnSource(duration)
tracking_paramsEQ = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    tracking_offset_az,
    tracking_offset_el,
)

tracking_paramsHO = TrackingParamsHO(az.value-60, el.value-30)

# Add scan, run unti complete to prepare position of antenna
add_scan(schedule_params, scantype_params, tracking_paramsHO)
result = run_queue()

# Add scan, CTRL+C to cancel while slewing
log("===== >>>>> PRESS CTRL+C AFTER 15 seconds <<<<<<=========================", level=WARNING)
add_scan(schedule_params, scantype_params, tracking_paramsEQ)
results = run_queue()

# Add scan again and run until complete
add_scan(schedule_params, scantype_params, tracking_paramsEQ)
result = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
