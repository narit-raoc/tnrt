# Minimum schedule parameters
project_id = 'no_projid'
obs_id = 'no_obsid'

# Scantype Parameters
subscan_duration = 10

# Tracking
az = 180
el = 60
tracking_offset_az = 0
tracking_offset_el = 0

# Parameter Blocks
schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsOnSource(subscan_duration)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# [1] No corrections
# -------------------
add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for no pointing corrections: {}'.format(results))

# [2] Refraction correction EL
# -------------------
# from Atmosphere test script printELcorrection.py
# get updated values of p,t,h
# Pressure: 913.30, Temperature:  21.70, Humidity 70.00
# r0: 65.65		b1:  5.90		b2:  2.50
# __________________________________________________

# EL angle (deg):  0.0	ElevationCorrection: 1592.979542
# EL angle (deg): 30.0	ElevationCorrection: 112.884381
# EL angle (deg): 60.0	ElevationCorrection: 37.759880
# EL angle (deg): 90.0	ElevationCorrection: 0.073086

set_refraction(65.65, 5.9, 2.5)


add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for set_refraction(65.65, 5.9, 2.5): {}'.format(results))
