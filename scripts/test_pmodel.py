# Minimum schedule parameters
project_id = 'no_projid'
obs_id = 'no_obsid'

# Scantype Parameters
duration = 10

# Tracking
az = 15
el = 66
tracking_offset_az = 0
tracking_offset_el = 0

# Pattern
arm_length = 10800
time_per_arm = 60

# Parameter objects
schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsCross(arm_length, time_per_arm)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# From Line chat 20230208
# @Spiro Captain แจ๊ค so, as suggested yesterday as responding to your simulation result in ACU, 
# could you test in case of inputting the practical P1 & P7 values (+3297, +1987) arcsec, and check "pointCorr[AZ]" 
# to be how much? if the value with the order of 100 arcsec come up with, it does make sense to cause offsets 
# everytime occurred in AZ direction as deltaAZ

# [1] No corrections
# -------------------
add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for no pointing corrections: {}'.format(results))

# [2] pointing model AZ only
# ------------------
set_pmodel(3297, 0, 0, 0, 0, 0, 0, 0, 0)

add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for set_pmodel(3297, 0, 0, 0, 0, 0, 0, 0, 0): {}'.format(results))

# [3] pointing model EL only
# ------------------
set_pmodel(0, 0, 0, 0, 0, 0, 1987, 0, 0)

add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for set_pmodel(0, 0, 0, 0, 0, 0, 1987, 0, 0): {}'.format(results))

# [4] pointing model AZ and EL
# ------------------
set_pmodel(3297, 0, 0, 0, 0, 0, 1987, 0, 0)

add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for set_pmodel(3297, 0, 0, 0, 0, 0, 1987, 0, 0): {}'.format(results))

clear_pmodel()

# [5] No offsets or corrections
# --------------
add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for no pointing corrections AFTER: {}'.format(results))
