# Minimum schedule parameters
project_id = 'no_projid'
obs_id = 'no_obsid'

# Scantype Parameters
subscan_duration = 10

# Tracking
az = 180
el = 60
tracking_offset_az = 0
tracking_offset_el = 0

# Parameter Blocks
schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsOnSource(subscan_duration)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# [1] No corrections
# -------------------
add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for no pointing corrections: {}'.format(results))

# [2] Tracking offsets AZ, EL
# -------------------
set_pmodel(3600, 0, 0, 0, 0, 0, 7200, 0, 0)

add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for set_pmodel(3600, 0, 0, 0, 0, 0, 7200, 0, 0): {}'.format(results))
