# Built in Python modules
import logging
import argparse
import os

# from pip
import coloredlogs
import numpy as np
from scipy.interpolate import interp1d
from astropy.time import Time
from astropy.io import fits
from astropy import units as u
from astropy.coordinates import EarthLocation
from astropy.coordinates import AltAz
from astropy.coordinates import ICRS

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui


def configure_logger(logger, level):
    logger.setLevel(level)
    fmt_scrn = "%(asctime)s [%(levelname)s] %(funcName)s(): %(message)s"
    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red", "bright": True},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta", "bright": True, "bold": True},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }

    formatter_screen = logging.Formatter(fmt=fmt_scrn)
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)
    handler_screen.setLevel(logging.DEBUG)
    logger.addHandler(handler_screen)


class AcuCompareTool:
    def __init__(self, loglevel):       
        self.logger = logging.getLogger(self.__class__.__name__)
        configure_logger(self.logger, loglevel)
        self.logger.debug("Started log for {}".format(self.logger.name))
    
    def process_acu_csv(self, acu_csv_filename):
        self.acu_csv_filename = acu_csv_filename
        self.logger.info("Select ACU file: {}".format(self.acu_csv_filename))
        
        self.acu_data = self.load_acu_csv()
        self.logger.debug("shape {}, dtype: {}".format(self.acu_data.shape, self.acu_data.dtype))
        
        self.indexof_tracking_enabled = np.where(self.acu_data['tracking_run_bit']==1)        
        self.logger.debug("tracking_run_bit: {}, self.indexof_tracking_enabled: {}".format(self.acu_data['tracking_run_bit'], self.indexof_tracking_enabled))

        self.tt_acu = Time(self.acu_data['actual_time'], format="mjd")
        self.tt_acu_unix = self.tt_acu.unix # for convenience not to convert many times
    
    def load_acu_csv(self):
        dtype_row = np.dtype(
            [
                ("actual_time", np.float64),
                ("prog_track_az", np.float64),
                ("prog_track_el", np.float64),
                ("prog_offset_az", np.float64),
                ("prog_offset_el", np.float64),
                ("desired_az", np.float64),
                ("desired_el", np.float64),
                ("encoder_position_az", np.float64),
                ("encoder_position_el", np.float64),
                ("pointing_correction_az", np.float64),
                ("pointing_correction_el", np.float64),
                ("actual_position_az", np.float64),
                ("actual_position_el", np.float64),
                ("safety_device_errors", np.uint),
                ("tracking_bit_status", np.uint),
                ("tracking_run_bit", np.uint),
                ("prog_track_bs", np.uint),
                ("prog_offset_bs", np.uint),
                ("trajectory_generator_acceleration_az", np.float64),
                ("trajectory_generator_velocity_az", np.float64),
                ("desired_velocity_az", np.float64),
                ("current_velocity_az", np.float64),
                ("position_deviation", np.float64),
                ("filtered_position_deviation", np.float64),
            ]
        )

        self.logger.info("Loading ACU data from {} ...".format(self.acu_csv_filename))
        data = np.loadtxt(
            self.acu_csv_filename, dtype=dtype_row, delimiter=",", skiprows=1
        )
        self.logger.info("Done")
        
        return data
    
    def process_mbfits(self, mbfits_dir, shift_time):
        self.mbfits_dir = mbfits_dir
        self.logger.info("Select MBFITS directory: {}".format(self.mbfits_dir))
        if shift_time == False:
            self.logger.debug("shift_time = False.  Don't change any timestamps or interpolated angle data")
        else:
            self.logger.warning("shift_time  = True.  Adjust timestamps and re-interpolate data")

        # Read some relevant information from .mbfits and create long "flat"
        # lists that include data for all mbfits files, for all subscans within
        # each mbfits file.  The result data will be a structure that is comparable
        # to the ACU data that have a long array of time and angles 
        # -- but no separation of scans and subscans  
        datapar_selected_buffer = {
            "MJD" : [],
            "LONGOFF" : [],
            "LATOFF" : [],
            "AZIMUTH" : [],
            "ELEVATIO" : [],
            }
        
        mbfits_dict = {
            "MJD" : [],
            "LONGOFF" : [],
            "LATOFF" : [],
            "AZIMUTH" : [],
            "ELEVATIO" : [],
            }

        mbfits_filename_list = sorted(os.listdir(self.mbfits_dir))
        self.logger.debug("filename list: {}".format(mbfits_filename_list))
        for filename in mbfits_filename_list:
            filepath = "{}/{}".format(self.mbfits_dir, filename)
            self.logger.info("Processing: {}".format(filepath))
            data_one_file = self.load_mbfits(filepath, shift_time)
            
            # Convert data to lists, append to buffer
            #self.logger.debug("data_one_file['MJD'] type: {}, shape: {}, data: {}".format(type(data_one_file['MJD']), data_one_file['MJD'].shape, data_one_file['MJD']))
            d1f_flat = data_one_file["MJD"].flatten()
            #self.logger.debug("d1f_flat type: {}, shape: {}, data: {}".format(type(d1f_flat), d1f_flat.shape, d1f_flat))
            #self.logger.debug("data_one_file['MJD'] type: {}, shape: {}, data: {}".format(type(data_one_file['MJD']), data_one_file['MJD'].shape, data_one_file['MJD']))
            datapar_selected_buffer['MJD'].append(data_one_file['MJD'])
            datapar_selected_buffer['LONGOFF'].append(data_one_file['LONGOFF'])
            datapar_selected_buffer['LATOFF'].append(data_one_file['LATOFF'])
            datapar_selected_buffer['AZIMUTH'].append(data_one_file['AZIMUTH'])
            datapar_selected_buffer['ELEVATIO'].append(data_one_file['ELEVATIO'])

        
        self.logger.debug("Flatten the list of arrays from all files into 1 long array")
        mbfits_dict["MJD"] = np.concatenate(datapar_selected_buffer["MJD"])
        mbfits_dict["LONGOFF"] = np.concatenate(datapar_selected_buffer["LONGOFF"])
        mbfits_dict["LATOFF"] = np.concatenate(datapar_selected_buffer["LATOFF"])
        mbfits_dict["AZIMUTH"] = np.concatenate(datapar_selected_buffer["AZIMUTH"])
        mbfits_dict["ELEVATIO"] = np.concatenate(datapar_selected_buffer["ELEVATIO"])
        
        #self.logger.debug('mbfits_dict[MJD] type: {}, shape: {}, data: {}'.format(type(mbfits_dict["MJD"]), mbfits_dict["MJD"].shape, mbfits_dict["MJD"]))
        
        npoints = len(mbfits_dict["MJD"])
        self.logger.debug("Data points in all subscans, all files (scans) flattened: {}".format(npoints))
        
        # Specify numpy datatype to store the result structured data for next processing
        # Since numpy arrays use contigous memory for performance, we must specify
        # the array size based on how much data was accumulated in the list
        dtype_datapar_flattened = np.dtype([
            ('MJD',			np.float64, (npoints,)), # MJD at integration midpoint in TIMESYS system
            ('LONGOFF',		np.float64, (npoints,)), # Longitude offset from source in user native frame 
            ('LATOFF',		np.float64, (npoints,)), # Latitude offset from source in user native frame
            ('AZIMUTH',		np.float64, (npoints,)), # Azimuth (derived)
            ('ELEVATIO',	np.float64, (npoints,)), # Elevation (derived)
            ])
        
        #self.logger.debug("dtype_datapar_flattened: {}".format(dtype_datapar_flattened))
        
        self.mbfits_data = np.array((mbfits_dict["MJD"],
                                     mbfits_dict["LONGOFF"],
                                     mbfits_dict["LATOFF"],
                                     mbfits_dict["AZIMUTH"],
                                     mbfits_dict["ELEVATIO"]), 
                                     dtype=dtype_datapar_flattened)
        
        #self.logger.debug("mbfits_dict: {}".format(mbfits_dict))
        #self.logger.debug("self.mbfits_data.dtype: {} {}".format(type(self.mbfits_data), self.mbfits_data.dtype))
        #self.logger.debug("self.mbfits_data['MJD']: {}".format(self.mbfits_data["MJD"]))
        #self.logger.debug("self.mbfits_data['LONGOFF']: {}".format(self.mbfits_data["LONGOFF"]))
        #self.logger.debug("self.mbfits_data['LATOFF']: {}".format(self.mbfits_data["LATOFF"]))
        #self.logger.debug("self.mbfits_data['AZIMUTH']: {}".format(self.mbfits_data["AZIMUTH"]))
        #self.logger.debug("self.mbfits_data['ELEVATIO']: {}".format(self.mbfits_data["ELEVATIO"]))
    
    def load_mbfits(self, filepath, shift_time):
        self.hdul = fits.open(filepath)
        # leave the file open becuase fits.open will not store the entire file in RAM if it is
        # too big.  Close file at the end of this script.
        # self.hdul.info()

        # Following the mbfits specification, each subscan will produce two HDU (header data unit)
        # items in the hdu list that is creaded by astropy.io.fits.
        # The HDU names are 'ARRAYDATA-MBFITS' and 'DATAPAR-MBFITS'
        # A file that contains more than 1 subscan will have more than 1 HDU that uses the same name.
        # but the index in the hdulist (generated by astropy) and the subscan number (in the DATAPAR header)
        # is unique.
        # Therefore, we must traverse the HDU list and generate new data structures with references to
        # the data in ARRAYDATA and DATAPAR of each subscan, but more convenient to access data by subscan.
        # Note these lists *must* have the same length and have associated data in the same sequence
        datapar_selected_buffer = {
            "MJD" : [],
            "LONGOFF" : [],
            "LATOFF" : [],
            "AZIMUTH" : [],
            "ELEVATIO" : [],
            }
        
        data = {
            "MJD" : None,
            "LONGOFF" : None,
            "LATOFF" : None,
            "AZIMUTH" : None,
            "ELEVATIO" : None,
            }

        arraydata_hdulist = []
        datapar_hdulist = []
        subscan_number_list = []
                    
        # Get more header information:
        self.scan_header_string = "{} {}: {} = ({:3f} deg, {:3f} deg)".format(
            self.hdul["SCAN-MBFITS"].header["OBJECT"],
            self.hdul["SCAN-MBFITS"].header["SCANMODE"],
            self.hdul["SCAN-MBFITS"].header["RADESYS"],
            self.hdul["SCAN-MBFITS"].header["BLONGOBJ"] or 0.0,
            self.hdul["SCAN-MBFITS"].header["BLATOBJ"] or 0.0,
        )
        
        self.logger.debug(self.scan_header_string)


        self.ra_icrs = self.hdul["SCAN-MBFITS"].header["BLONGOBJ"]
        self.dec_icrs = self.hdul["SCAN-MBFITS"].header["BLATOBJ"]
        self.logger.debug("RA: {}, DEC: {}".format(self.ra_icrs, self.dec_icrs))

        # Get integration time from the first integration of the first subscan.
        # INTEGTIM is recorded for all integrations / accumulations of data. In our operational
        # use case, it will not change during the same scan / .mbfits file, so use the first data
        self.integration_time = self.hdul['DATAPAR-MBFITS'].data['INTEGTIM'][0]
        self.logger.debug("Integration time: {} s".format(self.integration_time))

        # Choose the value of time to adjust.  Start with integration time for now.
        # TODO - make this more flexible if we need to adjust the time by another value
        self.time_offset_value = -1 * self.integration_time

        for hdu in self.hdul:
            self.logger.debug("found HDU name: {}".format(hdu.name))
            if hdu.name == "ARRAYDATA-MBFITS":
                self.logger.debug("name 'ARRAYDATA-MBFITS'. Append to arraydata_list")
                if hdu.data["MJD"][0] == 0.0:
                    self.logger.warning(
                        "Found timestamp MJD == 0.  Looks like dummy row. discard and continue"
                    )
                    continue

                if shift_time == True:
                    self.apply_time_offset_arraydata(hdu)                                    
                
                arraydata_hdulist.append(hdu)

                self.logger.debug("found subscan number: {}".format(hdu.header["SUBSNUM"]))
                subscan_number_list.append(hdu.header["SUBSNUM"])
                pass

            elif hdu.name == "DATAPAR-MBFITS":
                self.logger.debug("name is 'DATAPAR-MBFITS'. Append to datapar_list")
                if hdu.data["MJD"][0] == 0.0:
                    self.logger.warning(
                        "Found timestamp MJD == 0.  Looks like dummy row. discard and continue"
                    )
                    continue
                
                if shift_time == True:
                    self.apply_time_offset_datapar(hdu, 
                                                   self.hdul["MONITOR-ANT-GENERAL"], 
                                                   self.hdul["MONITOR-ANT-TRACKING"], 
                                                   self.hdul["MONITOR-ANT-AXIS"])
            
                datapar_hdulist.append(hdu)
                self.logger.debug("found subscan number: {}".format(hdu.header["SUBSNUM"]))
                if hdu.header["SUBSNUM"] != subscan_number_list[-1]:
                    raise IndexError(
                        "Subscan numbers out of sequence between ARRAYDATA and DATAPAR"
                    )

        self.logger.debug(
            "Found {} subscans.  Subscan ID numbers: {}".format(
                len(subscan_number_list), subscan_number_list
            )
        )

        if shift_time == True:
            if self.time_offset_value > 0:
                append_str = "_p{:d}".format(int(np.abs(1000*self.time_offset_value)))
            else:
                append_str = "_n{:d}".format(int(np.abs(1000*self.time_offset_value)))
            
            (dir, filename) = os.path.split(filepath)
            (basename, ext) = os.path.splitext(filename)
            self.logger.debug("dir {} filename {} basename {} ext {}".format(dir, filename, basename, ext))
            
            newdir = dir+"_time_offset"
            try:
                os.mkdir(newdir)
            except:
                self.logger.debug("Directory: {} alredy exists. continue".format(newdir))

            newpath =  os.path.join(newdir, basename + append_str + ext)
            self.logger.info("Write new file: {}".format(newpath))
            self.hdul.writeto(newpath, overwrite=True)

        # Traverse all DATAPAR from all subscans and prepare data in a convenient dictionary
        # of lists for each field
        for datapar_hdu in datapar_hdulist:
            self.logger.debug("subscan: {}, npoints: {}, start time: {}, end time: {}".format(
                datapar_hdu.header['SUBSNUM'],
                datapar_hdu.data["MJD"].shape,
                datapar_hdu.data["MJD"][0],
                datapar_hdu.data["MJD"][-1]))
            datapar_selected_buffer['MJD'].append(datapar_hdu.data['MJD'])
            datapar_selected_buffer['LONGOFF'].append(datapar_hdu.data['LONGOFF'])
            datapar_selected_buffer['LATOFF'].append(datapar_hdu.data['LATOFF'])
            datapar_selected_buffer['AZIMUTH'].append(datapar_hdu.data['AZIMUTH'])
            datapar_selected_buffer['ELEVATIO'].append(datapar_hdu.data['ELEVATIO'])
                
        #self.logger.debug("datapar_selected_buffer: {}".format(datapar_selected_buffer["MJD"]))
        
        # Concatenate list of arrays per file (1 array per subscan) and return 1 long array for all subscans
        data["MJD"] = np.concatenate(datapar_selected_buffer["MJD"])
        data["LONGOFF"] = np.concatenate(datapar_selected_buffer["LONGOFF"])
        data["LATOFF"] = np.concatenate(datapar_selected_buffer["LATOFF"])
        data["AZIMUTH"] = np.concatenate(datapar_selected_buffer["AZIMUTH"])
        data["ELEVATIO"] = np.concatenate(datapar_selected_buffer["ELEVATIO"])
       
        self.hdul.close()

        return(data)


    def apply_time_offset_arraydata(self, hdu):
        # Fix the timestamps (86400 seconds per day)
        hdu.data["MJD"] = hdu.data["MJD"] + self.time_offset_value / 86400.0


    def apply_time_offset_datapar(self, hdu_datapar, hdu_acu_general, hdu_acu_tracking, hdu_acu_axis):
        # Fix the timestamps (86400 seconds per day)
        hdu_datapar.data["MJD"] = hdu_datapar.data["MJD"] + self.time_offset_value / 86400.0

        # Interpolate ACU AZ, EL data onto the new spectrum timestamps to create
        # the new LONGOFF, LATOFF, AZIMUTH, ELEVATIO.  
        # Follow the same logic in PipelineMBfits.write_after_subscan
        
        # Copy some values from DataFormatAcu.py (keep this script stanalone -- not require import)
        AZ = 0
        EL = 1
        TR_SUN_TRACK_ACTIVE = 1 << 12
        TR_PROG_TRACK_ACTIVE = 1 << 13
        TR_TLE_TRACK_ACTIVE = 1 << 14
        TR_STAR_TRACK_ACTIVE = 1 << 15
        
        timestamps_spe = hdu_datapar.data["MJD"]
        timestamps_acu = hdu_acu_general.data["actual_time"]

        actual_position_az = hdu_acu_axis.data["actual_position"][:, AZ]
        actual_position_el = hdu_acu_axis.data["actual_position"][:, EL]

        # Find tracking center coordinate from selected tracking algorithm in monitor_ant_tracking["bit_status"]
        # The bit status exists for every ACU status message in the array.  
        # Assume it does not change during this subscan. So, we can use the first value at index [0] 
        if (hdu_acu_tracking.data["bit_status"][0] & TR_SUN_TRACK_ACTIVE) == TR_SUN_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_SUN_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = hdu_acu_tracking.data["sun_track_az"]
            selected_tracking_algorithm_position_el = hdu_acu_tracking.data["sun_track_el"]

        elif (hdu_acu_tracking.data["bit_status"][0] & TR_PROG_TRACK_ACTIVE) == TR_PROG_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_PROG_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = hdu_acu_tracking.data["prog_track_az"]
            selected_tracking_algorithm_position_el = hdu_acu_tracking.data["prog_track_el"]
        
        elif (hdu_acu_tracking.data["bit_status"][0] & TR_TLE_TRACK_ACTIVE) == TR_TLE_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_TLE_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = hdu_acu_tracking.data["tle_track_az"]
            selected_tracking_algorithm_position_el = hdu_acu_tracking.data["tle_track_el"]
        
        elif (hdu_acu_tracking.data["bit_status"][0] & TR_STAR_TRACK_ACTIVE) == TR_STAR_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_STAR_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = hdu_acu_tracking.data["star_track_az"]
            selected_tracking_algorithm_position_el = hdu_acu_tracking.data["star_track_el"]
        
        else:
            self.logger.warning("Cannot identify selected tracking algorithm.  Set tracking algorithm position = (0,0)")
            selected_tracking_algorithm_position_az = 0
            selected_tracking_algorithm_position_el = 0

        long_off = actual_position_az - selected_tracking_algorithm_position_az
        lat_off = actual_position_el - selected_tracking_algorithm_position_el

        interp_function_acu_az = interp1d(
            timestamps_acu, actual_position_az, kind="cubic", fill_value="extrapolate"
        )
        interp_function_acu_el = interp1d(
            timestamps_acu, actual_position_el, kind="cubic", fill_value="extrapolate"
        )
        interp_function_acu_long_off = interp1d(
            timestamps_acu, long_off, kind="cubic", fill_value="extrapolate"
        )
        interp_function_acu_lat_off = interp1d(
            timestamps_acu, lat_off, kind="cubic", fill_value="extrapolate"
        )

        az_at_timestamps_spectrometer = interp_function_acu_az(timestamps_spe)
        el_at_timestamps_spectrometer = interp_function_acu_el(timestamps_spe)
        long_off_at_timestamps_spectrometer = interp_function_acu_long_off(timestamps_spe)
        lat_off_at_timestamps_spectrometer = interp_function_acu_lat_off(timestamps_spe)

        # Log some data before the overwrite
        center_index = int(len(hdu_datapar.data["AZIMUTH"]) / 2)
        
        self.logger.debug("Before overwrite [center]: datapar AZIMUTH: {} deg, ELEVATIO: {} deg, LONGOFF: {} arcsec, LATOFF: {} arcsec".format(
            hdu_datapar.data["AZIMUTH"][center_index],
            hdu_datapar.data["ELEVATIO"][center_index],
            3600 * hdu_datapar.data["LONGOFF"][center_index],
            3600 * hdu_datapar.data["LATOFF"][center_index],  
        ))

        # Overwrite the data
        hdu_datapar.data["AZIMUTH"] = az_at_timestamps_spectrometer
        hdu_datapar.data["ELEVATIO"] = el_at_timestamps_spectrometer
        hdu_datapar.data["LATOFF"] = lat_off_at_timestamps_spectrometer
        
        # If the program offset table scan pattern is an AZ,EL pattern, scale the LONGOFF
        # values based on the GLS projection.
        # Reference

        """ Sinusoidal (GLS) projection for the LONGOFF and LATOFF columns in the DATAPAR-MBFITS table. This
        was done to be more compatible with the Gildas software that is used for subsequent spectral line data
        reductions. Many bolometer data reduction packages also inherently assume this kind of projection. The
        GLS projection is very similar though not identical to SFL. Originally, it was described in AIPS memo 46
        by E. Greisen. The difference is that the cosine is applied to the latitude rather than the delta in latitude:
        GLS projection: 
        x = (φ - φ0 ) cos θ
        y = (θ - θ0 ) 
        """
        self.logger.debug("NLNGTYPE: {}".format(hdu_datapar.header['NLNGTYPE']))

        if hdu_datapar.header['NLNGTYPE']=="ALON-GLS":
            self.logger.debug("program offset longitude is ALON-GLS (AZ,EL), scale LONGOFF for GLS projection.")
            hdu_datapar.data["LONGOFF"] = long_off_at_timestamps_spectrometer * np.cos(np.radians(hdu_datapar.data["ELEVATIO"]))
        else:
            self.logger.debug("program offset longitude is not ALON-GLS (AZ,EL). Do not scale LONGOFF.")
            hdu_datapar.data["LONGOFF"] = long_off_at_timestamps_spectrometer
        
        # Log some data after overwrite
        self.logger.debug("After overwrite [center]: datapar AZIMUTH: {} deg, ELEVATIO: {} deg, LONGOFF: {} arcsec, LATOFF: {} arcsec".format(
            hdu_datapar.data["AZIMUTH"][center_index],
            hdu_datapar.data["ELEVATIO"][center_index],
            3600 * hdu_datapar.data["LONGOFF"][center_index],
            3600 * hdu_datapar.data["LATOFF"][center_index],  
        ))



    def process_astropy(self):
        self.astropy_data = {"predicted_az" : None, "predicted_el" : None}

        self.location = EarthLocation(EarthLocation.from_geodetic(
            lon=99.216805 * u.deg,
            lat=18.864348 * u.deg,
            height=403.625 * u.m,
            ellipsoid="WGS84",
        ))

        self.logger.info("Site Location Latitude [deg]: {}".format(self.location.lat.value))
        self.logger.info("Site Location Longitude [deg]: {}".format(self.location.lon.value))
        self.logger.info("Site Location Height [m]: {}".format(self.location.height.value))


        frame_AltAz = AltAz(location=self.location, obstime=self.tt_acu)
        
        coord_icrs = ICRS(ra=self.ra_icrs * u.deg, dec=self.dec_icrs * u.deg)
        coord_altaz = coord_icrs.transform_to(frame_AltAz)
        self.astropy_data["predicted_az"] = coord_altaz.az.deg 
        self.astropy_data["predicted_el"] = coord_altaz.alt.deg

    def preview(self):
        app = QtGui.QApplication([])
        # NOTE: Default is dark background and CMY colors. Set white_background=True
        # if you want white background and RGB colors.
        white_background = False
        #white_background = True

        if white_background is True:
            pg.setConfigOption("background", "w")
            pg.setConfigOption("foreground", "k")

            # Use RGB color for white background
            color1 = "b"
            color2 = "r"
            color3 = "g"
            color4 = "m"
            color5 = "k"
        else:
            # Use CMY color for dark background
            color1 = "y"
            color2 = "m"
            color3 = "c"
            color4 = "b"
            color5 = "r"

        symbol1 = "o"
        symbol2= "+"
        symbol3 = "d"

        size1 = 18
        size2 = 12
        size3 = 6

        grid_opacity = 0.7

        ARCSEC_PER_DEG = 3600

        self.tt_spectrum = Time(self.mbfits_data['MJD'], format="mjd")
        self.tt_spectrum_unix = self.tt_spectrum.unix

        # Find time of maximum EL angle.  Draw a symbol on EL and Doppler graph
        el_argmax = np.argmax(self.acu_data['prog_track_el'])


        str_header_summary = "<font>{}<br>Max EL {:.2f} deg at {}</font> UTC<br>Integration Time {} s".format(self.scan_header_string, 
                                     self.acu_data['actual_position_el'][el_argmax], 
                                     self.tt_acu[el_argmax].isot,
                                     self.integration_time)
        
        self.logger.debug(str_header_summary)

        # -----------------------------------------------------------------------------
        # window 1. ACU time, prog track AZ, actual AZ, mbfits AZ, astropy AZ
        # -----------------------------------------------------------------------------
        glw_tracking_az = pg.GraphicsLayoutWidget(show=True)
        glw_tracking_az.resize(1600, 1200)
        glw_tracking_az.setWindowTitle("az_v_time")
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem(str_header_summary)

        glw_tracking_az.addItem(label_config_summary, 0, 0)

        pi_az_time = glw_tracking_az.addPlot(1, 0, title=None)
        pi_az_time.addLegend()
        pi_az_time.plot(
            self.tt_acu_unix,
            self.acu_data['prog_track_az'],
            name="ACU Prog Track AZ (command)",
            pen=color1,
            symbol=None,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_az_time.plot(
            self.tt_acu_unix,
            self.acu_data['actual_position_az'],
            name="ACU Actual Position AZ",
            pen=color2,
            symbol=None,
            symbolBrush=None,
            symbolPen=None,
            symbolSize=None,
        )
        pi_az_time.plot(
            self.tt_spectrum_unix,
            self.mbfits_data['AZIMUTH'],
            name="MBFITS DATAPAR AZIMUTH",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size2,
        )
        pi_az_time.plot(
            self.tt_acu_unix,
            self.astropy_data['predicted_az'],
            name="Astropy predict AZ",
            pen=color4,
            symbol=None,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size2,
        )
        ax_L = pg.AxisItem(orientation="left", text="Azimuth", units="deg")
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_az_time.showGrid(True, True, alpha=grid_opacity)
        

        apex_line = pg.InfiniteLine(pos=self.tt_acu_unix[el_argmax], 
                               angle=90, 
                               label="Max EL",
                               pen=color5)
        
        pi_az_time.addItem(apex_line)
        

        # -----------------------------------------------------------------------------
        # window 2. ACU time, prog track EL, actual EL, mbfits EL, astropy EL
        # -----------------------------------------------------------------------------
        glw_tracking_el = pg.GraphicsLayoutWidget(show=True)
        glw_tracking_el.resize(1600, 1200)
        glw_tracking_el.setWindowTitle("el_v_time")
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem(str_header_summary)

        glw_tracking_el.addItem(label_config_summary, 0, 0)

        pi_el_time = glw_tracking_el.addPlot(1, 0)
        pi_el_time.addLegend()
        pi_el_time.plot(
            self.tt_acu_unix,
            self.acu_data['prog_track_el'],
            name="ACU Prog Track EL (command)",
            pen=color1,
            symbol=None,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_el_time.plot(
            self.tt_acu_unix,
            self.acu_data['actual_position_el'],
            name="ACU Actual Position EL",
            pen=color2,
            symbol=None,
            symbolBrush=None,
            symbolPen=None,
            symbolSize=None,
        )
        pi_el_time.plot(
            self.tt_spectrum_unix,
            self.mbfits_data['ELEVATIO'],
            name="MBFITS DATAPAR ELEVATIO",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size2,
        )
        pi_el_time.plot(
            self.tt_acu_unix,
            self.astropy_data['predicted_el'],
            name="Astropy predict EL",
            pen=color4,
            symbol=None,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size2,
        )

        ax_L = pg.AxisItem(orientation="left", text="Elevation", units="deg")
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_el_time.showGrid(True, True, alpha=grid_opacity)

        apex_line = pg.InfiniteLine(pos=self.tt_acu_unix[el_argmax], 
                               angle=90, 
                               label="Max EL",
                               pen=color5)
        
        pi_el_time.addItem(apex_line)
        
        # -----------------------------------------------------------------------------
        # window 3. ACU time, ACU calculated LONGOFF. MBFITS LONGOFF
        # -----------------------------------------------------------------------------
        glw_longoff = pg.GraphicsLayoutWidget(show=True)
        glw_longoff.resize(1600, 1200)
        glw_longoff.setWindowTitle("longoff_v_time")
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem(str_header_summary)

        glw_longoff.addItem(label_config_summary, 0, 0)

        pi_longoff = glw_longoff.addPlot(1, 0)
        pi_longoff.addLegend()
        pi_longoff.plot(
            self.tt_acu_unix,
            ARCSEC_PER_DEG * self.acu_data['prog_offset_az'] * np.cos(np.radians(self.acu_data["actual_position_el"])),
            name="ACU Prog Offset AZ (command) * cos(actual pos EL)",
            pen=color1,
            symbol=None,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_longoff.plot(
            self.tt_acu_unix,
            ARCSEC_PER_DEG * (self.acu_data['actual_position_az']-self.acu_data["prog_track_az"]) * np.cos(np.radians(self.acu_data["actual_position_el"])),
            name="ACU LONGOFF (actual)",
            pen=color2,
            symbol=None,
            symbolBrush=None,
            symbolPen=None,
            symbolSize=None,
        )
        pi_longoff.plot(
            self.tt_spectrum_unix,
            ARCSEC_PER_DEG * self.mbfits_data['LONGOFF'],
            name="MBFITS DATAPAR LONGOFF",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size2,
        )

        ax_L = pg.AxisItem(orientation="left", text="LONGOFF", units="arcsec")
        ax_L.enableAutoSIPrefix(False)
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_longoff.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_longoff.showGrid(True, True, alpha=grid_opacity)
        

        apex_line = pg.InfiniteLine(pos=self.tt_acu_unix[el_argmax], 
                               angle=90, 
                               label="Max EL",
                               pen=color5)
        
        pi_longoff.addItem(apex_line)

        # -----------------------------------------------------------------------------
        # window 4. ACU time, ACU calculated LATOFF. MBFITS LONGOFF
        # -----------------------------------------------------------------------------     
        glw_latoff = pg.GraphicsLayoutWidget(show=True)
        glw_latoff.resize(1600, 1200)
        glw_latoff.setWindowTitle("latoff_v_time")
        pg.setConfigOptions(antialias=True)

        label_config_summary = pg.LabelItem(str_header_summary)

        glw_latoff.addItem(label_config_summary, 0, 0)

        pi_latoff = glw_latoff.addPlot(1, 0)
        pi_latoff.addLegend()
        pi_latoff.plot(
            self.tt_acu_unix,
            ARCSEC_PER_DEG * self.acu_data['prog_offset_el'],
            name="ACU Prog Offset EL (command)",
            pen=color1,
            symbol=None,
            symbolBrush=None,
            symbolPen=color1,
            symbolSize=size1,
        )
        pi_latoff.plot(
            self.tt_acu_unix,
            ARCSEC_PER_DEG * (self.acu_data['actual_position_el']-self.acu_data["prog_track_el"]),
            name="ACU LATOFF (actual)",
            pen=color2,
            symbol=None,
            symbolBrush=None,
            symbolPen=None,
            symbolSize=None,
        )
        pi_latoff.plot(
            self.tt_spectrum_unix,
            ARCSEC_PER_DEG * self.mbfits_data['LATOFF'],
            name="MBFITS DATAPAR LATOFF",
            pen=None,
            symbol=symbol1,
            symbolBrush=None,
            symbolPen=color3,
            symbolSize=size2,
        )

        ax_L = pg.AxisItem(orientation="left", text="LONGOFF", units="arcsec")
        ax_L.enableAutoSIPrefix(False)
        ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0, text="UTC")
        ax_L.showLabel(True)
        ax_B.showLabel(True)
        pi_latoff.setAxisItems(axisItems={"bottom": ax_B, "left": ax_L})
        pi_latoff.showGrid(True, True, alpha=grid_opacity)
        

        apex_line = pg.InfiniteLine(pos=self.tt_acu_unix[el_argmax], 
                               angle=90, 
                               label="Max EL",
                               pen=color5)
        
        pi_latoff.addItem(apex_line)
        # ------------------------------------------------------------------------------
        # Start the Qt event loop and block this script until user closes the GUI window
        app.exec_()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="python process_pointing_gap [ACU csv filename] [list of .mbfits filenames]"
    )
    parser.add_argument(
        "-a",
        "--acu_csv_filename",
        type=str,
        help="ACU .csv file",
    )

    parser.add_argument(
        "-m",
        "--mbfits_dir",
        type=str,
        help="directory that includes all .mbfits files to analyze",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        default=False,
        action="store_true",
        help="Enable verbose logging (python logging level DEBUG. Default is INFO)",
    )

    parser.add_argument(
        "-s",
        "--shift-int-time",
        dest="shift_time",
        default=False,
        action="store_true",
        help="Shift timestamp of all spectrum packets and re-interpolate the angles.  Value fo shift = 1 integration time (for now)",
    )

    parser.add_argument(
        "-g",
        "--gui",
        dest="show_gui",
        default=False,
        action="store_true",
        help="Show GUI.  Requires ACU .csv file specified by '-a' and at least 1 .mbfits file in the directory specified by '-m'",
    )

    # Read command line arguments into argparse structure
    args = parser.parse_args()
    if args.verbose == True:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    logger = logging.getLogger(__name__)
    configure_logger(logger, loglevel)

    compare_tool = AcuCompareTool(loglevel)
    
    try:
        compare_tool.process_acu_csv(args.acu_csv_filename)
    except (FileNotFoundError, OSError) as e:
        logger.error(
            "{} not found in PATH. Current directory is {}".format(
                args.acu_csv_filename, os.getcwd()
            )
        )
    except (ValueError) as e:
        logger.error("ACU file not specified.  Do not load ACU .csv data")
    
    try:
        compare_tool.process_mbfits(args.mbfits_dir, args.shift_time)
    except (FileNotFoundError, OSError) as e:
        logger.error(
            "{} not found in PATH. Current directory is {}".format(
                args.mbfits_dir, os.getcwd()
            )
        )
        raise
    
    if args.show_gui == True:
        compare_tool.process_astropy()
        compare_tool.preview()
