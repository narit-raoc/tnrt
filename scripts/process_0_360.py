# -*- coding: utf-8 -*-
import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore

from astropy.io import fits
from astropy import units
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.time import TimeDelta
from astropy.utils import iers

from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

from coordinate_utility import icrs_altaz_astropy
from coordinate_utility import icrs_altaz_iau
from coordinate_utility import reproduce_acu

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
# white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color_acu = "k"
    color_iau_no_polar_motion = "b"
    color_iau_polar_motion = "m"
    color_astropy = "r"
    color_manual = "g"
else:
    # Use CMY color for dark background
    color_acu = "b"
    color_iau_no_polar_motion = "y"
    color_iau_polar_motion = "b"
    color_astropy = "c"
    color_manual = "m"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# Constant
MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24

# Read important header information and add units
ra_catalog = Longitude(hdul["SCAN-MBFITS"].header["BLONGOBJ"], unit=units.deg)
dec_catalog = Latitude(hdul["SCAN-MBFITS"].header["BLATOBJ"], unit=units.deg)
print("SCAN Header: RA ICRS: {}".format(ra_catalog))
print("SCAN Header: DEC ICRS: {}".format(dec_catalog))

# Read data from SCAN header
dut1_scan_header = TimeDelta(
    hdul["SCAN-MBFITS"].header["UTC2UT1"] * units.s, format="sec"
)
print("SCAN Header: DUT1 (UT1-UTC) [s]: {}".format(dut1_scan_header))

loc_scan_header = EarthLocation(
    EarthLocation.from_geodetic(
        lon=hdul["SCAN-MBFITS"].header["SITELONG"] * units.deg,
        lat=hdul["SCAN-MBFITS"].header["SITELAT"] * units.deg,
        height=hdul["SCAN-MBFITS"].header["SITEELEV"] * units.m,
        ellipsoid="WGS84",
    )
)

print("SCAN Header: Site Location Longitude: {}".format(loc_scan_header.lon))
print("SCAN Header: Site Location Latitude: {}".format(loc_scan_header.lat))
print("SCAN Header: Site Location Height: {}".format(loc_scan_header.height))

# Select in index to use in this script and print in debug messages.
# In the case of this test the values are constant throughout the entire array.
# However, in a very long observation, the value of dut1, ra_geocentric_apparent,
# and dec_geocentric_apparent can change by a small amount.
debug_index = 0

loc_acu = EarthLocation(
    EarthLocation.from_geodetic(
        lon=hdul["MONITOR-ANT-TRACKING"].data["antenna_longitude"][debug_index]
        * units.deg,
        lat=hdul["MONITOR-ANT-TRACKING"].data["antenna_latitude"][debug_index]
        * units.deg,
        height=hdul["MONITOR-ANT-TRACKING"].data["antenna_altitude"][debug_index]
        * units.m,
        ellipsoid="WGS84",
    )
)

dut1_acu = TimeDelta(
    hdul["MONITOR-ANT-TRACKING"].data["dut1"][debug_index] * units.ms, format="sec"
)

GST0hUT1_ms = hdul["MONITOR-ANT-TRACKING"].data["gst0hut"][debug_index]
GST0hUT1 = Longitude((GST0hUT1_ms / MILLISECONDS_PER_DAY) * 24.0, unit=units.hourangle)

ra_acu_tracktable = Longitude(
    hdul["MONITOR-ANT-TRACKING-OBJECT"].data["pt_table11"][debug_index], unit=units.deg
)
dec_acu_tracktable = Latitude(
    hdul["MONITOR-ANT-TRACKING-OBJECT"].data["pt_table12"][debug_index], unit=units.deg
)

print("ACU: Site Location Longitude: {}".format(loc_acu.lon))
print("ACU: Site Location Latitude: {}".format(loc_acu.lat))
print("ACU: Site Location Height: {}".format(loc_acu.height))
print("ACU: DUT1 (UT1-UTC) : {}".format(dut1_acu))
print("ACU: GST0hUT1: {} ms".format(GST0hUT1_ms))
print("ACU: GST0hUT1: {}".format(GST0hUT1))
print("ACU: RA (system?): {}".format(ra_acu_tracktable))
print("ACU: DEC (system?): {}".format(dec_acu_tracktable))

# -----------------------------------------------------------------------------
# Select data while tracking on the object
# -----------------------------------------------------------------------------
# Find the section of the data file where the system is tracking (not slew to target).
# Use only this data for the remaining graphs.
AZ = 0
EL = 1
az_error = 3600 * (
    hdul["MONITOR-ANT-AXIS"].data["desired_position"][:, AZ]
    - hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, AZ]
)
el_error = 3600 * (
    hdul["MONITOR-ANT-AXIS"].data["desired_position"][:, EL]
    - hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL]
)
az_error_onsky = az_error * np.cos(
    hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL] * np.pi / 180.0
)
error_abs = np.sqrt(az_error_onsky**2 + el_error**2)
tolerance = 3600 # show all data that is within 10 degrees of tracking
#tolerance =  10 # filter the data down to display only when tracking within 10 arcsec
condition1 = np.array(error_abs < tolerance, dtype=bool)

# Second condition is when Program Track is "running" state
PTS_RUNNING = 1 << 4  # From ICD document.  bit 4 indicates program track running.
condition2 = np.array(
    (hdul["MONITOR-ANT-TRACKING"].data["prog_track_bs"] & PTS_RUNNING) > 0,
    dtype=bool,
)

index_to_keep = np.where(condition1 & condition2)

# processing_time_offset = 0.08783833660129127 * units.s
processing_time_offset = 0 * units.s

# Select the data
time_acu = (
    Time(
        hdul["MONITOR-ANT-GENERAL"].data["actual_time"][index_to_keep],
        format="mjd",
        scale="utc",
        location=loc_acu,
    )
    + processing_time_offset
)

starttime_ut1_integer_day = int(time_acu[0].ut1.mjd)
startday_0hUT1 = Time(starttime_ut1_integer_day, format="mjd", scale="ut1")
GST0hUT1_apy = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")
print("Sanity Check: Astropy: GST0hUT1: {}".format(GST0hUT1_apy))

# Get polar motion from IERS at the start time of tracking
iers_table = iers.IERS_Auto.open()
(pm_x, pm_y) = iers_table.pm_xy(Time.now().jd)  # arcsec
print("IERS: Polar motion x:{} arcsec".format(pm_x.value))
print("IERS: Polar motion y:{} arcsec".format(pm_y.value))
# xp = 8.369384499848348e-07
# yp = 2.348998103817482e-06

# Transform the coorinates using 4 different methods
# print("Astropy transform ICRS -> Observed AltAz ...")
# (az_astropy, alt_astropy) = icrs_altaz_astropy(
#     ra_catalog, dec_catalog, loc_scan_header, time_acu
# )
# print("Done")

# print("IAU SOFA transform ICRS -> Observed AltAz (atco13) no polar motion ...")
# (az_iau_no_polar_motion, alt_iau_no_polar_motion) = icrs_altaz_iau(
#     ra_catalog,
#     dec_catalog,
#     loc_scan_header,
#     time_acu,
#     dut1_scan_header,
#     units.quantity.Quantity(0),
#     units.quantity.Quantity(0),
# )
# print("Done")

# print("IAU SOFA transform ICRS -> Observed AltAz (atco13) including polar motion ...")
# (az_iau_polar_motion, alt_iau_polar_motion) = icrs_altaz_iau(
#     ra_catalog,
#     dec_catalog,
#     loc_scan_header,
#     time_acu,
#     dut1_scan_header,
#     pm_x,
#     pm_y,
# )
# print("Done")

print("RA / DEC Geocentric Apparent -> Observed AltAz (reproduce ACU algorithm)")
(manual_az, manual_el) = reproduce_acu(
    ra_acu_tracktable,
    dec_acu_tracktable,
    loc_acu,
    time_acu,
    dut1_acu,
    GST0hUT1,
    time_acu[0].ut1.mjd,
)

# Read the AZ and EL from ACU status message.
# Assume that all pointing corrections are 0.
# If any of the pointing corrections are non-zero, the value we read from the
# encoder will not align with the Astropy or IAU result, but actual_position should be OK.
acu_az = hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, 0][index_to_keep] - hdul['MONITOR-ANT-TRACKING'].data['prog_offset_az'][index_to_keep]
acu_el = hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, 1][index_to_keep] - hdul['MONITOR-ANT-TRACKING'].data['prog_offset_el'][index_to_keep]
acu_program_track_az = hdul['MONITOR-ANT-TRACKING'].data['prog_track_az'][index_to_keep]
acu_program_track_el = hdul['MONITOR-ANT-TRACKING'].data['prog_track_el'][index_to_keep]

# Time axis data
time_elapsed = (time_acu - time_acu[0]).sec
time_unix = time_acu.unix

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "File {}".format(os.path.basename(args.filename))
grid_opacity = 0.7
app = QtGui.QApplication([])

# -----------------------------------------------------------------------------
# Graph window 1 - Absolute Values
# -----------------------------------------------------------------------------
glw_time = pg.GraphicsLayoutWidget(show=True)
glw_time.resize(1024, 768)
glw_time.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Plot AZ vs Time
pi_az_time = glw_time.addPlot(0, 0, title=str_title)
pi_az_time.addLegend()
pi_az_time.plot(time_unix, acu_az, pen=None, symbol="o", symbolBrush=None, symbolPen="y", name="actual_position - program_offset")
pi_az_time.plot(time_unix, manual_az, pen=None, symbol="o", symbolBrush=None, symbolPen="c", name="offline calc tracking")
pi_az_time.plot(time_unix, acu_program_track_az, pen=None, symbol="o", symbolBrush=None, symbolPen="m", name="acu tracking cmd")
ax_L = pg.AxisItem(orientation="left", text="AZ", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot EL vs Time
pi_el_time = glw_time.addPlot(1, 0)
pi_el_time.addLegend()
pi_el_time.plot(time_unix, acu_az, pen=None, symbol="o", symbolBrush=None, symbolPen="y", name="actual_position - program_offset")
pi_el_time.plot(time_unix, manual_az, pen=None, symbol="o", symbolBrush=None, symbolPen="c", name="offline calc tracking")
pi_el_time.plot(time_unix, acu_program_track_az, pen=None, symbol="o", symbolBrush=None, symbolPen="m", name="acu tracking cmd")
ax_L = pg.AxisItem(orientation="left", text="EL", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_el_time.showGrid(True, True, alpha=grid_opacity)

# -----------------------------------------------------------------------------
# Graph window 2 - Differences
# -----------------------------------------------------------------------------
d_acu_manual_az = 3600 * (acu_az - manual_az)
d_acu_manual_el = 3600 * (acu_el - manual_el)

glw_diff = pg.GraphicsLayoutWidget(show=True)
glw_diff.resize(1024, 768)
glw_diff.setWindowTitle("ACU Tracking Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Plot diff AZ vs Time
pi_d_az_time = glw_diff.addPlot(0, 0, title=str_title)
pi_d_az_time.addLegend()
pi_d_az_time.plot(time_unix, d_acu_manual_az, pen=None, symbol="o", symbolBrush=None, symbolPen="y", name="difference")
ax_L = pg.AxisItem(orientation="left", text="AZ", units="arcsec")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_d_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_d_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot diff EL vs Time
pi_d_el_time = glw_diff.addPlot(1, 0)
pi_d_el_time.addLegend()
pi_d_el_time.plot(time_unix, d_acu_manual_el, pen=None, symbol="o", symbolBrush=None, symbolPen="y", name="difference")
ax_L = pg.AxisItem(orientation="left", text="EL", units="arcsec")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_d_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_d_el_time.showGrid(True, True, alpha=grid_opacity)

# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()
