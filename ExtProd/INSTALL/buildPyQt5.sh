#! /bin/bash
#*******************************************************************************
# E.S.O. - ALMA project
#
# "@(#) $Id$"
#
# who        when        what
# --------   ----------  ----------------------------------------------
# agrimstrup 2007-07-10  created
#

#************************************************************************
#   NAME
#
#   SYNOPSIS
#
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS
#
#------------------------------------------------------------------------
#

#
# Install Python Modules
#
#set -x
#export PYTHON_ROOT=/alma/$ALMASW_RELEASE/Python

# Load functions
. standardUtilities
#
# Fetch operating system and release version
#
os_discovery

LOG=buildPyQt5.log
CWD=`pwd`
#
exec > $LOG 2>&1

date

if [ ! -d $PYTHON_ROOT ]
then
  echo "$PYTHON_ROOT not found, cannot continue..."
  exit 1
fi

echo installing Python Bindings for Qt5

echo $SEPARATOR
if [ ${OS} = "LINUX" ] 
then
	echo "Installing on $DISTRO $OS version $REL"
else
	echo "Installing on $OS version $REL"
fi

cd ../PRODUCTS

# Set python version
echo "Select python version 3.6.9"
pyenv global 3.6.9 || { echo "FAILED: Setting the global Python version to 3.6.9" && exit 1; }

# Unzip the file
tar -xzf PyQt5-5.15.2.dev2010041344.tar.gz || { echo "FAILED: tar -xzf PyQt5-5.15.2.dev2010041344.tar.gz" && exit 1; }

# Run the installer program.  Depends on apt packages qt5-default, qt5-qmake, qtbase5-dev
cd PyQt5-5.15.2.dev2010041344 || { echo "FAILED: cd PyQt5-5.15.2.dev2010041344" && exit 1; }
sip-install --confirm-license --verbose || { echo "FAILED: sip-install" && exit 1; }

# Test
echo "Test import PyQt5 in python3"
python -c "from PyQt5 import QtCore, QtGui" || { echo "FAILED: from PyQt5 import QtCore, QtGui" && exit 1; }

cd $CWD
result=$(grep FAILED ${LOG}|wc -l)
if [ $result -gt 0 ]
then
    echo "PyQt5 fail."
    date
    exit 1
else
    echo "PyQt5 installation done."
fi
