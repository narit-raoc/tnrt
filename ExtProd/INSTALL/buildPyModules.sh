#! /bin/bash
#*******************************************************************************
# E.S.O. - ALMA project
#
# "@(#) $Id$"
#
# who        when        what
# --------   ----------  ----------------------------------------------
# agrimstrup 2007-07-10  created
#

#************************************************************************
#   NAME
#
#   SYNOPSIS
#
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS
#
#------------------------------------------------------------------------
#

#
# Install Python Modules
#
#set -x
#export PYTHON_ROOT=/alma/$ALMASW_RELEASE/Python

# Load functions
. standardUtilities
#
# Fetch operating system and release version
#
os_discovery

LOG=buildPyModules.log
CWD=`pwd`
#
exec > $LOG 2>&1

date

if [ ! -d $PYTHON_ROOT ]
then
  echo "$PYTHON_ROOT not found, cannot continue..."
  exit 1
fi

echo installing Python Modules

echo $SEPARATOR
if [ ${OS} = "LINUX" ] 
then
	echo "Installing on $DISTRO $OS version $REL"
else
	echo "Installing on $OS version $REL"
fi

#
# PyEnv
#
# Notes: See README for help
#
cd ../PRODUCTS

eval "$(pyenv init -)"
# use command "python -m pip .... to guarantee that we are using the version of python that will install to the ALMA python directory,
# not the default OS python directory if using the wrapper "pip.install"

# Try to install python modules for ACS-2020APR (using python 3.6.9)
pyenv global 3.6.9
return_code_369=$?
if [ $return_code_369 -eq 0 ]; then
  echo "Python 3.6.9 selected.  pip install modules"
  python -m pip install --force-reinstall -r requirements_py3.6.9.txt || { echo "Failed: Installing Python 3.6.9 requirements"; }
else
  echo "Python 3.6.9 not found.  Do not install modules"  
fi

# Try to install python modules for ACS-2022OCT (using python 3.8.6)
pyenv global 3.8.6
return_code_386=$?
if [ $return_code_386 -eq 0 ]; then
  echo "Python 3.8.6 selected.  pip install modules"
  python -m pip install --force-reinstall -r requirements_py3.8.6.txt || { echo "Failed: Installing Python 3.8.6 requirements"; }
else 
  echo "Python 3.8.6 not found.  Do not install modules"
fi

if [ $(($return_code_369 + $return_code_386)) -eq 2 ]; then
  echo "Failed: Acceptable python version (3.6.9, 3.8.6) not found"
else
  echo "Found at least one python version in (3.6.9, 3.8.6)"
fi

cd $CWD
result=$(grep Failed ${LOG}|wc -l)
if [ $result -gt 0 ]
then
    echo "Python Module installation fail."
    date
    exit 1
else
    echo "Python Module installation done."
fi
