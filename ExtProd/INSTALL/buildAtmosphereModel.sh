#! /bin/bash
#*******************************************************************************
# E.S.O. - ALMA project
#
# "@(#) $Id: buildPython,v 1.29 2013/03/01 11:37:54 eallaert Exp $"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# ssarris	01/2021		new script

# This script installs 2 programs to the runtime path.  am-10.0 is compiled
# from source then installed to the runtime path.  
# "atm" (pardo,cernicharo,serabyn) is only copied from git LFS source location
# to the runtime install path.

# Load functions
. standardUtilities
#
# Fetch operating system and release version
#
os_discovery

NUM_CPU=`grep -c processor  /proc/cpuinfo`


LOG=buildAtmosphereModel.log
CWD=`pwd`
#
exec > $LOG 2>&1

date

echo "Installing for $DISTRO $REL"

# Navigate to directory that contains the zip file
cd ../PRODUCTS

PRODUCTS_DIR=`pwd`

echo "Directory to read from source tree PRODUCTS_DIR=$PRODUCTS_DIR"
echo "Directory to install for runtime EXTPRODROOT=$EXTPRODROOT"

if [ "$EXTPRODROOT" == "" ]
then 
    echo ""
    echo "ERROR: EXTPRODROOT must be set to the install path"
    echo ""
    exit 1
fi

# Unzip the file
tar -xzf am-10.0.tgz

# CD to the unzipped directory.  If CD fails, the build script will show FAILED and
# immediately exit
cd am-10.0/src || { echo "FAILED: cd am-10.0/src" && exit 1; }

# Create the extprodroot directory if it doesn't exist.
mkdir -p $EXTPRODROOT/bin || { echo "FAILED: mkdir -p $HOME/extprodroot/bin" && exit 1; }

# Compile the package.
make am || { echo "FAILED: make am" && exit 1; }

# Copy the am-10.0 binary to install directory for runtime
echo "cp am $EXTPRODROOT/bin"
cp am $EXTPRODROOT/bin || { echo "FAILED: cp am $EXTPRODROOT/bin" && exit 1; }

# Copy the atm-pardo binary to the install directory for runtime
cd $PRODUCTS_DIR || { echo "FAILED: cd $PRODUCTS_DIR" && exit 1; }
echo "atm $EXTPRODROOT/bin"
cp $PRODUCTS_DIR/atm $EXTPRODROOT/bin || { echo "FAILED: cp $PRODUCTS_DIR/atm $EXTPRODROOT/bin" && exit 1; }

cd $CWD
result=$(grep FAILED ${LOG}|wc -l)
if [ $result -gt 0 ]
then
    echo "FAILED"
    date
    exit 1
else
    echo "Installation done."
fi
