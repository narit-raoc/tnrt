#! /bin/bash
#*******************************************************************************
# E.S.O. - ALMA project
#
# "@(#) $Id: buildPython,v 1.29 2013/03/01 11:37:54 eallaert Exp $"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# psivera   2002-08-21  created
# sturolla  2005-10-09  ported to bourne shell + add external subroutin for OS checking


# Load functions
. standardUtilities
#
# Fetch operating system and release version
#
os_discovery

NUM_CPU=`grep -c processor  /proc/cpuinfo`


LOG=buildGildas.log
CWD=`pwd`
#
exec > $LOG 2>&1

date

export GAG_SEARCH_PATH="/usr/lib:/usr/lib/x86_64-linux-gnu"

echo "Installing for $DISTRO $REL"

# Navigate to directory that contains the zip file
cd ../PRODUCTS

PRODUCTS_DIR=`pwd`

echo $PRODUCTS_DIR

# Unzip the file
tar -xf gildas-src-oct20a.tar.xz

# CD to the unzipped directory.  If CD fails, the build script will show FAILED and
# immediately exit
cd gildas-src-oct20a || { echo "FAILED: cd gildas-src-oct20a" && exit 1; }

# Prepare the environment for build
source admin/gildas-env.sh

# Create the extprodroot directory if it doesn't exist.
mkdir -p $HOME/extprodroot

# Set the environment variable for installation directory to be outside our source tree
export gagexedir=$HOME/extprodroot/gildas-exe-oct20a

# Compile the package.  Sometimes it fails if we use the parameter -j NUM_CPU 
# to parallel process with maximum CPU.  For example on this 8-core dell notebook,
# compiling with -j 8 : gfortran fails while building deconv.o.  
# If compile fails using parallel processing, remove the "-j 4" parameter.  
# It will be slow, but more reliable.
make

# Install the compiled code to the directory defined above in gagexedir
make install

# ![need to add more] when Installation succeeded!
# bash users: You should add in your ~/.bash_profile the following lines
# 	export GAG_ROOT_DIR=$HOME/extprodroot/gildas-exe-oct20a
# 	export GAG_EXEC_SYSTEM=x86_64-ubuntu20.04-gfortran
# 	source $GAG_ROOT_DIR/etc/bash_profile

# You should then exit this xterm and open a new one to reset your path.
# GILDAS should be ready for use (e.g. type greg @ gag_demo:demo).

cd $CWD
result=$(grep FAILED ${LOG}|wc -l)
if [ $result -gt 0 ]
then
    echo "Gildas fail."
    date
    exit 1
else
    echo "Gildas installation done."
fi
