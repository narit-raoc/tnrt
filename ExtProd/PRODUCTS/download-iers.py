from astropy.utils.data import clear_download_cache
from astropy.time import Time
# Clear the old cache data
clear_download_cache

# Call a function that uses new IERS data
t = Time.now().ut1
print("time now UT1 = %s" % t)
