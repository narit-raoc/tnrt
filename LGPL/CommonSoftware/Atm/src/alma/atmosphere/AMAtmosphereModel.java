/*********************************************************************
* ARIES21 - Antena Radiomilimétrica Espanola Siglo XXI
*
* Copyright (C) 2004
* Observatorio Astronómico Nacional, Spain
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Library General Public License as published by the Free
* Software Foundation; either version 2 of the License, or (at your option) any
* later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
* details.
*
* You should have received a copy of the GNU Library General Public License
* along with this library; if not, write to the Free Software Foundation, Inc.,
* 675 Massachusetts Ave, Cambridge, MA 02139, USA. 
*
* who          when        what
* --------     ----------  ----------------------------------------------
* T.A.        dec-08   First implementation
* T.A.        oct-09   Changes in units of input parameters
*/

package alma.atmosphere;

import java.io.*;
import java.net.URI;
import java.util.StringTokenizer;
import java.util.Scanner;

import alma.AtmError.*;
import alma.acs.component.ComponentImplBase;

/**
 * A binding to the AM atmospheric model adapted to Yebes Astronomical
 * Observatory. The AM model is available at http://www.cfa.harvard.edu/~spaine/am.
 * @author T. Alonso Albi - OAN (Spain)
 * @version 1.0
 */
public class AMAtmosphereModel{
	
	/**
	 * The default path to the local am program executable.  
	 * Installed at this path by AtmosphericModel/am-10.0/src/Makefile
	 */
	public static String AM_PROGRAM_PATH = System.getProperty("user.home")+"/extprodroot/bin/";
	/**
	 * The name of the am program executable.
	 */
	public static String AM_PROGRAM_EXECUTABLE = "am";
	/**
	 * The path input file for the am program. The input file will be created
	 * from the contents of am_in_generic.txt with text replacements.
	 */
	public static String AM_INPUT_PATH = System.getProperty("user.home")+"/introot/";
	/**
	 * The name of the input file for the am program. The input file will be created
	 * from the contents of {@link #GENERIC_MODEL}.
	 */
	public static String AM_INPUT_FILE = "am_in.txt";

	
	/**
	 * path to generic input model file.  Placeholder values will be replaced with 
	 * meaningful values.
	 */
	public static String AM_GENERIC_PATH = System.getProperty("user.home")+"/introot/";

	/**
	 * The name of the generic input model file.  Placeholder values with '@' character
	 * will be replaced by function generateInputFile()
	 */
	public static String AM_GENERIC_FILE = "am_in_generic.txt";
	
	/**
	 * Ambient temperature in K.
	 */
	public double ambientTemperature;
	/**
	 * Ambient pressure in mbar. Note 1 Pa = 1 N/m^2, and 10^5 Pa = 1 bar.
	 */
	public double ambientPressure;
	/**
	 * Relative humidity from 0 to 1.
	 */
	public double relativeHumidity;
	/**
	 * The output from the model.
	 */
	private String[] output;
	
	/**
	 * Constructor for a model instance.
	 * @param p Pressure.
	 * @param t Temperature.
	 * @param h Humidity.
	 */
	public AMAtmosphereModel(double p, double t, double h)
	{
		this.ambientPressure = p;
		this.ambientTemperature = t;
		this.relativeHumidity = h;
	}
	
	/**
	 * The amount of h2o in the generic model.
	 */
	public static final double MODEL_H2O_MM = 19.25;
		
	/**
	 * Returns the H2O scale parameter for the input model.
	 * @return H2O scale factor.
	 */
	public double getH2OScale(double hscale)
	{
		double t = this.ambientTemperature;
		double h = this.relativeHumidity;
		// See http://en.wikipedia.org/wiki/Goff-Gratch_equation
/*			double ts = 373.15;
			double logp = -7.90298 * (ts / t - 1.0) + 
			5.02808 * Math.log10(ts / t) - 1.3816E-7 * (Math.pow(10.0, 11.344 * (1.0 - t / ts)) - 1.0) + 
			8.1328E-3 * (Math.pow(10.0, -3.49149 * (ts / t - 1.0)) - 1.0) + 
			Math.log10(1013.246);
		double p = Math.pow(10.0, logp) * 10.19716213; // 0.75 => p from mb (hPa) to mm Hg, 10.2 => to mm of H2O
*/
		// updated: http://en.wikipedia.org/wiki/Arden_Buck_Equation
		// t = t - 273.15;
		// double p = 6.1121 * Math.exp((18.678 - t / 234.5) * t / (257.14 + t)) * 10.19716213;
		// double mmH2O = this.relativeHumidity * p;


		// Pablo de Vicente. 18/8/2010.
		// La presion está mal. Nueva manera de hacerlo:

		// Height scale: 2500 m. Up to May -28 - 2012
		// double hscale = 2500;
		// double hscale = 1400;
		// Mass of water molecule
		// double mh2o = 1.66053886e-27*18;
		// Boltzmann Constant
		// double kb = 1.3806503e-23; 

		// mh2o/kb
		double mh2o_kb = 0.0021649000822293665;

		// From Pere Planesas (probably I (PdV) got it from one of his programs). 
		// It gives exactly the same values as Graff approximation
		double ph2o = 100*(6.015*this.relativeHumidity/100)*Math.pow((t/273),-5.31)*Math.exp(25.22*(t-273)/t);
		// double mmH2O = mh2o*ph2o*hscale/(kb*t);
		double mmH2O = 100*mh2o_kb*ph2o*hscale/t;
		// System.out.println("ph2o="+ph2o+"mmH2O="+mmH2O+", AMAtmosphereModel.MODEL_H2O_MMT="+AMAtmosphereModel.MODEL_H2O_MM);
		
		return mmH2O/AMAtmosphereModel.MODEL_H2O_MM;
	}
	
	/**
	 * Returns the input file for the am program.
	 * @param fmin Minimum frequency in GHz.
	 * @param fmax Maximum frequency in GHz.
	 * @param n The number of frequencies to calculate, at least 1.
	 * @return The input file.
	 */
	public void generateInputFile(double fmin, double fmax, int n, double hscale)
	{
		Scanner sc = null;
		BufferedWriter file_out = null;

		String line_generic = "";
		String line  = "";
		String strLine = "";

		try {
			System.out.println("Opening file to read generic model: " + AM_GENERIC_PATH + AM_GENERIC_FILE);
			File f = new File(AM_GENERIC_PATH + AM_GENERIC_FILE);
			sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Opening file to write model input: " + AM_INPUT_PATH + AM_INPUT_FILE);
						file_out = new BufferedWriter(new FileWriter(new File(AM_INPUT_PATH + AM_INPUT_FILE)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	

		double df = 1000.0 * (fmax - fmin) / (double) n;
			if(!sc.hasNextLine()){
        		System.out.println("file has no first line");
        	}

        while(sc.hasNextLine())
        {
        	line = sc.nextLine();
        	
        	line = line.replaceAll("@DF", String.valueOf(df));
        	line = line.replaceAll("@HUMIDITY", String.valueOf(this.relativeHumidity));
        	line = line.replaceAll("@PRESSURE", String.valueOf(this.ambientPressure));
        	line = line.replaceAll("@TEMPERATURE", String.valueOf(this.ambientTemperature));
        	line = line.replaceAll("@H2OMM", String.valueOf(this.getH2OScale(hscale)));
  
        	try{
        		file_out.write(line+"\n");
        	} catch(IOException e){
        		e.printStackTrace();
        	}
        }

        try {
			file_out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Executes the model.
	 * @param fmin Minimum frequency in GHz.
	 * @param fmax Maximum frequency in GHz.
	 * @param elev Elevation angle in degrees.
	 * @throws cannotExecuteAMEx If AM cannot be executed.
	 * @throws cannotWriteAMInputFileEx If the input file cannot be written.
	 */
	public void execute(double fmin, double fmax, double elev, int nf, double hscale)
	throws cannotExecuteAMEx, cannotWriteAMInputFileEx {
		// System.out.println("Hola AM Model: P="+ambientPressure+", T="+ambientTemperature+", h="+relativeHumidity+", freq="+fmin+","+fmax+", elev="+elev);

		String filepath = AMAtmosphereModel.AM_INPUT_PATH + AMAtmosphereModel.AM_INPUT_FILE;
		System.err.println("path to write input file: "+filepath);
		this.generateInputFile(fmin, fmax, nf, hscale);
				
		String command = AMAtmosphereModel.AM_PROGRAM_PATH + AMAtmosphereModel.AM_PROGRAM_EXECUTABLE;
		command += " "+filepath+" " +fmin+" "+fmax+" "+(90.0-elev);
		
		System.err.println("command to run AM: "+command);
		try {
			Process p = executeSystemCommand(command);
		
			this.output = toStringArray(getConsoleOutputFromProcess(p),
				getLineSeparator());
		} catch (Exception exc) {
			throw new cannotExecuteAMEx();
		}
	}
	
	/**
	 * ID constant to retrieve the column frequency in GHz.
	 */
	public static final int COLUMN_FREQUENCY = 1;
	/**
	 * ID constant to retrieve the column opacity in neper.
	 */
	public static final int COLUMN_OPACITY = 2;
	/**
	 * ID constant to retrieve the column brightness temperature in K.
	 */
	public static final int COLUMN_BRIGHTNESS_TEMPERATURE = 3;
	/**
	 * ID constant to retrieve the column transmitancy.
	 */
	public static final int COLUMN_TRANSMITANCY = 4;
	/**
	 * ID constant to retrieve the column time delay in ps.
	 */
	public static final int COLUMN_TIME_DELAY = 5;
	
	private static final String[] COLUMN_NAMES = new String[] {"", "Frequency (GHz)", "@SIZE18@TAU@SIZE12 (neper)", 
			"T_{b} (K)", "Transmitancy", "Time Delay (ps)"};
	
	/**
	 * Returns a given column from the model output. The model should
	 * be executed first.
	 * @param column The column ID constant.
	 * @return The array of values.
	 */
	public double[] getOutputColumn(int column)
	{
		double out[] = new double[this.output.length];
		for (int i=0; i<out.length; i++)
		{
			out[i] = Double.parseDouble(getField(column, this.output[i], " "));
		}
		return out;
	}
	
	/**
     * Transforms a string into an array for a given field separator.
     * @param text Text.
     * @param separator Separator.
     * @return Array.
     */
    public static String[] toStringArray(String text, String separator)
    {
    	StringTokenizer tok = new StringTokenizer(text, separator);
    	int n = tok.countTokens();
    	String array[] = new String[n];
    	for (int i=0; i<n; i++)
    	{
    		array[i] = tok.nextToken();
    	}
    	return array;
    }
    
	/**
	 * Writes an external text file.
	 * 
	 * @param pathToFile Path to the file.
	 * @param text Text to write as an array.
	 */
	public static void writeAnyExternalFile(String pathToFile, String text[]) throws Exception
	{
		BufferedWriter dis = new BufferedWriter(new FileWriter(new File(pathToFile)));

		for (int i = 0; i< text.length; i++)
		{
			dis.write(text[i] + getLineSeparator());
		}
		dis.close();
	}
	
	/**
	 * Get the system line separator.
	 * 
	 * @return Line separator.
	 */
	public static String getLineSeparator()
	{
		return System.getProperty("line.separator");
	}
	
	/**
	 * Get certain field of a string. Fields are supposed to be separated by a
	 * string called separator.
	 * 
	 * @param field Number of field. 1, 2, ...
	 * @param arg String with fields.
	 * @param separator String that defines the separator.
	 * @return The field in the desired position number.
	 */
	public static String getField(int field, String arg, String separator)
	{
		int space = 0;
		int err = 0;
		String text = arg.trim();
		String myfield = "";
		for (int i = 0; i < field; i++)
		{
			space = text.indexOf(separator) + 1;

			if (space > 0)
			{
				myfield = text.substring(0, space - 1).trim();
				text = text.substring(space + separator.length() - 1).trim();
			}
			if (space <= 0)
			{
				space = text.length();
				myfield = text.substring(0, space).trim();
				text = "";
			}

			if (space <= 0)
				err = 1;
		}

		if (err == 1)
			myfield = null;

		return myfield;
	}
	
	/**
	 * Executes a system command.
	 * @param command Command to execute.
	 * @return The process.
	 * @throws Exception If an error occurs.
	 */
	public static Process executeSystemCommand(String command)
	throws Exception {
		try { 
			String osName = System.getProperty("os.name"); 
			String[] cmd = new String[3]; 
//			       System.out.println("Operating System: \"" + osName.toString() + "\""); 
			String cmdline = command; 
//			       System.out.println("cmdline : " + cmdline); 
			if (osName.equals("Windows 98") || osName.equals("Windows 95")) { 
				cmd[0] = "command.com"; 
				cmd[1] = "/C"; 
				cmd[2] = cmdline; 
			} else if (osName.startsWith("Windows")) { 
				cmd[0] = "cmd.exe"; 
				cmd[1] = "/C"; 
				cmd[2] = cmdline; 
			} else if (osName.equals("Linux")) { 
				cmd[0] = "/usr/bin/konsole"; 
				cmd[1] = "-e"; 
				cmd[2] = cmdline; 
			} else { // for MAC
				cmd[0] = "open"; 
				cmd[1] = "-a"; 
				cmd[2] = cmdline; 		    	   
			}
	 
//			      System.out.println("Executing " + cmd[0] + " " + cmd[1] + " " + cmd[2]); 
			Runtime runtime = Runtime.getRuntime(); 
			Process pr = runtime.exec(command);
	      
	    	return pr;
		} catch (Throwable t) { 
			throw new Exception("error while executing system command: "+t.toString());		      
		} 
	}
		
	/**
	 * Returns the console output of a process. The method waits until
	 * the process finish.
	 * @param pr The process.
	 * @return The output, or null if an error occurs.
	 */
	public static String getConsoleOutputFromProcess(Process pr)
	throws Exception {
		String output = "";
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null) {
				output += line + getLineSeparator();
			}
			pr.waitFor();
		} catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	  
		return output;
	}


	/**
	 * Deletes the input file after the model execution.
	 * This operation is optional.
	 * @throws Exception If an error occurs.
	 */
	public void deleteInputFile()
	throws Exception {
		String path = AMAtmosphereModel.AM_INPUT_FILE;
		File f = new File(path);
		boolean success = f.delete();
		if (!success) throw new Exception("could not delete file "+path);
	}

	  
	/**
	 * Test program.
	 * @param args Unused.
	 */
/*	public static void main(String args[])
	{
		System.out.println("AMAtmosphereModel Test");

		// First step is to check that AM_PROGRAM_PATH and AM_PROGRAM_EXECUTABLE are correct,
		// and the AM executable is properly compiled in the same computer
		
		try {
			// Create an instance with some pressure in mbar (hPa), temperature in K, and relative humidity 0-1
			AMAtmosphereModel am = new AMAtmosphereModel(900, 273+20, 0.25);

			// Set model parameters
			double freq = 8;       // GHz
			double elev = 90.0;       // Elevation angle degrees
			
			// Rest of input parameters, quite obvious since we only want one frequency
			double fmin = freq-1; // min freq MHz
			double fmax = freq+1; // max freq MHz
			int nFreq = 1; // Number of points (frequencies) to calculate (not strictly exact)
			
			// Execute
			am.execute(fmin, fmax, elev, nFreq);

			// Show a formatted table with the output values
			double freqs[] = am.getOutputColumn(AMAtmosphereModel.COLUMN_FREQUENCY);
			double opac[] = am.getOutputColumn(AMAtmosphereModel.COLUMN_OPACITY);
			double trans[] = am.getOutputColumn(AMAtmosphereModel.COLUMN_TRANSMITANCY);
			String format = "f3.3, 3x, f3.5, 3x, f1.5";
			System.out.println("Frequency (GHz)  Opacity (neper)  Transmitancy");
			for (int i=0; i<freqs.length; i++)
			{
				System.out.println(""+freqs[i]+ " , "+opac[i]+ " , "+trans[i]);
			}
		
		} catch (Exception exc)
		{
			exc.printStackTrace();
		}
	}
*/
}
