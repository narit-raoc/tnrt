package alma.atmosphere;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Calendar;
import java.util.Locale;
import java.lang.ProcessBuilder;

import alma.ACSErrTypeCommon.wrappers.AcsJCouldntPerformActionEx;
import alma.ACSErrTypeCommon.wrappers.AcsJIllegalStateEventEx;
import alma.AtmError.ATMExecutionErrorEx;
import alma.AtmError.ATMInputFileIOErrorEx;
import alma.AtmError.cannotExecuteAMEx;
import alma.AtmError.cannotWriteAMInputFileEx;
import alma.AtmError.paramOutOfLimits;
import alma.AtmError.paramOutOfLimitsEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.ComponentImplBase;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.AcsEventPublisher;
// import alma.acs.nc.AcsEventSubscriber; // wait for NARIT's weather station
import alma.cdbErrType.CDBFieldDoesNotExistEx;
import alma.cdbErrType.CDBRecordDoesNotExistEx;
import alma.cdbErrType.CDBXMLErrorEx;
import alma.cdbErrType.WrongCDBDataTypeEx;
// import alma.weatherComNet.weatherDataBlock; // wait for NARIT's weather station
import alma.acsnc.EventDescription;

public class AtmImpl extends ComponentImplBase implements AtmOperations/*, AcsEventSubscriber.Callback<weatherDataBlock> // wait for NARIT's weather station*/ {
	private double m_temp;
    private double m_hum;
    private double m_pres;
    
	// private static final String m_weatherstationCurl = "SEAC_ARIES21";

	// private AcsEventSubscriber<weatherDataBlock> m_consumerWeather = null; // wait for NARIT's weather station
	
    private double m_hscaleMeters = 0.0;
    
    private double m_opacity = 0;
    private double m_wh2omm = 0;
    private double m_freq = 0;
    
    private AcsEventPublisher<opacityNotifyBlock> m_supplier = null;
	
	public void initialize(ContainerServices containerServices) throws ComponentLifecycleException {
		super.initialize(containerServices);
		m_logger.finer("initialize() called...");
		String name = containerServices.getName();
		
		try {
			m_supplier = containerServices.createNotificationChannelPublisher(alma.atmosphere.ATMCHANNEL.value, opacityNotifyBlock.class);
			
			// wait for NARIT's weather station
	        // m_consumerWeather = containerServices.createNotificationChannelSubscriber(alma.weatherComNet.CHANNELNAME.value , weatherDataBlock.class);
	        // m_consumerWeather.addSubscription(this);
	        // m_consumerWeather.startReceivingEvents();

		} catch (Exception ex) {
			throw new ComponentLifecycleException(ex);
		}

	    try {
			m_hscaleMeters = containerServices.getCDB().get_DAO_Servant("alma/"+name).get_double("HSCALE");
        } catch (WrongCDBDataTypeEx e) {
			throw new ComponentLifecycleException(e);
        } catch (CDBFieldDoesNotExistEx e) {
			throw new ComponentLifecycleException(e);
        } catch (CDBXMLErrorEx e) {
			throw new ComponentLifecycleException(e);
        } catch (CDBRecordDoesNotExistEx e) {
			throw new ComponentLifecycleException(e);
        } catch (AcsJContainerServicesEx e) {
			throw new ComponentLifecycleException(e);
        }
	}

	public void cleanUp() {
		m_logger.info("cleanUp() called...");

		try {
			m_supplier.disconnect();
		} catch (AcsJIllegalStateEventEx e) {
			e.printStackTrace();
		}

		// wait for NARIT's weather station
        // try {
		// 	m_consumerWeather.disconnect(); 
		// } catch (AcsJIllegalStateEventEx e) {
		// 	e.printStackTrace();
		// } catch (AcsJCouldntPerformActionEx e) {
		// 	e.printStackTrace();
		// }
	}

	/**
	 *@return computed refraction coeficient R0
	 */
	public double r0() {
		double ts = 0.0;
		double ps = 0.0;
		double hr = 0.0;
		double pw = 0.0;
		double r0 = 0.0;

		ts = getTemperature() + 273.15;
		ps = getPressure();
		hr = getHumidity();
		
		pw = (6.015*hr/100)*Math.pow((ts/273),-5.31)*Math.exp(25.22*(ts-273)/ts);
		
		r0 = (16.01/ts)*(ps-(0.072*pw)+(4831*pw/ts));
		
		return r0;
	}

	/**
	 *@return computed B1 parameter in degrees for the refraction correction using the Bennett formulae
	 */
	public double b1() {
		double b1 = 5.9;
		return b1;
	}

	/**
	 *@return computed B2 parameter in degrees for the refraction correction using the Bennett formulae
	 */
	public double b2() {
		double b2 = 2.5;
		return b2;
	}

	/**
	 *@return correction in elevation in arcsecs
	 *@param elev elevation of the source in degrees
	 */
	public double elevationCorrection(double elev) {
		double angle = 0.0;
		double deltaEl = 0.0;

		angle = (90 - elev - b1() / (elev + b2()) ) * Math.PI/180.0;
		deltaEl = r0() * Math.abs( Math.tan(angle) );
		
		return deltaEl;
	}

	/**
	*@return dew temperature in degs C 
	*/

	public double dewTemperature() {
		double tempC = getTemperature();
		double hr = getHumidity();

		double a = 17.271;
		double b = 237.7;

		double gam = a * tempC / (b+tempC) + Math.log(hr/100.);
        double tdew = b * gam / (a - gam);

		//double tdew = Math.pow(hr/100.0, 0.125)*(110+tempC) - 110;
		return tdew;
	}

	/**
	 *@return an average temperature for the current time and date
	 */
	private double fixedTemperature() {
		double temp = 0.0;

		Calendar rightNow = Calendar.getInstance();

		/* month 0 = JANUARY */
		int month = rightNow.get(Calendar.MONTH);
		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		
		if (month == Calendar.DECEMBER || month == Calendar.JANUARY || month == Calendar.FEBRUARY || month == Calendar.MARCH) {
			if (hour >= 0 && hour <= 7) {
				temp = 2;
			} else {
				temp = 12;
			}
		} else if (month == Calendar.APRIL || month == Calendar.MAY || month == Calendar.OCTOBER || month == Calendar.NOVEMBER) {
			if (hour >= 0 && hour <= 7) {
				temp = 8;
			} else {
				temp = 16;
			}
		} else if (month == Calendar.JUNE || month == Calendar.JULY || month == Calendar.AUGUST || month == Calendar.SEPTEMBER) {
			if (hour >= 0 && hour <= 7) {
				temp = 16;
			} else {
				temp = 24;
			}
		}
		
		return temp;
	}

	private double fixedPressure() {
		double pres = 950.0;
		
		return pres;
	}
	
	private double fixedHumidity() {
		double hum = 0.0;
		
		Calendar rightNow = Calendar.getInstance();

		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		
		if (hour >= 0 && hour <= 7) {
			hum = 60.0;
		} else {
			hum = 30.0;
		}
		
		return hum;
	}
	
	public double getTemperature() {
		/* set variable to a nonsense value*/
		double temp = -300.0;
		
		temp = m_temp;

		if (temp != -300.0) {
			return temp;
		}
		
		temp = fixedTemperature();
		return temp;
	}

	public double getPressure() {
		/* set variable to a nonsense value*/
		double pres = -300.0;
		
		pres = m_pres;

		if (pres != -300.0) {
			return pres;
		}
		
		pres = fixedPressure();
		return pres;
	}

	public double getHumidity() {
		/* set variable to a nonsense value*/
		double hum = -300.0;
		
		hum = m_hum;
		if (hum != -300.0) {
			return hum;
		}

		hum = fixedHumidity();
		return hum;
	}

	public void setTemperature(double t){
		m_temp = t;
	}

	public void setPressure(double p){
		m_pres = p;
	}

	public void setHumidity(double h){
		m_hum = h;
	}

	/**
	 * Get opacity of the atmosphere.
	 * @param pressure Surface pressure in mbar (= hPa).
	 * @param temperature Surface temperature in C.
	 * @param humidity Humidity fraction, from 0 to 100.
	 * @param freq Frequency in MHz.
	 * @param elevation Antenna elevation in degrees.
	 * @return Opacity (neper), or -1 if an error occurs.
	 * @throws cannotExecuteAMEx If AM cannot be executed.
	 * @throws cannotWriteAMInputFileEx If the input file cannot be written.
	 * @throws paramOutOfLimits If the input parameters are out of bounds.
	 */
	public double atmOpacity(double pressure, double temperature, double humidity, double freq, double elevation)
	throws cannotExecuteAMEx, cannotWriteAMInputFileEx, paramOutOfLimitsEx {
		// Show input values
		Object [] obj = new Object[1];
		Double pres = new Double(pressure);
		obj[0] = pres;
		String strLine = String.format(Locale.US, "%f", obj);
		//String strLine = String.format(Locale.US, "AMAtmosphereModel(%10.4f %10.4f %10.4f %10.4f %10.4f)", pressure, temperature+273.15, humidity/100.0, freq/1000., elevation);
		//m_logger.info(strLine);

		// Check values. The model accept a wider range, but here they are constrained by the antenna/site limitations
		if (pressure < 500 || pressure > 2000) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Pressure %f [mbar] is out of limits", pressure);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}
		if (temperature < -50 || temperature > 80) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Temperature %f [C] is out of limits", temperature);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}
		if (humidity < 0 || humidity > 100) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Humidity %f is out of limits", humidity);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}
		if (freq < 500 || freq > 200000) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Frequency %f [MHz] is out of limits", freq);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}
		if (elevation < 0 || elevation > 90) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Elevation %f [degs] is out of limits", elevation);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}

		// Create an instance with some pressure in mbar (hPa), temperature in K, and relative humidity 0-1
		AMAtmosphereModel am = new AMAtmosphereModel(pressure, temperature+273.15, humidity/100.0);

		// Set model parameters
		double fmin = (freq-1)/1000.0; // min freq GHz
		double fmax = (freq+1)/1000.0; // max freq GHz
		int nFreq = 1; // Number of points (frequencies) to calculate (not strictly exact)
		
		// Execute
		am.execute(fmin, fmax, elevation, nFreq, m_hscaleMeters);

		// Get some arrays with the output values
//			double freqs[] = am.getOutputColumn(AMAtmosphereModel.COLUMN_FREQUENCY);
		double opac[] = am.getOutputColumn(AMAtmosphereModel.COLUMN_OPACITY);
//			double trans[] = am.getOutputColumn(AMAtmosphereModel.COLUMN_TRANSMITANCY);
		
		double opacity = opac[0];
		m_opacity = opacity;
		m_freq = freq;
		opacityNotifyBlock odb = new opacityNotifyBlock(m_opacity, m_wh2omm, m_freq);
		try {
			m_supplier.publishEvent(odb);
		} catch (AcsJException e) {
			m_logger.warning("Supplier unable to publish opacityNotifyBlock");
			e.printStackTrace();
		}
		return opacity;
	}


	public alma.atmosphere.totalAtmOpacityEmissivity atmOpacity_ATM(double freq, double wh2o, double elevation) throws ATMExecutionErrorEx, ATMInputFileIOErrorEx, paramOutOfLimitsEx {
		String strLine = "";
        String line = null;
        BufferedWriter out = null;
		Scanner sc = null;
        Process p = null;

        String ATM_PROGRAM_PATH = System.getProperty("user.home")+"/extprodroot/bin";
        String ATM_PROGRAM_EXECUTABLE = "atm";
        String ATM_INPUT_PATH = System.getProperty("user.home")+"/introot";
        String ATM_INPUT_FILE = "atm_in.txt";
        String ATM_OUTPUT_FILE = "atm_out.txt";

        alma.atmosphere.totalAtmOpacityEmissivity to = new alma.atmosphere.totalAtmOpacityEmissivity();

        String strLine2 = String.format(Locale.US, "Calculating ATM opacity: Freq: %9.3f wh2o %5.2f Elevation %4.1f", freq, wh2o, elevation);
        m_logger.info(strLine2);

		if (freq < 500 || freq > 200000) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
			strLine = "Frequency " + freq + " [MHz] is out of limits";
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}
		if (wh2o < 0) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
			strLine = "Wh2o " + wh2o + " [mm] is out of limits";
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}
		if (elevation < 0 || elevation > 90) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
			strLine = "Elevation " + elevation + " [degs] is out of limits";
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}

		String infilepath = ATM_INPUT_PATH +"/"+ ATM_INPUT_FILE;
		String outfilepath = ATM_INPUT_PATH +"/"+ ATM_OUTPUT_FILE;
		m_logger.info("path to write input file: "+infilepath);
		m_logger.info("path to write output file: "+outfilepath);

		try {
			out = new BufferedWriter(new FileWriter(new File(infilepath)));
		} catch (IOException e) {
			e.printStackTrace();
			alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx newEx = new alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx();
			strLine = "Unable to open file " + infilepath;
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toATMInputFileIOErrorEx();
		}
		Calendar now = Calendar.getInstance();
		if (now.get(Calendar.MONTH) < Calendar.NOVEMBER && now.get(Calendar.MONTH) > Calendar.MARCH) {
			try {
				out.write("obsv 19\n");
			} catch (IOException e) {
				e.printStackTrace();
				alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx newEx = new alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx();
				strLine = "Unable to write data to file " + infilepath;
				newEx.setProperty("ErrorDesc", strLine);
				throw newEx.toATMInputFileIOErrorEx();
			}
		} else {
			try {
				out.write("obsv 18\n");
			} catch (IOException e) {
				e.printStackTrace();
				alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx newEx = new alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx();
				strLine = "Unable to write data to " + infilepath;
				newEx.setProperty("ErrorDesc", strLine);
				throw newEx.toATMInputFileIOErrorEx();
			}
		}
		try {
			out.write("freq " + freq/1000.0 + "\n");
			out.write("wh2o " + wh2o + "\n");
			out.write("airm " + 1.0/Math.sin(Math.toRadians(elevation)) + "\n");
			out.write("lsb\n");
			out.write("comp\n");
			out.write("exit\n");
		} catch (IOException e) {
			e.printStackTrace();
			alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx newEx = new alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx();
			strLine = "Unable to write data to " + infilepath;
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toATMInputFileIOErrorEx();
		}
		
		try {
			out.close();
		} catch (IOException e2) {
			e2.printStackTrace();
			alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx newEx = new alma.AtmError.wrappers.AcsJATMInputFileIOErrorEx();
			strLine = "Unable to close " + infilepath;
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toATMInputFileIOErrorEx();
		}
			
        m_logger.info("Create ProcessBuilder command: sh -c "+ATM_PROGRAM_PATH+"/"+ATM_PROGRAM_EXECUTABLE);
        ProcessBuilder builder = new ProcessBuilder("sh","-c", ATM_PROGRAM_PATH+"/"+ATM_PROGRAM_EXECUTABLE);
		builder.redirectInput(new File(infilepath));
		builder.redirectOutput(new File(outfilepath));
		builder.redirectError(new File(outfilepath));
		
        try {
            p = builder.start(); // may throw IOException
            p.waitFor();
        } 
        catch (Exception e) {
            e.printStackTrace();
            alma.AtmError.wrappers.AcsJATMExecutionErrorEx newEx = new alma.AtmError.wrappers.AcsJATMExecutionErrorEx();
            strLine = "Error executing command: "+builder.toString();
    		newEx.setProperty("ErrorDesc", strLine);
            throw newEx.toATMExecutionErrorEx();
        }
       
        try{
        	p.getInputStream().close();
        	p.getOutputStream().close();
        	p.getErrorStream().close();
        	p.destroy();
        }
        catch (IOException e) {
            e.printStackTrace();
            alma.AtmError.wrappers.AcsJATMExecutionErrorEx newEx = new alma.AtmError.wrappers.AcsJATMExecutionErrorEx();
            strLine = "Error executing command: "+builder.toString();
    		newEx.setProperty("ErrorDesc", strLine);
            throw newEx.toATMExecutionErrorEx();
        }

   		try {
			m_logger.info("Opening file to read result: "+outfilepath);
			File f = new File(outfilepath);
			sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			alma.AtmError.wrappers.AcsJATMExecutionErrorEx newEx = new alma.AtmError.wrappers.AcsJATMExecutionErrorEx();
            strLine = "Unable to open file" + outfilepath + "after command execution: "+builder.toString();
			newEx.setProperty("ErrorDesc", strLine);
            throw newEx.toATMExecutionErrorEx();
		}
        
        try {
        	m_logger.info("Reading lines from file: "+outfilepath);
        	
        	if(!sc.hasNextLine()){
        		m_logger.warning("file has no first line");
        	}//if
        	
        	while(sc.hasNextLine()) {
			    line = sc.nextLine();
			    //m_logger.info("line: " + line);
			    if (line.contains("total atmospheric opacity")) {
                                String values = line.split("=")[1];
                                values = values.trim();
                                m_logger.finer("total atmospheric opacity - values: "+values);
                                String [] op = values.split(" ");
                                to.signalSideBandOpacity = new Double(op[0]).doubleValue();
                                to.imageSideBandOpacity = new Double(op[op.length - 1]).doubleValue();
                            } else if (line.contains("Calculated R.J. Temp")) {
                                String values = line.split("=")[1];
                                values = values.trim();
                                m_logger.finer("Calculated R.J. Temp - values: "+values);
                                String [] em = values.split(" ");
                                to.signalSideBandEmissivity = new Double(em[0]).doubleValue();
                                to.imageSideBandEmissivity = new Double(em[em.length - 1]).doubleValue();
                            }//else if
        	}//while
        	sc.close();
        }//try
     	catch(Exception e) {
        	e.printStackTrace();
        	alma.AtmError.wrappers.AcsJATMExecutionErrorEx newEx = new alma.AtmError.wrappers.AcsJATMExecutionErrorEx();
        	strLine = "Unable to read file /tmp/atm.out";
        	newEx.setProperty("ErrorDesc", strLine);
        	throw newEx.toATMExecutionErrorEx();
        }//catch
        
        m_opacity = to.signalSideBandOpacity;
        strLine2 = String.format(Locale.US, "ATM opacity: %f", m_opacity);
        m_logger.info(strLine2);

		m_freq = freq;

        opacityNotifyBlock odb = new opacityNotifyBlock(m_opacity, m_wh2omm, m_freq);

        try {
        	m_supplier.publishEvent(odb);
        } catch (AcsJException e) {
        	m_logger.warning("Supplier unable to publish opacityNotifyBlock");
        	e.printStackTrace();
        }

        return to;
	}


	/**
	 * Return water vapor pressure = relative humidity * saturation vapor pressure.
	 * @param temperature Surface temperature in K.
	 * @param humidity Humidity fraction, from 0 to 1.
	 * @return Water vapor pressure in hPa.
	 */
	public double Ph2o(double temperature, double humidity)  throws paramOutOfLimitsEx {
		// Create an instance with some pressure in mbar (hPa), temperature in K, and relative humidity
		// Note data from station has T in C, and humidity from 0 to 100

		String strLine = "";

		if (temperature < -50 || temperature > 80) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Temperature %f [C] is out of limits", temperature);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}

		if (humidity < 0 || humidity > 100) {
			alma.AtmError.wrappers.AcsJparamOutOfLimitsEx newEx = new alma.AtmError.wrappers.AcsJparamOutOfLimitsEx();
//			strLine = String.format("Humidity %f is out of limits", humidity);
			newEx.setProperty("ErrorDesc", strLine);
			throw newEx.toparamOutOfLimitsEx();
		}

		AMAtmosphereModel am = new AMAtmosphereModel(1000, temperature+273.15, humidity/100.0);
		double h2oScale = am.getH2OScale(m_hscaleMeters);
		double ph2o = h2oScale * AMAtmosphereModel.MODEL_H2O_MM / 10.19716213;
		return ph2o;	
	}
	
	/**
	 * Computes the ammount of precipitable water in [mm] obtained from weather parameters and an approximation for the partial pressure of water vapour
	 * Approximation is good between 0 and 60 degrees. This poses aquestion for very low temperatures.
	 * 
	 * @return wh2omm Ammount of precipitable water in the atmosphere.
	 */

	public double wh2o() {
		// Constants. Mass of H2O molecule [Kg]
		double mh2o = 1.66053886e-27*18;
		// Constants. Boltzmann constant
		double kb = 1.3806503e-23;
		// Height scale for water in atmosphere [m]
		// Now from the database
		// double hscale = 2500;
		
		
		// Dew Temperature
		double tdew = dewTemperature();
		
		// Partial pressure of water vapour
		double ph2ombar = Math.exp(1.81+17.27*tdew/(tdew+237.15));
		double ph2o = ph2ombar*100;

		double tk = getTemperature()+273.15;

		// millimeters of water
		double wh2omm = mh2o*ph2o*m_hscaleMeters/(kb*tk);
		m_wh2omm = wh2omm;
		opacityNotifyBlock odb = new opacityNotifyBlock(m_opacity, m_wh2omm, m_freq);
		try {
			m_supplier.publishEvent(odb);
		} catch (AcsJException e) {
			m_logger.warning("Supplier unable to publish opacityNotifyBlock");
			e.printStackTrace();
		}
		return wh2omm;
	}
	
	// wait for NARIT's weather station
	// public Class<weatherDataBlock> getEventType() {
	// 	return weatherDataBlock.class;
	// }
	
	// wait for NARIT's weather station
	// public void receive(weatherDataBlock eventData, EventDescription eventDescrip) {
	// 	m_temp = eventData.temperature;
	// 	m_hum = eventData.humidity;
	// 	m_pres = eventData.pressure;
	// }
}
