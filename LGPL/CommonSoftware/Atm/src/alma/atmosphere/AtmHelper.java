package alma.atmosphere;

import java.util.logging.Logger;

import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ComponentHelper;
import alma.atmosphere.AtmOperations;
import alma.atmosphere.AtmPOATie;

public class AtmHelper extends ComponentHelper
{
	public AtmHelper(Logger containerLogger)
	{
		super(containerLogger);
	}
	
	protected ComponentLifecycle _createComponentImpl()
	{
		return new AtmImpl();
	}
	
	protected Class _getPOATieClass()
	{
		return AtmPOATie.class;
	}
	
	protected Class _getOperationsInterface()
	{
		return AtmOperations.class;
	}
}

