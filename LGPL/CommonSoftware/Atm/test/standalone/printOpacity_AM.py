import AtmosphereWrapper as amw

a = amw.Atmosphere()
# Set default values of b1, b2
a.setB1(5.9)
a.setB2(2.5)

pressure = 915.7
temperature = -2.2
humidity = 70

# Set some values in the Atmosphere object.  will be used to calculate new r0,
# and refraction angle.
a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

# Choose elevation angle
elevation = 90

print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('Pressure: %6.2f, \tTemperature: %6.2f, \tHumidity %4.2f'
						% (p, t, h))
print('__________________________________________________')

freq = 22235
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 23400
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 8400
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 6600
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 4900
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))

pressure = 913.3
temperature = 21.7
humidity = 30

a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

print('\nget updated values of p,t,h')
p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

print('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p, t, h))
print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('__________________________________________________')
freq = 22235
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 23400
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 8400
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 6600
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 4900
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))

pressure = 913.3
temperature = 21.7
humidity = 70

a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

print('\nget updated values of p,t,h')
p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

print('Pressure: %6.2f\tTemperature: %6.2f\tHumidity %4.2f' % (p, t, h))
print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('__________________________________________________')
freq = 22235
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 23400
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 8400
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 6600
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
freq = 4900
result = a.opacity_AM(p, t, h, freq, elevation)
print('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, result['fmin']['opacity'], a.ph2o(t, h)))
