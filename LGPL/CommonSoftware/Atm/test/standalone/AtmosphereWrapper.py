# A python wrapper for atmosphere calculatoins.
# Does not require ACS. It can run in normal python
import numpy as np
import matplotlib
import subprocess
from io import StringIO

class Atmosphere:
	''' class atmosphere '''

	def __init__(self):
		self.pressure = -999.0
		self.temperature = -999.0
		self.humidity = -999.0
		self.wh2omm = -999.0
		# Some default values copied from OAN.  Probably should change for
		# Conditions in Thailand
		self.hscaleMeters = 1400.0
		self.b1 = 5.9
		self.b2 = 2.5

	# ----- Functions to set instance variables ------------------------------
	def setPressure(self, p):
		self.pressure = p

	def setTemperature(self, t):
		self.temperature = t

	def setHumidity(self, h):
		self.humidity = h

	def setB1(self, b1):
		self.b1 = b1

	def setB2(self, b2):
		self.b2 = b2

	# ----- Functions to get instance variables ------------------------------
	def getPressure(self):
		return self.pressure

	def getTemperature(self):
		return self.temperature

	def getHumidity(self):
		return self.humidity

	def getB1(self):
		'''returns B1 parameter in degrees for the refraction correction using the
		Bennett formula
		'''
		return self.b1

	def getB2(self):
		'''returns B2 parameter in degrees for the refraction correction using the
		Bennett formula
		'''
		return self.b2

	# ----- Functions to calculate -------------------------------------------
	def r0(self):
		''' Calculates refraction coefficient r0
		'''
		tk = self.temperature + 273.15

		pw = ((6.015 * self.humidity / 100)
								* ((tk / 273) ** -5.31)
								* np.exp(25.22 * (tk - 273) / tk))

		r0 = (16.01 / tk) * (self.pressure - (0.072 * pw) + (4831 * pw / tk))
		return r0

	def elevationCorrection(self, elev):
		angle = (90 - elev - self.b1 / (elev + self.b2)) * np.pi / 180.0
		deltaEl = self.r0() * np.abs(np.tan(angle))
		return deltaEl

	def dewTemperature(self):
		''' Calculates dew temperature in degrees C
		'''
		a = 17.271
		b = 237.7
		tempC = self.temperature
		hr = self.humidity

		gam = a * tempC / (b + tempC) + np.log(hr / 100.)
		tdew = b * gam / (a - gam)
		return tdew

	def wh2o(self):
		''' Computes the ammount of precipitable water in [mm] obtained from
		weather parameters and an approximation for the partial pressure of
		water vapour.  Approximation is good between 0 and 60 degrees celcius.
		returns wh2omm Ammount of precipitable water in the atmosphere.
		note: constants and equations copied from AtmImpl.java. Not verified
		for	correctness yet.
		'''
		# TODO: checK this value.  where does it come from?
		# Constants Mass of H2O molecule [Kg]
		MASS_H20 = 18 * 1.66053886e-27
		# Constants Boltzmann constant
		K_BOLTZMANN = 1.3806503e-23

		# Dew temparature
		tdew = self.dewTemperature()

		# Partial pressure of water vapor
		ph2ombar = np.exp(1.81 + 17.27 * tdew / (tdew + 237.15))
		ph2o = ph2ombar * 100

		tk = self.temperature + 273.15

		# millimeters of water
		wh2omm = MASS_H20 * ph2o * self.hscaleMeters / (K_BOLTZMANN * tk)
		self.wh2omm = wh2omm
		return wh2omm

	def ph2o(self, temperature_C, humidity_percent):
		'''
		Return water vapor pressure = relative humidity * saturation vapor pressure.
		param temperature Surface temperature in C.
		param humidity Humidity fraction, from 0 to 1.
		return Water vapor pressure in hPa.
		'''
		pressure = 1000
		am = Wrapper_AM(pressure, temperature_C + 273.15, humidity_percent / 100.0)
		h2oScale = am.getH2OScale(self.hscaleMeters)
		pres = h2oScale * Wrapper_AM.MODEL_H2O_MM / 10.19716213
		return pres

	def opacity_ATM(self, freq, wh2o, elevation):
		''' Use ATM model co calculate opacity and emissisivity of atmosphere
		The model calculates manyt results, but this function reutns only the
		Signal Sideband Opacity.
		'''
		atmObj = Wrapper_ATM(freq, wh2o, elevation)
		atmObj.generateInputFile()
		atmObj.runModel()
		resultsDict = atmObj.readResults()
		return resultsDict

	def opacity_AM(self, pressure, temperature, humidity, freq, elevation):
		''' Use AM model co calculate opacity of atmosphere
		The model calculates many results and writes them out to a text file,
		but this function reutns only the opacity
		'''
		# Set model parameters
		fmin = (freq - 1) / 1000.0  # // min freq GHz
		fmax = (freq + 1) / 1000.0  # // max freq GHz
		nfreq = 1  # Number of points (frequencies) to calculate (not strictly exact)
		# Create an instalce of Wrapper_AM with parameters that will be used as
		# input to the model
		# pressure in mbar (hPa), temperature in K, and relative humidity 0-1
		amObj = Wrapper_AM(pressure, temperature + 273.15, humidity / 100.0)
		amObj.generateInputFile(fmin, fmax, nfreq, self.hscaleMeters)
		amObj.runModel(freq, elevation)
		resultsDict= amObj.readResults()
		return resultsDict


class Wrapper_ATM:
	'''Class to interface with AM model.
	prepares input file from information in Atmosphere object
	parses output file and returns data of interest.
	'''
	PROGRAM_PATH = '$HOME/extprodroot/bin/atm'

	def __init__(self, freq, wh2o, elevation):
		'''Constructor for Wrapper_ATM'''
		self.filename_in = './modelIO/atm_in.txt'
		self.filename_out = './modelIO/atm_out.txt'
		self.freq = freq
		self.wh2o = wh2o
		self.elevation = elevation
		self.toeDict = {
			'signalSideBandOpacity': -999.0,
			'imageSideBandOpacity': -999.0,
			'signalSideBandEmissivity': -999.0,
			'imageSideBandEmissivity': -999.0,
		}

	def generateInputFile(self):
		# Create the text file that will go into the ATM model.
		# Use context manager 'with' to handle cleanup. No need to close files
		with open(self.filename_in, 'w') as file_in:
			# Observing station - OBSV 19 is Yebes winter season
			# TODO - change observing station after this function is confirmed correct
			# By comparing data from this standalone to the ACS version of software.
			# Enter the command "OBSV" in atm to see the list available
			file_in.write('obsv 18\n')

			# Write other parameters and commands to the atm input script
			file_in.write('freq %f \n' % (self.freq / 1000.0))
			file_in.write('wh2o %f\n' % self.wh2o)
			file_in.write('airm %f\n' % (1.0 / np.sin(np.radians(self.elevation))))
			file_in.write('lsb\n')
			file_in.write('comp\n')
			file_in.write('exit\n')

	def runModel(self):
		with open(self.filename_in, 'rb') as file_in, open(self.filename_out, 'w') as file_out:
			# Run the model and capture the output in a text file
			cmd = Wrapper_ATM.PROGRAM_PATH
			subprocess.call(cmd, shell=True, stdin=file_in, stdout=file_out, stderr=file_out)

	def readResults(self):
		# Parse the text file and read the opacity and emissivity data
		# from the last two columns on the right of the file (-2 and -1)
		with open(self.filename_out, 'rt') as file_parse:
			for line in file_parse:
				if 'total atmospheric opacity' in line:
					cols = line.split()
					self.toeDict['signalSideBandOpacity'] = float(cols[-2])
					self.toeDict['imageSideBandOpacity'] = float(cols[-1])
				if 'Calculated R.J. Temp' in line:
					cols = line.split()
					self.toeDict['signalSideBandEmissivity'] = float(cols[-2])
					self.toeDict['imageSideBandEmissivity'] = float(cols[-1])

		return self.toeDict

class Wrapper_AM:
	'''Class to interface with AM model.
	prepares input file from information in Atmosphere object
	parses output file and returns data of interest.
	'''

	# Path to compiled binary am.  Should be in the system $PATH already
	PROGRAM_PATH = '$HOME/extprodroot/bin/am'

	# Default value to use in the model input file (.amc) before we search
	# and replace with updated value from real pressure, temp, humidity.
	MODEL_H2O_MM = 19.25

	# column IDs of data file output am_out.txt. these output quantities
	# and units were specified on line 10 of am_in.txt and sent to the model
	# >> f %1 GHz  %2 GHz  2.000000 MHz
	# >> output f GHz  tau neper  Tb K  tx  L ps
	# See am-manual-10.0 PDF for more details.  
	# https://zenodo.org/record/1193646#.XF0BN5wxVhE
	# Appendix B.1 Keywords and Syntax.  PDF page 171, manual page 165.

	RESULTCOLUMN_FREQUENCY = 0				# GHz
	RESULTCOLUMN_OPACITY = 1				# neper
	RESULTCOLUMN_BRIGHTNESS_TEMPERATURE = 2	# K
	RESULTCOLUMN_TRANSMITTANCE = 3			# no unit
	RESULTCOLUMN_DELAY = 4					# ps
	RESULTROW = -1						# GHz. Last row in file


	def __init__(self, pressure, temperature_k, humidity_rel):
		'''Constructor for Wrapper_ATM'''
		self.filename_generic = './modelIO/am_in_generic.txt'
		self.filename_in = './modelIO/am_in.txt'
		self.filename_out = './modelIO/am_out.txt'
		self.fmin = -999.0
		self.fmax = -999.0
		self.nfreq = -999
		self.hscale = -999.0
		self.pressure = pressure
		self.temperature = temperature_k
		self.humidity = humidity_rel

		self.amOutDict = {'fmin': {'frequency': -999.0,'opacity': -999.0, 'brightness_temperature': -999.0, 'transmittance': -999.0,'delay': -999.0},
			'fmax': {'frequency': -999.0,'opacity': -999.0,'brightness_temperature': -999.0, 'transmittance': -999.0,'delay': -999.0}}

	def generateInputFile(self, fmin, fmax, nfreq, hscale):
		self.fmin = fmin
		self.fmax = fmax
		self.nfreq = nfreq
		self.hscale = hscale

		df = 1000.0 * (self.fmax - self.fmin) / float(self.nfreq)
		# Create the text file that will go into the AM model.
		# Load generic file and replace @ fields with numbers from this Atmosphere object
		with open(self.filename_generic, 'rt') as file_generic, open(self.filename_in, 'w') as file_in:
			lines = file_generic.readlines()
			for line in lines:
				line = line.replace('@DF', '%f' % df)
				line = line.replace('@PRESSURE', '%f' % self.pressure)
				line = line.replace('@TEMPERATURE', '%f' % self.temperature)
				line = line.replace('@HUMIDITY', '%f' % self.humidity)
				line = line.replace('@H2OMM', '%f' % self.getH2OScale(self.hscale))
				file_in.write(line)

	def runModel(self, freq, elevation):
		with open(self.filename_out, 'w') as file_out:
			# Generate the shell command string including all parameters
			cmd = '%s %s %f %f %f' % (Wrapper_AM.PROGRAM_PATH, self.filename_in, self.fmin, self.fmax, (90-elevation))
			# subprocess.call requires shell=True to accept additional command line arguments.
			subprocess.call(cmd, shell=True, stdout=file_out, stderr=file_out)

	def readResults(self):
		with open(self.filename_out, 'rt') as file_parse:
			lines = file_parse.read().splitlines()
			row_fmin = lines[Wrapper_AM.RESULTROW].split()
						
			self.amOutDict['fmin']['frequency'] = float(row_fmin[Wrapper_AM.RESULTCOLUMN_FREQUENCY])
			self.amOutDict['fmin']['opacity'] = float(row_fmin[Wrapper_AM.RESULTCOLUMN_OPACITY])
			self.amOutDict['fmin']['brightness_temperature'] = float(row_fmin[Wrapper_AM.RESULTCOLUMN_BRIGHTNESS_TEMPERATURE])
			self.amOutDict['fmin']['transmittance'] = float(row_fmin[Wrapper_AM.RESULTCOLUMN_TRANSMITTANCE])
			self.amOutDict['fmin']['delay'] = float(row_fmin[Wrapper_AM.RESULTCOLUMN_DELAY])
			
		return self.amOutDict

	def getH2OScale(self, hscale):
		'''Returns the H2O scale parameter to put in the model input file.
		return H2O scale factor.
		in parameter hscale:

		More comments from OAN software of this function

		See http://en.wikipedia.org/wiki/Goff-Gratch_equation
		double ts = 373.15;
		double logp = -7.90298 * (ts / t - 1.0) +
		5.02808 * Math.log10(ts / t) - 1.3816E-7 * (Math.pow(10.0, 11.344 * (1.0 - t / ts)) - 1.0) +
		8.1328E-3 * (Math.pow(10.0, -3.49149 * (ts / t - 1.0)) - 1.0) +
		Math.log10(1013.246);
		double p = Math.pow(10.0, logp) * 10.19716213; // 0.75 => p from mb (hPa) to mm Hg, 10.2 => to mm of H2O

		updated: http://en.wikipedia.org/wiki/Arden_Buck_Equation
		t = t - 273.15;
		double p = 6.1121 * Math.exp((18.678 - t / 234.5) * t / (257.14 + t)) * 10.19716213;
		double mmH2O = this.relativeHumidity * p;

		Pablo de Vicente. 18/8/2010.
		La presion esta mal. Nueva manera de hacerlo:
		Height scale: 2500 m. Up to May 28 2012
		double hscale = 2500;
		double hscale = 1400;
		Mass of water molecule
		double mh2o = 1.66053886e-27*18;
		Boltzmann Constant
		double kb = 1.3806503e-23;
		'''
		# make local copies of the varables to help the long equation be shorter
		t = self.temperature
		h = self.humidity
		# print('getH2Oscale, t: %f, h: %f' % (t,h))
		# mh2o/kb
		mh2o_kb = 0.0021649000822293665

		# From Pere Planesas (probably I (PdV) got it from one of his programs).
		# It gives exactly the same values as Graff approximation
		ph2o = 100 * (6.015 * h / 100.) * ((t / 273.)** -5.31) * np.exp(25.22 * (t - 273) / t)
		# print('getH2Oscale, ph2o: %f' % ph2o)

		# mmH2O = mh2o*ph2o*hscale/(kb*t);
		mmH2O = 100 * mh2o_kb * ph2o * hscale / t
		# print('getH2Oscale, mmH2O: %f' % mmH2O)
		return(mmH2O / Wrapper_AM.MODEL_H2O_MM)
