import socket
import threading
from constant import PORT, COMMAND, BUFFER_SIZE, PUMP_STATUS, COMMAND_INTERVAL
import time


class Pump:
    def __init__(self):
        self.device = None
        self.ip = socket.gethostbyname("LbandPump")
        self.isOn = threading.Event()
        self.isTurbo = threading.Event()

    def __del__(self):
        pass

    def connect(self):
        self.device = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.device.connect((self.ip, PORT))

    def disconnect(self):
        self.device.close()

    def stream_command(self, event, cmd):
        while event.is_set():
            try:
                self.send_command(cmd)
                time.sleep(COMMAND_INTERVAL)
            except Exception as e:
                print(e)
                event.clear()

    def start(self):
        if self.isOn.is_set():
            raise Exception("Pump already on")
        self.connect()
        self.isOn.set()
        powerThread = threading.Thread(
            name="power-on-thread",
            target=self.stream_command,
            args=[self.isOn, COMMAND["START"]],
        )
        powerThread.daemon = True
        powerThread.start()

    def turbo_on(self):
        if self.isTurbo.is_set():
            raise Exception("Already in turbo mode")
        if not self.isOn.is_set():
            raise Exception("Pump is off")
        self.isTurbo.set()
        powerThread = threading.Thread(
            name="turbo-thread",
            target=self.stream_command,
            args=[self.isTurbo, COMMAND["TURBO_ON"]],
        )
        powerThread.daemon = True
        powerThread.start()

    def turbo_off(self):
        self.isTurbo.clear()

    def stop(self):
        self.isOn.clear()
        self.isTurbo.clear()

    def send_command(self, cmd):
        self.connect()
        # send command
        self.device.send(str.encode(cmd))
        # recieve respond data
        data = self.device.recv(BUFFER_SIZE)
        data_decode = data.decode()
        # process response
        if cmd == COMMAND["CHECK_STATUS"]:
            if data_decode == PUMP_STATUS["ON"]:
                return "ON"
            elif data_decode == PUMP_STATUS["OFF"]:
                return "OFF"
            elif data_decode == PUMP_STATUS["SHORT_CIRCUIT"]:
                return "SHORT_CIRCUIT"
            else:
                return "FAIL"
        self.disconnect()
        if data_decode == cmd:
            return "SUCCESS"
        raise RuntimeError("something went wrong, please check the pump status and try again")


if __name__ == "__main__":
    try:
        myPump = Pump()
        print("connect to server at {}:{}".format(myPump.ip, PORT))
        myPump.start()
        time.sleep(10)
        myPump.stop()
    except KeyboardInterrupt:
        myPump.disconnect()

    # response = myPump.send_command(COMMAND["CHECK_STATUS"])
    # print(response)
