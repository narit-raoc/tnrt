# -*- coding: utf-8 -*-
import threading
from Pump import Pump
from constant import COMMAND
import logging


from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Nc.Supplier import Supplier
from Acspy.Nc.Consumer import Consumer

# TCS packages
import LBandPumpMod
import LBandPumpMod__POA
import LBandPumpErrorImpl


class LBandPump(LBandPumpMod__POA.LBandPump, ACSComponent, ContainerServices, ComponentLifecycle):

    # Constructor
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.myPump = Pump()

    # Destructor
    def __del__(self):
        pass

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.

    def initialize(self):
        self.logger.logInfo("INITIALIZE")
        self.logger.logInfo("Creating Notification Consumer for channel %s: " % LBandPumpMod.NC_CHANNEL_NAME)
        try:
            self.nc_consumer = Consumer(LBandPumpMod.NC_CHANNEL_NAME)
            self.nc_consumer.addSubscription(LBandPumpMod.ExampleStruct, self.handler_example)
            self.nc_consumer.consumerReady()
        except:
            self.nc_consumer = None
            self.logger.logError("Failed to initialize NC Consumer: %s " % LBandPumpMod.NC_CHANNEL_NAME)

        # Create notification channel suppliers to publish combined data
        # DataMonitor is one consumer of this data.

        self.logger.logInfo("Creating Notification Supplier for channel %s " % LBandPumpMod.NC_CHANNEL_NAME)
        try:
            self.nc_supplier = Supplier(LBandPumpMod.NC_CHANNEL_NAME)
        except:
            self.nc_supplier = None
            self.logger.logError("Failed to initialize NC Supplier: %s " % LBandPumpMod.NC_CHANNEL_NAME)

    def execute(self):
        self.logger.logInfo("EXECUTE")

    def aboutToAbort(self):
        self.logger.logInfo("ABOUT_TO_ABORT")

    def cleanUp(self):
        self.logger.logInfo("CLEANUP")

        self.logger.logDebug("disconnect Supplier for channel: %s" % LBandPumpMod.NC_CHANNEL_NAME)
        if self.nc_supplier is not None:
            try:
                self.nc_supplier.disconnect()
            except:
                self.nc_supplier = None

        self.logger.logDebug("disconnect Consumer for channel: %s" % LBandPumpMod.NC_CHANNEL_NAME)
        if self.nc_consumer is not None:
            try:
                self.nc_consumer.disconnect()
            except:
                self.nc_consumer = None

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class

    def start(self):
        try:
            self.myPump.start()
            self.logger.logInfo("starting pump...")
        except ConnectionError:
            self.logger.logError("Fail to connect")
            raise LBandPumpErrorImpl.ConnectionFailedExImpl()
        except RuntimeError as err:
            self.logger.logError(err)
            raise LBandPumpErrorImpl.PumpErrorExImpl
        except Exception as err:
            self.logger.logError(err)

    def stop(self):
        try:
            self.myPump.stop()
            self.logger.logInfo("stop pump")
        except ConnectionError:
            self.logger.logError("Fail to connect")
            raise LBandPumpErrorImpl.ConnectionFailedExImpl()
        except RuntimeError as err:
            self.logger.logError(err)
            raise LBandPumpErrorImpl.PumpErrorExImpl
        except Exception as err:
            self.logger.logError(err)

    def turbo_on(self):
        try:
            self.myPump.turbo_on()
            self.logger.logInfo("turbo on")
        except ConnectionError:
            self.logger.logError("Fail to connect")
            raise LBandPumpErrorImpl.ConnectionFailedExImpl
        except RuntimeError as err:
            self.logger.logError(err)
            raise LBandPumpErrorImpl.PumpErrorExImpl
        except Exception as err:
            self.logger.logError(err)

    def turbo_off(self):
        try:
            self.myPump.turbo_off()
            self.logger.logInfo("turbo off")
        except ConnectionError:
            self.logger.logError("Fail to connect")
            raise LBandPumpErrorImpl.ConnectionFailedExImpl
        except RuntimeError as err:
            self.logger.logError(err)
            raise LBandPumpErrorImpl.PumpErrorExImpl
        except Exception as err:
            self.logger.logError(err)

    def check_status(self):
        try:
            result = self.myPump.send_command(COMMAND["CHECK_STATUS"])
            self.logger.logInfo("checking pump status")
            return result
        except ConnectionError:
            self.logger.logError("Fail to connect")
            raise LBandPumpErrorImpl.ConnectionFailedExImpl
        except RuntimeError as err:
            self.logger.logError(err)
        except Exception as err:
            self.logger.logError(err)

    # ------------------------------------------------------------------------------------
    # Internal functions not exposed in IDL

    def handler_example(self, data_struct):
        lock = threading.Lock()
        lock.acquire()
        self.logger.logDebug("received event: %s" % data_struct)
        self.current_nc_struct = data_struct
        lock.release()
