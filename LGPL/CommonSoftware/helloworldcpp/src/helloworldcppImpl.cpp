#include <helloworldcppImpl.h>

/**
	Constructor
*/
/* StatusThread */
void ClientThread::runLoop()
{
	hwp_p->checkServer();

}
void helloworldcppImpl::checkServer(){

  char response[100];
  memset(response, '\0', sizeof(response));

  try {
                ACS_SHORT_LOG(( LM_INFO, "Read socket client."));
                m_socketClient_p->Read(response, sizeof(response), 0);

        } catch (socketError::cannotWriteExImpl & ex) {
                ACS_SHORT_LOG((LM_ERROR,"WE CAN'T READ MESSAGE"))
        }

    ACS_SHORT_LOG(( LM_INFO, response));
		ACS_SHORT_LOG(( LM_INFO, "Get  Rx message."));


}

helloworldcppImpl::helloworldcppImpl(const ACE_CString& name,
		                             maci::ContainerServices* containerServices)
	: CharacteristicComponentImpl(name, containerServices)

	,m_socketClient_p(0)
  ,m_clientThread_p(0)

{
        ACS_TRACE("::helloworldcppImpl::helloworldcppImpl");
}

/**
	Destructor
*/
helloworldcppImpl::~helloworldcppImpl()
{
        ACS_TRACE("::helloworldcppImpl::~helloworldcppImpl");
}

/**
	ACS Components Functions
*/

/**
	Initialize -> Component Constructor
*/
void helloworldcppImpl::initialize(void)
        throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	ACS_TRACE("::helloworldcppImpl::initialize");
	try {
                m_socketClient_p = new Tcpsocketclient("127.0.1.1", 7000);
		m_socketClient_p->Connect();
    ACS_SHORT_LOG(( LM_INFO, "Connect socket ."));
        }
        catch (socketError::cannotOpenExImpl &ex) {
                ACS_SHORT_LOG ((LM_ERROR, "Cannot create all the sockets"));
	}
	catch (socketError::cannotConnectExImpl & ex) {
		ACS_SHORT_LOG ((LM_ERROR, "Cannot connect all the sockets"));
	}

  helloworldcppImpl * selfPtr = this;

	m_clientThread_p = getContainerServices()->getThreadManager()->create<ClientThread, helloworldcppImpl*>("ClientLoop", selfPtr);
        m_clientThread_p->resume();
}

void helloworldcppImpl::execute(void)
        throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
        ACS_TRACE("::helloworldcppImpl::execute");
}

/**
	Clean up -> Component destructor
*/
void helloworldcppImpl::cleanUp(void)
{
        ACS_TRACE("::helloworldcppImpl::cleanUp");
	delete m_socketClient_p;

  if (m_clientThread_p != 0)
        {
                getContainerServices()->getThreadManager()->destroy(m_clientThread_p);
        }

        m_clientThread_p = 0;

  if (m_clientThread_p != 0) {
                delete m_clientThread_p;
        }

}

void helloworldcppImpl::aboutToAbort(void)
{
        ACS_TRACE("::helloworldcppImpl::aboutToAbort");
}

/* ------------------ [ Functions ] ----------------- */

void helloworldcppImpl::foo(void){
	ACS_SHORT_LOG(( LM_INFO, "bar baz"));
}

void helloworldcppImpl::readCDB(void){
	maci::ContainerServices* pCS=getContainerServices();
        CDB::DAL_ptr dal_p = pCS->getCDB();
        ACE_CString fullname("alma/");
	fullname=fullname+pCS->getName();
	CDB::DAO_ptr dao_p = dal_p->get_DAO_Servant(fullname.c_str());


        ACS_SHORT_LOG(( LM_INFO, dao_p->get_string("stringExample")));
}

void helloworldcppImpl::readCDB2(void){

 maci::ContainerServices* pCS=getContainerServices();
        CDB::DAL_ptr dal_p = pCS->getCDB();
        ACE_CString fullname("alma/");
        fullname=fullname+pCS->getName();
        CDB::DAO_ptr dao_p = dal_p->get_DAO_Servant(fullname.c_str());

        ACS_SHORT_LOG(( LM_INFO, dao_p->get_string("stringExample2")));
}

void helloworldcppImpl::writeSocket(const char* message){
	char realMessage[100];
	memset(realMessage, '\0', sizeof(realMessage));
	strncpy(realMessage,message,sizeof(realMessage)-1);
	try {
		m_socketClient_p->Write(realMessage, sizeof(realMessage), 0);
    ACS_SHORT_LOG(( LM_INFO, "Write socket client."));
	} catch (socketError::cannotWriteExImpl & ex) {
		ACS_SHORT_LOG((LM_ERROR,"WE CAN'T SEND MESSAGE"))
	}
}

char* helloworldcppImpl::readSocket(void){

	char response[100];
	memset(response, '\0', sizeof(response));

	try {
                ACS_SHORT_LOG(( LM_INFO, "Read socket client."));
                m_socketClient_p->Read(response, sizeof(response), 0);

        } catch (socketError::cannotWriteExImpl & ex) {
                ACS_SHORT_LOG((LM_ERROR,"WE CAN'T READ MESSAGE"))
        }

	return response;
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(helloworldcppImpl)
