# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

import unittest
import astropy
import pandas

from ParameterBlocks import BackendParamsEDD, BackendParamsHoloFFT
import ScanMod

# NOTE: This module does not override the standard unittest.TestCase
# functions setUp and tearDown because we run this unttest from iPython
# interactive session.  nash module takes care of setup and cleanup when
# user starts and stops a session.  Therefore, these tests are not pure
# unit tests.  BUt the unittest framework is helpful to organize and automate.


class TestNash(unittest.TestCase):
    def _get_ra_dec_above_horizon_now(self):
        longitude = 99.216805
        latitude = 18.864348
        height = 403.625
        az_apy = 230 * units.deg
        el_apy = 50 * units.deg
        loc_apy = EarthLocation(
            EarthLocation.from_geodetic(
                lon=longitude * units.deg,
                lat=latitude * units.deg,
                height=height * units.m,
                ellipsoid="WGS84",
            )
        )
        log(
            "Choose a coordinate above the horizon now. (AZ, EL) = (%f, %f) [deg]"
            % (az_apy.value, el_apy.value),
            DEBUG,
        )
        coord_altaz = SkyCoord(
            frame="altaz",
            az=az_apy,
            alt=el_apy,
            obstime=Time.now(),
            location=loc_apy,
        )
        coord_icrs = coord_altaz.transform_to("icrs")
        log(
            "Calculated ICRS (RA, DEC) = (%f, %f) [deg]" % (coord_icrs.ra.deg, coord_icrs.dec.deg),
            DEBUG,
        )

        ra_catalog = coord_icrs.ra.deg  # deg
        dec_catalog = coord_icrs.dec.deg  # deg

        return (ra_catalog, dec_catalog)

    def setUp(self):

        # Set some variables that we want to re-use many times in unit tests
        # makes code look more organized in each test

        # ScheduleParams Required data
        self.project_id = "UnitTest"
        self.obs_id = "SS"

        # ScheduleParams Optional data
        self.scan_id = 1000
        self.priority = 0
        self.start_mjd = Time.now().mjd
        self.source_name = "fake name"
        self.line_name = "fake line"

        # Tracking Common
        self.user_pointing_correction_az = 0.0
        self.user_pointing_correction_el = 0.0
        self.elevation_min = 15
        self.elevation_max = 85
        self.north_crossing = True
        self.use_azel_tables = False
        self.max_tracking_errors = 6

        # Tracking RA, DEC
        self.pm_ra = 0  # mas/yr
        self.pm_dec = 0  # mas/yr
        self.parallax = 0  # arcsec
        self.radial_velocity = 0.0  # km/s
        self.send_icrs_to_acu = False

        # Tracking TLE
        self.line0 = "INTELSAT 22 (IS-22)     "
        self.line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
        self.line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"

        # Tracking Solar System
        self.planet = "sun"

        # ScantypeParams On Source
        self.duration = 3.0  # s

    # -------------------------------------
    # Test functions for Session (0xxx)
    # -------------------------------------
    def test0000_logger(self):
        testlog()

    # def test010_pause(self):
    #   log('>>> test function pause()')
    #   pause()

    def test0020_run(self):
        run("fake.py")

    def test0040_set_loglevel(self):
        log("test function set_loglevel")
        log("set log level to WARNING")
        set_loglevel(WARNING)
        testlog()
        log("set log level to DEBUG")
        set_loglevel(DEBUG)
        testlog()

    def test0050_waitrel(self):
        log("test function waitrel(3, True, 1)")
        waitrel(2, True, 1)

    def test0060_waitabs(self):
        start_mjd = (Time.now() + 2 * units.s).mjd
        log("test function waitabs({}, True, 1)".format(start_mjd))
        waitabs(start_mjd, True, 1)

    def test0070_check_IERS(self):
        session._check_IERS()

    def test0080_check_time_sync(self):
        session._check_time_sync()

    # -------------------------------------
    # Test functions for Antenna (1xxx)
    # -------------------------------------
    def test1100_is_remote(self):
        log("test function is_remote")
        result = is_remote()
        self.assertIsInstance(result, bool)

    def test1120_local(self):
        log("test function local")
        local()

    def test1110_remote(self):
        log("test function remote")
        remote()

    def test1120_get_site_location(self):
        log("test function get_site_location")
        result = get_site_location()
        log("site location spherical coordinates:")
        log(
            "longitude [deg]: %f, latitude [deg]: %f, height [m, WGS84]: %f"
            % (result.lon.deg, result.lat.deg, result.height.value)
        )
        self.assertIsInstance(result, astropy.coordinates.earth.EarthLocation)

    def test1130_activate1_az(self):
        log('test function activate("az")')
        activate("az")

    def test1130_activate2_el(self):
        log('test function activate("el")')
        activate("el")

    def test1130_activate3_m3(self):
        log('test function activate("m3")')
        activate("m3")

    def test1130_activate4_m3r(self):
        log('test function activate("m3r")')
        activate("M4A")

    def test1130_activate5_m4a(self):
        log('test function activate("m4a")')
        activate("m4a")

    def test1130_activate6_m4b(self):
        log('test function activate("M4b")')
        activate("m4b")

    def test1130_activate7_thu(self):
        log('test function activate("thu")')
        activate("thu")

    def test1130_activate8_grs(self):
        log('test function activate("grs")')
        activate("grs")

    def test1130_activate9_hxp(self):
        log('test function activate("hxp")')
        activate("hxp")

    def test1132_stop(self):
        log('test function stop("M4A")')
        stop("M4A")

    def test1134_deactivate1_az(self):
        log('test function deactivate("az")')
        deactivate("az")

    def test1134_deactivate2_el(self):
        log('test function deactivate("el")')
        deactivate("el")

    def test1134_deactivate3_m3(self):
        log('test function deactivate("m3")')
        deactivate("m3")

    def test1134_deactivate4_m3r(self):
        log('test function deactivate("m3r")')
        deactivate("M4A")

    def test1134_deactivate5_m4a(self):
        log('test function deactivate("m4a")')
        deactivate("m4a")

    def test1134_deactivate6_m4b(self):
        log('test function deactivate("M4b")')
        deactivate("m4b")

    def test1134_deactivate7_thu(self):
        log('test function deactivate("thu")')
        deactivate("thu")

    def test1134_deactivate8_grs(self):
        log('test function deactivate("grs")')
        deactivate("grs")

    def test1134_deactivate9_hxp(self):
        log('test function deactivate("hxp")')
        deactivate("hxp")

    def test1136_reset(self):
        log('test function reset("M4A")')
        reset("M4A")

    def test1137_boot(self):
        log("test function boot()")
        boot()

    def test1140_goto_azel_non_blocking(self):
        log("test function goto_azel(-30, 15, 2.0, 1.0, blocking=False)")
        result = goto_azel(-30, 15, 2.0, 1.0, blocking=False)
        self.assertIsInstance(result, float)

    def test1142_goto_azel_blocking(self):
        log("test function goto_azel(-30, 15, 2.0, 1.0)")
        result = goto_azel(-30, 15, 2.0, 1.0, blocking=True, update_period=1.0)
        self.assertIsInstance(result, float)

    def test1144_set_grs_full_nonblocking(self):
        log("test function set_grs(100, 5.0, False)")
        result = set_grs(200, 5.0, False)
        # If it raises an exception, unittest will show ERROR

    def test1145_set_grs_full_blocking(self):
        log("test function set_grs(150, 5.0, True)")
        result = set_grs(200, 5.0, True)
        # If it raises an exception, unittest will show ERROR

    def test1146_set_grs_required_params(self):
        log("test function set_grs(200)")
        result = set_grs(200)
        # If it raises an exception, unittest will show ERROR

    def test1148_set_grs_raise_error(self):
        log("test function set_grs(200, 'hello')")
        with self.assertRaises(ValueError):
            result = set_grs(200, "hello")

    def test1150_block_until_arrived(self):
        log("test function block_until_arrived(1.0)")
        block_until_arrived(1.0)

    def test1160_goto_platform_non_blocking(self):
        log("test function goto_platform(blocking=False)")
        goto_platform(blocking=False)

    def test1162_goto_platform_blocking(self):
        log("test function goto_platform(blocking=True, update_period=1.0)")
        goto_platform(blocking=True, update_period=1.0)

    def test1170_stow(self):
        log("test function stow()")
        stow()

    def test1172_unstow(self):
        log("test function unstow()")
        unstow()

    def test1180_get_vertex_status(self):
        log("test function get_vertex_status()")
        result = get_vertex_status()
        self.assertIsInstance(result, str)

    # def test1182_open_vertex(self):
    #   log('test function open_vertex()')
    #   open_vertex()

    # def test1184_close_vertex(self):
    #   log('test function close_vertex()')
    #   close_vertex()

    def test1186_set_pmodel(self):
        log("test function set_pmodel(10, 20, 30, 40, 50, 60, 70, 80, 90)")
        set_pmodel(10, 20, 30, 40, 50, 60, 70, 80, 90)

    def test1188_clear_pmodel(self):
        log("test function clear_pmodel()")
        clear_pmodel()

    def test1190_set_refraction(self):
        log("test function set_refraction(76.7, 5.7346, 2.2874)")
        set_refraction(76.7, 5.7346, 2.2874)

    def test1191_clear_refraction(self):
        log("test function clear_refraction()")
        clear_refraction()

    def test1192_get_status(self):
        log("test function get_status())")
        result = get_status()
        self.assertIsInstance(result, str)

    def test1193_set_axis_position_offsets(self):
        log("test function set_axis_position_offsets")
        set_axis_position_offsets(100, 200, 0)

    def test1194_clear_axis_position_offsets(self):
        log("test function clear_axis_position_offsets")
        clear_axis_position_offsets()

    def test1195_set_constant_focus_offsets(self):
        log("test function set_constant_focus_offsets")
        set_constant_focus_offsets(10, 20, 30, 0.1, 0.2, 0.3)

    def test1196_clear_constant_focus_offsets(self):
        log("test function clear_constant_focus_offsets")
        clear_constant_focus_offsets()

    def test1197_set_hxp_el_model1_astro(self):
        log("test function set_hxp_el_model('astro')")
        set_hxp_el_model("astro")

    def test1197_set_hxp_el_model2_geo(self):
        log("test function set_hxp_el_model('astro')")
        set_hxp_el_model("astro")

    def test1197_set_hxp_el_model3_zero(self):
        log("test function set_hxp_el_model('zero')")
        set_hxp_el_model("zero")

    def test1198_clear_hxp_el_model(self):
        log("test function clear_hxp_el_model")
        clear_hxp_el_model()

    # -------------------------------------
    # Test create all different types of scan object parameters (2xxx)
    # -------------------------------------
    # Schedule params (20xx)
    def test2000_ScheduleParams(self):
        # Create basic schedule params. Don't need optional data now.
        log("Create object of type ScheduleParams")
        schedule_params = ScheduleParams(self.project_id, self.obs_id)

    # Tracking params (21xx)
    def test2100_TrackingParamsHO(self):
        log("Create object of type TrackingParamsHO minimum parameters")
        az = 20
        el = 40
        tracking_params_HO_min = TrackingParamsHO(az, el)

        log("Create object of type TrackingParamsHO full parameters")
        tracking_params_HO_full = TrackingParamsHO(
            az,
            el,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

    def test2102_TrackingParamsEQ(self):
        log("Create object of type TrackingParamsEQ minimum parameters")
        (ra_catalog, dec_catalog) = self._get_ra_dec_above_horizon_now()
        tracking_params_EQ_min = TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            self.pm_ra,
            self.pm_dec,
            self.parallax,
            self.radial_velocity,
        )

        log("Create object of type TrackingParamsEQ minimum parameters,  send ICRS")
        (ra_catalog, dec_catalog) = self._get_ra_dec_above_horizon_now()
        send_icrs_to_acu = True
        tracking_params_EQ_min = TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            self.pm_ra,
            self.pm_dec,
            self.parallax,
            self.radial_velocity,
            send_icrs_to_acu,
        )

        log("Create object of type TrackingParamsEQ full parameters")
        tracking_params_EQ_full = TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            self.pm_ra,
            self.pm_dec,
            self.parallax,
            self.radial_velocity,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

    def test2104_TrackingParamsTLE(self):
        log("Create object of type TrackingParamsTLE minimum parameters")
        tracking_params_TLE_min = TrackingParamsTLE(self.line0, self.line1, self.line2)

        log("Create object of type TrackingParamsTLE full parameters")
        tracking_params_TLE_full = TrackingParamsTLE(
            self.line0,
            self.line1,
            self.line2,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

    def test2106_TrackingParamsSS(self):
        log("Create object of type TrackingParamsSS minimum parameters")
        tracking_params_SS_min = TrackingParamsSS(self.planet)

        log("Create object of type TrackingParamsSS full parameters")
        tracking_params_SS_full = TrackingParamsSS(
            self.planet,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

    def test2107_TrackingParamsSS_wrong_planet(self):
        log("Create object of type TrackingParamsSS with wrong planet")
        with self.assertRaises(ValueError):
            tracking_params_SS_min = TrackingParamsSS("wrong planet")

    # Scantype params (22xx)
    def test2200_ScantypeParamsCalColdload(self):
        log("Create object of type ScantypeParamsCalColdload")
        time_per_phase = 2.0

        cal_coord_dx = 0
        cal_coord_dy = -0
        cal_coord_duration = 1
        cal_coord_system = "HO"

        result = ScantypeParamsCalColdload(
            time_per_phase,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsCalColdload)

    def test2202_ScantypeParamsCalHotload(self):
        log("Create object of type ScantypeParamsCalHotload")
        time_per_phase = 2.0

        cal_coord_dx = 0
        cal_coord_dy = -0
        cal_coord_duration = 1
        cal_coord_system = "HO"

        result = ScantypeParamsCalHotload(
            time_per_phase,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsCalHotload)

    def test2204_ScantypeParamsCalNoise(self):
        log("Create object of type ScantypeParamsCalNoise")
        time_per_phase = 2.0

        cal_coord_dx = 0
        cal_coord_dy = -0
        cal_coord_duration = 1
        cal_coord_system = "HO"

        result = ScantypeParamsCalNoise(
            time_per_phase,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsCalNoise)

    def test2206_ScanTypeParamsCross(self):
        log("Create object of type ScanTypeParamsCross")
        arm_length = 7200  # arcsec
        time_per_arm = 3.0  # s
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True

        result = ScantypeParamsCross(arm_length, time_per_arm, win_min, win_max, double_cross)
        self.assertIsInstance(result, ScantypeParamsCross)

    def test2207_ScanTypeParamsCross_cal_coord(self):
        log("Create object of type ScanTypeParamsCross")
        arm_length = 7200  # arcsec
        time_per_arm = 3.0  # s
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True

        cal_coord_dx = 100
        cal_coord_dy = -100
        cal_coord_duration = 1
        cal_coord_system = "ho"

        result = ScantypeParamsCross(
            arm_length,
            time_per_arm,
            win_min,
            win_max,
            double_cross,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsCross)

    def test2208_ScanTypeParamsFivePoint(self):
        log("Create object of type ScanTypeParamsFivePoint")
        arm_length = 7200  # arcsec
        time_per_point = 2.0
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec

        result = ScantypeParamsFivePoint(arm_length, time_per_point, win_min, win_max)
        self.assertIsInstance(result, ScantypeParamsFivePoint)

    def test2209_ScanTypeParamsFivePoint_cal_coord(self):
        log("Create object of type ScanTypeParamsFivePoint")
        arm_length = 7200  # arcsec
        time_per_point = 2.0
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec

        cal_coord_dx = 100
        cal_coord_dy = -100
        cal_coord_duration = 1
        cal_coord_system = "ho"

        result = ScantypeParamsFivePoint(
            arm_length,
            time_per_point,
            win_min,
            win_max,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsFivePoint)

    def test2210_ScantypeParamsFocus(self):
        log("Create object of type ScantypeParamsFocus")
        axis = "z"
        start_pos = 0
        end_pos = 10
        velocity = 1.0
        result = ScantypeParamsFocus(axis, start_pos, end_pos, velocity)
        self.assertIsInstance(result, ScantypeParamsFocus)

    def test2212_ScantypeParamsMap(self):
        log("Create object of type ScantypeParamsMap")
        # ---------------------------------------
        # Scan type - single horizontal line scan, offset pattern AZ, EL
        # ---------------------------------------
        line_length = 7200
        time_per_line = 10
        nlines = 1
        spacing = 720
        axis = "x"
        zigzag = False
        coord_system = "HO"
        subscans_per_cal = 0
        time_cal0 = 30
        time_per_cal = 5

        result = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
            zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)
        
        self.assertIsInstance(result, ScantypeParamsMap)

    def test2214_ScantypeParamsManual(self):
        log("Create object of type ScantypeParamsManual")
        duration = 3.0
        result = ScantypeParamsManual(duration)
        self.assertIsInstance(result, ScantypeParamsManual)

    def test2216_ScanTypeParamsOnSource(self):
        log("Create object of type ScanTypeParamsOnSource")
        duration = 2.0
        result = ScantypeParamsOnSource(duration)
        self.assertIsInstance(result, ScantypeParamsOnSource)

    def test2217_ScanTypeParamsOnSource_cal_coord(self):
        log("Create object of type ScanTypeParamsOnSource")
        duration = 2.0
        cal_coord_dx = 100
        cal_coord_dy = -100
        cal_coord_duration = 1
        cal_coord_system = "ho"

        result = ScantypeParamsOnSource(
            duration,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsOnSource)

    def test2218_ScanTypeParamsRaster(self):
        log("Create object of type ScanTypeParamsRaster")
        xlen = 3600
        xstep = 720
        ylen = 3600
        ystep = 720
        time_per_point = 1
        zigzag = True
        primary_axis_enum = "X"
        coord_system_enum = "HO"

        result = ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
        )
        self.assertIsInstance(result, ScantypeParamsRaster)

    def test2219_ScanTypeParamsRaster_cal_coord(self):
        log("Create object of type ScanTypeParamsRaster")
        xlen = 3600
        xstep = 720
        ylen = 3600
        ystep = 720
        time_per_point = 1
        zigzag = True
        primary_axis_enum = "X"
        coord_system_enum = "EQ"

        cal_coord_dx = 100
        cal_coord_dy = -100
        cal_coord_duration = 1
        cal_coord_system = "EQ"

        result = ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        self.assertIsInstance(result, ScantypeParamsRaster)

    def test2220_ScanTypeParamsSkydip(self):
        log("Create object of type ScanTypeParamsSkydip")
        az = 20
        el_start = 15
        el_end = 80
        duration = 100
        result = ScantypeParamsSkydip(az, el_start, el_end, duration)
        self.assertIsInstance(result, ScantypeParamsSkydip)

    def test2222_ScantypeParamsTimeSync(self):
        log("Create object of type ScantypeParamsTimeSync")
        result = ScantypeParamsTimeSync(self.duration)
        self.assertIsInstance(result, ScantypeParamsTimeSync)

    # Frontend params (23xx)
    def test2300_FrontendParamsL_default(self):
        log("Create object of type FrontendParamsL default parameters")
        result = FrontendParamsL()
        self.assertIsInstance(result, FrontendParamsL)

    def test2302_FrontendParamsL_specify_params(self):
        log("Create object of type FrontendParamsL specific parameters")
        log("TODO. improve this test after we know parameters", WARNING)
        result = FrontendParamsL()
        self.assertIsInstance(result, FrontendParamsL)

    def test2310_FrontendParamsCX_default(self):
        log("Create object of type FrontendParamsCX default parameters")
        result = FrontendParamsCX()
        self.assertIsInstance(result, FrontendParamsCX)

    def test2312_FrontendParamsCX_specify_params(self):
        log("Create object of type FrontendParamsCX specific parameters")
        log("TODO. improve this test after we know parameters", WARNING)
        result = FrontendParamsCX()
        self.assertIsInstance(result, FrontendParamsCX)

    def test2320_FrontendParamsKu_default(self):
        log("Create object of type FrontendParamsKu default parameters")
        result = FrontendParamsKu()
        self.assertIsInstance(result, FrontendParamsKu)

    def test2322_FrontendParamsKu_specify_params(self):
        log("Create object of type FrontendParamsKu specific parameters")
        log("TODO. improve this test after we know parameters", WARNING)
        result = FrontendParamsKu()
        self.assertIsInstance(result, FrontendParamsKu)

    def test2330_FrontendParamsK_default(self):
        log("Create object of type FrontendParamsK default parameters")
        result = FrontendParamsK()
        self.assertIsInstance(result, FrontendParamsK)

    def test2332_FrontendParamsK_specify_params(self):
        log("Create object of type FrontendParamsK specific parameters")
        log("TODO. improve this test after we know parameters", WARNING)
        result = FrontendParamsK()
        self.assertIsInstance(result, FrontendParamsK)

    def test2340_FrontendParamsOptical_default(self):
        log("Create object of type FrontendParamsOptical default parameters")
        result = FrontendParamsOptical()
        self.assertIsInstance(result, FrontendParamsOptical)

    def test2342_FrontendParamsOptical_specify_params(self):
        log("Create object of type FrontendParamsOptical specific parameters")
        log("TODO. improve this test after we know parameters", WARNING)
        result = FrontendParamsOptical()
        self.assertIsInstance(result, FrontendParamsOptical)

    # Backend params (24xx)
    def test2400_BackendParamsEDD_default(self):
        log("Create object of type BackendParamsEDD default")
        result = BackendParamsEDD()
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2402_BackendParamsEDD_specify_invalid_provision_name(self):
        log("Create object of type BackendParamsEDD invalid config file")
        with self.assertRaises(KeyError):
            result = BackendParamsEDD("example_config_name")

    def test2404_BackendParamsEDD_specify_valid_provision(self):
        log("Create object of type BackendParamsEDD valid config file")
        result = BackendParamsEDD("TNRT_dualpol_spectrometer_L")
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2406_BackendParamsEDD_specify_invalid_kwargs(self):
        log("Create object of type BackendParamsEDD set invalid kwargs")
        with self.assertRaises(ValueError):
            result = BackendParamsEDD("TNRT_dualpol_spectrometer_L", example_kwarg=99)

    def test2408_BackendParamsEDD_specify_invalid_kwargs_value(self):
        log("Create object of type BackendParamsEDD set invalid kwargs")
        with self.assertRaises(ValueError):
            result = BackendParamsEDD("TNRT_dualpol_spectrometer_L", freq_res=15)

    def test2410_BackendParamsEDD_valid(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_dualpol_spectrometer_L", freq_res=10e3, integration_time=20)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)
    
    def test2411_BackendParamsEDD_valid(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_dualpol_spectrometer_K", freq_res=10e3, integration_time=20)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2412_BackendParamsEDD_valid(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_stokes_spectrometer_L", freq_res=10e3, integration_time=20)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2413_BackendParamsEDD_valid(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_stokes_spectrometer_K", freq_res=10e3, integration_time=20)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2414_BackendParamsEDD_valid(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_pulsar_L", project="hello", source_name="xyz", ra=10, dec=20)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2415_BackendParamsEDD_valid(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_VLBI_L", output_samplerate=16e4, f_lo=598e6)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)
    
    def test2416_BackendParamsEDD_mock(self):
        log("Create object of type BackendParamsEDD set valid kwargs")
        result = BackendParamsEDD("TNRT_dualpol_spectrometer_L", mock=True, freq_res=10e3, integration_time=20)
        log(result, DEBUG)
        self.assertIsInstance(result, BackendParamsEDD)

    def test2420_BackendParamsHoloFFT_default(self):
        log("Create object of type BackendParamsHoloFFT default")
        result = BackendParamsHoloFFT()
        self.assertIsInstance(result, BackendParamsHoloFFT)

    def test2422_BackendParamsHoloFFT_specify_config(self):
        log("Create object of type BackendParamsHoloFFT set params")
        freq_res = 1
        fft_length = 200
        mock = False
        result = BackendParamsHoloFFT(freq_res,fft_length, mock)
        self.assertIsInstance(result, BackendParamsHoloFFT)

    def test2425_BackendParamsHoloFFT_mock(self):
        log("Create object of type BackendParamsHoloFFT use Mock")
        freq_res = 1
        fft_length = 200
        mock = True
        result = BackendParamsHoloFFT(freq_res,fft_length, mock)
        self.assertIsInstance(result, BackendParamsHoloFFT)

    def test2430_BackendParamsHoloSDR_default(self):
        log("Create object of type BackendParamsHoloSDR default")
        result = BackendParamsHoloSDR()
        self.assertIsInstance(result, BackendParamsHoloSDR)

    def test2432_BackendParamsHoloSDR_specify_config(self):
        log("Create object of type BackendParamsHoloSDR config file")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsHoloSDR()
        self.assertIsInstance(result, BackendParamsHoloSDR)

    def test2434_BackendParamsHoloSDR_specify_kwargs(self):
        log("Create object of type BackendParamsHoloSDR set kwargs")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsHoloSDR()
        self.assertIsInstance(result, BackendParamsHoloSDR)

    def test2440_BackendParamsSKARAB_default(self):
        log("Create object of type BackendParamsSKARAB default")
        result = BackendParamsSKARAB()
        self.assertIsInstance(result, BackendParamsSKARAB)

    def test2442_BackendParamsSKARAB_specify_config(self):
        log("Create object of type BackendParamsSKARAB config file")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsSKARAB()
        self.assertIsInstance(result, BackendParamsSKARAB)

    def test2444_BackendParamsSKARAB_specify_kwargs(self):
        log("Create object of type BackendParamsSKARAB set kwargs")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsSKARAB()
        self.assertIsInstance(result, BackendParamsSKARAB)

    def test2450_BackendParamsOptical_default(self):
        log("Create object of type BackendParamsOptical default")
        result = BackendParamsOptical()
        self.assertIsInstance(result, BackendParamsOptical)

    def test2452_BackendParamsOptical_specify_config(self):
        log("Create object of type BackendParamsOptical config file")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsOptical()
        self.assertIsInstance(result, BackendParamsOptical)

    def test2454_BackendParamsOptical_specify_kwargs(self):
        log("Create object of type BackendParamsOptical set kwargs")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsOptical()
        self.assertIsInstance(result, BackendParamsOptical)

    def test2460_BackendParamsPowerMeter_default(self):
        log("Create object of type BackendParamsPowerMeter default")
        result = BackendParamsPowerMeter()
        self.assertIsInstance(result, BackendParamsPowerMeter)

    def test2462_BackendParamsPowerMeter_specify_config(self):
        log("Create object of type BackendParamsPowerMeter config file")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsPowerMeter()
        self.assertIsInstance(result, BackendParamsPowerMeter)

    def test2464_BackendParamsPowerMeter_specify_kwargs(self):
        log("Create object of type BackendParamsPowerMeter set kwargs")
        log("TODO. improve this test after we know parameters", WARNING)
        result = BackendParamsPowerMeter()
        self.assertIsInstance(result, BackendParamsPowerMeter)

    # Data params (25xx)
    def test2500_test5500_DataParamsDefault(self):
        log("Create object of type DataParams default - empty parameter")
        result = DataParams()
        log(result, DEBUG)
        self.assertIsInstance(result, DataParams)

    def test2502_DataParamsNone(self):
        pipeline_names = []
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2504_DataParamsMbfits(self):
        pipeline_names = ["mbfits"]
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2506_DataParamSpectrum(self):
        pipeline_names = ["spectrum_preview"]
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2508_DataParamsAtfits(self):
        pipeline_names = ["atfits"]
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2510_DataParamsGildas(self):
        pipeline_names = ["gildas"]
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2512_DataParamsMultiple(self):
        pipeline_names = ["mbfits", "spectrum_preview"]
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2514_DataParamsMultiple2(self):
        pipeline_names = ["spectrum_preview", "atfits"]
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)
    
    def test2516_DataParamsString(self):
        pipeline_names = "mbfits"
        log("pipeline_names: {}".format(pipeline_names))
        self.result = DataParams(pipeline_names)
        self.assertIsInstance(self.result, DataParams)

    def test2516_DataParams2params(self):
        pipeline_names1 = "mbfits"
        pipeline_names2 = "spectrum_preview"
        log("pipeline_names1: {}".format(pipeline_names1))
        log("pipeline_names2: {}".format(pipeline_names2))
        with self.assertRaises(TypeError):
            self.result = DataParams(pipeline_names1, pipeline_names2)
        
    # -------------------------------------
    # Test Scan queue functions (test3xxx_)
    # -------------------------------------

    def test3000_clear_queue(self):
        log("Clear the queue")
        clear_queue()

    def test3002_add_scan(self):
        # Create all types of parameter blocks.  Add various combinations to the queue

        # Schedule params
        schedule_params = ScheduleParams(self.project_id, self.obs_id)

        # Scantype params
        time_per_phase = 2.0
        cal_coord_dx = 0
        cal_coord_dy = -0
        cal_coord_duration = 1
        cal_coord_system = "HO"

        scantype_params_calcoldload = ScantypeParamsCalColdload(
            time_per_phase,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        scantype_params_calhotload = ScantypeParamsCalHotload(
            time_per_phase,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )
        scantype_params_calnoise = ScantypeParamsCalNoise(
            time_per_phase,
            cal_coord_dx,
            cal_coord_dy,
            cal_coord_duration,
            cal_coord_system,
        )

        arm_length = 7200  # arcsec
        time_per_arm = 3.0  # s
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True
        scantype_params_cross = ScantypeParamsCross(
            arm_length, time_per_arm, win_min, win_max, double_cross
        )

        arm_length = 7200  # arcsec
        time_per_point = 2.0
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec
        scantype_params_fivepoint = ScantypeParamsFivePoint(
            arm_length, time_per_point, win_min, win_max
        )

        axis = "z"
        start_pos = 0
        end_pos = 10
        velocity = 1.0
        scantype_params_focus = ScantypeParamsFocus(axis, start_pos, end_pos, velocity)

        line_length = 7200
        time_per_line = 10
        nlines = 1
        spacing = 720
        axis = "x"
        zigzag = False
        coord_system = "HO"
        subscans_per_cal = 0
        time_cal0 = 30
        time_per_cal = 5

        scantype_params_map = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
            zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)
        

        duration = 2.0
        scantype_params_manual = ScantypeParamsManual(duration)
        scantype_params_onsource = ScantypeParamsOnSource(duration)

        xlen = 3600
        xstep = 1200
        ylen = 3600
        ystep = 1200
        time_per_point = 1
        zigzag = True
        primary_axis_enum = "X"
        coord_system_enum = "HO"
        scantype_params_raster = ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
        )

        az = 20
        el_start = 20
        el_end = 40
        duration = 20
        scantype_params_skydip = ScantypeParamsSkydip(az, el_start, el_end, duration)

        scantype_params_timesync = ScantypeParamsTimeSync(duration)

        # Tracking params
        az = 10
        el = 40
        tracking_params_HO_min = TrackingParamsHO(az, el)
        tracking_params_HO_full = TrackingParamsHO(
            az,
            el,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

        (ra_catalog, dec_catalog) = self._get_ra_dec_above_horizon_now()
        tracking_params_EQ_min = TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            self.pm_ra,
            self.pm_dec,
            self.parallax,
            self.radial_velocity,
        )
        tracking_params_EQ_full = TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            self.pm_ra,
            self.pm_dec,
            self.parallax,
            self.radial_velocity,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

        tracking_params_TLE_min = TrackingParamsTLE(self.line0, self.line1, self.line2)
        tracking_params_TLE_full = TrackingParamsTLE(
            self.line0,
            self.line1,
            self.line2,
            user_pointing_correction_az=self.user_pointing_correction_az,
            user_pointing_correction_el=self.user_pointing_correction_el,
            elevation_min=self.elevation_min,
            elevation_max=self.elevation_max,
            north_crossing=self.north_crossing,
            use_azel_tables=self.use_azel_tables,
            max_tracking_errors=self.max_tracking_errors,
        )

        # Frontend Params
        frontend_params_L = FrontendParamsL()
        frontend_params_Ku = FrontendParamsKu()

        # Backend Params
        backend_params_edd_default = BackendParamsEDD()
        backend_params_edd_specify = BackendParamsEDD(
            "TNRT_dualpol_spectrometer_L", freq_res=4096, integration_time=2.0
        )

        backend_params_holofft_default = BackendParamsHoloFFT()
        backend_params_holofft = BackendParamsHoloFFT(
            integration_time=1, num_steps=2, fft_length=800
        )
        backend_params_holofft_mock = BackendParamsHoloFFT(
            integration_time=1, num_steps=2, fft_length=400, mock=True
        )

        data_params_none = DataParams([])
        data_params_spectrometer = DataParams(["mbfits", "spectrum_preview"])
        data_params_holo = DataParams(["atfits"])
        data_params_string_nolist = DataParams("mbfits")

        log("Add one of each type of scantype params")
        log("select a reasonable combination of other parameter blocks for test")
        log(
            "This test does not exercise all combinations of all parameter types, but it confirms all of the object factory methods called from add_scan",
            WARNING,
        )

        add_scan(
            schedule_params,
            scantype_params_calcoldload,
            tracking_params_EQ_min,
            # frontend_params_L,
            # backend_params_edd_default,
        )
        add_scan(
            schedule_params,
            scantype_params_calhotload,
            tracking_params_EQ_full,
            # frontend_params_L,
            # backend_params_edd_default,
        )
        add_scan(
            schedule_params,
            scantype_params_calnoise,
            tracking_params_EQ_min,
            # frontend_params_L,
            # backend_params_edd_default,
        )
        add_scan(
            schedule_params,
            scantype_params_cross,
            tracking_params_TLE_min,
            # frontend_params_L,
            # backend_params_edd_default,
        )
        add_scan(
            schedule_params,
            scantype_params_fivepoint,
            tracking_params_TLE_full,
            # frontend_params_L,
            # backend_params_edd_default,
        )

        # TODO for K band to focus hexapod fix later
        # add_scan(
        #     schedule_params, scantype_params_focus, tracking_params_EQ_min, None, None
        # )

        # TODO add VLBI scan to the queue after the class is implemented
        # add_scan(schedule_params, scantype_params_holography, tracking_params_TLE_full, frontend_params_Ku, backend_params_holofft)
        add_scan(schedule_params,
                 scantype_params_manual,
                 tracking_params_HO_min)
        
        add_scan(
            schedule_params,
            scantype_params_map,
            tracking_params_HO_min,
            # frontend_params_L,
            # backend_params_edd_default,
            data_params=data_params_holo
        )
        add_scan(
            schedule_params,
            scantype_params_onsource,
            tracking_params_EQ_min,
            # frontend_params_L,
            # backend_params_edd_default,
        )
        add_scan(
            schedule_params,
            scantype_params_raster,
            tracking_params_EQ_full,
            # frontend_params_L,
            # backend_params_edd_default,
            data_params=data_params_spectrometer
        )
        add_scan(
            schedule_params,
            scantype_params_skydip,
            None,
            # frontend_params_L,
            # backend_params_edd_default,
        )
        add_scan(schedule_params, scantype_params_timesync)

    def test3100_run_queue_blocking(self):
        log("test run_queue_blocking")
        results = run_queue(blocking=True)
        log(results)

    # def test3102_run_queue_non_blocking_cancel_scheduled(self):
    #     log("test run_queue_non_blocking cancel scheduled (not yet running")

    #     # Clear the old queue
    #     clear_queue()

    #     # Schedule parameters
    #     project_id = "no_projid"
    #     obs_id = "no_obsid"
    #     scan_id = 265  # Set this so we can get the result easier

    #     # To test stop_queue when scan is scheduled but not yet run.
    #     start_mjd = (Time.now() + 30 * units.second).mjd

    #     # Scan type parameters.
    #     # Typically, a manual scan we must stop manually by command stop_queue().
    #     # We have option to set the time for the scan to automatically stop.
    #     # If we don't set the duration, it will automatically stop after 60 minutes
    #     # (file size grows approximately 1 MB per minute)
    #     duration = 30  # seconds

    #     schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, scan_id=scan_id)
    #     scantype_params = ScantypeParamsManual(duration)

    #     # Create the scan and add it to the queue
    #     add_scan(schedule_params, scantype_params)

    #     # Run all scans in the queue.  Results are emtpy if we run non-blocking
    #     results = run_queue(blocking=False)

    #     # Do something interesting
    #     goto_azel(0, 10)

    #     # Wait a 10 seconds for ACU to move.  Scan duration is actually 30, so we interrupt the scan.
    #     waitrel(10)
    #     results = stop_queue()

    # def test3104_run_queue_non_blocking_cancel_running(self):
    #     log("test run_queue_non_blocking cancel scan running")

    #     # Clear the old queue
    #     clear_queue()

    #     # Schedule parameters
    #     project_id = "no_projid"
    #     obs_id = "no_obsid"
    #     scan_id = 266  # Set this so we can get the result easier

    #     # To test stop_queue when scan is scheduled but not yet run.
    #     start_mjd = (Time.now() + 0 * units.second).mjd

    #     # Scan type parameters.
    #     # Typically, a manual scan we must stop manually by command stop_queue().
    #     # We have option to set the time for the scan to automatically stop.
    #     # If we don't set the duration, it will automatically stop after 60 minutes
    #     # (file size grows approximately 1 MB per minute)
    #     duration = 30  # seconds

    #     schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, scan_id=scan_id)
    #     scantype_params = ScantypeParamsManual(duration)

    #     # Create the scan and add it to the queue
    #     add_scan(schedule_params, scantype_params)

    #     # Run all scans in the queue.  Results are emtpy if we run non-blocking
    #     results = run_queue(blocking=False)

    #     # Do something interesting
    #     goto_azel(0, 10)

    #     # Wait a 10 seconds for ACU to move.  Scan duration is actually 30, so we interrupt the scan.
    #     waitrel(10)
    #     results = stop_queue()

    def test3200_get_scan_id_lastrun(self):
        log("test get_scan_id_lastrun")
        result = get_scan_id_lastrun()
        self.assertIsInstance(result, int)

    def test3202_get_tsys(self):
        log("test get_tsys")
        result = get_tsys()
        self.assertIsInstance(result, float)

    def test3204_get_focusfit(self):
        log("test get_focusfit")
        result = get_focusfit()
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 6)

    def test3206_get_az_el_vtopo(self):
        log("test get_az_el_vtopo")
        schedule_params = ScheduleParams("ABC-XYZ", "unittest_observer", start_mjd=59200)
        tracking_params = TrackingParamsEQ(20, 45, 0, 0, 0, 1)
        result = get_az_el_vtopo(schedule_params, tracking_params)
        (az, el, vtopo) = get_az_el_vtopo(schedule_params, tracking_params)
        self.assertIsInstance(result, tuple)
        self.assertEqual(len(result), 3)

    # -------------------------------------
    # Test Catalog functions
    # -------------------------------------
    def test4000_get_catalog(self):
        log("test get_catalog() using default example file")
        result = get_catalog()
        self.assertIsInstance(result, pandas.core.frame.DataFrame)

    def test4002_get_TLE(self):
        log("test get_TLE(38098)")
        result = get_TLE(38098)
        self.assertIsInstance(result, ScanMod.TLEData)

        log('test get_TLE() using default satelite (INTELSAT 22)')
        result = get_TLE()
        self.assertIsInstance(result,ScanMod.TLEData)

        log('test get_TLE() using wrong id')
        result = get_TLE(2)
        self.assertEqual(result,None)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestNash)
    unittest.TextTestRunner(verbosity=4).run(suite)
