import socket

SPECTROMETER_FREQ_RES_DEFAULT = 122070.3125 # 1 GHz / 8192 channels
SPECTROMETER_FREQ_RES_MIN = 1908.0
SPECTROMETER_FREQ_RES_MAX = 976563.0
SPECTROMETER_INTEGRATION_TIME_DEFAULT = 1.0
SPECTROMETER_SAMPLING_RATE_DEFAULT = 4e9
SPECTROMETER_REF_CHANNEL_DEFAULT = 0
SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT = 16384 # 2**14
SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT =  65536 # 2**16

PULSAR_DEFAULT_PROJECT = "UNDEF_PROJECT"
PULSAR_DEFAULT_SOURCE_NAME = "UNDEF_SOURCE"
PULSAR_DEFAULT_RA = 0
PULSAR_DEFAULT_DEC = 0

VLBI_DEFAULT_DDC_OUTPUT_SAMPLERATE = 160000000
VLBI_DEFAULT_F_LO = [238000000]

preset_config_name_setting = {
    "TNRT_dualpol_spectrometer": 
        {
        'user_script_parameters': 
            {
            "freq_res" : SPECTROMETER_FREQ_RES_DEFAULT,
            "integration_time" : SPECTROMETER_INTEGRATION_TIME_DEFAULT
            },
        'edd_config_parameters' : 
            {
            "fft_length" : SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT,
            "naccumulate" : SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT
            },
        'predecimation_factor': 2,
        'nusefeed': 4,
        'product_id_list': ["gated_spectrometer_0", "gated_spectrometer_1"],
        },
    "TNRT_dualpol_spectrometer_L": 
        {
        'user_script_parameters': 
            {
            "freq_res" : SPECTROMETER_FREQ_RES_DEFAULT,
            "integration_time" : SPECTROMETER_INTEGRATION_TIME_DEFAULT
            },
        'edd_config_parameters' : 
            {
            "fft_length" : SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT,
            "naccumulate" : SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT
            },
        'predecimation_factor': 2,
        'nusefeed': 4,
        'product_id_list': ["gated_spectrometer_0", "gated_spectrometer_1"],
        },
    "TNRT_dualpol_spectrometer_K":
        {
        'user_script_parameters': 
            {
            "freq_res" : SPECTROMETER_FREQ_RES_DEFAULT,
            "integration_time" : SPECTROMETER_INTEGRATION_TIME_DEFAULT
            },
        'edd_config_parameters' : 
            {
            "fft_length" : SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT,
            "naccumulate" : SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT
            },
        'predecimation_factor': 1,
        'nusefeed': 4,
        'product_id_list': ["gated_spectrometer_0", "gated_spectrometer_1"],
        },
    "TNRT_stokes_spectrometer":
        {
        'user_script_parameters': 
            {
            "freq_res" : SPECTROMETER_FREQ_RES_DEFAULT, 
            "integration_time" : SPECTROMETER_INTEGRATION_TIME_DEFAULT
            },
        'edd_config_parameters' : 
            {
            "fft_length" : SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT,
            "naccumulate" : SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT
            },
        'predecimation_factor': 2,
        'nusefeed': 8,
        'product_id_list': ["gated_stokes_spectrometer"],
        },
    "TNRT_stokes_spectrometer_L": 
        {
        'user_script_parameters': 
            {
            "freq_res" : SPECTROMETER_FREQ_RES_DEFAULT,
            "integration_time" : SPECTROMETER_INTEGRATION_TIME_DEFAULT
            },
        'edd_config_parameters' : 
            {
            "fft_length" : SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT,
            "naccumulate" : SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT
            },
        'predecimation_factor': 2,
        'nusefeed': 8,
        'product_id_list': ["gated_stokes_spectrometer"],
        },
    "TNRT_stokes_spectrometer_K": 
        {
        'user_script_parameters': 
            {
            "freq_res" : SPECTROMETER_FREQ_RES_DEFAULT,
            "integration_time" : SPECTROMETER_INTEGRATION_TIME_DEFAULT
            },
        'edd_config_parameters' : 
            {
            "fft_length" : SPECTROMETER_EDD_CONFIG_FFT_LENGTH_DEFAULT,
            "naccumulate" : SPECTROMETER_EDD_CONFIG_NACCUMULATE_DEFAULT
            },
        'predecimation_factor': 1,
        'nusefeed': 8,
        'product_id_list': ["gated_stokes_spectrometer"],
        },
    "TNRT_pulsar": {
        'user_script_parameters': 
            {
            "project" : PULSAR_DEFAULT_PROJECT, 
            "source_name" : PULSAR_DEFAULT_SOURCE_NAME, 
            "ra" : PULSAR_DEFAULT_RA, 
            "dec" : PULSAR_DEFAULT_DEC,
            },
        'edd_config_parameters' : {},
        'product_id_list': ["timing1", "timing2"],
        },
    "TNRT_pulsar_L": {
        'user_script_parameters': 
            {
            "project" : PULSAR_DEFAULT_PROJECT, 
            "source_name" : PULSAR_DEFAULT_SOURCE_NAME, 
            "ra" : PULSAR_DEFAULT_RA, 
            "dec" : PULSAR_DEFAULT_DEC,
            },
        'edd_config_parameters' : {},
        'product_id_list': ["timing1", "timing2"],
        },
    "TNRT_pulsar_search_dryrun": {
        'user_script_parameters': 
            {
            "project" : PULSAR_DEFAULT_PROJECT, 
            "source_name" : PULSAR_DEFAULT_SOURCE_NAME, 
            "ra" : PULSAR_DEFAULT_RA, 
            "dec" : PULSAR_DEFAULT_DEC,
            },
        'edd_config_parameters' : {},
        'product_id_list': ["timing1", "timing2"],
        },
    "TNRT_VLBI_L": {
        'user_script_parameters':
            {
            "output_samplerate" : VLBI_DEFAULT_DDC_OUTPUT_SAMPLERATE,
            "f_lo" : VLBI_DEFAULT_F_LO
            },
        'edd_config_parameters':
            {
            "output_samplerate" : VLBI_DEFAULT_DDC_OUTPUT_SAMPLERATE,
            "f_lo" : VLBI_DEFAULT_F_LO
            },
        'product_id_list': ["ddc_processor_1", "ddc_processor_2"],
        },
    "TEST_jp_pulsar_L": {
        'user_script_parameters': 
            {
            "project" : PULSAR_DEFAULT_PROJECT, 
            "source_name" : PULSAR_DEFAULT_SOURCE_NAME, 
            "ra" : PULSAR_DEFAULT_RA, 
            "dec" : PULSAR_DEFAULT_DEC,
            },
        'edd_config_parameters' : {},
        'product_id_list': ["timing1", "timing2"],
        },
    "TEST_jp_pulsar_L_R": {
        'user_script_parameters': 
            {
            "project" : PULSAR_DEFAULT_PROJECT, 
            "source_name" : PULSAR_DEFAULT_SOURCE_NAME, 
            "ra" : PULSAR_DEFAULT_RA, 
            "dec" : PULSAR_DEFAULT_DEC,
            },
        'edd_config_parameters' : {},
        'product_id_list': ["timing1", "timing2"],
        },
}

edd_redis_host = socket.gethostbyname("edd_redis")
edd_redis_port = int(6379)
