from astropy import units as u
from astropy.coordinates import SkyCoord


c = SkyCoord(ra=10.625*u.degree, dec=41.2*u.degree, frame='icrs')
print(c)
c = SkyCoord(10.625, 41.2, frame='icrs', unit='deg')
print(c)
c = SkyCoord('00h42m30s', '+41d12m00s', frame='icrs')
print(c)
c = SkyCoord('00h42.5m', '+41d12m')
print(c)
c = SkyCoord('00 42 30 +41 12 00', unit=(u.hourangle, u.deg))
print(c)
# c = SkyCoord('00:42.5 +41:12', unit=(u.hourangle, u.deg))

print(c) 
print(c.ra) 
print(c.ra.hour) 
print(c.ra.hms) 
print(c.dec)
print(c.dec.degree)
print(c.dec.radian)  

print(c.ra.degree)



