import pandas 
from astroquery.simbad import Simbad
from astroquery.vizier import Vizier
from astropy import units as u
from astropy.coordinates import SkyCoord

def test_csv():
    myFile = pd.read_csv('./catalog1.csv', sep=',')

    print(type(myFile))
    print(myFile)

    a = myFile.values.tolist()

    source = a[0][0]

    print(type(source))

    result_table = Simbad.query_object("M1")
    print(result_table)

def get_catalog(self, catalog_name):
    
    try:
        if(catalog_name == ""):
            catalog_name = "/home/user1/tnrt/LGPL/Tools/nash/test/catalog.csv"

        catalog = pandas.read_csv(catalog_name, sep=',')
        catalog_list = catalog.values.tolist()

        source_list = []
        for source in catalog_list:
            source_list.append(source[0])
        
        # convert hmr to degree to catalog_degree
        # catalog_degree = catalog.values.tolist()
        print(type(catalog_list))
        catalog_degree = catalog.values.tolist()
  
        row=0
        for colum in catalog_degree:
            # sourcename, system, Epoch, RA, Dec, LSR, !Commnet
            ra_decx = colum[3]+" "+colum[4]
            print(colum)
            coordinates = SkyCoord(ra_decx, unit=(u.hourangle, u.deg))
            catalog_degree[row][3] = str(coordinates.ra.degree)
            catalog_degree[row][4] = str(coordinates.dec.degree)
            row += 1

        # for colum in catalog_degree:
        #     # sourcename, system, Epoch, RA, Dec, LSR, !Commnet
        #     print(colum)
        
        print("-------")
        print(catalog_list)
        print("-------")
        print(catalog_degree)
        print("-------")
        print(catalog)


        return(catalog)
        
    except Exception as error:
        print('Failed to get catalog : %s. ' % error)

class Catalog:

    def get_catalog(self, catalog_name):
        
        try:
            if(catalog_name == ""):
                catalog_name = "/home/user1/tnrt/LGPL/Tools/nash/test/catalog.csv"

            catalog = pandas.read_csv(catalog_name, sep=',')
            catalog_list = catalog.values.tolist()

            source_list = []
            for source in catalog_list:
                source_list.append(source[0])
            
            # convert hmr to degree to catalog_degree
            catalog_degree = catalog_list
            
            for colum in catalog_degree:
                # sourcename, system, Epoch, RA, Dec, LSR, !Commnet
                ra_decx = colum[3]+" "+colum[4]
                
                print(colum)


            return(catalog)
            
        except Exception as error:
            print('Failed to get catalog : %s. ' % error)

x = get_catalog("","")

print(x)