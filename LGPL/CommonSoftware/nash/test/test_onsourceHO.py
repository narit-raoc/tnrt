delay_until_scheduled_start = 0

project_id = "no_projid"
obs_id = "no_obsid"
scan_id = 8
priority = 0
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd

source_name = "no_source"
line_name = "no_line"

duration = 5.0

az = 0
el = 10
tracking_offset_az = 0.0
tracking_offset_el = 0.0

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
)
scantype_params = ScantypeParamsOnSource(duration)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params, tracking_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue(blocking=True)
# results = run_queue(blocking=False)

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
