# Create and add a ScanOnSourceTLE to the queue

# Schedule the scan to start after 2 minutes (120 seconds).  This is to confirm that the scheduler
# works afer antenna has already slewed to start AZ/EL position and wait for scheduled start time.

# In real operations, the operator will most likely create a timestamp in ISO format.
# So, the control software must accept timee string in the ISO format: 2020-03-22T10:06:57.067

delay_until_scheduled_start = 30

project_id = "no_projid"
obs_id = "no_obsid"
scan_id = 8
priority = 0
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "no_source"
line_name = "no_line"

az = 20
el = 40
tracking_offset_az = 0.0
tracking_offset_el = 0.0

xlen = 3600
xstep = 720
ylen = 3600
ystep = 720
time_per_point = 1
zigzag = True
primary_axis_enum = "X"
coord_system_enum = "HO"

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
)
scantype_params = ScantypeParamsRaster(
    xlen,
    xstep,
    ylen,
    ystep,
    time_per_point,
    zigzag,
    primary_axis_enum,
    coord_system_enum,
)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

add_scan(schedule_params, scantype_params, tracking_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
