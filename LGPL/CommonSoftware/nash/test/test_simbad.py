from astroquery.simbad import Simbad
import astropy.coordinates as coord

# Simbad.add_votable_fields('typed_id') 				
# result_table = Simbad.query_objects(["Orion", "M3", "M4"])
# result_table = Simbad.query_region(coord.SkyCoord("01h08m38.771s +01h35m00.317s", frame='icrs'), radius='1d0m0s')
result_table = Simbad.query_object("iau J0116-1136")
# result_table = Simbad.query_object("iau 0108+0135")
					
print(result_table)


