delay_until_scheduled_start = 30

project_id = "no_projid"
obs_id = "no_obsid"
scan_id = 8
priority = 0
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "no_source"
line_name = "no_line"

az = 30 * units.deg
el = 20 * units.deg
logger.debug(
    "Choose a coordinate above the horizon now. (AZ, EL) = (%f, %f) [deg]"
    % (az.value, el.value)
)
coord_altaz = SkyCoord(
    frame="altaz", az=az, alt=el, obstime=Time.now(), location=get_site_location()
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug(
    "Calculated ICRS (RA, DEC) = (%f, %f) [deg]"
    % (coord_icrs.ra.deg, coord_icrs.dec.deg)
)

ra_catalog = coord_icrs.ra.deg
dec_catalog = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 10.0
tracking_offset_az = 0.0
tracking_offset_el = 0.0

arm_length = 7200
time_per_arm = 10.0
win_min = -1000
win_max = 1000
double_cross = True

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
)
scantype_params = ScantypeParamsCross(
    arm_length, time_per_arm, win_min, win_max, double_cross
)
tracking_params = TrackingParamsEQ(
    ra_catalog,
    dec_catalog,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    tracking_offset_az,
    tracking_offset_el,
)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params, tracking_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
