from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.coordinates import EarthLocation
from astropy.coordinates import AltAz
from astropy.time import Time
print('Download data from Astropy IERS if cache data is old or this is the first time ....')
print('Create an EarthLocation object')
loc = EarthLocation(EarthLocation.from_geodetic(lon=0 * u.deg,
										lat=0 * u.deg,
										height=0 * u.m,
										ellipsoid='WGS84'))

print('Create a Time object')
time_now = Time(Time.now(), format='jd', scale='ut1')

# Make an Astropy coordinate data structure from AZ, EL measurement data
# Astropy uses parameter "alt" (Altitude) same as we write EL (Elevation)
print('Create a SkyCoord object')
az_el = SkyCoord(az=0 * u.deg,
						alt=90 * u.deg,
						frame='altaz', obstime=time_now, location=loc)
print('az_el %s' % az_el)

print('>>> TEST: translate {AZ, EL} to {RA ,DEC}')
ra_dec = az_el.transform_to('icrs')
print('ra_dec %s' % ra_dec)
