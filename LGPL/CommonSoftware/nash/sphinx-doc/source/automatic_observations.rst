.. currentmodule:: nash

Automatic Observations
======================
The workflow to do automatic observations:

#. [In the text editor of your choice] Open an empty python script file or start from one of the examples that uses `nash` module

#. [In the python script] Construct blocks of parameters to define the behavior of the Scan.  Choose 1 type of parameter block from each column in the table below.  All scan types require `ScheduleParams` and `ScanTypeParams`.  Parameter blocks from the remaining columns may or may not be required -- depends on the specific ScanTypeParams that was selected.

.. list-table::
   :widths: 60 60 60 60 60 60
   :header-rows: 1

   * - `ScheduleParams`
     - `ScantypeParams`
     - `TrackingParams`
     - `FrontendParams`
     - `BackendParams`
     - `DataParams`
   * - :py:class:`~ParameterBlocks.ScheduleParams`
     - :py:class:`~ParameterBlocks.ScantypeParamsCalColdload`
     - :py:class:`~ParameterBlocks.TrackingParamsEQ`
     - :py:class:`~ParameterBlocks.FrontendParamsL`
     - :py:class:`~ParameterBlocks.BackendParamsEDD`
     - :py:class:`~ParameterBlocks.DataParams`
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsCalHotload`
     - :py:class:`~ParameterBlocks.TrackingParamsHO`
     - :py:class:`~ParameterBlocks.FrontendParamsCX`
     - :py:class:`~ParameterBlocks.BackendParamsHoloFFT`
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsCalNoise`
     - :py:class:`~ParameterBlocks.TrackingParamsTLE`
     - :py:class:`~ParameterBlocks.FrontendParamsKu`
     - :py:class:`~ParameterBlocks.BackendParamsHoloSDR`
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsCross`
     - :py:class:`~ParameterBlocks.TrackingParamsSS`
     - :py:class:`~ParameterBlocks.FrontendParamsK`
     - :py:class:`~ParameterBlocks.BackendParamsSKARAB`
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsFivePoint`
     -
     - :py:class:`~ParameterBlocks.FrontendParamsOptical`
     - :py:class:`~ParameterBlocks.BackendParamsOptical`
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsFocus`
     -
     -
     - :py:class:`~ParameterBlocks.BackendParamsPowerMeter`
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsMap`
     -
     -
     -
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsManual`
     -
     -
     -
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsOnSource`
     -
     -
     -
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsRaster`
     -
     -
     -
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsSkydip`
     - 
     -
     -
     -
   * -
     - :py:class:`~ParameterBlocks.ScantypeParamsTimeSync`
     -
     -
     -
     -
   * -
     -
     -
     -
     -
     -

#. [In your python script] Use the function :py:func:`~Scan.add_scan` to send all of the parameter blocks (`ScheduleParams`, `ScantypeParams`, `TrackingParams`, `FrontendParams`, `BackendParams`) to the Scan controller who will construct the scan and add it to the queue.  See API documentation for function :py:func:`~Scan.add_scan` for details about each type of parameter. 

#. [In your python script] Create more parameter blocks and add more scans to the queue using function :py:func:`~Scan.add_scan` as needed

#. [In your python script] Run the queue until all scans are completed or queue is canceled

   * To run the queue: use function :py:func:`~Scan.run_queue()`

   * To cancel the queue and stop: use keyboard CTRL+C in the `nash` window

#. [In the iPython interactive session] To run your script: use function :py:func:`~Session.run()` in the interactive session window.

Schedule Parameters
-------------------
.. autoclass:: ParameterBlocks.ScheduleParams
   :members:

Scan Type Parameters
--------------------
Calibrate: Cold Load
^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsCalColdload
   :members:

Calibrate: Hot Load
^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsCalHotload
   :members:

Calibrate: Noise Source
^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsCalNoise
   :members:

Cross (OTF)
^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsCross
   :members:

Five Point
^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsFivePoint
   :members:

Focus M2 / HXP
^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsFocus
   :members:

Map (OTF)
^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsMap
   :members:

Manual
^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsManual
   :members:

On Source
^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsOnSource
   :members:

Raster
^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsRaster
   :members:

Skydip
^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsSkydip
   :members:

Time Sync
^^^^^^^^^
.. autoclass:: ParameterBlocks.ScantypeParamsTimeSync
   :members:

Tracking Parameters
-------------------
Equatorial (Right Ascension, Declination)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.TrackingParamsEQ
   :members:

Horizontal (Azimuth, Elevation) / (Altitude, Azimuth)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.TrackingParamsHO
   :members:

Solar System Body (Planets and Earth's moon)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.TrackingParamsSS
   :members:

TLE (Satellite)
^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.TrackingParamsTLE
   :members:

Receiver Frontend Parameters
----------------------------
Receiver "Frontend" is the signal path between the sky and the digitizer - but not including digitizer itself (Digitizer is included in Backend parameters).  MPIfR software uses the name "rsc" for Frontend.  Parameters are not the same for every Frontend, but may include items such as:

* attenuation / gain control
* noise diode / calibration control
* donwconverter LO frequency control
* ... other

.. _L Band:

L Band
^^^^^^
Connected to the :ref:`EDD` backend

.. autoclass:: ParameterBlocks.FrontendParamsL
   :members:

.. _CX Band:

CX Band
^^^^^^^^
Connected to the :ref:`SKARAB` backend

.. autoclass:: ParameterBlocks.FrontendParamsCX
   :members:

.. _Ku Band:

Ku Band
^^^^^^^
Connected to the :ref:`HoloFFT` Backend

.. autoclass:: ParameterBlocks.FrontendParamsKu
   :members:

.. _K Band:

K Band
^^^^^^
Connected to the :ref:`EDD` backend

.. autoclass:: ParameterBlocks.FrontendParamsK
   :members:

.. _Optical_FE:

Optical
^^^^^^^
Connected to the :ref:`Optical_BE` backend

.. autoclass:: ParameterBlocks.FrontendParamsOptical
   :members:

Digital Backend Parameters
--------------------------
"Backend" includes ADC board / Digitizer and real-time digital signal processing.  Parameters are not the same for every Backend, but may include items such as:

* integration / accumulation time
* spectral channel width for FFT / PFB
* ... other

.. _EDD:

EDD
^^^
Effelsberg Direct Digitization.  Connected to :ref:`L Band` and :ref:`K Band` frontends

.. autoclass:: ParameterBlocks.BackendParamsEDD
   :members:

.. _HoloFFT:

HoloFFT
^^^^^^^
Keysight 35760A FFT Analyzer.  Connected to :ref:`Ku Band` frontend

.. autoclass:: ParameterBlocks.BackendParamsHoloFFT
   :members:

.. _HoloSDR:

HoloSDR
^^^^^^^
.. todo:: not available yet

.. autoclass:: ParameterBlocks.BackendParamsHoloSDR
   :members:

.. _SKARAB:

SKARAB
^^^^^^
Square Kilometer Array Reconfigurable Application Board.
Connected to the :ref:`CX Band` frontend

.. autoclass:: ParameterBlocks.BackendParamsSKARAB
   :members:

.. _Optical_BE:

Optical
^^^^^^^
Connected to the :ref:`Optical_FE` frontend

.. todo:: add content

.. autoclass:: ParameterBlocks.BackendParamsOptical
   :members:

.. _Powermeter:

Wideband RF Power Meter
^^^^^^^^^^^^^^^^^^^^^^^
.. todo:: add content

.. autoclass:: ParameterBlocks.BackendParamsPowerMeter
   :members:

EDD Mock Spectrometer
^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ParameterBlocks.BackendParamsMockEDD
   :members:

.. autoclass:: ParameterBlocks.BackendParamsMock
   :members:

TCS Data Pipeline Parameters
----------------------------
Enables the user to select TCS data pipelines per scan.  
These pipelines run on the TCS `DataAggregator` server.
Note that these pipelines are separate from the EDD pipelines which run on the EDD GPU server.  However, TCS pipelines can
consume data that is output from EDD pipelines.  For example: output data from EDD provision `TNRT_dualpol_spectrometer_L`
is typically consumed by the TCS pipeline `mbfits` to write a file in `.mbfits` format.

.. _DataParams:

DataParams
^^^^^^^^^^

.. autoclass:: ParameterBlocks.DataParams
   :members:
