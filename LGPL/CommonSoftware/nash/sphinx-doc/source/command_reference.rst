.. currentmodule:: nash

Command Reference
=================
All functions in this section can be used in the interactive iPython session and in user scripts.  Use the function :py:func:`~Session.run()` to run your script within the iPython interactive session.

Session
-------
.. autoclass:: nash.Session
   :members:

Antenna
-------
Summary of manual control functions for :py:class:`~nash.Antenna`

.. list-table::
   :widths: 60 60 60 60 60 60
   :header-rows: 1

   * - General System
     - Axis State
     - Move Axis
     - Axis Offsets (persistent)
     - Pointing Models (persistent)
     - Aux Systems
   * - :py:func:`~Antenna.remote`
     - :py:func:`~Antenna.activate`
     - :py:func:`~Antenna.goto_azel`
     - :py:func:`~Antenna.set_axis_position_offsets`
     - :py:func:`~Antenna.set_pmodel`
     - :py:func:`~Antenna.get_vertex_status`
   * - :py:func:`~Antenna.local`
     - :py:func:`~Antenna.deactivate`
     - :py:func:`~Antenna.goto_platform`
     - :py:func:`~Antenna.clear_axis_position_offsets`
     - :py:func:`~Antenna.clear_pmodel`
     - :py:func:`~Antenna.open_vertex`
   * - :py:func:`~Antenna.is_remote`
     - :py:func:`~Antenna.stop`
     - :py:func:`~Antenna.set_grs`
     - :py:func:`~Antenna.set_constant_focus_offsets`
     - :py:func:`~Antenna.set_refraction`
     - :py:func:`~Antenna.close_vertex`
   * - :py:func:`~Antenna.get_status`
     - :py:func:`~Antenna.reset`
     - 
     - :py:func:`~Antenna.clear_constant_focus_offsets`
     - :py:func:`~Antenna.clear_refraction`
     - :py:func:`~Antenna.stow`
   * - :py:func:`~Antenna.boot`
     - 
     - 
     - 
     - :py:func:`~Antenna.set_hxp_el_model`
     - :py:func:`~Antenna.unstow`

   * - :py:func:`~Antenna.get_site_location`
     - 
     - 
     -
     - :py:func:`~Antenna.clear_hxp_el_model`
     -
   * - :py:func:`~Antenna.block_until_arrived`
     - 
     - 
     -
     -
     -
   * - :py:func:`~Catalog.get_TLE`
     - 
     - 
     -
     -
     -

Detail of manual control functions for :py:class:`~nash.Antenna`

.. autoclass:: nash.Antenna
   :members:

Scan
----
.. autoclass:: nash.Scan
   :members:

Catalog
-------

.. autoclass:: nash.Catalog
   :members:
