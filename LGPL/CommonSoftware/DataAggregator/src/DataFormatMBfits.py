# -*- coding: ascii -*-
# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.06.12

import numpy as np
import os
from astropy.io.fits import Header
from astropy.io.fits import Card
from astropy.io.fits import ColDefs

from DataFormatMONITORfits import DataFormatMONITORfits
import DataFormatFitsPrimitives as fp

from CentralDB import CentralDB
import cdbConstants
# -------------------------------------------------------------
# Generate MBFITS headers
# -------------------------------------------------------------
# Fits header can generate cards automatically from tuple (keyword', value, comment),
# but create the list of Card objects explicitly to make more obvious to 
# someone who reads this code in the future (SS).

class DataFormatMBfits(DataFormatMONITORfits):
	'''
	Note: Inherit monitor point data formats (ACU status message, weather station, ...} from common format.  
	This common monitor data format is also inherited by DataFormatMBfits
	'''

	def generate_header_primary(hdr_idl):
		'''
		Generate MBFITS primary header from IDL struct
		
		Parameters
		----------
		hdr_idl : ScanMod.PRIMARY_MBFITS_HEADER_TYPE	
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header

		'''
		cdb = CentralDB()

		git_tag_path = os.environ["INTROOT"] + '/config/git_tag.txt'
		git_tag_file = open(git_tag_path)
		git_tag = git_tag_file.read().replace('\n','')
		git_tag_file.close()

		cards = [
			Card(keyword='NAXIS',		value=hdr_idl.NAXIS,	comment='type="I" value=0'),
			Card(keyword='SIMPLE',		value=hdr_idl.SIMPLE,	comment='type="L" value=True'),
			Card(keyword='BITPIX',		value=hdr_idl.BITPIX,	comment='type="I" value=32'),
			Card(keyword='EXTEND',		value=hdr_idl.EXTEND,	comment='type="L" value=True'),
			Card(keyword='TELESCOP',	value=cdb.get(cdbConstants.TELESCOPE_NAME),	comment='Telescope Name [20A]'), 
			Card(keyword='ORIGIN',		value='NARIT',	comment='Organisation or Institution [20A]'),
			Card(keyword='CREATOR',		value=git_tag,	comment='Software (including version) [20A]'),
			Card(keyword='MBFTSVER',	value=hdr_idl.MBFTSVER,	comment='MBFITS version [10A]'),
			Card(keyword='COMMENT',		value=hdr_idl.COMMENT,	comment='[20A]')
			]
		header_apy = Header(cards)
		return(header_apy)

	def generate_header_scan():
		'''
		Generate SCAN-MBFITS header from IDL struct
		'''
		cdb = CentralDB()
		cards = [
			Card(keyword='EXTNAME',		value='SCAN-MBFITS',							comment='name of this table "SCAN-MBFITS"'),
			Card(keyword='TELESCOP',	value=cdb.get(cdbConstants.TELESCOPE_NAME),		comment='Telescope Name [20A]'),
			Card(keyword='SITELONG',	value=cdb.get(cdbConstants.SITE_LONG, float),	comment='Observatory longitude [deg]'),
			Card(keyword='SITELAT',		value=cdb.get(cdbConstants.SITE_LAT, float),	comment='Observatory latitude [deg]'),
			Card(keyword='SITEELEV',	value=cdb.get(cdbConstants.SITE_ELEV, float),	comment='Observatory elevation [m]'),
			Card(keyword='DIAMETER',	value=cdb.get(cdbConstants.DIAMETER, float),	comment='Dish diameter [m]'),

			Card(keyword='PROJID',		value=cdb.get(cdbConstants.PROJID),			comment='Project ID" [20A]'),
			Card(keyword='OBSID',		value=cdb.get(cdbConstants.OBSID),			comment='Observer and operator ID [24A]'),
			Card(keyword='SCANNUM',		value=cdb.get(cdbConstants.SCANNUM, int),	comment='Scan number'),
			
			Card(keyword='TIMESYS',		value=cdb.get(cdbConstants.TIMESYS),		comment='Time system {TAI, UTC, UT1} [04A]'),
			Card(keyword='DATE-OBS',	value=cdb.get(cdbConstants.DATE_OBS),		comment='Scan start [23A] ISO-8601'),
			Card(keyword='MJD',			value=cdb.get(cdbConstants.MJD, float),		comment='Scan MJD in TIMESYS system [day]'),
			Card(keyword='NSUBS',		value=cdb.get(cdbConstants.NSUBS, int),		comment='Number of subscans in this scan'),
			Card(keyword='EQUOBS',		value=cdb.get(cdbConstants.EQUOBS, float),	comment='(opt) Equinox of observation [year]'),
			Card(keyword='LST',			value=cdb.get(cdbConstants.LST, float),		comment='Local apparent sidereal time (scan start) [s]'),
			Card(keyword='NOBS',		value=cdb.get(cdbConstants.NOBS, int),		comment='(DEPRECATED) replaced by NSUBS'),
			Card(keyword='UTC2UT1',		value=cdb.get(cdbConstants.UTC2UT1, float),	comment='UT1-UTC time translation [s]'),
			Card(keyword='TAI2UTC',		value=cdb.get(cdbConstants.TAI2UTC, float),	comment='UTC-TAI time translation [s]'),
			Card(keyword='ETUTC',		value=cdb.get(cdbConstants.ETUTC, float),	comment='Ephemeris Time - UTC time translation [s]'),
			Card(keyword='GPSTAI',		value=cdb.get(cdbConstants.GPSTAI, float),	comment='GPS time - TAI translation [s]'),
			
			Card(keyword='WCSNAME',		value=cdb.get(cdbConstants.WCSNAME),			comment='description of frame [20A]. ex: "HO"/"EQ'),
			Card(keyword='RADESYS',		value=cdb.get(cdbConstants.RADESYS),			comment='System for ecliptic/equatorial coords [8A]'),
			Card(keyword='EQUINOX',		value=cdb.get(cdbConstants.EQUINOX, float),		comment='Equinox [Julian years]'),
			Card(keyword='BLNGTYPE',	value=cdb.get(cdbConstants.BLNGTYPE),			comment='Basis system type (longitude) xLON-SFL [8A]'),
			Card(keyword='BLATTYPE',	value=cdb.get(cdbConstants.BLATTYPE),			comment='Basis system type (latitude) xLAT-SFL [8A]'),
			Card(keyword='NLNGZERO',	value=cdb.get(cdbConstants.NLNGZERO, float),	comment='Native frame zero in basis system (longitude)'),
			Card(keyword='NLATZERO',	value=cdb.get(cdbConstants.NLATZERO, float),	comment='Native frame zero in basis system (latitude)'),
			Card(keyword='NLNGTYPE',	value=cdb.get(cdbConstants.NLNGTYPE),			comment='Native system type (longitude) xLON-SFL [8A]'),
			Card(keyword='NLATTYPE',	value=cdb.get(cdbConstants.NLATTYPE),			comment='Native system type (latitude) xLAT-SFL [8A]'),
			Card(keyword='LONPOLE',		value=cdb.get(cdbConstants.LONPOLE, float),		comment='longitude of celestial pole:-180 to +180 [deg]'),
			Card(keyword='LATPOLE',		value=cdb.get(cdbConstants.LATPOLE, float),		comment='latitude of celestial pole [deg]'),
			
			Card(keyword='OBJECT',		value=cdb.get(cdbConstants.OBJECT),				comment='Source name [20A]'),
			Card(keyword='BLONGOBJ',	value=cdb.get(cdbConstants.BLONGOBJ, float),	comment='Source longitude in basis frame [deg]'),
			Card(keyword='BLATOBJ',		value=cdb.get(cdbConstants.BLATOBJ, float),		comment='Source latitude in basis frame [deg]'),
			Card(keyword='LONGOBJ',		value=cdb.get(cdbConstants.LONGOBJ, float),		comment='Source longitude in user native frame [deg]'),
			Card(keyword='LATOBJ',		value=cdb.get(cdbConstants.LATOBJ, float),		comment='Source latitude in user native frame [deg]'),
			Card(keyword='PATLONG',		value=cdb.get(cdbConstants.PATLONG, float),		comment='Pattern longitude offset in user native frame'),
			Card(keyword='PATLAT',		value=cdb.get(cdbConstants.PATLAT, float),		comment='Pattern latitude offset in user native frame'),
			Card(keyword='CALCODE',		value=cdb.get(cdbConstants.CALCODE),			comment='Calibrator Code [04A]'),
			
			Card(keyword='MOVEFRAM',	value=cdb.get(cdbConstants.MOVEFRAM, bool),		comment='True if tracking a moving frame. '),
			Card(keyword='PERIDATE',	value=cdb.get(cdbConstants.PERIDATE, float),	comment='TP, Full Julian date of perihelion [days]'),
			Card(keyword='PERIDIST',	value=cdb.get(cdbConstants.PERIDIST, float), 	comment='QR, perihelion distance [AU]'),
			Card(keyword='LONGASC',		value=cdb.get(cdbConstants.LONGASC, float),		comment='OM, Longitude of ascending node [deg]'),
			Card(keyword='OMEGA',		value=cdb.get(cdbConstants.OMEGA, float),		comment='W, Angle from asc. node to perihelion" [deg]'),  
			Card(keyword='INCLINAT',	value=cdb.get(cdbConstants.INCLINAT, float),	comment='IN, Inclination [deg]'),
			Card(keyword='ECCENTR',		value=cdb.get(cdbConstants.ECCENTR, float),		comment='EC, Eccentricity'),
			Card(keyword='ORBEPOCH',	value=cdb.get(cdbConstants.ORBEPOCH, float),	comment='EPOCH, Epoch of orbital elements [Julian days]'),
			Card(keyword='ORBEQNOX',	value=cdb.get(cdbConstants.ORBEQNOX, float),	comment='Elements equinox: J2000.0 or B1950.0 [year]'),
			Card(keyword='DISTANCE',	value=cdb.get(cdbConstants.DISTANCE, float),	comment='Geocentric Distance [AU]'),
			
			Card(keyword='SCANTYPE',	value=cdb.get(cdbConstants.SCANTYPE),			comment='Scan astronomical type [20A]'),
			Card(keyword='SCANMODE',	value=cdb.get(cdbConstants.SCANMODE),			comment='Mapping mode [20A]'),
			Card(keyword='SCANGEOM',	value=cdb.get(cdbConstants.SCANGEOM),			comment='Scan geometry [20A]'),
			Card(keyword='SCANDIR',		value=cdb.get(cdbConstants.SCANDIR),			comment='scan direction [4A]'),
			Card(keyword='SCANLINE',	value=cdb.get(cdbConstants.SCANLINE, int),		comment='number of lines in a scan'),
			Card(keyword='SCANRPTS',	value=cdb.get(cdbConstants.SCANRPTS, int),		comment='number of repeats of each scan line'),
			Card(keyword='SCANLEN',		value=cdb.get(cdbConstants.SCANLEN, float),		comment='(OTF/RASTER) line length [deg]'),
			Card(keyword='SCANXVEL',	value=cdb.get(cdbConstants.SCANXVEL, float),	comment='(OTF) tracking rate along line [deg/s]'),
			Card(keyword='SCANTIME',	value=cdb.get(cdbConstants.SCANTIME, float),	comment='(OTF) time for one line [s]'),
			Card(keyword='SCANXSPC',	value=cdb.get(cdbConstants.SCANXSPC, float),	comment='(RASTER) step along line between samples [deg]'),
			Card(keyword='SCANYSPC',	value=cdb.get(cdbConstants.SCANYSPC, float),	comment='(OTF/RASTER) step between scan lines [deg]'),
			Card(keyword='SCANSKEW',	value=cdb.get(cdbConstants.SCANSKEW, float),	comment='(OTF/RASTER) offset in direction btw lines'),
			Card(keyword='SCANROT',		value=cdb.get(cdbConstants.SCANROT, float),		comment='(OTF/RASTER) rotation of user frame E of N'),
			Card(keyword='SCANPAR1',	value=cdb.get(cdbConstants.SCANPAR1, float),	comment='spare scan parameter'),
			Card(keyword='SCANPAR2',	value=cdb.get(cdbConstants.SCANPAR2, float),	comment='another spare scan parameter'),
			Card(keyword='CROCYCLE',	value=cdb.get(cdbConstants.CROCYCLE),			comment='CAL/REF/ON loop str [20A]'), 
			Card(keyword='ZIGZAG',		value=cdb.get(cdbConstants.ZIGZAG, bool),		comment='(OTF/RASTER) Scan in zigzag? [T/F]'),
			
			Card(keyword='TRANDIST',	value=cdb.get(cdbConstants.TRANDIST, float),	comment='Holography TX distance [m]'), 
			Card(keyword='TRANFREQ',	value=cdb.get(cdbConstants.TRANFREQ, float),	comment='Holography TX frequency [Hz]'),
			Card(keyword='TRANFOCU',	value=cdb.get(cdbConstants.TRANFOCU, float),	comment='Holography TX offset from prime focus [deg]'),
			
			Card(keyword='WOBUSED',		value=cdb.get(cdbConstants.WOBUSED, bool),		comment='Wobbler used?"'),
			Card(keyword='WOBTHROW',	value=cdb.get(cdbConstants.WOBTHROW, float),	comment='Wobbler throw [deg]'), 
			Card(keyword='WOBDIR',		value=cdb.get(cdbConstants.WOBDIR),				comment='Wobbler throw direction [04A]'),
			Card(keyword='WOBCYCLE',	value=cdb.get(cdbConstants.WOBCYCLE, float),	comment='Wobbler period [s]'),
			Card(keyword='WOBMODE',		value=cdb.get(cdbConstants.WOBMODE),			comment='Wobbler mode (SQUARE/TRIANGULAR)[20A]'),
			Card(keyword='WOBPAT',		value=cdb.get(cdbConstants.WOBPAT),				comment='Wobbler Pattern (e.g. NEG, POS, SYM)'),
			
			Card(keyword='PHASE1',		value=cdb.get(cdbConstants.PHASE1),	comment='Phase 1 description [20A]'),
			Card(keyword='PHASE2',		value=cdb.get(cdbConstants.PHASE2),	comment='Phase 2 description [20A]'),
			Card(keyword='PHASE3',		value=cdb.get(cdbConstants.PHASE3),	comment='Phase 3 description [20A]'),
			Card(keyword='PHASE4',		value=cdb.get(cdbConstants.PHASE4),	comment='Phase 4 description [20A]'),
			Card(keyword='PHASE5',		value=cdb.get(cdbConstants.PHASE5),	comment='Phase 5 description [20A]'),
			Card(keyword='PHASE6',		value=cdb.get(cdbConstants.PHASE6),	comment='Phase 6 description [20A]'),
			Card(keyword='PHASE7',		value=cdb.get(cdbConstants.PHASE7),	comment='Phase 7 description [20A]'),
			Card(keyword='PHASE8',		value=cdb.get(cdbConstants.PHASE8),	comment='Phase 8 description [20A]'),
			
			Card(keyword='NFEBE',		value=cdb.get(cdbConstants.NFEBE, int),	comment='Number of FEBEs'),
			
			Card(keyword='PDELTAIA',	value=cdb.get(cdbConstants.PDELTAIA, float),	comment='Accumulated user ptg correction IA [deg]'),
			Card(keyword='PDELTACA',	value=cdb.get(cdbConstants.PDELTACA, float),	comment='Accumulated user ptg correction CA [deg]'),
			Card(keyword='PDELTAIE',	value=cdb.get(cdbConstants.PDELTAIE, float),	comment='Accumulated user ptg correction IE [deg]'),
			Card(keyword='FDELTAX',		value=cdb.get(cdbConstants.FDELTAX, float),		comment='Accumulated user focus correction X [m]'),
			Card(keyword='FDELTAY',		value=cdb.get(cdbConstants.FDELTAY, float),		comment='Accumulated user focus correction Y [m]'),
			Card(keyword='FDELTAZ',		value=cdb.get(cdbConstants.FDELTAZ, float),		comment='Accumulated user focus correction Z [m]'),
			Card(keyword='FDELTAXT',	value=cdb.get(cdbConstants.FDELTAXT, float),	comment='Accumulated user focus correction XTilt [deg]'),
			Card(keyword='FDELTAYT',	value=cdb.get(cdbConstants.FDELTAYT, float),	comment='Accumulated user focus correction YTilt [deg]'),
			Card(keyword='FDELTAZT',	value=cdb.get(cdbConstants.FDELTAZT, float),	comment='Accumulated user focus correction ZTilt [deg]'),
			Card(keyword='FDELTAIA',	value=cdb.get(cdbConstants.FDELTAIA, float),	comment='Accumulated ptg corr to IA due to focus [deg]'),
			Card(keyword='FDELTACA',	value=cdb.get(cdbConstants.FDELTACA, float),	comment='Accumulated ptg corr to CA due to focus [deg]'),
			Card(keyword='FDELTAIE',	value=cdb.get(cdbConstants.FDELTAIE, float),	comment='Accumulated ptg corr to IE due to focus [deg]'),
			
			Card(keyword='IA',			value=cdb.get(cdbConstants.IA, float),		comment='Pointing Coefficient (-P1)  [deg]'),
			Card(keyword='IE',			value=cdb.get(cdbConstants.IE, float),		comment='Pointing Coefficient (P7) [deg]'),
			Card(keyword='HASA',		value=cdb.get(cdbConstants.HASA, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HACA',		value=cdb.get(cdbConstants.HACA, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HESE',		value=cdb.get(cdbConstants.HESE, float),	comment='Pointing Coefficient (P9 was ZFLX) [deg]'),
			Card(keyword='HECE',		value=cdb.get(cdbConstants.HECE, float),	comment='Pointing Coefficient (P8 was ECEC) [deg]'), 
			Card(keyword='HESA',		value=cdb.get(cdbConstants.HESA, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HASA2',		value=cdb.get(cdbConstants.HASA2, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HACA2',		value=cdb.get(cdbConstants.HACA2, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HESA2',		value=cdb.get(cdbConstants.HESA2, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HECA2',		value=cdb.get(cdbConstants.HECA2, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HACA3',		value=cdb.get(cdbConstants.HACA3, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HECA3',		value=cdb.get(cdbConstants.HECA3, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='HESA3',		value=cdb.get(cdbConstants.HESA3, float),	comment='Pointing Coefficient [deg]'),
			Card(keyword='NPAE',		value=cdb.get(cdbConstants.NPAE, float),	comment='Pointing Coefficient (-P3) [deg]'),
			Card(keyword='CA',			value=cdb.get(cdbConstants.CA, float),		comment='Pointing Coefficient (-P2) [deg]'),
			Card(keyword='AN',			value=cdb.get(cdbConstants.AN, float),		comment='Pointing Coefficient (-P5) [deg]'),
			Card(keyword='AW',			value=cdb.get(cdbConstants.AW, float),		comment='Pointing Coefficient (-P4) [deg]')
			]
		header_apy = Header(cards)
		return(header_apy)

	def generate_header_febepar(hdr_idl):
		'''
		Generate FEBAR-MBFITS header from IDL struct
		
		Parameters
		----------
		hdr_idl : ScanMod.SCAN_FEBEBAR_HEADER_TYPE	
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header

		'''
		cards = [
			Card(keyword='EXTNAME',		value='FEBEPAR-MBFITS',	comment='name of this table "FEBEPAR-MBFITS"'),
			Card(keyword='FEBE',		value=hdr_idl.FEBE,		comment='Frontend-backend ID" [68A]'),
			Card(keyword='SCANNUM',		value=hdr_idl.SCANNUM,	comment='Scan number'),
			Card(keyword='DATE-OBS',	value=hdr_idl.DATE_OBS,	comment='Scan start in TIMESYS system [23A]'),
			Card(keyword='DEWCABIN',	value=hdr_idl.DEWCABIN,	comment='Dewar cabin: (RX location)" [68A]'),
			Card(keyword='DEWRTMOD',	value=hdr_idl.DEWRTMOD,	comment='Dewar tracking system" [05A]'),
			Card(keyword='DEWUSER',		value=hdr_idl.DEWUSER,	comment='Dewar user angle" [Deg]'),
			Card(keyword='DEWZERO',		value=hdr_idl.DEWZERO,	comment='Dewar zero angle" [Deg]'),
			Card(keyword='FEBEBAND',	value=hdr_idl.FEBEBAND,	comment='Max number basebands for this FEBE"'),
			Card(keyword='FEBEFEED',	value=hdr_idl.FEBEFEED,	comment='Total number of feeds'),
			Card(keyword='NUSEBAND',	value=hdr_idl.NUSEBAND,	comment='Number of BASEBANDs in use'),
			Card(keyword='FDTYPCOD',	value=hdr_idl.FDTYPCOD,	comment='FEEDTYPE code definition [512A]'),
			Card(keyword='FEGAIN',		value=hdr_idl.FEGAIN,	comment='Frontend amplifier gain'),
			Card(keyword='SWTCHMOD',	value=hdr_idl.SWTCHMOD,	comment='Switch mode [20A]'),
			Card(keyword='NPHASES',		value=hdr_idl.NPHASES,	comment='No. of switch phases in a switch"'),
			Card(keyword='FRTHRWLO',	value=hdr_idl.FRTHRWLO,	comment='Phase 1 frequency switching throw [Hz]'),
			Card(keyword='FRTHRWHI',	value=hdr_idl.FRTHRWHI,	comment='Phase 2 frequency switching throw [Hz]'),
			Card(keyword='TBLANK',		value=hdr_idl.TBLANK,	comment='Blank time of backend [s]'),
			Card(keyword='TSYNC',		value=hdr_idl.TSYNC,	comment='Sync. time of backend [s]'),
			Card(keyword='IARX',		value=hdr_idl.IARX,		comment='Receiver ptg coef. adds to IA [deg]'),
			Card(keyword='IERX',		value=hdr_idl.IERX,		comment='Receiver ptg coef. adds to IE [deg]'),
			Card(keyword='HASARX',		value=hdr_idl.HASARX,	comment='Receiver ptg coef. adds to HASA [deg]'),
			Card(keyword='HACARX',		value=hdr_idl.HACARX,	comment='Receiver ptg coef. adds to HACA [deg]'),
			Card(keyword='HESERX',		value=hdr_idl.HESERX,	comment='Receiver ptg coef. adds to HESE [deg]'),
			Card(keyword='HECERX',		value=hdr_idl.HECERX,	comment='Receiver ptg coef. adds to HECE [deg]'),
			Card(keyword='HESARX',		value=hdr_idl.HESARX,	comment='Receiver ptg coef. adds to HESA [deg]'),
			Card(keyword='HASA2RX',		value=hdr_idl.HASA2RX,	comment='Receiver ptg coef. adds to HASA2 [deg]'),
			Card(keyword='HACA2RX',		value=hdr_idl.HACA2RX,	comment='Receiver ptg coef. adds to HACA2 [deg]'),
			Card(keyword='HESA2RX',		value=hdr_idl.HESA2RX,	comment='Receiver ptg coef. adds to HESA2 [deg]'),
			Card(keyword='HECA2RX',		value=hdr_idl.HECA2RX,	comment='Receiver ptg coef. adds to HECA2 [deg]'),
			Card(keyword='HACA3RX',		value=hdr_idl.HACA3RX,	comment='Receiver ptg coef. adds to HACA3 [deg]'),
			Card(keyword='HECA3RX',		value=hdr_idl.HECA3RX,	comment='Receiver ptg coef. adds to HECA3 [deg]'),
			Card(keyword='HESA3RX',		value=hdr_idl.HESA3RX,	comment='Receiver ptg coef. adds to HESA3 [deg]'),
			Card(keyword='NPAERX',		value=hdr_idl.NPAERX,	comment='Receiver ptg coef. adds to NPAE [deg]'),
			Card(keyword='CARX',		value=hdr_idl.CARX,		comment='Receiver ptg coef. adds to CA [deg]'),
			Card(keyword='ANRX',		value=hdr_idl.ANRX,		comment='Receiver ptg coef. adds to AN [deg]'),
			Card(keyword='AWRX',		value=hdr_idl.AWRX,		comment='Receiver ptg coef. adds to AW [deg] ')
			]
		header_apy = Header(cards)
		return(header_apy)

	def generate_header_arraydata(hdr_dict):
		'''
		Generate ARRAYDATA-MBFITS header from IDL struct
		
		Parameters
		----------
		hdr_dict : dict
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header

		'''
		cards = [
			Card(keyword='EXTNAME', 	value='ARRAYDATA-MBFITS',		comment='name of this table "ARRAYDATA-MBFITS"'),
			Card(keyword='FEBE',		value=hdr_dict['FEBE'],			comment='Frontend-backend combination ID" [17A]'),
			Card(keyword='BASEBAND',	value=hdr_dict['BASEBAND'],		comment='"Baseband number" type="J" unit="--"'),
			Card(keyword='SCANNUM',		value=hdr_dict['SCANNUM'],		comment='Scan number'),
			Card(keyword='SUBSNUM',		value=hdr_dict['SUBSNUM'],		comment='Subscan number'),
			Card(keyword='DATE-OBS',	value=hdr_dict['DATE-OBS'],		comment='Subscan start in TIMESYS system [23A]'),
			Card(keyword='CHANNELS',	value=hdr_dict['CHANNELS'],		comment='Number of spec channels for this BASEBAND'),
			Card(keyword='NUSEFEED',	value=hdr_dict['NUSEFEED'],		comment='Number of feeds in use for this BASEBAND'),
			Card(keyword='FREQRES',		value=hdr_dict['FREQRES'],		comment='Frequency resolution [Hz]'),
			Card(keyword='BANDWID',		value=hdr_dict['BANDWID'],		comment='Bandwidth in the sky frequency frame'),
			Card(keyword='MOLECULE',	value=hdr_dict['MOLECULE'],		comment='Main line molecule [20A]'),
			Card(keyword='TRANSITI',	value=hdr_dict['TRANSITI'],		comment='Transition [20A]'),
			Card(keyword='RESTFREQ',	value=hdr_dict['RESTFREQ'],		comment='Rest frequency of line [Hz]'),
			Card(keyword='SKYFREQ',		value=hdr_dict['SKYFREQ'],		comment='Sky frequency including velocity corrections'),
			Card(keyword='SIDEBAND',	value=hdr_dict['SIDEBAND'],		comment='Main sideband (USB/LSB) [03A]'),
			Card(keyword='SBSEP',		value=hdr_dict['SBSEP'],		comment='Sideband separation including band offsets'),
			Card(keyword='2CTYP2',		value=hdr_dict['2CTYP2'],		comment='Feed axis (in USEFEED array) [08A]'),
			Card(keyword='2CRPX2',		value=hdr_dict['2CRPX2'],		comment='Axis 2 Ref position'),
			Card(keyword='2CRVL2',		value=hdr_dict['2CRVL2'],		comment='Axis 2 feed index value at this position'),
			Card(keyword='2CDLT2A',		value=hdr_dict['2CDLT2A'],		comment='Axis 2 Feed index separation'),
			
			Card(keyword='WCSNM2F',		value=hdr_dict['WCSNM2F'],	comment='Frequency description in rest frame'), 
			Card(keyword='1CTYP2F',		value=hdr_dict['1CTYP2F'],	comment='Frequency axis for col.2 [8A]. value="FREQ"'), 
			Card(keyword='1CRPX2F',		value=hdr_dict['1CRPX2F'],	comment='Ref. channel'), #0
			Card(keyword='1CRVL2F',		value=hdr_dict['1CRVL2F'],	comment='Frequency at ref. channel in rest frame [Hz]'), # first freq of ny.. zone 2
			Card(keyword='1CDLT2F',		value=hdr_dict['1CDLT2F'],	comment='Channel separation [Hz]'), # freq res
			Card(keyword='1CUNI2F',		value='Hz',					comment='Unit [8A] value="Hz"'),
			Card(keyword='1SPEC2F',		value=hdr_dict['1SPEC2F'],	comment='Standard of rest for frequencies [8A] ex=LSRK'),
			Card(keyword='1SOBS2F',		value=hdr_dict['1SOBS2F'],	comment='Observing frame [8A]'), # topocen ?

			Card(keyword='WCSNM2I',		value=hdr_dict['WCSNM2I'],	comment='Frequency source rest frame, image sideband'),
			Card(keyword='1CTYP2I',		value=hdr_dict['1CTYP2I'],	comment='Frequency axis for col.2 [8A]. value="FREQ"'),
			Card(keyword='1CRPX2I',		value=hdr_dict['1CRPX2I'],	comment='Ref. channel'),
			Card(keyword='1CRVL2I',		value=hdr_dict['1CRVL2I'],	comment='Frequency at ref. channel in rest frame [Hz]'),
			Card(keyword='1CDLT2I',		value=hdr_dict['1CDLT2I'],	comment='Channel separation [Hz]'),
			Card(keyword='1CUNI2I',		value=hdr_dict['1CUNI2I'],	comment='Unit [8A] value="Hz"'),
			Card(keyword='1SPEC2I',		value=hdr_dict['1SPEC2I'],	comment='Standard of rest for frequencies [8A] ex=LSRK'),
			Card(keyword='1SOBS2I',		value=hdr_dict['1SOBS2I'],	comment='Observing frame [8A]'),

			Card(keyword='WCSNM2S',		value=hdr_dict['WCSNM2S'],	comment='Frequency sky/telescope frame, main sideband'),
			Card(keyword='1CTYP2S',		value=hdr_dict['1CTYP2S'],	comment='Frequency axis for col.2 [8A]. value="FREQ"'),
			Card(keyword='1CRPX2S',		value=hdr_dict['1CRPX2S'],	comment='Ref. channel'),
			Card(keyword='1CRVL2S',		value=hdr_dict['1CRVL2S'],	comment='Frequency at ref. channel in rest frame [Hz]'),
			Card(keyword='1CDLT2S',		value=hdr_dict['1CDLT2S'],	comment='Channel separation [Hz]'),
			Card(keyword='1CUNI2S',		value=hdr_dict['1CUNI2S'],	comment='Unit [8A] value="Hz"'),
			Card(keyword='1SPEC2S',		value=hdr_dict['1SPEC2S'],	comment='Standard of rest for frequencies [8A] ex=LSRK'),
			Card(keyword='1SOBS2S',		value=hdr_dict['1SOBS2S'],	comment='Observing frame [8A]'),

			Card(keyword='WCSNM2J',		value=hdr_dict['WCSNM2J'],	comment='Frequency sky/telescope frame, image sideband'),
			Card(keyword='1CTYP2J',		value=hdr_dict['1CTYP2J'],	comment='Frequency axis for col.2 [8A]. value="FREQ"'),
			Card(keyword='1CRPX2J',		value=hdr_dict['1CRPX2J'],	comment='Ref. channel'),
			Card(keyword='1CRVL2J',		value=hdr_dict['1CRVL2J'],	comment='Frequency at ref. channel in rest frame [Hz]'),
			Card(keyword='1CDLT2J',		value=hdr_dict['1CDLT2J'],	comment='Channel separation [Hz]'),
			Card(keyword='1CUNI2J',		value=hdr_dict['1CUNI2J'],	comment='Unit [8A] value="Hz"'),
			Card(keyword='1SPEC2J',		value=hdr_dict['1SPEC2J'],	comment='Standard of rest for frequencies [8A] ex=LSRK'),
			Card(keyword='1SOBS2J',		value=hdr_dict['1SOBS2J'],	comment='Observing frame [8A].'),

			Card(keyword='WCSNM2R',		value=hdr_dict['WCSNM2R'],	comment='Velocity description in rest frame'),
			Card(keyword='1CTYP2R',		value=hdr_dict['1CTYP2R'],	comment='Velocity axis for col.2 [8A]. value="VRAD"'),
			Card(keyword='1CRPX2R',		value=hdr_dict['1CRPX2R'],	comment='Ref. channel'),
			Card(keyword='1CRVL2R',		value=hdr_dict['1CRVL2R'],	comment='Velocity at ref. channel in rest frame [km/s]'),
			Card(keyword='1CDLT2R',		value=hdr_dict['1CDLT2R'],	comment='Velocity Channel separation [km/s]'),
			Card(keyword='1CUNI2R',		value=hdr_dict['1CUNI2R'],	comment='Unit [8A] value="Hz"'),
			Card(keyword='1SPEC2R',		value=hdr_dict['1SPEC2R'],	comment='Standard of rest frame for velo. [8A] ex=LSRK'),
			Card(keyword='1SOBS2R',		value=hdr_dict['1SOBS2R'],	comment='Observing frame [8A]'),

			Card(keyword='1VSOU2R',		value=hdr_dict['1VSOU2R'],	comment='Source velocity in rest frame [km/s]'),
			Card(keyword='1VSYS2R',		value=hdr_dict['1VSYS2R'],	comment='Observer vel. in rest frame in dir of observe')
			]
		header_apy = Header(cards)
		return(header_apy)

	def generate_header_datapar(hdr_dict):
		'''
		Generate DATAPAR-MBFITS header from IDL struct
		
		Parameters
		----------
		hdr_dict : dict	
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header

		'''
		cards = [
			Card(keyword='EXTNAME',		value='DATAPAR-MBFITS',		comment='name of this table "DATAPAR-MBFITS"'),
			Card(keyword='SCANNUM',		value=hdr_dict['SCANNUM'],		comment='Scan number'),
			Card(keyword='OBSNUM',		value=hdr_dict['OBSNUM'],		comment='Subscan number'),
			Card(keyword='SUBSNUM',		value=hdr_dict['SUBSNUM'],		comment='Subscan number'),
			Card(keyword='DATE-OBS',	value=hdr_dict['DATE-OBS'],		comment='Subscan start in TIMESYS system [23A]'),
			Card(keyword='FEBE',		value=hdr_dict['FEBE'],			comment='Frontend-backend combination ID [17A]'),
			Card(keyword='LST',			value=hdr_dict['LST'],			comment='Local apparent sidereal time subs. start [s]'),
			Card(keyword='SUBSTYPE',	value=hdr_dict['SUBSTYPE'],		comment='Subscan type [4A]'),
			Card(keyword='SCANTYPE',	value=hdr_dict['SCANTYPE'],		comment='Scan type [20A]'),
			Card(keyword='SCANMODE',	value=hdr_dict['SCANMODE'],		comment='Mapping mode [20A]'),
			Card(keyword='SCANDIR',		value=hdr_dict['SCANDIR'],		comment='(opt) scan direction [4A]'),
			Card(keyword='SCANLEN',		value=hdr_dict['SCANLEN'],		comment='(opt,OTF/RASTER) line length [deg]'),
			Card(keyword='SCANXVEL',	value=hdr_dict['SCANXVEL'],		comment='(opt,OTF) tracking rate along line [deg/s]'),
			Card(keyword='SCANTIME',	value=hdr_dict['SCANTIME'],		comment='(opt,OTF) time for one line [s]'),
			Card(keyword='SCANXSPC',	value=hdr_dict['SCANXSPC'],		comment='(opt,RASTER) step along line between samples'),
			Card(keyword='SCANYSPC',	value=hdr_dict['SCANYSPC'],		comment='(opt,OTF/RASTER) step between scan/raster lines'),
			Card(keyword='SCANSKEW',	value=hdr_dict['SCANSKEW'],		comment='(opt,OTF/RASTER) offset in scan dir btw lines'),
			Card(keyword='SCANPAR1',	value=hdr_dict['SCANPAR1'],		comment='(opt) spare scan parameter" type="D"'),
			Card(keyword='SCANPAR2',	value=hdr_dict['SCANPAR2'],		comment='(opt) another spare scan parameter" type="D"'),
			Card(keyword='NLNGTYPE',	value=hdr_dict['NLNGTYPE'],		comment='Native system type (longitude) xLON-SFL'),
			Card(keyword='NLATTYPE',	value=hdr_dict['NLATTYPE'],		comment='Native system type (latitude) xLAT-SFL'),
			Card(keyword='DPBLOCK',		value=hdr_dict['DPBLOCK'],		comment='Data blocking? [T/F]'),
			Card(keyword='NINTS',		value=hdr_dict['NINTS'],		comment='Number of integrations in block'),
			Card(keyword='OBSTATUS',	value=hdr_dict['OBSTATUS'],		comment='Subscan ok? (OK/ABORT) [10A]'),
			Card(keyword='WOBCOORD',	value=hdr_dict['WOBCOORD'],		comment='Coordinates include wobbler offsets? [T/F]')
			]
		header_apy = Header(cards)
		return(header_apy)

	# -------------------------------------------------------------
	# Generate MBFITS Binary Data table arrays
	# -------------------------------------------------------------
	# Fits BinTableHDU can generate a binary data table from a Numpy structured
	# array with named fields and numpy datatypes, but I can't find a way to add 
	# units (MHz, km/s, Jy, etc) in the numpy arrays, so add the units later to the coldefs.

	#binary data specification to use with current SCAN-MBFITS header in BinTableHDU
	dtype_scan_row = np.dtype([
		('FEBE', np.bytes_, 68) # List of Frontend-Backend combination identification
		])


	def generate_dtype_febepar_row(febefeed, nuseband):
		'''
		Generate binary data specification to use with current FEBEPAR-MBFITS header in BinTableHDU
		Data array dimensions come from current values in FEBEPAR-MBFITS header

		Parameters
		----------
		febefeed : int
			Number of frontend feeds during simultaneous observation
			Value should be consistent with ScanMod.FEBEPAR_MBFITS_HEADER_TYPE.FEBEFEED

		nuseband : int
			Number of sections / used bands / noise diode states for each feed)
			Value should be consistent with ScanMod.FEBEPAR_MBFITS_HEADER_TYPE.NUSEBAND

		Returns
		-------
		dtype_febepar_row : numpy.dtype
			specification of binary data "columns" defined using 
			numpy data types that astropy.io.fits understands and and auto-translates to FITS data types.
			Array dimensions of each item come from current values of header
		'''

		# add default value to prevent error
		if not nuseband:
			nuseband = 1

		dtype_febepar_row = np.dtype([
			('USEBAND',		np.int32, 	(nuseband,)), # List of BASEBANDs which are in use
			('NUSEFEED',	np.int32, 	(nuseband,)), # Number of feeds in use for each BASEBAND
			('USEFEED',		np.int32), # List of feeds which are in use for each BASEBAND
			('BESECTS',		np.int32), # List of backend sections connected to feeds in use for each BASEBAND
			('FEEDTYPE',	np.int32), # Feed type in order of USEFEED
			('FEEDOFFX',	np.float64,	(febefeed,)), # Feed x offset
			('FEEDOFFY',	np.float64, (febefeed,)), # Feed y offset
			('REFFEED',		np.int32), # Reference feed number
			('POLTY',		np.bytes_,	febefeed), # Feed polarisation type {X, Y, L, R}
			('POLA',		np.float32,	(febefeed,)), # (NOT IN USE) Feed polarisation angle
			('APEREFF',		np.float32,	(febefeed, nuseband)), # Aperture efficiency
			('BEAMEFF',		np.float32,	(febefeed, nuseband)), # Beam efficiency
			('ETAFSS',		np.float32,	(febefeed, nuseband)), # Forward efficiency
			('HPBW',		np.float32,	(febefeed, nuseband)), # Half-power beam width
			('ANTGAIN',		np.float32,	(febefeed, nuseband)), # (NOT IN USE) Antenna Gain
			('TCAL',		np.float32,	(febefeed, nuseband)), # Calibration temperature
			('BOLCALFC',	np.float32,	(nuseband,)), # (NOT IN USE) Bolometer calibration factor
			('BEGAIN',		np.float32,	(nuseband,)), # (NOT IN USE) Backend reference gain/attenuation factor
			('FLATFIEL',	np.float32,	(febefeed, nuseband)), # (NOT IN USE) Bolometer flat field (relative gains)
			('BOLDCOFF',	np.float32,	(febefeed, nuseband)), # (NOT IN USE) Bolometer DC offset
			('GAINIMAG',	np.float32,	(febefeed, nuseband)), # Gain ratio (image/signal sideband)
			('GAINELE1',	np.float32,	(nuseband,)), # Gain-elevation correction parameter 1
			('GAINELE2',	np.float32,	(nuseband,)), # Gain-elevation correction parameter 2
			])

		return(dtype_febepar_row)

	def generate_dtype_arraydata_row(nusefeed, channels):
		'''
		Generate binary data to attach with current ARRAYDATA-MBFITS header in BinTableHDU
		Data array dimensions come from current values in ARRAYDATA-MBFITS header

		Parameters
		----------
		nusefeed : int
			Number of feeds in use for this BASEBAND
			Value should be consistent with ScanMod.ARRAYDATA_MBFITS_HEADER_TYPE.NUSEFEED

		channels : int
			Number of spec channels for this BASEBAND
			Value should be consistent with ScanMod.ARRAYDATA_MBFITS_HEADER_TYPE.CHANNELS

		Returns
		-------
		dtype_arraydata_row : numpy.dtype
			specification of binary data "columns" defined using 
			numpy data types that astropy.io.fits understands and and auto-translates to FITS data types.
			Array dimensions of each item come from current values of header	
		'''

		# add default value to prevent error
		if not nusefeed:
			nusefeed = 1

		if not channels:
			channels = 1

		dtype_arraydata_row = np.dtype([
			('MJD',		np.float64), # MJD at integration midpoint in TIMESYS system
			('DATA', 	np.float32, (nusefeed, channels)) # Data
			])
		return(dtype_arraydata_row)

	# Generate binary data to attach with current DATAPAR-MBFITS header in BinTableHDU
	dtype_datapar_row = np.dtype([
		('MJD',			np.float64), # MJD at integration midpoint in TIMESYS system
		('LST',			np.float64), # Local apparent sidereal time at integration midpoint
		('INTEGTIM',	np.float64), # Integration time
		('PHASE',		np.uint32), # Integration phase number
		('LONGOFF',		np.float64), # Longitude offset from source in user native frame 
		('LATOFF',		np.float64), # Latitude offset from source in user native frame
		('AZIMUTH',		np.float64), # Azimuth (derived) inc. wobbler offsets 
		('ELEVATIO',	np.float64), # Elevation (derived) inc. wobbler offsets
		('RA',			np.float64), # Mean right ascension inc. wobbler offsets
		('DEC',			np.float64), # Mean declination inc. wobbler offsets
		('PARANGLE',	np.float64), # Parallactic angle
		('CBASLONG',	np.float64), # Commanded long. basis frame (defined by CTYPEjB)
		('CBASLAT',		np.float64), # Commanded lat. basis frame (defined by CTYPEjB)
		('BASLONG',		np.float64), # Actual long. in basis frame inc. wobbler offsets
		('BASLAT',		np.float64), # Actual lat. in basis frame inc. wobbler offsets
		('ROTANGLE',	np.float64), # Angle of array, measured E wrt N in user native frame
		('MCRVAL1',		np.float64), # (opt.) Native frame zero in basis system (long.) 
		('MCRVAL2',		np.float64), # (opt.) Native frame zero in basis system (lat.) 
		('MLONPOLE',	np.float64), # (opt.) longitude of basis celestial pole in body system 
		('MLATPOLE',	np.float64), # (opt.) latitude of basis celestial pole in body system
		('DFOCUS_X',	np.float64), # (opt.) Commanded focus X offset (subreflector position) 
		('DFOCUS_Y',	np.float64), # (opt.) Commanded focus Y offset (subreflector position)
		('DFOCUS_Z',	np.float64), # (opt.) Commanded focus Z offset (subreflector position)
		('DPHI_X',		np.float64), # (opt.) Commanded Phi X offset (subreflector rotation)
		('DPHI_Y',		np.float64), # (opt.) Commanded Phi Y offset (subreflector rotation)
		('DPHI_Z',		np.float64) # (opt.) Commanded Phi Z offset (subreflector rotation)
		])

	# -------------------------------------------------------------
	# Generate ColDefs
	# -------------------------------------------------------------
	# Fits BinTableHDU can generate data table directly from numpy structured array,
	# but I don't see a way to add units.  Therefore, create Coldefs objects
	# from numpy structured arrays, and add units if present.  
	# Then BinTableHDU can read this ColdDefs object including the original
	# numpy array and column units.

	def generate_coldefs_scan(data_ndarray):
		coldefs = ColDefs(data_ndarray)
		return(coldefs)

	def generate_coldefs_febepar(data_ndarray):
		coldefs = ColDefs(data_ndarray)
		coldefs['FEEDOFFX'].unit = 'deg'
		coldefs['FEEDOFFY'].unit = 'deg'
		coldefs['POLA'].unit = 'deg'
		coldefs['HPBW'].unit = 'deg'
		coldefs['ANTGAIN'].unit = 'K/Jy'
		coldefs['TCAL'].unit = 'mK'
		coldefs['BOLCALFC'].unit = 'Jy/counts'
		coldefs['GAINELE1'].unit = 'deg'
		return(coldefs)

	def generate_coldefs_arraydata(data_ndarray):
		coldefs = ColDefs(data_ndarray)
		coldefs['MJD'].unit = 'day[MJD]'
		return(coldefs)

	def generate_coldefs_datapar(data_ndarray):
		coldefs = ColDefs(data_ndarray)
		coldefs['MJD'].unit = 'day[MJD]'
		coldefs['LST'].unit = 's'
		coldefs['INTEGTIM'].unit = 's'
		coldefs['LONGOFF'].unit = 'deg'
		coldefs['LATOFF'].unit = 'deg'
		coldefs['AZIMUTH'].unit = 'deg'
		coldefs['ELEVATIO'].unit = 'deg'
		coldefs['RA'].unit = 'deg'
		coldefs['DEC'].unit = 'deg'
		coldefs['PARANGLE'].unit = 'deg'
		coldefs['CBASLONG'].unit = 'deg'
		coldefs['CBASLAT'].unit = 'deg'
		coldefs['BASLONG'].unit = 'deg'
		coldefs['BASLAT'].unit = 'deg'
		coldefs['ROTANGLE'].unit = 'deg'
		coldefs['MCRVAL1'].unit = 'deg'
		coldefs['MCRVAL2'].unit = 'deg'
		coldefs['MLONPOLE'].unit = 'deg'
		coldefs['MLATPOLE'].unit = 'deg'
		coldefs['DFOCUS_X'].unit = 'm'
		coldefs['DFOCUS_Y'].unit = 'm'
		coldefs['DFOCUS_Z'].unit = 'm'
		coldefs['DPHI_X'].unit = 'deg'
		coldefs['DPHI_Y'].unit = 'deg'
		coldefs['DPHI_Z'].unit = 'deg'
		return(coldefs)
