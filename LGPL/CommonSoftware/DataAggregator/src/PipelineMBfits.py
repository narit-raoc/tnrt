# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.03

import logging
import time
import datetime
import os
import numpy as np

from astropy.time import Time
from astropy.io import fits
from scipy.interpolate import interp1d
import astropy.units as units

# QT for QRunnable task in Threadpool
from PyQt5 import QtCore

import DataAggregatorMod
import TypeConverterDA as tc
import tnrtAntennaMod
import DataFormatAcu as acu
import DataAggregatorErrorImpl
from AbstractBasePipeline import AbstractBasePipeline
from DataFormatMBfits import DataFormatMBfits
from DataFormatMONITORfits import DataFormatMONITORfits

import FrontendConfig as frontend_config
from CentralDB import CentralDB
import cdbConstants


class PipelineMBfits(AbstractBasePipeline):
    """
    Docstring
    """

    # Define properties that are required by the parent class, but implemented in subclass
    # These properties are tagged with decorator @abstractproperty
    def __init__(self, metadata_ref):
        AbstractBasePipeline.__init__(self, metadata_ref)

        self.inputs = {
            "antenna": {
                "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
                "dtype": tnrtAntennaMod.AntennaStatusNotifyBlock,
                "handler": self.handler_antenna,
                "consumer": None,
            },
            "frontend": {
                "channel": frontend_config.nc_channel_monitor,
                "handler": self.handler_frontend,
                "consumer": None,
            }
        }

        self.outputs = {
            "pipeline": {
                "channel": DataAggregatorMod.CHANNEL_NAME_PIPELINE_MBFITS,
                "supplier": None,
            }
        }

        # Create an empty lists to accumulate data as it is recorded
        # Use Python list instead of numpy array becuase list can append faster.
        # Each item in the list is a numpy array with specified dtype.
        # At end of subscan, convert Python list of numpy 1-D arrays into a 2-D numpy structured array
        # for faster / convenient processing after all append operations are finished.
        self.arraydata_buffer = []
        self.datapar_buffer = []
        self.monitor_antenna_PacketHeader_buffer = []
        self.monitor_antenna_GeneralStatus_buffer = []
        self.monitor_antenna_SystemStatus_buffer = []
        self.monitor_antenna_VertexShutterStatus_buffer = []
        self.monitor_antenna_AxisStatus_buffer = []
        self.monitor_antenna_MotorStatus_buffer = []
        self.monitor_antenna_TrackingStatus_buffer = []
        self.monitor_antenna_GeneralHexapodStatus_buffer = []
        self.monitor_antenna_HexapodStatus_buffer = []
        self.monitor_antenna_SpindleStatus_buffer = []
        self.monitor_antenna_HexapodTrackStatus_buffer = []
        self.monitor_antenna_TrackingObjectStatus_buffer = []
        self.monitor_weather_buffer = []
        self.monitor_frontend_buffer = []

        self.latest_data_timestamp = None

    def open(self, mbfits_headers):
        AbstractBasePipeline.open(self, mbfits_headers)
        # Generate filename and open a new file

        # adding a Y-m-d folder format
        file_date_utc = datetime.datetime.utcnow()
        file_date_year = file_date_utc.strftime("%Y")
        file_date_month = file_date_utc.strftime("%m")
        file_date_day = file_date_utc.strftime("%d")

        # Year / month / day folder
        self.data_dir = self.data_dir + "/" + file_date_year + "/" + file_date_month + "/" + file_date_day + "/"

        if not os.path.exists(self.data_dir):
            self.logger.logInfo("data directory %s does not exist.  Create now" % self.data_dir)
            os.makedirs(name=self.data_dir, exist_ok=True)

        self.filename_base = file_date_year + file_date_month + file_date_day + "_" + file_date_utc.strftime("%H%M%S")
        self.filename = self.data_dir + self.filename_base + ".mbfits"

        self.logger.info("Generate file = " + self.filename_base + " full path: " + self.filename)

        self.cdb.set(cdbConstants.FILE_NAME, self.filename_base + ".mbfits")
        self.cdb.set(cdbConstants.DATA_DIR, self.data_dir)
        self.cdb.set(cdbConstants.FILE_CREATE_DATE, Time.now().isot)

        self.logger.logDebug("Write Primary HDU")
        # Create a new file with only primary header.
        # Convert the Python object with attributes into dictionary
        # and automatically fill the primary header.
        hdr = DataFormatMBfits.generate_header_primary(
            mbfits_headers.PRIMARY_MBFITS_HEADER
        )
        hdu_primary = fits.PrimaryHDU(header=hdr)
        # Temporary hdul to create file
        hdul = fits.HDUList([hdu_primary])
        # Write file
        hdul.writeto(self.filename)
        hdul.close()

        # Open the same file in append mode (fits mode 'update').
        # 'update' mode allows use of hdul.flush() to write to file after
        # data in hdul list is updated / appended
        self.hdul = fits.open(self.filename, mode="append")

    def close(self):

        if self.hdul is not None:
            # Blocking wait for event write_subscan_finished to be set true by another thread before close the file.
            self.logger.info(
                "Wait for write_monitor_finished event before closing file ..."
            )
            self.write_monitor_finished.wait(timeout=60.0)
            if self.write_monitor_finished.is_set() == False:
                ex = DataAggregatorErrorImpl.timeoutOnCloseErrorExImpl()
                ex.addData(
                    "errExpl",
                    "Timeout while waiting for file to close.  Astropy IERS download?",
                )
                raise ex
            else:
                self.hdul.close()
                del self.hdul
                return AbstractBasePipeline.close(self)

    def handler_antenna(self, data_idl):
        # Convert the IDL structs (OmniORB python objects) to a numpy arrays
        (
            monitor_antenna_PacketHeader_row,
            monitor_antenna_GeneralStatus_row,
            monitor_antenna_SystemStatus_row,
            monitor_antenna_VertexShutterStatus_row,
            monitor_antenna_AxisStatus_row,
            monitor_antenna_MotorStatus_row,
            monitor_antenna_TrackingStatus_row,
            monitor_antenna_GeneralHexapodStatus_row,
            monitor_antenna_HexapodStatus_row,
            monitor_antenna_SpindleStatus_row,
            monitor_antenna_HexapodTrackStatus_row,
            monitor_antenna_TrackingObjectStatus_row,
        ) = tc.from_idl_AntennaStatusNotifyBlock(data_idl)

        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        # append to the buffers
        self.monitor_antenna_PacketHeader_buffer.append(
            monitor_antenna_PacketHeader_row
        )
        self.monitor_antenna_GeneralStatus_buffer.append(
            monitor_antenna_GeneralStatus_row
        )
        self.monitor_antenna_SystemStatus_buffer.append(
            monitor_antenna_SystemStatus_row
        )
        self.monitor_antenna_VertexShutterStatus_buffer.append(
            monitor_antenna_VertexShutterStatus_row
        )
        self.monitor_antenna_AxisStatus_buffer.append(monitor_antenna_AxisStatus_row)
        self.monitor_antenna_MotorStatus_buffer.append(monitor_antenna_MotorStatus_row)
        self.monitor_antenna_TrackingStatus_buffer.append(
            monitor_antenna_TrackingStatus_row
        )
        self.monitor_antenna_GeneralHexapodStatus_buffer.append(
            monitor_antenna_GeneralHexapodStatus_row
        )
        self.monitor_antenna_HexapodStatus_buffer.append(
            monitor_antenna_HexapodStatus_row
        )
        self.monitor_antenna_SpindleStatus_buffer.append(
            monitor_antenna_SpindleStatus_row
        )
        self.monitor_antenna_HexapodTrackStatus_buffer.append(
            monitor_antenna_HexapodTrackStatus_row
        )
        self.monitor_antenna_TrackingObjectStatus_buffer.append(
            monitor_antenna_TrackingObjectStatus_row
        )

        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()

    def handler_spectrum_arraydata(self, header, arraydata_row):
        """
        Parameters
        ----------
        header : DataFormatEDDfits.FWHeader
        arraydata_row : numpy array defined by function DataFormatMBfits.generate_dtype_arraydata_row
        """

        datapar_row = np.array(0, dtype=DataFormatMBfits.dtype_datapar_row)

        # Set same timestamp in datapar.
        datapar_row["MJD"] = arraydata_row["MJD"]

        # Set integration time for this integration period.  Convert milliseconds to seconds
        datapar_row["INTEGTIM"] = header.integration_time / 1e6

        # Everything else in DATAPAR can be processed at the end of the subscan.
        # For example, use timestamps to interpolate MONITOR data
        # for AZ, EL, etc. into the DATAPAR array which will be used to write files
        # and post-process.

        packet_timestamp = Time(arraydata_row["MJD"],format='mjd')
        self.latest_data_timestamp = packet_timestamp

        # snap the dictionary before iterate because 
        # the size can be modified by another thread when start new subscan
        subscan_dict_shallow_copy = dict(self.subscan_dict)
        
        for subs_num, subscan in subscan_dict_shallow_copy.items():
            if packet_timestamp >= subscan.time_enable_write and packet_timestamp <= (subscan.time_disable_write or Time.now()):
                self.subscan_dict[subs_num].arraydata_buffer.append(arraydata_row)
                self.subscan_dict[subs_num].datapar_buffer.append(datapar_row)
        
    def handler_frontend(self, data_dict):
        monitor_row = np.array(0, dtype=DataFormatMONITORfits.dtype_monitor_frontend_row)
        
        for key, val in data_dict.items():
			# fill monitor row with data received from backend
            monitor_row[key] = val

        self.monitor_frontend_buffer.append(monitor_row)


    def write_scan_header(self, mbfits_headers):
        """
        Writes scan header for current scan.  In the case of Gildas, it doesn't actually write
        data to the file.  It prepares the pyclassfiller data struct with information.
        When we write the actual data from integrations / drifts this data will go to to the file
        with current values.

        Parameters
        ----------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                struct MetadataNotifyBlock{
                        PRIMARY_MBFITS_HEADER_TYPE PRIMARY_MBFITS_HEADER;
                        SCAN_MBFITS_HEADER_TYPE SCAN_MBFITS_HEADER;
                        SCAN_MBFITS_COLUMNS_TYPE SCAN_MBFITS_COLUMNS;
                        FEBEPAR_MBFITS_HEADER_TYPE FEBEPAR_MBFITS_HEADER;
                        ARRAYDATA_MBFITS_HEADER_TYPE ARRAYDATA_MBFITS_HEADER;
                        DATAPAR_MBFITS_HEADER_TYPE DATAPAR_MBFITS_HEADER;
                        };
        scan_data : numpy.ndarray with rows of type dtype_scan_row
        febepar_data : nump.ndarray with rows of type dtype_febepar_row
        """

        # Define the required numpy datatypes for this SCAN whose array dimensions depend on
        # on current values of header at runtime.  Other dtypes for MBfits have constant dimensions, and don't need
        # to generate at runtime.
        self.logger.logDebug(
            "Generate dtype_febepar_row and dtype_arraydata_row dims from SCAN metadata"
        )
        self.dtype_febepar_row = DataFormatMBfits.generate_dtype_febepar_row(
            mbfits_headers.FEBEPAR_MBFITS_HEADER.FEBEFEED,
            mbfits_headers.FEBEPAR_MBFITS_HEADER.NUSEBAND,
        )

        # Empty array
        scan_data = np.zeros(1, dtype=DataFormatMBfits.dtype_scan_row)
        febepar_data = np.zeros(1, dtype=self.dtype_febepar_row)

        febepar_data["TCAL"] = 99 * np.ones(
            (
                mbfits_headers.FEBEPAR_MBFITS_HEADER.FEBEFEED,
                mbfits_headers.FEBEPAR_MBFITS_HEADER.NUSEBAND,
            )
        )
        self.logger.logDebug(
            "QThreadpool.start WorkerWriteScanHeader, Scan = {}".format(self.cdb.get(cdbConstants.SCANNUM))
        )

        # Create a runnable and delegate the work to the Threadpool.
        worker = WorkerWriteScanHeader(self.hdul, mbfits_headers, scan_data, febepar_data)

        self.threadpool.start(worker)

    def write_before_subscan(self, mbfits_headers):
        """
        NO OP.  Subscan header is written at the end of the subscan for MBFITS using astropy.io.fits
        This function exists to implement required abstractmethod in AbstractBaseScan
        """
        pass

    def write_after_subscan(self, mbfits_headers, subscan_number):
        """
        astropy.io.fits does not have a convenient method to append rows to binary tables
        in real-time because it uses numpy arrays that are sequential in memory.  Therefore
        data are buffered in lists in DataAggregator.  At the end of a subscan, the lists
        are converted to numpy arrays of the correct format for MBFITS and written to file
        here.
        TODO : if this technique consumes too much memory for long subscans and large dimension
        arrays, find a better way to write blocks of data to the file throughout subscan
        before memory is overloaded.  Or, change to use a separate package fitsio instead
        of astropy.io.fits because the documentation suggests that it can append to files in
        realtime better than astropy.io.fits

        Parameters
        ---------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                Complete set of MBFITS-style header information.  Including the DATAPAR header
                that was created at runtime during the subscan for each integration.
        """

        subscan_obj = self.subscan_dict[subscan_number]
        
        # check for data buffer if it has some data and wait for the late data due to backend latency
        # only apply to AbstractSubscanAZEL (SubscanLine, SubscanSingle) where we intend to get data from backend
        if subscan_obj.time_disable_write != None:
            while len(subscan_obj.arraydata_buffer) <= 0 :
                self.logger.logInfo('subscan {} - wait for first data packet...'.format(subscan_number))

                # if we are expecting data from backend, the latest_data_timestamp should not be None because 
                # it should records timestamp of received packet since measurement start. If we expect to get data 
                # from backend but expecting_backend_data is None, there is 2 possible reason
                # 1. we don't receive data from backend
                # 2. the integration time is too long and does not suit the observation time. 
                expecting_backend_data = self.latest_data_timestamp != None 

                # check if the lates data timestamp has passed the time enable write.
                # If we really receiving spectrum data, the buffer should already not be empty.
                # If enter this condition could possibly from 3 reason
                # 1. we do not use backend for the observation (nothing to worry about)
                # 2. some thing wrong with backend streaming 
                # 3. some thing wrong with the data recieving client
                # break the loop and skip to adding dummy data
                if expecting_backend_data and (self.latest_data_timestamp > subscan_obj.time_enable_write):
                    self.logger.logInfo('Subscan {} - No data appended to data buffer')
                    self.logger.logInfo('time_enable_write = {}, time_disable_write = {}'.format(subscan_obj.time_enable_write, subscan_obj.time_disable_write))
                    break
                elif (not expecting_backend_data) and (Time.now() > subscan_obj.time_disable_write):
                    self.logger.logInfo('Subscan does not require backend, break the loop.')
                    break
                time.sleep(0.5)
            
            if len(subscan_obj.arraydata_buffer) > 0:
                while (self.latest_data_timestamp < subscan_obj.time_disable_write):
                    self.logger.logInfo('subscan {} - wait for late data... '.format(subscan_number))
                    self.logger.logDebug('buffer latest timestamp {}, disable write timestamp {}'.format(
                        Time(subscan_obj.arraydata_buffer[-1]['MJD'],format='mjd').iso,
                        subscan_obj.time_disable_write.iso
                        ))
                    time.sleep(0.5)
        
        # Check if any of the buffers are empty.  If empty, create 1 fake row of zeros
        # so that length of array == 1. If length == 0, error in creating FITS columns because 0
        # is not a valid dimension.  In a real operation system, all buffers should have real data
        # For test and debug, sometimes we do not have data in some buffer.  Also, if we
        # only want to record data about Antenna position -- but not use receivers, we need
        # to create a placeholder for arrays that otherwise have a 0 dimension.
        if len(subscan_obj.arraydata_buffer) == 0:
            self.logger.logDebug(
                "arraydata_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
            )

            self.dtype_arraydata_row = DataFormatMBfits.generate_dtype_arraydata_row(
                subscan_obj.metadata.arraydata_mbfits_header['NUSEFEED'], 
                subscan_obj.metadata.arraydata_mbfits_header['CHANNELS'],
            )

            subscan_obj.arraydata_buffer.append(np.array(0, dtype=self.dtype_arraydata_row))

        if len(subscan_obj.datapar_buffer) == 0:
            self.logger.logDebug(
                "datapar_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
            )
            subscan_obj.datapar_buffer.append(
                np.array(0, dtype=DataFormatMBfits.dtype_datapar_row)
            )


        # self.<xxx>_buffer is a Python list of self.dtype_<xxx>_row.  Each dtype_<xxx>_row
        # is a numpy structured array with timestamp MJD and an array of data that has dimensions
        # depend on spectrum channels, sections, etc (defined at runtime).  List of rows was used
        # because it is faster to append list than concatenate numpy array.   Now append is finished
        # and convert the entire list into 1 numpy array for convenient / efficient processing.
        arraydata = np.asarray(subscan_obj.arraydata_buffer)

        # subscan_obj.datapar_buffer is a Python list of self.dtype_datapar_row.  Each dtype_datapar_row
        # is a fixed size and includes metadata associated with each row of ARRAYDATA (Az, El angles, etc)
        datapar = np.asarray(subscan_obj.datapar_buffer)

        # if datapar is not a dummy data (>1), interpolate position (AZ,EL), LONGOFF and LATOFF
        # from monitor antenna to datapar timestamps
        if len(datapar) > 0:
            try:
                AZ = 0
                EL = 1
                monitor_ant_axis = np.asarray(self.monitor_antenna_AxisStatus_buffer)
                # sometimes the acu data is appended to buffer in another thread in the middle of reading these data
                # we need to make sure that the length of 3 arrays (monitor_ant_axis, monitor_ant_tracking, monitor_ant_general)
                # are the same, so use length of monitor_ant_axis as a reference and make it the length of the other 
                # by cutting the last one of
                monitor_ant_tracking = np.asarray(self.monitor_antenna_TrackingStatus_buffer)[:len(monitor_ant_axis)]
                monitor_ant_general = np.asarray(self.monitor_antenna_GeneralStatus_buffer)[:len(monitor_ant_axis)]

                timestamps_spe = datapar["MJD"]
                timestamps_acu = monitor_ant_general["actual_time"]

                actual_position_az = monitor_ant_axis["actual_position"][:, AZ]
                actual_position_el = monitor_ant_axis["actual_position"][:, EL]

                # Find tracking center coordinate from selected tracking algorithm in monitor_ant_tracking["bit_status"]
                # The bit status exists for every ACU status message in the array.  
                # Assume it does not change during this subscan. So, we can use the first value at index [0] 
                if (monitor_ant_tracking["bit_status"][0] & acu.TR_SUN_TRACK_ACTIVE) == acu.TR_SUN_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_SUN_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["sun_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["sun_track_el"]

                elif (monitor_ant_tracking["bit_status"][0] & acu.TR_PROG_TRACK_ACTIVE) == acu.TR_PROG_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_PROG_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["prog_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["prog_track_el"]
                
                elif (monitor_ant_tracking["bit_status"][0] & acu.TR_TLE_TRACK_ACTIVE) == acu.TR_TLE_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_TLE_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["tle_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["tle_track_el"]
                
                elif (monitor_ant_tracking["bit_status"][0] & acu.TR_STAR_TRACK_ACTIVE) == acu.TR_STAR_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_STAR_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["star_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["star_track_el"]
                
                else:
                    self.logger.logWarning("Cannot identify selected tracking algorithm.  Set tracking algorithm position = (0,0)")
                    selected_tracking_algorithm_position_az = 0
                    selected_tracking_algorithm_position_el = 0

                #long_off = monitor_ant_tracking["prog_offset_az"]
                #lat_off = monitor_ant_tracking["prog_offset_el"]
                long_off = actual_position_az - selected_tracking_algorithm_position_az
                lat_off = actual_position_el - selected_tracking_algorithm_position_el

                self.logger.logDebug('timestamps_acu len = {}'.format(np.asarray(timestamps_acu).shape))
                self.logger.logDebug('actual_position_az len = {}'.format(np.asarray(actual_position_az).shape))
                self.logger.logDebug('actual_position_el len = {}'.format(np.asarray(actual_position_el).shape))
                self.logger.logDebug('long_off len = {}'.format(np.asarray(long_off).shape))
                self.logger.logDebug('lat_off len = {}'.format(np.asarray(lat_off).shape))

                interp_function_acu_az = interp1d(
                    timestamps_acu, actual_position_az, kind="cubic", fill_value="extrapolate"
                )
                interp_function_acu_el = interp1d(
                    timestamps_acu, actual_position_el, kind="cubic", fill_value="extrapolate"
                )
                interp_function_acu_long_off = interp1d(
                    timestamps_acu, long_off, kind="cubic", fill_value="extrapolate"
                )
                interp_function_acu_lat_off = interp1d(
                    timestamps_acu, lat_off, kind="cubic", fill_value="extrapolate"
                )

                az_at_timestamps_spectrometer = interp_function_acu_az(timestamps_spe)
                el_at_timestamps_spectrometer = interp_function_acu_el(timestamps_spe)
                long_off_at_timestamps_spectrometer = interp_function_acu_long_off(timestamps_spe)
                lat_off_at_timestamps_spectrometer = interp_function_acu_lat_off(timestamps_spe)

                datapar["AZIMUTH"] = az_at_timestamps_spectrometer
                datapar["ELEVATIO"] = el_at_timestamps_spectrometer
                datapar["LATOFF"] = lat_off_at_timestamps_spectrometer
                
                # If the program offset table scan pattern is an AZ,EL pattern, scale the LONGOFF
                # values based on the GLS projection.
                # Reference

                """ Sinusoidal (GLS) projection for the LONGOFF and LATOFF columns in the DATAPAR-MBFITS table. This
                was done to be more compatible with the Gildas software that is used for subsequent spectral line data
                reductions. Many bolometer data reduction packages also inherently assume this kind of projection. The
                GLS projection is very similar though not identical to SFL. Originally, it was described in AIPS memo 46
                by E. Greisen. The difference is that the cosine is applied to the latitude rather than the delta in latitude:
                GLS projection: 
                x = (φ - φ0 ) cos θ
                y = (θ - θ0 ) 
                """
                self.logger.logDebug("NLNGTYPE: {}".format(subscan_obj.metadata.datapar_mbfits_header['NLNGTYPE']))

                if subscan_obj.metadata.datapar_mbfits_header['NLNGTYPE']=="ALON-GLS":
                    self.logger.logDebug("program offset longitude is ALON-GLS (AZ,EL), scale LONGOFF for GLS projection.")
                    datapar["LONGOFF"] = long_off_at_timestamps_spectrometer * np.cos(np.radians(datapar["ELEVATIO"]))
                else:
                    self.logger.logDebug("program offset longitude is not ALON-GLS (AZ,EL). Do not scale LONGOFF.")
                    datapar["LONGOFF"] = long_off_at_timestamps_spectrometer
                
            except ValueError as e:
                self.logger.logError(e)
                self.logger.logError('Can not interpolate data when using MockACU')
            except Exception as e:
                self.logger.logError(e)
                self.logger.logError('Something went wrong while interpolating data')

        if self.opened is True:
            self.logger.logDebug("pipeline opened is True")
            self.logger.logDebug("write_enabled is True")
            # Clear the flow control flag
            self.write_subscan_finished.clear()
            self.logger.logDebug("QThreadpool.start WorkerWriteAfterSubscan")
            # Create a runnable and delegate the work to the Threadpool.
            worker = WorkerWriteAfterSubscan(
                self.write_subscan_finished,
                self.hdul,
                subscan_obj.metadata,
                arraydata,
                datapar,
            )

            self.threadpool.start(worker)
            self.packets_written += arraydata.shape[0]

        # clear finished subscan to save the ram
        subscan_obj.clear_buffer()

    def write_monitor(self, mbfits_headers):

        # Blocking wait for event write_subscan_finished to be set true by another thread before close the file.
        self.logger.logInfo(
            "Wait for write_subscan_finished event before write monitor data ..."
        )
        self.write_subscan_finished.wait(timeout=60.0)
        if self.write_subscan_finished.is_set() == False:
            self.logger.logDebug(
                "write_subscan_finished.set is still FALSE after timeout. raise exception"
            )
            ex = DataAggregatorErrorImpl.timeoutOnCloseErrorExImpl()
            ex.addData("errExpl", "Timeout while waiting for subscan to finish")
            raise ex

        if self.opened is True:
            self.logger.logDebug("pipeline opened is True")
            self.logger.logDebug("write_enabled is True")
            # Clear the flow control flag
            self.write_monitor_finished.clear()

            # Get Mutex lock so that other threads cannot append to the buffer lists
            # at the same time that we convert to numpy arrays.
            self.logger.debug("mutex lock")
            self.mutex.lock()

            self.logger.logDebug(
                "Append 1 blank item to MONITOR* buffer if length == 0"
            )
            if len(self.monitor_antenna_PacketHeader_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_PacketHeader_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_PacketHeader_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_PacketHeader_row,
                    )
                )

            if len(self.monitor_antenna_GeneralStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_GeneralStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_GeneralStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_GeneralStatus_row,
                    )
                )

            if len(self.monitor_antenna_SystemStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_SystemStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_SystemStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_SystemStatus_row,
                    )
                )

            if len(self.monitor_antenna_VertexShutterStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_VertexShutterStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_VertexShutterStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_VertexShutterStatus_row,
                    )
                )

            if len(self.monitor_antenna_AxisStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_AxisStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_AxisStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_AxisStatus_row,
                    )
                )

            if len(self.monitor_antenna_MotorStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_MotorStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_MotorStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_MotorStatus_row,
                    )
                )

            if len(self.monitor_antenna_TrackingStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_TrackingStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_TrackingStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_TrackingStatus_row,
                    )
                )

            if len(self.monitor_antenna_GeneralHexapodStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_GeneralHexapodStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_GeneralHexapodStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_GeneralHexapodStatus_row,
                    )
                )

            if len(self.monitor_antenna_HexapodStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_HexapodStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_HexapodStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_HexapodStatus_row,
                    )
                )

            if len(self.monitor_antenna_SpindleStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_SpindleStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_SpindleStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_SpindleStatus_row,
                    )
                )

            if len(self.monitor_antenna_HexapodTrackStatus_buffer) == 0:
                self.logger.logDebug(
                    "arraydata_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_HexapodTrackStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_HexapodTrackStatus_row,
                    )
                )

            if len(self.monitor_antenna_TrackingObjectStatus_buffer) == 0:
                self.logger.logDebug(
                    "monitor_antenna_TrackingObjectStatus_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_antenna_TrackingObjectStatus_buffer.append(
                    np.array(
                        0,
                        dtype=DataFormatMONITORfits.dtype_monitor_antenna_TrackingObjectStatus_row,
                    )
                )

            if len(self.monitor_weather_buffer) == 0:
                self.logger.logDebug(
                    "monitor_weather_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
                )
                self.monitor_weather_buffer.append(
                    np.array(
                        0, dtype=DataFormatMONITORfits.dtype_monitor_weather_row
                    )
                )

            if len(self.monitor_frontend_buffer) == 0:
                self.logger.logDebug('monitor_frontend_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file')
                self.monitor_frontend_buffer.append(
                    np.array(0, dtype=DataFormatMONITORfits.dtype_monitor_frontend_row)
                )

            self.logger.logDebug(
                "Convert the monitor buffers (python list) into Numpy structured arrays"
            )
            monitor_antenna_PacketHeader = np.asarray(
                self.monitor_antenna_PacketHeader_buffer
            )
            monitor_antenna_GeneralStatus = np.asarray(
                self.monitor_antenna_GeneralStatus_buffer
            )
            monitor_antenna_SystemStatus = np.asarray(
                self.monitor_antenna_SystemStatus_buffer
            )
            monitor_antenna_VertexShutterStatus = np.asarray(
                self.monitor_antenna_VertexShutterStatus_buffer
            )
            monitor_antenna_AxisStatus = np.asarray(
                self.monitor_antenna_AxisStatus_buffer
            )
            monitor_antenna_MotorStatus = np.asarray(
                self.monitor_antenna_MotorStatus_buffer
            )
            monitor_antenna_TrackingStatus = np.asarray(
                self.monitor_antenna_TrackingStatus_buffer
            )
            monitor_antenna_GeneralHexapodStatus = np.asarray(
                self.monitor_antenna_GeneralHexapodStatus_buffer
            )
            monitor_antenna_HexapodStatus = np.asarray(
                self.monitor_antenna_HexapodStatus_buffer
            )
            monitor_antenna_SpindleStatus = np.asarray(
                self.monitor_antenna_SpindleStatus_buffer
            )
            monitor_antenna_HexapodTrackStatus = np.asarray(
                self.monitor_antenna_HexapodTrackStatus_buffer
            )
            monitor_antenna_TrackingObjectStatus = np.asarray(
                self.monitor_antenna_TrackingObjectStatus_buffer
            )
            monitor_weather = np.asarray(self.monitor_weather_buffer)

            monitor_frontend = np.asarray(self.monitor_frontend_buffer)

            # Release the thread lock.
            self.logger.debug("mutex unlock")
            self.mutex.unlock()

            self.logger.logDebug("QThreadpool.start WorkerWriteMonitor")
            # Create a runnable and delegate the work to the Threadpool.
            worker = WorkerWriteMonitor(
                self.write_monitor_finished,
                self.hdul,
                mbfits_headers,
                monitor_antenna_PacketHeader,
                monitor_antenna_GeneralStatus,
                monitor_antenna_SystemStatus,
                monitor_antenna_VertexShutterStatus,
                monitor_antenna_AxisStatus,
                monitor_antenna_MotorStatus,
                monitor_antenna_TrackingStatus,
                monitor_antenna_GeneralHexapodStatus,
                monitor_antenna_HexapodStatus,
                monitor_antenna_SpindleStatus,
                monitor_antenna_HexapodTrackStatus,
                monitor_antenna_TrackingObjectStatus,
                monitor_weather,
                monitor_frontend,
            )
            self.threadpool.start(worker)

    # TODO: remove this function after modify other pipelines to the same structure as mbfits
    def clear_subscan_buffers(self): 
        """subscan finished, buffers written, clear buffers to prepare for next subscan"""
        list_to_delete = [
            self.arraydata_buffer,
            self.datapar_buffer,
        ]
        # Delete
        for item in list_to_delete:
            try:
                del item
            except NameError:
                self.logger.error("buffer | array not defined.  Nothing to delete")

        # Initialize buffers to empty list
        self.arraydata_buffer = []
        self.datapar_buffer = []

    def clear_monitor_buffers(self):
        """scan finished, buffers written, clear buffers to prepare for next scan"""
        list_to_delete = [
            self.monitor_antenna_PacketHeader_buffer,
            self.monitor_antenna_GeneralStatus_buffer,
            self.monitor_antenna_SystemStatus_buffer,
            self.monitor_antenna_VertexShutterStatus_buffer,
            self.monitor_antenna_AxisStatus_buffer,
            self.monitor_antenna_MotorStatus_buffer,
            self.monitor_antenna_TrackingStatus_buffer,
            self.monitor_antenna_GeneralHexapodStatus_buffer,
            self.monitor_antenna_HexapodStatus_buffer,
            self.monitor_antenna_SpindleStatus_buffer,
            self.monitor_antenna_HexapodTrackStatus_buffer,
            self.monitor_antenna_TrackingObjectStatus_buffer,
            self.monitor_weather_buffer,
            self.monitor_frontend_buffer,
        ]
        # Delete
        for item in list_to_delete:
            try:
                del item
            except NameError:
                self.logger.error("buffer | array not defined.  Nothing to delete")

        # Initialize buffers to empty list
        self.monitor_antenna_PacketHeader_buffer = []
        self.monitor_antenna_GeneralStatus_buffer = []
        self.monitor_antenna_SystemStatus_buffer = []
        self.monitor_antenna_VertexShutterStatus_buffer = []
        self.monitor_antenna_AxisStatus_buffer = []
        self.monitor_antenna_MotorStatus_buffer = []
        self.monitor_antenna_TrackingStatus_buffer = []
        self.monitor_antenna_GeneralHexapodStatus_buffer = []
        self.monitor_antenna_HexapodStatus_buffer = []
        self.monitor_antenna_SpindleStatus_buffer = []
        self.monitor_antenna_HexapodTrackStatus_buffer = []
        self.monitor_antenna_TrackingObjectStatus_buffer = []
        self.monitor_weather_buffer = []
        self.monitor_frontend_buffer = []

    
class WorkerWriteScanHeader(QtCore.QRunnable):
    """Worker thread to write scan header using resources from Threadpool"""

    def __init__(self, hdul, mbfits_headers, scan_data, febepar_data):
        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.hdul = hdul
        self.mbfits_headers = mbfits_headers
        self.scan_data = scan_data
        self.febepar_data = febepar_data
        self.cdb = CentralDB()
        self.scan_number = self.cdb.get(cdbConstants.SCANNUM)

    def run(self):
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        self.logger.info(
            "Add SCAN-MBFITS header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_scan()
        coldefs = DataFormatMBfits.generate_coldefs_scan(self.scan_data)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info("Add FEBEPAR-MBFITS header and data. Scan: {}".format(self.scan_number))
        hdr = DataFormatMBfits.generate_header_febepar(
            self.mbfits_headers.FEBEPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMBfits.generate_coldefs_febepar(self.febepar_data)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)
        # Flush data out to file
        self.hdul.flush()

        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


class WorkerWriteAfterSubscan(QtCore.QRunnable):
    """Worker thread to write drift using resources from Threadpool"""

    def __init__(
        self, write_subscan_finished, hdul, mbfits_headers, arraydata, datapar
    ):

        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.write_subscan_finished = write_subscan_finished
        self.hdul = hdul
        self.mbfits_headers = mbfits_headers
        self.arraydata = arraydata
        self.datapar = datapar

    def run(self):
        # Get the mutex to the hdul and file.  If another subscan finished too fast before this subscan,
        # It will have to wait before modifying the MBFITS HDU List object.
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        self.logger.info(
            "Add ARRAYDATA-MBFITS header and data. Scan: {}, Subscan: {}".format(
                self.mbfits_headers.arraydata_mbfits_header['SCANNUM'],
                self.mbfits_headers.arraydata_mbfits_header['SUBSNUM'],
            )
        )
        hdr = DataFormatMBfits.generate_header_arraydata(
            self.mbfits_headers.arraydata_mbfits_header
        )
        coldefs = DataFormatMBfits.generate_coldefs_arraydata(self.arraydata)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add DATAPAR-MBFITS header and data. Scan: {}, Subscan: {}".format(
                self.mbfits_headers.datapar_mbfits_header['SCANNUM'],
                self.mbfits_headers.datapar_mbfits_header['SUBSNUM'],
            )
        )
        hdr = DataFormatMBfits.generate_header_datapar(
            self.mbfits_headers.datapar_mbfits_header
        )
        coldefs = DataFormatMBfits.generate_coldefs_datapar(self.datapar)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        # Write to file on disk.
        # Sometimes it shows IOError: close() called during concurrent operation on the same file object
        # So catch the error, wait a little bit, and try again.
        try:
            self.logger.logDebug("try: mbfits hdul flush Subscan data")
            self.hdul.flush()
        except IOError:
            self.logger.logError("Caught IOError.  Wait 1 second and try again")
            time.sleep(1.0)
            self.logger.logDebug("repeat: mbfits hdul flush Subscan data")
            self.hdul.flush()

        # Set the event to let other threads know that this write operation is finished.
        self.logger.logDebug("set write_subscan_finished Event")
        self.write_subscan_finished.set()

        # Unlock access to the objects in this worker thread and allow other waiting worker threads
        # to continue
        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


class WorkerWriteMonitor(QtCore.QRunnable):
    """Worker thread to write drift using resources from Threadpool"""

    def __init__(
        self,
        write_monitor_finished,
        hdul,
        mbfits_headers,
        monitor_antenna_PacketHeader,
        monitor_antenna_GeneralStatus,
        monitor_antenna_SystemStatus,
        monitor_antenna_VertexShutterStatus,
        monitor_antenna_AxisStatus,
        monitor_antenna_MotorStatus,
        monitor_antenna_TrackingStatus,
        monitor_antenna_GeneralHexapodStatus,
        monitor_antenna_HexapodStatus,
        monitor_antenna_SpindleStatus,
        monitor_antenna_HexapodTrackStatus,
        monitor_antenna_TrackingObjectStatus,
        monitor_weather,
        monitor_frontend,
    ):

        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.write_monitor_finished = write_monitor_finished
        self.hdul = hdul
        self.mbfits_headers = mbfits_headers
        self.monitor_antenna_PacketHeader = monitor_antenna_PacketHeader
        self.monitor_antenna_GeneralStatus = monitor_antenna_GeneralStatus
        self.monitor_antenna_SystemStatus = monitor_antenna_SystemStatus
        self.monitor_antenna_VertexShutterStatus = monitor_antenna_VertexShutterStatus
        self.monitor_antenna_AxisStatus = monitor_antenna_AxisStatus
        self.monitor_antenna_MotorStatus = monitor_antenna_MotorStatus
        self.monitor_antenna_TrackingStatus = monitor_antenna_TrackingStatus
        self.monitor_antenna_GeneralHexapodStatus = monitor_antenna_GeneralHexapodStatus
        self.monitor_antenna_HexapodStatus = monitor_antenna_HexapodStatus
        self.monitor_antenna_SpindleStatus = monitor_antenna_SpindleStatus
        self.monitor_antenna_HexapodTrackStatus = monitor_antenna_HexapodTrackStatus
        self.monitor_antenna_TrackingObjectStatus = monitor_antenna_TrackingObjectStatus
        self.monitor_weather = monitor_weather
        self.monitor_frontend = monitor_frontend

        self.cdb = CentralDB()
        self.scan_number = self.cdb.get(cdbConstants.SCANNUM)
        self.date_obs = self.cdb.get(cdbConstants.DATE_OBS)
        self.header_data = {
            'SCANNUM': self.scan_number,
            'DATE_OBS': self.date_obs,
            'SUBSNUM': None
        }

    def run(self):
        # Get the mutex to the hdul and file.  If another subscan finished too fast before this subscan,
        # It will have to wait before modifying the MBFITS HDU List object.
        self.logger.logTrace("mutex lock")
        self.mutex.lock()



        self.logger.info(
            "Add MONITOR-ANT-PACKET header and data. Scan: {}".format(
                self.scan_number
            )
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_PacketHeader(self.header_data )
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_PacketHeader(
            self.monitor_antenna_PacketHeader
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-GENERAL header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_GeneralStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_GeneralStatus(
            self.monitor_antenna_GeneralStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-SYSTEM header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_SystemStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_SystemStatus(
            self.monitor_antenna_SystemStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-VERTEX header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_VertexShutterStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_VertexShutterStatus(
            self.monitor_antenna_VertexShutterStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-AXIS header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_AxisStatus(self.header_data )
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_AxisStatus(
            self.monitor_antenna_AxisStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-MOTOR header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_MotorStatus(self.header_data )
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_MotorStatus(
            self.monitor_antenna_MotorStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-TRACKING header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_TrackingStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_TrackingStatus(
            self.monitor_antenna_TrackingStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-HXP-GEN header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_GeneralHexapodStatus(self.header_data)
        coldefs = (
            DataFormatMBfits.generate_coldefs_monitor_antenna_GeneralHexapodStatus(
                self.monitor_antenna_GeneralHexapodStatus
            )
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-HXP header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_HexapodStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_HexapodStatus(
            self.monitor_antenna_HexapodStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-SPINDLE header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_SpindleStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_SpindleStatus(
            self.monitor_antenna_SpindleStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-HXP-TRACK header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_HexapodTrackStatus(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_HexapodTrackStatus(
            self.monitor_antenna_HexapodTrackStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-ANT-TRACKING-OBJECT header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_antenna_TrackingObjectStatus(self.header_data)
        coldefs = (
            DataFormatMBfits.generate_coldefs_monitor_antenna_TrackingObjectStatus(
                self.monitor_antenna_TrackingObjectStatus
            )
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add MONITOR-WEATHER header and data. Scan: {}".format(self.scan_number)
        )
        hdr = DataFormatMBfits.generate_header_monitor_weather(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_weather(
            self.monitor_weather
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)
  
        self.logger.info('Add MONITOR-FRONTEND header and data. Scan: {}'.format(self.scan_number))
        hdr = DataFormatMBfits.generate_header_monitor_frontend(self.header_data)
        coldefs = DataFormatMBfits.generate_coldefs_monitor_frontend(self.monitor_frontend)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        # Write to file on disk.
        # Sometimes it shows IOError: close() called during concurrent operation on the same file object
        # So catch the error, wait a little bit, and try again.
        try:
            self.logger.logDebug("try: mbfits hdul flush Monitor data")
            self.hdul.flush()
        except IOError:
            self.logger.logError("Caught IOError.  Wait 1 second and try again")
            time.sleep(1.0)
            self.logger.logDebug("repeat: mbfits hdul flush Monitor data")
            self.hdul.flush()

        # Set the event to let other threads know that this write operation is finished.
        # Perhaps we can find a smarter way to do this in the future.  Even using the threading.Event,
        # it still returns too fast before it is really finished.  hack: add time.sleep .  TODO improve this.
        self.logger.logDebug("set write_monitor_finished Event")
        self.write_monitor_finished.set()

        # Unlock access to the objects in this worker thread and allow other waiting worker threads
        # to continue
        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    p = PipelineMBfits()
