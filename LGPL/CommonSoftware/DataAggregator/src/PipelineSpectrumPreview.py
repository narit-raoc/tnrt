# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.02.23

import numpy as np

import DataAggregatorMod
from AbstractBasePipeline import AbstractBasePipeline


class PipelineSpectrumPreview(AbstractBasePipeline):
    """
    Docstring
    """

    # Define properties that are required by the parent class, but implemented in subclass
    # These properties are tagged with decorator @abstractproperty
    def __init__(self, metadata_ref):
        AbstractBasePipeline.__init__(self, metadata_ref)

        self.inputs = {}

        self.outputs = {
            "spectrum_preview": {
                "channel": DataAggregatorMod.CHANNEL_NAME_SPECTRUM_PREVIEW,
                "supplier": None,
            },
            "pipeline": {
                "channel": DataAggregatorMod.CHANNEL_NAME_PIPELINE_MBFITS,
                "supplier": None,
            },
        }

    def handler_spectrum_arraydata(self, header, arraydata_row):
        # Reshape and max-hold the full-resolution spectrum arraydata_row to a low-resolution spectrum preview and publish.
        try:
            spectrum_reshaped0 = np.reshape(
                arraydata_row["DATA"][0], (DataAggregatorMod.SPECTRUM_PREVIEW_NCHAN, -1)
            )
        except ValueError:
            spectrum_reshaped0 = self._pad_and_reshape(
                arraydata_row["DATA"][0], DataAggregatorMod.SPECTRUM_PREVIEW_NCHAN
            )

        try:
            spectrum_reshaped1 = np.reshape(
                arraydata_row["DATA"][1], (DataAggregatorMod.SPECTRUM_PREVIEW_NCHAN, -1)
            )
        except ValueError:
            spectrum_reshaped1 = self._pad_and_reshape(
                arraydata_row["DATA"][1], DataAggregatorMod.SPECTRUM_PREVIEW_NCHAN
            )

        # Sum the data on axis 1 of the 2D reshaped spectrum array to combine power from many bins of the the high-resolution
        # spectrum into the bins of the low-resolution spectrum.  Divide by number of elements added together to rescale the
        # data back to a resonable level that we can see in the preview graph.
        # Note: spectrum data that arrives in arraydata_row is already an integrated power spectrum (doesn't have phase / complex value data)
        # The units are linear, not decibel (dB).
        # Convert to decibel units here to prepare the preview data for graph
        spectrum_lowres_dB0 = np.max(10 * np.log10(spectrum_reshaped0), axis=1)
        spectrum_lowres_dB1 = np.max(10 * np.log10(spectrum_reshaped1), axis=1)

        # Make a packet that includes the low-resolution spectrum for preview
        # Note that we have to convert
        # numpy float64 and float32 types to standard python float
        # numpy array to standard python list
        # before we can send this struct through ACS Notification channel.  Because ACS / OmniORB can't serialize numpy data types
        spectrum_preview_packet = DataAggregatorMod.SpectrumPreview(
            float(arraydata_row["MJD"]),
            [
                list(spectrum_lowres_dB0.astype(float)),
                list(spectrum_lowres_dB1.astype(float)),
            ],
        )
        self.outputs["spectrum_preview"]["supplier"].publish_event(spectrum_preview_packet)

    def write_scan_header(self, mbfits_headers):
        pass

    def write_before_subscan(self, mbfits_headers):
        pass

    def write_after_subscan(self, mbfits_headers, subscan_number):
        pass

    def write_monitor(self, mbfits_headers):
        pass

    def clear_subscan_buffers(self):
        pass

    def clear_monitor_buffers(self):
        pass

    def _pad_and_reshape(self, array, nchannels_preview):
        """
        If the original number of spectrum channels is not a multiple of DataAggregatorMod.SPECTRUM_PREVIEW_NCHAN,
        pad the array with -120 (mimimum power in spectrum) value until it is a multiple of SPECTRUM_PREVIEW_NCHAN and reshape it to
        prepare for max-hold of data in the new spectrum preview bins.
        """
        defecit = nchannels_preview - len(array) % nchannels_preview
        self.logger.logTrace(
            "array size {} is not a multiple of {}. defecit: {}".format(
                len(array), nchannels_preview, defecit
            )
        )
        self.logger.logTrace(
            "zero pad to size {} (the next multiple of {})".format(
                len(array) + defecit, nchannels_preview
            )
        )
        array_padded = np.pad(array, (0, defecit), constant_values=-120)
        return np.reshape(array_padded, (nchannels_preview, -1))


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    p = PipelineSpectrumPreview()
