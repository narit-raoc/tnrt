import socket
import numpy as np
from astropy.time import Time
import os

import DataAggregatorMod
import AntennaDefaults
import atmosphere
import BackendMod
import ScanDefaults

# import weatherComNet # wait for NARIT's weather station
edd_fits_server_host = socket.gethostbyname("edd_fits_server")
edd_fits_server_port = int(5002)

metadata = {
    "antenna": AntennaDefaults.metadata,
    "atmosphere": atmosphere.opacityNotifyBlock(0.0, 0.0, 0.0),
    "backend": BackendMod.StatusNotifyBlock(0),
    "scan": ScanDefaults.metadata,
    # 'weather': weatherComNet.weatherDataBlock(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), # wait for NARIT's weather station
}

# Defaults for Pipeline status notification
# Create an object of all the data
pipeline_status = DataAggregatorMod.PipelineStatus(
    "time_str", "pipeline_name", "filename", False, 0, 0
)


DB_URL = os.getenv("DATA_ARCHIVE_CONNECTION")
DB_USER = os.getenv("DATA_ARCHIVE_USER")
DB_PASSWORD = os.getenv("DATA_ARCHIVE_PASS")
DB_NAME = os.getenv("DATA_ARCHIVE_DB")
DB_COLLECTION = os.getenv("DATA_ARCHIVE_COLLECTION")
ARCHIVE_FILE_PATH = "/TNRT_data/tnrt-storage-pool-TNRT_data-rsync/tnrt_data/"
ARCHIVE_FILE_URL_PATH = "/tnro/tnrt-storage-pool-TNRT_data-rsync/tnrt_data/"

STATION_NAME = 13 # constant for TNRO assigned by OPD team
PROJECT_DURATION = 1

MAP_SCAN_MODE_TO_ARCHIVE_ENUM = {
    "U": "UNDEFINED",
    "FOCUS": "FOCUS",
    "CAL": "CAL",
    "POINT": "CROSS",
    "MON": "MANUAL",
    "ON": "ONSOURCE",
    "ONOFF": "ONSOURCE",
    "MAP": "RASTER",
    "SKYDIP": "SKYDIP",
}
MAP_FRONTEND_TO_RECEIVER_TYPE = {
    "NULL": "NULL",
    "K": "K_BAND",
    "L": "L_BAND",
    "CX": "CX_BAND",
    "KU": "KU_BAND",
}

# FIXME: find the real meaning of observation mode, where to get it.
# enum = NULL, PSR, CAL, SEARCH, VLBI, TIMING, ALL, OTF, POINTING, SKYDIP, NONE
MAP_PROVISION_TO_OBSERVATION_MODE = {
    "NULL": "NULL",
    "NONE": "NONE",
    "TNRT_dualpol_spectrometer": "NULL",
    "TNRT_stokes_spectrometer": "NULL",
    "TNRT_dualpol_spectrometer_K": "NULL",
}

MAP_PROVISION_TO_OBSERVATION = {
    "NULL": "NONE",
    "NONE": "NONE",
    "TNRT_dualpol_spectrometer": "SPECTROMETER",
    "TNRT_stokes_spectrometer": "SPECTROMETER",
    "TNRT_dualpol_spectrometer_K": "SPECTROMETER",
}

MAP_BACKEND_TO_ENUM = {
    "EDD": "EDD",
    "HOLOFFT": "NULL",
    "HOLOSDR": "NULL", 
    "SKARAB": "NULL",
    "OPTICAL": "NULL",
    "POWERMETER": "NULL",
    "TEST": "NULL",
    "NULL": "NULL",
}

MAP_INDEX_TO_POLARIZATION = ["H", "V", "H", "V"]
MAP_INDEX_TO_NOISE = ["ON", "ON", "OFF", "OFF"]
