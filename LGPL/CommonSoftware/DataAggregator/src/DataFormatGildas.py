# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.06

import numpy as np
from pyclassfiller import code

# Gildas CLASS developer manual shows that the time origin is MJD-60549 for dobs and dred. why?
GAG_MJD_OFFSET = int(60549)

DEGREES_PER_RADIAN = 180.0 / np.pi
ARCSEC_PER_RADIAN = 3600.0 * DEGREES_PER_RADIAN

RADIANS_PER_DEGREE = 1.0 / DEGREES_PER_RADIAN
RADIANS_PER_ARCSEC = 1.0 / ARCSEC_PER_RADIAN

# Dictionaries to translate MBFITS codes / strings to Gildas codes
map_code_coord = {
	'ALON-GLS' : code.coord.hor,
	'ALAT-GLS' : code.coord.hor,
	'RA-SFL' : code.coord.equ,
	'DEC-SFL' : code.coord.equ,
	'GLON-SFL' : code.coord.gal,
	'GLAT-SFL' : code.coord.gal,
	}

map_apos = {
	'ALON' : np.pi / 2.0, # longitude drift horizontal coordinates (AZ drift)
	'RA' : np.pi / 2.0, # longitude drift equatorial coordiantes (RA drift)
	'ALAT' : 0.0, # latitude drift horizontal coordinates (EL drift)
	'DEC' : 0.0	 # latitude drift equatorial coordiantes (DEC drift)
	}

map_angle_key = {
	'ALON' : 'LONGOFF', 
	'ALAT' : 'LATOFF',
	'RA' : 'LONGOFF', 
	'DEC' : 'LATOFF',
	'U': 'LONGOFF', #FIXME: remove this when datapar header is filled properly
}

# Default values to use if Scan metadata is not complete and fails to lookup tables.
# Follow naming conventions of pyclassfiller.ClassObservation.head
class gen:
	num = int(0)
	ver = int(0)
	teles = 'TNRT'
	dobs = int(0) - GAG_MJD_OFFSET
	dred = int(0) - GAG_MJD_OFFSET
	qual = code.qual.unknown
	scan = int(1)
	kind = code.kind.cont
	subscan = int(1)
	ut = 0.0 # comes at runtim from DATAPAR
	st = 0.0 # comes at runtim from DATAPAR
	tau = 0.0
	tsys = 0.0
	time = 1.0
	xunit = code.xunit.freq

class pos:
	sourc = 'SourceName'	
	system = code.coord.hor
	equinox = 2000.0
	proj = code.proj.none
	lam = 0.0
	bet = 0.0
	projang = 0.0
	lamof = 0.0
	betof = 0.0

class spe:
	line  = 'LineName'
	restf = 0.0
	nchan = int(1)
	rchan = 0.0
	fres = 0.0
	vres = 0.0
	voff = 0.0
	bad = 0.0
	image = 0.0
	vtype = code.velo.obs
	vconv = code.conv.rad
	doppler = 0.0

class dri:
	freq = 0.0
	width = 0.0
	# comes at runtime:  from dimensions of DATAPAR binary table at end of subscan
	npoin = 1
	# comes at runtime: from the array index at which DATAPAR.LONGOFF | LATOFF is nearest to 0.
	# If the scan is perfectly symmetric, this will be DATAPAR_COLUMNS.shape[1] / 2  for shape[1] = odd number
	# and DATAPAR_COLUMNS.shape[1] / 2 + 0.5 for shape[1] = even number.
	rpoin = 1
	tref = 0.0
	aref = 0.0
	apos = 0.0
	# Comes at runtime: from DATAPAR binary table INTEGTIME, which comes from the backend
	# Likely to be constant throughout a subscan, but it is in the binary table not the header because
	# it could change during a subscan (why?)
	tres = 1.0
	# Comes at runtime: from calculation of DATAPAR.AZ / DATAPAR.EL binary tables. 
	ares = 1.0
	bad = 0.0
	ctype = code.coord.hor
	cimag = 0.0
	colla = 0.0 
	colle = 0.0

class head:
	gen = gen()
	pos = pos()
	spe = spe()
	dri = dri()

class defaults:
	head = head()	
	datay = np.zeros(dri.npoin)