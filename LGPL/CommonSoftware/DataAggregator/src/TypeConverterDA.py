# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.06.12

# Standard packages
import numpy as np

# CORBA IDL Types
import DataAggregatorMod

# Data formats
from DataFormatMONITORfits import DataFormatMONITORfits

def from_idl_struct(idl_struct, dt):
	'''
	Converts an object into a dictionary 
	and copies the values over by key
	Parameters
	----------
		idl_struct : Python object with members that was created by OmniORB from IDL struct

		dt : numpy.dtype . defined in data format module such as DataFormatMONITORfits
	'''
	# TODO (SS).  If this technique of filling the numpy array is too slow, find a more efficient way to
	# do this.  Perhaps don't create a new array every time.  Just pass a reference to already allocated numpy array
	# and fill the data.  It will add more code, so leave that as an option for later.
	
	# Intitialize a blank array of the correct type.
	# NOTE (SS.) key names in numpy array monitor_row must exactly match key names in IDL struct (OmniORB Python object)
	monitor_row = np.array(0, dtype=dt)
	
	for key, val in idl_struct.__dict__.items():
		#print('key: {}, val: {}'.format(key, val))
		monitor_row[key] = val
	return(monitor_row)

def from_list_idl_struct(list_idl_struct, dt):
	'''
	Iterates through a list of structs and transposes the data into a struct of lists
	'''
	monitor_row = np.array(0, dtype=dt)

	for index in np.arange(len(list_idl_struct)):
		for key, val in list_idl_struct[index].__dict__.items():
			#print('index: {} key: {}, val: {}'.format(index, key, val))
			# swap index and key to transpose the data
			monitor_row[key][index] = val
	return(monitor_row)


def from_idl_AntennaStatusNotifyBlock(idl_struct):
	'''
	Returns a tuple of 12 rows to use for MBFITS monitor format.
	'''
	# Packet header does not have another layer deep of parameters, so just copy the 2 top-level items manually
	monitor_antenna_PacketHeader_row = np.array(0, dtype=DataFormatMONITORfits.dtype_monitor_antenna_PacketHeader_row)
	monitor_antenna_PacketHeader_row['time_tcs_mjd'] = idl_struct.time_tcs_mjd
	monitor_antenna_PacketHeader_row['status_message_counter'] = idl_struct.status_message_counter
	
	# Other sections have a 1-level deep dictionary of data. Iterate through the object ditionary and copy.
	monitor_antenna_GeneralStatus_row = from_idl_struct(idl_struct.general_sts, DataFormatMONITORfits.dtype_monitor_antenna_GeneralStatus_row)
	monitor_antenna_SystemStatus_row = from_idl_struct(idl_struct.system_sts, DataFormatMONITORfits.dtype_monitor_antenna_SystemStatus_row)
	monitor_antenna_VertexShutterStatus_row = from_idl_struct(idl_struct.vertexshutter_sts, DataFormatMONITORfits.dtype_monitor_antenna_VertexShutterStatus_row)
	monitor_antenna_AxisStatus_row = from_list_idl_struct(idl_struct.axis_status,DataFormatMONITORfits.dtype_monitor_antenna_AxisStatus_row)
	monitor_antenna_MotorStatus_row = from_list_idl_struct(idl_struct.motor_status, DataFormatMONITORfits.dtype_monitor_antenna_MotorStatus_row)
	monitor_antenna_TrackingStatus_row = from_idl_struct(idl_struct.tracking_sts, DataFormatMONITORfits.dtype_monitor_antenna_TrackingStatus_row)
	monitor_antenna_GeneralHexapodStatus_row = from_idl_struct(idl_struct.general_hexapod_status, DataFormatMONITORfits.dtype_monitor_antenna_GeneralHexapodStatus_row)
	monitor_antenna_HexapodStatus_row = from_idl_struct(idl_struct.hexapod_sts, DataFormatMONITORfits.dtype_monitor_antenna_HexapodStatus_row)
	monitor_antenna_SpindleStatus_row = from_list_idl_struct(idl_struct.spindle_sts, DataFormatMONITORfits.dtype_monitor_antenna_SpindleStatus_row)
	monitor_antenna_HexapodTrackStatus_row = from_idl_struct(idl_struct.hexapod_track_sts, DataFormatMONITORfits.dtype_monitor_antenna_HexapodTrackStatus_row)
	monitor_antenna_TrackingObjectStatus_row = from_idl_struct(idl_struct.tracking_obj_sts, DataFormatMONITORfits.dtype_monitor_antenna_TrackingObjectStatus_row)

	return(monitor_antenna_PacketHeader_row,
		monitor_antenna_GeneralStatus_row,
		monitor_antenna_SystemStatus_row,
		monitor_antenna_VertexShutterStatus_row,
		monitor_antenna_AxisStatus_row,
		monitor_antenna_MotorStatus_row,
		monitor_antenna_TrackingStatus_row,
		monitor_antenna_GeneralHexapodStatus_row,
		monitor_antenna_HexapodStatus_row,
		monitor_antenna_SpindleStatus_row,
		monitor_antenna_HexapodTrackStatus_row,
		monitor_antenna_TrackingObjectStatus_row)