# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.13

import numpy as np
from scipy import interpolate

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph.Qt import QtWidgets

ARCSEC_PER_DEG = 3600


class PointingSimulator:
    """
    Generate spectrum data that we expect to see in a cross pointing scan.  Start off center, then
    sweep the antenna cross the gaussian magnitude profile of the radio source..
    """

    def __init__(
        self,
        subscan_duration=60.0,
        integration_time=1.0,
        amplitude=1,
        fwhm=160,
        offaxis=0,
        fs=800e6,
        nchan=1024,
        nusefeed=1,
    ):

        # Scan pattern time
        self.subscan_duration = subscan_duration

        # After each integration_time, the receiver backend publishes a packet of data.
        self.integration_time = integration_time

        # Numbe of polarizations / sections / noise diode ON | OFF states
        self.nusefeed = nusefeed
        self.nsections = nusefeed

        # Gaussian beam shape and simulate offset [arcsec]
        self.signal_amplitude = amplitude
        self.fwhm = fwhm
        self.offaxis = offaxis  # [arcsec]

        # Number of position data points to generate per subscan.
        self.npoin = int(subscan_duration / integration_time)

        # Offset to scan 10 * FWHM
        self.lim = (-10 * fwhm / 2, 10 * fwhm / 2)

        # List of angles go generate simulation data
        self.angles = np.linspace(self.lim[0], self.lim[1], self.npoin)

        # Spectrum parameters
        self.fs = fs
        self.nchan = nchan
        self.freq = (
            0 * fs
        )  # hard-code the sine wave frequency to at the center of spectrum (after fftshift)

        # -----------------------------------------------------------------------------
        # Simulate a spectrum that has 1 signal
        # -----------------------------------------------------------------------------
        # Time axis
        tt = np.arange(0, self.nchan) / self.fs

        # Generate a complex-valued cosine signal (spectral line)
        sig_tt = self.signal_amplitude * np.exp(1j * (2 * np.pi * self.freq * tt))

        # Calculate FFT Spectcrum of signal and noise separately
        spectrum_signal_complex = np.fft.fft(sig_tt * np.hamming(self.nchan)) / self.nchan
        spectrum_signal_power = np.abs(np.fft.fftshift(spectrum_signal_complex))

        # Use equation 3 from Nikom's report on telescope performance evaluation
        # Axes of this cube are [peak_angle][index_spectral_channel]
        gaussian = self.signal_amplitude * np.exp(
            (-4 * np.log(2) * (self.angles - self.offaxis) ** 2) / (self.fwhm**2)
        )
        self.data2D_signal = np.outer(gaussian, spectrum_signal_power)

        # Generate noise.  We cannot simply use outerproduct.  Must generate a new set of
        # noise data for each angle / integration.
        # Amplitude of noise to add (set signal to noise ratio)
        noise_amplitude = self.signal_amplitude * 1e-6

        self.data2D_noise = np.zeros(self.data2D_signal.shape)
        for row in np.arange(self.data2D_noise.shape[0]):
            noise_tt = noise_amplitude * (
                np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0])
            )
            spectrum_noise_complex = np.fft.fft(noise_tt * np.hamming(self.nchan)) / self.nchan
            spectrum_noise_power = np.abs(np.fft.fftshift(spectrum_noise_complex))
            self.data2D_noise[row] = spectrum_noise_power

        self.data2D = self.data2D_signal + self.data2D_noise

        # Multiply for a scale factor to compensate for the losses introduced by the hamming window
        # in the FFT.
        amplitude_compensate = 1 / np.max(self.data2D)

        self.data2D = self.data2D * amplitude_compensate

        self.peak_angle = np.argmax(gaussian)
        self.peak_channel = np.argmax(spectrum_signal_power)

        # Create a new array that has 1 more dimensions for section (eddfits) / NUSEFEED (MBfits) / polarization
        # NOTE: the dimensions selected here are convenient to fill the array with data.
        # swap axes after data is full to be consistent with MBfits dimensions
        data3D_temp = np.zeros((self.nsections, self.npoin, self.nchan))

        # Fill the 3D array with copies of the 2D generated data
        for section_index in range(data3D_temp.shape[0]):
            # Divide the data by section index so the simulation data has different magnitude
            # in each section.  This will be helpful for debugging.
            # Section 0 will be divided by 1 -- so no change.
            data3D_temp[section_index] = self.data2D / (section_index + 1)

        # Swap the axes to make the dimensions consistent with MBfits
        self.data3D = data3D_temp.swapaxes(0, 1)

    def preview(self):
        # -------------------------
        # Show the preview plot
        # -------------------------
        # Section to display (section 0 is X polarization in dualpol_spectrometer or Stokes I in stokes_spectrometer)
        section = 0

        # Create an interpolation function to make beatiful plot preview data
        angle_list_interp = np.linspace(self.lim[0], self.lim[1], 10 * self.npoin)
        interpfunc = interpolate.interp1d(
            self.angles, np.squeeze(self.data3D[:, section, self.peak_channel]), kind="cubic"
        )
        drift_interp = interpfunc(angle_list_interp)

        # Use CMY color for dark background
        color1 = "y"
        color2 = "m"
        color3 = "c"
        color4 = "w"

        # Create Pen and Brush objects to use in the graphs
        pen1 = pg.mkPen(color1)
        pen2 = pg.mkPen(color2)
        pen3 = pg.mkPen(color3)
        pen4 = pg.mkPen(color4)

        brush1 = pg.mkBrush(color1)
        brush2 = pg.mkBrush(color2)
        brush3 = pg.mkBrush(color3)
        brush4 = pg.mkBrush(color4)

        grid_opacity = 0.7

        str_title = "Pointing Simulator Preview"
        app = QtGui.QApplication([])
        vbox_slices = QtGui.QVBoxLayout()
        pg.setConfigOptions(antialias=True)

        # Power vs. Frequency (Spectrum)
        pi_spectrum = pg.PlotWidget(title="Simulated Pointing Scan - Spectrum at Selected Angle")
        vbox_slices.addWidget(pi_spectrum)
        pi_spectrum.addLegend()

        ff = np.linspace(
            -self.fs / 2, self.fs / 2, self.data3D[self.peak_angle].shape[1], endpoint=True
        )
        ydata = 10 * np.log10(self.data3D[self.peak_angle][section])

        pi_spectrum.plot(
            ff,
            ydata,
            name="no name",
            pen=pen1,
            symbol="o",
            symbolPen=pen1,
            symbolBrush=brush1,
        )

        # Some config after we add the lines
        axis_bottom1 = pg.AxisItem(
            orientation="bottom", text="Frequency offset [MHz] from Reference f0", units="Hz"
        )
        axis_bottom1.showLabel(True)

        axis_left1 = pg.AxisItem(orientation="left", text="Power [dB normalized]")
        axis_left1.showLabel(True)

        pi_spectrum.setAxisItems(
            axisItems={
                "bottom": axis_bottom1,
                "left": axis_left1,
            }
        )

        pi_spectrum.showGrid(True, True, alpha=grid_opacity)

        # Power vs. Angle (Drift)
        pi_drift = pg.PlotWidget(
            title="Simulated Pointing Scan - Drift Power at Selected Spectrum Channel FWHM = {} [arcsec]\n design offset =({}) [arcsec]".format(
                self.fwhm, self.offaxis
            )
        )
        vbox_slices.addWidget(pi_drift)
        pi_drift.addLegend()

        pi_drift.plot(
            self.angles,
            self.data3D[:, section, self.peak_channel],
            name="measured",
            pen=None,
            symbol="+",
            symbolPen=pen3,
            symbolBrush=brush3,
        )

        pi_drift.plot(
            angle_list_interp,
            drift_interp,
            name="interpolated",
            pen=pen4,
            symbol=None,
            symbolPen=pen4,
            symbolBrush=brush4,
        )

        # Some config after we add the lines
        axis_bottom1 = pg.AxisItem(
            orientation="bottom", text="{AZ | EL} offset from Tracking Coord [arcsec]"
        )
        axis_bottom1.showLabel(True)

        axis_left1 = pg.AxisItem(orientation="left", text="Power [arbitrary linear unit]")
        axis_left1.showLabel(True)

        pi_drift.setAxisItems(
            axisItems={
                "bottom": axis_bottom1,
                "left": axis_left1,
            }
        )

        pi_drift.showGrid(True, True, alpha=grid_opacity)

        # Set axis order show spectrogram time axis horizontal and frequency axis vertical
        pg.setConfigOptions(imageAxisOrder="col-major")

        # Create the spectrogram widget
        # Note that ImageView does not inherit from GraphicsItem.  It is a PyQt widget that has more interactive
        # functions than a simple GraphicsItemm.  Create an instance with parameter
        # view=pg.PlotItem() to show axis ticks and labels.
        imv_spectrogram = pg.ImageView(view=pg.PlotItem())
        imv_spectrogram.setPredefinedGradient("magma")

        # Configure the PlotItem to stretch the image to fit the window
        imv_spectrogram.view.setAspectLocked(False)
        imv_spectrogram.view.autoRange()

        # Set Y pixel coordinate origin  (pixel 0) to the bottom (default is top)
        # Coordinates for X pixels have origin (pixel x=0) at the left already
        imv_spectrogram.view.invertY(False)

        # Create axes
        imv_spectrogram_axis_top = pg.AxisItem("top")
        imv_spectrogram_axis_top.showLabel(True)
        imv_spectrogram_axis_top.setLabel("Integration Number")

        imv_spectrogram_axis_left = pg.AxisItem("left")
        imv_spectrogram_axis_left.showLabel(True)
        imv_spectrogram_axis_left.setLabel("FFT Channel Index")

        imv_spectrogram_axis_right = pg.AxisItem("right")
        imv_spectrogram_axis_right.showLabel(True)
        imv_spectrogram_axis_right.setLabel("FFT Channel Index")

        imv_spectrogram_axis_bottom = pg.AxisItem("bottom")
        imv_spectrogram_axis_bottom.showLabel(True)
        imv_spectrogram_axis_bottom.setLabel("Integration Number")

        imv_spectrogram.view.setAxisItems(
            axisItems={
                "top": imv_spectrogram_axis_top,
                "bottom": imv_spectrogram_axis_bottom,
                "left": imv_spectrogram_axis_left,
                "right": imv_spectrogram_axis_right,
            }
        )
        imv_spectrogram.view.showGrid(True, True)
        imv_spectrogram.timeLine.hide()
        intensity_datacube = np.squeeze(10 * np.log10(np.abs(self.data3D[:, section, :])))
        imv_spectrogram.setImage(intensity_datacube)

        win = QtGui.QMainWindow()
        win.resize(1200, 800)
        central_widget = QtWidgets.QWidget()
        win.setCentralWidget(central_widget)

        hbox_main = QtGui.QHBoxLayout(central_widget)
        hbox_main.addWidget(imv_spectrogram, 50)
        hbox_main.addLayout(vbox_slices, 50)

        win.show()

        dark_theme = QtGui.QPalette()
        dark_theme.setColor(QtGui.QPalette.Active, QtGui.QPalette.Window, QtGui.QColor(0, 0, 0))
        app.setPalette(dark_theme)
        app.exec_()
        
class HolographySimulator:
    def __init__(
        self,
        npoints=128,
        angular_resolution=20,
        time_per_angular_step=0.5,
        subscans_per_cal=1,
        integrations_per_calN=10,
        integrations_per_cal0=120,
        peak_magnitude_ref=1,
        peak_magnitude_tst=1,
        fwhm=160,
        offaxis_x=0,
        offaxis_y=0,
    ):
        """
        Generate numpy array of data points to simulate results of holography scan pattern using gaussian beam.
        This data can be written to the HOLODATA and DATAPAR binary tables of ALMATI-FITS file.

        Parameters
        ----------
        npoints : float, default = 128
                number of points in both horizontal and vertical directions to simulate.
                result array will have dimension npoints x npoints

        angular_resolution : float. [unit = arcsec], default = 10
                Angular sample rate between points in the array.  In the real system, this must be syncrhonized
                with the integration time of the receiver such that a packet of data arrives for each angular step.

        time_per_angular_step : float [unit = sec], default = 0.25
                Antenna will sweep smoothly on scan lines.  This is the desired transit time between data points.
                It determines the veloctiy of the antenna.
                IMPORTANT: It should be the same as the receiver integration time to align the receiver signal data
                to the map grid mechanical movement of the ACU control.  Post processing / data reduction interpolates
                the data onto the grid from precise timestamps, but TCS should align as close as possible during
                data acquisition to create the best results.

        subscans_per_cal : int, default = 1
                Number of SubscanLines to run before return to center point and measure on-source for calibration

        integrations_per_calN : int, default = 1
                Number of integrations on-source at center point where each integration time is the same as
                time_per_angular_step.  This parameter is given as an integer rather than duration in seconds
                so the operator chooses a the calibration duration that is a ratio of a single sample of
                antenna pattern sweep data.  For example, integration time 0.5 seconds at each scan measurement point.
                integrations_per_calN = 10 means the on-source calibration will receive 10 integrations of calibration = 5 seconds.

        integrations_per_cal0 : int, default = 1
                Number of integrations on-source at center point BEFORE the the OTF map pattern starts.
                This calibration is typically longer than the other calibrations measurements between each OTF map line
                described by `integrations_per_cal`
                Each integration duration is the same as time_per_angular_step.
                This parameter is given as an integer rather than duration in seconds
                so the operator chooses a the calibration duration that is a ratio of a single sample of
                antenna pattern sweep data.  For example, integration time 0.5 seconds at each OTF scan measurement point.
                integrations_per_cal0 = 120 means the on-source calibration will receive 120 integrations of calibration = 60 seconds.

        peak_magnitude_ref : float, default = 1
                constant magnitude of reference antenna signal

        peak_magnitude_ref : float, default = 1
                Magnitude of the gaussian beam at the center using Ku band feed and 40m reflector

        fwhm : float, [unit = arcsec], default = 160
                Full-width at half max.  The 3dB beamwidth of magnitude_otf_map in arcseconds.

        offaxis_x : float [unit = arcsec]
                Offset x (longitude coordinate AZ / RA) from center of the data array to center the Gaussian beam

        offaxis_y : float [unit = arcsec]
                Offset y (latitude coordinate EL / DEC) from center of the data array to center the Gaussian beam
        """
        # Gaussian beam shape and offset [arcsec]
        self.npoints_per_otf_line = int(npoints)
        self.angular_resolution = angular_resolution
        self.time_per_angular_step = time_per_angular_step
        self.subscans_per_cal = int(subscans_per_cal)
        self.integrations_per_calN = int(integrations_per_calN)
        self.integrations_per_cal0 = int(integrations_per_cal0)
        self.peak_magnitude_ref = peak_magnitude_ref
        self.peak_magnitude_tst = peak_magnitude_tst
        self.fwhm = fwhm
        self.offaxis_x = offaxis_x  # [arcsec]
        self.offaxis_y = offaxis_y  # [arcsec]

        # x and y limits are the same.  Use one variable for both axes
        self.lim_deg = (
            -0.5 * float(self.npoints_per_otf_line - 1) * self.angular_resolution / ARCSEC_PER_DEG,
            0.5 * float(self.npoints_per_otf_line - 1) * self.angular_resolution / ARCSEC_PER_DEG,
        )

        self.xx = np.linspace(self.lim_deg[0], self.lim_deg[1], self.npoints_per_otf_line)
        self.yy = np.linspace(self.lim_deg[0], self.lim_deg[1], self.npoints_per_otf_line)

        # Define a numpy data type to keep x,y,radius coordinates and complex-valued gaussian beam
        # simulated data together numpy structured array
        # This makes easy work to create the array with vectorized / broadcast numpy functions
        dtype_holodata_2D = np.dtype(
            [
                ("x", np.float64),
                ("y", np.float64),
                ("r", np.float64),
                ("ref_antenna_signal_watts", np.cdouble),  # complex-value
                ("tst_antenna_signal_watts", np.cdouble),
            ]
        )  # complex-value

        # Initialize arrays of zeros using coordinate data type and dimensions of xx and yy generated above
        # Assume the primary axis is x. (step through all "columns" x, then move to the next "row" y)
        # keep this as an instance variable to help debugging.  Real operation will actually use the list
        # of subscans that is generated from the final array.
        self.center_cal0 = np.zeros((1, self.integrations_per_cal0), dtype=dtype_holodata_2D)
        self.center_calN = np.zeros((1, self.integrations_per_calN), dtype=dtype_holodata_2D)
        self.otf_map = np.zeros((self.yy.size, self.xx.size), dtype=dtype_holodata_2D)

        # Generate a meshgrid of coordinates on which to calculate the antenna beam shape
        self.otf_map["x"], self.otf_map["y"] = np.meshgrid(self.xx, self.yy)
        # no need to generate coordinates for center_cal0 or center_calN because ideal case stays at (0,0)

        # Set the data for the reference antenna to be an ideal isotropic antenna. magnitude=1, phase=0
        self.center_cal0["ref_antenna_signal_watts"] = self.peak_magnitude_ref * np.exp(1j * 0)
        self.center_calN["ref_antenna_signal_watts"] = self.peak_magnitude_ref * np.exp(1j * 0)
        self.otf_map["ref_antenna_signal_watts"] = self.peak_magnitude_ref * np.exp(1j * 0)

        # From Yebes example file, amplitude difference is 6, phase difference is -~-90 degrees.
        # Fill all of the calibration data with similar values.
        self.center_cal0["tst_antenna_signal_watts"] = self.peak_magnitude_tst * np.exp(
            -1j * np.pi / 2
        )
        self.center_calN["tst_antenna_signal_watts"] = self.peak_magnitude_tst * np.exp(
            -1j * np.pi / 2
        )

        # Calculate radius to each point in the grid so we can use cylindrical coordinates for
        # Gaussian equation.  Don't need to create a theta angle coordinate because the Gaussian
        # pattern is symmetric around all angles theta
        self.otf_map["r"] = np.sqrt((self.otf_map["x"]) ** 2 + (self.otf_map["y"]) ** 2)

        # Generate a complex-valued gaussian beam cross-section and use this for the far_field_antenna_pattern
        # that we will measure with the real receiver.
        # Reference wikipedia
        beam_waist_arcsec = self.fwhm / (np.sqrt(2 * np.log(2)))
        radius_of_curvature = 1e-2

        beam_waist = beam_waist_arcsec / ARCSEC_PER_DEG

        # Generate magnitude profile
        # NOTE: add the smallest 32-bit fractional number to prevent truncation or divide by zero warnings
        # when we use this 64-bit simulated data with the 32-bit floats in ALMATI-FITS data format.
        magnitude_profile = np.exp(-1 * (self.otf_map["r"] ** 2) / (beam_waist**2))

        # This phase term is not correct, but it creates phase with a few wraps around 2*pi and a circular
        # pattern  Godd enough to confirm if the data file format has bugs.
        phase_profile = np.exp(-1j * (self.otf_map["r"] ** 2) / (2 * radius_of_curvature))

        # Combine the magnitude and phase profiles to make a complex-valued antenna pattern
        self.otf_map["tst_antenna_signal_watts"] = (
            self.peak_magnitude_tst * magnitude_profile * phase_profile
        )

    def preview(self):
        app = QtGui.QApplication([])
        win = QtGui.QMainWindow()
        win.resize(1200, 800)
        central_widget = QtWidgets.QWidget()
        win.setCentralWidget(central_widget)
        hbox_main = QtGui.QHBoxLayout(central_widget)

        # ---------
        # Magnitude
        # ---------
        # self.imv_beam_pattern_magnitude = pg.ImageView(view=self.pi_beam_pattern_magnitude)
        self.imv_beam_pattern_magnitude = pg.ImageView(view=pg.PlotItem())
        self.imv_beam_pattern_magnitude.getView().setAspectLocked(True)
        self.imv_beam_pattern_magnitude.setPredefinedGradient("magma")

        # Set Y pixel coordinate origin  (pixel 0) to the bottom (default is top)
        # Coordinates for X pixels have origin (pixel x=0) at the left already
        self.imv_beam_pattern_magnitude.getView().invertY(False)

        # Hide the slider for 3rd dimension of data (time steps of animation)
        # because this data is only 2D
        self.imv_beam_pattern_magnitude.timeLine.hide()

        # -----
        # Phase
        # -----
        self.imv_beam_pattern_phase = pg.ImageView(view=pg.PlotItem())
        self.imv_beam_pattern_phase.getView().setAspectLocked(True)

        # Use cyclic color map so that phase wrap from -pi to +pi looks
        self.imv_beam_pattern_phase.setPredefinedGradient("cyclic")

        # Set Y pixel coordinate origin  (pixel 0) to the bottom (default is top)
        # Coordinates for X pixels have origin (pixel x=0) at the left already
        self.imv_beam_pattern_phase.getView().invertY(False)

        # Hide the slider for 3rd dimension of data (time steps of animation)
        # because this data is only 2D
        self.imv_beam_pattern_phase.timeLine.hide()

        # Axes
        self.imv_beam_pattern_axis_list = [
            pg.AxisItem(
                orientation="bottom",
                text="Longitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="top",
                text="Longitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="left",
                text="Latitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="right",
                text="Latitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="bottom",
                text="Longitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="top",
                text="Longitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="left",
                text="Latitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="right",
                text="Latitude offset from tracking",
                units="deg",
            ),
        ]

        # Scale axis to convert degrees to arcseconds
        self.imv_beam_pattern_axis_list[0].setScale(3600)
        self.imv_beam_pattern_axis_list[2].setScale(3600)
        self.imv_beam_pattern_axis_list[4].setScale(3600)
        self.imv_beam_pattern_axis_list[6].setScale(3600)

        # Disable automatic SI prefix and always show unit = arcsec
        self.imv_beam_pattern_axis_list[0].enableAutoSIPrefix(False)
        self.imv_beam_pattern_axis_list[2].enableAutoSIPrefix(False)
        self.imv_beam_pattern_axis_list[4].enableAutoSIPrefix(False)
        self.imv_beam_pattern_axis_list[6].enableAutoSIPrefix(False)

        for axisitem in self.imv_beam_pattern_axis_list:
            axisitem.showLabel(True)

        self.imv_beam_pattern_magnitude.getView().setAxisItems(
            axisItems={
                "bottom": self.imv_beam_pattern_axis_list[0],
                "top": self.imv_beam_pattern_axis_list[1],
                "left": self.imv_beam_pattern_axis_list[2],
                "right": self.imv_beam_pattern_axis_list[3],
            }
        )

        self.imv_beam_pattern_phase.getView().setAxisItems(
            axisItems={
                "bottom": self.imv_beam_pattern_axis_list[4],
                "top": self.imv_beam_pattern_axis_list[5],
                "left": self.imv_beam_pattern_axis_list[6],
                "right": self.imv_beam_pattern_axis_list[7],
            }
        )

        far_field_antenna_pattern = (
            self.otf_map["tst_antenna_signal_watts"] / self.otf_map["ref_antenna_signal_watts"]
        )
        magnitude_otf_map = 10 * np.log10(np.abs(far_field_antenna_pattern))
        phase_rad_otf_map = np.angle(far_field_antenna_pattern)

        magnitude_otf_map_normalized = magnitude_otf_map - np.max(magnitude_otf_map)

        # NOTE: X and Y angular resolution and min, max range are the same because the data was
        # already interpolated onto a square grid in the function load_data().
        # If the observation data was recorded using some non-uniform spacing, the effect might
        # confusing here.  (Or if the Az offset * cos(EL) is not accounted for correctly)
        ARCSEC_PER_DEG = 3600
        x_angular_resolution = self.angular_resolution / ARCSEC_PER_DEG
        y_angular_resolution = x_angular_resolution

        x_min = self.xx[0]
        y_min = self.yy[0]

        self.imv_beam_pattern_magnitude.setImage(
            magnitude_otf_map_normalized,
            pos=[x_min, y_min],
            scale=[x_angular_resolution, y_angular_resolution],
        )

        self.imv_beam_pattern_phase.setImage(
            phase_rad_otf_map,
            pos=[x_min, y_min],
            scale=[x_angular_resolution, y_angular_resolution],
        )

        hbox_main.addWidget(self.imv_beam_pattern_magnitude, 50)
        hbox_main.addWidget(self.imv_beam_pattern_phase, 50)
        win.show()

        dark_theme = QtGui.QPalette()
        dark_theme.setColor(QtGui.QPalette.Active, QtGui.QPalette.Window, QtGui.QColor(0, 0, 0))

        app.setPalette(dark_theme)
        app.exec_()

if __name__ == "__main__":
    offaxis_arcsec = 0
    psim = PointingSimulator(30, 1, nusefeed=4, nchan=2048, offaxis=offaxis_arcsec)

    npoints = 128
    angular_resolution = 20
    time_per_angular_step = 0.5  # should be same as receiver integration time
    subscans_per_cal = 1
    integrations_per_calN = 10
    integrations_per_cal0 = 120
    peak_magntidue_ref = 8.5e-6  # mean value of first calibration in Yebes file TEST5772.FITS
    peak_magntidue_tst = 5.8e-5  # mean value of first calibration in Yebes file TEST5772.FITS
    fwhm = 160

    # Set integration time equal to angular step time.  Important if we want receiver data to
    # align with OTF map grid points.
    integration_time = time_per_angular_step
    integrations_per_line = npoints

    hsim = HolographySimulator(
        npoints,
        angular_resolution,
        time_per_angular_step,
        subscans_per_cal,
        integrations_per_calN,
        integrations_per_cal0,
        peak_magntidue_ref,
        peak_magntidue_tst,
        fwhm,
    )
