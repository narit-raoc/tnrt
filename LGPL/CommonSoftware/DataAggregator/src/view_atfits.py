# Standard python modules
import logging
import argparse
from argparse import RawTextHelpFormatter
import os
import sys

# Modules installed from pip
import coloredlogs
import numpy as np

from astropy.io import fits
from pyqtgraph.Qt import QtCore
import pyqtgraph as pg
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFrame
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from scipy import interpolate
from scipy.interpolate import griddata
from scipy.ndimage import gaussian_filter
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.spatial import cKDTree

# Import Qt widgets that are found by PyQgtraph.  
# No need to specify exactly which PyQt5, PyQt6, PySide6
# This approach makes upgrade of PyQt more easy without editing our source code.
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore

# NOTE: in the future, we prefer to import these modules using syntax
# from pyqtgraph.Qt.QtWidgets import <xxxx> to abstract out the actual
# version of PyQt or PySide.  Let pyqtgraph select.  This makes easier
# upgrading verions of Qt without editing code.  But for pyqtgraph 0.11.1
# and Python 3.6, we have to reference PyQt5 explicitly.from pyqtgraph.Qt.QtGui import QPalette, QColor
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5.QtWidgets import QTabWidget, QFrame, QHBoxLayout, QVBoxLayout, QGridLayout, QLabel

dark_theme = QPalette()

# Color group alias (to keep lines of code short)
dis = QPalette.Disabled
act = QPalette.Active
inac = QPalette.Inactive

txt_flags = QtCore.Qt.TextSelectableByMouse

bg_button = 30
bg_gray = 60
# Central roles.  If ColorGroup is not not specified, default is QPalette.Active
dark_theme.setColor(act, QPalette.Window, QColor(0, 0, 0))
dark_theme.setColor(act, QPalette.WindowText, QColor(255, 255, 255))
dark_theme.setColor(act, QPalette.Base, QColor(bg_gray, bg_gray, bg_gray))
dark_theme.setColor(act, QPalette.AlternateBase, QColor(66, 66, 66))
dark_theme.setColor(act, QPalette.ToolTipBase, QColor(53, 53, 53))
dark_theme.setColor(act, QPalette.ToolTipText, QColor(180, 180, 180))
# dark_theme.setColor(QPalette.PlaceHolderText, QColor(180, 180, 180))
dark_theme.setColor(act, QPalette.Text, QColor(255, 255, 255))
dark_theme.setColor(act, QPalette.Button, QColor(bg_button, bg_button, bg_button))
dark_theme.setColor(act, QPalette.ButtonText, QColor(255, 255, 255))
dark_theme.setColor(act, QPalette.BrightText, QColor(255, 255, 255))

# Colors for 3D rendered edges of buttons, scrollbars, ...
dark_theme.setColor(act, QPalette.Light, QColor(180, 180, 180))
dark_theme.setColor(act, QPalette.Midlight, QColor(90, 90, 90))
dark_theme.setColor(act, QPalette.Dark, QColor(35, 35, 35))
dark_theme.setColor(act, QPalette.Mid, QColor(35, 35, 35))
dark_theme.setColor(act, QPalette.Shadow, QColor(50, 50, 50))

# Selected items
dark_theme.setColor(act, QPalette.Highlight, QColor(42, 130, 218))
dark_theme.setColor(act, QPalette.HighlightedText, QColor(180, 180, 180))

# Hyperlink  / URL
dark_theme.setColor(act, QPalette.Link, QColor(56, 252, 196))
dark_theme.setColor(act, QPalette.LinkVisited, QColor(56, 252, 196))

# For widgets that don't have a color role assigned
dark_theme.setColor(act, QPalette.NoRole, QColor(66, 66, 66))

# Colors for disabled items in the current focus window (QPalette.Disabled)
dark_theme.setColor(dis, QPalette.WindowText, QColor(127, 127, 127))
dark_theme.setColor(dis, QPalette.Text, QColor(127, 127, 127))
dark_theme.setColor(dis, QPalette.ButtonText, QColor(127, 127, 127))
dark_theme.setColor(dis, QPalette.Highlight, QColor(80, 80, 80))
dark_theme.setColor(dis, QPalette.HighlightedText, QColor(127, 127, 127))


class QHLine(QFrame):
    def __init__(self):
        super().__init__()
        self.setFrameShape(QFrame.HLine)

def configure_logger(logger):
    logger.setLevel(logging.DEBUG)
    fmt_scrn = "%(asctime)s [%(levelname)s] %(funcName)s(): %(message)s"
    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red", "bright": True},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta", "bright": True, "bold": True},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }

    formatter_screen = logging.Formatter(fmt=fmt_scrn)
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)
    handler_screen.setLevel(logging.DEBUG)
    logger.addHandler(handler_screen)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, filename):
        """
        Create MainWindow QtGui
        """
        super(MainWindow, self).__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        configure_logger(self.logger)
        self.logger.info("Started log for {}".format(self.logger.name))

        # Initialize a vertical layout for the select area
        self.layout_select = QtWidgets.QVBoxLayout()

        # Initialize a flag to track if the beam pattern should be updated
        self.beam_pattern_valid = False

        # Load data from file and index subscans
        self.load_data(filename)

        # Retrieve data for both HOLO and CORR
        self.holo_data, self.corr_data = self.retrieve_data(filename)

        # Create widgets, add to layouts and initialize all values that
        # remain constant and do not depend on selected subscan.
        self.create_widgets(filename)

        # Add all of the new widgets to the GUI layout
        self.layout_widgets()

        # Conditionally set the data in beam pattern widgets if beam pattern is valid
        if self.beam_pattern_valid:
            self.update_data_beam_pattern()

    def shutdown(self):
        self.logger.debug("shutdown")
        self.hdul.close()

    def interpolate_onto_linspace_angles(self, angles_measured, data_to_interpolate):
        interpolation_function = interpolate.interp1d(
            angles_measured, data_to_interpolate, kind="cubic"
        )
        return interpolation_function(self.angles_linspace)

    def load_data(self, filename):
        self.hdul = fits.open(filename)

        # -----------------------------------------------------------------------------
        # Separate the calibration and and line scan data into separate
        # multi-dimensional data arrays
        # -----------------------------------------------------------------------------
        # Create a numpy data type to keep all of the data that we are interested from the file
        # and make convenient calculation and graphs.
        # Some data comes from DATAPAR and some comes from HOLODATA

        dtype_datapar_holodata_integration = np.dtype(
            [
                ("MJD", np.float64),
                ("LONGOFF", np.float32),
                ("LATOFF", np.float32),
                ("HOLOSS", np.float32),
                ("HOLORR", np.float32),
                ("HOLOQQ", np.float32),
                ("HOLOSR", np.float32),
                ("HOLOSQ", np.float32),
                ("HOLOQR", np.float32),
                ("AMPREF", np.float32),
                ("AMPTST", np.float32),
                ("AMPREL", np.float32),
                ("PHAREL", np.float32),
                ("FREQMX", np.float32),
            ]
        )

        # At first, we don't know the number of:
        # * data points / integrations are in the line scans
        # * total number of scan lines
        # * integrations per calibration subscan on center
        # * integrations per first calibration subscan
        # * subscans per calibration

        # We can separate the calibration data from the holography pattern line scan
        # data using the header item DATAPAR_MBFITS_HEADER.SUBSTYPE.
        # 'CORR' is calibration data
        # 'HOLO' is holo scan line data

        # Assume that the data file HDU list follows this pattern:
        # * hdul[0]	is the primary file header
        # * hdul[odd index] is a DATAPAR hdu
        # * hdul[even index] is a HOLODATA hdu
        # * HOLODATA comes after DATAPAR of the same subscan.
        # For example (hdul[3] is DATAPAR, and hdul[4] is HOLODATA pair with hdul[3])

        # If the data file does not follow this pattern, we have to do something
        # more complicated to organize the data.

        dict_hdulists = {
            "CORR": {"DATAPAR-ALMATI": [], "HOLODATA-ALMATI": []},
            "HOLO": {"DATAPAR-ALMATI": [], "HOLODATA-ALMATI": []},
        }

        # Traverse odd index numbers to find DATAPAR.  read index+1 to find HOLODATA
        for k in np.arange(1, len(self.hdul), 2):
            obsmode = self.hdul[k].header["OBSMODE"]
            self.logger.debug(
                "reading (DATAPAR: subscan={:3d} integrations={:4d}, HOLODATA: subscan={:3d} integrations={:4d}) obsmode={}".format(
                    self.hdul[k].header["OBS-NUM"],
                    len(self.hdul[k].data),
                    self.hdul[k + 1].header["OBS-NUM"],
                    len(self.hdul[k + 1].data),
                    obsmode,
                )
            )

            try:
                dict_hdulists[obsmode]["DATAPAR-ALMATI"].append(self.hdul[k])
                dict_hdulists[obsmode]["HOLODATA-ALMATI"].append(self.hdul[k + 1])
            except KeyError as e:
                self.logger.warning(
                    "Current HDU index: {}, has OBSMODE: {} not in CORR / HOLO".format(k, obsmode)
                )

            # Now that we have separated the 'CORR' calibrations and 'HOLO' antenna pattern measurements,
            # Count how many scan lines are in the antenna pattern measurement
            n_scan_lines = len(dict_hdulists["HOLO"]["DATAPAR-ALMATI"])
            n_integrations = n_scan_lines

        # Brief report of what we found
        self.logger.info(
            "Found {} CORR subscans".format(len(dict_hdulists["CORR"]["DATAPAR-ALMATI"]))
        )
        self.logger.info(
            "Found {} HOLO subscans".format(len(dict_hdulists["HOLO"]["DATAPAR-ALMATI"]))
        )

        # In the ideal case, the number of integrations per scan line is the same as number of scan lines (square grid)
        # However, sometimes the HoloFFT backend produces +1 / -1 packet of data during a scan line due to lack
        # of careful synchronization between HoloFFT data acquisition and ACU scan pattern control.
        # This +/- 1 is not a problem becuase each integration is tagged with LONGOFF (x) and LATOFF (y) coordinates.
        # We can use the actual coordinates to interpolate the data onto a square grid of uniform-spaced coordinates where number
        # of integrations == number of scan lines..

        # Check the first scan line and see if it scans in longitude 'x' or latitude 'y'
        # All other scan lines should scan the same direction, so no need to check this after the first line
        try:
            hdu = dict_hdulists["HOLO"]["DATAPAR-ALMATI"][0]
        except IndexError:
            self.logger.error("File does not contain any valid data. Exit now")
            sys.exit(0)
        span_lat = np.max(hdu.data["LATOFF"]) - np.min(hdu.data["LATOFF"])
        span_lon = np.max(hdu.data["LONGOFF"]) - np.min(hdu.data["LONGOFF"])

        if span_lon > span_lat:
            # Scan lines go in 'x' longitude direction. Interpolate along this axis
            fast_axis_key = "LONGOFF"
        else:
            # Scan lines go in 'y' latitude direction. Interpolate along this axis
            fast_axis_key = "LATOFF"

        # Traverse all of the scan lines and accumulate a list of maximum and minimum angles
        max_angles = np.zeros(n_scan_lines)
        min_angles = np.zeros(n_scan_lines)

        for k in np.arange(n_scan_lines):
            min_angles[k] = np.min(dict_hdulists["HOLO"]["DATAPAR-ALMATI"][k].data[fast_axis_key])
            max_angles[k] = np.max(dict_hdulists["HOLO"]["DATAPAR-ALMATI"][k].data[fast_axis_key])

        # From the min and max angles of each scan line, select the min and max that gives the shortest scan line
        # So that longer scan lines can be interpolated onto the shorter scan line (not extrapolate short line onto long line)
        select_min_angle = np.max(min_angles)
        select_max_angle = np.min(max_angles)

        # We want to center the data, so choose the smaller of the two angle ranges and use that to cut out more data.
        select_angle = np.min([np.abs(select_min_angle), np.abs(select_max_angle)])

        # Generate an array of equally-spaced angles onto which we will interpolate
        self.angles_linspace = np.linspace(
            -1 * select_angle, select_angle, n_integrations, endpoint=True
        )

        self.angular_resolution = np.mean(np.diff(self.angles_linspace))

        self.logger.debug(
            "angular_resolution: {} [deg], {} [arcsec]".format(
                self.angular_resolution, 3600 * self.angular_resolution
            )
        )

        try:

            # Create the empty array of 0 with correct dimensions.
            otf_map = np.zeros((n_scan_lines, n_integrations), dtype=dtype_datapar_holodata_integration)

            for (otf_map_scan_line, hdu_datapar, hdu_holodata) in zip(
                otf_map,
                dict_hdulists["HOLO"]["DATAPAR-ALMATI"],
                dict_hdulists["HOLO"]["HOLODATA-ALMATI"],
            ):
                otf_map_scan_line["MJD"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_datapar.data["MJD"]
                )
                otf_map_scan_line["LATOFF"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_datapar.data["LATOFF"]
                )
                otf_map_scan_line["LONGOFF"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_datapar.data["LONGOFF"]
                )
                otf_map_scan_line["HOLOSS"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["HOLOSS"]
                )
                otf_map_scan_line["HOLORR"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["HOLORR"]
                )
                otf_map_scan_line["HOLOQQ"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["HOLOQQ"]
                )
                otf_map_scan_line["HOLOSR"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["HOLOSR"]
                )
                otf_map_scan_line["HOLOSQ"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["HOLOSQ"]
                )
                otf_map_scan_line["HOLOQR"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["HOLOQR"]
                )
                otf_map_scan_line["AMPREF"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["AMPREF"]
                )
                otf_map_scan_line["AMPTST"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["AMPTST"]
                )
                otf_map_scan_line["AMPREL"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["AMPREL"]
                )
                otf_map_scan_line["PHAREL"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["PHAREL"]
                )
                otf_map_scan_line["FREQMX"] = self.interpolate_onto_linspace_angles(
                    hdu_datapar.data[fast_axis_key], hdu_holodata.data["FREQMX"]
                )
        except ValueError as e:
            self.logger.error(f"Interpolation error: {e}. Skipping beam pattern.")
            return  # Skip further execution if interpolation fails

        # Note, add 1E-10 (-100dB) to make  floor in the 3D graph and remove the
        # warning about divide by zero in log10 function if data contains zeros
        # (usually from simulation data. Real data should have a noise floor around -80 dB)
        try:
            magnitude_otf_map = 10 * np.log10(np.abs(otf_map["AMPREF"]) + 1e-10)
            #magnitude_otf_map = 10 * np.log10(otf_map["HOLORR"] * 8) + 30
            self.magnitude_otf_map_normalized = magnitude_otf_map - np.max(magnitude_otf_map)
            #self.magnitude_otf_map_normalized = magnitude_otf_map
            self.phase_rad_otf_map = np.radians(otf_map["PHAREL"])

            self.bean_pattern_valid = True
        except ValueError as e:
            self.logger.error(f"Interpolation error: {e}. Skipping beam pattern.")
            self.beam_pattern_valid = False  # Set flag to False if there's an interpolation error

    def retrieve_data(self, filename):
        # Open FITS file
        hdul = fits.open(filename)

        # Create dictionaries for classification
        dict_hdulists = {
            "CORR": {"DATAPAR-ALMATI": [], "HOLODATA-ALMATI": []},
            "HOLO": {"DATAPAR-ALMATI": [], "HOLODATA-ALMATI": []},
        }

        # Separate CORR and HOLO subscans
        # Odd indexes: DATAPAR, Even indexes: HOLODATA
        for k in np.arange(1, len(hdul), 2):
            obsmode = hdul[k].header["OBSMODE"]
            if obsmode in dict_hdulists:
                dict_hdulists[obsmode]["DATAPAR-ALMATI"].append(hdul[k])
                dict_hdulists[obsmode]["HOLODATA-ALMATI"].append(hdul[k + 1])

        # Extract LATOFF, LONGOFF, and HOLOSS for both CORR and HOLO
        def extract_data(datapars, holodatas):
            latoff_list = []
            longoff_list = []
            holoss_list = []
            holorr_list = []
            holoqq_list = []
            holosr_list = []
            holosq_list = []
            holoqr_list = []
            ampref_list = []
            amptst_list = []
            amprel_list = []
            pharel_list = []
            freqmx_list = []

            line_count = 0
            for datapar, holodata in zip(datapars, holodatas):
                line_count = line_count + 1
                min_length = min(len(datapar.data['LONGOFF']), len(datapar.data['LATOFF']), len(holodata.data['HOLOSS']))

                # Trim all lists to the same length
                latoff_list.append(datapar.data["LATOFF"][:min_length])             

                print(f"======================line: {line_count}")
                print(datapar.data["LONGOFF"])
                longoff_list.append(datapar.data["LONGOFF"][:min_length])

                holoss_list.append(holodata.data["HOLOSS"][:min_length])

                print(f"======================line: {line_count}")
                print(10 * np.log10(holodata.data["HOLORR"] * 8)  + 30)
                holorr_list.append(holodata.data["HOLORR"][:min_length])

                holoqq_list.append(holodata.data["HOLOQQ"][:min_length])
                holosr_list.append(holodata.data["HOLOSR"][:min_length])
                holosq_list.append(holodata.data["HOLOSQ"][:min_length])
                holoqr_list.append(holodata.data["HOLOQR"][:min_length])
                ampref_list.append(holodata.data["AMPREF"][:min_length])
                amptst_list.append(holodata.data["AMPTST"][:min_length])
                amprel_list.append(holodata.data["AMPREL"][:min_length])
                pharel_list.append(holodata.data["PHAREL"][:min_length])
                freqmx_list.append(holodata.data["FREQMX"][:min_length])

            # Pad all lists to the same length
            max_length = max(len(item) for item in latoff_list)

            return {
                'LATOFF': np.concatenate(latoff_list),
                'LONGOFF': np.concatenate(longoff_list),
                'HOLOSS': np.concatenate(holoss_list),
                'HOLORR': np.concatenate(holorr_list),
                'HOLOQQ': np.concatenate(holoqq_list),
                'HOLOSR': np.concatenate(holosr_list),
                'HOLOSQ': np.concatenate(holosq_list),
                'HOLOQR': np.concatenate(holoqr_list),
                'AMPREF': np.concatenate(ampref_list),
                'AMPTST': np.concatenate(amptst_list),
                'AMPREL': np.concatenate(amprel_list),
                'PHAREL': np.concatenate(pharel_list),
                'FREQMX': np.concatenate(freqmx_list),
            }

        # Extract data for HOLO and CORR
        holo_data = extract_data(dict_hdulists["HOLO"]["DATAPAR-ALMATI"], dict_hdulists["HOLO"]["HOLODATA-ALMATI"])
        corr_data = extract_data(dict_hdulists["CORR"]["DATAPAR-ALMATI"], dict_hdulists["CORR"]["HOLODATA-ALMATI"])

        # Replace zeros or invalid values to avoid log10 errors
        corr_data['HOLOSS'] = np.where(corr_data['HOLOSS'] <= 0, 1e-10, corr_data['HOLOSS']) 
        holo_data['HOLOSS'] = np.where(holo_data['HOLOSS'] <= 0, 1e-10, holo_data['HOLOSS'])
        corr_data['HOLORR'] = np.where(corr_data['HOLORR'] <= 0, 1e-10, corr_data['HOLORR'])
        holo_data['HOLORR'] = np.where(holo_data['HOLORR'] <= 0, 1e-10, holo_data['HOLORR'])


        return holo_data, corr_data

    def create_widgets(self, filename):
        pg.setConfigOptions(antialias=True)
        self.resize(1920, 1080)
        self.setWindowTitle(filename)
        self.create_widgets_select()
        self.create_widgets_tabs()
        self.create_widgets_holo_corr_pattern()
        self.create_widgets_magnitude_pattern()
        self.create_widgets_beam_pattern()

    def create_widgets_select(self):
        # Create a container widget to hold the radio buttons with a fixed width
        self.select_widget = QtWidgets.QWidget()
        self.select_widget.setFixedWidth(150)

        # Create a vertical layout for the radio buttons inside the container
        self.layout_select = QtWidgets.QVBoxLayout(self.select_widget)  # Set layout directly to the widget
        self.layout_select.setAlignment(QtCore.Qt.AlignTop)

        # Create the label for the selection area
        self.label_select = QtWidgets.QLabel("Select Data:")
        self.layout_select.addWidget(self.label_select)
        self.layout_select.addWidget(QHLine())
        
        # """
        # We didn't display the 'HOLOQQ' data because it's identical to 'HOLOSS', and HOLOSQ is set to 0 (hardcoded).
        # For now, we're not showing it. Once we understand the purpose of these data, wecan easily add the 'HOLOQQ', 
        # 'HOLOSR', and 'HOLOQR' parameters as well!
        # """
        
        # for index, param in enumerate(["HOLOSS", "HOLORR", "AMPREF", "AMPTST", "AMPREL", "PHAREL", "FREQMX"]):
        #     radio_button = QtWidgets.QRadioButton(param)
        #     self.radio_group.addButton(radio_button, id=index)
        #     self.layout_select.addWidget(radio_button)
        #     self.radio_buttons[param] = radio_button
    
    def create_widgets_tabs(self):
        # Create the top-level tab group
        self.tabgroup_graphs = QTabWidget()

        # Create empty container widget for each tab
        self.tab_holo_corr_pattern = QtWidgets.QWidget()
        self.tab_magnitude_pattern = QtWidgets.QWidget()
        self.tab_beam_pattern = QtWidgets.QWidget()
        self.tab_todo = QtWidgets.QWidget()

        # Add tab container widgets to the tab group
        self.tabgroup_graphs.addTab(self.tab_holo_corr_pattern, "HOLO Data")
        self.tabgroup_graphs.addTab(self.tab_magnitude_pattern, "FFT Analyzer Data")
        self.tabgroup_graphs.addTab(self.tab_beam_pattern, "Beam Pattern")
        self.tabgroup_graphs.addTab(self.tab_todo, "...")

    def create_widgets_beam_pattern(self):
        self.label_beam_pattern_magnitude = QLabel("Magnitude (color scale: dB no cal)")
        self.label_beam_pattern_magnitude.setAlignment(QtCore.Qt.AlignCenter)
        self.label_beam_pattern_phase = QLabel("Phase (color scale: radian)")
        self.label_beam_pattern_phase.setAlignment(QtCore.Qt.AlignCenter)

        pg.setConfigOptions(imageAxisOrder="col-major")

        # Note that ImageView does not inherit from GraphicsItem.  It is a PyQt widget
        # that has more interactive functions than a simple GraphicsItemm.
        # Create an instance with parameter view=pg.PlotItem() to show
        # axis ticks and labels

        # ---------
        # Magnitude
        # ---------
        # self.imv_beam_pattern_magnitude = pg.ImageView(view=self.pi_beam_pattern_magnitude)
        self.imv_beam_pattern_magnitude = pg.ImageView(view=pg.PlotItem())
        self.imv_beam_pattern_magnitude.getView().setAspectLocked(True)
        self.imv_beam_pattern_magnitude.setPredefinedGradient("magma")

        # Set Y pixel coordinate origin  (pixel 0) to the bottom (default is top)
        # Coordinates for X pixels have origin (pixel x=0) at the left already
        self.imv_beam_pattern_magnitude.getView().invertY(False)

        # Hide the slider for 3rd dimension of data (time steps of animation)
        # because this data is only 2D
        self.imv_beam_pattern_magnitude.timeLine.hide()

        # -----
        # Phase
        # -----
        self.imv_beam_pattern_phase = pg.ImageView(view=pg.PlotItem())
        self.imv_beam_pattern_phase.getView().setAspectLocked(True)

        # Use cyclic color map so that phase wrap from -pi to +pi looks
        self.imv_beam_pattern_phase.setPredefinedGradient("cyclic")

        # Set Y pixel coordinate origin  (pixel 0) to the bottom (default is top)
        # Coordinates for X pixels have origin (pixel x=0) at the left already
        self.imv_beam_pattern_phase.getView().invertY(False)

        # Hide the slider for 3rd dimension of data (time steps of animation)
        # because this data is only 2D
        self.imv_beam_pattern_phase.timeLine.hide()

        # Axes
        self.imv_beam_pattern_axis_list = [
            pg.AxisItem(
                orientation="bottom",
                text="Longitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="top",
                text="Longitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="left",
                text="Latitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="right",
                text="Latitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="bottom",
                text="Longitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="top",
                text="Longitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="left",
                text="Latitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="right",
                text="Latitude offset from tracking",
                units="deg",
            ),
        ]

        # Scale axis to convert degrees to arcseconds
        self.imv_beam_pattern_axis_list[0].setScale(3600)
        self.imv_beam_pattern_axis_list[2].setScale(3600)
        self.imv_beam_pattern_axis_list[4].setScale(3600)
        self.imv_beam_pattern_axis_list[6].setScale(3600)

        # Disable automatic SI prefix and always show unit = arcsec
        self.imv_beam_pattern_axis_list[0].enableAutoSIPrefix(False)
        self.imv_beam_pattern_axis_list[2].enableAutoSIPrefix(False)
        self.imv_beam_pattern_axis_list[4].enableAutoSIPrefix(False)
        self.imv_beam_pattern_axis_list[6].enableAutoSIPrefix(False)

        for axisitem in self.imv_beam_pattern_axis_list:
            axisitem.showLabel(True)

        self.imv_beam_pattern_magnitude.getView().setAxisItems(
            axisItems={
                "bottom": self.imv_beam_pattern_axis_list[0],
                "top": self.imv_beam_pattern_axis_list[1],
                "left": self.imv_beam_pattern_axis_list[2],
                "right": self.imv_beam_pattern_axis_list[3],
            }
        )

        self.imv_beam_pattern_phase.getView().setAxisItems(
            axisItems={
                "bottom": self.imv_beam_pattern_axis_list[4],
                "top": self.imv_beam_pattern_axis_list[5],
                "left": self.imv_beam_pattern_axis_list[6],
                "right": self.imv_beam_pattern_axis_list[7],
            }
        )

    def create_widgets_magnitude_pattern(self):
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)

        self.ax_magnitude_tst = self.fig.add_subplot(221) # top-left (magnitude test HOLOSS - Scatter)
        self.ax_magnitude_tst_heatmap = self.fig.add_subplot(223) # bottom-left (magnitude test HOLOSS - HeatMap)

        self.ax_magnitude_ref = self.fig.add_subplot(222) # top-right (magnitude ref HOLORR - Scatter)
        self.ax_magnitude_ref_heatmap = self.fig.add_subplot(224) # bottom-right (magnitude ref HOLORR - HeatMap)

        self.plot_magnitude_pattern(self.ax_magnitude_tst, self.ax_magnitude_tst_heatmap, self.ax_magnitude_ref, self.ax_magnitude_ref_heatmap)

        layout_magnitude_pattern = QtWidgets.QVBoxLayout()
        layout_magnitude_pattern.addWidget(self.canvas)
        
        self.tab_magnitude_pattern.setLayout(layout_magnitude_pattern)

    def create_widgets_holo_corr_pattern(self):
        # self.fig = Figure()
        self.fig = plt.figure(figsize=(14, 6))
        self.canvas = FigureCanvas(self.fig)

        gs = GridSpec(2, 7, figure=self.fig, height_ratios=[0.5, 1], hspace=0.5, wspace=0.5)

        # Top
        self.ax_holo_ss = self.fig.add_subplot(gs[0, 0])      # HOLOSS
        self.ax_holo_rr = self.fig.add_subplot(gs[0, 1])      # HOLORR
        self.ax_holo_ampref = self.fig.add_subplot(gs[0, 2])  # AMPREF
        self.ax_holo_amptst = self.fig.add_subplot(gs[0, 3])  # AMPTST
        self.ax_holo_amprel = self.fig.add_subplot(gs[0, 4])  # AMPREL
        self.ax_holo_pharel = self.fig.add_subplot(gs[0, 5])  # PLAREL

        # Bottom
        self.ax_holo_ss_heatmap = self.fig.add_subplot(gs[1, 0])  # HOLOSS - Heatmap
        self.ax_holo_rr_heatmap = self.fig.add_subplot(gs[1, 1])  # HOLORR - Heatmap
        self.ax_holo_ampref_heatmap = self.fig.add_subplot(gs[1, 2]) # AMPREF - Heatmap
        self.ax_holo_amptst_heatmap = self.fig.add_subplot(gs[1, 3]) # AMPTST - Heatmap
        self.ax_holo_amprel_heatmap = self.fig.add_subplot(gs[1, 4]) # AMPREL - Heatmap
        self.ax_holo_pharel_heatmap = self.fig.add_subplot(gs[1, 5]) # PHAREL - Heatmap
        
        self.plot_holo_corr_pattern(self.ax_holo_ss, 
                                    self.ax_holo_rr, 
                                    self.ax_holo_ampref, 
                                    self.ax_holo_amptst, 
                                    self.ax_holo_amprel, 
                                    self.ax_holo_pharel, 
                                    self.ax_holo_ss_heatmap,
                                    self.ax_holo_rr_heatmap,
                                    self.ax_holo_ampref_heatmap,
                                    self.ax_holo_amptst_heatmap,
                                    self.ax_holo_amprel_heatmap,
                                    self.ax_holo_pharel_heatmap)
        layout_holo_corr_pattern = QtWidgets.QVBoxLayout()
        layout_holo_corr_pattern.addWidget(self.canvas)

        self.tab_holo_corr_pattern.setLayout(layout_holo_corr_pattern)

        self.canvas.draw()

    def plot_magnitude_pattern(self, ax_magnitude_tst, ax_magnitude_tst_heatmap, ax_magnitude_ref, ax_magnitude_ref_heatmap):
        ax_magnitude_tst.cla()
        ax_magnitude_tst_heatmap.cla()

        longoff_holo_arcsec = self.holo_data['LONGOFF'] * 3600
        latoff_holo_arcsec = self.holo_data['LATOFF'] * 3600
        longoff_corr_arcsec = self.corr_data['LONGOFF'] * 3600
        latoff_corr_arcsec = self.corr_data['LATOFF'] * 3600

        longoff_min = min(longoff_holo_arcsec)
        longoff_max = max(longoff_holo_arcsec)
        latoff_min = min(latoff_holo_arcsec)
        latoff_max = max(latoff_holo_arcsec)

        # Magnitude Test data 'HOLOSS'
        holodata_ss = 10 * np.log10(self.holo_data['HOLOSS'] * 8) + 30
        corrdata_ss = 10 * np.log10(self.corr_data['HOLOSS'] * 8) + 30

        # Magnitude Ref data 'HOLORR
        holodata_rr = 10 * np.log10(self.holo_data['HOLORR'] * 8) + 30
        corrdata_rr = 10 * np.log10(self.holo_data['HOLORR'] * 8) + 30

        # Plot Magnitude Test Scatter
        sc1 = ax_magnitude_tst.scatter(longoff_holo_arcsec, latoff_holo_arcsec,
                                       c=holodata_ss, cmap='plasma')
        ax_magnitude_tst.set_title(f"[Scatter - HOLOSS] Magnitude Test")
        ax_magnitude_tst.set_xlabel('LONGOFF (arcsec)')
        ax_magnitude_tst.set_ylabel('LATOFF (arcsec)')
        ax_magnitude_tst.set_xlim([longoff_min, longoff_max])
        ax_magnitude_tst.set_ylim([latoff_min, latoff_max])
        ax_magnitude_tst.set_aspect('equal')

        self.magnitude_tst_colorbar = self.fig.colorbar(sc1,
                                                    ax=ax_magnitude_tst,
                                                    label=f'HOLOSS [dBm]')
        
        self.fig.tight_layout()
        
        # Plot Magnitude Test Heatmap
        grid_lat_tst, grid_lon_tst = np.meshgrid(
            np.linspace(latoff_holo_arcsec.min(), latoff_holo_arcsec.max(), 100),
            np.linspace(longoff_holo_arcsec.min(), longoff_holo_arcsec.max(), 100)
        )

        grid_data_tst = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec), 
            holodata_ss, 
            (grid_lat_tst, grid_lon_tst), 
            method='cubic'
        )

        filled_data_tst = np.where(np.isnan(grid_data_tst), np.nanmean(holodata_ss), grid_data_tst)
        smoothed_data_tst = gaussian_filter(filled_data_tst, sigma=3)
        smoothed_transposed_tst = np.transpose(smoothed_data_tst)
        vmin, vmax = np.nanmin(smoothed_data_tst), np.nanmax(smoothed_data_tst)

        im_mag_tst = ax_magnitude_tst_heatmap.imshow(
            smoothed_transposed_tst, 
            cmap='plasma', 
            extent=(
                longoff_holo_arcsec.min(), 
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(), 
                latoff_holo_arcsec.max()
            ),
            origin='lower', 
            interpolation='nearest', 
            vmin=vmin, 
            vmax=vmax
        )
        ax_magnitude_tst_heatmap.set_title(f"[HeatMap - HOLOSS] Magnitude Test")
        ax_magnitude_tst_heatmap.set_xlabel("LONGOFF (arcsec)")
        ax_magnitude_tst_heatmap.set_ylabel("LATOFF (arcsec)")

        self.magnitude_tst_heatmap_colorbar = self.fig.colorbar(im_mag_tst, ax=ax_magnitude_tst_heatmap, label=f'HOLOSS [dBm]')

        # Plot Magnitude Ref Scatter
        sc2 = ax_magnitude_ref.scatter(longoff_holo_arcsec, latoff_holo_arcsec,
                                       c=holodata_rr, cmap='plasma')
        ax_magnitude_ref.set_title(f"[Scatter - HOLORR] Magnitude Ref")
        ax_magnitude_ref.set_xlabel('LONGOFF (arcsec)')
        ax_magnitude_ref.set_ylabel('LATOFF (arcsec)')
        ax_magnitude_ref.set_xlim([longoff_min, longoff_max])
        ax_magnitude_ref.set_ylim([latoff_min, latoff_max])
        ax_magnitude_ref.set_aspect('equal')

        self.magnitude_ref_colorbar = self.fig.colorbar(sc2,
                                                        ax=ax_magnitude_ref,
                                                        label=f'HOLORR [dBm]')
        
        self.fig.tight_layout()

        # Plot Magnitude Ref Heatmap
        grid_lat_ref, grid_lon_ref = np.meshgrid(
            np.linspace(latoff_holo_arcsec.min(), latoff_holo_arcsec.max(), 100),
            np.linspace(longoff_holo_arcsec.min(), longoff_holo_arcsec.max(), 100)
        )

        grid_data_ref = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec), 
            holodata_rr, 
            (grid_lat_ref, grid_lon_ref), 
            method='cubic'
        )

        filled_data_ref = np.where(np.isnan(grid_data_ref), np.nanmean(holodata_rr), grid_data_ref)
        smoothed_data_ref = gaussian_filter(filled_data_ref, sigma=3)
        smoothed_transposed_ref = np.transpose(smoothed_data_ref)
        vmin, vmax = np.nanmin(smoothed_data_ref), np.nanmax(smoothed_data_ref)

        im_mag_ref = ax_magnitude_ref_heatmap.imshow(
            smoothed_transposed_ref, 
            cmap='plasma', 
            extent=(
                longoff_holo_arcsec.min(), 
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(), 
                latoff_holo_arcsec.max()
            ),
            origin='lower', 
            interpolation='nearest', 
            vmin=vmin, 
            vmax=vmax
        )
        ax_magnitude_ref_heatmap.set_title(f"[HeatMap - HOLORR] Magnitude Ref")
        ax_magnitude_ref_heatmap.set_xlabel("LONGOFF (arcsec)")
        ax_magnitude_ref_heatmap.set_ylabel("LATOFF (arcsec)")

        self.magnitude_ref_heatmap_colorbar = self.fig.colorbar(im_mag_ref, ax=ax_magnitude_ref_heatmap, label=f'HOLORR [dBm]')

    def plot_holo_corr_pattern(self, 
                               ax_holo_ss, ax_holo_rr, 
                               ax_holo_ampref, 
                               ax_holo_amptst, 
                               ax_holo_amprel, 
                               ax_holo_pharel, 
                               ax_holo_ss_heatmap, 
                               ax_holo_rr_heatmap,
                               ax_holo_ampref_heatmap,
                               ax_holo_amptst_heatmap,
                               ax_holo_amprel_heatmap,
                               ax_holo_pharel_heatmap):
        longoff_holo_arcsec = self.holo_data['LONGOFF'] * 3600
        latoff_holo_arcsec = self.holo_data['LATOFF'] * 3600
        longoff_corr_arcsec = self.corr_data['LONGOFF'] * 3600
        latoff_corr_arcsec = self.corr_data['LATOFF'] * 3600

        longoff_min = min(longoff_holo_arcsec)
        longoff_max = max(longoff_holo_arcsec)
        latoff_min = min(latoff_holo_arcsec)
        latoff_max = max(latoff_holo_arcsec)

        # HOLO Data
        holodata_ss = self.holo_data['HOLOSS']
        holodata_rr = self.holo_data['HOLORR']
        holodata_ampref = self.holo_data['AMPREF']
        holodata_amptst = self.holo_data['AMPTST']
        holodata_amprel = self.holo_data['AMPREL']
        holodata_pharel = self.holo_data['PHAREL']

        # CORR Data
        corrdata_ss = self.corr_data['HOLOSS']

        # Plot HOLOSS - Scatter
        sc1 = ax_holo_ss.scatter(longoff_holo_arcsec, latoff_holo_arcsec, c=holodata_ss, cmap='plasma')
        ax_holo_ss.set_title('HOLOSS')
        ax_holo_ss.set_xlabel('LONGOFF (arcsec)')
        ax_holo_ss.set_ylabel('LATOFF (arcsec)')
        ax_holo_ss.set_xlim([longoff_min, longoff_max])
        ax_holo_ss.set_ylim([latoff_min, latoff_max])
        ax_holo_ss.set_aspect('equal')
        divider1 = make_axes_locatable(ax_holo_ss)
        cax1 = divider1.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(sc1, cax=cax1)

        # Plot HOLORR - Scatter
        sc2 = ax_holo_rr.scatter(longoff_holo_arcsec, latoff_holo_arcsec, c=holodata_rr, cmap='plasma')
        ax_holo_rr.set_title('HOLORR')
        ax_holo_rr.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_rr.set_ylabel('LATOFF (arcsec)')
        ax_holo_rr.set_xlim([longoff_min, longoff_max])
        ax_holo_rr.set_ylim([latoff_min, latoff_max])
        ax_holo_rr.set_aspect('equal')
        divider2 = make_axes_locatable(ax_holo_rr)
        cax2 = divider2.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(sc2, cax=cax2)

        # Plot AMPREF - Scatter
        sc3 = ax_holo_ampref.scatter(longoff_holo_arcsec, latoff_holo_arcsec, c=holodata_ampref, cmap='plasma')
        ax_holo_ampref.set_title('AMPREF')
        ax_holo_ampref.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_ampref.set_ylabel('LATOFF (arcsec)')
        ax_holo_ampref.set_xlim([longoff_min, longoff_max])
        ax_holo_ampref.set_ylim([latoff_min, latoff_max])
        ax_holo_ampref.set_aspect('equal')
        divider3 = make_axes_locatable(ax_holo_ampref)
        cax3 = divider3.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(sc3, cax=cax3)

        # Plot AMPTST - Scatter
        sc4 = ax_holo_amptst.scatter(longoff_holo_arcsec, latoff_holo_arcsec, c=holodata_amptst, cmap="plasma")
        ax_holo_amptst.set_title('AMPTST')
        ax_holo_amptst.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_amptst.set_ylabel('LATOFF (arcsec)')
        ax_holo_amptst.set_xlim([longoff_min, longoff_max])
        ax_holo_amptst.set_ylim([latoff_min, latoff_max])
        ax_holo_amptst.set_aspect('equal')
        divider4 = make_axes_locatable(ax_holo_amptst)
        cax4 = divider4.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(sc4, cax=cax4)

        # Plot AMPREL - Scatter
        sc5 = ax_holo_amprel.scatter(longoff_holo_arcsec, latoff_holo_arcsec, c=holodata_amprel, cmap="plasma")
        ax_holo_amprel.set_title('AMPREL')
        ax_holo_amprel.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_amprel.set_ylabel('LATOFF (arcsec)')
        ax_holo_amprel.set_xlim([longoff_min, longoff_max])
        ax_holo_amprel.set_ylim([latoff_min, latoff_max])
        ax_holo_amprel.set_aspect('equal')
        divider5 = make_axes_locatable(ax_holo_amprel)
        cax5 = divider5.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(sc5, cax=cax5)

        # Plot PHAREL - Scatter
        sc6 = ax_holo_pharel.scatter(longoff_holo_arcsec, latoff_holo_arcsec, c=holodata_pharel, cmap="plasma")
        ax_holo_pharel.set_title('PHAREL')
        ax_holo_pharel.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_pharel.set_ylabel('LATOFF (arcsec)')
        ax_holo_pharel.set_xlim([longoff_min, longoff_max])
        ax_holo_pharel.set_ylim([latoff_min, latoff_max])
        ax_holo_pharel.set_aspect('equal')
        divider6 = make_axes_locatable(ax_holo_pharel)
        cax6 = divider6.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(sc6, cax=cax6)

        # Plot HeatMap
        # Plot HOLOSS - Heatmap
        grid_lat_holo, grid_lon_holo = np.meshgrid(
            np.linspace(latoff_holo_arcsec.min(), latoff_holo_arcsec.max(), 100),
            np.linspace(longoff_holo_arcsec.min(), longoff_holo_arcsec.max(), 100)
        )
        grid_data_holo = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec), 
            holodata_ss,
            (grid_lat_holo, grid_lon_holo), 
            method='cubic'
        )
        filled_data_holo = np.where(np.isnan(grid_data_holo), np.nanmean(holodata_ss), grid_data_holo)
        smoothed_data_holo = gaussian_filter(filled_data_holo, sigma=3)
        smoothed_transposed_holo = np.transpose(smoothed_data_holo)
        vmin_holo_ss, vmax_holo_ss = np.nanmin(smoothed_data_holo), np.nanmax(smoothed_data_holo)       
        im_holo_heatmap = ax_holo_ss_heatmap.imshow(
            smoothed_transposed_holo, 
            cmap='plasma', 
            extent=(
                longoff_holo_arcsec.min(), 
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(), 
                latoff_holo_arcsec.max()
            ),
            origin='lower', 
            interpolation='nearest', 
            vmin=vmin_holo_ss, 
            vmax=vmax_holo_ss
        )
        ax_holo_ss_heatmap.set_title('HOLOSS') # and Gaussian Smoothing
        ax_holo_ss_heatmap.set_xlabel('LONGOFF (arcsec)')
        ax_holo_ss_heatmap.set_ylabel('LATOFF (arcsec)')
        divider1_heatmap = make_axes_locatable(ax_holo_ss_heatmap)
        cax1_heatmap = divider1_heatmap.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(im_holo_heatmap, cax=cax1_heatmap)

        # Plot HOLORR - Heatmap
        grid_data_holo_rr = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec),
            holodata_rr,
            (grid_lat_holo, grid_lon_holo),
            method='cubic'
        )
        filled_data_holo_rr = np.where(np.isnan(grid_data_holo_rr), np.nanmean(holodata_rr), grid_data_holo_rr)
        smoothed_data_holo_rr = gaussian_filter(filled_data_holo_rr, sigma=3)
        smoothed_transposed_holo_rr = np.transpose(smoothed_data_holo_rr)
        vmin_holo_rr, vmax_holo_rr = np.nanmin(smoothed_data_holo_rr), np.nanmax(smoothed_data_holo_rr)
        im_holo_rr_heatmap = ax_holo_rr_heatmap.imshow(
            smoothed_transposed_holo_rr,
            cmap='plasma',
            extent=(
                longoff_holo_arcsec.min(),
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(),
                latoff_holo_arcsec.max()
            ),
            origin='lower',
            interpolation='nearest',
            vmin=vmin_holo_rr,
            vmax=vmax_holo_rr
        )
        ax_holo_rr_heatmap.set_title('HOLORR')
        ax_holo_rr_heatmap.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_rr_heatmap.set_ylabel('LATOFF (arcsec)')
        divider2_heatmap = make_axes_locatable(ax_holo_rr_heatmap)
        cax2_heatmap = divider2_heatmap.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(im_holo_rr_heatmap, cax=cax2_heatmap)

        # Plot AMPREF - Heatmap
        grid_data_ampref = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec),
            holodata_ampref,
            (grid_lat_holo, grid_lon_holo),
            method='cubic'
        )
        filled_data_holo_ampref = np.where(np.isnan(grid_data_ampref), np.nanmean(holodata_ampref), grid_data_ampref)
        smoothed_data_holo_ampref = gaussian_filter(filled_data_holo_ampref, sigma=3)
        smoothed_transposed_holo_ampref = np.transpose(smoothed_data_holo_ampref)
        vmin_ampref, vmax_ampref = np.nanmin(smoothed_data_holo_ampref), np.nanmax(smoothed_data_holo_ampref)
        im_holo_ampref_heatmap = ax_holo_ampref_heatmap.imshow(
            smoothed_transposed_holo_ampref,
            cmap='plasma',
            extent=(
                longoff_holo_arcsec.min(),
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(),
                latoff_holo_arcsec.max()
            ),
            origin='lower',
            interpolation='nearest',
            vmin=vmin_ampref,
            vmax=vmax_ampref
        )
        ax_holo_ampref_heatmap.set_title('AMPREF')
        ax_holo_ampref_heatmap.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_ampref_heatmap.set_ylabel('LATOFF (arcsec)')
        divider3_heatmap = make_axes_locatable(ax_holo_ampref_heatmap)
        cax3_heatmap = divider3_heatmap.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(im_holo_ampref_heatmap, cax=cax3_heatmap)

        # Plot AMPTST - Heatmap
        grid_data_amptst = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec),
            holodata_amptst,
            (grid_lat_holo, grid_lon_holo),
            method='cubic'
        )
        filled_data_holo_amptst = np.where(np.isnan(grid_data_amptst), np.nanmean(holodata_amptst), grid_data_amptst)
        smoothed_data_holo_amptst = gaussian_filter(filled_data_holo_amptst, sigma=3)
        smoothed_transposed_holo_amptst = np.transpose(smoothed_data_holo_amptst)
        vmin_amptst, vmax_amptst = np.nanmin(smoothed_data_holo_amptst), np.nanmax(smoothed_data_holo_amptst)
        im_holo_amptst_heatmap = ax_holo_amptst_heatmap.imshow(
            smoothed_transposed_holo_amptst,
            cmap='plasma',
            extent=(
                longoff_holo_arcsec.min(),
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(),
                latoff_holo_arcsec.max()
            ),
            origin='lower',
            interpolation='nearest',
            vmin=vmin_amptst,
            vmax=vmax_amptst
        )
        ax_holo_amptst_heatmap.set_title('AMPTST')
        ax_holo_amptst_heatmap.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_amptst_heatmap.set_ylabel('LATOFF (arcsec)')
        divider4_heatmap = make_axes_locatable(ax_holo_amptst_heatmap)
        cax4_heatmap = divider4_heatmap.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(im_holo_amptst_heatmap, cax=cax4_heatmap)

        # Plot AMPREL - Heatmap
        grid_data_amprel = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec),
            holodata_amprel,
            (grid_lat_holo, grid_lon_holo),
            method='cubic'
        )
        filled_data_holo_amprel = np.where(np.isnan(grid_data_amprel), np.nanmean(holodata_amprel), grid_data_amprel)
        smoothed_data_holo_amprel = gaussian_filter(filled_data_holo_amprel, sigma=3)
        smoothed_transposed_holo_amprel = np.transpose(smoothed_data_holo_amprel)
        vmin_amprel, vmax_amprel = np.nanmin(smoothed_data_holo_amprel), np.nanmax(smoothed_data_holo_amprel)
        im_holo_amprel_heatmap = ax_holo_amprel_heatmap.imshow(
            smoothed_transposed_holo_amprel,
            cmap='plasma',
            extent=(
                longoff_holo_arcsec.min(),
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(),
                latoff_holo_arcsec.max()
            ),
            origin='lower',
            interpolation='nearest',
            vmin=vmin_amprel,
            vmax=vmax_amprel
        )
        ax_holo_amprel_heatmap.set_title('AMPREL')
        ax_holo_amprel_heatmap.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_amprel_heatmap.set_ylabel('LATOFF (arcsec)')
        divider5_heatmap = make_axes_locatable(ax_holo_amprel_heatmap)
        cax5_heatmap = divider5_heatmap.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(im_holo_amprel_heatmap, cax=cax5_heatmap)

        # Plot PHAREL - Heatmap
        grid_data_pharel = griddata(
            (latoff_holo_arcsec, longoff_holo_arcsec),
            holodata_pharel,
            (grid_lat_holo, grid_lon_holo),
            method='cubic'
        )
        filled_data_holo_pharel = np.where(np.isnan(grid_data_pharel), np.nanmean(holodata_pharel), grid_data_pharel)
        smoothed_data_holo_pharel = gaussian_filter(filled_data_holo_pharel, sigma=3)
        smoothed_transposed_holo_pharel = np.transpose(smoothed_data_holo_pharel)
        vmin_pharel, vmax_pharel = np.nanmin(smoothed_data_holo_pharel), np.nanmax(smoothed_data_holo_pharel)
        im_holo_pharel_heatmap = ax_holo_pharel_heatmap.imshow(
            smoothed_transposed_holo_pharel,
            cmap='plasma',
            extent=(
                longoff_holo_arcsec.min(),
                longoff_holo_arcsec.max(),
                latoff_holo_arcsec.min(),
                latoff_holo_arcsec.max()
            ),
            origin='lower',
            interpolation='nearest',
            vmin=vmin_pharel,
            vmax=vmax_pharel
        )
        ax_holo_pharel_heatmap.set_title('PHAREL')
        ax_holo_pharel_heatmap.set_xlabel('LONGOFF (arcsec)')
        # ax_holo_pharel_heatmap.set_ylabel('LATOFF (arcsec)')
        divider6_heatmap = make_axes_locatable(ax_holo_pharel_heatmap)
        cax6_heatmap = divider6_heatmap.append_axes("right", size="5%", pad=0.1)
        self.fig.colorbar(im_holo_pharel_heatmap, cax=cax6_heatmap)


        # Plot CORR Data - Scatter
        # sc2 = ax_corr.scatter(longoff_corr_arcsec, latoff_corr_arcsec,
        #                     c=corrdata_ss, cmap='plasma')
        # ax_corr.set_title(f"[CORR][Scatter - ''] ScanMap")
        # ax_corr.set_xlabel('LONGOFF (arcsec)')
        # ax_corr.set_ylabel('LATOFF (arcsec)')
        # ax_corr.set_xlim([longoff_min, longoff_max])
        # ax_corr.set_ylim([latoff_min, latoff_max])
        # ax_corr.set_aspect('equal') 

        # self.corr_scatter_colorbar = self.fig.colorbar(sc2, ax=ax_corr, label=f'HOLOSS [Watt]') 

        # self.fig.tight_layout()

    def update_selected_tab(self, index):
        if index is self.tabgroup_graphs.indexOf(self.tab_beam_pattern):
            self.logger.debug("Beam Pattern")
            self.update_graph_beam_pattern()
        if index is self.tabgroup_graphs.indexOf(self.tab_todo):
            self.logger.debug("TO DO")
            self.update_graph_todo()

    def update_graph_beam_pattern(self):
        self.update_widgets_beam_pattern()
        self.update_data_beam_pattern()

    def update_widgets_beam_pattern(self):
        # Update widget labels or axes if they need to change from user selection
        self.label_beam_pattern_magnitude.setText("Antenna Beam Magnitude [dB no cal]")
        self.label_beam_pattern_phase.setText("Antenna Beam Phase [radian]")

    def update_data_beam_pattern(self):
        # Set X and Y range of PlotItem that is the background on which ImageView draws the Image
        x_min = self.angles_linspace[0]
        x_max = self.angles_linspace[-1]
        x_angular_resolution = self.angular_resolution

        # NOTE: X and Y angular resolution and min, max range are the same because the data was
        # already interpolated onto a square grid in the function load_data().
        # If the observation data was recorded using some non-uniform spacing, the effect might
        # confusing here.  (Or if the Az offset * cos(EL) is not accounted for correctly)
        y_min = x_min
        y_max = x_max
        y_angular_resolution = x_angular_resolution

        #        self.imv_beam_pattern_magnitude.getView().setXRange(x_min, x_max)
        #        self.imv_beam_pattern_magnitude.getView().setYRange(y_min, y_max)
        self.imv_beam_pattern_magnitude.getView().showGrid(True, True)
        self.imv_beam_pattern_magnitude.setImage(
            self.magnitude_otf_map_normalized,
            pos=[x_min, y_min],
            scale=[x_angular_resolution, y_angular_resolution],
        )

        #       self.imv_beam_pattern_phase.getView().setXRange(x_min, x_max)
        #       self.imv_beam_pattern_phase.getView().setYRange(y_min, y_max)
        self.imv_beam_pattern_phase.getView().showGrid(True, True)
        self.imv_beam_pattern_phase.setImage(
            self.phase_rad_otf_map,
            pos=[x_min, y_min],
            scale=[x_angular_resolution, y_angular_resolution],
        )

    def update_graph_todo(self):
        pass

    def layout_widgets(self):
        self.central_widget = QWidget()
        self.hbox_main = QHBoxLayout(self.central_widget)
        self.setCentralWidget(self.central_widget)

        # Select Data (top level. not in a Tab)
        self.layout_select = QVBoxLayout()
        self.layout_select.setAlignment(QtCore.Qt.AlignTop)
        self.layout_select.addWidget(QHLine())

        # Add the selection widget (with radio buttons) to the main layout
        self.hbox_main.addWidget(self.select_widget)

        # Antenna Beam Pattern Tab
        self.layout_beam_pattern = QGridLayout()
        self.layout_beam_pattern.addWidget(self.label_beam_pattern_magnitude, 0, 0)
        self.layout_beam_pattern.addWidget(self.imv_beam_pattern_magnitude, 1, 0)
        self.layout_beam_pattern.addWidget(self.label_beam_pattern_phase, 0, 1)
        self.layout_beam_pattern.addWidget(self.imv_beam_pattern_phase, 1, 1)
        self.tab_beam_pattern.setLayout(self.layout_beam_pattern)

        # Top-level layout
        self.hbox_main.addLayout(self.layout_select)
        self.hbox_main.addWidget(self.tabgroup_graphs)


if __name__ == "__main__":
    # Start logger
    logger = logging.getLogger(__name__)
    configure_logger(logger)
    logger.debug("Started log for {}".format(logger.name))

    parser = argparse.ArgumentParser(
        description="Reads FITS file written by PipelineMBfits and diplays some graphs",
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument("filename", help="FITS file with format specified by AntennaDataFormat.py")

    # If no command line arguments are avaiable, show help message and exit
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        exit()

    args = parser.parse_args()

    if os.path.isfile(args.filename) is not True:
        logger.error("File not found")
        logger.error("os.path.isfile({}) = False".format(args.filename))
        sys.exit(1)
    else:
        logger.info("Processing file {}: ".format(args.filename))

        # Create instance of Qt GUI Application
        app = QApplication([])

        # Set theme style
        app.setStyle("Fusion")

        # Set colors
        app.setPalette(dark_theme)

        # Create instance of MainWindow.  The class MainWindow inherits QtGui objects
        # from Qt.QMainWindow and from DataMonitorGUI.  It extends the parent classes by
        # adding non-GUI member variables and functions to update GUI.
        window = MainWindow(args.filename)

        # Enable the window to show after Qt event loop starts
        window.show()

        # Start Qt Event loop (including open GUI window)
        app.exec_()

        # After GUI window is closed, clean up.
        window.shutdown()

        # End Python process
        sys.exit(0)