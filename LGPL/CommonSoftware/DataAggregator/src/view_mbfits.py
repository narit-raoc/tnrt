# Spiro Sarris

# Standard python modules
import logging
import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import time

# Modules installed from pip
import coloredlogs
import numpy as np
from astropy.io import fits
from astropy.time import Time

# Qt GUI
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore

# NOTE: in the future, we prefer to import these modules using syntax
# from pyqtgraph.Qt.QtWidgets import <xxxx> to abstract out the actual
# version of PyQt or PySide.  Let pyqtgraph select.  This makes easier
# upgrading verions of Qt without editing code.  But for pyqtgraph 0.11.1
# and Python 3.6, we have to reference PyQt5 explicitly.
from PyQt5.QtGui import QPalette, QColor, QFont, QPainterPath, QTransform
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5.QtWidgets import QTabWidget, QFrame, QHBoxLayout, QVBoxLayout, QGridLayout, QGroupBox
from PyQt5.QtWidgets import QCheckBox, QComboBox, QLabel, QMessageBox, QPushButton, QSpinBox

dark_theme = QPalette()

# Color group alias (to keep lines of code short)
dis = QPalette.Disabled
act = QPalette.Active
inac = QPalette.Inactive

txt_flags = QtCore.Qt.TextSelectableByMouse

bg_button = 30
bg_gray = 60
# Central roles.  If ColorGroup is not not specified, default is QPalette.Active
dark_theme.setColor(act, QPalette.Window, QColor(0, 0, 0))
dark_theme.setColor(act, QPalette.WindowText, QColor(255, 255, 255))
dark_theme.setColor(act, QPalette.Base, QColor(bg_gray, bg_gray, bg_gray))
dark_theme.setColor(act, QPalette.AlternateBase, QColor(66, 66, 66))
dark_theme.setColor(act, QPalette.ToolTipBase, QColor(53, 53, 53))
dark_theme.setColor(act, QPalette.ToolTipText, QColor(180, 180, 180))
# dark_theme.setColor(QPalette.PlaceHolderText, QColor(180, 180, 180))
dark_theme.setColor(act, QPalette.Text, QColor(255, 255, 255))
dark_theme.setColor(act, QPalette.Button, QColor(bg_button, bg_button, bg_button))
dark_theme.setColor(act, QPalette.ButtonText, QColor(255, 255, 255))
dark_theme.setColor(act, QPalette.BrightText, QColor(255, 255, 255))

# Colors for 3D rendered edges of buttons, scrollbars, ...
dark_theme.setColor(act, QPalette.Light, QColor(180, 180, 180))
dark_theme.setColor(act, QPalette.Midlight, QColor(90, 90, 90))
dark_theme.setColor(act, QPalette.Dark, QColor(35, 35, 35))
dark_theme.setColor(act, QPalette.Mid, QColor(35, 35, 35))
dark_theme.setColor(act, QPalette.Shadow, QColor(50, 50, 50))

# Selected items
dark_theme.setColor(act, QPalette.Highlight, QColor(42, 130, 218))
dark_theme.setColor(act, QPalette.HighlightedText, QColor(180, 180, 180))

# Hyperlink  / URL
dark_theme.setColor(act, QPalette.Link, QColor(56, 252, 196))
dark_theme.setColor(act, QPalette.LinkVisited, QColor(56, 252, 196))

# For widgets that don't have a color role assigned
dark_theme.setColor(act, QPalette.NoRole, QColor(66, 66, 66))

# Colors for disabled items in the current focus window (QPalette.Disabled)
dark_theme.setColor(dis, QPalette.WindowText, QColor(127, 127, 127))
dark_theme.setColor(dis, QPalette.Text, QColor(127, 127, 127))
dark_theme.setColor(dis, QPalette.ButtonText, QColor(127, 127, 127))
dark_theme.setColor(dis, QPalette.Highlight, QColor(80, 80, 80))
dark_theme.setColor(dis, QPalette.HighlightedText, QColor(127, 127, 127))


class QHLine(QFrame):
    def __init__(self):
        super().__init__()
        self.setFrameShape(QFrame.HLine)

def configure_logger(logger):
    logger.setLevel(logging.DEBUG)
    fmt_scrn = "%(asctime)s [%(levelname)s] %(funcName)s(): %(message)s"
    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red", "bright": True},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta", "bright": True, "bold": True},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }

    formatter_screen = logging.Formatter(fmt=fmt_scrn)
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)
    handler_screen.setLevel(logging.DEBUG)
    logger.addHandler(handler_screen)


class MainWindow(QMainWindow):
    """ """

    def __init__(self, filename):
        """
        Create MainWindow QtGui
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        configure_logger(self.logger)
        self.logger.info("Started log for {}".format(self.logger.name))

        super(MainWindow, self).__init__()
        # Load data from file and index subscans
        self.load_data(filename)

        # Create widgets, add to layouts and initialize all values that
        # remain constant and do not depend on selected subscan.
        self.create_widgets(filename)

        # Add all of the new widgets to the GUI layout
        self.layout_widgets()

        # Get selected subscan and prepare data arrays.
        # Note update subscan also calls update_selected_tab
        self.update_data_subscan()

        # Update the selected frequency range data for Cross, Raster, ...
        self.update_data_freq_range()

    def shutdown(self):
        self.logger.debug("shutdown")
        self.hdul.close()

    def load_data(self, filename):
        self.mbfits_basename = os.path.splitext(os.path.basename(filename))[0]
        self.hdul = fits.open(filename)
        # leave the file open becuase fits.open will not store the entire file in RAM if it is
        # too big.  Close file at the end of this script.
        # self.hdul.info()

        # Following the mbfits specification, each subscan will produce two HDU (header data unit)
        # items in the hdu list that is creaded by astropy.io.fits.
        # The HDU names are 'ARRAYDATA-MBFITS' and 'DATAPAR-MBFITS'
        # A file that contains more than 1 subscan will have more than 1 HDU that uses the same name.
        # but the index in the hdulist (generated by astropy) and the subscan number (in the DATAPAR header)
        # is unique.
        # Therefore, we must traverse the HDU list and generate new data structures with references to
        # the data in ARRAYDATA and DATAPAR of each subscan, but more convenient to access data by subscan.
        # Note these lists *must* have the same length and have associated data in the same sequence
        self.arraydata_hdulist = []
        self.datapar_hdulist = []
        self.subscan_number_list = []

        # Get more header information:
        self.scan_header_string = "{} {}: {} = ({:3f} deg, {:3f} deg)".format(
            self.hdul["SCAN-MBFITS"].header["OBJECT"],
            self.hdul["SCAN-MBFITS"].header["SCANMODE"],
            self.hdul["SCAN-MBFITS"].header["RADESYS"],
            self.hdul["SCAN-MBFITS"].header["BLONGOBJ"] or 0.0,
            self.hdul["SCAN-MBFITS"].header["BLATOBJ"] or 0.0,
        )
        # TODO: use longitude and latitude types from header
        # self.hdul['SCAN-MBFITS'].header['BLNGTYPE'],
        # self.hdul['SCAN-MBFITS'].header['BLATTYPE'],

        self.logger.info(self.scan_header_string)

        for hdu in self.hdul:
            self.logger.info("found HDU name: {}".format(hdu.name))
            if hdu.name == "ARRAYDATA-MBFITS":
                self.logger.info("name 'ARRAYDATA-MBFITS'. Append to arraydata_list")
                if hdu.data["MJD"][0] == 0.0:
                    self.logger.warning(
                        "Found timestamp MJD == 0.  Looks like dummy row. discard and continue"
                    )
                    continue
                self.arraydata_hdulist.append(hdu)

                self.logger.info("found subscan number: {}".format(hdu.header["SUBSNUM"]))
                self.subscan_number_list.append(hdu.header["SUBSNUM"])

            elif hdu.name == "DATAPAR-MBFITS":
                self.logger.info("name is 'DATAPAR-MBFITS'. Append to datapar_list")
                if hdu.data["MJD"][0] == 0.0:
                    self.logger.warning(
                        "Found timestamp MJD == 0.  Looks like dummy row. discard and continue"
                    )
                    continue
                self.datapar_hdulist.append(hdu)
                self.logger.info("found subscan number: {}".format(hdu.header["SUBSNUM"]))
                if hdu.header["SUBSNUM"] != self.subscan_number_list[-1]:
                    raise IndexError(
                        "Subscan numbers out of sequence between ARRAYDATA and DATAPAR"
                    )

        self.logger.info(
            "Found {} subscans.  Subscan ID numbers: {}".format(
                len(self.subscan_number_list), self.subscan_number_list
            )
        )

        if len(self.subscan_number_list) == 0:
            self.logger.error("Subscans do not contain spectrum data. exit now")
            self.shutdown()

            # End Python process
            sys.exit(0)

        # ----------------
        # Frequency
        # -----------------
        self.febe_label = self.arraydata_hdulist[0].header['FEBE']
        self.freq_res = self.arraydata_hdulist[0].header['FREQRES'] 
        self.nchannels = self.arraydata_hdulist[0].header['CHANNELS']
        rf_freq_at_ref_channel = self.arraydata_hdulist[0].header['1CRVL2F']

        # check if open .mbfits that newer than 2023/04 (v1.0.1) that has good data in the header
        # otherwise default to the L-band frequency axis
        if self.freq_res and self.freq_res != 0:
            self.ff = np.arange(rf_freq_at_ref_channel, rf_freq_at_ref_channel + (self.freq_res * self.nchannels), self.freq_res)
        else:
            fs = 2e9
            DIM_FREQUENCY = 2
            self.nchannels = self.arraydata_hdulist[0].data['data'].shape[DIM_FREQUENCY]
            self.freq_res = (fs / 2) / self.nchannels
            self.ff = np.linspace(
                fs / 2,
                fs,
                self.nchannels,
                endpoint=False,
            )

        # ----------------
        # Geometry
        # -----------------
        # Prepare data for geometry tab
        # Traverse subscans.  assemble a data structure that has SUBSNUM, LONGOFF, LATOFF
        self.longoff_flat = []
        self.latoff_flat = []
        self.subsnum_flat = []
        self.arraydata_flat = []

        for subsnum, datapar, arraydata in zip(
            self.subscan_number_list, self.datapar_hdulist, self.arraydata_hdulist
        ):
            self.longoff_flat = self.longoff_flat + list(datapar.data["LONGOFF"])
            self.latoff_flat = self.latoff_flat + list(datapar.data["LATOFF"])
            self.subsnum_flat = self.subsnum_flat + list(
                np.full((len(datapar.data["LATOFF"]),), subsnum)
            )
            self.arraydata_flat = self.arraydata_flat + list(arraydata.data["DATA"])

        self.logger.debug(
            "length of longoff: {}, latoff: {}, subsnum: {}, arraydata: {}".format(
                len(self.longoff_flat),
                len(self.latoff_flat),
                len(self.subsnum_flat),
                len(self.arraydata_flat),
            )
        )

        # Create numpy array from the flattened list of ARRAYDATA spectrum.
        # NOTE: This array contains all polarizations and all spectrum channels.
        # Function update_graph_azel_image will select the polarization and frequency based on user
        # selection and prepare display image
        self.heatmap_magnitude = np.array(self.arraydata_flat)
        self.logger.debug("heatmap_magnitude.shape: {}".format(self.heatmap_magnitude.shape))
        
        # Get continuous dAz, dEL from ACU.  Follow the same logic in PipelineMBfits to calculate
        # the "delta" from tracking center and projection of cos(EL)
        # COPY+PASTE some constant from DataFormatAcu to keep this script standalone and not need to import DataFormatAcu
        AZ = 0
        EL = 1
        TR_SUN_TRACK_ACTIVE = 1 << 12
        TR_PROG_TRACK_ACTIVE = 1 << 13
        TR_TLE_TRACK_ACTIVE = 1 << 14
        TR_STAR_TRACK_ACTIVE = 1 << 15

        actual_position_az = self.hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, AZ]
        actual_position_el = self.hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL]
        
        # Find tracking center coordinate from selected tracking algorithm in self.hdul["MONITOR-ANT-TRACKING"].data["bit_status"]
        # The bit status exists for every ACU status message in the array.  
        # Assume it does not change during this subscan. So, we can use the first value at index [0] 
        if (self.hdul["MONITOR-ANT-TRACKING"].data["bit_status"][0] & TR_SUN_TRACK_ACTIVE) == TR_SUN_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_SUN_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = self.hdul["MONITOR-ANT-TRACKING"].data["sun_track_az"]
            selected_tracking_algorithm_position_el = self.hdul["MONITOR-ANT-TRACKING"].data["sun_track_el"]

        elif (self.hdul["MONITOR-ANT-TRACKING"].data["bit_status"][0] & TR_PROG_TRACK_ACTIVE) == TR_PROG_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_PROG_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = self.hdul["MONITOR-ANT-TRACKING"].data["prog_track_az"]
            selected_tracking_algorithm_position_el = self.hdul["MONITOR-ANT-TRACKING"].data["prog_track_el"]
        
        elif (self.hdul["MONITOR-ANT-TRACKING"].data["bit_status"][0] & TR_TLE_TRACK_ACTIVE) == TR_TLE_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_TLE_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = self.hdul["MONITOR-ANT-TRACKING"].data["tle_track_az"]
            selected_tracking_algorithm_position_el = self.hdul["MONITOR-ANT-TRACKING"].data["tle_track_el"]
        
        elif (self.hdul["MONITOR-ANT-TRACKING"].data["bit_status"][0] & TR_STAR_TRACK_ACTIVE) == TR_STAR_TRACK_ACTIVE:
            self.logger.debug("Found selected tracking algorithm: TR_STAR_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = self.hdul["MONITOR-ANT-TRACKING"].data["star_track_az"]
            selected_tracking_algorithm_position_el = self.hdul["MONITOR-ANT-TRACKING"].data["star_track_el"]
        
        else:
            self.logger.warning("Cannot identify selected tracking algorithm.  Set tracking algorithm position = (0,0)")
            selected_tracking_algorithm_position_az = 0
            selected_tracking_algorithm_position_el = 0

        self.acu_daz = actual_position_az - selected_tracking_algorithm_position_az
        self.acu_del = actual_position_el - selected_tracking_algorithm_position_el
         
        # projection of dAZ onto EL plane
        self.acu_dazcosel = self.acu_daz * np.cos(np.radians(actual_position_el))
        
        # ----------------
        # Grid image
        # -----------------
        # NOTE: (SS. 10/2022). This grid is not used yet, but might be useful in the future
        # Traverse all time integrations within all subscans to find the min and max angles.
        # This will define the pixel grid
        xmin = np.min(self.longoff_flat)
        xmax = np.max(self.longoff_flat)
        ymin = np.min(self.latoff_flat)
        ymax = np.max(self.latoff_flat)
        self.logger.debug(
            "Found xmin: {}, xmax: {}, ymin: {}, ymax: {}".format(xmin, xmax, ymin, ymax)
        )

        self.angular_resolution_arcsec = 10
        self.angular_resolution_deg = self.angular_resolution_arcsec / 3600.0

        # Use scipy griddata to fill a uniform grid from the data that we have
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.griddata.html
        self.grid_x, self.grid_y = np.mgrid[
            xmin : xmax : self.angular_resolution_deg, ymin : ymax : self.angular_resolution_deg
        ]

        # self.logger.debug("self.grid_x.shape: {}".format(self.grid_x.shape))
        # self.logger.debug("self.grid_y.shape: {}".format(self.grid_y.shape))

        # Create labels for the polarization and noise state sections.
        # If the data has 4 sections, we are using provision description
        # TNRT_dualpol_spectrometer.  If the data has 8 sections, assume we are using provision
        # TNRT_stokes_spectrometer
        # TODO: (SS. 2023/01) K-band uses LCP/RCP --*not* H/V, so fix this logig to show the 
        # correct label for K-band polarization
        self.polarization_labels = {
            4: [
                "H, Noise ON",
                "V, Noise ON",
                "H, Noise OFF",
                "V, Noise OFF",
            ],
            8: [
                "Stokes I, Noise ON",
                "Stokes Q, Noise ON",
                "Stokes U, Noise ON",
                "Stokes V, Noise ON",
                "Stokes I, Noise OFF",
                "Stokes Q, Noise OFF",
                "Stokes U, Noise OFF",
                "Stokes V, Noise OFF",
            ],
        }


    def create_widgets(self, filename):
        pg.setConfigOptions(antialias=True)
        self.resize(1920, 1080)
        self.setWindowTitle(filename)
        self.create_widgets_select()
        self.create_widgets_tabs()
        self.create_widgets_geometry()
        self.create_widgets_spectrogram()
        self.create_widgets_freq_slice()
        self.create_widgets_time_slice()
        self.create_widgets_cross()

    def layout_widgets(self):
        self.central_widget = QWidget()
        self.hbox_main = QHBoxLayout(self.central_widget)
        self.setCentralWidget(self.central_widget)

        # -----------------
        # Spectrogram Slice
        # -----------------
        self.layout_spectrogram_slice = QHBoxLayout()

        # Interactive ImageView is a PyQtGraph Widget, not a GraphicsItem.
        # Therefore we must use the "standard" QtGui layout.addWidget instead of using
        # pyqtgtraph GraphicsLayout.addItem.
        self.layout_vbox_left = QVBoxLayout()
        self.layout_vbox_left.addWidget(self.imv_spectrogram)
        self.layout_spectrogram_slice.addLayout(self.layout_vbox_left, 6)

        # Create horizontal box layouts to add axis options for X axis of each plot
        self.layout_hbox_freq_slice_options = QHBoxLayout()
        self.layout_hbox_freq_slice_options.addWidget(QLabel("Select X axis"), 2)
        self.layout_hbox_freq_slice_options.addWidget(self.combobox_xaxis_freq_slice, 8)

        self.layout_hbox_time_slice_options = QHBoxLayout()
        self.layout_hbox_time_slice_options.addWidget(QLabel("Select X axis"), 2)
        self.layout_hbox_time_slice_options.addWidget(self.combobox_xaxis_time_slice, 8)

        # Create a vertical box layout to stack these 2 graphs in 1 column.
        # Then add this VBoxLayout to be the second column in the HBox layouet
        # where spectrogram is the first colunn.
        self.layout_vbox_right = QVBoxLayout()
        self.layout_vbox_right.addWidget(self.pw_freq_slice, 5)
        self.layout_vbox_right.addLayout(self.layout_hbox_freq_slice_options, 1)
        self.layout_vbox_right.addWidget(self.pw_time_slice, 5)
        self.layout_vbox_right.addLayout(self.layout_hbox_time_slice_options, 1)
        self.layout_spectrogram_slice.addLayout(self.layout_vbox_right, 4)

        # Add the layout to the tab
        self.tab_spectrogram.setLayout(self.layout_spectrogram_slice)

        # ----------------
        # Select data
        # ----------------
        self.layout_select = QVBoxLayout()
        self.layout_select.setAlignment(QtCore.Qt.AlignTop)

        # Select subscan and polarization
        # -- use for all tabs of the GUI

        try:
            [frontend, backend, provision] = self.febe_label.split('-')
        except:
            frontend = '-'
            backend = '-'
            provision = '-'

        self.layout_select.addWidget(QLabel(".mbfits file Version: {}".format(self.hdul["PRIMARY"].header["CREATOR"])))
        self.layout_select.addWidget(QLabel("Frontend: {}".format(frontend)))
        self.layout_select.addWidget(QLabel('Backend: {}'.format(backend)))
        self.layout_select.addWidget(QLabel('Provision: {}'.format(provision)))
        self.layout_select.addWidget(QHLine())
        self.layout_select.addWidget(self.label_select_subscan)
        self.layout_select.addWidget(self.combobox_subscan)
        self.layout_select.addWidget(QHLine())
        self.layout_select.addWidget(self.label_select_polarization)
        self.layout_select.addWidget(self.combobox_polarization)
        self.layout_select.addWidget(QHLine())

        # Select Frequency
        # -- use for tabs CROSS and RASTER
        self.layout_select_freq = QVBoxLayout()
        self.layout_select_freq.setAlignment(QtCore.Qt.AlignTop)
        self.groupbox_select_freq = QGroupBox("Frequency")

        self.layout_select_freq.addWidget(self.label_fft_index_min)
        self.layout_select_freq.addWidget(self.spinbox_fft_index_min)
        self.layout_select_freq.addWidget(QHLine())
        self.layout_select_freq.addWidget(self.label_fft_index_max)
        self.layout_select_freq.addWidget(self.spinbox_fft_index_max)
        self.layout_select_freq.addWidget(QHLine())
        self.layout_select_freq.addWidget(self.label_freq_select_nchan_name)
        self.layout_select_freq.addWidget(self.label_freq_select_nchan_value)
        self.layout_select_freq.addWidget(QHLine())
        self.layout_select_freq.addWidget(self.label_freq_min_name)
        self.layout_select_freq.addWidget(self.label_freq_min_value)
        self.layout_select_freq.addWidget(QHLine())
        self.layout_select_freq.addWidget(self.label_freq_max_name)
        self.layout_select_freq.addWidget(self.label_freq_max_value)
        self.layout_select_freq.addWidget(QHLine())
        self.layout_select_freq.addWidget(self.label_freq_select_fc_name)
        self.layout_select_freq.addWidget(self.label_freq_select_fc_value)
        self.layout_select_freq.addWidget(QHLine())
        self.layout_select_freq.addWidget(self.label_freq_select_bw_name)
        self.layout_select_freq.addWidget(self.label_freq_select_bw_value)
        self.groupbox_select_freq.setLayout(self.layout_select_freq)
        self.layout_select.addWidget(self.groupbox_select_freq)

        # Select options for export CSV
        self.layout_select.addWidget(QHLine())
        self.layout_select_export = QVBoxLayout()
        self.layout_select_export.setAlignment(QtCore.Qt.AlignTop)
        self.groupbox_select_export = QGroupBox("Export")

        for cb in self.checkbox_pol_list:
            self.layout_select_export.addWidget(cb)    
        self.layout_select_export.addWidget(self.pb_export_csv)

        self.groupbox_select_export.setLayout(self.layout_select_export)
        self.layout_select.addWidget(self.groupbox_select_export)

        # ----------
        # Geometry
        # ----------
        self.layout_geometry_settings_interp = QGridLayout()
        self.layout_geometry_settings_interp.setAlignment(QtCore.Qt.AlignLeft)
        self.layout_geometry_settings_interp.addWidget(self.checkbox_geometry_interp_coords, 0, 0)
        self.layout_geometry_settings_interp.addWidget(self.checkbox_geometry_interp_subsnum, 1, 0)
        self.layout_geometry_settings_interp.addWidget(self.checkbox_geometry_interp_heatmap, 2, 0)
        self.layout_geometry_settings_interp.addWidget(self.label_heatmap_symbol_size, 2, 1)
        self.layout_geometry_settings_interp.addWidget(self.spinbox_heatmap_symbol_size, 2, 2)
        self.layout_geometry_settings_interp.addWidget(self.label_heatmap_symbol_opacity, 2, 3)
        self.layout_geometry_settings_interp.addWidget(self.spinbox_heatmap_symbol_opacity, 2, 4)
        self.layout_geometry_settings_interp.addWidget(self.pb_heatmap_refresh, 2, 5)
        self.groupbox_spectrum_interp_pos = QGroupBox("Spectrum Interpolated Positions")
        self.groupbox_spectrum_interp_pos.setLayout(self.layout_geometry_settings_interp)

        self.layout_geometry_settings_acu_cont = QGridLayout()
        self.layout_geometry_settings_acu_cont.setAlignment(QtCore.Qt.AlignLeft)
        self.layout_geometry_settings_acu_cont.addWidget(self.checkbox_geometry_acu_daz_del, 0, 0)
        self.layout_geometry_settings_acu_cont.addWidget(self.checkbox_geometry_acu_dazcosel_del, 1, 0)
        self.groupbox_acu_cont_pos = QGroupBox("ACU Continuous Positions")
        self.groupbox_acu_cont_pos.setLayout(self.layout_geometry_settings_acu_cont)

        self.layout_geometry_data = QHBoxLayout()
        self.layout_geometry_data.addWidget(self.glw_geometry)
        self.layout_geometry_data.addWidget(self.geometry_heatmap_hLUT)

        self.layout_geometry_settings = QHBoxLayout()
        #self.layout_geometry_settings.setAlignment(QtCore.Qt.AlignLeft)
        self.layout_geometry_settings.addWidget(self.groupbox_spectrum_interp_pos)
        self.layout_geometry_settings.addWidget(self.groupbox_acu_cont_pos)

        self.layout_tab_geometry = QVBoxLayout()
        self.layout_tab_geometry.addLayout(self.layout_geometry_settings)
        self.layout_tab_geometry.addLayout(self.layout_geometry_data)
        # Add the layout to the tab
        self.tab_geometry.setLayout(self.layout_tab_geometry)

        # ----------
        # Cross Scan
        # ----------
        self.layout_tab_cross = QGridLayout()
        self.layout_tab_cross.addWidget(self.glw_cross)

        # Add the layout to the tab
        self.tab_cross.setLayout(self.layout_tab_cross)

        # ----------
        # Top-level layout
        # ----------
        self.hbox_main.addLayout(self.layout_select)
        self.hbox_main.addWidget(self.tabgroup_graphs)

    def create_widgets_tabs(self):
        # Create the top-level tab group
        self.tabgroup_graphs = QTabWidget()

        # Create empty container widget for each tab
        self.tab_spectrogram = QWidget()
        self.tab_geometry = QWidget()
        self.tab_cross = QWidget()

        # Add tab container widgets to the tab group
        self.tabgroup_graphs.addTab(self.tab_spectrogram, "Spectrogram Slice")
        self.tabgroup_graphs.addTab(self.tab_geometry, "Geometry")
        self.tabgroup_graphs.addTab(self.tab_cross, "Cross Scan")

        # Listen for signal from QtGUI tab select
        self.tabgroup_graphs.currentChanged.connect(self.update_selected_tab)

    def create_widgets_geometry(self):
        # Create widgets for select options        
        self.checkbox_geometry_interp_coords = QCheckBox("coordinate symbols")
        self.checkbox_geometry_interp_coords.clicked.connect(self.show_geometry_interp_symbols_coords)
        self.checkbox_geometry_interp_subsnum = QCheckBox("subscan number labels")
        self.checkbox_geometry_interp_subsnum.clicked.connect(self.show_geometry_interp_symbols_subsnum)
        self.checkbox_geometry_interp_heatmap = QCheckBox("heatmap symbols")
        self.checkbox_geometry_interp_heatmap.clicked.connect(self.show_geometry_interp_symbols_heatmap)

        self.label_heatmap_symbol_size = QLabel("size [arcsec]")
        self.spinbox_heatmap_symbol_size = QSpinBox()
        self.spinbox_heatmap_symbol_size.setRange(0, 10000)
        self.spinbox_heatmap_symbol_size.setValue(100)
        self.spinbox_heatmap_symbol_size.setEnabled(False)
        self.spinbox_heatmap_symbol_size.valueChanged.connect(self.refresh_geometry_heatmap)

        self.label_heatmap_symbol_opacity = QLabel("opacity [0-255]")
        self.spinbox_heatmap_symbol_opacity = QSpinBox()
        self.spinbox_heatmap_symbol_opacity.setRange(0, 255)
        self.spinbox_heatmap_symbol_opacity.setValue(100)
        self.spinbox_heatmap_symbol_opacity.setEnabled(False)
        self.spinbox_heatmap_symbol_opacity.valueChanged.connect(self.refresh_geometry_heatmap)

        self.pb_heatmap_refresh = QPushButton("refresh heatmap")
        self.pb_heatmap_refresh.clicked.connect(self.refresh_geometry_heatmap)
        self.pb_heatmap_refresh.setEnabled(False)
        # Delta unicode symbol = "\u0394"
        self.checkbox_geometry_acu_daz_del = QCheckBox("\u0394 AZ, \u0394 EL")
        self.checkbox_geometry_acu_daz_del.clicked.connect(self.show_geometry_acu_daz_del)

        self.checkbox_geometry_acu_dazcosel_del = QCheckBox("\u0394 AZ * cos(EL), \u0394 EL")
        self.checkbox_geometry_acu_dazcosel_del.clicked.connect(self.show_geometry_acu_dazcosel_del)

        # Select coordinate symbol view at startup
        self.checkbox_geometry_interp_coords.setChecked(True)

        # Create AxisItems for time slice.  Include {top, bottom} x {Time, Angles} = 4 AxisItems
        self.geometry_axis_list = [
            pg.AxisItem(
                orientation="bottom",
                text="Longitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="top",
                text="Longitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="left",
                text="Latitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="right",
                text="Latitude offset from tracking",
                units="deg",
            ),
        ]

        # Scale axis to convert degrees to arcseconds
        self.geometry_axis_list[0].setScale(3600)
        self.geometry_axis_list[2].setScale(3600)

        # Disable automatic SI prefix and always show unit = arcsec
        self.geometry_axis_list[0].enableAutoSIPrefix(False)
        self.geometry_axis_list[2].enableAutoSIPrefix(False)

        for axisitem in self.geometry_axis_list:
            axisitem.showLabel(True)

        self.glw_geometry = pg.GraphicsLayoutWidget()
        # Create PlotItem from the PlotLayoutWidget.
        # Then Create the PlotDataItem from the PlotItem
        self.pi_geometry = self.glw_geometry.addPlot(
            0,
            0,
            title=self.scan_header_string,
            axisItems={
                "bottom": self.geometry_axis_list[0],
                "top": self.geometry_axis_list[1],
                "left": self.geometry_axis_list[2],
                "right": self.geometry_axis_list[3],
            },
        )
        self.pi_geometry.addLegend()
        self.pi_geometry.showGrid(True, True, alpha=0.3)
        self.pi_geometry.setAspectLocked(True)
        # -------------
        # Coord symbols
        # -------------
        # All symbols are the same, can draw in advance, not dependent on user frequency select
        # NOTE: pxMode defines the unit of size parameter.
        #  True : constant pixels. False : graph axis units
        self.spi_geometry_coords = pg.ScatterPlotItem(
            x=[0], y=[0], pen=None, brush="y", symbol="+", size=15, pxMode=True
        )
        self.spi_geometry_coords.setData(x=self.longoff_flat, y=self.latoff_flat, name="Spectrum Interpolated Position")
        self.pi_geometry.addItem(self.spi_geometry_coords)

        # -------------
        # Subscan label symbols
        # -------------
        # Symbols not the same, can draw in advance, not dependent on user frequency select
        self.spi_geometry_subscan_labels = pg.ScatterPlotItem(pxMode=True)
        self.geometry_subscan_labels = []
        for x, y, ss in zip(self.longoff_flat, self.latoff_flat, self.subsnum_flat):
            self.geometry_subscan_labels.append(
                {
                    "pos": (x, y),
                    "size": 15,
                    "pen": None,
                    "brush": "w",
                    "symbol": self.create_label_symbol(str(ss)),
                }
            )
        self.spi_geometry_subscan_labels.addPoints(self.geometry_subscan_labels)

        # -------------
        # Heatmap symbols
        # -------------
        # Symbols not the same, depend on user frequency select.  cannot draw in advance
        # NOTE:  use data from user selected frequency to calculate the total power in
        # the bandwidth. Then, we can create the symbol color and size based on the power.
        # See function update_graph_geometry()
        self.geometry_heatmap_hLUT = pg.HistogramLUTWidget()
        self.geometry_heatmap_hLUT.gradient.loadPreset("magma")
        # Don't connect the color gradient signal because it is too slow.
        # It works well for ImageItem (full grid of pixels).  Too slow for scatter plot.
        # self.geometry_heatmap_hLUT.sigLevelsChanged.connect(self.refresh_geometry_heatmap)

        # -------------------------------
        # ACU continuous position symbols
        # -------------------------------
        # All symbols are the same, can draw in advance, not dependent on user frequency select
        # NOTE: pxMode defines the unit of size parameter.
        #  True : constant pixels. False : graph axis units
        self.spi_geometry_acu_daz_del_coords = pg.ScatterPlotItem(
            x=[0], y=[0], pen=pg.mkPen("m", width=1), brush=None, symbol="o", size=20, pxMode=True
        )
        self.spi_geometry_acu_daz_del_coords.setData(x=self.acu_daz, y=self.acu_del, name="ACU \u0394 AZ, \u0394 EL")

        self.spi_geometry_acu_dazcosel_del_coords = pg.ScatterPlotItem(
            x=[0], y=[0], pen=pg.mkPen("c", width=1), brush=None, symbol="d", size=20, pxMode=True
        )
        self.spi_geometry_acu_dazcosel_del_coords.setData(x=self.acu_dazcosel, y=self.acu_del, name="ACU \u0394 AZ*cos(EL), \u0394 EL)")

    def create_widgets_spectrogram(self):
        # Set axis order show spectrogram time axis horizontal and frequency axis vertical
        pg.setConfigOptions(imageAxisOrder="col-major")

        # Create the spectrogram widget
        # Note that ImageView does not inherit from GraphicsItem.  It is a PyQt widget that has more interactive
        # functions than a simple GraphicsItemm.  Create an instance with parameter
        # view=pg.PlotItem() to show axis ticks and labels.
        self.imv_spectrogram = pg.ImageView(view=pg.PlotItem())
        self.imv_spectrogram.setPredefinedGradient("magma")

        # Configure the PlotItem to stretch the image to fit the window
        self.imv_spectrogram.view.setAspectLocked(False)
        self.imv_spectrogram.view.autoRange()

        # Set Y pixel coordinate origin  (pixel 0) to the bottom (default is top)
        # Coordinates for X pixels have origin (pixel x=0) at the left already
        self.imv_spectrogram.view.invertY(False)

        # Create axes
        self.imv_spectrogram_axis_top = pg.AxisItem("top")
        self.imv_spectrogram_axis_top.showLabel(True)

        self.imv_spectrogram_axis_left = pg.AxisItem("left")
        self.imv_spectrogram_axis_left.showLabel(True)

        self.imv_spectrogram_axis_right = pg.AxisItem("right")
        self.imv_spectrogram_axis_right.showLabel(True)

        self.imv_spectrogram_axis_bottom = pg.AxisItem("bottom")
        self.imv_spectrogram_axis_bottom.showLabel(True)

        self.imv_spectrogram.view.setAxisItems(
            axisItems={
                "top": self.imv_spectrogram_axis_top,
                "bottom": self.imv_spectrogram_axis_bottom,
                "left": self.imv_spectrogram_axis_left,
                "right": self.imv_spectrogram_axis_right,
            }
        )
        self.imv_spectrogram.view.showGrid(True, True)
        self.imv_spectrogram.timeLine.hide()

    def create_widgets_freq_slice(self):
        # Create labels for the X axis of frequency slice graph.
        self.xaxis_labels_freq_slice = [
            "Frequency",
        ]

        self.pw_freq_slice_axis_bottom = pg.AxisItem("bottom", text="Frequency", units="Hz")
        self.pw_freq_slice_axis_bottom.showLabel(True)

        # Setup widget to select X axis of time slice graph
        self.combobox_xaxis_freq_slice = QComboBox()
        self.combobox_xaxis_freq_slice.addItems(self.xaxis_labels_freq_slice)
        # Note - this combo box is not connected to any function.   Only placeholder
        # for future function.

        # Create PlotWidget to show 2-D frequency slice of spectrogram
        # empty plot. fill data later
        self.pw_freq_slice = pg.PlotWidget(name="pw_freq_slice")
        pen_freq_slice = pg.mkPen(color=("y"), width=2)
        self.plot_freq_slice = self.pw_freq_slice.plot(x=[0], y=[0], pen=pen_freq_slice)

    def create_widgets_time_slice(self):
        # Create labels for the X axis of time slice graph.
        self.xaxis_labels_time_slice = [
            "Time",
            "Angle offset from tracking AZ",
            "Angle offset from tracking EL",
        ]

        # Create AxisItems for time slice.  Include {top, bottom} x {Time, Angles} = 4 AxisItems
        self.pw_time_slice_axis_bottom_list = [
            pg.DateAxisItem(
                orientation="bottom",
                utcOffset=0,
                text="Universal Time Coordinated (UTC)",
            ),
            pg.AxisItem(
                orientation="bottom",
                text=self.xaxis_labels_time_slice[1],
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="bottom",
                text=self.xaxis_labels_time_slice[2],
                units="arcsec",
            ),
        ]

        # Scale axis to convert degrees to arcseconds
        self.pw_time_slice_axis_bottom_list[1].setScale(3600)
        self.pw_time_slice_axis_bottom_list[2].setScale(3600)

        # Disable automatic SI prefix and always show unit = arcsec
        self.pw_time_slice_axis_bottom_list[1].enableAutoSIPrefix(False)
        self.pw_time_slice_axis_bottom_list[2].enableAutoSIPrefix(False)

        for axisitem in self.pw_time_slice_axis_bottom_list:
            axisitem.showLabel(True)

        # Note: If utcOffset is not specified, DateAxisItem uses time.timezone to
        # automatically set the time axis to local time on this computer
        self.pw_time_slice_axis_top_list = [
            pg.DateAxisItem(
                orientation="top",
                text="Local Time (UTC+{})".format(-1 * time.timezone / 3600),
            ),
            pg.AxisItem(orientation="top", text=self.xaxis_labels_time_slice[1], units="deg"),
            pg.AxisItem(orientation="top", text=self.xaxis_labels_time_slice[2], units="deg"),
        ]

        # Disable automatic SI prefix and always show unit = deg
        self.pw_time_slice_axis_top_list[1].enableAutoSIPrefix(False)
        self.pw_time_slice_axis_top_list[2].enableAutoSIPrefix(False)

        for axisitem in self.pw_time_slice_axis_top_list:
            axisitem.showLabel(True)

        # Setup widget to select X axis of time slice graph
        self.combobox_xaxis_time_slice = QComboBox()
        self.combobox_xaxis_time_slice.addItems(self.xaxis_labels_time_slice)
        self.combobox_xaxis_time_slice.currentIndexChanged.connect(self.update_axis_time_slice)

        # Create PlotWidget to show 2-D time-slice of spectrogram
        # empty plot. fill data later
        self.pw_time_slice = pg.PlotWidget(name="pw_time_slice")
        self.plot_time_slice = self.pw_time_slice.plot(
            pen=None, symbol="o", symbolPen="c", symbolSize=5
        )

    def create_widgets_select(self):
        # Setup widget to select subscan
        self.label_select_subscan = QLabel("Subscan Number")
        self.combobox_subscan = QComboBox()
        self.combobox_subscan.addItems(map(str, self.subscan_number_list))
        self.combobox_subscan.currentIndexChanged.connect(self.update_subscan)

        # Setup widget to select polarization and cal noise state.
        self.label_select_polarization = QLabel("{Polarization, Cal Noise State}")
        self.combobox_polarization = QComboBox()

        self.label_fft_index_min = QLabel("Channel index min")
        self.spinbox_fft_index_min = QSpinBox()
        self.spinbox_fft_index_min.setRange(0, len(self.ff) - 2)
        self.spinbox_fft_index_min.setValue(0)
        self.spinbox_fft_index_min.valueChanged.connect(self.update_data_freq_range)

        self.label_fft_index_max = QLabel("Channel index max")
        self.spinbox_fft_index_max = QSpinBox()
        self.spinbox_fft_index_max.setRange(1, len(self.ff) - 1)
        self.spinbox_fft_index_max.setValue(len(self.ff) - 1)
        self.spinbox_fft_index_max.valueChanged.connect(self.update_data_freq_range)

        self.label_freq_min_name = QLabel("Minimum")
        self.label_freq_min_name.setTextInteractionFlags(txt_flags)
        self.label_freq_min_value = QLabel()
        self.label_freq_min_value.setTextInteractionFlags(txt_flags)

        self.label_freq_max_name = QLabel("Maximum")
        self.label_freq_max_name.setTextInteractionFlags(txt_flags)
        self.label_freq_max_value = QLabel()
        self.label_freq_max_value.setTextInteractionFlags(txt_flags)

        self.label_freq_select_nchan_name = QLabel("N channels")
        self.label_freq_select_nchan_name.setTextInteractionFlags(txt_flags)
        self.label_freq_select_nchan_value = QLabel()
        self.label_freq_select_nchan_value.setTextInteractionFlags(txt_flags)

        self.label_freq_select_fc_name = QLabel("Center")
        self.label_freq_select_fc_name.setTextInteractionFlags(txt_flags)
        self.label_freq_select_fc_value = QLabel()
        self.label_freq_select_fc_value.setTextInteractionFlags(txt_flags)

        self.label_freq_select_bw_name = QLabel("Bandwidth")
        self.label_freq_select_bw_name.setTextInteractionFlags(txt_flags)
        self.label_freq_select_bw_value = QLabel()
        self.label_freq_select_bw_value.setTextInteractionFlags(txt_flags)

        # Make a list of checkboxes based on the data file structure.
        # Add all of the checkboxes to the GUI.  Then check which are selected
        # when we export the data.
        self.checkbox_pol_list = []
        nsections = self.arraydata_hdulist[self.combobox_subscan.currentIndex()].data["DATA"].shape[1]
        for pol_noise_label in self.polarization_labels[nsections]:
            checkbox = QCheckBox("{}".format(pol_noise_label))
            self.checkbox_pol_list.append(checkbox)

        self.pb_export_csv = QPushButton(".CSV")
        self.pb_export_csv.clicked.connect(self.popup_export_csv_confirm)

    def update_selected_tab(self, index):
        if index is self.tabgroup_graphs.indexOf(self.tab_geometry):
            self.logger.debug("Geometry")
            self.update_graph_geometry()
        if index is self.tabgroup_graphs.indexOf(self.tab_spectrogram):
            self.logger.debug("Spectrogram")
            self.update_graph_spectrogram()
        if index is self.tabgroup_graphs.indexOf(self.tab_cross):
            self.logger.debug("Cross")
            self.update_graph_cross()

    def create_widgets_cross(self):
        # Create AxisItems
        self.cross_scan_axes = [
            pg.AxisItem(
                orientation="bottom",
                text="Angle offset from tracking AZ",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="bottom",
                text="Angle offset from tracking AZ",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="bottom",
                text="Angle offset from tracking EL",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="bottom",
                text="Angle offset from tracking EL",
                units="arcsec",
            ),
        ]

        for axis in self.cross_scan_axes:
            self.logger.debug("config axis: {}".format(axis))
            # Scale axis to convert degrees to arcseconds
            axis.setScale(3600)
            # Disable automatic SI prefix and always show unit = arcsec
            axis.enableAutoSIPrefix(False)
            axis.showLabel(True)

        # Create plots to show 2-D intensity vs angle of 4 subscans of cross
        # 4 subcans are {left -> right, right -> left, down -> up, up -> down}
        # empty plot. fill data later
        self.glw_cross = pg.GraphicsLayoutWidget()

        actual_data_pen = None
        actual_data_symbol = "o"
        actual_data_symbolPen = "c"
        actual_data_symbolSize = 5

        # Create PlotItem from the PlotLayoutWidget.
        # Then Create the PlotDataItem from the PlotItem
        self.pi_cross_left_right = self.glw_cross.addPlot(
            0,
            0,
            axisItems={"bottom": self.cross_scan_axes[0]},
        )
        self.pi_cross_left_right.showGrid(True, True, alpha=0.7)
        self.pdi_cross_left_right = self.pi_cross_left_right.plot(
            x=[0],
            y=[0],
            pen=actual_data_pen,
            symbol=actual_data_symbol,
            symbolPen=actual_data_symbolPen,
            symbolSize=actual_data_symbolSize,
        )

        self.pi_cross_right_left = self.glw_cross.addPlot(
            1,
            0,
            axisItems={"bottom": self.cross_scan_axes[1]},
        )
        self.pi_cross_right_left.showGrid(True, True, alpha=0.7)
        self.pdi_cross_right_left = self.pi_cross_right_left.plot(
            x=[0],
            y=[0],
            pen=actual_data_pen,
            symbol=actual_data_symbol,
            symbolPen=actual_data_symbolPen,
            symbolSize=actual_data_symbolSize,
        )

        self.pi_cross_down_up = self.glw_cross.addPlot(
            0,
            1,
            axisItems={"bottom": self.cross_scan_axes[2]},
        )
        self.pi_cross_down_up.showGrid(True, True, alpha=0.7)
        self.pdi_cross_down_up = self.pi_cross_down_up.plot(
            x=[0],
            y=[0],
            pen=actual_data_pen,
            symbol=actual_data_symbol,
            symbolPen=actual_data_symbolPen,
            symbolSize=actual_data_symbolSize,
        )

        self.pi_cross_up_down = self.glw_cross.addPlot(
            1,
            1,
            axisItems={"bottom": self.cross_scan_axes[3]},
        )
        self.pi_cross_up_down.showGrid(True, True, alpha=0.7)
        self.pdi_cross_up_down = self.pi_cross_up_down.plot(
            x=[0],
            y=[0],
            pen=actual_data_pen,
            symbol=actual_data_symbol,
            symbolPen=actual_data_symbolPen,
            symbolSize=actual_data_symbolSize,
        )

        # Add PlotItems, PlotDataItems, and title strings to a dictionary so we can
        # index by subscan number and iterate
        self.cross_widgets_dict = {
            0: {
                "PlotItem": self.pi_cross_left_right,
                "PlotDataItem": self.pdi_cross_left_right,
                "title": "Subscan 0 (LEFT -> RIGHT) ",
            },
            1: {
                "PlotItem": self.pi_cross_right_left,
                "PlotDataItem": self.pdi_cross_right_left,
                "title": "Subscan 1 (RIGHT -> LEFT) ",
            },
            2: {
                "PlotItem": self.pi_cross_down_up,
                "PlotDataItem": self.pdi_cross_down_up,
                "title": "Subscan 2 (DOWN -> UP) ",
            },
            3: {
                "PlotItem": self.pi_cross_up_down,
                "PlotDataItem": self.pdi_cross_up_down,
                "title": "Subscan 3 (UP -> DOWN) ",
            },
            100001: {
                "PlotItem": None,
                "PlotDataItem": None,
                "title": "Subscan 100001 (OFF SOURCE BEFORE) ",
            },
            100002: {
                "PlotItem": None,
                "PlotDataItem": None,
                "title": "Subscan 100001 (OFF SOURCE AFTER) ",
            },
        }

    def popup_export_csv_confirm(self):
        self.sections_to_export_index = []
        self.sections_to_export_names = []
        for section_index in np.arange(len(self.checkbox_pol_list)):
            cb = self.checkbox_pol_list[section_index]
            if cb.isChecked():
                # Append this section (pol / noise state) to the list to export
                self.sections_to_export_index.append(section_index)
                self.sections_to_export_names.append(cb.text())
        
        n_sections = len(self.sections_to_export_index)
        n_subs = len(self.arraydata_hdulist)
        n_integrations = self.arraydata_hdulist[0].data["DATA"].shape[0]
        n_files = n_sections * n_subs * n_integrations
        
        confirm_message = "export {} sections: {} * {} subscans * {} integrations. Total {} files".format(n_sections, self.sections_to_export_names, n_subs, n_integrations, n_files)
        self.logger.debug(confirm_message)

        result = QMessageBox.question(self, "Confirm Export", "Export {} files to path {} ?".format(n_files, os.getcwd()))
        if result == QMessageBox.Yes:
            self.export_csv()
        else:
            self.logger.debug("cancel export .csv")
        
    def export_csv(self):
        # Traverse the list of selected sections
        for section_index, section_name in zip(self.sections_to_export_index, self.sections_to_export_names):
            # create sutable file name string from section label (remove comma and space)
            section_prefix = "{}".format(section_index)+section_name.replace(",", "").replace(" ", "")
            
            # Traverse the list of subscans
            for subsnum, arraydata in zip(self.subscan_number_list, self.arraydata_hdulist):
                
                # Traverse all integrations / time steps of this subscan
                for timestep in np.arange(arraydata.data["DATA"].shape[0]):
                    spectrum_data = arraydata.data["DATA"][timestep][section_index]
                    data2D = np.array([np.arange(self.data.shape[2]), spectrum_data]).T

                    outfilename = "{}_section{}_subscan{:03d}_timestep{:03d}.csv".format(self.mbfits_basename, section_prefix, subsnum, timestep)
                    self.logger.debug("Export {}".format(outfilename))
                    np.savetxt(outfilename,
                        data2D,
                        fmt=["%d","%20.20e"],
                        delimiter=",",
                        header="FFT Channel Index,Power(linear SCALE_FACTOR * watts)"
                        )

        
    def update_data_freq_range(self):
        self.label_freq_min_value.setText(
            "{:.6f} MHz".format(self.ff[self.spinbox_fft_index_min.value()] / 1e6)
        )
        self.label_freq_max_value.setText(
            "{:.6f} MHz".format(self.ff[self.spinbox_fft_index_max.value()] / 1e6)
        )

        self.selected_freq_index_array = np.arange(
            self.spinbox_fft_index_min.value(), self.spinbox_fft_index_max.value() + 1
        )

        self.label_freq_select_nchan_value.setText("{}".format(len(self.selected_freq_index_array)))

        self.label_freq_select_fc_value.setText(
            "{:.6f} MHz".format(
                (0.5 / 1e6)
                * (
                    self.ff[self.spinbox_fft_index_min.value()]
                    + self.ff[self.spinbox_fft_index_max.value()]
                )
            )
        )

        self.label_freq_select_bw_value.setText(
            "{:.6f} MHz".format(
                (self.freq_res / 1e6)
                * (self.spinbox_fft_index_max.value() + 1 - self.spinbox_fft_index_min.value())
            )
        )

        # After frequencies are updated, use the same function as click on the tab so that
        # only the selected tab will update the graphs
        self.update_selected_tab(self.tabgroup_graphs.currentIndex())

    def update_graph_spectrogram(self):
        # Update axes and widgets that can change between subscans
        self.update_widgets_spectrogram()
        self.update_axis_freq_slice()
        self.update_axis_time_slice()

        # After frequency and time graphs are full of data, create the marker arrows
        # self.create_marker_arrows()

        # Now that we have set the new axis, draw the graph again using the correct x axis data
        # from the selection
        self.logger.debug("update time slice data ...")
        self.update_data_time_slice()

        # Now that we have set the new axis, draw the graph again using the correct x axis data
        # from the selection. NOTE: Frequency slice doesn't have any
        # option to select another x-axis.  So this code is only
        # placeholder structure for future.
        self.logger.debug("update freq slice data ...")
        self.update_data_freq_slice()

    def update_graph_cross(self):
        freq_range_string = "{:.6f} - {:.6f} MHz".format(
            self.ff[self.spinbox_fft_index_min.value()] / 1e6,
            self.ff[self.spinbox_fft_index_max.value()] / 1e6,
        )

        for subsnum, datapar, arraydata in zip(
            self.subscan_number_list, self.datapar_hdulist, self.arraydata_hdulist
        ):
            # add the power together for the range of selected frequencies
            # update the graph with power vs angle
            if (subsnum == 0) or (subsnum == 1):
                xdata = datapar.data["LONGOFF"]
            if (subsnum == 2) or (subsnum == 3):
                xdata = datapar.data["LATOFF"]

            # Sum the data together from all selected frequency channels.
            # Time dimension and length remains the same
            # Polarization is reduced to length 1 before the np.sum function.
            # Array shape will be (N time, 1 polarization, N frequency)
            # When an array dimension has length==1, numpy reduces this "singleton dimension",
            # Result array dimensions are (N time, N frequency)
            # Therefore, the array dimension for frequency axis moves from axis 2 to axis 1.
            # use this for np.sum
            ydata = np.sum(
                arraydata.data["DATA"][
                    :,
                    self.combobox_polarization.currentIndex(),
                    self.selected_freq_index_array,
                ],
                1,
            )

            # self.logger.debug("{}".format("ydata.shape: {}".format(ydata.shape)))
            self.cross_widgets_dict[subsnum]["PlotDataItem"].setData(x=xdata, y=ydata)
            self.cross_widgets_dict[subsnum]["PlotItem"].setTitle(
                self.cross_widgets_dict[subsnum]["title"] + freq_range_string
            )

    def update_graph_geometry(self):
        # Geometry tab coordinates and labels don't need to update.  They are constant across
        # any other user settings such as polarization or frequency.  However,
        # The heatmap needs to update if the user changes frequency.
        self.logger.debug("If heatmap is enabled, update the data for freq range")
        if self.checkbox_geometry_interp_heatmap.isChecked():
            self.show_geometry_interp_symbols_heatmap()

    def updata_data_heatmap(self):
        # Add the data together from all selected frequency channels using np.sum.
        # Select polarization and reduce dimension to 1 before np.sum
        # When an array dimension has length==1, numpy reduces this "singleton dimension",
        # Result array dimensions are (N time, N frequency)
        # Therefore, the array dimension for frequency axis moves from axis 2 to axis 1.
        # After accumulating the sum of many frequencies into 1 number and reduce the singleton
        # dimensions, the final output array has dimension (N time,)

        self.heatmap_magnitude_freqrange = np.sum(
            self.heatmap_magnitude[
                :,
                self.combobox_polarization.currentIndex(),
                self.selected_freq_index_array,
            ],
            1,
        )
        # self.logger.debug(
        #     "heatmap_magnitude_freqrange.shape: {}, data: {}".format(
        #         self.heatmap_magnitude_freqrange.shape, self.heatmap_magnitude_freqrange
        #     )
        # )

        self.magnitude_min = np.min(self.heatmap_magnitude_freqrange)
        self.magnitude_max = np.max(self.heatmap_magnitude_freqrange)

        # Create a "fake" ImageItem using the magnitude data.  This ImageItem will not be
        # displayed.  It will only be used to fill the HistogramLUT and color gradient.
        # After gradient is filled, we must manually read the colors back from the
        # GradientEditorItem and set the colors of points in the heatmap ScatterPlotItem
        img_fake = pg.ImageItem(
            np.stack([self.heatmap_magnitude_freqrange, self.heatmap_magnitude_freqrange])
        )
        self.geometry_heatmap_hLUT.setImageItem(img_fake)

        self.logger.debug(
            "In selected freq range mag min: {}, max: {}".format(
                self.magnitude_min, self.magnitude_max
            )
        )

    def generate_scatterplot_heatmap(self):
        # TDDO (SS. 10/2022): Gradient does not appear to change when I drag the sliders.  I don't
        # understand why, but fix this later.
        # lut = self.geometry_heatmap_hLUT.gradient.getLookupTable(10)
        # self.logger.debug("LUT: {}".format(lut))

        spi_geometry_heatmap = pg.ScatterPlotItem(pxMode=False)
        # scatterplot pxMode=False, symbol size scales with axes.  Not fixed pixel size.

        symbol_size = self.spinbox_heatmap_symbol_size.value() / 3600
        self.geometry_heatmap_spots = []
        for x, y, mag in zip(self.longoff_flat, self.latoff_flat, self.heatmap_magnitude_freqrange):
            self.geometry_heatmap_spots.append(
                self.create_heatmap_symbol(
                    x, y, mag, symbol_size, self.spinbox_heatmap_symbol_opacity.value()
                )
            )
        spi_geometry_heatmap.addPoints(self.geometry_heatmap_spots)
        return spi_geometry_heatmap

    def create_heatmap_symbol(self, x, y, magnitude, spotsize, opacity):
        # self.logger.debug("create heatmap symbol (x, y, mag): {}, {}, {}".format(x, y, magnitude))
        color = self.geometry_heatmap_hLUT.gradient.getColor(
            magnitude / self.magnitude_max, toQColor=False
        )
        # self.logger.debug("type(color): {}, color: {}".format(type(color), color))
        spot = {
            "pos": (x, y),
            "size": spotsize,
            "pen": None,
            "brush": pg.mkBrush((color[0], color[1], color[2], opacity)),
            "symbol": "o",
        }
        return spot

    def create_label_symbol(self, label):
        # copy from pyqtgraph example ScatterPlot.py
        symbol = QPainterPath()
        f = QFont()
        f.setPointSize(10)
        symbol.addText(0, 0, f, label)
        br = symbol.boundingRect()
        scale = min(1.0 / br.width(), 1.0 / br.height())
        tr = QTransform()
        tr.scale(scale, scale)
        tr.translate(-br.x() - br.width() / 2.0, -br.y() - br.height() / 2.0)
        return tr.map(symbol)

    def show_geometry_interp_symbols_coords(self):
        try:
            self.logger.debug("remove coordinate '+' symbols")
            self.pi_geometry.removeItem(self.spi_geometry_coords)
        except AttributeError:
            self.logger.debug("symbols don't exist. nothing to remove")

        if self.checkbox_geometry_interp_coords.isChecked():
            self.logger.debug("add coordinate '+' symbols")
            self.pi_geometry.addItem(self.spi_geometry_coords)

    def show_geometry_interp_symbols_subsnum(self):
        try:
            self.logger.debug("remove subscan label symbols")
            self.pi_geometry.removeItem(self.spi_geometry_subscan_labels)
        except AttributeError:
            self.logger.debug("labels don't exit. nothing to remove")

        if self.checkbox_geometry_interp_subsnum.isChecked():
            self.logger.debug("add subscan label symbols")
            self.pi_geometry.addItem(self.spi_geometry_subscan_labels)

    def show_geometry_interp_symbols_heatmap(self):
        if self.checkbox_geometry_interp_heatmap.isChecked():
            self.spinbox_heatmap_symbol_size.setEnabled(True)
            self.spinbox_heatmap_symbol_opacity.setEnabled(True)
            self.pb_heatmap_refresh.setEnabled(True)
            self.geometry_freq_range_string = "{:.6f} - {:.6f} MHz".format(
                self.ff[self.spinbox_fft_index_min.value()] / 1e6,
                self.ff[self.spinbox_fft_index_max.value()] / 1e6,
            )

            self.logger.debug("add heatmap symbols and freq range title")
            self.pi_geometry.setTitle(
                "{}, Frequency ({})".format(
                    self.scan_header_string, self.geometry_freq_range_string
                )
            )
            # For some reason, removeItem does not work consistently, so clear the PlotItem
            # and restore the coord symbols and labels
            self.pi_geometry.clear()
            self.show_geometry_interp_symbols_coords()
            self.show_geometry_interp_symbols_subsnum()
            self.updata_data_heatmap()
            self.spi_geometry_heatmap = self.generate_scatterplot_heatmap()
            self.pi_geometry.addItem(self.spi_geometry_heatmap)
        else:
            self.spinbox_heatmap_symbol_size.setEnabled(False)
            self.spinbox_heatmap_symbol_opacity.setEnabled(False)
            self.pb_heatmap_refresh.setEnabled(False)
            self.logger.debug("remove heatmap symbols and freq range title")
            self.pi_geometry.setTitle(self.scan_header_string)

            # For some reason, removeItem does not work consistently, so clear the PlotItem
            # and restore the coord symbols and labels
            self.pi_geometry.clear()
            self.show_geometry_interp_symbols_coords()
            self.show_geometry_interp_symbols_subsnum()

    def show_geometry_acu_daz_del(self):
        try:
            self.logger.debug("remove acu_daz_del symbols")
            self.pi_geometry.removeItem(self.spi_geometry_acu_daz_del_coords)
        except AttributeError:
            self.logger.debug("symbols don't exist. nothing to remove")

        if self.checkbox_geometry_acu_daz_del.isChecked():
            self.logger.debug("add acu_daz_del symbols")
            self.pi_geometry.addItem(self.spi_geometry_acu_daz_del_coords)

    def show_geometry_acu_dazcosel_del(self):
        try:
            self.logger.debug("remove acu_dazcosel_del symbols")
            self.pi_geometry.removeItem(self.spi_geometry_acu_dazcosel_del_coords)
        except AttributeError:
            self.logger.debug("symbols don't exist. nothing to remove")

        if self.checkbox_geometry_acu_dazcosel_del.isChecked():
            self.logger.debug("add acu_dazcosel_del symbols and legend")
            self.pi_geometry.addItem(self.spi_geometry_acu_dazcosel_del_coords)

    def refresh_geometry_heatmap(self):
        # self.logger.debug("draw new scatter plot")
        # For some reason, removeItem does not work consistently, so clear the PlotItem
        # and restore the coord symbols and labels
        self.pi_geometry.clear()
        self.show_geometry_interp_symbols_coords()
        self.show_geometry_interp_symbols_subsnum()
        self.pi_geometry.addItem(self.generate_scatterplot_heatmap())

    def remove_select_freq_cursor(self):
        try:
            self.logger.debug("Remove freq select cursor ...")
            self.imv_spectrogram.removeItem(self.roi_select_frequency)
        except AttributeError:
            self.logger.debug("ROI select freq cursor does not exist. nothing to remove")

    def remove_select_time_cursor(self):
        try:
            self.logger.debug("Remove time select cursor ...")
            self.imv_spectrogram.removeItem(self.roi_select_time)
        except AttributeError:
            self.logger.debug("ROI select time cursor does not exist. nothing to remove")

    def create_select_freq_cursor(self):
        # The frequency axis is the same for all subscans, but we must create a new
        # new roi_select_freq slider for each subscan the horizontal length of the
        # displayed line in the GUI changes following the number of time steps
        # in the time axis (horizontal pixels of spectrogram)
        self.remove_select_freq_cursor()

        # If this is not the first run, and ROI select freq has already been used to select a frequency ...
        # re-use the selected frequency when we create a new frequency select cursor.
        # This is convenient for the  user to jump between
        # subscans or polarizations and see the same selected frequency.  This works for frequency axis because
        # it does not change between subscans (unlike time axis, which is different every subscan)
        try:
            self.logger.debug("try: if self.ff_channel_index exists (from previous subscan) ...")
            self.logger.debug(
                "YES. fft_channel_index already selected {}".format(self.fft_channel_index)
            )
            # Define ROI cursor lines and set the initial position to be the fft channel index
            # that was selected already
            self.logger.debug("Create ROI select freq cursor using already selected frequency")
            self.roi_select_frequency = pg.LineSegmentROI(
                [
                    [0, self.fft_channel_index],
                    [
                        self.data.shape[self.DIM_TIME],
                        self.fft_channel_index,
                    ],
                ],
                maxBounds=QtCore.QRect(
                    0,
                    int(-1 * self.fft_channel_index),
                    0,
                    int(self.data.shape[self.DIM_FREQUENCY] + 1),
                ),
                snapSize=1,
                translateSnap=True,
                pen="g",
                removable=True,
            )

        except AttributeError:
            self.logger.debug("self.fft_channel_index does not exist")
            self.logger.debug("First run. create new ROI select frequency at the center")
            # Define ROI cursor lines and set the initial position to be the center of
            # the spectrogram (time / 2, frequency /2)
            self.roi_select_frequency = pg.LineSegmentROI(
                [
                    [0, int(self.data.shape[self.DIM_FREQUENCY] / 2)],
                    [
                        self.data.shape[self.DIM_TIME],
                        int(self.data.shape[self.DIM_FREQUENCY] / 2),
                    ],
                ],
                maxBounds=QtCore.QRect(
                    0,
                    -1 * int(self.data.shape[self.DIM_FREQUENCY] / 2),
                    0,
                    self.data.shape[self.DIM_FREQUENCY] + 1,
                ),
                snapSize=1,
                translateSnap=True,
                pen="g",
                removable=True,
            )

        self.imv_spectrogram.addItem(self.roi_select_frequency)
        self.roi_select_frequency.sigRegionChanged.connect(self.update_data_time_slice)

    def create_select_time_cursor(self):
        # Delete old ROI time select (freq slice) cursor line and create new with
        # boundaries that make sense for the current subscan. -- because each subscan
        # has different timestamps and time axis that you can select
        self.remove_select_time_cursor()

        self.logger.debug("create new ROI select time cursor and select time at index 0")
        self.roi_select_time = pg.LineSegmentROI(
            [
                [0, 0],
                [
                    0,
                    self.data.shape[self.DIM_FREQUENCY],
                ],
            ],
            maxBounds=QtCore.QRect(
                0,
                0,
                self.data.shape[self.DIM_TIME] + 1,
                0,
            ),
            snapSize=1,
            translateSnap=True,
            pen="y",
            removable=True,
        )

        self.imv_spectrogram.addItem(self.roi_select_time)
        self.roi_select_time.sigRegionChanged.connect(self.update_data_freq_slice)

    def update_widgets_spectrogram(self):
        self.logger.debug("set axes of spectrogram for selected subscan")
        self.imv_spectrogram_axis_top.setLabel(
            "Integration Number Index (subscan {}, {} points, {} s per point)".format(
                self.subscan_number_list[self.combobox_subscan.currentIndex()],
                len(self.time_mjd),
                self.datapar_hdulist[self.combobox_subscan.currentIndex()].data["INTEGTIM"][0],
            )
        )
        # self.imv_spectrogram_axis_bottom.setLabel("(TO DO!) Time")

        # self.imv_spectrogram_axis_left.setLabel("(TO DO!) Frequency")

        self.imv_spectrogram_axis_right.setLabel(
            "FFT Channel Index ({} points, {} kHz per point)".format(
                self.data.shape[self.DIM_FREQUENCY], self.freq_res / 1e3
            )
        )

        # Polarization combo box
        # The logic chooses from number of data sections if
        # we are using H/V dualpol or Stokes spectrometer.
        count = self.combobox_polarization.count()
        if count == 0:
            self.logger.debug(
                "combobox_polarization has 0 items.  Add items: {}".format(
                    self.polarization_labels[self.data.shape[self.DIM_SECTION]]
                )
            )
            self.combobox_polarization.addItems(
                self.polarization_labels[self.data.shape[self.DIM_SECTION]]
            )
            self.combobox_polarization.currentIndexChanged.connect(self.update_polarization)
        else:
            self.logger.debug(
                "combobox_polarization already has {} items.  Nothing to do".format(count)
            )

        self.logger.debug("update spectrogram data ...")
        self.update_data_spectrogram()

        self.logger.debug(
            "Subscan {} data has dimensions: {}".format(
                self.subscan_number_list[self.combobox_subscan.currentIndex()],
                self.data.shape,
            )
        )

        self.logger.debug("create cursors ...")

        self.create_select_freq_cursor()
        self.create_select_time_cursor()

    def update_data_spectrogram(self):
        polarization_index = self.combobox_polarization.currentIndex()
        self.logger.debug(
            "set spectrogram data to polarization index: {}".format(polarization_index)
        )
        self.imv_spectrogram.setImage(self.intensity3D[polarization_index])

    def update_data_time_slice(self):
        try:
            (ydata, coords) = self.roi_select_frequency.getArrayRegion(
                self.intensity3D,
                self.imv_spectrogram.imageItem,
                axes=(1, 2),
                returnMappedCoords=True,
            )
            # self.logger.debug('ydata shape: {} val: {}'.format(ydata.shape, ydata))
            self.fft_channel_index = int(coords[1, 0])
        except AttributeError:
            self.logger.debug("roi_select_freq cursor does not exist ... select fft channel 0")
            self.fft_channel_index = 0

            # select frequency channel index.  polarization and time axis remain full size
            ydata = self.intensity3D[:, :, self.fft_channel_index]

        selected_frequency = self.ff[self.fft_channel_index]

        self.pw_time_slice.setLabel(
            "left",
            "Power at Freq Index {:d} [{:.3f} MHz]".format(
                self.fft_channel_index, selected_frequency / 1e6
            ),
            units="dB no cal",
        )
        self.plot_time_slice.setData(
            x=self.xdata_time_slice_list[self.selected_xaxis_time_slice],
            y=ydata[int(self.combobox_polarization.currentIndex())],
        )

        try:
            self.freq_tracking_point.setIndex(self.fft_channel_index)
        except AttributeError:
            self.logger.debug("freq_tracking_point not created yet. create it now")
            self.freq_tracking_point = pg.CurvePoint(self.plot_freq_slice)
            self.arrow_freq = pg.ArrowItem(angle=270)
            self.arrow_freq.setParentItem(self.freq_tracking_point)

    def update_axis_time_slice(self):
        self.selected_xaxis_time_slice = self.combobox_xaxis_time_slice.currentIndex()
        self.logger.debug(
            "set X axis to option {}: {}".format(
                self.selected_xaxis_time_slice,
                self.xaxis_labels_time_slice[self.selected_xaxis_time_slice],
            )
        )

        self.pw_time_slice.setAxisItems(
            axisItems={
                "bottom": self.pw_time_slice_axis_bottom_list[self.selected_xaxis_time_slice],
                "top": self.pw_time_slice_axis_top_list[self.selected_xaxis_time_slice],
            }
        )
        self.pw_time_slice.showGrid(True, True, alpha=0.7)

    def update_axis_freq_slice(self):
        self.pw_freq_slice.setAxisItems(axisItems={"bottom": self.pw_freq_slice_axis_bottom})
        self.pw_freq_slice.showGrid(True, True, alpha=0.7)

    def update_data_freq_slice(self):
        try:
            (ydata, coords) = self.roi_select_time.getArrayRegion(
                self.intensity3D,
                self.imv_spectrogram.imageItem,
                axes=(1, 2),
                returnMappedCoords=True,
            )
            # self.logger.debug('ydata shape: {} val: {}'.format(ydata.shape, ydata))
            integration_number = int(coords[0, 0])
        except AttributeError:
            self.logger.debug(
                "roi_select_time cursor does not exist ... select integration number 0"
            )
            integration_number = 0

            # select an integration number (time axis).  polarization and freq axis remain full size
            ydata = self.intensity3D[:, integration_number, :]

        selected_time_iso = self.time_iso[integration_number]

        self.pw_freq_slice.setLabel(
            "left",
            "Power at Time Index {:d} [{}]".format(integration_number, selected_time_iso),
            units="dB no cal",
        )
        self.plot_freq_slice.setData(
            x=self.ff, y=ydata[int(self.combobox_polarization.currentIndex())]
        )

        try:
            self.time_tracking_point.setIndex(integration_number)
        except AttributeError:
            self.logger.debug("time_tracking_point not created yet. create it now")
            self.time_tracking_point = pg.CurvePoint(self.plot_time_slice)
            self.arrow_time = pg.ArrowItem(angle=270)
            self.arrow_time.setParentItem(self.time_tracking_point)
        except IndexError:
            self.logger.debug(
                "time_tracking_point previous index not available in this subscan.  Set to 0"
            )
            self.time_tracking_point.setIndex(0)

    def update_data_subscan(self):
        # Get the index of selected subscan from combo box
        self.logger.debug(
            "combobox index: {}. subscan number from .mbfits: {}".format(
                self.combobox_subscan.currentIndex(),
                self.subscan_number_list[self.combobox_subscan.currentIndex()],
            )
        )
        # Define shortcuts for the array dimensions to avoid confusion
        self.DIM_TIME = 0
        self.DIM_SECTION = 1
        self.DIM_FREQUENCY = 2

        # Set the current selected subscan data from the list of
        # ARRAYDATA indexed by subscan combobox index. (ref. function load_data)

        # Swap the axes from [TIME][SECTION][FREQUENCY] --> [SECTION][TIME][FREQUENCY] so we can use
        # the 3D slider of PyQtGraph ImageView
        self.data = (
            self.arraydata_hdulist[self.combobox_subscan.currentIndex()]
            .data["DATA"]
            .swapaxes(self.DIM_TIME, self.DIM_SECTION)
        )

        # Now that axes are swapped, edit the dimension "shortcuts"
        self.DIM_SECTION = 0
        self.DIM_TIME = 1

        # Prepare data for Time axis
        self.time_mjd = self.datapar_hdulist[self.combobox_subscan.currentIndex()].data["MJD"]
        self.time_astropy = Time(self.time_mjd, format="mjd", scale="utc")
        self.time_unix = self.time_astropy.unix
        self.time_iso = self.time_astropy.iso

        # Prepare data for Angle axis X and Y
        # NOTE: angle data in .mbfits file has unit = deg.
        # We can display the data in arcsec using AxisItem.setScale(3600)
        self.angle_offset_x = self.datapar_hdulist[self.combobox_subscan.currentIndex()].data[
            "LONGOFF"
        ]
        self.angle_offset_y = self.datapar_hdulist[self.combobox_subscan.currentIndex()].data[
            "LATOFF"
        ]

        self.xdata_time_slice_list = [
            self.time_unix,
            self.angle_offset_x,
            self.angle_offset_y,
        ]

        self.intensity3D = 10 * np.log10(np.abs(self.data))

    def update_subscan(self):
        self.update_data_subscan()
        self.update_selected_tab(self.tabgroup_graphs.currentIndex())

    def update_polarization(self):
        self.update_selected_tab(self.tabgroup_graphs.currentIndex())


if __name__ == "__main__":
    # Start logger
    logger = logging.getLogger(__name__)
    configure_logger(logger)
    logger.info("Started log for {}".format(logger.name))

    parser = argparse.ArgumentParser(
        description="Reads FITS file written by PipelineMBfits and diplays some graphs",
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument("filename", help="FITS file with format specified by AntennaDataFormat.py")

    # If no command line arguments are avaiable, show help message and exit
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        exit()

    args = parser.parse_args()

    if os.path.isfile(args.filename) is not True:
        logger.error("File not found")
        logger.error("os.path.isfile({}) = False".format(args.filename))
        sys.exit(1)
    else:
        logger.info("Processing file {}: ".format(args.filename))

        # Create instance of Qt GUI Application
        app = QApplication([])

        # Set theme style
        app.setStyle('Fusion')
        
        # Set colors
        app.setPalette(dark_theme)

        # Create instance of MainWindow.  The class MainWindow inherits QtGui objects
        # from Qt.QMainWindow and from DataMonitorGUI.  It extends the parent classes by
        # adding non-GUI member variables and functions to update GUI.
        window = MainWindow(args.filename)

        # Enable the window to show after Qt event loop starts
        window.show()

        # Start Qt Event loop (including open GUI window)
        app.exec_()

        # After GUI window is closed, clean up.
        window.shutdown()

        # End Python process
        sys.exit(0)
