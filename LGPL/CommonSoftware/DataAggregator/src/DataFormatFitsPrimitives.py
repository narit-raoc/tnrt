# -*- coding: ascii -*-
# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.05.15

# Primitive data types from MBfits and ALMATIfits specification defined in terms of numpy binary data types
# Use these fits data types in FITS binary table column definitions.  Internally, Python / numpy / astropy
# will use the numpy data types, but these "alias" types make easier for a human to compare the source code
# to the FITS specification documents.

import numpy as np
L = np.bool_	# 1-byte logical
A = np.unicode_	# 1-byte ASCII character
I = np.int16	# 2-byte integer (+/-) 32767
J = np.int32	# 4-byte integer
E = np.float32 	# 4-byte real
D = np.float64 	# 8-byte double
# P = ?? # 16-byte pointer (not used in numpy / astropy fits implementation)



