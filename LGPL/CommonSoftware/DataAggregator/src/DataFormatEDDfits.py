# -*- coding: ascii -*-
# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.02.05

import ctypes as C
import numpy as np

TYPE_MAP = {"F": (C.c_float, "float32"), "I": (C.c_uint32, "uint32")}

# Artificial time delta between noise diode on / off status (same as value in edd_fits_interface.py)
NOISEDIODETIMEDELTA = 0.001


class FWSectionHeader(C.LittleEndianStructure):
    _pack_ = 1

    _fields_ = [("section_id", C.c_uint32), ("nchannels", C.c_uint32)]

    def __repr__(self):
        return "<{} {}>".format(
            self.__class__.__name__,
            ", ".join(["{} = {}".format(key, getattr(self, key)) for key, _ in self._fields_]),
        )


class FWHeader(C.LittleEndianStructure):
    _pack_ = 1
    _fields_ = [
        ("data_type", C.c_char * 4),
        ("channel_data_type", C.c_char * 4),
        ("packet_size", C.c_uint32),
        ("backend_name", C.c_char * 8),
        ("timestamp", C.c_char * 28),
        ("integration_time", C.c_uint32),
        ("blank_phases", C.c_uint32),
        ("nsections", C.c_uint32),
        ("blocking_factor", C.c_uint32),
    ]

    def __repr__(self):
        return "<{} {}>".format(
            self.__class__.__name__,
            ", ".join(["{} = {}".format(key, getattr(self, key)) for key, _ in self._fields_]),
        )
