# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

from unittest_common import DataAggregatorTestCase

# import TCS modules
import DataAggregatorError


class RemoteExceptionTestCase(DataAggregatorTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1000_EddFitsClient_ConnectionRefused(self):
        with self.assertRaises(DataAggregatorError.ConnectionRefusedErrorEx):
            self.logger.debug("Call fits_client_start when server is not available")
            self.logger.debug("raise ConnectionRefusedError ...")
            self.objref.fits_client_start()
