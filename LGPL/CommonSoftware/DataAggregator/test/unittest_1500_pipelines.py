# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
import os
import time
import subprocess
import numpy as np
from astropy.time import Time
import threading

import AntennaDefaults
from DataSimulator import HolographySimulator
from BackendDefaults import generate_dtype_msg_holodata
import tnrtAntennaMod
from Supplier import Supplier
from CentralDB import CentralDB
import cdbConstants

from unittest_common import AsyncMessagingTestCase


class PipelineTestCase(AsyncMessagingTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1510_PipelineMBfits(self):
        """
        EddFits packets should arrive at EddFitsClient form the socket.  However, this test bypasses
        the socket connection and generates packets of the same data structure in DataAggregator.
        Then, use these locally-generated packets to test PipelineMBfits with top-down control from
        DataAggregator.
        """
        DELAY = 0.05

        # Paramters to simulate data and generate spectrum arraydata_row objects.
        subscan_duration = 10
        integration_time = 0.2
        nsections = 4
        nchannels = 2048
        subs_num = '1'

        # Simulate header data
        (metadata, time_apy) = self.simulate_headers()

        # Publish new scan metadata.  DataAggregator notification consumer will catch this data and keep it in the local self.metadata
        self.suppliers["scan"]["supplier"].publish_event(metadata)
        time.sleep(DELAY)

        # Do a test of pipeline with simulated data for a pointing scan.
        self.objref.open_pipeline("mbfits")
        time.sleep(DELAY)

        # Clear accumulators of MONITOR-XXX for each new SCAN
        self.objref.clear_monitor_buffers()
        time.sleep(DELAY)

        # Write current scan header in all open pipelines
        self.objref.write_scan_header()
        time.sleep(DELAY)

        # Clear accumulators of ARRAYDATA and DATAPAR for each new SUBSCAN
        self.objref.clear_subscan_buffers()
        time.sleep(DELAY)

        # Write subscan header in all open pipelines
        self.objref.write_before_subscan()
        time.sleep(DELAY)

        # Enable writing realtime packets
        self.objref.enable_write("mbfits", subs_num)

        t = threading.Timer(5.0, self.objref.disable_write, ['mbfits', subs_num] )
        t.start()

        self.logger.debug("simulate spectrum data 1 subscan ...")
        self.objref.simulate_spectrum_arraydata_subscan(
            subscan_duration, integration_time, nsections, nchannels
        )
        # Note >>>> simulate_spectrum_arraydata_subscan blocks until simulated subscan is finished.

        # Subscan is finished.  Write all the subscan data
        self.objref.write_after_subscan(subs_num)

        self.objref.write_monitor()

        # Close all
        filename = self.objref.close_pipeline("mbfits")
        self.logger.debug("closed file {}".format(filename))

        # Preview file.  Block unittst until the user close the GUI window
        absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/view_mbfits.py"
        cmd = ["python", absolute_path, filename]
        self.logger.debug("Launching process: {}".format(cmd))
        self.gui_proc = subprocess.Popen(cmd)
        returncode = self.gui_proc.wait()
        self.logger.debug("GUI process finished. return code: {}".format(returncode))

    def test1520_PipelineALMATIfits(self):
        # clear cdb and fill correct metadata
        cdb = CentralDB()
        cdb.reset_obs()
        
        self.suppliers['antenna'] = {
            "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
            "dtype": tnrtAntennaMod.AntennaStatusNotifyBlock,
            "supplier": None,
        }
        self.suppliers['antenna']["supplier"] = Supplier(self.suppliers['antenna']["channel"])

        # Simple test.  Send one event of each type.  View Logs to confirm
        # That all events are received by the DataAggregator.  If DataMonitor GUI is open,
        # look at update information in each section.
        DELAY = 0.2
    
        # Simulate header data
        (metadata, time_apy) = self.simulate_headers()
        # Modify some items that are not the same as MBFITS defaults
        metadata.PRIMARY_MBFITS_HEADER.CREATOR = "test75_PipelineALMATIfits"
        metadata.SCAN_MBFITS_HEADER.BLNGTYPE = "ALON-GLS"
        metadata.SCAN_MBFITS_HEADER.BLATTYPE = "ALAT-GLS"
        
        # Edit some items of DATAPAR -- these must be done in redis CDB, not only in temporary
        # idl structures.  In the future, it will all be in redis CDB.
        cdb.set(cdbConstants.SUBS_NLNGTYPE, "ALON-GLS")
        cdb.set(cdbConstants.SUBS_NLATTYPE, "ALAT-GLS")
        cdb.set(cdbConstants.SCANDIR, "ALON")

        # Publish new scan metadata.  DataAggregator notification consumer subscribes to this data.
        # Use it to create next header
        self.suppliers["scan"]["supplier"].publish_event(metadata)
        time.sleep(DELAY)

        # Do a test of pipeline with simulated data for a pointing scan.
        self.objref.open_pipeline("atfits")
        time.sleep(DELAY)

        # Clear accumulators of MONITOR-XXX for each new SCAN
        self.objref.clear_monitor_buffers()
        time.sleep(DELAY)

        # Write current scan header in all open pipelines
        self.objref.write_scan_header()
        time.sleep(DELAY)

        # Simulate data arrays directly (in real system, DataAggregator
        # gets access to all of the data and prepares the data arrays in real-time).
        npoints = 16
        angular_resolution = 160
        time_per_angular_step = 0.2
        subscans_per_cal = 1
        integrations_per_calN = 10
        integrations_per_cal0 = 10
        peak_magntidue_ref = 8.5e-6  # mean value of first calibration in Yebes file TEST5772.FITS
        peak_magntidue_tst = 5.8e-5  # mean value of first calibration in Yebes file TEST5772.FITS
        fwhm = 160
        
        hsim = HolographySimulator(
            npoints,
            angular_resolution,
            time_per_angular_step,
            subscans_per_cal,
            integrations_per_calN,
            integrations_per_cal0,
            peak_magntidue_ref,
            peak_magntidue_tst,
            fwhm,
        )

        eventAntenna = AntennaDefaults.metadata
        
        # Generate numpy array datatype for 1024 points of FFT
        fft_length = 1024
        dtype_msg_holodata = generate_dtype_msg_holodata(fft_length)

        # create msg_holodate numpy array of zeros.  Use np.squeeze to reduce arrays lengh=1 to be scalar values
        msg_holodata = np.squeeze(np.zeros(1, dtype=dtype_msg_holodata))

        sample_rate = 51.2e3 # default sample rate of Keysight FFT analyzer before decimation (DDC)
        fft_length = 1024
        freq_ref = 20e3
        freq_tst = 20e3
        noise_magnitude_ref = 1e-7 # I don't know the real system values.  make something convenient for sim
        noise_magnitude_tst = 1e-9

        # A series of time points to generate all signals for spectrum simulation
        tt = np.arange(0, fft_length) / sample_rate
        
        msg_holodata["sample_capture_duration"] = time_per_angular_step    
        msg_holodata["spectrum_freq_axis"] = np.linspace(0, sample_rate, fft_length)

        
        # --------------------------------------------------
        # first calibration on source (Subscan 1)
        # --------------------------------------------------
        # Update metadata for this subscan to use in DATAPAR AND HOLODATA headers
        cdb.set(cdbConstants.SCANNUM, 5678)
        cdb.set(cdbConstants.SUBSCAN_NUMBER, 1)
        cdb.set(cdbConstants.DATE_OBS, Time.now().isot)
        cdb.set(cdbConstants.SUBSTYPE, "CORR")

        # Enable write data for the current subscan
        self.objref.enable_write("atfits",str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

        # Publish new scan metadata.  DataAggregator notification consumer subscribes to this data.
        # Use it to create next header
        self.suppliers["scan"]["supplier"].publish_event(metadata)
        time.sleep(DELAY)

        # Clear accumulators of HOLODATA and DATAPAR for each new SUBSCAN
        self.objref.clear_subscan_buffers()

        # Write subscan header in all open pipelines
        self.objref.write_before_subscan()


        for k in range(integrations_per_cal0):
            # Publish the new offset angles
            # self.logger.debug('send Antenna packet with sim offset angles ({}/{}), (az, el) : ({}, {}) [arcsec], ({}, {}) [deg]'.format(k, integrations_per_cal0, hsim.center_cal0['x'][0][k] * 3600, hsim.center_cal0['y'][0][k] * 3600, hsim.center_cal0['x'][0][k], hsim.center_cal0['y'][0][k]))
            eventAntenna.general_sts.actual_time = Time.now().mjd
            eventAntenna.tracking_sts.prog_offset_az = hsim.center_cal0["x"][0][k]
            eventAntenna.tracking_sts.prog_offset_el = hsim.center_cal0["y"][0][k]

            self.suppliers["antenna"]["supplier"].publish_event(eventAntenna)
            time.sleep(DELAY)

            # Publish the new holodata message
            msg_holodata["time_mjd"] = Time.now().mjd
            ref_magnitude = np.abs(hsim.center_cal0["ref_antenna_signal_watts"][0][k])
            tst_magnitude = np.abs(hsim.center_cal0["tst_antenna_signal_watts"][0][k])
            ref_phase = np.angle(hsim.center_cal0["ref_antenna_signal_watts"][0][k])
            tst_phase = np.angle(hsim.center_cal0["tst_antenna_signal_watts"][0][k])
            sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
            sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))
            # Define noise floor
            noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
            noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
            # Calculate FFT spectrum
            spectrum_ref_complex = np.fft.fft((sig_ref+noise_ref) * np.hamming(fft_length)) / fft_length
            spectrum_tst_complex = np.fft.fft((sig_tst+noise_tst) * np.hamming(fft_length)) / fft_length
            spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
            msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
            msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
            msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
            msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))
            marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
            
            msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
            msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
            msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
            msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
            msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]
            
            self.logger.debug(
                "subscan ({:03d}/{:03d}) CORR send message ({:03d}/{:03d})".format(
                    cdb.get(cdbConstants.SUBSCAN_NUMBER, int),
                    2 * npoints + 1,
                    k + 1,
                    integrations_per_cal0,
                )
            )
            self.suppliers["holodata"]["supplier"].publish_event(msg_holodata)
            time.sleep(DELAY)

        # Subscan is finished.  Write all the subscan data
        self.objref.write_after_subscan(str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

        # Disable write data for the current subscan
        self.objref.disable_write("atfits",str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

        for otf_map_row in hsim.otf_map:
            # Update metadata
            cdb.set(cdbConstants.SUBSCAN_NUMBER, cdb.get(cdbConstants.SUBSCAN_NUMBER, int) + 1)
            cdb.set(cdbConstants.DATE_OBS, Time.now().isot)
            cdb.set(cdbConstants.SUBSTYPE, "HOLO")

            # Enable write data for the current subscan
            self.objref.enable_write("atfits",str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

            # Publish new scan metadata.  DataAggregator notification consumer subscribes to this data.
            # Use it to create next header
            self.suppliers["scan"]["supplier"].publish_event(metadata)
            time.sleep(DELAY)

            # Clear accumulators of HOLODATA and DATAPAR for each new SUBSCAN
            self.objref.clear_subscan_buffers()

            # Write subscan header in all open pipelines
            self.objref.write_before_subscan()

            for k in range(npoints):
                eventAntenna.general_sts.actual_time = Time.now().mjd
                eventAntenna.tracking_sts.prog_offset_az = otf_map_row["x"][k]
                eventAntenna.tracking_sts.prog_offset_el = otf_map_row["y"][k]
                self.suppliers["antenna"]["supplier"].publish_event(eventAntenna)
                time.sleep(DELAY)

                # Publish the new holodata
                msg_holodata["time_mjd"] = Time.now().mjd

                ref_magnitude = np.abs(otf_map_row["ref_antenna_signal_watts"][k])
                tst_magnitude = np.abs(otf_map_row["tst_antenna_signal_watts"][k])
                ref_phase = np.angle(otf_map_row["ref_antenna_signal_watts"][k])
                tst_phase = np.angle(otf_map_row["tst_antenna_signal_watts"][k])
                
                sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
                sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))

                # Define noise floor
                noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))

                # Calculate FFT spectrum
                spectrum_ref_complex = np.fft.fft((sig_ref+noise_ref) * np.hamming(fft_length)) / fft_length
                spectrum_tst_complex = np.fft.fft((sig_tst+noise_tst) * np.hamming(fft_length)) / fft_length
                spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
                
                msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
                msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
                msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
                msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))

                marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
                msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
                msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
                msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
                msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
                msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]

                self.logger.debug(
                    "subscan ({:03d}/{:03d}) HOLO send message ({:03d}/{:03d})".format(
                        cdb.get(cdbConstants.SUBSCAN_NUMBER, int),
                        2 * npoints + 1,
                        k + 1,
                        npoints,
                    )
                )
                self.suppliers["holodata"]["supplier"].publish_event(msg_holodata)
                time.sleep(DELAY)

            # Subscan is finished.  Write all the subscan data
            self.objref.write_after_subscan(str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

            # Disable write data for the current subscan
            self.objref.disable_write("atfits",str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

            # Update metadata
            cdb.set(cdbConstants.SUBSCAN_NUMBER, cdb.get(cdbConstants.SUBSCAN_NUMBER, int) + 1)
            cdb.set(cdbConstants.DATE_OBS, Time.now().isot)
            cdb.set(cdbConstants.SUBSTYPE, "CORR")

            # Enable write data for the current subscan
            self.objref.enable_write("atfits",str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))

            # Publish new scan metadata.  DataAggregator notification consumer subscribes to this data.
            # Use it to create next header
            self.suppliers["scan"]["supplier"].publish_event(metadata)
            time.sleep(DELAY)

            # Clear accumulators of HOLODATA and DATAPAR for each new SUBSCAN
            self.objref.clear_subscan_buffers()

            # Write subscan header in all open pipelines
            self.objref.write_before_subscan()

            for k in range(integrations_per_calN):
                eventAntenna.general_sts.actual_time = Time.now().mjd
                eventAntenna.tracking_sts.prog_offset_az = hsim.center_calN["x"][0][k]
                eventAntenna.tracking_sts.prog_offset_el = hsim.center_calN["y"][0][k]
                self.suppliers["antenna"]["supplier"].publish_event(eventAntenna)
                time.sleep(DELAY)

                # Publish the new holodata
                msg_holodata["time_mjd"] = Time.now().mjd
                    
                ref_magnitude = np.abs(hsim.center_calN["ref_antenna_signal_watts"][0][k])
                tst_magnitude = np.abs(hsim.center_calN["tst_antenna_signal_watts"][0][k])
                ref_phase = np.angle(hsim.center_calN["ref_antenna_signal_watts"][0][k])
                tst_phase = np.angle(hsim.center_calN["tst_antenna_signal_watts"][0][k])

                sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
                sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))

                # Define noise floor
                noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))

                # Calculate FFT spectrum
                spectrum_ref_complex = np.fft.fft((sig_ref+noise_ref) * np.hamming(fft_length)) / fft_length
                spectrum_tst_complex = np.fft.fft((sig_tst+noise_tst) * np.hamming(fft_length)) / fft_length
                spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
                
                msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
                msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
                msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
                msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))
        
                marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
                msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
                msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
                msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
                msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
                msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]
                
                self.logger.debug(
                    "subscan ({:03d}/{:03d}) CORR send message ({:03d}/{:03d})".format(
                        cdb.get(cdbConstants.SUBSCAN_NUMBER, int),
                        2 * npoints + 1,
                        k + 1,
                        integrations_per_calN,
                    )
                )
                self.suppliers["holodata"]["supplier"].publish_event(msg_holodata)
                time.sleep(DELAY)

            # Subscan is finished.  Write all the subscan data
            self.objref.write_after_subscan(str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))
            
            # Disable write data for the current subscan
            self.objref.disable_write("atfits",str(cdb.get(cdbConstants.SUBSCAN_NUMBER, int)))


        # Clear accumulators of HOLODATA and DATAPAR for each new SUBSCAN
        self.objref.clear_subscan_buffers()
        time.sleep(DELAY)

        self.objref.write_monitor()

        # Close all
        filename = self.objref.close_pipeline("atfits")
        self.logger.debug("closed file {}".format(filename))

        # Preview file.  Block unittst until the user close the GUI window
        absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/view_atfits.py"
        cmd = ["python", absolute_path, filename]
        self.logger.debug("Launching process: {}".format(cmd))
        self.gui_proc = subprocess.Popen(cmd)
        returncode = self.gui_proc.wait()
        self.logger.debug("GUI process finished. return code: {}".format(returncode))

    # NOTE (SS. 12/2023) PipelineGildas is not in use.  More convenient to record
    # .mbfits files and convert to Gildas CLASS in post processing
    def test1530_PipelineGildas(self):
        pass
        #     # Simple test.  Send one event of each type.  View Logs to confirm
        #     # That all events are received by the DataAggregator.  If DataMonitor GUI is open,
        #     # look at update information in each section.
        #     DELAY = 0.05

        #     # Paramters to simulate data and generate spectrum arraydata_row objects.
        #     subscan_duration = 10
        #     integration_time = 0.2
        #     nsections = 1
        #     nchannels = 2048
        #     subs_num = '1'

        #     # Simulate header data
        #     (metadata, time_apy) = self.simulate_headers()

        #     # Publish new scan metadata.  DataAggregator notification consumer will catch this data and keep it in the local self.metadata
        #     self.suppliers["scan"]["supplier"].publish_event(metadata)
        #     time.sleep(DELAY)

        #     # Open data pipelines and test packet counters during the time that
        #     # write_enable is True and False

        #     # Do a test of pipeline with simulated data for a pointing scan.
        #     self.objref.open_pipeline("gildas")
        #     time.sleep(DELAY)

        #     # Enable writing realtime packets
        #     self.objref.enable_write("gildas", subs_num)

        #     # Clear accumulators of MONITOR-XXX for each new SCAN
        #     self.objref.clear_monitor_buffers()
        #     time.sleep(DELAY)

        #     # Write current scan header in all open pipelines
        #     self.objref.write_scan_header()
        #     time.sleep(DELAY)

        #     # Clear accumulators of ARRAYDATA and DATAPAR for each new SUBSCAN
        #     self.objref.clear_subscan_buffers()
        #     time.sleep(DELAY)

        #     # Write subscan header in all open pipelines
        #     self.objref.write_before_subscan()
        #     time.sleep(DELAY)

        #     t = threading.Timer(5.0, self.objref.disable_write, ['gildas', subs_num] )
        #     t.start()

        #     self.logger.debug("simulate spectrum data 1 subscan ...")
        #     self.objref.simulate_spectrum_arraydata_subscan(
        #         subscan_duration, integration_time, nsections, nchannels
        #     )
        #     # Note >>>> simulate_spectrum_arraydata_subscan blocks until simulated subscan is finished.

        #     # Subscan is finished.  Write the drift for pointing scan (1 spectrum channel, all angles)
        #     self.objref.write_after_subscan(subs_num)
        #     time.sleep(DELAY)
        #     self.objref.write_monitor()
        #     time.sleep(1.0)
        #     # Close all
        #     filename = self.objref.close_pipeline("gildas")
        #     self.logger.debug("closed file {}".format(filename))

    def test1540_PipelineSpectrumPreview(self):
        """
        EddFits packets should arrive at EddFitsClient form the socket.  However, this test bypasses
        the socket connection and generates packets of the same data structure in DataAggregator.
        Then, use these locally-generated packets to test PipelineMBfits with top-down control from
        DataAggregator.
        """
        DELAY = 0.05

        # Paramters to simulate data and generate spectrum arraydata_row objects.
        subscan_duration = 10
        integration_time = 0.2
        nsections = 2
        nchannels = 2048
        subs_num = '1'

        # Simulate header data
        (metadata, time_apy) = self.simulate_headers()

        # Publish new scan metadata.  DataAggregator notification consumer will catch this data and keep it in the local self.metadata
        self.suppliers["scan"]["supplier"].publish_event(metadata)
        time.sleep(DELAY)

        self.objref.open_pipeline("spectrum_preview")
        time.sleep(DELAY)

        # spectrum preview does not write a file, but we must test the
        # empty function that will be called by scan controller for the general case
        self.objref.enable_write("spectrum_preview", subs_num)

        # spectrum preview does not save monitor buffers, but we must test the
        # empty function that will be called by scan controller for the general case
        self.objref.clear_monitor_buffers()
        time.sleep(DELAY)

        # spectrum preview does not write a file, but we must test the
        # empty function that will be called by scan controller for the general case
        self.objref.write_scan_header()
        time.sleep(DELAY)

        # spectrum preview does not save spectrum in subscan buffer, but we must test the
        # empty function that will be called by scan controller for the general case
        self.objref.clear_subscan_buffers()
        time.sleep(DELAY)

        # spectrum preview does not write headers before subscan, but we must test the
        # empty function that will be called by scan controller for the general case
        self.objref.write_before_subscan()
        time.sleep(DELAY)

        t = threading.Timer(5.0, self.objref.disable_write, ['spectrum_preview', subs_num] )
        t.start()

        self.logger.debug("simulate spectrum data 1 subscan ...")
        self.objref.simulate_spectrum_arraydata_subscan(
            subscan_duration, integration_time, nsections, nchannels
        )
        # Note >>>> simulate_spectrum_arraydata_subscan blocks until simulated subscan is finished.

        # spectrum preview does not write spectrum buffer to file after subscan,
        # but we must test the  empty function that will be called by scan controller for the general case
        self.objref.write_after_subscan(subs_num)

        # spectrum preview does not write monitor buffer to file after subscan,
        # but we must test the  empty function that will be called by scan controller for the general case
        self.objref.write_monitor()

        # Close pipeline and get file name
        filename = self.objref.close_pipeline("spectrum_preview")
        self.logger.debug("closed file {}".format(filename))

    def test1550_Pipelines_All(self):
        # That all events are received by the DataAggregator.  If DataMonitor GUI is open,
        # look at update information in each section.
        DELAY = 0.05

        # Paramters to simulate data and generate spectrum arraydata_row objects.
        subscan_duration = 10
        integration_time = 0.2
        nsections = 2
        nchannels = 2048
        subs_num = '1'

        # Simulate header data
        (metadata, time_apy) = self.simulate_headers()

        # Publish new scan metadata.  DataAggregator notification consumer will catch this data and keep it in the local self.metadata
        self.suppliers["scan"]["supplier"].publish_event(metadata)
        time.sleep(DELAY)

        # Open data pipelines and test packet counters during the time that

        # Do a test of pipeline with simulated data for a pointing scan.
        self.objref.open_pipeline("mbfits")
        # self.objref.open_pipeline("gildas")
        self.objref.open_pipeline("atfits")
        self.objref.open_pipeline("spectrum_preview")
        time.sleep(DELAY)

        # Enable writing realtime packets
        self.objref.enable_write("mbfits", subs_num)
        # self.objref.enable_write("gildas", subs_num)
        self.objref.enable_write("atfits", subs_num)
        self.objref.enable_write("spectrum_preview", subs_num)

        # Clear accumulators of MONITOR-XXX for each new SCAN
        self.objref.clear_monitor_buffers()
        time.sleep(DELAY)

        # Write current scan header in all open pipelines
        self.objref.write_scan_header()
        time.sleep(DELAY)

        # Clear accumulators of ARRAYDATA and DATAPAR for each new SUBSCAN
        self.objref.clear_subscan_buffers()
        time.sleep(DELAY)

        # Write subscan header in all open pipelines
        self.objref.write_before_subscan()
        time.sleep(DELAY)

        t1 = threading.Timer(5.0, self.objref.disable_write, ["mbfits", subs_num] )
        #t2 = threading.Timer(5.0, self.objref.disable_write, ["gildas", subs_num] )
        t3 = threading.Timer(5.0, self.objref.disable_write, ["atfits", subs_num] )
        t4 = threading.Timer(5.0, self.objref.disable_write, ["spectrum_preview", subs_num] )
        t1.start()
        #t2.start()
        t3.start()
        t4.start()

        self.logger.debug("simulate spectrum data 1 subscan ...")
        self.objref.simulate_spectrum_arraydata_subscan(
            subscan_duration, integration_time, nsections, nchannels
        )
        # Note >>>> simulate_spectrum_arraydata_subscan blocks until simulated subscan is finished.

        # Subscan is finished.  Write all the subscan data
        self.objref.write_after_subscan(subs_num)
        time.sleep(DELAY)
        self.objref.write_monitor()
        time.sleep(1.0)
        # Close all
        filename = self.objref.close_pipeline("mbfits")
        self.logger.debug("closed file {}".format(filename))

        # filename = self.objref.close_pipeline("gildas")
        # self.logger.debug("closed file {}".format(filename))

        filename = self.objref.close_pipeline("atfits")
        self.logger.debug("closed file {}".format(filename))

        filename = self.objref.close_pipeline("spectrum_preview")
        self.logger.debug("closed file {}".format(filename))
