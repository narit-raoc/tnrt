# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
import os
import time
import subprocess

from unittest_common import AsyncMessagingTestCase


class EddFitsRealTestCase(AsyncMessagingTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test2000_EddFitsClient_registered_handlers_real_server(self):
        """
        Test that the EddFitsClient can receive packets from the real EDD fits_interface server
        handle the packets using registered handler functions from list of
        active pipelinesin DataAggregator
        """
        # That all events are received by the DataAggregator.  If DataMonitor GUI is open,
        # look at update information in each section.
        DELAY = 0.05
        subs_num ='1'

        capture_data_duration = 30

        # Simulate header data
        (metadata, time_apy) = self.simulate_headers()

        # Publish new scan metadata.  DataAggregator notification consumer will catch this data and keep it in the local self.metadata
        self.suppliers["scan"]["supplier"].publish_event(metadata)
        time.sleep(DELAY)

        self.objref.open_pipeline("mbfits")
        self.objref.open_pipeline("spectrum_preview")
        time.sleep(DELAY)

        # Clear accumulators of MONITOR-XXX for each new SCAN
        self.objref.clear_monitor_buffers()
        time.sleep(DELAY)

        # Write current scan header in all open pipelines
        self.objref.write_scan_header()
        time.sleep(DELAY)

        # Clear accumulators of ARRAYDATA and DATAPAR for each new SUBSCAN
        self.objref.clear_subscan_buffers()
        time.sleep(DELAY)

        # Write subscan header in all open pipelines
        self.objref.write_before_subscan()
        time.sleep(DELAY)

        # # Start the FitsClient to receive the packets and process them.
        self.logger.debug(
            "Start fits client and receive packets for {} seconds".format(capture_data_duration)
        )
        self.objref.fits_client_start()

        # Enable writing realtime packets
        self.objref.enable_write("mbfits", subs_num)
        self.objref.enable_write("spectrum_preview", subs_num)

        # EddFitsMockServer and EddFitsClient are running infinite loop.
        # Sleep this unittest thread and let the client and pipelines work.
        time.sleep(capture_data_duration)

        # disable writing realtime packets
        self.objref.disable_write("mbfits", subs_num)
        self.objref.disable_write("spectrum_preview", subs_num)

        # Subscan is finished.  Write all the subscan data
        self.objref.write_after_subscan(subs_num)
        time.sleep(DELAY)

         # Stop server, then client
        self.logger.debug("Stop fits client")
        self.objref.fits_client_stop()
        time.sleep(1)
        
        self.objref.write_monitor()
        time.sleep(1.0)
        # Close all
        filename_mbfits = self.objref.close_pipeline("mbfits")
        self.logger.debug("closed file {}".format(filename_mbfits))

        filename_preview = self.objref.close_pipeline("spectrum_preview")
        self.logger.debug("closed file {}".format(filename_preview))

        # Preview file.  Block unittest until the user close the GUI window
        absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/view_mbfits.py"
        cmd = ["python", absolute_path, filename_mbfits]
        self.logger.debug("Launching process: {}".format(cmd))
        self.gui_proc = subprocess.Popen(cmd)
        returncode = self.gui_proc.wait()
        self.logger.debug("GUI process finished. return code: {}".format(returncode))
