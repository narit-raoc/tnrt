import unittest
from unittest import mock
from DataArchive import DataArchive
from unittest.mock import patch, Mock, call
import DataAggregatorDefaults as config
import ScanDefaults


class TestDataArchive(unittest.TestCase):
    def setUp(self):
        self.data_archive = DataArchive()

    def tearDown(self):
        self.data_archive = None

    def test_connect_db(self):
        with patch("DataArchive.MongoClient") as mock_mongo_client:
            mock_mongo_client.return_value = {
                config.DB_NAME: {
                    config.DB_COLLECTION: "mock_collection_instance",
                },
            }
            self.data_archive.connect_db()
            mock_mongo_client.assert_called_once_with(
                config.DB_URL,
                username=config.DB_USER,
                password=config.DB_PASSWORD,
            )
            self.assertIn(config.DB_NAME, self.data_archive.db_client)
            self.assertIn(config.DB_COLLECTION, self.data_archive.db)
            self.assertEqual(self.data_archive.collection, "mock_collection_instance")

    @patch("DataArchive.datetime")
    def test_set_filename(self, mock_time):
        mock_filename = "test_file_name.mbfits"
        expected_file_path = "/TNRT_data/tnrt-storage-pool-TNRT_data-rsync/tnrt_data/"
        expected_file_url_path = "/tnro/tnrt-storage-pool-TNRT_data-rsync/tnrt_data/"
        expected_current_time = "now"
        mock_utcnow = Mock(return_value=expected_current_time)
        mock_time.utcnow = mock_utcnow

        self.data_archive.set_filename(mock_filename)

        self.assertEqual(self.data_archive.fitInfo.fileName, mock_filename)
        self.assertEqual(self.data_archive.fitInfo.filePath, expected_file_path + mock_filename)
        self.assertEqual(
            self.data_archive.fitInfo.fileUrlPath,
            expected_file_url_path + mock_filename,
        )
        mock_utcnow.assert_called()
        self.assertEqual(self.data_archive.fitInfo.fileCreateDate, expected_current_time)

    @patch("DataArchive.DataArchive.connect_db")
    def test_record_metadata(self, mock_connect):
        self.data_archive.observerId = "mock observerId"
        self.data_archive.observerName = "mock observerName"
        self.data_archive.operatorName = "mock operatorName"
        self.data_archive.location = "mock location"
        self.data_archive.proposalId = "mock proposalId"

        self.data_archive.fitInfo.filePath = "mock filePath"
        self.data_archive.fitInfo.fileUrlPath = "mock fileUrlPath"
        self.data_archive.fitInfo.fileName = "mock fileName"
        self.data_archive.fitInfo.fileCreateDate = "mock fileCreateDate"
        self.data_archive.fitInfo.observationDate = "mock observationDate"
        self.data_archive.fitInfo.fileType = "mock fileType"
        self.data_archive.fitInfo.scanMode = "mock scanMode"
        self.data_archive.fitInfo.telescopeName = "mock telescopeName"

        mock_insert_one = Mock()
        self.data_archive.collection = Mock()
        self.data_archive.collection.insert_one = mock_insert_one

        self.data_archive.record_metadata()
        mock_insert_one.assert_called_once_with(
            {
                "observerId": "mock observerId",
                "observerName": "mock observerName",
                "operatorName": "mock operatorName",
                "projectDuration": 1,
                "stationName": 13,
                "location": "mock location",
                "proposalId": "mock proposalId",
                "fitInfo": {
                    "filePath": "mock filePath",
                    "fileUrlPath": "mock fileUrlPath",
                    "fileName": "mock fileName",
                    "fileCreateDate": "mock fileCreateDate",
                    "observationDate": "mock observationDate",
                    "fileType": 1,
                    "scanMode": "mock scanMode",
                    "telescopeName": "mock telescopeName",
                    "receiverType": "NULL",
                    "observationMode": "NULL",
                    "backEnd": 'NULL',
                    "observation": "NONE"
                },
            }
        )

    def test_transform_header_to_archive_field(self):

        mock_header = ScanDefaults.metadata
        mock_header.FEBEPAR_MBFITS_HEADER.FEBE = "L-EDD-TNRT_dualpol_spectrometer"
        mock_header.SCAN_MBFITS_HEADER.SITELAT = 18.864348
        mock_header.SCAN_MBFITS_HEADER.SITELONG = 99.216805

        self.data_archive.transform_header_to_archive_field(mock_header)

        self.assertEqual(self.data_archive.fitInfo.receiverType, "L_BAND")
        self.assertEqual(self.data_archive.fitInfo.observationMode, "NULL")
        self.assertEqual(self.data_archive.fitInfo.observation, "SPECTROMETER")
        self.assertEqual(self.data_archive.fitInfo.backEnd, "EDD")

        self.assertEqual(
            self.data_archive.location,
            {
                "coordinates": [
                    mock_header.SCAN_MBFITS_HEADER.SITELONG, 
                    mock_header.SCAN_MBFITS_HEADER.SITELAT,
                ], 
                "type": "Point"
            },
        )

    def test_transform_header_to_archive_field_KeyError(self):

        mock_header = ScanDefaults.metadata
        mock_header.FEBEPAR_MBFITS_HEADER.FEBE = "L-UNDEFINED-TNRT_dualpol_spectrometer"
        mock_header.SCAN_MBFITS_HEADER.SITELAT = 18.864348
        mock_header.SCAN_MBFITS_HEADER.SITELONG = 99.216805

        self.data_archive.transform_header_to_archive_field(mock_header)

        self.assertEqual(self.data_archive.fitInfo.receiverType, "L_BAND")
        self.assertEqual(self.data_archive.fitInfo.observationMode, "NULL")
        self.assertEqual(self.data_archive.fitInfo.observation, "SPECTROMETER")
        self.assertEqual(self.data_archive.fitInfo.backEnd, "NULL")

        self.assertEqual(
            self.data_archive.location,
            {
                "coordinates": [
                    mock_header.SCAN_MBFITS_HEADER.SITELONG, 
                    mock_header.SCAN_MBFITS_HEADER.SITELAT,
                ], 
                "type": "Point"
            },
        )


if __name__ == "__main__":
    unittest.main()
