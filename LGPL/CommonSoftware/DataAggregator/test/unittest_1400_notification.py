# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

import time

import AntennaDefaults
import atmosphere
import numpy as np
from BackendDefaults import generate_dtype_msg_holodata
import tnrtAntennaMod
from Supplier import Supplier

from unittest_common import AsyncMessagingTestCase


class SupplierTestCase(AsyncMessagingTestCase):
    def setUp(self):
        super().setUp()
        self.dtype_msg_holodata = generate_dtype_msg_holodata(1024)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1400_supplier_notification(self):
        DELAY = 0.05

        self.suppliers['antenna'] = {
            "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
            "dtype": tnrtAntennaMod.AntennaStatusNotifyBlock,
            "supplier": None,
        }
        self.suppliers['antenna']["supplier"] = Supplier(self.suppliers['antenna']["channel"])

        eventAtmosphere = atmosphere.opacityNotifyBlock(0.0, 0.0, 0.0)
        # eventWeather = weatherComNet.weatherDataBlock(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) # wait for NARIT's weather station
        (eventScan, time_apy) = self.simulate_headers()

        self.logger.debug("publish message Atmosphere")
        self.suppliers["atmosphere"]["supplier"].publish_event(eventAtmosphere)
        time.sleep(DELAY)

        # wait for NARIT's weather station
        # self.suppliers['weather']['supplier'].publish_event(eventWeather)
        # time.sleep(DELAY)

        self.logger.debug("publish message Scan metadata")
        self.suppliers["scan"]["supplier"].publish_event(eventScan)
        time.sleep(DELAY)

        eventAntenna = AntennaDefaults.metadata
        self.logger.debug("publish message Antenna / ACU status")
        self.suppliers["antenna"]["supplier"].publish_event(eventAntenna)
        time.sleep(DELAY)

        msg_holodata = np.squeeze(np.zeros(1, dtype=self.dtype_msg_holodata))
        self.logger.debug("publish message {}".format(type(msg_holodata)))
        self.suppliers["holodata"]["supplier"].publish_event(msg_holodata)
