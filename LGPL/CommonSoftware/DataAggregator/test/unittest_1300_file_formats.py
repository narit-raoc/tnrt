# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

import os
import numpy as np
import datetime

from astropy.io import fits
from astropy import units as units
from astropy.time import Time
from astropy.coordinates import EarthLocation

import pyclassfiller
from pyclassfiller import code  # The various codes needed by the Class Data Format

from DataSimulator import PointingSimulator
from DataSimulator import HolographySimulator
from DataFormatMONITORfits import DataFormatMONITORfits
from DataFormatMBfits import DataFormatMBfits
from DataFormatALMATIfits import DataFormatALMATIfits

from unittest_common import DataAggregatorTestCase


class FileFormatTestCase(DataAggregatorTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1300_DataFormatMBfits(self):
        try:
            # Simulate header data
            (metadata, time_apy) = self.simulate_headers()

            data_dir = os.getenv("DATA_DIR") or os.getenv("HOME") + "/data"
            if not os.path.exists(data_dir):
                self.logger.debug("data directory %s does not exist.  Create now" % data_dir)
                os.mkdir(data_dir)

            filename_base = datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
            filename = data_dir + "/" + filename_base + ".mbfits"
            self.logger.debug("open. filename= " + filename)

            # ----------------------------
            # Create a new file with only primary metadata.
            # Convert the Python object with attributes into dictionary
            # and automatically fill the primary metadata.
            hdr = DataFormatMBfits.generate_header_primary(metadata.PRIMARY_MBFITS_HEADER)

            self.logger.debug("Create PrimaryHDU")
            hdu_primary = fits.PrimaryHDU(header=hdr)

            # Create a temporary HDU list for debugging
            hdul = fits.HDUList([hdu_primary])

            # Write file
            hdul.writeto(filename)

            # Close file
            hdul.close()

            del hdul

            # Simulate datacube, Fill MBFITS data arrays directly (in real system, DataAggregator
            # gets access to all of the data and prepares the data arrays in real-time).
            offaxis_arcsec = -100
            sim = PointingSimulator(
                offaxis=offaxis_arcsec,
                nusefeed=metadata.ARRAYDATA_MBFITS_HEADER['NUSEFEED'],
                nchan=metadata.ARRAYDATA_MBFITS_HEADER['CHANNELS'],
            )

            # Generate emtpy Numpy array where each column has keyword name from MBFITS Columns specification.
            # Binary data type of each item in the row is defined using numpy data types that astropy.io.fits
            # can understand and auto-translate to FITS data types.
            # Dimensions of each binary array come from current values of header

            # Open the same file again in append mode (fits mode 'update').
            # 'update' mode allows use of hdul.flush() to write incremental data to file after append / modify.
            hdul = fits.open(filename, mode="update")

            self.logger.debug("Add SCAN-MBFITS header and data")
            hdr = DataFormatMBfits.generate_header_scan()
            data = np.zeros(1, dtype=DataFormatMBfits.dtype_scan_row)
            # Write current data to this binary data array.
            data["FEBE"] = np.array(["L-EDD-TNRT_dualpol_spectrometer"])
            coldefs = DataFormatMBfits.generate_coldefs_scan(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add FEBEPAR-MBFITS header and data")
            hdr = DataFormatMBfits.generate_header_febepar(metadata.FEBEPAR_MBFITS_HEADER)
            dtype_febepar_row = DataFormatMBfits.generate_dtype_febepar_row(
                metadata.FEBEPAR_MBFITS_HEADER.FEBEFEED,
                metadata.FEBEPAR_MBFITS_HEADER.NUSEBAND,
            )
            data = np.zeros(1, dtype_febepar_row)
            # Write current data to this binary data array.
            data["TCAL"] = 99 * np.ones(
                (
                    metadata.FEBEPAR_MBFITS_HEADER.FEBEFEED,
                    metadata.FEBEPAR_MBFITS_HEADER.NUSEBAND,
                )
            )
            coldefs = DataFormatMBfits.generate_coldefs_febepar(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add ARRAYDATA-MBFITS header data")
            hdr = DataFormatMBfits.generate_header_arraydata(metadata.ARRAYDATA_MBFITS_HEADER)
            dtype_arraydata_row = DataFormatMBfits.generate_dtype_arraydata_row(
                metadata.ARRAYDATA_MBFITS_HEADER['NUSEFEED'],
                metadata.ARRAYDATA_MBFITS_HEADER['CHANNELS'],
            )
            integration_time = sim.integration_time
            integration_count = sim.data3D.shape[0]
            data = np.zeros(integration_count, dtype=dtype_arraydata_row)
            # Write current data to this binary data array.
            data["MJD"] = time_apy.mjd + integration_time * np.arange(integration_count)
            data["DATA"] = sim.data3D
            coldefs = DataFormatMBfits.generate_coldefs_arraydata(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add DATAPAR-MBFITS header and data")
            hdr = DataFormatMBfits.generate_header_datapar(metadata.DATAPAR_MBFITS_HEADER)
            data = np.zeros(integration_count, DataFormatMBfits.dtype_datapar_row)
            # Write current data to this binary data array.
            data["MJD"] = time_apy.mjd + integration_time * np.arange(integration_count)
            data["INTEGTIM"] = integration_time
            data["LONGOFF"] = sim.angles
            coldefs = DataFormatMBfits.generate_coldefs_datapar(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-PACKET header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_PacketHeader(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_PacketHeader_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_PacketHeader(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-GENERAL header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_GeneralStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_GeneralStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_GeneralStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-SYSTEM header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_SystemStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_SystemStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_SystemStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-VERTEX header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_VertexShutterStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_VertexShutterStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_VertexShutterStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-AXIS header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_AxisStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_AxisStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_AxisStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-MOTOR header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_MotorStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_MotorStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_MotorStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-TRACKING header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_TrackingStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_TrackingStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_TrackingStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-HXP-GEN header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_GeneralHexapodStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_GeneralHexapodStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_GeneralHexapodStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-HXP header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_HexapodStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_HexapodStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_HexapodStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-SPINDLE header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_SpindleStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_SpindleStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_SpindleStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-HXP-TRACK header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_HexapodTrackStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_HexapodTrackStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_HexapodTrackStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-ANT-TRACKING-OBJECT header")
            hdr = DataFormatMBfits.generate_header_monitor_antenna_TrackingObjectStatus(
                metadata.ARRAYDATA_MBFITS_HEADER
            )
            data = np.zeros(
                integration_count,
                DataFormatMONITORfits.dtype_monitor_antenna_TrackingObjectStatus_row,
            )
            coldefs = DataFormatMBfits.generate_coldefs_monitor_antenna_TrackingObjectStatus(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            self.logger.debug("Add MONITOR-WEATHER header")
            hdr = DataFormatMBfits.generate_header_monitor_weather(metadata.ARRAYDATA_MBFITS_HEADER)
            data = np.zeros(1, DataFormatMONITORfits.dtype_monitor_weather_row)
            # Write current data to this binary data array.
            coldefs = DataFormatMBfits.generate_coldefs_monitor_weather(data)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
            hdul.append(hdu)

            # hdul.info()
            hdul.flush()
            hdul.close()
        except Exception as e:
            self.fail(e)

    def test1310_DataFormatALMATIfits(self):
        try:
            # Simulate header data
            (metadata, time_apy) = self.simulate_headers()
            # Modify some items that are not the same as MBFITS defaults
            metadata.PRIMARY_MBFITS_HEADER.TELESCOP = "OAN-ARIESY"  # (SS. 08/2021). Yebes review has this hard-coded, so use their value for convenience.
            metadata.PRIMARY_MBFITS_HEADER.CREATOR = "test55_DataFormatALMATIfits"
            metadata.SCAN_MBFITS_HEADER.BLNGTYPE = "AZIM-SIN"
            metadata.SCAN_MBFITS_HEADER.BLATTYPE = "ELEV-SIN"
            metadata.DATAPAR_MBFITS_HEADER['NLNGTYPE'] = "AZIM-SIN"
            metadata.DATAPAR_MBFITS_HEADER['CTYPE1N'] = "AZIM-SIN"
            metadata.DATAPAR_MBFITS_HEADER['NLATTYPE'] = "ELEV-SIN"
            metadata.DATAPAR_MBFITS_HEADER['CTYPE2N'] = "ELEV-SIN"
            metadata.DATAPAR_MBFITS_HEADER['SCANDIR'] = "ALON"

            data_dir = os.getenv("DATA_DIR") or os.getenv("HOME") + "/data"
            if not os.path.exists(data_dir):
                self.logger.debug("data directory %s does not exist.  Create now" % data_dir)
                os.mkdir(data_dir)

            filename_base = datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
            filename = data_dir + "/" + filename_base + ".atfits"
            self.logger.debug("open. filename= " + filename)

            # ----------------------------
            # Create a new file with only primary metadata.
            # Convert the Python object with attributes into dictionary
            # and automatically fill the primary metadata.
            hdr = DataFormatALMATIfits.generate_header_primary(
                metadata.PRIMARY_MBFITS_HEADER, metadata.SCAN_MBFITS_HEADER
            )

            self.logger.debug("Create PrimaryHDU")
            hdu_primary = fits.PrimaryHDU(header=hdr)

            # Create a temporary HDU list for debugging
            hdul = fits.HDUList([hdu_primary])

            # Write file
            hdul.writeto(filename)

            # Close file
            hdul.close()

            del hdul

            # Simulate datacube, Fill MBFITS data arrays directly (in real system, DataAggregator
            # gets access to all of the data and prepares the data arrays in real-time).
            npoints = 128
            angular_resolution = 100
            time_per_angular_step = 0.5  # should be same as receiver integration time
            subscans_per_cal = 1
            integrations_per_calN = 10
            integrations_per_cal0 = 120
            peak_magntidue_ref = (
                8.5e-6  # mean value of first calibration in Yebes file TEST5772.FITS
            )
            peak_magntidue_tst = (
                5.8e-5  # mean value of first calibration in Yebes file TEST5772.FITS
            )
            fwhm = 160

            # Set integration time equal to angular step time.  Important if we want receiver data to
            # align with OTF map grid points.
            integration_time = time_per_angular_step
            integrations_per_line = npoints

            hsim = HolographySimulator(
                npoints,
                angular_resolution,
                time_per_angular_step,
                subscans_per_cal,
                integrations_per_calN,
                integrations_per_cal0,
                peak_magntidue_ref,
                peak_magntidue_tst,
                fwhm,
            )

            # Generate emtpy Numpy array where each column has keyword name from ALMATI FITS Columns specification.
            # Scan.idl uses the naming conventions from MBFITS but includes most of the same header data
            # from ALMATI FITS format, but in different sections.  So, this code looks unusual using
            # MBFITS keywords, but it's not a mistake.  The data we need in ALMATI is exists in the headers.
            # Binary data type of each item in the row is defined using numpy data types that astropy.io.fits
            # can understand and auto-translate to FITS data types.
            # Dimensions of each binary array come from current values of header

            # Open the same file again in append mode (fits mode 'update').
            # 'update' mode allows use of hdul.flush() to write incremental data to file after append / modify.
            hdul = fits.open(filename, mode="update")

            # --------------------------------------------------
            # Append the first calibration on source (Subscan 1)
            # --------------------------------------------------
            # Update metadata for this subscan to use in DATAPAR AND HOLODATA headers
            metadata.DATAPAR_MBFITS_HEADER['SCANNUM'] = 5678
            metadata.DATAPAR_MBFITS_HEADER['SUBSNUM'] = 1
            metadata.DATAPAR_MBFITS_HEADER['SUBSTYPE'] = "CORR"

            # DATAPAR
            self.logger.debug(
                "Add DATAPAR-ALMATI\t\tcal0\tsubscan {0:03d}".format(
                    metadata.DATAPAR_MBFITS_HEADER['SUBSNUM']
                )
            )
            hdr_datapar = DataFormatALMATIfits.generate_header_datapar(
                metadata.PRIMARY_MBFITS_HEADER,
                metadata.SCAN_MBFITS_HEADER,
                metadata.DATAPAR_MBFITS_HEADER,
            )
            data_datapar = np.zeros(integrations_per_cal0, DataFormatALMATIfits.dtype_datapar_row)

            # Write current data to DATAPAR numpy structured arrays header (header data that is not already in the MBFITS-style headers)
            hdr_datapar["EXPOSURE"] = (
                integration_time * integrations_per_cal0
            )  # (SS 05/2021) Doesn't agree with OAN Yebes format (incorrectly swaps 'EXPOSURE' and 'INTEGTIM')
            hdr_datapar["AZIMUTH"] = 237.590165
            hdr_datapar["ELEVATIO"] = 52.366062
            hdr_datapar["AZIM-FIX"] = 237.590165
            hdr_datapar["ELEV-FIX"] = 52.366062

            # Write simulated data to numpy structured arrays for this subscan
            data_datapar["INTEGNUM"] = np.arange(
                1, integrations_per_cal0 + 1
            )  # arange doesn't include last point by default, so +1
            data_datapar[
                "INTEGTIM"
            ] = integration_time  # (SS 05/2021) Doesn't agree with OAN Yebes format (incorrectly swaps 'EXPOSURE' and 'INTEGTIM')
            data_datapar["MJD"] = metadata.SCAN_MBFITS_HEADER.MJD + (
                integration_time / 86400
            ) * np.arange(1, integrations_per_cal0 + 1)
            data_datapar["AZELERR"] = 1e-6  # broadcast scalar value across entire array
            data_datapar["LATOFF"] = hsim.center_cal0["y"]
            data_datapar["LONGOFF"] = hsim.center_cal0["x"]
            data_datapar["WINDSPEE"] = 1.234  # broadcast
            data_datapar["WINDDIRE"] = 90
            data_datapar["FLAG"] = 1
            data_datapar["ISWITCH"] = 1
            data_datapar["HOLO"] = True

            # Generate the FITS objects from numpy structured arrays and append the file
            coldefs = DataFormatALMATIfits.generate_coldefs_datapar(data_datapar)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr_datapar)
            hdul.append(hdu)

            # HOLODATA
            self.logger.debug(
                "Add HOLODATA-ALMATI\tcal0\tsubscan {0:03d}".format(
                    metadata.DATAPAR_MBFITS_HEADER['SUBSNUM']
                )
            )
            hdr_holodata = DataFormatALMATIfits.generate_header_holodata(
                metadata.SCAN_MBFITS_HEADER, metadata.DATAPAR_MBFITS_HEADER
            )
            data_holodata = np.zeros(
                integrations_per_cal0, dtype=DataFormatALMATIfits.dtype_holodata_row
            )

            # Write current data to DATAPAR numpy structured arrays header (header data that is not already in the MBFITS-style headers)
            hdr_holodata["TRANDIST"] = 36941635.90396384
            hdr_holodata["TRANFREQ"] = 11.699e9

            # Write simulated data to numpy structured arrays for this subscan
            data_holodata["INTEGNUM"] = np.arange(
                1, integrations_per_cal0 + 1
            )  # arange doesn't include last point by default, so +1
            data_holodata["HOLOSS"] = np.abs(hsim.center_cal0["tst_antenna_signal_watts"]) / 8
            data_holodata["HOLORR"] = np.abs(hsim.center_cal0["ref_antenna_signal_watts"]) / 8
            data_holodata["HOLOQQ"] = np.abs(hsim.center_cal0["tst_antenna_signal_watts"]) / 8
            data_holodata["HOLOSR"] = (
                np.sqrt(
                    np.abs(
                        hsim.center_cal0["tst_antenna_signal_watts"]
                        * hsim.center_cal0["ref_antenna_signal_watts"]
                    )
                )
                * np.cos(
                    np.angle(
                        hsim.center_cal0["tst_antenna_signal_watts"]
                        / hsim.center_cal0["ref_antenna_signal_watts"]
                    )
                )
                / 8
            )
            data_holodata["HOLOSQ"] = 0  # broadcast
            data_holodata["HOLOQR"] = (
                -1
                * np.sqrt(
                    np.abs(
                        hsim.center_cal0["tst_antenna_signal_watts"]
                        * hsim.center_cal0["ref_antenna_signal_watts"]
                    )
                )
                * np.sin(
                    np.angle(
                        hsim.center_cal0["tst_antenna_signal_watts"]
                        / hsim.center_cal0["ref_antenna_signal_watts"]
                    )
                )
                / 8
            )
            data_holodata["AMPREF"] = np.sqrt(
                50 * np.abs(hsim.center_cal0["ref_antenna_signal_watts"])
            )
            data_holodata["AMPTST"] = np.sqrt(
                50 * np.abs(hsim.center_cal0["tst_antenna_signal_watts"])
            )
            data_holodata["AMPREL"] = np.abs(
                hsim.center_cal0["tst_antenna_signal_watts"]
                / hsim.center_cal0["ref_antenna_signal_watts"]
            )
            data_holodata["PHAREL"] = np.degrees(
                np.angle(
                    hsim.center_cal0["tst_antenna_signal_watts"]
                    / hsim.center_cal0["ref_antenna_signal_watts"]
                )
            )
            data_holodata["FREQMX"] = 20e3  # broadcast

            # Generate the FITS objects from numpy structured arrays and append the file
            coldefs = DataFormatALMATIfits.generate_coldefs_holodata(data_holodata)
            hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr_holodata)
            hdul.append(hdu)

            # --------------------------------------------------
            # Iterate through all rows of simulation data numpy arrays.  Append DATAPAR and HOLODATA
            # FITS HDUs for the antenna pattern scan line and on-source calibration.
            # --------------------------------------------------
            for otf_map_row in hsim.otf_map:
                # Update metadata
                metadata.DATAPAR_MBFITS_HEADER['SUBSNUM'] = metadata.DATAPAR_MBFITS_HEADER['SUBSNUM'] + 1
                metadata.DATAPAR_MBFITS_HEADER['SUBSTYPE'] = "HOLO"

                # DATAPAR
                self.logger.debug(
                    "Add DATAPAR-ALMATI\t\tline\tsubscan {0:03d}".format(
                        metadata.DATAPAR_MBFITS_HEADER['SUBSNUM']
                    )
                )
                hdr_datapar = DataFormatALMATIfits.generate_header_datapar(
                    metadata.PRIMARY_MBFITS_HEADER,
                    metadata.SCAN_MBFITS_HEADER,
                    metadata.DATAPAR_MBFITS_HEADER,
                )
                data_datapar = np.zeros(
                    integrations_per_line, DataFormatALMATIfits.dtype_datapar_row
                )

                # Write current data to DATAPAR numpy structured arrays header (header data that is not already in the MBFITS-style headers)
                hdr_datapar["EXPOSURE"] = (
                    integration_time * integrations_per_line
                )  # (SS 05/2021) Doesn't agree with OAN Yebes format (incorrectly swaps 'EXPOSURE' and 'INTEGTIM')
                hdr_datapar["AZIMUTH"] = 237.590165
                hdr_datapar["ELEVATIO"] = 52.366062
                hdr_datapar["AZIM-FIX"] = 237.590165
                hdr_datapar["ELEV-FIX"] = 52.366062

                # Write simulated data to numpy structured arrays for this subscan
                data_datapar["INTEGNUM"] = np.arange(
                    1, integrations_per_line + 1
                )  # arange doesn't include last point by default, so +1
                data_datapar["INTEGTIM"] = integration_time
                data_datapar["MJD"] = metadata.SCAN_MBFITS_HEADER.MJD + (
                    integration_time / 86400
                ) * np.arange(1, integrations_per_line + 1)
                data_datapar["AZELERR"] = 1e-6  # broadcast scalar value across entire array
                data_datapar["LATOFF"] = otf_map_row["y"]
                data_datapar["LONGOFF"] = otf_map_row["x"]
                data_datapar["WINDSPEE"] = 1.234  # broadcast
                data_datapar["WINDDIRE"] = 90
                data_datapar["FLAG"] = 1
                data_datapar["ISWITCH"] = 1
                data_datapar["HOLO"] = True

                # Generate the FITS objects from numpy structured arrays and append the file
                coldefs = DataFormatALMATIfits.generate_coldefs_datapar(data_datapar)
                hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr_datapar)
                hdul.append(hdu)

                # HOLODATA
                self.logger.debug(
                    "Add HOLODATA-ALMATI\tline\tsubscan {0:03d}".format(
                        metadata.DATAPAR_MBFITS_HEADER['SUBSNUM']
                    )
                )
                hdr_holodata = DataFormatALMATIfits.generate_header_holodata(
                    metadata.SCAN_MBFITS_HEADER, metadata.DATAPAR_MBFITS_HEADER
                )
                data_holodata = np.zeros(
                    integrations_per_line, dtype=DataFormatALMATIfits.dtype_holodata_row
                )

                # Write current data to DATAPAR numpy structured arrays header (header data that is not already in the MBFITS-style headers)
                hdr_holodata["TRANDIST"] = 36941635.90396384
                hdr_holodata["TRANFREQ"] = 11.699e9

                # Write simulated data to numpy structured arrays for this subscan
                data_holodata["INTEGNUM"] = np.arange(
                    1, integrations_per_line + 1
                )  # arange doesn't include last point by default, so +1
                data_holodata["HOLOSS"] = np.abs(otf_map_row["tst_antenna_signal_watts"]) / 8
                data_holodata["HOLORR"] = np.abs(otf_map_row["ref_antenna_signal_watts"]) / 8
                data_holodata["HOLOQQ"] = np.abs(otf_map_row["tst_antenna_signal_watts"]) / 8
                data_holodata["HOLOSR"] = (
                    np.sqrt(
                        np.abs(
                            otf_map_row["tst_antenna_signal_watts"]
                            * otf_map_row["ref_antenna_signal_watts"]
                        )
                    )
                    * np.cos(
                        np.angle(
                            otf_map_row["tst_antenna_signal_watts"]
                            / otf_map_row["ref_antenna_signal_watts"]
                        )
                    )
                    / 8
                )
                data_holodata["HOLOSQ"] = 0  # broadcast
                data_holodata["HOLOQR"] = (
                    -1
                    * np.sqrt(
                        np.abs(
                            otf_map_row["tst_antenna_signal_watts"]
                            * otf_map_row["ref_antenna_signal_watts"]
                        )
                    )
                    * np.sin(
                        np.angle(
                            otf_map_row["tst_antenna_signal_watts"]
                            / otf_map_row["ref_antenna_signal_watts"]
                        )
                    )
                    / 8
                )
                data_holodata["AMPREF"] = np.sqrt(
                    50 * np.abs(otf_map_row["ref_antenna_signal_watts"])
                )
                data_holodata["AMPTST"] = np.sqrt(
                    50 * np.abs(otf_map_row["tst_antenna_signal_watts"])
                )
                data_holodata["AMPREL"] = np.abs(
                    otf_map_row["tst_antenna_signal_watts"]
                    / otf_map_row["ref_antenna_signal_watts"]
                )
                data_holodata["PHAREL"] = np.degrees(
                    np.angle(
                        otf_map_row["tst_antenna_signal_watts"]
                        / otf_map_row["ref_antenna_signal_watts"]
                    )
                )
                data_holodata["FREQMX"] = 20e3  # broadcast

                # Generate the FITS objects from numpy structured arrays and append the file
                coldefs = DataFormatALMATIfits.generate_coldefs_holodata(data_holodata)
                hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr_holodata)
                hdul.append(hdu)

                # Update metadata
                metadata.DATAPAR_MBFITS_HEADER['SUBSNUM'] = metadata.DATAPAR_MBFITS_HEADER['SUBSNUM'] + 1
                metadata.DATAPAR_MBFITS_HEADER['SUBSTYPE'] = "CORR"

                # DATAPAR
                self.logger.debug(
                    "Add DATAPAR-ALMATI\t\tcal\tsubscan {0:03d}".format(
                        metadata.DATAPAR_MBFITS_HEADER['SUBSNUM']
                    )
                )
                hdr_datapar = DataFormatALMATIfits.generate_header_datapar(
                    metadata.PRIMARY_MBFITS_HEADER,
                    metadata.SCAN_MBFITS_HEADER,
                    metadata.DATAPAR_MBFITS_HEADER,
                )
                data_datapar = np.zeros(
                    integrations_per_calN, DataFormatALMATIfits.dtype_datapar_row
                )

                # Write current data to DATAPAR numpy structured arrays header (header data that is not already in the MBFITS-style headers)
                hdr_datapar["EXPOSURE"] = (
                    integration_time * integrations_per_calN
                )  # (SS 05/2021) Doesn't agree with OAN Yebes format (incorrectly swaps 'EXPOSURE' and 'INTEGTIM')
                hdr_datapar["AZIMUTH"] = 237.590165
                hdr_datapar["ELEVATIO"] = 52.366062
                hdr_datapar["AZIM-FIX"] = 237.590165
                hdr_datapar["ELEV-FIX"] = 52.366062

                # Write simulated data to numpy structured arrays for this subscan
                data_datapar["INTEGNUM"] = np.arange(
                    1, integrations_per_calN + 1
                )  # arange doesn't include last point by default, so +1
                data_datapar["INTEGTIM"] = integration_time
                data_datapar["MJD"] = metadata.SCAN_MBFITS_HEADER.MJD + (
                    integration_time / 86400
                ) * np.arange(1, integrations_per_calN + 1)
                data_datapar["AZELERR"] = 1e-6  # broadcast scalar value across entire array
                data_datapar["LATOFF"] = hsim.center_calN["y"]
                data_datapar["LONGOFF"] = hsim.center_calN["x"]
                data_datapar["WINDSPEE"] = 1.234  # broadcast
                data_datapar["WINDDIRE"] = 90
                data_datapar["FLAG"] = 1
                data_datapar["ISWITCH"] = 1
                data_datapar["HOLO"] = True

                # Generate the FITS objects from numpy structured arrays and append the file
                coldefs = DataFormatALMATIfits.generate_coldefs_datapar(data_datapar)
                hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr_datapar)
                hdul.append(hdu)

                # HOLODATA
                self.logger.debug(
                    "Add HOLODATA-ALMATI\tcal\tsubscan {0:03d}".format(
                        metadata.DATAPAR_MBFITS_HEADER['SUBSNUM']
                    )
                )
                hdr_holodata = DataFormatALMATIfits.generate_header_holodata(
                    metadata.SCAN_MBFITS_HEADER, metadata.DATAPAR_MBFITS_HEADER
                )
                data_holodata = np.zeros(
                    integrations_per_calN, dtype=DataFormatALMATIfits.dtype_holodata_row
                )

                # Write current data to DATAPAR numpy structured arrays header (header data that is not already in the MBFITS-style headers)
                hdr_holodata["TRANDIST"] = 36941635.90396384
                hdr_holodata["TRANFREQ"] = 11.699e9

                # Write simulated data to numpy structured arrays for this subscan
                data_holodata["INTEGNUM"] = np.arange(
                    1, integrations_per_calN + 1
                )  # arange doesn't include last point by default, so +1
                data_holodata["HOLOSS"] = np.abs(hsim.center_calN["tst_antenna_signal_watts"]) / 8
                data_holodata["HOLORR"] = np.abs(hsim.center_calN["ref_antenna_signal_watts"]) / 8
                data_holodata["HOLOQQ"] = np.abs(hsim.center_calN["tst_antenna_signal_watts"]) / 8
                data_holodata["HOLOSR"] = (
                    np.sqrt(
                        np.abs(
                            hsim.center_calN["tst_antenna_signal_watts"]
                            * hsim.center_calN["ref_antenna_signal_watts"]
                        )
                    )
                    * np.cos(
                        np.angle(
                            hsim.center_calN["tst_antenna_signal_watts"]
                            / hsim.center_calN["ref_antenna_signal_watts"]
                        )
                    )
                    / 8
                )
                data_holodata["HOLOSQ"] = 0  # broadcast
                data_holodata["HOLOQR"] = (
                    -1
                    * np.sqrt(
                        np.abs(
                            hsim.center_calN["tst_antenna_signal_watts"]
                            * hsim.center_calN["ref_antenna_signal_watts"]
                        )
                    )
                    * np.sin(
                        np.angle(
                            hsim.center_calN["tst_antenna_signal_watts"]
                            / hsim.center_calN["ref_antenna_signal_watts"]
                        )
                    )
                    / 8
                )
                data_holodata["AMPREF"] = np.sqrt(
                    50 * np.abs(hsim.center_calN["ref_antenna_signal_watts"])
                )
                data_holodata["AMPTST"] = np.sqrt(
                    50 * np.abs(hsim.center_calN["tst_antenna_signal_watts"])
                )
                data_holodata["AMPREL"] = np.abs(
                    hsim.center_calN["tst_antenna_signal_watts"]
                    / hsim.center_calN["ref_antenna_signal_watts"]
                )
                data_holodata["PHAREL"] = np.degrees(
                    np.angle(
                        hsim.center_calN["tst_antenna_signal_watts"]
                        / hsim.center_calN["ref_antenna_signal_watts"]
                    )
                )
                data_holodata["FREQMX"] = 20e3  # broadcast

                # Generate the FITS objects from numpy structured arrays and append the file
                coldefs = DataFormatALMATIfits.generate_coldefs_holodata(data_holodata)
                hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr_holodata)
                hdul.append(hdu)

            # hdul.info()
            hdul.flush()
            hdul.close()
        except Exception as e:
            self.fail(e)

    def test1320_DataFormatGildas(self):
        try:
            longitude = 99.216805
            latitude = 18.864348
            height = 403.625

            site_location = EarthLocation(
                EarthLocation.from_geodetic(
                    lon=longitude * units.deg,
                    lat=latitude * units.deg,
                    height=height * units.m,
                    ellipsoid="WGS84",
                )
            )

            # ------------------------------------------------
            # Simulate data to look like gaussian antenna beam.
            # ------------------------------------------------
            # Full-width, half-max antenna befam width [arcsec]
            amplitude = 1
            fwhm = 160
            offaxis = -200

            # Design the minimum scan parameters
            subscan_duration = 60.0
            integration_time = 1.0

            # Number of channels in the spectrum
            nchan = 2048

            # Number of position data points to generate per subscan.
            npoin = int(subscan_duration / integration_time)

            # Select an integration index (from  0 to npoin-1) to slice the datacube for spectrum write
            # Select an spectral channel (from 0 to nchan-1) to slice the datacube for drift / pointing
            index_integration = 22
            index_channel = int(nchan / 2 + 10)

            # Reference point for offset calculation
            if npoin % 2 == 0:
                rpoin = npoin / 2 + 0.5
            else:
                rpoin = npoin / 2

            # Offset to scan 10 * FWHM
            lim = (-10 * fwhm / 2, 10 * fwhm / 2)

            # List of angles go generate simulation data
            angles = np.linspace(lim[0], lim[1], npoin)

            # Calculate angular resolution of scan
            ares = (np.pi / 180) * (np.mean(np.diff(angles)) / 3600)

            # -----------------------------------------------------------------------------
            # Simulate a spectrum that has 1 signal
            # -----------------------------------------------------------------------------
            # Amplitude of signal
            S = 1

            # Amplitude of noise to add (set signal to noise ratio)
            N = S * 0.1

            # Choose number of samples (same for FFT size)
            # Remember to use .0 to force floating point if using Python2
            n_samples = nchan
            fs = 800e6
            freq = 10 * fs / nchan
            tt = np.arange(0, n_samples) / fs

            # Generate a complex-valued cosine signal (spectral line)
            noise_spectrum = N * (
                np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0])
            )
            sig = S * np.exp(1j * (2 * np.pi * freq * tt))
            sig_c = sig + noise_spectrum

            # Calculate FFT Spectcrum
            spectrum_c = np.fft.fft(sig_c * np.hamming(nchan)) / n_samples
            spectrum = np.abs(np.fft.fftshift(spectrum_c))

            # Use equation 3 from Nikom's report on telescope performance evaluation
            # Axes of this cube are [index_integration][index_spectral_channel]
            noise_drift = 0.01 * np.random.normal(size=angles.shape[0])
            gaussian = (
                amplitude * np.exp((-4 * np.log(2) * (angles - offaxis) ** 2) / (fwhm**2))
                + noise_drift
            )

            datacube = np.outer(gaussian, spectrum)

            # Copy / Paste data types used in the real DataAggregator
            dtype_arraydata_row = np.dtype([("MJD", np.float64), ("DATA", np.float64, (nchan,))])

            # Create an array of zeros - to be replaced by actual arraydata (receiver voltage/ power) values.
            # each row is data type defined by dtype_datapar_row
            # Number of rows is the number of integrations in this subscan.
            arraydata = np.zeros(npoin, dtype_arraydata_row)

            # Fill timestamps of arraydata.
            arraydata["MJD"] = Time(
                Time.now(), location=site_location
            ).mjd + integration_time * np.arange(npoin)

            time_apy = Time(arraydata["MJD"], format="mjd", location=site_location)
            time_ut = 2 * np.pi * ((time_apy.ut1.mjd) % 1)  # fractional days  * 2pi
            time_st = time_apy.sidereal_time("apparent").radian

            # Fill channel zero data with power calculations from   Notice the dimensions.
            # Since the last row dimension is the spectral channel, we have to fill down the colum
            # with power measurements vs time at this one channel.  Even though we only have 1 channel,
            # we must keep this singleton dimension in the matrix to be compatible with multi-channel
            # data array that can use the same code.
            arraydata["DATA"] = datacube

            # Create an array of zeros - to be replaced by actual datapar values.
            # each row is data type defined by dtype_datapar_row
            # Number of rows is the number of integrations in this subscan.
            datapar = np.zeros(npoin, dtype=DataFormatMBfits.dtype_datapar_row)

            # Use same timestamps for datapar
            datapar["MJD"] = arraydata["MJD"]

            # Assume same integration time for each data point
            datapar["INTEGTIM"] = integration_time * np.ones(npoin)

            # Since we simulate a longitude drift, write the simulated angles to the correct place in DATAPAR
            datapar["LONGOFF"] = angles

            # ------------------------------------------------
            # Prepare file
            # ------------------------------------------------
            data_dir = os.getenv("DATA_DIR") or os.getenv("HOME") + "/data"
            if not os.path.exists(data_dir):
                self.logger.debug("data directory %s does not exist.  Create now" % data_dir)
                os.mkdir(data_dir)

            filename_base = datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
            filename = data_dir + "/" + filename_base + ".40m"
            self.logger.debug("open. filename= " + filename)

            # Allow appending file if file exists
            allow_file_exists = True

            # If file exists, overwrite.
            overwrite_mode = True

            # Maximum number of scans in the file (obsolete)
            max_scans = 999999

            # enforce each scan to be unique in the file
            enforce_unique_scans = True

            fileout = pyclassfiller.ClassFileOut()
            fileout.open(
                file=filename,
                new=allow_file_exists,
                over=overwrite_mode,
                size=max_scans,
                single=enforce_unique_scans,
            )

            # -----------------------
            # Enable sections of file
            # -----------------------
            obs = pyclassfiller.ClassObservation()
            obs.head.presec[:] = False  # Disable all sections except...
            obs.head.presec[code.sec.gen] = True  # General
            obs.head.presec[code.sec.pos] = True  # Position
            obs.head.presec[code.sec.spe] = True  # Spectroscopy
            obs.head.presec[code.sec.dri] = True  # Continuum drifts
            obs.head.presec[code.sec.cal] = True  # Calibration

            # --------------------------------------------------------------------
            # Iterate through simulated data cube and write to Gildas CLASS format
            # --------------------------------------------------------------------
            # For efficiency, not all of these parameters in the gildas struct need to be updated
            # at the inntermost level of the loop, but leave it that way for now to keep the code simple
            # and straightforward.

            # Gildas CLASS developer manual shows that the time origin is MJD-60549 for dObs
            # Why 60549?
            GAG_MJD_OFFSET = 60549

            self.logger.debug("Writing data to filename: %s" % filename)
            self.logger.debug("integrations / drift angles: %d" % arraydata.shape[0])

            # ------------------
            # General Parameters
            # ------------------
            # Observation number [int32][no unit]
            obs.head.gen.num = int(0)

            # Version number [int32][no unit]
            obs.head.gen.ver = int(0)

            # ------------------
            # Scan header
            # ------------------
            # Telescope name. char[12]
            # Telescope name + Frontend-Backend configuration of telescope.
            # compare to MBFITS FEBE
            obs.head.gen.teles = str("TNRT40-POWR")

            # Integer date of observation [int32], [MJD - 60549].
            obs.head.gen.dobs = int(datapar["MJD"][index_integration]) - GAG_MJD_OFFSET

            # Integer date of reduction [int32] [MJD - 60549].
            obs.head.gen.dred = int(datapar["MJD"][index_integration]) - GAG_MJD_OFFSET

            # Type of data [code]
            # If head.gen.kind = spec (spectrum), header section obs.head.spe must be filled
            # If head.gen.kind = cont (continuum), header section obs.head.dri must be filled
            obs.head.gen.kind = code.kind.spec

            # Quality of data [code]  Ignore
            obs.head.gen.qual = code.qual.unknown

            # Scan number [int32][no unit]
            obs.head.gen.scan = int(1)

            # Subscan number [int32][no unit]
            obs.head.gen.subscan = int(1)

            # UT of observation [float64][unit = radian]
            obs.head.gen.ut = time_ut[index_integration]

            # LST of observation [float64][unit = radian]
            obs.head.gen.st = time_st[index_integration]

            # Opacity [float32][unit = neper]
            obs.head.gen.tau = 0.0

            # System temperature [float32][unit = Kelvin]
            obs.head.gen.tsys = 0.0

            # Integration Time [float32][unit = second]
            obs.head.gen.time = integration_time

            # X Unit. [code]
            # (If X coodinates section is present) {.velo, .freq, .wave}
            obs.head.gen.xunit = code.xunit.freq  # Unused (in pyclassfiller/__init__.py)

            # --------------------
            # Position Information
            # --------------------
            # Source name char[12]
            obs.head.pos.sourc = "INTELSAT22"

            # Coordinate System [code] {.unk, .equ, .gal, .hor}
            obs.head.pos.system = code.coord.hor

            # Equinox of coordinates [float32][unit = year]
            obs.head.pos.equinox = 2000.0

            # Projection system [code]
            obs.head.pos.proj = code.proj.none

            # Lambda : Longitude of source [float64][unit = radian]
            obs.head.pos.lam = 237.0 * np.pi / 180

            # Beta : Latitude of source [float64][unit = radian]
            obs.head.pos.bet = 52.0 * np.pi / 180

            # Projection angle [float64][unit = radian]
            obs.head.pos.projang = 0.0

            # Offset in longitude [float32][unit = radian]
            obs.head.pos.lamof = angles[index_integration] * np.pi / 180 / 3600

            # Offset in latitude [float32][unit = radian]
            obs.head.pos.betof = 0.0

            # -------------------------
            # Spectroscopic Information
            # (only used if If head.gen.kind = spec)
            # -------------------------
            # Spectral line name char[12]
            # (Use beam name from Intelsat FCC technical report.
            # Document uses "UPK1" and "UPKR" in same report, but I use "UPKR" to indicate
            # RHCP (right hand circular polarization)
            obs.head.spe.line = "UPKR"

            # Rest frequency [float64][unit = MHz]
            obs.head.spe.restf = 11699.00

            # Number of channels [int32][no unit]
            # In this example, hardcode 1 because it is a continuum detector.  In the real
            # data pipeline, this will come from an array dimension
            obs.head.spe.nchan = int(nchan)

            if nchan % 2 == 0:
                obs.head.spe.rchan = nchan / 2 + 0.5
            else:
                obs.head.spe.rchan = nchan / 2

            # Frequency resolution [float32][unit = MHz]
            obs.head.spe.fres = fs / nchan / 1e6

            # Velocity resolution [float32][unit = km/s]
            speed_of_light = 299792458.0
            obs.head.spe.vres = -1 * speed_of_light * obs.head.spe.fres / obs.head.spe.restf / 1e3

            # Velocity at reference channel [float32][unit = km/s]
            obs.head.spe.voff = 0.0

            # Blanking value [float32][unit?]
            obs.head.spe.bad = 0.0

            # Image frequency [float64][unit = MHz]
            obs.head.spe.image = 0.0

            # Type of velocity [code] {unk, lsr, helio, obs, earth, auto}
            obs.head.spe.vtype = code.velo.obs

            # Velocity convention [code] {unk, rad, opt, thirtym}
            # Use rad = radio (not optical or 30m IRAM convention .. whatever that means).
            obs.head.spe.vconv = code.conv.rad

            # Doppler correction -V/c (CLASS convention) [float64][unit = km/s]
            obs.head.spe.doppler = 0.0

            # -------------------------
            # Spectrum: Measurement Data
            # -------------------------
            # Slice the data array to select 1 integration, all channels
            obs.datay = np.array(arraydata["DATA"][index_integration, :], dtype=np.float32)

            # Write to the file
            obs.write()

            # -------------------------
            # Prepare header to write Continuum drift
            # -------------------------
            obs.head.gen.kind = code.kind.cont

            # Clear the position offset  It doesn't have meaning for this drift data structure,
            # but it will display in the graph window and be confusing.
            obs.head.pos.lamof = 0.0

            # -------------------------
            # Continuum drift description
            # (only used if If head.gen.kind = cont)
            # -------------------------
            # Rest frequency [float64][unit = MHz]
            obs.head.dri.freq = 11699.00

            # Bandwidth [float32][unit = MHz]
            obs.head.dri.width = 0.0  # Where does this show up in the file?

            # Number of data points [int32][no unit]
            obs.head.dri.npoin = npoin

            # Reference point [float32][no unit]

            # If number of points in drift is an odd number, use the center point.
            # else, create a reference point between the 2 center points.
            if npoin % 2 == 0:
                obs.head.dri.rpoin = npoin / 2 + 0.5
            else:
                obs.head.dri.rpoin = npoin / 2

            # Time at reference [float32][?]
            obs.head.dri.tref = 0.0

            # Angular offset at reference [float32][unit = radian]
            obs.head.dri.aref = 0.0

            # Position angle of drift [float32][unit = radian]
            obs.head.dri.apos = 0.0

            # Resolution [float32][unit = seconds]
            obs.head.dri.tres = integration_time

            # Angular resolution [float32][unit = radian]
            # convert arcseconds to radians also
            obs.head.dri.ares = (np.pi / 180) * (np.mean(np.diff(angles)) / 3600)

            # Blanking value [float32]
            obs.head.dri.bad = 0.0

            # Type of offsets [code]
            obs.head.dri.ctype = code.coord.hor

            # Image frequency [float64][unit = MHz]
            obs.head.dri.cimag = 0.0

            # Collimation error Az [float32]
            obs.head.dri.colla = 0.0

            # Collimation error El [float32]
            obs.head.dri.colle = 0.0

            # -------------------------
            # Drift: Measurement Data
            # -------------------------
            # Numpy array. Dimensions should be 1 * nchan. set data type = float32
            # Slice the data array to select 1 channel, all integrations.  good for pointing scan.
            obs.datay = np.array(arraydata["DATA"][:, index_channel], dtype=np.float32)
            obs.write()
            fileout.close()
        except Exception as e:
            self.fail(e)
