# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
import os
import time
import subprocess

from unittest_common import AsyncMessagingTestCase
from CentralDB import CentralDB
import cdbConstants


class EddFitsMockTestCase(AsyncMessagingTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1600_EddFitsClient_no_handlers(self):
        """
        Test that the EddFitsClient can receive packets from EddFitsMockServer. Don't do anything
        with the packets.  Only confirm that the client can receive from the socket.
        """
        try:
            # Start the FitsMockServer to generate packets of data
            subscan_duration = 5
            period = 0.2
            nsections = 4
            nchannels = 2048
            capture_data_duration = 5

            self.logger.debug("Start fits mock server")

            absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/EddFitsMockServer.py"
            cmd = [
                "python",
                absolute_path,
                "-d",
                "{}".format(subscan_duration),
                "-T",
                "{}".format(period),
                "-s",
                "{}".format(nsections),
                "-n",
                "{}".format(nchannels),
                "-c",
            ]
            self.logger.debug("Launching process: {}".format(cmd))
            fits_mock_server_proc = subprocess.Popen(cmd)

            time.sleep(1)
            # # Start the FitsClient to receive the packets and process them.
            self.logger.debug(
                "Start fits client and receive packets for {} seconds".format(capture_data_duration)
            )
            self.objref.fits_client_start()

            time.sleep(capture_data_duration)

            # Stop server, then client
            self.logger.debug("Stop fits client")
            self.objref.fits_client_stop()
            time.sleep(1)
            self.logger.debug("Stop fits mock server")
            fits_mock_server_proc.terminate()
            fits_mock_server_proc.wait()

        except Exception as e:
            # Close the fits server process and then fail this unittest
            self.logger.debug("Stop fits mock server")
            fits_mock_server_proc.terminate()
            fits_mock_server_proc.wait()
            raise

    def test1610_EddFitsClient_registered_handlers_mock_server(self):
        """
        Test that the EddFitsClient can receive packets from EddFitsMockServer and
        handle the packets using registered handler functions from list of
        active pipelinesin DataAggregator
        """
        try:
            # That all events are received by the DataAggregator.  If DataMonitor GUI is open,
            # look at update information in each section.
            DELAY = 0.05

            # Paramters to simulate data and generate spectrum arraydata_row objects.
            subs_num = '1'
            subscan_duration = 30
            integration_time = 1
            nsections = 2
            nchannels = 1024
            interleave_noise_packets = True

            # clear cdb and fill correct metadata
            cdb = CentralDB()
            cdb.reset_obs()
            cdb.set(cdbConstants.SUBSCAN_NUMBER, subs_num)
            cdb.set(cdbConstants.CHANNELS, nchannels)

            capture_data_duration = 30

            # Simulate header data
            (metadata, time_apy) = self.simulate_headers()

            # Publish new scan metadata.  DataAggregator notification consumer will catch this data and keep it in the local self.metadata
            self.suppliers["scan"]["supplier"].publish_event(metadata)
            time.sleep(DELAY)

            self.logger.debug("Start fits mock server")
            absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/EddFitsMockServer.py"
            cmd = [
                "python",
                absolute_path,
                "-d",
                "{}".format(subscan_duration),
                "-T",
                "{}".format(integration_time),
                "-s",
                "{}".format(nsections),
                "-n",
                "{}".format(nchannels),
                "-c",
            ]
            self.logger.debug("Launching process: {}".format(cmd))
            fits_mock_server_proc = subprocess.Popen(cmd)
            time.sleep(1)

            # Do a test of pipeline with simulated data for a pointing scan.
            self.objref.open_pipeline("mbfits")
            self.objref.open_pipeline("spectrum_preview")
            time.sleep(DELAY)

            # Clear accumulators of MONITOR-XXX for each new SCAN
            self.objref.clear_monitor_buffers()
            time.sleep(DELAY)

            # Write current scan header in all open pipelines
            self.objref.write_scan_header()
            time.sleep(DELAY)

            # Clear accumulators of ARRAYDATA and DATAPAR for each new SUBSCAN
            self.objref.clear_subscan_buffers()
            time.sleep(DELAY)

            # Write subscan header in all open pipelines
            self.objref.write_before_subscan()
            time.sleep(DELAY)

            # # Start the FitsClient to receive the packets and process them.
            self.logger.debug(
                "Start fits client and receive packets for {} seconds".format(capture_data_duration)
            )
            self.objref.fits_client_start()

            # Enable writing realtime packets
            self.objref.enable_write("mbfits", subs_num)
            self.objref.enable_write("spectrum_preview", subs_num)

            # EddFitsMockServer and EddFitsClient are running infinite loop.
            # Sleep this unittest thread and let the client and pipelines work.
            time.sleep(capture_data_duration)

            # Enable writing realtime packets
            self.objref.disable_write("mbfits", subs_num)
            self.objref.disable_write("spectrum_preview", subs_num)

            # Subscan is finished.  Write all the subscan data
            self.objref.write_after_subscan(subs_num)
            time.sleep(DELAY)
            
            # Stop server, then client
            self.logger.debug("Stop fits client")
            self.objref.fits_client_stop()
            time.sleep(1)

            self.objref.write_monitor()
            time.sleep(1.0)
            # Close all
            filename_mbfits = self.objref.close_pipeline("mbfits")
            self.logger.debug("closed file {}".format(filename_mbfits))

            filename_preview = self.objref.close_pipeline("spectrum_preview")
            self.logger.debug("closed file {}".format(filename_preview))

            self.logger.debug("Stop fits mock server")
            fits_mock_server_proc.terminate()
            fits_mock_server_proc.wait()

            # Preview file.  Block unittest until the user close the GUI window
            absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/view_mbfits.py"
            cmd = ["python", absolute_path, filename_mbfits]
            self.logger.debug("Launching process: {}".format(cmd))
            self.gui_proc = subprocess.Popen(cmd)
            returncode = self.gui_proc.wait()
            self.logger.debug("GUI process finished. return code: {}".format(returncode))

        except Exception as e:
            # Close the fits server process and then fail this unittest
            self.logger.debug("Stop fits mock server")
            fits_mock_server_proc.terminate()
            fits_mock_server_proc.wait()
            self.logger.logError(e)
            raise
