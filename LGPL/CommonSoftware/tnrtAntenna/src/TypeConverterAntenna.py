# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.05

# TypeConverterAntennas module includes classes and functions to convert data types between
# * ACU binary message (ctypes) and  ACS notification channel classes (CORBA / OMG IDL structs)
# * CORBA / IDL enum type and cytpes objects

# Conversion from ctypes to python dictionaries is simple - use from_buffer command.
# however, IDL structs do not have such a convenient function.  CORBA ValueType could
# be used instead of struct and enable the use of a Factory function to construct, but
# This technique is easier becuase data always moves between ACU and Antenna component
# locally on this machine.

# NOTE!  convert ctypes array to list using [:] .  This allows us to copy into the IDL
# struct without manual iteration.

# CORBA IDL Types
import tnrtAntennaMod

# shortcuts
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import PO as PO
from tnrtAntennaMod import TR as TR
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import M3 as M3
from tnrtAntennaMod import M3R as M3R
from tnrtAntennaMod import M4A as M4A
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import VX as VX
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import GRS as GRS
from tnrtAntennaMod import hxp_x as hxp_x
from tnrtAntennaMod import hxp_y as hxp_y
from tnrtAntennaMod import hxp_z as hxp_z
from tnrtAntennaMod import hxp_tx as hxp_tx
from tnrtAntennaMod import hxp_ty as hxp_ty
from tnrtAntennaMod import hxp_tz as hxp_tz

# Python ctypes
import DataFormatAcu as acu

# Lookup Table: code (command to ACU) from enum (used in ACS Object Explorer GUI)
enum_properties = {
    tnrtAntennaMod.LOCAL_M: {"acu_code": acu.CMD_SET_MASTER_LCP},
    tnrtAntennaMod.REMOTE_M: {"acu_code": acu.CMD_SET_MASTER_REMOTE},
    tnrtAntennaMod.LOCAL_HHP: {"acu_code": acu.CMD_SET_MASTER_HHP},
    tnrtAntennaMod.LOCAL_SECONDARY_M: {"acu_code": acu.CMD_SET_MASTER_SCP},
    tnrtAntennaMod.LOCAL_TCP: {"acu_code": acu.CMD_SET_MASTER_TCP},
    tnrtAntennaMod.PresetPos1: {"acu_code": acu.PRESET_POS_1},
    tnrtAntennaMod.PresetPos2: {"acu_code": acu.PRESET_POS_2},
    tnrtAntennaMod.PresetPos3: {"acu_code": acu.PRESET_POS_3},
    tnrtAntennaMod.PresetPos4: {"acu_code": acu.PRESET_POS_4},
    tnrtAntennaMod.PresetPos5: {"acu_code": acu.PRESET_POS_5},
    tnrtAntennaMod.PresetPos6: {"acu_code": acu.PRESET_POS_6},
    tnrtAntennaMod.PresetPos7: {"acu_code": acu.PRESET_POS_7},
    tnrtAntennaMod.PresetPos8: {"acu_code": acu.PRESET_POS_8},
    tnrtAntennaMod.PresetPos9: {"acu_code": acu.PRESET_POS_9},
    tnrtAntennaMod.PresetPos10: {"acu_code": acu.PRESET_POS_10},
    tnrtAntennaMod.stowpin1: {"acu_code": acu.STOWPIN_ONLY1},
    tnrtAntennaMod.stowpin2: {"acu_code": acu.STOWPIN_ONLY2},
    tnrtAntennaMod.stowpinBoth: {"acu_code": acu.STOWPIN_BOTH},
    tnrtAntennaMod.stowPositionNearest: {"acu_code": acu.CMD_DRIVE_TO_STOW_NEXT},
    tnrtAntennaMod.stowPosition1: {"acu_code": acu.CMD_DRIVE_TO_STOW_1},
    tnrtAntennaMod.stowPosition2: {"acu_code": acu.CMD_DRIVE_TO_STOW_2},
    tnrtAntennaMod.offF: {"acu_code": acu.CMD_SET_FAN_OFF},
    tnrtAntennaMod.onF: {"acu_code": acu.CMD_SET_FAN_ON},
    tnrtAntennaMod.INACTIVE: {"acu_code": acu.CMD_SET_MThreesyncEl_INACTIVE},
    tnrtAntennaMod.ACTIVE: {"acu_code": acu.CMD_SET_MThreesyncEl_ACTIVE},
    tnrtAntennaMod.POINT_COR_OFF: {"acu_code": acu.CMD_POINTING_CORRECTION_OFF},
    tnrtAntennaMod.POINT_COR_ON: {"acu_code": acu.CMD_POINTING_CORRECTION_ON},
    tnrtAntennaMod.INTERNAL: {"acu_code": acu.CMD_TIME_SOURCE_INTERNAL},
    tnrtAntennaMod.NTP: {"acu_code": acu.CMD_TIME_SOURCE_NTP},
    tnrtAntennaMod.ABSOLUTE: {"acu_code": acu.CMD_TIME_SOURCE_ABSOLUTE},
    tnrtAntennaMod.PLUS1SEC: {"acu_code": acu.CMD_TIME_OFFSET_PLUS1SEC},
    tnrtAntennaMod.MINUS1SEC: {"acu_code": acu.CMD_TIME_OFFSET_MINUS1SEC},
    tnrtAntennaMod.ABS_OFFSET: {"acu_code": acu.CMD_TIME_OFFSET_ABSOLUTE},
    tnrtAntennaMod.REL_OFFSET: {"acu_code": acu.CMD_TIME_OFFSET_RELATIVE},
    tnrtAntennaMod.EL_TABLE_OFF: {
        "acu_code": acu.CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON_DISABLE
    },
    tnrtAntennaMod.EL_TABLE_ON: {
        "acu_code": acu.CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON_ENABLE
    },
    tnrtAntennaMod.INCLINOMETER: {
        "acu_code": acu.CMD_CORRECTION_VALUES_ON_INCLINOMETER
    },
    tnrtAntennaMod.REFRACTION: {"acu_code": acu.CMD_CORRECTION_VALUES_ON_REFRACTION},
    tnrtAntennaMod.POINTING_MODEL: {"acu_code": acu.CMD_CORRECTION_VALUES_ON_PMODEL},
    tnrtAntennaMod.CORRECTION_OFF: {"acu_code": acu.CMD_CORRECTION_VALUES_ON_DISABLE},
    tnrtAntennaMod.CORRECTION_ON: {"acu_code": acu.CMD_CORRECTION_VALUES_ON_ENABLE},
    tnrtAntennaMod.POS_OFF_X: {"acu_code": acu.CMD_HEXAPOD_POSITION_OFFSET_X},
    tnrtAntennaMod.POS_OFF_Y: {"acu_code": acu.CMD_HEXAPOD_POSITION_OFFSET_Y},
    tnrtAntennaMod.POS_OFF_Z: {"acu_code": acu.CMD_HEXAPOD_POSITION_OFFSET_Z},
    tnrtAntennaMod.POS_OFF_TX: {"acu_code": acu.CMD_HEXAPOD_POSITION_OFFSET_TX},
    tnrtAntennaMod.POS_OFF_TY: {"acu_code": acu.CMD_HEXAPOD_POSITION_OFFSET_TY},
    tnrtAntennaMod.POS_OFF_TZ: {"acu_code": acu.CMD_HEXAPOD_POSITION_OFFSET_TZ},
    tnrtAntennaMod.DISABLE_AND_DEACTIVATE: {
        "acu_code": acu.CMD_ENABLE_TRACKING_DISABLE_AND_DEACTIVATE
    },
    tnrtAntennaMod.ENABLE_AND_ACTIVATE: {
        "acu_code": acu.CMD_ENABLE_TRACKING_ENABLE_AND_ACTIVATE
    },
    tnrtAntennaMod.newL: {"acu_code": acu.CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_NEW},
    tnrtAntennaMod.addL: {"acu_code": acu.CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_ADD},
    tnrtAntennaMod.resetL: {"acu_code": acu.CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_RESET},
    tnrtAntennaMod.NEWTON: {
        "acu_code": acu.CMD_LOAD_PROGRAM_TRACK_INTERPOLATION_MODE_NEWTON
    },
    tnrtAntennaMod.SPLINE: {
        "acu_code": acu.CMD_LOAD_PROGRAM_TRACK_INTERPOLATION_MODE_SPLINE
    },
    tnrtAntennaMod.azel: {"acu_code": acu.CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_AZEL},
    tnrtAntennaMod.radec: {"acu_code": acu.CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_RADEC},
    tnrtAntennaMod.radecshortcut: {
        "acu_code": acu.CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_RADECSHORTCUT
    },
    tnrtAntennaMod.TLE: {"acu_code": acu.CMD_SET_TRACK_MODE_TLE},
    tnrtAntennaMod.PROGRAM_TRACK: {"acu_code": acu.CMD_SET_TRACK_MODE_PROGRAM_TRACK},
    tnrtAntennaMod.SUN_TRACK: {"acu_code": acu.CMD_SET_TRACK_MODE_SUN_TRACK},
    tnrtAntennaMod.STAR_TRACK: {"acu_code": acu.CMD_SET_TRACK_MODE_STAR_TRACK},
}

ack_properties = {
    acu.CMD_ACK_STS_NO_COMMAND.value: {"idl_enum": tnrtAntennaMod.NO_COMMAND},
    acu.CMD_ACK_STS_DONE.value: {"idl_enum": tnrtAntennaMod.DONE},
    acu.CMD_ACK_STS_WRONG_MODE.value: {"idl_enum": tnrtAntennaMod.WRONG_MODE},
    acu.CMD_ACK_STS_PARAM_ERR.value: {"idl_enum": tnrtAntennaMod.PARAM_ERROR},
    acu.CMD_ACK_STS_WRONG_LEN.value: {"idl_enum": tnrtAntennaMod.WRONG_LENGTH},
    acu.CMD_ACK_STS_UNDEF.value: {"idl_enum": tnrtAntennaMod.UNDEF_COMMAND},
    acu.CMD_ACK_STS_ACCEPTED.value: {"idl_enum": tnrtAntennaMod.ACCEPTED},
    acu.CMD_ACK_STS_TIMEOUT.value: {"idl_enum": tnrtAntennaMod.TIMEOUT},
}

hxp_axis_properties = {
    "x": tnrtAntennaMod.hxp_x,
    "y": tnrtAntennaMod.hxp_y,
    "z": tnrtAntennaMod.hxp_z,
    "tx": tnrtAntennaMod.hxp_tx,
    "ty": tnrtAntennaMod.hxp_ty,
    "tz": tnrtAntennaMod.hxp_tz,
}

# Lookup Table: code (command to ACU) from IDL subsystem ID (used in ACS Object Explorer).
# and properties for each subsystem.  velocity and position limits come from the MTM
# ACU local configuration parameters.
subsystem_properties = {
    AZ: {
        "acu_code": acu.SUBSYSTEM_AZ,
        "P108_v_MaxSys": 2.0,
        "P109_v_MinSys" : 0.0001,
        "P104_p_FinEndDn": -72,
        "P105_p_FinEndUp": 427,
        "P111_a_MaxSys" : 0.7,
    },
    EL: {
        "acu_code": acu.SUBSYSTEM_EL,
        "P108_v_MaxSys": 1.0,
        "P109_v_MinSys" : 0.0001,
        "P104_p_FinEndDn": 3.5,
        "P105_p_FinEndUp": 90.0,
        "P111_a_MaxSys" : 0.33,
    },
    PO: {"acu_code": acu.SUBSYSTEM_PO},
    TR: {"acu_code": acu.SUBSYSTEM_TR},
    HXP: {
        "acu_code": acu.SUBSYSTEM_HXP,
        hxp_x: {"EndDn": -35, "EndUp": 35, "v_MaxSys": 2.0, "v_MinSys": 0.0001},
        hxp_y: {"EndDn": -45, "EndUp": 45, "v_MaxSys": 2.0, "v_MinSys": 0.0001},
        hxp_z: {"EndDn": -85, "EndUp": 85, "v_MaxSys": 2.0, "v_MinSys": 0.0001},
        hxp_tx: {"EndDn": -0.25, "EndUp": 0.25, "v_MaxSys": 0.04, "v_MinSys": 0.0001},
        hxp_ty: {"EndDn": -0.25, "EndUp": 0.25, "v_MaxSys": 0.04, "v_MinSys": 0.0001},
        hxp_tz: {"EndDn": -0.25, "EndUp": 0.25, "v_MaxSys": 0.04, "v_MinSys": 0.0001},
    },
    M3: {
        "acu_code": acu.SUBSYSTEM_M3,
        "P108_v_MaxSys": 1.0,
        "P104_p_FinEndDn": -2,
        "P105_p_FinEndUp": 182,
    },
    M3R: {"acu_code": acu.SUBSYSTEM_M3R, "P108_v_MaxSys": 2.0},
    M4A: {"acu_code": acu.SUBSYSTEM_M4A, "P108_v_MaxSys": 0.5},
    M4B: {"acu_code": acu.SUBSYSTEM_M4B, "P108_v_MaxSys": 0.5},
    VX: {
        "acu_code": acu.SUBSYSTEM_VX,
    },
    THU: {
        "acu_code": acu.SUBSYSTEM_THU,
        "P108_v_MaxSys": 0.5,
        "P104_p_FinEndDn": 0,
        "P105_p_FinEndUp": 180,
    },
    GRS: {
        "acu_code": acu.SUBSYSTEM_GRS,
        "P108_v_MaxSys": 6.0,
        "P104_p_FinEndDn": -3.50,
        "P105_p_FinEndUp": 928.5,
    },
}

# Converter functions to translate ctypes into IDL structs.
# Use the constructor of IDL struct Python objects to gaurantee that we fill all
# parameters in the correct order.

# NOTE!  All ctypes arrays must convert to list using [:] to assign in IDL struct
# This allows us to copy into the IDL struct without manual iteration.
def from_ctype_GeneralStatus(ctypes_object):
    return tnrtAntennaMod.GeneralStatus(
        ctypes_object.version,
        ctypes_object.diagnosis_signal,
        ctypes_object.facility_control_status,
        ctypes_object.power_status,
        ctypes_object.operation_status,
        ctypes_object.system_warnings,
        ctypes_object.software_interlock,
        ctypes_object.emergency_stops,
        ctypes_object.safety_device_errors,
        ctypes_object.safety_device_errors_aux,
        ctypes_object.communication_status,
        ctypes_object.control_mode_status,
        ctypes_object.actual_time,
        ctypes_object.actual_time_offset,
        ctypes_object.time_source,
        ctypes_object.no_pps_signal,
        ctypes_object.clock_online,
        ctypes_object.clock_ok,
        ctypes_object.wind_speed,
        ctypes_object.temperature_M1[:],
    )


def from_ctype_SystemStatus(ctypes_object):
    return tnrtAntennaMod.SystemStatus(ctypes_object.bit_status)


def from_ctype_VertexShutterStatus(ctypes_object):
    return tnrtAntennaMod.VertexShutterStatus(
        ctypes_object.status,
        ctypes_object.elements_open,
        ctypes_object.elements_close,
        ctypes_object.command_elements_open,
        ctypes_object.command_elements_close,
    )


def from_ctype_AxisStatus(ctypes_object):
    return tnrtAntennaMod.AxisStatus(
        ctypes_object.bit_sts,
        ctypes_object.stow_status,
        ctypes_object.error_status,
        ctypes_object.warning_status,
        ctypes_object.encoder_disc_status,
        ctypes_object.state,
        ctypes_object.trajectory_state,
        ctypes_object.actual_mode,
        ctypes_object.desired_position,
        ctypes_object.trajectory_generator_position,
        ctypes_object.encoder_position_incl_pcorr,
        ctypes_object.actual_position,
        ctypes_object.current_position_offset,
        ctypes_object.position_deviation,
        ctypes_object.filtered_position_deviation,
        ctypes_object.current_pointing_correction,
        ctypes_object.external_position_offset,
        ctypes_object.trajectory_generator_velocity,
        ctypes_object.desired_velocity,
        ctypes_object.current_velocity,
        ctypes_object.trajectory_generator_acceleration,
        ctypes_object.motor_selection,
        ctypes_object.brakes_selection,
        ctypes_object.brakes_open,
        ctypes_object.brakes_command,
    )


def from_ctype_MotorStatus(ctypes_object):
    return tnrtAntennaMod.MotorStatus(
        ctypes_object.bit_sts,
        ctypes_object.actual_position,
        ctypes_object.actual_velocity,
        ctypes_object.actual_torque,
        ctypes_object.utilization_rate,
        ctypes_object.motor_temperature,
        ctypes_object.motor_overload_inverter,
        ctypes_object.motor_overload_motor,
        ctypes_object.motor_error_instance,
        ctypes_object.motor_error_reaction,
        ctypes_object.motor_error_module,
        ctypes_object.motor_error_code,
        ctypes_object.limit_down,
        ctypes_object.limit_up,
    )


def from_ctype_TrackingStatus(ctypes_object):
    return tnrtAntennaMod.TrackingStatus(
        ctypes_object.bit_status,
        ctypes_object.prog_track_bs,
        ctypes_object.prog_track_act_index,
        ctypes_object.prog_track_end_index,
        ctypes_object.prog_offset_bs,
        ctypes_object.prog_offset_act_index,
        ctypes_object.prog_offset_end_index,
        ctypes_object.sun_track_az,
        ctypes_object.sun_track_el,
        ctypes_object.prog_track_az,
        ctypes_object.prog_track_el,
        ctypes_object.star_track_az,
        ctypes_object.star_track_el,
        ctypes_object.tle_track_az,
        ctypes_object.tle_track_el,
        ctypes_object.prog_offset_az,
        ctypes_object.prog_offset_el,
        ctypes_object.position_offset_az,
        ctypes_object.position_offset_el,
        ctypes_object.time_offset,
        ctypes_object.desired_az,
        ctypes_object.desired_el,
        ctypes_object.antenna_latitude,
        ctypes_object.antenna_longitude,
        ctypes_object.antenna_altitude,
        ctypes_object.inclinometer1_x,
        ctypes_object.inclinometer1_y,
        ctypes_object.inclinometer1_temp,
        ctypes_object.inclinometer2_x,
        ctypes_object.inclinometer2_y,
        ctypes_object.inclinometer2_temp,
        ctypes_object.inclinometer_model_correction_el,
        ctypes_object.p1,
        ctypes_object.p2,
        ctypes_object.p3,
        ctypes_object.p4,
        ctypes_object.p5,
        ctypes_object.p6,
        ctypes_object.p7,
        ctypes_object.p8,
        ctypes_object.p9,
        ctypes_object.pointing_model_correction_az,
        ctypes_object.pointing_model_correction_el,
        ctypes_object.r0,
        ctypes_object.b1,
        ctypes_object.b2,
        ctypes_object.refraction_model_correction_el,
        ctypes_object.user_fine_pointing_correction_az,
        ctypes_object.user_fine_pointing_correction_el,
        ctypes_object.total_pointing_correction_az,
        ctypes_object.total_pointing_correction_el,
        ctypes_object.total_pointing_correction_grs,
        ctypes_object.dut1,
        ctypes_object.gst0hut,
    )


def from_ctype_GeneralHexapodStatus(ctypes_object):
    return tnrtAntennaMod.GeneralHexapodStatus(ctypes_object.general_hexapod_status)


def from_ctype_HexapodStatus(ctypes_object):
    return tnrtAntennaMod.HexapodStatus(
        ctypes_object.bit_sts,
        ctypes_object.bit_error,
        ctypes_object.bit_warning,
        ctypes_object.state,
        ctypes_object.spindle_select,
        ctypes_object.mode,
        ctypes_object.pos_desired[:],
        ctypes_object.velocity_desired[:],
        ctypes_object.pos_trajectory[:],
        ctypes_object.velocity_trajectory[:],
        ctypes_object.offset[:],
        ctypes_object.pos_actual[:],
        ctypes_object.velocity_actual[:],
    )


def from_ctype_SpindleStatus(ctypes_object):
    return tnrtAntennaMod.SpindleStatus(
        ctypes_object.motor_sts,
        ctypes_object.motor_temperature,
        ctypes_object.motor_temperature_inverter,
        ctypes_object.motor_overload_motor,
        ctypes_object.motor_error_instance,
        ctypes_object.motor_error_reaction,
        ctypes_object.motor_error_module,
        ctypes_object.motor_error_code,
        ctypes_object.upper_limit_switch,
        ctypes_object.lower_limit_switch,
        ctypes_object.actual_torque,
        ctypes_object.rate_loop,
        ctypes_object.actual_velocity,
        ctypes_object.spindle_sts,
        ctypes_object.spindle_errors,
        ctypes_object.spindle_warnings,
        ctypes_object.actual_position,
    )


def from_ctype_HexapodTrackStatus(ctypes_object):
    return tnrtAntennaMod.HexapodTrackStatus(
        ctypes_object.bit_sts,
        ctypes_object.start_time,
        ctypes_object.actual_index,
        ctypes_object.end_index,
        ctypes_object.track_mode_actual_position[:],
    )


def from_ctype_TrackingObjectStatus(ctypes_object):
    return tnrtAntennaMod.TrackingObjectStatus(
        ctypes_object.prog_track_mode,
        ctypes_object.time_offset,
        ctypes_object.tracking_object_position_offset_az,
        ctypes_object.tracking_object_position_offset_el,
        ctypes_object.tle_name.decode("ascii"),
        ctypes_object.tle_line1.decode("ascii"),
        ctypes_object.tle_line2.decode("ascii"),
        ctypes_object.pt_interpolation_mode,
        ctypes_object.pt_track_mode,
        ctypes_object.pt_end_index,
        ctypes_object.pt_start_time,
        ctypes_object.pt_table11,
        ctypes_object.pt_table12,
        ctypes_object.pt_table21,
        ctypes_object.pt_table22,
        ctypes_object.po_interpolation_mode,
        ctypes_object.po_track_mode,
        ctypes_object.po_end_index,
        ctypes_object.po_start_time,
        ctypes_object.po_table11,
        ctypes_object.po_table12,
        ctypes_object.po_table21,
        ctypes_object.po_table22,
        ctypes_object.pt_star_track_epoch,
        ctypes_object.pt_star_track_ra,
        ctypes_object.pt_star_track_dec,
    )


# StatusMessage frame has some arrays of complex types.  we must call the convert_from
# function on only item.  We could make smart functions to handle lists, but this
# situation exists only here, so I just list the items manually to display the contents
# of the packet clearly.
def from_ctype_StatusMessageFrame(time_tcs_mjd, ctypes_object):
    return tnrtAntennaMod.AntennaStatusNotifyBlock(
        time_tcs_mjd,
        ctypes_object.status_message_counter,
        from_ctype_GeneralStatus(ctypes_object.general_sts),
        from_ctype_SystemStatus(ctypes_object.system_sts),
        from_ctype_VertexShutterStatus(ctypes_object.vertexshutter_sts),
        [
            from_ctype_AxisStatus(ctypes_object.axis_status[0]),
            from_ctype_AxisStatus(ctypes_object.axis_status[1]),
            from_ctype_AxisStatus(ctypes_object.axis_status[2]),
            from_ctype_AxisStatus(ctypes_object.axis_status[3]),
            from_ctype_AxisStatus(ctypes_object.axis_status[4]),
            from_ctype_AxisStatus(ctypes_object.axis_status[5]),
            from_ctype_AxisStatus(ctypes_object.axis_status[6]),
            from_ctype_AxisStatus(ctypes_object.axis_status[6]),
        ],
        [
            from_ctype_MotorStatus(ctypes_object.az_msts[0]),
            from_ctype_MotorStatus(ctypes_object.az_msts[1]),
            from_ctype_MotorStatus(ctypes_object.az_msts[2]),
            from_ctype_MotorStatus(ctypes_object.az_msts[3]),
            from_ctype_MotorStatus(ctypes_object.el_msts[0]),
            from_ctype_MotorStatus(ctypes_object.el_msts[1]),
            from_ctype_MotorStatus(ctypes_object.el_msts[2]),
            from_ctype_MotorStatus(ctypes_object.el_msts[3]),
            from_ctype_MotorStatus(ctypes_object.m3_msts[0]),
            from_ctype_MotorStatus(ctypes_object.m3_msts[1]),
            from_ctype_MotorStatus(ctypes_object.m3r_msts),
            from_ctype_MotorStatus(ctypes_object.m4a_msts),
            from_ctype_MotorStatus(ctypes_object.m4b_msts),
            from_ctype_MotorStatus(ctypes_object.thu_msts[0]),
            from_ctype_MotorStatus(ctypes_object.thu_msts[1]),
            from_ctype_MotorStatus(ctypes_object.grs_msts),
        ],
        from_ctype_TrackingStatus(ctypes_object.tracking_sts),
        from_ctype_GeneralHexapodStatus(ctypes_object.general_hexapod_status),
        from_ctype_HexapodStatus(ctypes_object.hexapod_sts),
        [
            from_ctype_SpindleStatus(ctypes_object.spindle_sts[0]),
            from_ctype_SpindleStatus(ctypes_object.spindle_sts[1]),
            from_ctype_SpindleStatus(ctypes_object.spindle_sts[2]),
            from_ctype_SpindleStatus(ctypes_object.spindle_sts[3]),
            from_ctype_SpindleStatus(ctypes_object.spindle_sts[4]),
            from_ctype_SpindleStatus(ctypes_object.spindle_sts[5]),
        ],
        from_ctype_HexapodTrackStatus(ctypes_object.hexapod_track_sts),
        from_ctype_TrackingObjectStatus(ctypes_object.tracking_obj_sts),
    )


def from_idl_timeAzRaElDecList(idl_array):
    pylist = []

    # Convert the list of IDL structures into a standard python list of ctypes objects
    for k in range(len(idl_array)):
        item = acu.TimeAzRaElDec(
            idl_array[k].absTimeMJD, idl_array[k].azRa, idl_array[k].elDec
        )

        pylist.append(item)

    # Convert the standard python list into a ctypes array
    return (acu.TimeAzRaElDec * acu.M1_TRACK_TABLE_LEN_MAX)(*pylist)


def from_idl_timeXYZTXTYTZList(idl_array):
    pylist = []

    # Convert the list of IDL structures into a standard python list of ctypes objects
    for k in range(len(idl_array)):
        item = acu.TimePos(
            idl_array[k].absTimeMJD,
            idl_array[k].x,
            idl_array[k].y,
            idl_array[k].z,
            idl_array[k].tx,
            idl_array[k].ty,
            idl_array[k].tz,
        )

        pylist.append(item)

    # Convert the standard python list into a ctypes array
    return (acu.TimePos * acu.HXP_TRACK_TABLE_LEN_MAX)(*pylist)


def from_idl_elXYZTXTYTZList(idl_array):
    pylist = []

    # Convert the list of IDL structures into a standard python list of ctypes objects
    for k in range(len(idl_array)):
        item = acu.ElPos(
            idl_array[k].el,
            idl_array[k].x,
            idl_array[k].y,
            idl_array[k].z,
            idl_array[k].tx,
            idl_array[k].ty,
            idl_array[k].tz,
        )
        pylist.append(item)

    # Convert the standard python list into a ctypes array
    return (acu.ElPos * acu.HXP_EL_TABLE_LEN_MAX)(*pylist)


def from_idl_REAL64_array_9(idl_array):
    # Convert the standard python list into a ctypes array.  Since the array is only 1D,
    # no need to iterate.  cast the whole thing at once.
    pylist = []
    for k in range(len(idl_array)):
        item = acu.REAL64(idl_array[k])
        pylist.append(item)

    # Convert the standard python list into a ctypes array
    return (acu.REAL64 * 9)(*pylist)


def from_idl_noradTLELine(idl_string):
    # Convert the python strings (which arrived through the CORBA IDL struct)
    # into ctype byte arrays for ACU command packet
    return bytes(idl_string, "ascii")
