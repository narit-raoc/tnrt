# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5200_HXP_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.deactivate(HXP)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5201_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate HXP")
        self.result = self.objref.deactivate(HXP)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5202_acu_axis_mode_activate(self):
        self.logger.debug("activate HXP")
        self.result = self.objref.activate(HXP)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5203_acu_axis_mode_reset(self):
        self.logger.debug("reset HXP")
        self.result = self.objref.reset(HXP)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5250_HXP_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.activate(HXP)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(HXP)
        super().tearDown()

    def test5251_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate HXP")
        self.result = self.objref.deactivate(HXP)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5252_acu_axis_mode_stop(self):
        self.logger.debug("stop HXP")
        self.result = self.objref.stop(HXP)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5253_acu_axis_mode_absolute_position(self):
        self.logger.debug("absolutePosition HXP 0 0")
        self.result = self.objref.absolutePosition(HXP, 0, 0, False)
        self.assertTrue(self.result in [tnrtAntennaMod.PARAM_ERROR])

    def test5254_acu_axis_mode_absolute_position(self):
        self.logger.debug(
            "absolutePositionHxp 10, 2.0, 20, 2.0, 30, 2.0, 0.1, 0.04, -0.1, 0.04, 0.0, 0.04"
        )
        self.result = self.objref.absolutePositionHxp(
            10, 2.0, 20, 2.0, 30, 2.0, 0.1, 0.04, -0.1, 0.04, 0.0, 0.04
        )
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5255_acu_axis_mode_relative_position(self):
        self.logger.debug("relativePosition HXP 0 0")
        self.result = self.objref.relativePosition(HXP, 0, 0)
        self.assertTrue(self.result in [tnrtAntennaMod.PARAM_ERROR])

    def test5256_acu_axis_mode_relative_position(self):
        self.logger.debug(
            "relativePositionHxp 10, 2.0, 20, 2.0, 30, 2.0, 0.1, 0.04, -0.1, 0.04, 0.0, 0.04"
        )
        self.result = self.objref.relativePositionHxp(
            10, 2.0, 20, 2.0, 30, 2.0, 0.1, 0.04, -0.1, 0.04, 0.0, 0.04
        )
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5257_acu_axis_mode_programTrackHxp(self):
        self.logger.debug("programTrack HXP 2.0 0.04")
        self.result = self.objref.programTrackHxp(2.0, 0.04)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5260_acu_parameter_hexapodPositionOffset_X(self):
        self.logger.debug("hexapodPositionOffset POS_OFF_X, 0.1")
        self.result = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_X, 0.1)
        self.logger.info(self.result)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5261_acu_parameter_hexapodPositionOffset_Y(self):
        self.logger.debug("hexapodPositionOffset POS_OFF_Y, 0.1")
        self.result = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_Y, 0.1)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5262_acu_parameter_hexapodPositionOffset_Z(self):
        self.logger.debug("hexapodPositionOffset POS_OFF_Z, 0.1")
        self.result = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_Z, 0.1)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5263_acu_parameter_hexapodPositionOffset_TX(self):
        self.logger.debug("hexapodPositionOffset POS_OFF_TX, 0.1")
        self.result = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TX, 0.1)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5264_acu_parameter_hexapodPositionOffset_TY(self):
        self.logger.debug("hexapodPositionOffset POS_OFF_TY, 0.1")
        self.result = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TY, 0.1)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5265_acu_parameter_hexapodPositionOffset_TZ(self):
        self.logger.debug("hexapodPositionOffset POS_OFF_TZ, 0.1")
        self.result = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TZ, 0.1)
        self.assertTrue(self.result in [ACCEPTED, DONE])


if __name__ == "__main__":
    unittest.main()
