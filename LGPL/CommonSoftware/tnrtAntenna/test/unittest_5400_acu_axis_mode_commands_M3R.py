# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
import time
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import M3R as M3R
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5400_M3R_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.deactivate(M3R)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5401_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate M3R")
        self.result = self.objref.deactivate(M3R)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5402_acu_axis_mode_activate(self):
        self.logger.debug("activate M3R")
        self.result = self.objref.activate(M3R)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5402_acu_axis_mode_reset(self):
        self.logger.debug("reset M3R")
        self.result = self.objref.reset(M3R)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5410_M3R_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.activate(M3R)
        time.sleep(1)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5411_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate M3R")
        self.result = self.objref.deactivate(M3R)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5412_acu_axis_mode_stop(self):
        self.logger.debug("stop M3R")
        self.result = self.objref.stop(M3R)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

if __name__ == "__main__":
    unittest.main()
