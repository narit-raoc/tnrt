# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import TR as TR
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import M3 as M3
from tnrtAntennaMod import M3R as M3R
from tnrtAntennaMod import M4A as M4A
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import VX as VX
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import GRS as GRS
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class Tests(AcuRemoteCommandTestCase):
    def setUp(self):
        """
        This function runs 1 time per test_xxx function in this class. before test start
        """
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.objref.deactivate(HXP)
        self.result = None

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        self.logger.info(self.result)
        super().tearDown()

    def test4000_acu_parameter_setMaster(self):
        self.logger.debug("set mode LOCAL_M")
        self.result = self.objref.setMaster(tnrtAntennaMod.LOCAL_M)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4001_acu_parameter_setMaster(self):
        self.logger.debug("set mode REMOTE_M")
        self.result = self.objref.setMaster(tnrtAntennaMod.REMOTE_M)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4002_acu_parameter_setMaster(self):
        self.logger.debug("set mode LOCAL_HHP")
        self.result = self.objref.setMaster(tnrtAntennaMod.LOCAL_HHP)
        self.assertTrue(self.result in [ACCEPTED, DONE, PARAM_ERROR])

    def test4003_acu_parameter_setMaster(self):
        self.logger.debug("set mode LOCAL_SECONDARY_M")
        self.result = self.objref.setMaster(tnrtAntennaMod.LOCAL_SECONDARY_M)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4004_acu_parameter_setMaster(self):
        self.logger.debug("set mode LOCAL_TCP")
        self.result = self.objref.setMaster(tnrtAntennaMod.LOCAL_TCP)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4006_acu_parameter_setFan(self):
        self.logger.debug("setFan offF")
        self.result = self.objref.setFan(tnrtAntennaMod.offF)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4007_acu_parameter_setFan(self):
        self.logger.debug("setFan onF")
        self.result = self.objref.setFan(tnrtAntennaMod.onF)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4008_acu_parameter_m3SyncEl(self):
        self.logger.debug("M3SyncEl INACTIVE")
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4009_acu_parameter_m3SyncEl(self):
        self.logger.debug("M3SyncEl ACTIVE")
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.ACTIVE)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4016_acu_parameter_setUserPointingCorrectionAz(self):
        self.logger.debug("setUserPointingCorrectionAz 0.02")
        self.result = self.objref.setUserPointingCorrectionAz(0.02)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4017_acu_parameter_setUserPointingCorrectionEl(self):
        self.logger.debug("setUserPointingCorrectionEl 0.03")
        self.result = self.objref.setUserPointingCorrectionEl(0.03)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4018_acu_parameter_trackingOffsetTime(self):
        self.logger.debug("trackingOffsetTime 0.04")
        self.result = self.objref.trackingOffsetTime(0.04)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4019_acu_parameter_timeSource(self):
        self.logger.debug("timeSource ABSOLUTE 59100.0")
        self.result = self.objref.timeSource(
            tnrtAntennaMod.ABSOLUTE, float(Time.now().mjd)
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4020_acu_parameter_timeSource(self):
        self.logger.debug("timeSource NTP 0")
        self.result = self.objref.timeSource(tnrtAntennaMod.NTP, 0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4021_acu_parameter_timeSource(self):
        self.logger.debug("timeSource INTERNAL 0")
        self.result = self.objref.timeSource(tnrtAntennaMod.INTERNAL, 0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4022_acu_parameter_timeOffset(self):
        self.logger.debug("timeOffset PLUS1SEC 5.0")
        self.result = self.objref.timeOffset(tnrtAntennaMod.PLUS1SEC, 5.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4023_acu_parameter_timeOffset(self):
        self.logger.debug("timeOffset MINUS1SEC 5.0")
        self.result = self.objref.timeOffset(tnrtAntennaMod.MINUS1SEC, 5.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4025_acu_parameter_timeOffset(self):
        self.logger.debug("timeOffset REL_OFFSET 5.0")
        self.result = self.objref.timeOffset(tnrtAntennaMod.REL_OFFSET, 5.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4024_acu_parameter_timeOffset(self):
        self.logger.debug("timeOffset ABS_OFFSET 5.0")
        self.result = self.objref.timeOffset(tnrtAntennaMod.ABS_OFFSET, 0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4026_acu_parameter_hexapodElevationTable_OFF(self):
        self.logger.debug("setHexapodElevationOffsetTableEnabled EL_TABLE_OFF")
        self.result = self.objref.setHexapodElevationOffsetTableEnabled(tnrtAntennaMod.EL_TABLE_OFF)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4027_acu_parameter_hexapodElevationTable_ON(self):
        self.logger.debug("setHexapodElevationOffsetTableEnabled EL_TABLE_ON")
        self.result = self.objref.setHexapodElevationOffsetTableEnabled(tnrtAntennaMod.EL_TABLE_ON)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4028_acu_parameter_setCorrectionValuesEnabled(self):
        self.logger.debug("setCorrectionValuesEnabled INCLINOMETER, CORRECTION_OFF")
        self.result = self.objref.setCorrectionValuesEnabled(
            tnrtAntennaMod.INCLINOMETER, tnrtAntennaMod.CORRECTION_OFF
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4029_acu_parameter_setCorrectionValuesEnabled(self):
        self.logger.debug("setCorrectionValuesEnabled INCLINOMETER, CORRECTION_ON")
        self.result = self.objref.setCorrectionValuesEnabled(
            tnrtAntennaMod.INCLINOMETER, tnrtAntennaMod.CORRECTION_ON
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4030_acu_parameter_setCorrectionValuesEnabled(self):
        self.logger.debug("setCorrectionValuesEnabled REFRACTION, CORRECTION_OFF")
        self.result = self.objref.setCorrectionValuesEnabled(
            tnrtAntennaMod.REFRACTION, tnrtAntennaMod.CORRECTION_OFF
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4031_acu_parameter_setCorrectionValuesEnabled(self):
        self.logger.debug("setCorrectionValuesEnabled REFRACTION, CORRECTION_ON")
        self.result = self.objref.setCorrectionValuesEnabled(
            tnrtAntennaMod.REFRACTION, tnrtAntennaMod.CORRECTION_ON
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4032_acu_parameter_setCorrectionValuesEnabled(self):
        self.logger.debug("setCorrectionValuesEnabled POINTING_MODEL, CORRECTION_OFF")
        self.result = self.objref.setCorrectionValuesEnabled(
            tnrtAntennaMod.POINTING_MODEL, tnrtAntennaMod.CORRECTION_OFF
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4033_acu_parameter_setCorrectionValuesEnabled(self):
        self.logger.debug("setCorrectionValuesEnabled POINTING_MODEL, CORRECTION_ON")
        self.result = self.objref.setCorrectionValuesEnabled(
            tnrtAntennaMod.POINTING_MODEL, tnrtAntennaMod.CORRECTION_ON
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4034_acu_parameter_setDUT1AndGST0(self):
        self.logger.debug("setDUT1AndGST0 320.0 876564.32")
        self.result = self.objref.setDUT1AndGST0(320.0, 87654.32)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4041_acu_parameter_enableTracking_enabled_activated(self):
        self.logger.debug("enableTracking Enable and Activate axes")
        self.result = self.objref.enableTracking(tnrtAntennaMod.ENABLE_AND_ACTIVATE)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4042_acu_parameter_enableTracking_disabled_deactivated(self):
        self.logger.debug("enableTracking Disable and Deactivate axes")
        self.result = self.objref.enableTracking(tnrtAntennaMod.DISABLE_AND_DEACTIVATE)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test4043_acu_parameter_stopTracking(self):
        self.logger.debug("stopTracking")
        self.result = self.objref.stopTracking()
        self.assertTrue(self.result in [ACCEPTED, DONE])


if __name__ == "__main__":
    unittest.main()
