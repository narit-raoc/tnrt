# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

# import standard modules
import unittest
from unittest_common import ColoredTestCase

# import TCS modules
import tnrtAntennaMod
import TypeConverterAntenna as tc
import DataFormatAcu as acu


class Tests(ColoredTestCase):
    def setUp(self):
        """
        This function runs 1 time per test_xxx function in this class. before test start
        """
        super().setUp()

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        super().tearDown()

    def test1000_type_converter_general_status(self):
        self.logger.debug("from_ctype_GeneralStatus")
        ctypes_obj = acu.GeneralStatus()
        result = tc.from_ctype_GeneralStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.GeneralStatus)

    def test1001_type_converter_system_status(self):
        self.logger.debug("from_ctype_SystemStatus")
        ctypes_obj = acu.SystemStatus()
        result = tc.from_ctype_SystemStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.SystemStatus)

    def test1002_type_converter_vertex_status(self):
        self.logger.debug("from_ctype_VertexShutterStatus")
        ctypes_obj = acu.VertexShutterStatus()
        result = tc.from_ctype_VertexShutterStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.VertexShutterStatus)

    def test1003_type_converter_axis_status(self):
        self.logger.debug("from_ctype_AxisStatus")
        ctypes_obj = acu.AxisStatus()
        result = tc.from_ctype_AxisStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.AxisStatus)

    def test1004_type_converter_motor_status(self):
        self.logger.debug("from_ctype_MotorStatus")
        ctypes_obj = acu.MotorStatus()
        result = tc.from_ctype_MotorStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.MotorStatus)

    def test1005_type_converter_tracking_status(self):
        self.logger.debug("from_ctype_TrackingStatus")
        ctypes_obj = acu.TrackingStatus()
        result = tc.from_ctype_TrackingStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.TrackingStatus)

    def test1006_type_converter_general_hexapod_status(self):
        self.logger.debug("from_ctype_GeneralHexapodStatus")
        ctypes_obj = acu.GeneralHexapodStatus()
        result = tc.from_ctype_GeneralHexapodStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.GeneralHexapodStatus)

    def test1007_type_converter_hexapod_status(self):
        self.logger.debug("from_ctype_HexapodStatus")
        ctypes_obj = acu.HexapodStatus()
        result = tc.from_ctype_HexapodStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.HexapodStatus)

    def test1008_type_converter_spindle_status(self):
        self.logger.debug("from_ctype_SpindleStatus")
        ctypes_obj = acu.SpindleStatus()
        result = tc.from_ctype_SpindleStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.SpindleStatus)

    def test1009_type_converter_hexapod_tracking_status(self):
        self.logger.debug("from_ctype_HexapodTrackStatus")
        ctypes_obj = acu.HexapodTrackStatus()
        result = tc.from_ctype_HexapodTrackStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.HexapodTrackStatus)

    def test110_type_converter_tracking_object_status(self):
        self.logger.debug("from_ctype_TrackingObjectStatus")
        ctypes_obj = acu.TrackingObjectStatus()
        result = tc.from_ctype_TrackingObjectStatus(ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.TrackingObjectStatus)

    def test111_type_converter_status_message_frame(self):
        self.logger.debug("from_ctype_StatusMessageFrame")
        time_mjd = 59111.0
        ctypes_obj = acu.StatusMessageFrame()
        result = tc.from_ctype_StatusMessageFrame(time_mjd, ctypes_obj)
        self.logger.info(result)
        self.assertIsInstance(result, tnrtAntennaMod.AntennaStatusNotifyBlock)


if __name__ == "__main__":
    unittest.main()
