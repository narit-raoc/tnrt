# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
from unittest_common import AcuTestCase

# import TCS modules
import tnrtAntennaMod


class Tests(AcuTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test3000_acu_status_getAzPosition(self):
        self.logger.debug("getAzPosition")
        self.result = self.objref.getAzPosition()
        self.assertIsInstance(self.result, float)

    def test3001_acu_status_getElPosition(self):
        self.logger.debug("getElPosition")
        self.result = self.objref.getElPosition()
        self.assertIsInstance(self.result, float)

    def test3002_acu_status_getM3Position(self):
        self.logger.debug("getM3Position")
        self.result = self.objref.getM3Position()
        self.assertIsInstance(self.result, float)

    def test3003_acu_status_getM3RPosition(self):
        self.logger.debug("getM3RPosition")
        self.result = self.objref.getM3RPosition()
        self.assertIsInstance(self.result, float)

    def test3004_acu_status_getM4APosition(self):
        self.logger.debug("getM4APosition")
        self.result = self.objref.getM4APosition()
        self.assertIsInstance(self.result, float)

    def test3005_acu_status_getM4BPosition(self):
        self.logger.debug("getM4BPosition")
        self.result = self.objref.getM4BPosition()
        self.assertIsInstance(self.result, float)

    def test3006_acu_status_getTHUPosition(self):
        self.logger.debug("getTHUPosition")
        self.result = self.objref.getTHUPosition()
        self.assertIsInstance(self.result, float)

    def test3007_acu_status_getGRSPosition(self):
        self.logger.debug("getGRSPosition")
        self.result = self.objref.getGRSPosition()
        self.assertIsInstance(self.result, float)

    def test3008_acu_status_getCurrentPointingCorrectionAZ(self):
        self.logger.debug("getCurrentPointingCorrectionAZ")
        self.result = self.objref.getCurrentPointingCorrectionAZ()
        self.assertIsInstance(self.result, float)

    def test3009_acu_status_getCurrentPointingCorrectionEL(self):
        self.logger.debug("getCurrentPointingCorrectionEL")
        self.result = self.objref.getCurrentPointingCorrectionEL()
        self.assertIsInstance(self.result, float)

    def test3010_acu_status_getFacilityControlSts(self):
        self.logger.debug("getFacilityControlSts")
        self.result = self.objref.getFacilityControlSts()
        self.assertIsInstance(self.result, int)

    def test3011_acu_status_getTrackStatus(self):
        self.logger.debug("getTrackStatus")
        self.result = self.objref.getTrackStatus()
        self.assertIsInstance(self.result, int)

    def test3012_acu_status_getProgramTrackStatus(self):
        self.logger.debug("getProgramTrackStatus")
        self.result = self.objref.getProgramTrackStatus()
        self.assertIsInstance(self.result, int)

    def test3013_acu_status_getProgramOffsetStatus(self):
        self.logger.debug("getProgramOffsetStatus")
        self.result = self.objref.getProgramOffsetStatus()
        self.assertIsInstance(self.result, int)

    def test3015_acu_status_stowPositionReachedAz(self):
        self.logger.debug("stowPositionReachedAz")
        self.result = self.objref.stowPositionReachedAz()
        self.assertIsInstance(self.result, bool)

    def test3016_acu_status_preLimitSwUpReachedAz(self):
        self.logger.debug("preLimitSwUpReachedAz")
        self.result = self.objref.preLimitSwUpReachedAz()
        self.assertIsInstance(self.result, bool)

    def test3017_acu_status_preLimitSwDownpReachedAz(self):
        self.logger.debug("preLimitSwDownpReachedAz")
        self.result = self.objref.preLimitSwDownpReachedAz()
        self.assertIsInstance(self.result, bool)

    def test3018_acu_status_stowPin1InAz(self):
        self.logger.debug("stowPin1InAz")
        self.result = self.objref.stowPin1InAz()
        self.assertIsInstance(self.result, bool)

    def test3019_acu_status_stowPin1OutAz(self):
        self.logger.debug("stowPin1OutAz")
        self.result = self.objref.stowPin1OutAz()
        self.assertIsInstance(self.result, bool)

    def test3020_acu_status_stowPin1ErrorAz(self):
        self.logger.debug("stowPin1ErrorAz")
        self.result = self.objref.stowPin1ErrorAz()
        self.assertIsInstance(self.result, bool)

    def test3021_acu_status_stowPin1SelectedAz(self):
        self.logger.debug("stowPin1SelectedAz")
        self.result = self.objref.stowPin1SelectedAz()
        self.assertIsInstance(self.result, bool)

    def test3022_acu_status_stowPin2InAz(self):
        self.logger.debug("stowPin2InAz")
        self.result = self.objref.stowPin2InAz()
        self.assertIsInstance(self.result, bool)

    def test3023_acu_status_stowPin2OutAz(self):
        self.logger.debug("stowPin2OutAz")
        self.result = self.objref.stowPin2OutAz()
        self.assertIsInstance(self.result, bool)

    def test3024_acu_status_stowPin2ErrorAz(self):
        self.logger.debug("stowPin2ErrorAz")
        self.result = self.objref.stowPin2ErrorAz()
        self.assertIsInstance(self.result, bool)

    def test3025_acu_status_stowPin2SelectedAz(self):
        self.logger.debug("stowPin2SelectedAz")
        self.result = self.objref.stowPin2SelectedAz()
        self.assertIsInstance(self.result, bool)

    def test3026_acu_status_cmdStowPin1InAz(self):
        self.logger.debug("cmdStowPin1InAz")
        self.result = self.objref.cmdStowPin1InAz()
        self.assertIsInstance(self.result, bool)

    def test3027_acu_status_cmdStowPin1OutAz(self):
        self.logger.debug("cmdStowPin1OutAz")
        self.result = self.objref.cmdStowPin1OutAz()
        self.assertIsInstance(self.result, bool)

    def test3028_acu_status_cmdStowPin2InAz(self):
        self.logger.debug("cmdStowPin2InAz")
        self.result = self.objref.cmdStowPin2InAz()
        self.assertIsInstance(self.result, bool)

    def test3029_acu_status_cmdStowPin2OutAz(self):
        self.logger.debug("cmdStowPin2OutAz")
        self.result = self.objref.cmdStowPin2OutAz()
        self.assertIsInstance(self.result, bool)

    def test3030_acu_status_stowPositionReachedEl(self):
        self.logger.debug("stowPositionReachedEl")
        self.result = self.objref.stowPositionReachedEl()
        self.assertIsInstance(self.result, bool)

    def test3031_acu_status_preLimitSwUpReachedEl(self):
        self.logger.debug("preLimitSwUpReachedEl")
        self.result = self.objref.preLimitSwUpReachedEl()
        self.assertIsInstance(self.result, bool)

    def test3032_acu_status_preLimitSwDownpReachedEl(self):
        self.logger.debug("preLimitSwDownpReachedEl")
        self.result = self.objref.preLimitSwDownpReachedEl()
        self.assertIsInstance(self.result, bool)

    def test3033_acu_status_stowPin1InEl(self):
        self.logger.debug("stowPin1InEl")
        self.result = self.objref.stowPin1InEl()
        self.assertIsInstance(self.result, bool)

    def test3034_acu_status_stowPin1OutEl(self):
        self.logger.debug("stowPin1OutEl")
        self.result = self.objref.stowPin1OutEl()
        self.assertIsInstance(self.result, bool)

    def test3035_acu_status_stowPin1ErrorEl(self):
        self.logger.debug("stowPin1ErrorEl")
        self.result = self.objref.stowPin1ErrorEl()
        self.assertIsInstance(self.result, bool)

    def test3036_acu_status_stowPin1SelectedEl(self):
        self.logger.debug("stowPin1SelectedEl")
        self.result = self.objref.stowPin1SelectedEl()
        self.assertIsInstance(self.result, bool)

    def test3037_acu_status_stowPin2InEl(self):
        self.logger.debug("stowPin2InEl")
        self.result = self.objref.stowPin2InEl()
        self.assertIsInstance(self.result, bool)

    def test3038_acu_status_stowPin2OutEl(self):
        self.logger.debug("stowPin2OutEl")
        self.result = self.objref.stowPin2OutEl()
        self.assertIsInstance(self.result, bool)

    def test3039_acu_status_stowPin2ErrorEl(self):
        self.logger.debug("stowPin2ErrorEl")
        self.result = self.objref.stowPin2ErrorEl()
        self.assertIsInstance(self.result, bool)

    def test3040_acu_status_stowPin2SelectedEl(self):
        self.logger.debug("stowPin2SelectedEl")
        self.result = self.objref.stowPin2SelectedEl()
        self.assertIsInstance(self.result, bool)

    def test3041_acu_status_cmdStowPin1InEl(self):
        self.logger.debug("cmdStowPin1InEl")
        self.result = self.objref.cmdStowPin1InEl()
        self.assertIsInstance(self.result, bool)

    def test3042_acu_status_cmdStowPin1OutEl(self):
        self.logger.debug("cmdStowPin1OutEl")
        self.result = self.objref.cmdStowPin1OutEl()
        self.assertIsInstance(self.result, bool)

    def test3043_acu_status_cmdStowPin2InEl(self):
        self.logger.debug("cmdStowPin2InEl")
        self.result = self.objref.cmdStowPin2InEl()
        self.assertIsInstance(self.result, bool)

    def test3044_acu_status_cmdStowPin2OutEl(self):
        self.logger.debug("cmdStowPin2OutEl")
        self.result = self.objref.cmdStowPin2OutEl()
        self.assertIsInstance(self.result, bool)

    def test3045_acu_status_getRemoteCtl(self):
        self.logger.debug("getRemoteCtl")
        self.result = self.objref.getRemoteCtl()
        self.assertIsInstance(self.result, bool)

    def test3046_acu_status_getAzActive(self):
        self.logger.debug("getAzActive")
        self.result = self.objref.getAzActive()
        self.assertIsInstance(self.result, bool)

    def test3047_acu_status_getElActive(self):
        self.logger.debug("getElActive")
        self.result = self.objref.getElActive()
        self.assertIsInstance(self.result, bool)

    def test3048_acu_status_getM3Active(self):
        self.logger.debug("getM3Active")
        self.result = self.objref.getM3Active()
        self.assertIsInstance(self.result, bool)

    def test3049_acu_status_getM3RActive(self):
        self.logger.debug("getM3RActive")
        self.result = self.objref.getM3RActive()
        self.assertIsInstance(self.result, bool)

    def test3050_acu_status_getM4AActive(self):
        self.logger.debug("getM4AActive")
        self.result = self.objref.getM4AActive()
        self.assertIsInstance(self.result, bool)

    def test3051_acu_status_getM4BActive(self):
        self.logger.debug("getM4BActive")
        self.result = self.objref.getM4BActive()
        self.assertIsInstance(self.result, bool)

    def test3052_acu_status_getTHUActive(self):
        self.logger.debug("getTHUActive")
        self.result = self.objref.getTHUActive()
        self.assertIsInstance(self.result, bool)

    def test3053_acu_status_getGRSActive(self):
        self.logger.debug("getGRSActive")
        self.result = self.objref.getGRSActive()
        self.assertIsInstance(self.result, bool)

    def test3054_acu_status_getHexapodStatus(self):
        self.logger.debug("getHexapodStatus()")
        self.result = self.objref.getHexapodStatus()
        self.assertIsInstance(self.result, tnrtAntennaMod.HexapodStatus)

    def test3055_acu_status_getVertex(self):
        self.logger.debug("getVertex()")
        self.result = self.objref.getVertex()
        self.assertIsInstance(self.result, tnrtAntennaMod.VertexShutterStatus)

    def test3056_acu_status_getTemperatureSensors(self):
        self.logger.debug("getTemperatureSensors()")
        self.result = self.objref.getTemperatureSensors()
        self.assertIsInstance(self.result, list)
        self.assertEqual(len(self.result), 16)

    def test3057_acu_status_stowPositionReachedTHU(self):
        self.logger.debug("stowPositionReachedTHU")
        self.result = self.objref.stowPositionReachedTHU()
        self.assertIsInstance(self.result, bool)


if __name__ == "__main__":
    unittest.main()
