# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
import time
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State6000_PointingCorrections(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test6001_acu_tracking_setPointingModel(self):
        pmodel_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.logger.debug("setPointingModel")
        self.result = self.objref.setPointingModel(pmodel_list)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6002_acu_tracking_setRefractionCorrection(self):
        self.logger.debug("setRefractionCorrection 4,5,6")
        self.result = self.objref.setRefractionCorrection(4, 5, 6)
        self.assertTrue(self.result in [ACCEPTED, DONE])


class State6100_TrackProgramTable(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.activate(AZ)
        self.result = self.objref.activate(EL)
        time.sleep(2)

        current_el = self.objref.getElPosition()
        self.logger.debug("current_el: {}".format(current_el))
        if current_el > 88:
            self.logger.debug("move out of zenith")
            self.objref.absolutePosition(EL, 88, 0.5, False)

        # Define data tables to load
        self.timelist_az_el = (
            Time.now().mjd
            + (np.linspace(15, 64, DataFormatAcu.M1_TRACK_TABLE_LEN_MIN)) / 86400.0
        )
        az_ra_list = np.arange(len(self.timelist_az_el))
        el_dec_list = np.arange(len(self.timelist_az_el)) + 1
        self.table_az_el = []
        for (t, az_ra, el_dec) in zip(self.timelist_az_el, az_ra_list, el_dec_list):
            self.table_az_el.append(
                tnrtAntennaMod.timeAzRaElDecStruct(
                    float(t), float(az_ra), float(el_dec)
                )
            )

        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        # self.result = self.objref.deactivate(AZ)
        # self.result = self.objref.deactivate(EL)
        super().tearDown()

    def test6100_acu_tracking_loadProgramTrackTable(self):
        self.logger.debug(
            "loadProgramTrackTable tnrtAntennaMod.newL, tnrtAntennaMod.NEWTON, tnrtAntennaMod.azel"
        )
        self.result = self.objref.loadProgramTrackTable(
            tnrtAntennaMod.newL,
            tnrtAntennaMod.NEWTON,
            tnrtAntennaMod.azel,
            len(self.table_az_el),
            self.table_az_el,
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6101_acu_tracking_loadProgramTrackTable(self):
        self.logger.debug(
            "loadProgramTrackTable tnrtAntennaMod.newL, tnrtAntennaMod.SPLINE, tnrtAntennaMod.radecshortcut"
        )
        self.result = self.objref.loadProgramTrackTable(
            tnrtAntennaMod.newL,
            tnrtAntennaMod.SPLINE,
            tnrtAntennaMod.radec,
            len(self.table_az_el),
            self.table_az_el,
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6104_acu_tracking_loadProgramOffsetTable(self):
        # #-
        self.logger.debug(
            "loadProgramOffsetTable tnrtAntennaMod.newL, tnrtAntennaMod.NEWTON, tnrtAntennaMod.azel"
        )
        self.result = self.objref.loadProgramOffsetTable(
            tnrtAntennaMod.newL,
            tnrtAntennaMod.NEWTON,
            tnrtAntennaMod.azel,
            len(self.table_az_el),
            self.table_az_el,
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6105_acu_tracking_loadProgramOffsetTable(self):
        self.logger.debug(
            "loadProgramOffsetTable tnrtAntennaMod.newL, tnrtAntennaMod.SPLINE, tnrtAntennaMod.radec"
        )
        self.result = self.objref.loadProgramOffsetTable(
            tnrtAntennaMod.newL,
            tnrtAntennaMod.SPLINE,
            tnrtAntennaMod.radec,
            len(self.table_az_el),
            self.table_az_el,
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6106_acu_tracking_setTrackMode_program_track(self):
        self.logger.debug("setTrackMode tnrtAntennaMod.PROGRAM_TRACK")
        self.result = self.objref.setTrackMode(tnrtAntennaMod.PROGRAM_TRACK)
        self.assertTrue(self.result in [ACCEPTED, DONE])


class State6200_TrackHXPvsTime(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.objref.deactivate(HXP)
        self.objref.reset(HXP)

        self.timelist_hxp = (
            Time.now().mjd
            + (np.linspace(5, 24, DataFormatAcu.HXP_TRACK_TABLE_LEN_MAX)) / 86400.0
        )
        xx = np.arange(len(self.timelist_hxp))
        yy = np.arange(len(self.timelist_hxp)) + 1
        zz = np.arange(len(self.timelist_hxp)) + 2
        txx = np.arange(len(self.timelist_hxp))
        tyy = np.arange(len(self.timelist_hxp)) + 0.01
        tzz = np.arange(len(self.timelist_hxp)) + 0.02
        self.table_hxp = []
        for (t, x, y, z, tx, ty, tz) in zip(
            self.timelist_hxp, xx, yy, zz, txx, tyy, tzz
        ):
            self.table_hxp.append(
                tnrtAntennaMod.timeXYZTXTYTZStruct(
                    float(t),
                    float(x),
                    float(y),
                    float(z),
                    float(tx),
                    float(ty),
                    float(tz),
                )
            )

        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test6201_acu_tracking_loadProgramTrackTableHexapod(self):
        self.logger.debug(
            "loadProgramTrackTableHexapod tnrtAntennaMod.newL, tnrtAntennaMod.NEWTON"
        )
        self.result = self.objref.loadProgramTrackTableHexapod(
            tnrtAntennaMod.newL,
            tnrtAntennaMod.NEWTON,
            len(self.table_hxp),
            self.table_hxp,
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6202_acu_tracking_loadProgramTrackTableHexapod(self):
        self.logger.debug(
            "loadProgramTrackTableHexapod tnrtAntennaMod.newL, tnrtAntennaMod.SPLINE, len={}".format(
                len(self.table_hxp)
            )
        )
        self.result = self.objref.loadProgramTrackTableHexapod(
            tnrtAntennaMod.newL,
            tnrtAntennaMod.SPLINE,
            len(self.table_hxp),
            self.table_hxp,
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])

class State6300_OffsetHXPvsEL(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.objref.deactivate(HXP)
        self.objref.reset(HXP)

        el_list = np.linspace(5, 54, DataFormatAcu.HXP_EL_TABLE_LEN_MAX)
        self.table_el = []
        xx = np.arange(len(el_list))
        yy = np.arange(len(el_list)) + 1
        zz = np.arange(len(el_list)) + 2
        txx = np.arange(len(el_list))
        tyy = np.arange(len(el_list)) + 0.01
        tzz = np.arange(len(el_list)) + 0.02
        for e in el_list:
            self.table_el.append(
                tnrtAntennaMod.elXYZTXTYTZStruct(
                    float(e),
                    float(e) + 10,
                    float(e) + 20,
                    float(e) + 30,
                    float(e) + 40,
                    float(e) + 50,
                    float(e) + 60,
                )
            )

        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test6300_acu_tracking_loadHexapodElevationOffsetTable(self):
        self.logger.debug("loadHexapodElevationOffsetTable")
        self.result = self.objref.loadHexapodElevationOffsetTable(
            len(self.table_el), self.table_el
        )
        self.assertTrue(self.result in [ACCEPTED, DONE])


class State6400_TrackTLE(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test6401_acu_tracking_loadNORAD_GEO(self):
        line0 = "INTELSAT 22 (IS-22)     "
        line1 = "1 38098U 12011A   21161.75480069 -.00000046  00000-0  00000-0 0  9994"
        line2 = "2 38098   0.0032  11.6488 0002732  74.1630 157.3454  1.00271005 33447"
        self.logger.debug(
            "loadNORAD line0: {}, line1: {}, line2: {}".format(line0, line1, line2)
        )
        self.result = self.objref.loadNORAD(line0, line1, line2)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6401_acu_tracking_loadNORAD_LEO(self):
        line0 = "LEMUR-2-SPIROVISION     "
        line1 = "1 42755U 17019E   22185.10451823  .00028765  00000+0  47696-3 0  9995"
        line2 = "2 42755  51.6329 134.5874 0005315 115.4668 333.2990 15.51888648283958"
        self.logger.debug(
            "loadNORAD line0: {}, line1: {}, line2: {}".format(line0, line1, line2)
        )
        self.result = self.objref.loadNORAD(line0, line1, line2)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test6404_acu_tracking_setTrackMode_TLE(self):
        self.logger.debug("setTrackMode, tnrtAntennaMod.NORAD_TLE")
        self.result = self.objref.setTrackMode(tnrtAntennaMod.TLE)
        self.assertTrue(self.result in [ACCEPTED, DONE])


if __name__ == "__main__":
    unittest.main()
