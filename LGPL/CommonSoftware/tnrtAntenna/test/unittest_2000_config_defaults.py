# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

from unittest_common import AcuTestCase

# import TCS modules
import tnrtAntennaMod
import tnrtAntennaError

class Tests(AcuTestCase):
    def setUp(self):
        """
        This function runs 1 time per test_xxx function in this class. before test start
        """
        super().setUp()
        self.result = None

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        self.logger.info(self.result)
        super().tearDown()

    def test2000_config_default_site_location(self):
        self.result = self.objref.getSiteLocation()
        self.assertIsInstance(self.result, tnrtAntennaMod.llh)

    def test2001_config_default_status_host_mock(self):
        self.result = self.objref.get_status_host_mock()
        self.assertIsInstance(self.result, str)

    def test2002_config_default_status_host(self):
        self.result = self.objref.get_status_host()
        self.assertIsInstance(self.result, str)

    def test2003_config_default_status_port(self):
        self.result = self.objref.get_status_port()
        self.assertIsInstance(self.result, int)

    def test2004_config_default_command_host_mock(self):
        self.result = self.objref.get_command_host_mock()
        self.assertIsInstance(self.result, str)

    def test2005_config_default_command_host(self):
        self.result = self.objref.get_command_host()
        self.assertIsInstance(self.result, str)

    def test2006_config_default_command_port(self):
        self.result = self.objref.get_command_port()
        self.assertIsInstance(self.result, int)
    
    def test2010_config_get_velocity_az(self):
        self.result = self.objref.get_velocity(tnrtAntennaMod.AZ)
    
    def test2012_config_get_velocity_el(self):
        self.result = self.objref.get_velocity(tnrtAntennaMod.EL)

    def test2014_config_set_velocity_az(self):
        self.result = self.objref.set_velocity(tnrtAntennaMod.AZ, 1.0)
        self.assertEqual(self.objref.get_velocity(tnrtAntennaMod.AZ), 1.0)

    def test2016_config_set_velocity_el(self):
        self.result = self.objref.set_velocity(tnrtAntennaMod.EL, 0.5)
        self.assertEqual(self.objref.get_velocity(tnrtAntennaMod.EL), 0.5)

    def test2018_config_set_velocity_az_over(self):
        with self.assertRaises(tnrtAntennaError.paramOutOfLimitsEx):
            self.result = self.objref.set_velocity(tnrtAntennaMod.AZ, 5.0)

    def test2020_config_set_velocity_az_under(self):
        with self.assertRaises(tnrtAntennaError.paramOutOfLimitsEx):
            self.result = self.objref.set_velocity(tnrtAntennaMod.AZ, 0.000001)
    
    def test2022_config_set_velocity_el_over(self):
        with self.assertRaises(tnrtAntennaError.paramOutOfLimitsEx):
            self.result = self.objref.set_velocity(tnrtAntennaMod.EL, 4.0)

    def test2024_config_set_velocity_el_under(self):
        with self.assertRaises(tnrtAntennaError.paramOutOfLimitsEx):
            self.result = self.objref.set_velocity(tnrtAntennaMod.EL, 0.000001)


if __name__ == "__main__":
    unittest.main()
