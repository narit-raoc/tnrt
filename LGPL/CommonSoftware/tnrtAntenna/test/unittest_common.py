# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

# import standard modules
import unittest
import logging
import coloredlogs
from astropy.time import Time

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
import tnrtAntennaMod
import tnrtAntennaErrorImpl
import tnrtAntennaError


class ColoredTestCase(unittest.TestCase):
    def setUp(self):
        # Configure logger
        self.logger = logging.getLogger("ColoredTestCase")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(funcName)s(): %(message)s"

        level_styles_scrn = {
            "critical": {"color": "red", "bold": True},
            "debug": {"color": "white", "faint": True},
            "error": {"color": "red"},
            "info": {"color": "green", "bright": True},
            "notice": {"color": "magenta"},
            "spam": {"color": "green", "faint": True},
            "success": {"color": "green", "bold": True},
            "verbose": {"color": "blue"},
            "warning": {"color": "yellow", "bright": True, "bold": True},
        }
        field_styles_scrn = {
            "asctime": {},
            "hostname": {"color": "magenta"},
            "levelname": {"color": "cyan", "bright": True},
            "name": {"color": "blue", "bright": True},
            "programname": {"color": "cyan"},
        }
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)


class AcuTestCase(ColoredTestCase):
    """
    Test case that has colored logs and connection to ACS Component TNRTANTANNA
    ACS Component has a socket client connection to ACU
    """

    def setUp(self):
        # Use parent setup function to configure the logger
        super().setUp()

        # Connect ACS component
        # self.logger.debug("Starting PySimpleClient")
        self.sc = PySimpleClient()

        self.component_name = "AntennaTNRT"
        # Get reference to ComponentA.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, This command will activate and create a new instance.
        try:
            # self.logger.debug("Connecting to ACS component {}".format(self.component_name))
            self.objref = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error(
                "Cannot get ACS component object reference for {}".format(
                    self.component_name
                )
            )
            self.tearDown()

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        try:
            # self.logger.debug("Releasing ACS component %s" % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.objref = None
        except Exception as e:
            self.logger.error(
                "Cannot release component reference for %s" % self.component_name
            )

        try:
            # Remove logging handlers from logger in PySimpleClient to reduce clutter
            # We are going to disconnect and delete the client, so don't care about last log messages
            self.sc.logger.handlers = []
            self.sc.disconnect()
            del self.sc
        except AttributeError as e:
            self.logger.error(e)


class AcuRemoteCommandTestCase(AcuTestCase):
    """
    Test case that has colored logs and connection to ACS Component TNRTANTANNA
    ACS Component has a socket client connection to ACU
    """

    def setUp(self):
        # Use parent setup function to configure the logger
        super().setUp()

        # Set ACU to be in remote control mode
        result = self.objref.setMaster(tnrtAntennaMod.REMOTE_M)

        # Set the clock to use absolute time. same as this test computer.
        # If we don't set the time source, it remains at MJD=0 (year 1858)
        result = self.objref.timeSource(tnrtAntennaMod.ABSOLUTE, float(Time.now().mjd))

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        # Set ACU to be in local control mode.  This will also deactivate all axes
        # result = self.objref.setMaster(tnrtAntennaMod.LOCAL_M)

        super().tearDown()
