# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.07.06

import logging
import numpy as np
import time
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State8100_AZ_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.activate(AZ)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(AZ)
        super().tearDown()

    def test8110_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow AZ stowPosition1")
        self.result = self.objref.driveToStow(AZ, tnrtAntennaMod.stowPosition1, 2.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8120_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow AZ stowPosition2")
        self.result = self.objref.driveToStow(AZ, tnrtAntennaMod.stowPosition2, 2.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8130_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow AZ stowPositionNearest")
        self.result = self.objref.driveToStow(
            AZ, tnrtAntennaMod.stowPositionNearest, 2.0
        )
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8140_acu_axis_mode_drivetopark(self):
        self.logger.debug("driveToPark AZ 1.0")
        self.result = self.objref.driveToPark(AZ, 1.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State8200_AZ_Active_StowPosition(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()

        # Move AZ and EL to park position to prepare for STOW commands
        self.result = self.objref.activate(AZ)

        if self.objref.stowPositionReachedAz() is False:
            self.result = self.objref.driveToStow(AZ, tnrtAntennaMod.stowPosition1, 2.0)
            self.objref.activate(AZ)

        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        # Unstow all stow pins to prepare for the next test
        self.result = self.objref.unstow(AZ, tnrtAntennaMod.stowpin1)
        super().tearDown()

    def test8210_acu_axis_mode_stopstow(self):
        self.logger.debug("stopstow AZ pin1")
        self.result = self.objref.stopstow(AZ, tnrtAntennaMod.stowpin1)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8220_acu_axis_mode_stow(self):
        self.logger.debug("stow AZ pin1")
        self.result = self.objref.stow(AZ, tnrtAntennaMod.stowpin1)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8230_acu_axis_mode_unstow(self):
        self.logger.debug("unstow AZ pin1")
        self.result = self.objref.unstow(AZ, tnrtAntennaMod.stowpin1)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State8300_EL_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()

        # THU must be inactive before EL can move
        self.objref.deactivate(THU)

        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self.result = self.objref.activate(EL)
        # Note - if the axis was deactivated, then activate in unittest setUp(),
        # and then call next function immediately, it responds WRONG_MODE.
        # Looks like the
        # ACU status bits indicate too early that EL axis is active (tnrtAntenna.py flow control)
        # when it is actually not 100% active (enough for a stop command)
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(EL)

        current_el = self.objref.getElPosition()
        if current_el > 88:
            self.objref.absolutePosition(EL, 88, 0.5, False)

        super().tearDown()

    def test8310_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow EL stowPosition1")
        self.result = self.objref.driveToStow(EL, tnrtAntennaMod.stowPosition1, 1.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8320_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow EL stowPositionNearest")
        self.result = self.objref.driveToStow(
            EL, tnrtAntennaMod.stowPositionNearest, 1.0
        )
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8330_acu_axis_mode_drivetopark(self):
        self.logger.debug("driveToPark EL 0.5")
        self.result = self.objref.driveToPark(EL, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State8400_EL_Active_StowPosition(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()

        # THU must be inactive before EL can move
        self.objref.deactivate(THU)

        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)

        # Move EL to stow position to prepare for STOW commands
        self.result = self.objref.activate(EL)
        if self.objref.stowPositionReachedEl() is False:
            self.result = self.objref.driveToStow(EL, tnrtAntennaMod.stowPosition1, 1.0)
            result = self.objref.activate(EL)
            # Even after ACU indicates bit in status message that stow position
            # is reached, it is not yet ready to accept the stow command (responds
            # WRONG_MODE).  Wait 1 second and command will be ACCEPTED
            time.sleep(2)
            # Unstow all stow pins to prepare for the next test
            self.result = self.objref.unstow(EL, tnrtAntennaMod.stowpinBoth)
            time.sleep(2)

        self.result = None

    def tearDown(self):
        self.logger.info(self.result)

        current_el = self.objref.getElPosition()
        self.logger.debug("current_el: {}".format(current_el))
        if current_el > 88:
            self.objref.absolutePosition(EL, 88, 0.5, False)

        super().tearDown()

    def test8410_acu_axis_mode_stow(self):
        self.logger.debug("stow EL pinBoth")
        self.result = self.objref.stow(EL, tnrtAntennaMod.stowpinBoth)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8420_acu_axis_mode_unstow(self):
        self.logger.debug("unstow EL pinBoth")
        self.result = self.objref.unstow(EL, tnrtAntennaMod.stowpinBoth)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8430_acu_axis_mode_stopstow(self):
        self.logger.debug("stopstow EL pin both")
        # Call stow function first to start the stow action before we call stop
        self.result = self.objref.stow(EL, tnrtAntennaMod.stowpinBoth)
        self.result = self.objref.stopstow(EL, tnrtAntennaMod.stowpinBoth)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State8500_THU_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()

        # Set EL up to zenith (> 85 degrees).  EL must be in this "up"
        # postion before we can rotate the THU.  If not, the cable tray will be
        # destroyed.
        self.objref.absolutePosition(EL, 88, 1, False)

        # TODO: THU doesn't care where the AZ is, but the flow control function
        # tnrtAntenna.blockUntilTracking doesn't work correctly if AZ has been reset.
        # (because desired position is not equal to encoder position)
        # Therefore set the AZ desired position to the same as the current encoder
        # here to make the flow control work until it is improved.
        self.objref.absolutePosition(AZ, self.objref.getAzPosition(), 2, False)
        self.objref.blockUntilTracking(2.0)

        self.result = self.objref.activate(THU)
        # Activate is a blocking function, but the status bit arrives active before it is actually
        # ready to receive the command in active mode.  perhaps an artifact of simulator or an issue
        # in the real ACU too.  From experience on ACU simulator test, we need approximately 2 more
        # seconds to guarantee the next command works while fully active.
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(THU)
        self.result = self.objref.unstow(THU, tnrtAntennaMod.stowpinBoth)
        super().tearDown()

    def test8510_acu_axis_mode_drivetopark(self):
        self.logger.debug("driveToPark THU 0.5")
        self.result = self.objref.driveToPark(THU, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8520_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow THU stowPositionNearest")
        self.result = self.objref.driveToStow(
            THU, tnrtAntennaMod.stowPositionNearest, 0.5
        )
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8530_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow THU stowPosition1")
        self.result = self.objref.driveToStow(THU, tnrtAntennaMod.stowPosition1, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8540_acu_axis_mode_drivetostow(self):
        self.logger.debug("driveToStow THU stowPosition2")
        self.result = self.objref.driveToStow(THU, tnrtAntennaMod.stowPosition2, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State8600_THU_StowPosition(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()

        # Set EL up to zenith (> 85 degrees).  EL must be in this "up"
        # postion before we can rotate the THU.  If not, the cable tray will be
        # destroyed.
        self.objref.absolutePosition(EL, 88, 1, False)

        # TODO: THU doesn't care where the AZ is, but the flow control function
        # tnrtAntenna.blockUntilTracking doesn't work correctly if AZ has been reset.
        # (because desired position is not equal to encoder position)
        # Therefore set the AZ desired position to the same as the current encoder
        # here to make the flow control work until it is improved.
        self.objref.absolutePosition(AZ, self.objref.getAzPosition(), 2, False)
        self.objref.blockUntilTracking(2.0)

        # Move THU to stow position to prepare for STOW commands
        self.result = self.objref.activate(THU)
        if self.objref.stowPositionReachedTHU() is False:
            self.result = self.objref.driveToStow(
                THU, tnrtAntennaMod.stowPosition1, 0.5
            )
            result = self.objref.activate(THU)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        # Unstow all stow pins to prepare for the next test
        self.result = self.objref.unstow(THU, tnrtAntennaMod.stowpinBoth)
        super().tearDown()

    def test8610_acu_axis_mode_stow(self):
        self.logger.debug("unstow THU pinBoth")
        self.result = self.objref.stow(THU, tnrtAntennaMod.stowpinBoth)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test8620_acu_axis_mode_unstow(self):
        self.logger.debug("unstow THU pinBoth")
        self.result = self.objref.unstow(THU, tnrtAntennaMod.stowpinBoth)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
