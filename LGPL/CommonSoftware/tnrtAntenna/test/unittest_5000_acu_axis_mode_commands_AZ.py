# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
import time
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5000_AZ_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()

        # Deactivate axes and clear errors from previous test
        self.result = self.objref.deactivate(AZ)
        self.result = self.objref.reset(AZ)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.unstow(AZ, tnrtAntennaMod.stowpin1)
        super().tearDown()

    def test5001_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate AZ")
        self.result = self.objref.deactivate(AZ)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5002_acu_axis_mode_activate(self):
        self.logger.debug("activate AZ")
        self.result = self.objref.activate(AZ)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5003_acu_axis_mode_reset(self):
        self.logger.debug("reset AZ")
        self.result = self.objref.reset(AZ)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5004_acu_axis_mode_drivetopark(self):
        self.logger.debug("driveToPark AZ 1.0")
        self.result = self.objref.driveToPark(AZ, 1.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5050_AZ_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.activate(AZ)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(AZ)
        super().tearDown()

    def test5051_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate AZ")
        self.result = self.objref.deactivate(AZ)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5052_acu_axis_mode_stop(self):
        self.logger.debug("stop AZ")
        self.result = self.objref.stop(AZ)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5053_acu_axis_mode_absolute_position(self):
        self.logger.debug("absolutePosition AZ 0 0.2")
        self.result = self.objref.absolutePosition(AZ, 0, 0.2, False)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5054_acu_axis_mode_relative_position(self):
        self.logger.debug("relativePosition AZ 0 0.2")
        self.result = self.objref.relativePosition(AZ, 0, 0.2)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5055_acu_axis_mode_slew(self):
        self.logger.debug("slew AZ 2.0")
        self.result = self.objref.slew(AZ, 0.2)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5056_acu_axis_mode_slew(self):
        self.logger.debug("slew AZ -0.2")
        self.result = self.objref.slew(AZ, -0.2)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5057_acu_axis_mode_programTrack_AZ(self):
        self.logger.debug("programTrack AZ 1.0")
        self.result = self.objref.programTrack(AZ, 1.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5062_acu_axis_mode_predefined_position(self):
        self.logger.debug("predefinePosition AZ 1 1.0")
        self.result = self.objref.predefinePosition(AZ, tnrtAntennaMod.PresetPos1, 1.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5063_acu_parameter_positionOffset_AZ(self):
        self.logger.debug("positionOffset AZ 2.0")
        self.result = self.objref.positionOffset(AZ, 2.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5064_acu_parameter_positionOffset_AZ(self):
        self.logger.debug("positionOffset AZ 0")
        self.result = self.objref.positionOffset(AZ, 0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

if __name__ == "__main__":
    unittest.main()
