#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from Acspy.Nc.Supplier import Supplier
from Acspy.Nc.Consumer import Consumer
from EPM1913A import EPM1913A
import ACSErrTypeCommonImpl
import PowerMeterMod__POA
import PowerMeterMod
import PowerMeterErrorImpl
import PowerMeterError
import threading
import ctypes 
import time 
import astropy.units as units
from astropy.time import Time

class PowerMeter(PowerMeterMod__POA.PowerMeter, ACSComponent, ContainerServices, ComponentLifecycle):
	# Constructor
	def __init__(self):
		ACSComponent.__init__(self)
		ContainerServices.__init__(self)
		self.logger = self.getLogger()
		self.mainNotificationSuppInstance = None
		self.RecordThread = None
		self.RecordThreadFlag = False
		self.EPM = EPM1913A()
		self.EPM_isconnected = False
		
	#-----------------------------------------------------------------------------------
	# LifeCycle functions called by ACS system when we activate and deactivate components. 
	# Inherited from headers in Acspy.Servants.ComponentLifecycle.   
	def initialize(self):
		self.logger.logDebug("Power Meter initialize() finished")
		
	def execute(self):
		self.mainNotificationSuppInstance = Supplier(PowerMeterMod.MAINNOTIFICATIONCHANNEL)
		self.logger.logInfo("Power Meter execute() finished")
		
	def aboutToAbort(self):
		self.EPM = None
		self.RecordThread = None
		if self.mainNotificationSuppInstance != None:
			self.mainNotificationSuppInstance.disconnect()	
		self.stopRecorder()
		
	def cleanUp(self):
		if self.RecordThreadFlag:
			self.RecordThreadFlag = False

		if self.mainNotificationSuppInstance != None:
			self.mainNotificationSuppInstance.disconnect()	

	#------------------------------------------------------------------------------------
	# Functions defined in IDL to interface from outside this class	
	def startRecorder(self):
		if self.EPM_isconnected is True :
			if self.RecordThreadFlag is False:
				self.RecordThread = Record(self)
				self.RecordThread.start()
				self.RecordThreadFlag = True
				return "START EPM recroder"
			else:
				return "EPM recorder still recording"
		else:
			self.logger.error("EPM1913A : Power meter is not connect")

	def stopRecorder(self):
		if self.RecordThreadFlag is True:
			self.RecordThread.raise_exception()
			self.RecordThread._running = False
			self.RecordThread.join()
			self.RecordThread = None
			self.RecordThreadFlag = False
			return "STOP EPM recroder"
		else:
			return "EPM recorder still not recording"

	def connect(self):	
		try:
			res = self.EPM.Connect()		# Connect to EPM1913A with IP address  
			res = str(res) 
			self.EPM_isconnected = True
			return res
		except Exception as e:
			self.logger.error("Error fail to connect Power meter EPM1913A : " + str(e))

	def identify(self):
		try: 
			return self.EPM.IDN()				# Queries the identification of the EPM Series power meter and checks whether you are communicating with the right EPM Series power meter
		except Exception as e:
			self.logger.error("Error fail to connect Power meter EPM1913A : " + str(e))

	def preset(self):
		res = self.EPM.Preset()
		self.logger.logInfo(str(res))
		return str(res)

	def checkEPMError(self):
		try:
			return self.EPM.CheckError()		# Checks the EPM Series power meter system error queue.  
		except Exception as e:
			self.logger.error("Error fail to connect Power meter EPM1913A : " + str(e))

	def autoAveragingMode(self, cmd):
		try: 
			return str(self.EPM.AutoAveragingMode(cmd))
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))

	def checkAutoAveragingMode(self):
		try: 
			return str(self.EPM.CheckAutoAveragingMode())						# Still need to calibration?, becasues calibration must do front of power meter equ 
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))

	def query(self, cmd):
		res = self.EPM.inst.query(cmd)
		return str(res)

	def write(self, cmd):
		res = self.EPM.inst.write(cmd)
		return str(res)
	#------------------------------------------------------------------------------------
	# Internal functions not exposed in IDL	
	def calibration(self):
		# Need to calibrate in front of Power meter
		pass

	def test_MEAS(self):
		try:
			res = self.EPM.MeasureMEASMode() 			
			self.logger.logInfo(str(res))
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))
	
	def test_FETC(self):
		try:
			res = self.EPM.MeasureFETCMode()
			self.logger.logInfo(str(res))
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))

	def test_sigle_FETC(self):
		try:
			res = self.EPM.SingleTringger_ModeFETC()
			self.logger.logInfo(str(res))
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))

	def test_DutyCycle_Correction(self):
		try:	
			res = self.EPM.PulsedRFpowerMeasure()
			self.logger.logInfo(str(res))
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))

	def test_CWPowerMeFETC(self):
		try:
			res = self.EPM.CWPowerMeasureFETC_FreeRunMode()
			self.logger.logInfo(str(res))
		except Exception as e:
			self.logger.error("Error Power meter EPM1913A : " + str(e))
		
	
class Record(threading.Thread, ContainerServices):

	def __init__(self,PowerMeter):
		threading.Thread.__init__(self)
		self.dataPower = 0
		self.PowerMeter = PowerMeter
		self._running = True
		self.notifyThread = None
		self.notifyThreadFlag = False

	def run(self):  
		# target function of the thread class 
		try: 
			while self._running: 
				# 1. read data from power meter
				self.dataPower = self.PowerMeter.EPM.MeasureFETCMode()
				# 2. start EPM recorder
				# self.dataPower += 0.1
				time.sleep(1)
				# 3. start notifi cationChannel with if condition when notifyThreadFlag is False cause next loop will not start again
				if self.notifyThreadFlag is False:
					self.startNotifyThread()
			
			# 4. stop the notify channel when self._running is False
			self.stopNotifyThread()	
			
		except Exception as e:
			print(e)
			
	def startNotifyThread(self):
		self.notifyThreadFlag = True
		self.notifyThread = notificationChannelThread(self)
		self.notifyThread.start()
		print("Start recorder")
		return True
	
	def stopNotifyThread(self):
		self.notifyThreadFlag = False
		self.notifyThread.raise_exception()
		self.notifyThread._running = False
		self.notifyThread.join()
		print("Stop recorder")
		return True

	def get_id(self): 
		# returns id of the respective thread 
		if hasattr(self, '_thread_id'): 
			return self._thread_id 
		for id, thread in threading._active.items(): 
			if thread is self: 
				return id

	def raise_exception(self): 
		thread_id = self.get_id()
		res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
			  ctypes.py_object(SystemExit)) 
		if res > 1: 
			ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0) 
			print('Exception raise failure')	


class notificationChannelThread(threading.Thread, ContainerServices):

	def __init__(self, Record):
		threading.Thread.__init__(self)
		self.__debug = 1
		self.Record = Record
		self.logger = self.Record.PowerMeter.logger
		self.PowerMeter = self.Record.PowerMeter
		self.tolerance = 8
		self._running = True
		self.sequence_counter = 0

	def run(self):
		self.logger.logInfo("Entering notificationChannelThread.run()")
		try:
			while self._running:
				data = self.Record.dataPower
				self.sequence_counter += 1
				powerValue = float(data)
				powerTimestamp = Time.now().mjd
				time.sleep(1.5)   
				self.logger.logInfo("")
				self.logger.logInfo("PowerValue : " + data + " TimeStamp : " + str(powerTimestamp))      
				self.logger.logInfo("")
				eventToPublish = PowerMeterMod.mainPowerMeterNotifyBlock(self.sequence_counter,powerValue,powerTimestamp)
				self.PowerMeter.mainNotificationSuppInstance.publishEvent(eventToPublish)

		except Exception as e:
			self.logger.logInfo("Error notification thread  : " + str(e))
			

		self.logger.logInfo("Notification stopped")
		  
	def get_id(self): 
		# returns id of the respective thread 
		if hasattr(self, '_thread_id'): 
			return self._thread_id 
		for id, thread in threading._active.items(): 
			if thread is self: 
				return id

	def raise_exception(self): 
		thread_id = self.get_id() 
		res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
			  ctypes.py_object(SystemExit)) 
		if res > 1: 
			ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0) 
			self.logger.logInfo('Exception raise failure')

# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
	self = PowerMeter()
