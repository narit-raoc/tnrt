/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2002
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 *
 * ========================================================================
 * == THIS IS GENERATED CODE!!! DO NOT MODIFY! ALL CHANGES WILL BE LOST! ==
 * ======================================================================== 
 *
 */
 
package alma.PowerMeterError.wrappers;

import alma.ACSErr.Completion;
import alma.acs.exceptions.*;
import alma.PowerMeterError.wrappers.AcsJexampleErrorEx;

public class exampleErrorAcsJCompletion extends AcsJCompletion
{
	/**
	 * Creates a new <code>exampleErrorAcsJCompletion</code>
	 * with a corresponding exception (AcsJexampleErrorEx) attached.
	 */
	public exampleErrorAcsJCompletion () {
		super(new AcsJexampleErrorEx()); 
	}

	/**
	 * Creates a new <code>exampleErrorAcsJCompletion</code>
	 * with a corresponding exception (AcsJexampleErrorEx) attached 
	 * that wraps an existing exception (<code>acsJEx</code>.)
	 */
	public exampleErrorAcsJCompletion (AcsJException acsJEx) {
		super(new AcsJexampleErrorEx(acsJEx));  
	}
	
	/**
	 * Creates a new <code>exampleErrorAcsJCompletion</code>
	 * from another <code>AcsJCompletion</code> (<code>acsJComp</code>).
	 * <p>
	 * If present, the existing error trace is converted to Java exceptions
	 * and wrapped with an <code>AcsJexampleErrorEx</code>.
	 */
	public exampleErrorAcsJCompletion(AcsJCompletion acsJComp) {		
			init(new AcsJexampleErrorEx(acsJComp.getAcsJException()));
	}
	
	/**
	 * Converts a Corba completion to an <code>exampleErrorAcsJCompletion</code>
	 * such that a new <code>AcsJexampleErrorEx</code> is created as the attached error.
	 * If <code>corbaComp</code> carries error information, these <code>ErrorTrace</code>
	 * objects are converted to Java exceptions, which are attached as the course of
	 * the new <code>AcsJexampleErrorEx</code>.
	 * @param corbaComp
	 */
	public exampleErrorAcsJCompletion(Completion corbaComp) {
		
		 this(AcsJCompletion.fromCorbaCompletion(corbaComp));
	}
	
       /**
        * Returns the short description of the error
        */
	public String getShortDescription() 
	{
	   return "short description of exampleError";
	}	

	/////////////////////////////////////////////////////////////
	// Getter/Setter for members
	/////////////////////////////////////////////////////////////	

}
