/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2002
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 * 
 * ========================================================================
 * == THIS IS GENERATED CODE!!! DO NOT MODIFY! ALL CHANGES WILL BE LOST! ==
 * ======================================================================== 
 */
package alma.PowerMeterError.wrappers;

import alma.acs.exceptions.*;
//import alma.ACSErr.ACSErrType;
import alma.ACSErr.ErrorTrace;
import alma.PowerMeterError.PowerMeterErrorEx;


/**
 * Java native style exception class representing the error type 
 * <code>alma.ACSErr.ACSErrType.ACSErrTypeTest</code>.    
 * <p> 
 * This class is abstract, so that individual classes for each error code
 * within the given error type can be generated as subclasses.
 * They are interoperable with {@link alma.ACSErr.ErrorTrace}.
 * The purpose is to bring back to Java a limited hierarchy of exceptions, which can
 * only be mimicked in CORBA by using a twofold number scheme with 
 * "type" and "code".
 * <p> 
 * @author ACS Error System Code Generator
 * created Sep 25, 2003 4:18:09 PM
 */
public abstract class AcsJPowerMeterErrorEx extends AcsJException
{

	/////////////////////////////////////////////////////////////
	// Implementation of Constructors from AcsJException
	// most of which are derived from java.lang.Throwable
	/////////////////////////////////////////////////////////////

	public AcsJPowerMeterErrorEx()
	{
		super();
	}

        /**
          * @deprecated The data in <code>message</code> should be given as parameters instead!
          */
	public AcsJPowerMeterErrorEx(String message)
	{
		super(message);
	}

        /**
         * @deprecated The data in <code>message</code> should be given as parameters instead!
         */
	public AcsJPowerMeterErrorEx(String message, Throwable cause)
	{
		super(message, cause);
	}

	public AcsJPowerMeterErrorEx(Throwable cause)
	{
		super(null, cause);
	}

	public AcsJPowerMeterErrorEx(ErrorTrace etCause)
	{
		super(etCause);
	}

        /**
         * @deprecated The data in <code>message</code> should be given as parameters instead!
         */
	public AcsJPowerMeterErrorEx(String message, ErrorTrace etCause)
	{
		super(message, etCause);
	}

	/////////////////////////////////////////////////////////////
	// Mapping of Error Type 
	/////////////////////////////////////////////////////////////

	/**
	 * Returns the error type, which is fixed to 
	 * <code>ACSErrType.ACSErrTypeTest</code>. 
	 * 
	 * @see alma.acs.exceptions.AcsJException#getErrorType()
	 */
	protected final int getErrorType()
	{
		return alma.ACSErr.PowerMeterError.value;
	}

	/**
	 * Returns the CORBA exception that corresponds to the error type
	 * (and not to any particular error code).
	 * Note that if CORBA exceptions would support inheritance,
	 * the error code-specific CORBA exceptions should inherit from this one,	 
	 * which unfortunately they can't.
	 */
	public PowerMeterErrorEx toPowerMeterErrorEx()
	{
		ErrorTrace et = getErrorTrace(); 
		PowerMeterErrorEx acsEx = new PowerMeterErrorEx(et);
		return acsEx;
	}

}  