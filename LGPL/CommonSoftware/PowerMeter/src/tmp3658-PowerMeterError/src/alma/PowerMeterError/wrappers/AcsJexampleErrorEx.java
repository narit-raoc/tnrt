/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2002
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 * 
 * ========================================================================
 * == THIS IS GENERATED CODE!!! DO NOT MODIFY! ALL CHANGES WILL BE LOST! ==
 * ======================================================================== 
 */
 
package alma.PowerMeterError.wrappers;

import org.omg.CORBA.UserException; 
import alma.acs.exceptions.*;

import alma.ACSErr.ErrorTrace;
import alma.PowerMeterError.exampleError;
import alma.PowerMeterError.exampleErrorEx;

/**
 * Java native style exception class representing the error type 
 * <code>alma.ACSErr.ACSErrType.PowerMeterError</code>,
 * error code <code>exampleError.value</code>.     
 * 
 * @see AcsJPowerMeterErrorEx
 * 
 * @author ACS Error System Code Generator
 * created Sep 25, 2003 4:18:09 PM
 */
public class AcsJexampleErrorEx extends AcsJPowerMeterErrorEx
{

	public AcsJexampleErrorEx()
	{
		super();
	}
	
        /**
         * @deprecated The data in <code>message</code> should be given as parameters instead!
         */
	public AcsJexampleErrorEx(String message)
	{
		super(message);
	}

        /**
         * @deprecated The data in <code>message</code> should be given as parameters instead!
         */
	public AcsJexampleErrorEx(String message, Throwable cause)
	{
		super(message, cause);
	}

	public AcsJexampleErrorEx(Throwable cause)
	{
		super(null, cause);
	}

	public AcsJexampleErrorEx(ErrorTrace etCause)
	{
		super(etCause);
	}

        /**
         * @deprecated The data in <code>message</code> should be given as parameters instead!
         */
	public AcsJexampleErrorEx(String message, ErrorTrace etCause)
	{
		super(message, etCause);
	}


	/////////////////////////////////////////////////////////////
	// Code specific methods
	/////////////////////////////////////////////////////////////
  
       /**
        * Returns the short description of the error
        */
	public String getShortDescription() 
	{
	   return "short description of exampleError";
	}
    
	/**
	 * Returns the error code, which is fixed to <code>0</code>, given by
	 * <code>exampleError.value</code>. 
	 * 
	 * @see alma.acs.exceptions.AcsJException#getErrorCode()
	 */ 
	protected final int getErrorCode()
	{
		return exampleError.value;
	 }

	/**
	 * @see alma.acs.exceptions.AcsJException#toCorbaException()
	 */
	public UserException toCorbaException()
	{
		return toexampleErrorEx();
	}
	
	/**
	 * Creates an <code>exampleError</code> that represents this exception
	 * with all its caused-by child exceptions.
	 * <p>
	 * Typically to be called from a top-level catch block that must
	 * convert any of the Java exceptions used internally by the Java program
	 * to an IDL type exception that can be thrown on over CORBA.
	 *   
	 * @return the type-safe subclass of <code>org.omg.CORBA.UserException</code> 
	 * 			with an embedded <code>ErrorTrace</code>.
	 * @see #toCorbaException
	 */
	public exampleErrorEx toexampleErrorEx()
	{
		ErrorTrace et = getErrorTrace(); 
		exampleErrorEx acsEx = new exampleErrorEx(et);
		return acsEx;
	}
	
	
	/**
	 * Converts a CORBA <code>exampleError</code> to an instance of this class.
	 * <p>
	 * Note that unlike the constructor {@link #AcsJErrTest0Ex(ErrorTrace)}, 
	 * this static conversion method
	 * does not wrap the existing chain of exceptions with a new exception. 
	 * It simply converts all exceptions found in the <code>ErrorTrace<code>
	 * of <code>corbaEx</code> to the corresponding Java exceptions, knowing that 
	 * the top level exception is of type <code>exampleError</code> and can 
	 * always be converted to <code>AcsJexampleErrorEx</code>.
	 * <p>
	 * Here's an example of how to use this method in a Java program that 
	 * makes a call so some other component etc.:
	 * <pre>
	 * private void methodThatMakesARemoteCall() throws AcsJexampleErrorEx
	* {
	 *   try
	 *   {
	 *     // this fakes the remote call to a method
	 *     // which can throw an exampleErrorEx...
	 *     throw new exampleErrorEx();
	 *   }
	 *   catch (exampleErrorEx corbaEx)
	 *   {
	 *     throw AcsJexampleErrorEx.fromexampleErrorEx(corbaEx);
	 *   }
	 * } 
	 * </pre> 
	 * @param corbaEx the CORBA equivalent of this class; will be converted
	 * @return  the newly created instance, with data fields and caused-by exceptions 
	 * 			converted from <code>corbaEx</code>. 
	 */
	public static AcsJexampleErrorEx fromexampleErrorEx(exampleErrorEx corbaEx)
	{
		ErrorTrace et = corbaEx.errorTrace;
		
		String message = ErrorTraceManipulator.getProperty(
			et, CorbaExceptionConverter.PROPERTY_JAVAEXCEPTION_MESSAGE);
		AcsJexampleErrorEx jEx = new AcsJexampleErrorEx(message);
		
		CorbaExceptionConverter.convertErrorTraceToJavaException(et, jEx);
		
		return jEx;
	}
	
	/////////////////////////////////////////////////////////////
	// Getter/Setter for members
	/////////////////////////////////////////////////////////////	

} 
