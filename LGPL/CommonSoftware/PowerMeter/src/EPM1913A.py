import pyvisa
from time import sleep
from datetime import datetime


class EPM1913A:
	
	def __init__(self):	
		## Connect VISA
		self.rm = pyvisa.ResourceManager('@py')
		self.today = datetime.now()
		self.inst = None

	def Connect(self):
		# Based on the resource manager, open a session to a specific VISA resource string as provided via
		# ALTER LINE BELOW - Updated VISA resource string to match your specific configuration

		# from ip can make a text block for fill ip?
		self.inst = self.rm.open_resource('TCPIP0::169.254.94.193::inst0::INSTR')
		#Set Timeout - 10 seconds
		self.inst.timeout = 10000
		
		return self.inst
			
	def Disconnect(self):
		self.inst.clear()
		self.inst.close()

	def IDN(self):
		'''
		Queries the identification of the EPM Series power meter and checks whether you are communicating with the right EPM Series power meter
		'''
		r = self.inst.query("*IDN?")
		r = str(r)
		return r
		
	def CheckError(self):
		'''
		Checks the EPM Series power meter system error queue.
		'''
		r = self.inst.query("SYST:ERR?")
		r = str(r)
		return r
	
	def CheckAutoAveragingMode(self):
		return self.inst.query("AVER:COUN:AUTO?")

	def AutoAveragingMode(self,cmd):
		return self.inst.write("AVERage:COUNt:AUTO " + cmd)

	def Clear(self):
		r = self.inst.write("*CLS")      # Clear all status registers
		return "Clear all status registers"
	
	def OPC(self):
		return self.inst.query("*OPC?")
	
	def Preset(self):
		self.inst.write("SYST:PRES")
		if (int(self.OPC()) == 1):
			return "Preset complete"
		else : 
			return "Preset error"


	# --------------------------------------------------------------------------------------------
	# measurement mode
	'''
	There are three different ways to query the power measurlocement using FETC?, MEAS?, and READ?.
	In Free Run or Continuous mode, you can use either FETC? or MEAS? to query the power measurement.
	''' 
	def MeasureFETCMode(self):
		# Queries the measurement results from the buffer.
		self.inst.write("INIT:CONT ON")    # Sets the meter to Free Run mode.
		return self.inst.query("FETC?")

	def MeasureMEASMode(self):
		# Reads the measurement results. MEAS? is equivalent to CONF followed by a READ?.
		self.inst.write("INIT:CONT ON")
		return self.inst.query("MEAS?")

	'''
	In Single Trigger mode, you can use FETC?, MEAS?, or READ? to query the power measurement.
	'''
	def SingleTringger_ModeFETC(self):
		self.inst.write("INIT:CONT OFF")     # Sets the EPM Series power meter to Single Trigger mode.
		self.inst.write("CONF")              # Configures the measurement.
		self.inst.write("INIT")              # Initializes the measurement.
		return self.inst.query("FETC?")      # Queries the measurement results. The above sequence must be followed.

	def SingleTringger_ModeMEAS(self):
		self.inst.write("INIT:CONT OFF")     # Sets the EPM Series power meter to Single Trigger mode.
		return self.inst.query("MEAS?")      # Reads the measurement results. MEAS? is equivalent to CONF followed by a READ?.

	def SingleTringger_ModeREAD(self):
		self.inst.write("INIT:CONT OFF")     # Sets the EPM Series power meter to Single Trigger mode.
		self.inst.query("CONF")              # Configures the measurement.
		return self.inst.query("READ?")      # Reads the measurement results. READ? is equivalent to INIT followed by a FETC?(Assuming that TRIG:SOUR is set to IMMediate).

	'''
	Making Repetitively Pulsed RF Power Measurement With Duty Cycle Correction
	Configure the EPM Series power meter to make repetitively pulsed RF power measurement and to apply duty cycle correction.
	'''
	def PulsedRFpowerMeasure(self):
		'''
		NOTE : Users are advised to use filter size >50 and to disable the step detect to obtain proper data when measuring pulse signals.
		'''
		self.inst.write("SYST:PRES")             # Presets the EPM Series power meter.
		self.inst.write("FREQ 1000MHz")          # Presets the EPM Series power meter.
		self.inst.write("ICORR:DCYC:STAT 1")     # Enables the duty cycle.
		self.inst.write("CORR:DCYC 50")          # Sets the duty cycle to 50%.
		self.inst.write("COSENS:AVER:COUN 256")  # Sets the filter length to 256.
		self.inst.write("SENS:AVER:SDET OFF")    # Disable step detection.
		return self.inst.query("FETC?")          # Queries the measurement results. The above sequence must be followed.
		
	'''
	The SCPI programming sequence examples for simple CW power measurement
	with Free Run and Single Trigger modes are shown as follows.
	'''
	def CWPowerMeasureFETC_FreeRunMode(self):
		self.inst.write("SYST:PRES")             # Presets the EPM Series power meter.
		self.inst.write("INIT:CONT ON")          # Sets the meter to Free Run mode.
		self.inst.write("FREQ 1000MHz")          # Presets the EPM Series power meter.
		return self.inst.query("FETC?")          # Queries the measurement results. The above sequence must be followed.
		
	def CWPowerMeasureFETC_SingelTringgerMode(self):
		# NOTE: In Single Trigger mode, INIT must be executed before FETC?.
		self.inst.write("SYST:PRES")             # Presets the EPM Series power meter.
		self.inst.write("INIT:CONT OFF")         # Sets the EPM Series power meter to Single Trigger mode.
		self.inst.write("FREQ 1000MHz")          # Presets the EPM Series power meter.
		self.inst.write("INIT")                  # Initializes the measurement.
		return self.inst.query("FETC?")          # Queries the measurement results. The above sequence must be followed.
	
	def CWPowerMeasureMEAS_SingelTringgerMode(self):
		# NOTE: In Single Trigger mode, MEAS? can be used without executing INIT.
		self.inst.write("SYST:PRES")             # Presets the EPM Series power meter.
		self.inst.write("INIT:CONT OFF")         # Sets the EPM Series power meter to Single Trigger mode.
		self.inst.write("FREQ 1000MHz")          # Presets the EPM Series power meter.
		return self.inst.query("MEAS?")          # Queries the measurement results. The above sequence must be followed.
	
	'''
	Low Power Measurement
	Configure the EPM Series power meter to perform low-power measurement and to apply required filtering for settled measurements.
	'''
	def LPM_SingelTringgerMode(self):
		# NOTE: It is advisable to perform zeroing and calibration of the power meter prior to measuring low level signals.
		# NOTE: Increasing the value of filter length increases measurement accuracy but also increases the time taken to make a power measurement.
		self.inst.write("SYST:PRES")                 # Presets the EPM Series power meter.
		self.inst.write("CAL")                       # Performs zeroing and calibration.
		if ( int(self.inst.query("*OPC?")) == 1 ):   # Waits for the operation to complete. Returns a 1 when zeroing and calibration have completed.
			self.inst.write("SENS:AVER:COUN 1024")   # Sets the filter length to 1024.
			self.inst.write("INIT:CONT OFF")         # Sets the meter to Single Trigger mode.
			return self.inst.query("READ?")          # Reads the measurement results. Timeout delay of approximately 55 s needed in order to obtain proper data.
		else :
			pass
			# shold return error

	'''
	Power Sweep Operation
	The SCPI programming sequence for the Power Sweep usage is shown below.
	'''
	def PowerSweepOperation():
		self.inst.write("TRIG:SOUR EXT")     # Sets to external trigger source, which is required for the Power Sweep operation.
		self.inst.write("TRIG:SLOP POS")     # Sets the EPM Series power meter to accept an external positive-edge trigger.
		self.inst.write("AVER:COUN 64")      # Sets the filter length to 64.
		self.inst.write("*OPC")              # Enables the OPC feature.
		# The *ESR? is issued for the first time.
		if(self.inst.query("*ESR?") > 0 and self.inst.query("*ESR?") < 255):    # 129 Some non-zero value (any value ranging from 0 to 255) will be returned when the *ESR? is issued for the first time.
			pass
		else:
			return False
		# The *ESR? is issued for the second time.
		if(self.inst.query("*ESR?")  == 0 ):            # 0 The returned value will be cleared to 0 when the *ESR? is issued for the second time.
			self.inst.write("BUFF:COUN 2")       # Sets the Power Sweep mode to capture two triggers.
			self.inst.write("INIT:CONT ON")      # Sets the EPM Series power meter to accept continuous trigger cycles.
			
	
		# Sends a positive-edged trigger to the EPM Series power meter through the external trigger port.
		if(self.inst.query("*ESR?")  == 0 ):        # Checks the OPC bit to confirm that the Power Sweep operation has completed. # Returns a 0 if the Power Sweep operation has not completed.
			pass

		# Sends another positive-edged trigger to the EPM Series power meter through the external trigger port.
		if(self.inst.query("*ESR?") == 1 ):                    # Checks the OPC bit to confirm that the Power Sweep operation has completed. # Returns a 1 if the Power Sweep operation has completed.
			return self.inst.query("FETC?")      # Reads back the two data points captured.
		
