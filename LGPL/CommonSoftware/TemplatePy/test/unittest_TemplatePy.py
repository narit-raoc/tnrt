# Project: TCS - Thai National Radio Telescope
# Company: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.10.20

#import standard modules
import unittest
import logging
import coloredlogs

# import data type definitions
import omniORB

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
import TemplatePyMod
import TemplatePyErrorImpl
import TemplatePyError


class Tests(unittest.TestCase):
	def setUp(self):
		'''
		This function runs 1 time per test_xxx function in this class. before test start
		'''
		# Configure logger
		self.logger = logging.getLogger('TemplateTester')
		self.logger.setLevel(logging.DEBUG)

		fmt_scrn = '%(asctime)s [%(levelname)s]: %(message)s'

		level_styles_scrn = {'critical': {'color': 'red', 'bold': True}, 'debug': {'color': 'white', 'faint': True}, 'error': {'color': 'red'}, 'info': {'color': 'green', 'bright': True}, 'notice': {'color': 'magenta'}, 'spam': {'color': 'green', 'faint': True}, 'success': {'color': 'green', 'bold': True}, 'verbose': {'color': 'blue'}, 'warning': {'color': 'yellow', 'bright': True, 'bold': True}}
		field_styles_scrn = {'asctime': {}, 'hostname': {'color': 'magenta'}, 'levelname': {'color': 'cyan', 'bright': True}, 'name': {'color': 'blue', 'bright': True}, 'programname': {'color': 'cyan'}}
		formatter_screen = coloredlogs.ColoredFormatter(fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn)

		# creating a handler to log on the console
		handler_screen = logging.StreamHandler()
		handler_screen.setFormatter(formatter_screen)
		handler_screen.setLevel(logging.DEBUG)

		# remove all handlers if they exist
		# remove because PySimpleClient logger already has handlers which
		# show duplicate of my log messages, but without the pretty color format.
		if (len(self.logger.handlers) > 0):
			self.logger.handlers = []

		# adding handlers
		self.logger.addHandler(handler_screen)
		self.logger.debug('Started logger name %s' % self.logger.name)

		self.logger.debug('Starting PySimpleClient')
		self.sc = None
		self.sc = PySimpleClient()
		
		self.objref = None
		self.component_name = 'TemplatePy'
		# Get reference to ComponentA.  If it is already activated by
		# ACS Command Center, this will be a reference to the same object.
		# If it is not active, this command will activate and create a new instance.
		try:
			self.logger.debug('Connecting to ACS component %s' % self.component_name)
			self.objref = self.sc.getComponent(self.component_name)
		except Exception as e:
			self.logger.error('Cannot get ACS component object reference for %s' % self.component_name)
			self.cleanup()

		# member variables to keep data for tests
		self.current_struct = None
		self.current_valuetype = None

	def tearDown(self):
		'''
		This function runs 1 time per test_xxx function in this class. after test complete
		'''
		try:
			self.logger.debug('Releasing ACS component %s' % self.component_name)
			self.sc.releaseComponent(self.component_name)
			self.objref = None
		except Exception as e:
			self.logger.error('Cannot release component reference for %s' % self.component_name)

		if(self.sc is not None):
			self.sc.disconnect()
			del self.sc

	def test100_datatypes(self):
		# Confirm that we can get all of the data types from the the remote object
		self.logger.debug('test data type: objref to ACS component')
		self.assertEqual(type(self.objref), TemplatePyMod._objref_TemplatePy)
	
	def test101_datatypes(self):
		self.logger.debug('test data type: CDB value')
		self.assertEqual(self.objref.get_database_value(), 123.456)
	
	def test102_datatypes(self):
		self.logger.debug('test data type: double')
		self.assertEqual(type(self.objref.get_double()), float)
		
	def test103_datatypes(self):
		self.logger.debug('test data type: enum')
		self.assertEqual(type(self.objref.get_enum()), omniORB.EnumItem)
	
	def test104_datatypes(self):
		self.logger.debug('test data type: struct')
		self.assertEqual(type(self.objref.get_struct()), TemplatePyMod.ExampleStruct)
	
	def test105_datatypes(self):
		self.logger.debug('test data type: valuetype')
		vtype = type(self.objref.get_valuetype())
		condition = (vtype == TemplatePyMod.ExampleChildValueType1) or (vtype == TemplatePyMod.ExampleChildValueType2)
		self.assertTrue(condition)

	def test200_set_get_data_values(self):
		new_double = 987.654
		new_enum = TemplatePyMod.option2
		new_struct = TemplatePyMod.ExampleStruct(10, 11.1, 22.2, 33.3)

		self.logger.debug('test set and get data type: double')
		self.objref.set_double(new_double)
		self.assertEqual(self.objref.get_double(), new_double)

		self.logger.debug('test set and get data type: enum')
		self.objref.set_enum(new_enum)
		self.assertEqual(self.objref.get_enum(), new_enum)

		self.logger.debug('test set and get data type: struct')
		self.objref.set_struct(new_struct)
		current_struct = self.objref.get_struct()
		self.assertEqual(current_struct.sequence_counter, new_struct.sequence_counter)
		self.assertEqual(current_struct.val1, new_struct.val1)
		self.assertEqual(current_struct.val2, new_struct.val2)
		self.assertEqual(current_struct.val3, new_struct.val3)

	def test300_set_valuetype_simple_good_param(self):
		new_valuetype_1 = TemplatePyMod.ExampleChildValueType1(3.3, 4.4, 9876, 5432, False)
		self.objref.set_valuetype_simple(new_valuetype_1)

	def test301_set_valuetype_simple_bad_param(self):
		new_valuetype_2 = TemplatePyMod.ExampleChildValueType2(7.7, 8, False)
		# Python unittest doesn't allow me to use omniORB.CORBA._omni_sys_exc 
		# with assertRaises() because apparently it does not inherit from python's
		# built in Exception class.  So, I just catch all in this test becuase
		# I know the parameter will be wrong.
		with self.assertRaises(Exception):
			self.objref.set_valuetype_simple(new_valuetype_2)

	def test302_set_valuetype_inherit(self):
		new_valuetype_1 = TemplatePyMod.ExampleChildValueType1(3.3, 4.4, 9876, 5432, False)
		new_valuetype_2 = TemplatePyMod.ExampleChildValueType2(7.7, 8, False)
		
		self.logger.debug('test set and get valuetype wtih inheritance: child type1')
		self.objref.set_valuetype_inherit(new_valuetype_1)
		current_valuetype = self.objref.get_valuetype()
		self.assertEqual(current_valuetype.val1, new_valuetype_1.val1)
		self.assertEqual(current_valuetype.val2, new_valuetype_1.val2)
		self.assertEqual(current_valuetype.val3, new_valuetype_1.val3)
		self.assertEqual(current_valuetype.val4, new_valuetype_1.val4)
		self.assertEqual(current_valuetype.val5, new_valuetype_1.val5)

		self.logger.debug('test set and get valuetype wtih inheritance: child type2')
		self.objref.set_valuetype_inherit(new_valuetype_2)
		current_valuetype = self.objref.get_valuetype()
		self.assertEqual(current_valuetype.val1, new_valuetype_2.val1)
		self.assertEqual(current_valuetype.val3, new_valuetype_2.val3)
		self.assertEqual(current_valuetype.val5, new_valuetype_2.val5)

	def test400_notification_channel(self):
		nc_result = self.objref.test_notification()
		self.assertTrue(nc_result)

	def test500_custom_user_exception(self):
		with self.assertRaises(TemplatePyError.ExampleErrorEx):
			self.objref.test_exception()

if __name__ == '__main__':
	unittest.main()
