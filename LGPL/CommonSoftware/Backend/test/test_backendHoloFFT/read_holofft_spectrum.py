import time
import threading
import logging
import coloredlogs
from argparse import ArgumentParser

from FFT35670A import FFT35670A
from Supplier import Supplier

data_supplier = Supplier("BACKEND_HOLO_DATA")
stop_event = threading.Event()

def get_colored_logger(name, log_level):
    fmt_scrn = "%(asctime)s.%(msecs)03d [%(levelname)s] %(name)s.%(funcName)s(): %(message)s"

    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red"},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta"},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    # creating a handler to log on the console
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)

    logger = logging.getLogger(name)
    logger.addHandler(handler_screen)

    if log_level.upper() == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif log_level.upper() == "INFO":
        logger.setLevel(logging.INFO)
    elif log_level.upper() == "WARNING":
        logger.setLevel(logging.WARNING)
    elif log_level.upper() == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.DEBUG)
        logger.error("invalid parameter log-level. Select level DEBUG")

    return logger

        
if __name__ == "__main__":
    parser = ArgumentParser(
        description="Receive (and optionally record) tcp packages send by EDD Fits writer interface."
    )
    parser.add_argument(
        "-s",
        "--search",
        dest="search",
        default=False,
        action="store_true",
        help="Search and center FFT analyzer spectrum on detected signal")
    
    args = parser.parse_args()

    logger = get_colored_logger("__main__", "DEBUG")
    logger.debug("test logger in module {}".format(__name__))
    logger.info("Starting client for BackendHoloFFT")
    
    # Get reference to Component.  If it is already activated by
    # ACS Command Center, this will be a reference to the same object.
    # If it is not active, this command will activate and create a new instance.
    host = "192.168.90.40"
    port = 1234
    try:
        client = FFT35670A(host,port, True, "DEBUG")
        client.connect()
        client.measurement_start(search=False)    
        stop_event.clear()
        logger.info("Starting data receive and publish loop...")
        
        # Infinite loop that can be interrupted by CTRL+C
        while not stop_event.is_set():
            t0  = time.perf_counter()
            msg_holodata = client.get_holodata()
            t1  = time.perf_counter()
            data_supplier.publish_event(msg_holodata)
            logger.info(f"elapsed time to read data: {t1-t0}")

    except ConnectionRefusedError:
        logger.error(f"ConnectionRefusedError.  Cannot connect eddfits spectrum server {host}:{port}")
    except KeyboardInterrupt:
        logger.warning("Catch KeyboardInterrupt.  stop client")
        stop_event.set()
        logger.info("Wait for loop to exit ...")    
    client.disconnect()
