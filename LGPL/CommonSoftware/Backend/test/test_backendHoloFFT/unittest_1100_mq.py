# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

import BackendMod
import numpy as np
from BackendDefaults import generate_dtype_msg_holodata
from Supplier import Supplier
from unittest_common import ColoredTestCase

class SupplierTestCase(ColoredTestCase):
    def setUp(self):
        super().setUp()
        self.dtype_msg_holodata = generate_dtype_msg_holodata(1024)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1000_supplier_notification(self):
        s = Supplier(BackendMod.CHANNEL_NAME_HOLO_DATA)
        msg_holodata = np.squeeze(np.zeros(1, dtype=self.dtype_msg_holodata))
        self.logger.debug("publish message type: {}, data: {}".format(type(msg_holodata), msg_holodata.__repr__()))
        s.publish_event(msg_holodata)
        s.disconnect()
