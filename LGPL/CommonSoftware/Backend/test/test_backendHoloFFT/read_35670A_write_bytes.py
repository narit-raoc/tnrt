import time
from FFT35670A import FFT35670A
try:
    client = FFT35670A("192.168.90.40",1234, True, "DEBUG")
    client.connect()
    client.get_measurement_duration()


    # Check how many data points on CALC:DATA for all 4 traces
    #client.logging_socket_query("CALC1:DATA:HEADER:POINTS?")
    #client.logging_socket_query("CALC2:DATA:HEADER:POINTS?")
    # client.logging_socket_query("CALC3:DATA:HEADER:POINTS?")
    #client.logging_socket_query("CALC4:DATA:HEADER:POINTS?")

    # client.set_fft_length(100)
    # client.search_and_center(100)
    # client.calculate_time_volts(3,1)
    # client.calculate_time_volts(4,2)
    #client.set_axis_format(3, "NYQ")
    #client.set_axis_format(4, "NYQ")

    client.logging_socket_write("CALC1:UNIT:AMPL RMS")
    client.logging_socket_write("CALC2:UNIT:AMPL RMS")
    client.logging_socket_write("CALC3:UNIT:AMPL PEAK")
    client.logging_socket_write("CALC4:UNIT:AMPL PEAK")

    client.logging_socket_query("CALC1:UNIT:AMPL?")
    client.logging_socket_query("CALC2:UNIT:AMPL?")
    client.logging_socket_query("CALC3:UNIT:AMPL?")
    client.logging_socket_query("CALC4:UNIT:AMPL?")

    client.logging_socket_query("CALC4:UNIT:X?")
    client.logging_socket_query("CALC3:UNIT:VOLT?")
    client.logging_socket_query("CALC4:UNIT:VOLT?")

    client.logging_socket_write("INIT:CONT OFF")
    fc = client.get_center()

    (xdataf, xdataf_bytes) = client.binary_socket_query("CALC1:X:DATA?", return_raw_bytes=True)
    (xdatat, xdatat_bytes) = client.binary_socket_query("CALC3:X:DATA?", return_raw_bytes=True)

    t0  = time.perf_counter()
    (d1, d1bytes) = client.binary_socket_query("CALC1:DATA?", return_raw_bytes=True)
    t1 = time.perf_counter()
    (d2, d2bytes) = client.binary_socket_query("CALC2:DATA?", return_raw_bytes=True)
    t2 = time.perf_counter()
    (d3, d3bytes) = client.binary_socket_query("CALC3:DATA?", complex_pairs=True, return_raw_bytes=True)
    t3 = time.perf_counter()
    (d4, d4bytes) = client.binary_socket_query("CALC4:DATA?", complex_pairs=True, return_raw_bytes=True)
    t4 = time.perf_counter()
except:
    client.disconnect()
    exit(1)

# Write raw bytes to files for post processing
with open("d1.txt", "wb") as f:
    f.write(d1bytes)

with open("d2.txt", "wb") as f:
    f.write(d2bytes)

with open("d3.txt", "wb") as f:
    f.write(d3bytes)

with open("d4.txt", "wb") as f:
    f.write(d4bytes)

with open("xdatat.txt", "wb") as f:
    f.write(xdatat_bytes)

with open("xdataf.txt", "wb") as f:
    f.write(xdataf_bytes)

print(f"elapsed time 1: {t1-t0} s")
print(f"elapsed time 2: {t2-t1} s")
print(f"elapsed time 3: {t3-t2} s")
print(f"elapsed time 4: {t4-t3} s")
print(f"elapsed time total: {t4-t0} s")
print(f"elapsed time read complex 3,4: {t4-t2} s")



