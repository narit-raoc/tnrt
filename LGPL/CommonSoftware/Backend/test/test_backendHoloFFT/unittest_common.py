# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

# import standard modules
import unittest
import logging
import coloredlogs

class ColoredTestCase(unittest.TestCase):
    def setUp(self):
        # Configure logger
        self.logger = logging.getLogger("ColoredTestCase")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(funcName)s(): %(message)s"

        level_styles_scrn = {
            "critical": {"color": "red", "bold": True},
            "debug": {"color": "white", "faint": True},
            "error": {"color": "red"},
            "info": {"color": "green", "bright": True},
            "notice": {"color": "magenta"},
            "spam": {"color": "green", "faint": True},
            "success": {"color": "green", "bold": True},
            "verbose": {"color": "blue"},
            "warning": {"color": "yellow", "bright": True, "bold": True},
        }
        field_styles_scrn = {
            "asctime": {},
            "hostname": {"color": "magenta"},
            "levelname": {"color": "cyan", "bright": True},
            "name": {"color": "blue", "bright": True},
            "programname": {"color": "cyan"},
        }
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)

        self.result = None
    
    def tearDown(self):
        # show result of test
        self.logger.info("result type: {}, data: {}".format(type(self.result), self.result))
        
