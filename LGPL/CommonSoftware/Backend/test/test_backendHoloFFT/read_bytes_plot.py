import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets
from scipy import signal

cwd = "/home/ssarris/archive/"

with open(cwd+"xdatat.txt", "rb") as f:
    xdatat_bytes = f.read()
    xdatat = np.frombuffer(xdatat_bytes, dtype=">f8")

with open(cwd+"xdataf.txt", "rb") as f:
    xdataf_bytes = f.read()
    xdataf = np.frombuffer(xdataf_bytes, dtype=">f8")

with open(cwd+"d1.txt", "rb") as f:
    d1bytes = f.read()
    d1 = np.frombuffer(d1bytes, dtype=">f8")
    
with open(cwd+"d2.txt", "rb") as f:
    d2bytes = f.read()
    d2 = np.frombuffer(d2bytes, dtype=">f8")

# D3 and D4 are complex-valued time-series voltages (real, imag) pairs of data in binary file
with open(cwd+"d3.txt", "rb") as f:
    d3bytes = f.read()
    data_interleaved = np.frombuffer(d3bytes, dtype=[('real', '>f8'), ('imag', '>f8')])
    d3 = data_interleaved["real"] + 1j * data_interleaved["imag"]
    
with open(cwd+"d4.txt", "rb") as f:
    d4bytes = f.read()
    data_interleaved = np.frombuffer(d4bytes, dtype=[('real', '>f8'), ('imag', '>f8')])
    d4 = data_interleaved["real"] + 1j * data_interleaved["imag"]
    
d5 = d4 / d3 # complex divide - result is relative magnitude and phase

print(f"xdatat ndarray len: {len(xdatat)}, data: {xdatat}")
print(f"xdataf ndarray len: {len(xdataf)}, data: {xdataf}")
# print(f'd1 ndarray len: {len(d1)}, data: {d1}\n')
# print(f'd2 ndarray len: {len(d2)}, data: {d2}\n')
# print(f'd3 ndarray len: {len(d3)}, data: {d3}\n')
# print(f'd4 ndarray len: {len(d4)}, data: {d4}\n')

fft_length = len(d3)
# Truncate the first and last samples because the manul says these are in the anti alias filter
# rolloff, os not good data.
start = fft_length / 1.28
window = signal.windows.flattop(fft_length)

anti_alias_passband_min = int(fft_length * (1 - 1 / 1.28) / 2)
anti_alias_passband_max = int(fft_length - (fft_length * (1 - 1 / 1.28)) / 2 + 1)
# print(f"anti alias passband: [{anti_alias_passband_min}:{anti_alias_passband_max}]")

ff_antialias = xdataf[anti_alias_passband_min:anti_alias_passband_max]
# print(f"ff_antialiased len: {len(ff_antialias)}, data: {ff_antialias}")

d3_ff = np.fft.fftshift(np.fft.fft(d3*window) / fft_length)[anti_alias_passband_min:anti_alias_passband_max]
d4_ff = np.fft.fftshift(np.fft.fft(d4*window) / fft_length)[anti_alias_passband_min:anti_alias_passband_max]
d5_ff = np.fft.fftshift(np.fft.fft(d5) / fft_length)[anti_alias_passband_min:anti_alias_passband_max]

d3_mag = 30 + 20 * np.log10(np.abs(d3_ff))
d4_mag = 30 + 20 * np.log10(np.abs(d4_ff))
d5_mag = 20 * np.log10(np.abs(d5_ff))
d5_phase = np.angle(d5_ff)

# Show graphs
grid_opacity = 0.7
app = QtWidgets.QApplication([])

glw = pg.GraphicsLayoutWidget(show=True)
glw.resize(1024, 768)
glw.setWindowTitle("FFT Analzyer Data")
pg.setConfigOptions(antialias=True)


pi_mag = glw.addPlot(0, 0, title="Magnitude from FFT Analyzer CALC1,2")
pi_mag.addLegend()
pi_mag.plot(ff_antialias, d1, name="d1", pen="y", unit="Hz")
pi_mag.plot(ff_antialias, d2, name="d2", pen="c", unit="Hz")
#pi_mag.plot(ff_antialias, d2-d1, name="rel", pen="m", unit="Hz")

ax_L = pg.AxisItem(orientation="left", text="Mag", units="dB", siPrefix=False)
ax_B = pg.AxisItem(orientation="bottom", text="Frequency", units="Hz")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_mag.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_mag.showGrid(True, True, alpha=grid_opacity)

pi_fft_spectrum = glw.addPlot(1, 0, title="Magnitude from numpy fft of CALC3,4 compelex I/Q")
pi_fft_spectrum.addLegend()
pi_fft_spectrum.plot(ff_antialias, d3_mag, name="d3", pen="y")
pi_fft_spectrum.plot(ff_antialias, d4_mag, name="d4", pen="c")
pi_fft_spectrum.plot(ff_antialias, d5_mag, name="rel", pen="m")

ax_L2 = pg.AxisItem(orientation="left", text="Mag", units="dB", siPrefix=False)
ax_B2 = pg.AxisItem(orientation="bottom", text="Frequency", units="Hz")
ax_L2.showLabel(True)
ax_B2.showLabel(True)
pi_fft_spectrum.setAxisItems(axisItems={"bottom": ax_B2, "left" : ax_L2})
pi_fft_spectrum.showGrid(True, True, alpha=grid_opacity)

# Markers
marker_index = np.argmax(d3_mag)
marker_freq = ff_antialias[marker_index]
print(f"marker freq: {marker_freq}")

pi_phase = glw.addPlot(0, 1, title="Relative Phase")
pi_phase.addLegend()
pi_phase.plot(ff_antialias, np.angle(d5_ff), name="rel", pen="m")
ax_L3 = pg.AxisItem(orientation="left", text="Phase", units="rad")
ax_B3 = pg.AxisItem(orientation="bottom", text="Frequency", units="Hz")
ax_L3.showLabel(True)
ax_B3.showLabel(True)
pi_phase.setAxisItems(axisItems={"bottom": ax_B3, "left" : ax_L3})

pi_phase.showGrid(True, True, alpha=grid_opacity)

pi_constellation = glw.addPlot(1, 1, 2, 1, title="I/Q Constellation (Normalized Magnitude=1)")
pi_constellation.addLegend()
angles = np.linspace(0, 2*np.pi, 360, endpoint=True)
unit_circle_xx = np.cos(angles)
unit_circle_yy = np.sin(angles)

pi_constellation.plot(unit_circle_xx, unit_circle_yy, pen="w")
pi_constellation.plot(np.cos(np.angle(d3)), np.sin(np.angle(d3)), name="d3", pen="y", symbolPen="y", symbolSize=20, symbolBrush=None)
pi_constellation.plot(np.cos(np.angle(d4)), np.sin(np.angle(d4)), name="d4", pen="c", symbolPen="c", symbolSize=20, symbolBrush=None)
pi_constellation.plot(np.cos(np.angle(d5)), np.sin(np.angle(d5)), name="drel", pen="m", symbolPen="m", symbolSize=20, symbolBrush=None)
pi_constellation.setAspectLocked(True)
pi_constellation.setXRange(-1, 1)
pi_constellation.setYRange(-1, 1)

pi_constellation.showGrid(True, True, alpha=grid_opacity)


pdi_magnitude_ref_marker = pi_fft_spectrum.plot(
    [ff_antialias[marker_index]], [d3_mag[marker_index]], pen=None, symbol="o", symbolPen="y", symbolBrush=None
)
pdi_magnitude_tst_marker = pi_fft_spectrum.plot(
    [ff_antialias[marker_index]], [d4_mag[marker_index]], pen=None, symbol="o", symbolPen="c", symbolBrush=None
)
pdi_magnitude_rel_marker = pi_fft_spectrum.plot(
    [ff_antialias[marker_index]], [d5_mag[marker_index]], pen=None, symbol="o", symbolPen="m", symbolBrush=None
)
pdi_phase_marker = pi_phase.plot(
    [ff_antialias[marker_index]], [d5_phase[marker_index]], pen=None, symbol="o", symbolPen="m", symbolBrush=None
)

pdi_constellation_marker_ref = pi_constellation.plot(
    [np.cos(np.angle(d3[marker_index]))], [np.sin(np.angle(d3[marker_index]))], pen=None, symbol="+", symbolPen=pg.mkPen(color="y", width=2, size=12)
)
pdi_constellation_marker_tst = pi_constellation.plot(
    [np.cos(np.angle(d4[marker_index]))], [np.sin(np.angle(d4[marker_index]))], pen=None, symbol="+", symbolPen=pg.mkPen(color="c", width=2, size=12)
)
pdi_constellation_marker_rel = pi_constellation.plot(
    [np.cos(np.angle(d5[marker_index]))], [np.sin(np.angle(d5[marker_index]))], pen=None, symbol="+", symbolPen=pg.mkPen(color="m", width=2, size=12)
)

# Start the Qt event loop and block this script until user closes the GUI window
app.exec()

