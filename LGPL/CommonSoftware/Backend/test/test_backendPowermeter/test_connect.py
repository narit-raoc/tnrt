import pyvisa
import time
from datetime import datetime
import sys
sys.path.insert(1, '/home/teep/tnrt/LGPL/CommonSoftware/Backend/src')
from EPM1913A import EPM1913A
import BackendDefaults as config

EPM = EPM1913A()

print(EPM.connect())

print(EPM.IDN())

print(EPM.clear())

print(EPM.preset())

print(EPM.checkError())

print(EPM.deviceSetup())

print(EPM.measureFETCMode())

print(EPM.getData())

# x = input("wait")
print(EPM.disconnect())

