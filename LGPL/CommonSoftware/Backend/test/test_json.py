import json
from decimal import Decimal

# Converting JSON data to Python objects 
# JSON data can be converted (deserialized) to Pyhon objects using the json.loads() function.  A table of the mapping:

# JSON	        Python
# object	    dict
# array	        list
# string	    str
# number (int)	int
# number (real)	float
# true	        True
# false	        False
# null	        None

# ---------------------------------------------------------------------
# Convert JSON to Python Object (Dict)
# To convert JSON to a Python dict use this:
def test_dict():  
    json_data = '{"name": "Brian", "city": "Seattle"}'
    python_obj = json.loads(json_data)
    print(python_obj["name"])
    print(python_obj["city"])

# Convert JSON to Python Object (List)
# JSON data can be directly mapped to a Python list.
def test_list():
    array = '{"drinks": ["coffee", "tea", "water"]'
    data = json.loads(array)

    for element in data['drinks']:
        print(element)

# Convert JSON to Python Object (float)
# Floating points can be mapped using the decimal library.
def test_float():
    jsondata = '{"number": 1.573937659}'
    x = json.loads(jsondata, parse_float=decimal)
    print(x['number'])

# Convert JSON to Python Object (Example)
# JSON data often holds multiple objects, an example of how to use that below:
def test_example():
    json_input = '{"person": [{"name": "Brian", "city": "Seattle""}]}'
    try:
        decoded = json.loads(json_input)
        # Access data
        for x in decoded['persons']:
            print(x['name'])
    except(ValueError, KeyError, TypeError):
        print("JSON format error")

# Convert Python Object (Dict) to JSON
# If you want to convert a Python Object to JSON use the json.dumps() method.
def test_dict_to_json():
    d = {}
    d["name"] = "Luke"  
    d["Country"] = "Canada"
    print(json.dumps(d, ensure_ascii=False))
    # result {"Country": "Canada", "Name": "Luke"}

# Pretty printing
# If you want to display JSON data you can use the json.dumps() function.
def test_print():
    json_data = '{"name": "Brain", "city": "Seattle"}'
    python_obj = json.loads(json_data)
    print(json.dumps(python_obj, sort_keys=True, indent=4))

# ------------------------------------------------------------
if __name__== "__main__":
    test_dict()
    test_print()
    test_list()