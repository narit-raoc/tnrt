import unittest
import numpy as np
import time
import json
from unittest_common import ColoredTestCase

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

class Tests(ColoredTestCase):
	def setUp(self):
		super().setUp()
		self.result = None
		
		self.sc = None
		self.sc = PySimpleClient()
		
		self.objref = None
		self.component_name = 'BackendMockEDD'
		
		try:
			self.objref = self.sc.getComponent(self.component_name)
			self.logger.debug("component name {}, ref: {}".format(self.component_name, self.objref))
		except Exception as e:
			self.logger.error("Cannot get ACS component object reference for {}".format(self.component_name))
			self.cleanup()

	def tearDown(self):
		try:
			self.logger.debug('Releasing ACS component %s' % self.component_name)
			self.sc.releaseComponent(self.component_name)
			self.objref = None		
			if self.sc is not None:
				self.sc.disconnect()
				del self.sc
		except Exception as e:
			self.logger.error('Cannot release component reference for %s' % self.component_name)
			if self.sc is not None:
				self.sc.disconnect()
				del self.sc
				raise(e)
	
	def test_2000_all_state_sequence(self):
		self.objref.client_start()
		
		result = self.objref.provision("TNRT_dualpol_spectrometer_L")
		self.logger.debug('result: {}'.format(result))
		self.assertEqual(result, "provision success")
		
		# Digitizer
		sampling_rate = 4e9
		predecimation_factor = 2
		# User parameter
		integration_time = 1.0 # seconds
		# Spectrometer pipeline
		fft_length = 2048
		naccumulate = integration_time * (sampling_rate / predecimation_factor) / fft_length
		self.logger.debug('naccumulate: {}'.format(naccumulate))
		naccumulate_power_of_2 = int(2 ** np.ceil(np.log2(naccumulate)))
		self.logger.debug('naccumulate_power_of_2: {}'.format(naccumulate_power_of_2))

		config_dict = {
			"products":{
				"gated_spectrometer_0": {
					"naccumulate": naccumulate_power_of_2,
					"fft_length": fft_length
					},
				"gated_spectrometer_1": {
					"naccumulate": naccumulate_power_of_2,
					"fft_length": fft_length
					},
				}
			}

		config_json = json.dumps(config_dict)
		self.logger.debug('configure config_json_string="{}"'.format(config_json))
		
		result = self.objref.configure(config_json)
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "configuration success")
		
		result = self.objref.capture_start()
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "capture start success")
		
		result = self.objref.measurement_prepare("")
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "measurement prepare success")
		
		result = self.objref.measurement_start()
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "measurement start success")
		self.logger.warning("wait 5 seconds ...")
		time.sleep(5)
		
		result = self.objref.measurement_stop()
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "measurement stop success")
		
		result = self.objref.deconfigure()
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "reset complete")
		
		result = self.objref.deprovision()
		self.logger.info('result: {}'.format(result))
		self.assertEqual(result, "deprovision success")
		
		self.objref.client_stop()
		
if __name__ == '__main__':
	unittest.main()
