import sys
import katcp_blocking_client as kat
import json
import os

kat_path = os.environ['INTROOT']+'/lib/python/site-packages'
sys.path.insert(1, kat_path)

my_kat_connection = kat.BlockingRequest("127.0.0.1","5000")

def convert_katmessage_to_string(katMessage):
    katMessage = str(katMessage)                # convert to string
    katMessage = katMessage.replace("\_u", "")   # clean \_ to \n
    katMessage = katMessage.replace("{u", "{")    # clean \_ to \n
    katMessage = katMessage.replace("\_", "")    # clean \_ to \n
    katMessage = katMessage.replace("'", '"')    # clean \_ to \n
    return katMessage

def get_katmessage_function(katMessage):
    dataString = convert_katmessage_to_string(katMessage)
    sub_status = dataString.find(']')
    katMessage_function = dataString[0:int(sub_status)-1]
    return katMessage_function

def get_katmessage_status(katMessage):
    dataString = convert_katmessage_to_string(katMessage)
    sub_data = dataString.find('{')  
    sub_status = dataString.find(']')
    katMessage_status = dataString[int(sub_status)+2:int(sub_data)-1]
    return katMessage_status

def get_katmessage_data(katMessage):
    dataString = convert_katmessage_to_string(katMessage)
    sub_data = dataString.find('{')  
    katMessage_data = dataString[int(sub_data):]
    return katMessage_data

def Get_all_metadata():
    global my_kat_connection
    kat_result = my_kat_connection.get_metadata_all()  # Get all metadata from status server service
    
    if get_katmessage_status(kat_result) == "ok" :
        metadata = get_katmessage_data(kat_result)
    else:
        print("ERROR:" + str(kat_result))
        return
    
    metadata_dictionary = json.loads(metadata)   # Convert sting to python dictionary
    for first_key in metadata_dictionary:    # Print fist kay of meta data
        print first_key

    print "Test get metadata all pass"
    return metadata_dictionary

def Get_separate_metadata(key):
    print("key : "+ key)
    global my_kat_connection
    kat_result = my_kat_connection.get_metadata_superate(key)  # Get all metadata from status server service
    
    if get_katmessage_status(kat_result) == "ok" :
        metadata = get_katmessage_data(kat_result)
    else:
        print("ERROR:" + str(kat_result))
        return
    
    metadata_dictionary = json.loads(metadata)  # Convert sting to python dictionary
    for first_key in metadata_dictionary:   # Print fist kay of meta data
        print first_key
    
    print "Test get metadata superate pass"
    return metadata_dictionary


def main():
    '''
    first_key = {  
                    "data_scan",
                    "data_antenna",
                    "data_weather",
                    "data_backend",
                    "data_obsengine",
                    "data_atmosphere",
                    "data_tag"
                }
    '''
    global my_kat_connection
    print my_kat_connection.start()

    first_key = "data_scan,data_antenna,data_weather"

    # metadata = Get_separate_metadata(first_key)
    metadata = Get_all_metadata() 
   
    print metadata['data_weather']  # Test print metadata with 'data_weather' key
    
    my_kat_connection.stop()      # Stop kat connection


if __name__== "__main__":
    main()

