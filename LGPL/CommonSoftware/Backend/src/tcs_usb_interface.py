"""This module is the interface between TNRT TCS and MPIFR USB backend."""
# Copyright (c) 2021 Jason Wu <jwu@mpifr-bonn.mpg.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import codecs
import re
import katcp
import logging
import coloredlogs
import json
from copy import deepcopy
from katcp import BlockingClient
import EDDDataStore as EDDDataStore
import os

logging.getLogger().addHandler(logging.NullHandler())
log = logging.getLogger("tcs_usb_interface")
coloredlogs.install(
    fmt="[ %(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", level="INFO", logger=log
)


ESCAPE_SEQUENCE_RE = re.compile(
    r"""
    ( \\U........      # 8-digit hex escapes
    | \\u....          # 4-digit hex escapes
    | \\x..            # 2-digit hex escapes
    | \\[0-7]{1,3}     # Octal escapes
    | \\N\{[^}]+\}     # Unicode characters by name
    | \\[\\'"abfnrtv]  # Single-character escapes
    )""",
    re.UNICODE | re.VERBOSE,
)


class TcsUsbInterface(BlockingClient):
    def __init__(self, host, port, redis_host, redis_port):
        master_host = host
        master_port = port
        super(TcsUsbInterface, self).__init__(master_host, master_port)
        self.start(timeout=3)
        if not self.is_connected():
            log.warning('self.is_connected() is False. Stop the client')
            self.stop()
            raise ConnectionError("Fail to connect master controller at {}:{}".format(master_host, master_port))
        self.__eddDataStore = EDDDataStore.EDDDataStore(redis_host, redis_port)

    def __del__(self):
        super(TcsUsbInterface, self).stop()
        super(TcsUsbInterface, self).join()

    def unescape_string(self, s):
        def decode_match(match):
            return codecs.decode(match.group(0), "unicode-escape")

        return ESCAPE_SEQUENCE_RE.sub(decode_match, s)

    def decode_katcp_message(self, s):
        return self.unescape_string(s).replace("\_", " ")

    def to_stream(self, reply, informs):
        log.info(self.decode_katcp_message(reply.__str__()))
        for msg in informs:
            log.info(self.decode_katcp_message(msg.__str__()))

    def start(self, timeout=3):
        """
        @brief      Start the blocking client
        """
        log.info("Starting TCS-USB blocking client interface")
        self.setDaemon(True)
        super(TcsUsbInterface, self).start()
        self.wait_protocol(timeout)

    def stop(self):
        """
        @brief      Stop the blocking client
        """
        super(TcsUsbInterface, self).stop()
        super(TcsUsbInterface, self).join()

    def deconfigure(self):
        """
        @brief      Send deconfigure command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending deconfigure to MASTER CONTROLLER")
        reply, informs = self.blocking_request(katcp.Message.request("deconfigure"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def get_current_config(self):
        reply, informs = self.blocking_request(katcp.Message.request("sensor-value", "current-config"), timeout=120.0)
        if len(informs) > 1 and len(informs[1].arguments) == 1:  # getting error
            return informs[0].arguments[0].decode()
            
        return informs[0].arguments[4].decode("utf-8")

    def get_pipeline_status(self):
        reply, informs = self.blocking_request(katcp.Message.request("sensor-value", "pipeline-status"), timeout=120.0)
        if len(informs) > 1 and len(informs[0].arguments) == 1:  # getting error
            return informs[0].arguments[0].decode()

        return informs[0].arguments[4].decode("utf-8")

    def get_current_provision(self):
        reply, informs = self.blocking_request(katcp.Message.request("sensor-value", "provision"), timeout=120.0)
        if len(informs) > 1 and len(informs[0].arguments) == 1:  # getting error
            return informs[0].arguments[0].decode()

        provision_path = informs[0].arguments[4].decode("utf-8")

        if provision_path == 'Unprovisioned':
            return provision_path

        provision_file = os.path.basename(provision_path)
        [provision_name, file_type] = provision_file.split('.')
        return provision_name


    def configure(self, config=""):
        """
        @brief      Send configure command to the server

        Args:
            config (dict): Config python dictionary.

        Returns:
            KATCP message object

        """
        log.info("Sending configure with argument {} to MASTER CONTROLLER".format(config))
        reply, informs = self.blocking_request(katcp.Message.request("configure", config), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def capture_start(self):
        """
        @brief      Send capture_start command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending capture_start to MASTER CONTROLLER")
        reply, informs = self.blocking_request(katcp.Message.request("capture-start"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def capture_stop(self):
        """
        @brief      Send capture_stop command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending capture_stop to MASTER CONTROLLER")
        reply, informs = self.blocking_request(katcp.Message.request("capture-stop"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def measurement_prepare(self, config={}):
        """
        @brief      Send measurement_prepare command to the server

        Args:
            config (dict): Source specific python dictionary and/or metadata python dictionary.

        Returns:
            KATCP message object
        """
        config_dict = deepcopy(config)

        if "metadata" in config.keys():
            for item in config_dict["metadata"]:
                log.info(
                    "Setting metadata: {} with value: {} to Redis server.".format(item, config_dict["metadata"][item])
                )
                self.__eddDataStore.setTelescopeDataItem(item, config_dict["metadata"][item])
            config_dict.pop("metadata")
        log.info("Sending measurement_prepare with argument {} to MASTER CONTROLLER".format(config_dict))
        reply, informs = self.blocking_request(
            katcp.Message.request("measurement-prepare", json.dumps(config_dict)), timeout=120.0
        )
        self.to_stream(reply, informs)
        return reply

    def metadata_update(self, metadata_dict={}):
        """
        @brief      Forward key:value pair information to USB Redis server

        Args:
            metadata_dict (dict): metadata python dictionary in key:value pair format

        Returns:
            None
        """
        for key in metadata_dict.keys():
            log.info("Setting metadata: {} with value: {} to Redis server.".format(key, metadata_dict[key]))
            self.__eddDataStore.setTelescopeDataItem(key, metadata_dict[key])

    def measurement_start(self):
        """
        @brief      Send measurement_start command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending measurement_start to MASTER CONTROLLER")
        reply, informs = self.blocking_request(katcp.Message.request("measurement-start"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def measurement_stop(self):
        """
        @brief      Send measurement_stop command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending measurement_stop to MASTER CONTROLLER")
        reply, informs = self.blocking_request(katcp.Message.request("measurement-stop"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def set(self, config):
        """
        @brief      Send set command to the server

        Args:
            config (dict): Config python dictionary.

        Returns:
            KATCP message object
        """
        log.info("Sending set with arguemnt {} to MASTER CONTROLLER".format(config))
        reply, informs = self.blocking_request(katcp.Message.request("set", json.dumps(config)), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def provision(self, config):
        """
        @brief      Send provision command to the serveri

        Args:
            config (str): Provision name.

        Returns:
            KATCP message object
        """
        log.info("Sending provision with argument {} to MASTER CONTROLLER".format(config))
        reply, informs = self.blocking_request(katcp.Message.request("provision", config), timeout=240.0)

        # if reply.arguments[0].decode() == "invalid":
        #     raise Exception("Error: {}".format(reply))
        #     # raise Exception("Error: {}".format(reply.arguments[1].decode()))

        # if reply.arguments[0].decode() == "fail":
        #     raise Exception("Error: {}".format(reply))
        #     # raise Exception("Error: {}".format(informs[0].arguments[0].decode()))

        # if len(informs) > 1 and len(informs[0].arguments) == 1:  # getting error
        #     raise Exception("Error: {}".format(reply))
        #     # raise Exception(informs[0].arguments[0].decode()) 

        return reply

    def deprovision(self):
        """
        @brief      Send deprovision command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending deprovision to MASTER CONTROLLER")
        reply, informs = self.blocking_request(katcp.Message.request("deprovision"), timeout=240.0)
        self.to_stream(reply, informs)
        return reply



if __name__ == "__main__":
    client = TcsUsbInterface("192.168.90.11", 7147, "192.168.90.11", 6379)
    # client.provision('TNRT_dualpol_spectrometer')
    current_config = client.get_current_config()
    print(current_config)
    # current_state = client.get_pipeline_status()
    # print(current_state)
    # current_provision = client.get_current_provision()
    # print(current_provision)
