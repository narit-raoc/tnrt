# python standard
import logging
import inspect
import threading

# pip 3rd party
import pyvisa
import coloredlogs
import numpy as np
from astropy.time import Time
import astropy.units as units
from scipy import signal

# tcs
from BackendDefaults import holofft_host, holofft_port
from BackendDefaults import generate_dtype_msg_holodata

class Register:
    """
    parent class to share common functions among all registers
    """
    def __init__(self, value):
        self.value = value
        self.bits = dict()
        for bit_name, mask in self.masks.items():
            # Translate register into python convenient dictionary
            # Dictionary key is the bit name, value is True | False
            self.bits[bit_name] = (value & mask) > 0

    def __repr__(self):
        """
        Create formatted string for print log.
        """
        s = "{} {}\n".format(self.value, self.__class__.__name__)
        for item in self.bits.items():
            s = s + "{} {}\n".format(1 if item[1]==True else 0, item[0])
        return(s)
    
class DeviceStateRegister(Register):
    masks = {"AUTOCAL_OFF" : 1 << 0,
            "UNUSED1" : 1 << 1,
            "HARDWARE_FAILED" : 1 << 2,
            "UNUSED3" : 1 << 3,
            "KEY_PRESSED" : 1 << 4,
            "DISPLAY_READY" : 1 << 5,
            "RS232_CHAR_AVAILABLE" : 1 << 6,
            "RS232_INPUT_HELD_OFF" : 1 << 7,
            "RS232_OUTPUT_HELD_OFF" : 1 << 8,
            "RS232_ERROR" : 1 << 9,
            "UNUSED10" : 1 << 10,
            "UNUSED11" : 1 << 11,
            "UNUSED12" : 1 << 12,
            "UNUSED13" : 1 << 13,
            "UNUSED14" : 1 << 14,
            "UNUSED15" : 1 << 15,
            }
class OperationStatusRegister(Register):
    masks = {"CALIBRATING" : 1 << 0,
            "SETTLING" : 1 << 1,
            "RANGING" : 1 << 2,
            "UNUSED3" : 1 << 3,
            "MEASURING" : 1 << 4,
            "WAITING_FOR_TRIG" : 1 << 5,
            "WAITING_FOR_ARM" : 1 << 6,
            "UNUSED7" : 1 << 7,
            "AVERAGING" : 1 << 8,
            "HARDCOPY_IN_PROGRESS" : 1 << 9,
            "WAITING_FOR_ACCEPT_REJECT" : 1 << 10,
            "LOADING_WATERFALL" : 1 << 11,
            "UNUSED12" : 1 << 12,
            "UNUSED13" : 1 << 13,
            "PROGRAM_RUNNING" : 1 << 14,
            "UNUSED15" : 1 << 15,
            }
    
class StandardEventRegister(Register):
    masks = {"OPERATION_COMPLETE" :  1 << 0,
            "REQUEST_CONTROL" :  1 << 1,
            "QUERY_ERROR" : 1 << 2,
            "DEVICE_DEPENDENT_ERROR" : 1 << 3, 
            "EXECUTION_ERROR" : 1 << 4, 
            "COMMAND_ERROR" : 1 << 5,
            "UNUSED6" : 1 << 6, 
            "POWER_ON" : 1 << 7,
            }        
        
class StatusRegister(Register):
    masks = {"USER_STATUS_SUMMARY" :  1 << 0,
            "UNUSED1" : 1 << 1,
            "DEVICE_STATE_SUMMARY" : 1 << 2,
            "QUESTIONABLE_STATUS_SUMMARY" : 1 << 3,
            "MESSAGE_AVAILABLE" : 1 << 4,
            "STANDARD_EVENT_SUMMARY" : 1 << 5,
            "REQUEST_SERVICE_MASTER_SUMMARY_STATUS" : 1 << 6,
            "OPERATION_STATUS_SUMMARY" : 1 << 7,
            }

class FFT35670A:
    """
    From page 1-10 GPIB Programming Document
    
    GPIB Queues in the Agilent 35670A

        Queues enhance the exchange of messages between the Agilent 35670A and other devices on the bus.
        The Agilent 35670 contains three queues.
            The input queue holds up to 128 bytes.
            The error queue temporarily stores up to 5 error messages.
            The output queue temporarily stores a single response message until it is read by a controller.
        For additional information about the use of queues in exchanging messages between an analyzer and an external controller, see the GPIB Programmer’s Guide.
    
    Command Synchronization
        Device commands can be divided into two broad classes:
            sequential commands
            overlapped commands
        Most device commands that you send to the analyzer are processed sequentially. A sequential command
        holds off the processing of subsequent commands until it has been completely processed. Some
        commands do not hold off the processing of subsequent commands. These are called overlapped
        commands and in many situations they require synchronization.
    """
    def __init__(self, host, port, standalone=False, standalone_log_level="DEBUG"):
        self.host = str(host)
        self.port = int(port)
        self.blocking_measurement_return_latency = 0.0
        
        # Flow control events
        self.exit_success = threading.Event()
        self.exit_timeout = threading.Event()

        if standalone is False:
            # getLogger will use ACS Python Logger
            self.logger = logging.getLogger(self.__class__.__name__)
            self.logger.setLevel(logging.DEBUG)
        else:
            # use standalone colored logger
            self.logger = get_colored_logger(self.__class__.__name__, standalone_log_level)
        
        self.logger.debug("test logger in class {}, module {}".format(self.__class__.__name__, __name__))
    
    # -------------------------------------------------------------------------    
    # Connection and socket functions
    # -------------------------------------------------------------------------
    def connect(self):
        """
        pyvisa.ResourceManager().open_resource() returns a VISA socket.  It is similar to a
        unix socket, but includes many convenience functions of VISA instrument control library
        This socket will be used to send SCPI string commands across a VISA TCP/IP interface to 
        control and query the device.
        """ 
        resource_description_string = "TCPIP0::{}::{}::SOCKET".format(self.host, self.port)
        self.logger.info("Open VISA resource {}".format(resource_description_string))
        self.socket = pyvisa.ResourceManager().open_resource(
            resource_description_string,
            write_termination="\r\n",
            read_termination="\n",
        )
        
        self.default_socket_timeout_ms = 30000
        self.logger.debug("Set default socket timeout: {}".format(self.default_socket_timeout_ms))
        self.socket.timeout = self.default_socket_timeout_ms

        self.logger.info("Connected VISA resource: {}".format(self.socket))

        return self.socket

    def disconnect(self):
        self.logger.debug("Clear socket")
        self.socket.clear()
        self.logger.debug("Close socket")
        self.socket.close()

    def logging_socket_write(self, cmd):
        """
        From Prologix programming manual page 9.  Section 8.2 "auto" command
        Some instruments generate “Query Unterminated” or “-420” error if they are addressed
        to talk after sending a command that does not generate a response (often called non-
        query commands). In effect the instrument is saying, I have been asked to talk but I have
        nothing to say. The error is often benign and may be ignored. Otherwise, use the
        ++read command to read the instrument response. 
        
        (SS)  Simple solution is to read back something from the instrument after every command.
        This could be helpful to detect errors too.
        """
        # *ESR? Reads and clears the Standard Event event register.
        # Append *ESR? to all outgoing commands to check for errors.  The only negative effect is that
        # we see "*ESR?" on the screen of 35670A as the *last* commands -- when we actually want to see
        # the command before the *ESR?
        cmd = cmd+"; *ESR?"
        self.logger.debug("{}() write: {}".format(inspect.stack()[1].function, cmd))
        self.socket.write(cmd)

        r = self.socket.read()
        self.logger.debug("{}() read standard event register: {}".format(inspect.stack()[1].function, r))
        return(r)
                
    def logging_socket_read(self):
        r = self.socket.read()
        self.logger.debug("{}() read: {}".format(inspect.stack()[1].function, r))
        return(r)
    
    def logging_socket_query(self, cmd):
        self.logger.debug("{}() write: {}".format(inspect.stack()[1].function, cmd))
        self.socket.write(cmd)
        r = self.socket.read()
        self.logger.debug("{}() read: {}".format(inspect.stack()[1].function, r))
        return(r)
    
    def binary_socket_query(self, cmd, complex_pairs=False, return_raw_bytes=False):
        # Reference Keysight 35670A GPIB Programming manual page 6-20.
        # If FORM:DATA has already been seet to binary transfer format ("FORM:DATA REAL, 64"), the FFT Analzyer
        # prepares the output queue of data on the socket in the format:
        # If FORMat [:DATA] REAL:
        #     <DEF_BLOCK> ::= #<byte><length_bytes> <1st_Y-axis_value> . . . <last_Y-axis_value>
        #     <byte> ::= number of length bytes to follow (ASCII encoded)
        #     <length_bytes> ::= number of data bytes to follow (ASCII encoded)
        self.logger.debug("{}() write: {}".format(inspect.stack()[1].function, cmd))
        self.socket.write(cmd)
        start_flag = self.socket.read_bytes(1)
        # self.logger.info("read start flag: {}".format(start_flag))
        if not start_flag == b'#':
            self.logger.error("start flag: {}.  Invalid message".format(start_flag))
        
        number_of_length_bytes = int(self.socket.read_bytes(1))
        # self.logger.debug("number_of_length_bytes type: {}, value: {}".format(type(number_of_length_bytes), number_of_length_bytes))
        
        data_length_bytes = int(self.socket.read_bytes(number_of_length_bytes))
        # self.logger.debug("data_length_bytes type: {}, value: {}".format(type(data_length_bytes), data_length_bytes))
        
        self.logger.debug("Reading {} bytes from FFT Analzyer ...".format(data_length_bytes))
        raw_bytes = self.socket.read_bytes(data_length_bytes)
        # self.logger.debug("raw_bytes type: {}, len: {}, value: {}".format(type(raw_bytes), len(raw_bytes), raw_bytes))
        
        end_flag = self.socket.read_bytes(1)
        #self.logger.info("read end flag: {}".format(end_flag))
        if not end_flag == b'\n':
            self.logger.error("end flag: {}.  Invalid message".format(start_flag))
        
        # Translate raw bytes into numpy array.
        # Keysight device uses big-endien byte order, 8 bytes per floating point sample
        # In order to read this type correctly, we tell numpy to read data as dtype ">f8".  If we specify
        # are not specific about the byte order, numpy.float64 uses Linux OS default which is little-endian 
        # (numpy dtype "<f8") 
        if complex_pairs is True:
            data_interleaved = np.frombuffer(raw_bytes, dtype=[('real', '>f8'), ('imag', '>f8')])
            data = data_interleaved["real"] + 1j * data_interleaved["imag"]
        else:
            data = np.frombuffer(raw_bytes, dtype=">f8")
            
        if return_raw_bytes is True:
            return(data, raw_bytes)
        else:
            return(data)

    # -------------------------------------------------------------------------
    # Device system utility functions
    # -------------------------------------------------------------------------
    def get_id(self):
        """
        Queries the unique ID string of the FFT Analyzer
        
        *IDN? Synchronization Required: no
        """
        return(self.logging_socket_query("*IDN?"))

    def reset_device(self):
        """
        Page 3-12
        Sets the analyzer to a reset state. In addition, *RST cancels any pending *OPC command or query.# Set complete system config to 
        default preset_instrument (stored localy on the analyzer)
        
        The reset state is similiar to the preset state. The preset state of each command is listed in the Attribute
        Summary. In some cases, however, a command’s reset state differs from its preset state. These
        commands (and their reset states) are listed below.

        *RST Synchronization Required: yes
        """
        self.logging_socket_write("*RST")

    def preset_device(self):
        """
        Page 21-24
        Returns most of the analyzer’s parameters to their preset states
        In addition to returning parameters to their preset states, this command does all of the following:
            Aborts any GPIB operations.
            Cancels any pending *OPC command or query.
            Clears the error queue.
            Clears all event registers (sets all bits to 0).
        
        PRES Synchronization Required: no
        """
        self.logging_socket_write("SYST:PRES")  
 
    def get_error(self):
        """
        Checks the system error queue.

        SYST:ERR? Synchronization Required: no
        """
        return(self.logging_socket_query("SYST:ERR?"))

    def get_operation_complete(self):
        """
        Page 3-7
        Some commands are processed sequentially by the analyzer. A sequential command holds off the
        processing of subsequent commands until it has been completely processed. However, some commands
        do not hold off the processing of subsequent commands. These commands are called overlapped
        commands. At times, overlapped commands require synchronization. The Attribute Summary for each
        command indicates whether it requires synchronization.

        Sets or queries completion of all pending overlapped commands.
        *OPC? Synchronization Required: no
        """
        return(int(float(self.logging_socket_query("*OPC?"))))
            
    def wait(self):
        """
        Use *WAI to hold off the processing of subsequent commands until all pending overlapped commands
        have been completed.
        Synchronization Required: no
        """
        self.logging_socket_write("*WAI")

    def _poll_timeout_function(self):
        self.exit_timeout.set()

    def _unblock_condition_function(self):
        return(self.exit_success.is_set() or self.exit_timeout.is_set())
    
    def wait_operation_complete(self, timeout=10, loop_period=0.2):
        """
        *OPC?
        Synchronization Required: no
        """       
        # clear old value of condition events so we must wait again until exit condition.
        self.exit_success.clear()
        self.exit_timeout.clear()

        # Create a watchdog timer 
        wdt = threading.Timer(timeout, self._poll_timeout_function)
        wdt.daemon = True
        wdt.start()

        ready_to_unblock = threading.Condition()
        
        # This thread must hold the reference to Mutex Lock to enable blocking
        ready_to_unblock.acquire()
        
        # Poll Device State register
        while not ready_to_unblock.wait_for(self._unblock_condition_function, timeout=loop_period):
            val = int(self.logging_socket_query("*OPC?"))
            self.logger.debug(val)
            
            if val == 1:
                self.exit_success.set()
                # Cancel the watchtdog timer because we can already exit the loop.
                wdt.cancel()

        self.logger.debug("unblocked! success : {}, timed out: {}".format(self.exit_success.is_set(), self.exit_timeout.is_set()))

    def blocking_capture_data(self, loop_period=0.2, verbose=False):
        """
        STAT:OPER:COND?, STAT:OPER:ENABLE?, STAT:OPER:EVENT?, STAT:OPER:NTR?, STAT:OPER:PTR?,  
        Synchronization Required: no
        """               
        # clear old value of condition events so we must wait again until exit condition.
        # self.exit_success.clear()
        # self.exit_timeout.clear()

        # # Create a watchdog timer
        # expected_measurement_duration = self.get_measurement_duration()
        # extra_timeout = 10
        # watchdog_timeout = expected_measurement_duration + extra_timeout

        # self.logger.debug("expected_measurement_duration: {}, extra_timeout: {}, total watchdog_timeout: {}".format(
        #     expected_measurement_duration, extra_timeout, watchdog_timeout))
            
        # ready_to_unblock = threading.Condition()

        # wdt = threading.Timer(watchdog_timeout, self._poll_timeout_function)
        # wdt.daemon = True
    
        # Page 3-3 *CLS
        # This command clears the Status Byte register. It does so by emptying the error queue and clearing
        # (setting to 0) all bits in the event registers of the following register sets:
        #   User Status
        #   Device State
        #   Questionable Voltage
        #   Limit Fail
        #   Questionable Status
        #   Standard Event
        #   Operation Status
        # self.logger.debug("clear event registers")
        # self.logging_socket_write("*CLS")
        
        # Page 4-2: ABORT
        # This command aborts any measurement in progress and resets the trigger system. ABOR forces the
        # trigger system to an idle state. ABORt forces the Measuring bit (bit 4) and the Averaging bit (bit 8) of the
        # Operation Status register to 0. To restart a measurement you must send the INIT:IMM command.

        # Page 11-3: INIT:IMM
        # This command starts a new measurement and ensures that any changes made to the analyzer’s state are
        # reflected in the measurement results. The new measurement is started immediately whether the current
        # measurement is running, “paused,” or completed. All data from the previous measurement is discarded
        # when the new measurement is started.
        # INIT[:IMM] causes the trigger system to initiate and complete one full trigger cycle.
        # If the command INIT:CONT ON has been sent, the INIT[:IMM] command has no affect.
        # The program message ABOR;:INIT:IMM serves a special synchronization function. When you send this
        # message to restart a measurement, the analyzer’s No Pending Operation (NPO) flag is set to 1 until the
        # measurement is complete. The two commands that test the state of this flag—*WAI and *OPC—allow
        # you to hold off subsequent actions until the measurement is complete. See chapter 3 of the GPIB
        # Programmer’s Guide for more information about synchronization.
            
        self.logger.info("trigger now.  wait for sampel capture ...")
        self.logging_socket_write("ABORT; :INIT:IMM; *WAI")

        # # Start watchdog timer to escape from infinite loop if GPIB device is waiting forever
        # wdt.start()

        # # This thread must hold the reference to Mutex Lock to enable blocking
        # ready_to_unblock.acquire()

        # # Poll Device State register
        # while not ready_to_unblock.wait_for(self._unblock_condition_function, timeout=loop_period):
        #     reg = OperationStatusRegister(int(self.logging_socket_query("STAT:OPER:EVENT?"))) # TODO - read condition or event register?
        #     if verbose is True:
        #         self.logger.debug(reg)
            
        #     if reg.bits["MEASURING"] == False:
        #         self.exit_success.set()
        #         # Cancel the watchtdog timer because we can already exit the loop.
        #         wdt.cancel()

        # self.logger.debug("unblocked! success : {}, timed out: {}".format(self.exit_success.is_set(), self.exit_timeout.is_set()))
        
        # if self.exit_timeout.is_set():
        #     raise TimeoutError
        
    def wait_calibration_complete(self, timeout=10, loop_period=0.2):
        """
        STAT:OPER:COND?, STAT:OPER:ENABLE?, STAT:OPER:EVENT?, STAT:OPER:NTR?, STAT:OPER:PTR?,  
        Synchronization Required: no
        """
        # clear old value of condition events so we must wait again until exit condition.
        self.exit_success.clear()
        self.exit_timeout.clear()

        # Create a watchdog timer 
        wdt = threading.Timer(timeout, self._poll_timeout_function)
        wdt.daemon = True
        wdt.start()

        ready_to_unblock = threading.Condition()
        
        # This thread must hold the reference to Mutex Lock to enable blocking
        ready_to_unblock.acquire()
        
        # Poll the Operation status register
        while not ready_to_unblock.wait_for(self._unblock_condition_function, timeout=loop_period):
            reg = OperationStatusRegister(int(self.logging_socket_query("STAT:OPER:COND?"))) # TODO read condition or event register?
            self.logger.debug(reg)
            
            if reg.bits["CALIBRATING"] == False:
                self.exit_success.set()
                # Cancel the watchtdog timer because we can already exit the loop.
                wdt.cancel()

        self.logger.debug("unblocked! success : {}, timed out: {}".format(self.exit_success.is_set(), self.exit_timeout.is_set()))

    def wait_display_ready(self, timeout=10, loop_period=0.2):
        """
        """        
        # clear old value of condition events so we must wait again until exit condition.
        self.exit_success.clear()
        self.exit_timeout.clear()

        # Create a watchdog timer 
        wdt = threading.Timer(timeout, self._poll_timeout_function)
        wdt.daemon = True
        wdt.start()

        ready_to_unblock = threading.Condition()
        
        # This thread must hold the reference to Mutex Lock to enable blocking
        ready_to_unblock.acquire()
        
        # Poll Device State register
        while not ready_to_unblock.wait_for(self._unblock_condition_function, timeout=loop_period):
            reg = DeviceStateRegister(int(self.logging_socket_query("STAT:DEV:EVENT?")))
            self.logger.debug(reg)
            
            if reg.bits["DISPLAY_READY"] == True:
                self.exit_success.set()
                # Cancel the watchtdog timer because we can already exit the loop.
                wdt.cancel()

        self.logger.debug("unblocked! success : {}, timed out: {}".format(self.exit_success.is_set(), self.exit_timeout.is_set()))
        
        # NOTE . Keysight document page 1-16.  Looks like this bit will not be very useful
        # Display Ready (bit 5) is set to 1 when measurement results are available. This is an event. 
        # The condition register will always return 0 for this bit.
            
    def set_command_echo_enabled(self, en):
        """
        Show SCPI commands on the analyzer front panel display
        DISP:GPIB:ECHO Synchronization Required: no
        """
        if en is True:
            self.logging_socket_write("DISP:GPIB:ECHO ON")
        else:
            self.logging_socket_write("DISP:GPIB:ECHO OFF")
        
    def set_keybaord_enabled(self, en):
        """
        Show SCPI commands on the analyzer front panel display
        SYST:KLOC Synchronization Required: no
        """
        if en is True:
            self.logging_socket_write("SYST:KLOC OFF")
        else:
            self.logging_socket_write("SYST:KLOC ON")

    def set_instrument_mode_FFT(self):
        """
        Selects the analyzer’s six major instrument modes to FFT (not HIST or CORR).
        INST:SEL Synchronization Required: no
        """
        self.logging_socket_write("INST:SEL FFT")

    # -------------------------------------------------------------------------
    # Hardware input config functions
    # -------------------------------------------------------------------------
    def calibrate(self):
        """
        CAL:AUTO Synchronization Required: no
        """
        self.logging_socket_write("CAL:AUTO ONCE; *WAI")
        self.wait_calibration_complete()

    def set_nchannels_measurement(self, nchan):
        """
        Enable number input channels of the analyzer using INput command
        one-channel, two-channel or four-channel measurements
        To select a two-channel measurement, send INP2 ON.
        INPut[1|2|3|4][:STATe] Synchronization Required: no
        """
        self.logging_socket_write("INP{} ON".format(nchan))
    
    def set_ac_input_coupling(self, channel):
        """
        Set voltage coupling to AC (remove zero Hz DC offsets)
        INPut[1|2|3|4]:COUPling Synchronization Required: no
        """
        self.logging_socket_write("INP{}:COUP AC".format(channel))

    # -------------------------------------------------------------------------
    # SENSE functions.  determine how measurement data is acquired 
    # including decimated span, FFT length, measurement capture duration
    # -------------------------------------------------------------------------
    def get_measurement_duration(self):
        """
        Typically, the user will request a frequency resolution.  The TCS software will set:
            - number of frequency bins in the 
                - SENS:FREQ:RES <fft_length>
            - frequency span.  This is achieved by decimation from instrument raw sample rate
                - SENS:FREQ:SPAN <span>
        
        The measurement duration required to achieve the
        measurement are derived results.  
        
        The 35670A allows the user to set the measurement duration via the command SENS:SWEEP:TIME.
        The analyzer will keep the FFT length (SENS:FREQ:RES) constant and automatically adjust 
        the frequency span to maintain the required relationship FREQ:SPAN = FREQ:RES / SWE:TIME
        """
        return(float(self.logging_socket_query("SENS:SWE:TIME?")))
    
    def get_fft_length(self):
        """
        Get the current value of FFT length from the device
        """
        # NOTE: convert string to float first because format is something like "+128"
        return(int(float(self.logging_socket_query("SENS:FREQ:RES?"))))
    
    def set_fft_length(self, fft_length):
        """
        Specifies the frequency measurement resolution for FFT and swept sine instrument modes
        SENS:FREQ:RES Synchronization Required: no
        """
        self.logging_socket_write("SENS:FREQ:RES {}".format(fft_length))
    
    def get_span(self):
        """
        Get the current value of decimated frequency span from the device.
        """
        return(float(self.logging_socket_query("SENS:FREQ:SPAN?")))
    
    def set_span(self, span):
        """
        Allowable values for the frequency span are determined by the following formula:
            (maximum span) / 2**n
            Where:
            - n is the decimation factor (integer) 0 ≤ n ≤ 19 
            - maximum span = 51.2 kHz for 2 Channel measurement

        In the case of a 2-channel measurement and decimation values 0 to 19 result in
        these available values of span.
        
        [5.120000e+04 2.560000e+04 1.280000e+04 6.400000e+03 3.200000e+03
        1.600000e+03 8.000000e+02 4.000000e+02 2.000000e+02 1.000000e+02
        5.000000e+01 2.500000e+01 1.250000e+01 6.250000e+00 3.125000e+00
        1.562500e+00 7.812500e-01 3.906250e-01 1.953125e-01 9.765625e-02]
        
        
        If span is set to a value that is not on the list, the analyer rounds UP
        to the next available value.
        """
        self.logging_socket_write("SENS:FREQ:SPAN {}".format(span))

    def get_center(self):
        """
        
        """
        return(float(self.logging_socket_query("SENS:FREQ:CENT?")))
    
    def set_center(self, c):
        """
        Sets the center frequency for Digital Downconverter (DDC) which then creates complex-valued output samples
        """
        self.logging_socket_write("SENS:FREQ:CENT {}".format(c))
        
    def set_window_function(self, winType):
        """
        Selects the type of windowing function for FFT measurements
        HANNing|FLATtop|UNIForm| FORCe|EXPonential|LAG|LLAG
        
        The channel specifier is ignored for the Hanning, flattop, and uniform window functions.
        Synchronization Required: no
        """
        self.logging_socket_write("SENS:WIND:TYPE {}".format(winType))

    def set_voltage_range_auto(self, ch, state):
        """
        Synchronization Required: no
        """
        if state == True:
            self.logging_socket_write("SENS:VOLT{}:RANG:AUTO {};*WAI".format(ch, "ON"))
        else:
            self.logging_socket_write("SENS:VOLT{}:RANG:AUTO {};*WAI".format(ch, "OFF"))
   
    # -------------------------------------------------------------------------
    # CALC functions. control the processing of measurement data
    # -------------------------------------------------------------------------
    def set_active_traces(self, trace_id):
        """ 
        Selects the active trace(s).  Available traces are "A", "B", "C", "D". 
        If only one trace is active, trace_id is only one character.  For example trace_id = "A"  
        In addition to the individual traces, you can set multiple traces to be active at any time.
        If more than one trace should be activated at the same time, characters are concatenated.
        
        For example trace_id = "ABCD" to set all 4 traces active (next commands will apply to all 4 traces)

        The trace_pairs "AB", "CD", and "ABCD" are valid for trace_id
        The trace pairs "AC" and "BD" are not valid for trace_id 

        If "AB" or "ABCD" are active, trace A is considered the “most active trace.” 
        If traces "CD" are active, trace C is considered the “most active trace.”
        
        The specifier in CALC command, CALCULATE[1|2|3|4], is not valid for the ACT sub-command.

        Synchronization Required: no
        """
        self.logging_socket_write("CALC:ACT {}".format(trace_id))
             
    def set_axis_format(self, trace_id, format):
        """
        Page 6-29
        Selects a coordinate system for displaying measurement data and for 
        transferring coordinate transformed data to a controller.

        Parameters:
        -----------
            trace_id : int {1,2,3,4}
            format : str {"MLOG", "PHAS"}
                "MLIN" : displays linar magnitude on Y-axis
                "MLOG" : displays logarithmic magnitude data on a linear Y-axis scale
                "PHAS" : selects a coordinate system that displays wrapped phase along the Y-axis
                "REAL" : displays the REAL part of a complex number on the Y-axis
                "IMAG" : displays the IMAG part of a complex number on the Y-axis
                "NYQ" : displays a Nyquist diagram (imaginary numbers along the Y-axis and
                        real numbers along the X-axis), 
        
        Phase wraps at the display boundaries. If you change the display boundaries with the
        DISP:WIND:TRAC:Y:TOP and DISP:WIND:TRAC:Y:BOTTOM commands, you change where phase
        wraps on the display.
        Synchronization Required: yes
        """
        
        self.logging_socket_write("CALC{}:FORM {}".format(trace_id, format))
    
    def set_amplitude_unit(self, trace_id, unit):
        """
        Page 6-111: Specifies the unit of voltage amplitude for both linear and logarithmic scales
        
        Parameters:
        ----------
            trace_id : int {1,2,3,4}
            unit : str {'PEAK', 'RMS', 'PP'}
                "PEAK" : peak voltage
                "PP" : peak-to-peak voltage
                "RMS" : root mean squared voltage        
            
        Synchronization Required: yes
        """
        self.logging_socket_write("CALC{}:UNIT:AMPL {};*WAI".format(trace_id, unit))

    def set_decibel_reference(self, trace_id, unit):
        """
        Page 6-114: Specifies the reference for dB magnitude trace coordinates.
        
        Parameters:
        ----------
            trace_id : int {1,2,3,4}
            unit : str {'DBM'}
                "DBM" : reference the dB magnitude to 1 milliwatt        
            
        Synchronization Required: yes
        """
        self.logging_socket_write("CALC{}:UNIT:AMPL RMS;*WAI".format(trace_id))
        self.logging_socket_write("CALC{}:UNIT:DBR \"{}\";*WAI".format(trace_id, unit))
        
    def set_yaxis_angle_unit(self, trace_id, unit):
        """
        Page 6-113: Specifies the reference for dB magnitude trace coordinates.
        
        Parameters:
        -----------
            trace_id : int {1,2,3,4}
            unit: str {"DEGR", "RAD"}
                "DEGR" : degrees
                "RAD" : radians
            
        Synchronization Required: yes
        """        
        self.logging_socket_write("CALC{}:UNIT:ANGL {};*WAI".format(trace_id, unit))

    def calculate_absolute_power_spectrum(self, trace_id, input_channel):
        """
        Page 6-23: Selects the measurement data to be displayed in the specified trace.
        To select the power spectrum function for the specified channel, send CALC[1|2|3|4]:FEED XFR:POW [1|2|3|4].
        Synchronization Required: yes
        """
        self.logging_socket_write("CALC{}:FEED 'XFR:POW {}';MATH:STAT OFF;*WAI".format(trace_id, input_channel))
    
    def calculate_time_volts(self, trace_id, input_channel):
        """
        Page 6-23: Selects the measurement data to be displayed in the specified trace.
            To select the most recent time record for the specified channel, send CALC[1|2|3|4]:FEED
            ‘XTIM:VOLT [1|2|3|4]’.
        """
        self.logging_socket_write("CALC{}:FEED 'XTIM:VOLT {}';MATH:STAT OFF;*WAI".format(trace_id, input_channel))

    def calculate_relative_power_spectrum(self, trace_id):
        """
        Page 6-23: Selects the measurement data to be displayed in the specified trace.
        In FFT analysis instrument mode (INST:SEL FFT), the following commands are available:
          To select the most recent frequency response function, send CALC[1|2|3|4]:FEED ‘XFR:POW:RAT
          [2,1|3,1|4,1|4,3]’. The analyzer must be in 2 channel instrument mode (INPut 2 ON) for selection
          2,1. INPut 4 must be ON for selections 3,1 and 4,1. INPut 4 must be ON and SENse:REFerence
          must be PAIR for selection 4,3.
          # TODO on the front panel, I see marker ch1 = 4, marker ch2 = -4.  So the difference is 8, but wrong way.
          # TODO what is dBm rms.  looks like the value of dBm + 6.  why?
          Synchronization Required: yes
        """
        self.logging_socket_write("CALC{}:FEED 'XFR:POW:RAT 2,1';MATH:STAT OFF;*WAI".format(trace_id))
        
    def enable_marker_peak_tracking(self, trace_id):
        """
        Enable the main marker for <trace_id> and its annotation. 
        The analyzer displays the X-axis and Y-axis values at the top of the grid
        CALCulate[1|2|3|4]:MARKer[:STATe] Synchronization Required: no
        CALCulate[1|2|3|4]:MARKer:MAXimum[:GLOBal] Synchronization Required: no
        CALCulate[1|2|3|4]:MARKer:MAXimum[:GLOBal]:TRACk Synchronization Required: no
        """
        self.logging_socket_write("CALC{}:MARK ON".format(trace_id))

        # Moves the main marker to the highest point on the specified trace
        self.logging_socket_write("CALC{}:MARKER:MAX:GLOB".format(trace_id))

        # Set peak tracking function of marker ON.
        self.logging_socket_write("CALC{}:MARK:MAX:GLOB:TRAC ON".format(trace_id))

    def set_center_to_marker(self, trace_id):
        """
        Get the X-axis value of marker, then sets the center frequency of the instrument to that value
        "re-center" display based on detected signal
        [SENSe:]FREQuency:CENTer Synchronization Required: no
        """
        marker = float(self.logging_socket_query("CALC{}:MARK:POS?".format(trace_id)))
        self.logger.debug("marker {} value: {} kHz".format(trace_id, marker/1000))
        self.logging_socket_write("SENS:FREQ:CENT {}".format(marker))

    def get_frequency_for_doppler_correction(self):
        return(float(self.logging_socket_query("CALC1:MARK:X?")))
        
    # -------------------------------------------------------------------------
    # DISP functions. control the analyzer’s presentation of data on its front-panel display
    # -------------------------------------------------------------------------
    def set_display_format_quad(self):
        """
        # Selects a format for displaying trace data.
        # QUAD, the analyzer displays four trace boxes to show all relevant data at the same time
        DISPlay:FORMat Synchronization Required: no
        """
        self.logging_socket_write("DISP:FORM QUAD")

    def autoscale_axis_once(self, trace_id):
        """
        Autoscale the axis once. then disable auto after the scale is set.
        DISPlay[:WINDow[1|2|3|4]]:TRACe:Y[:SCALe]:AUTO Synchronization Required: no
        """
        # TODO - do we need this first command to disable auto scale?
        # self.logging_socket_write("DISP:WIND{}:TRAC:Y:SCAL:AUTO OFF".format(trace_id))
        self.logging_socket_write("DISP:WIND{}:TRAC:Y:SCAL:AUTO ONCE".format(trace_id))

    # -------------------------------------------------------------------------
    # High level functions to achieve the Holography 2 channel measurement
    # -------------------------------   ------------------------------------------
    def provision(self):
        """
        Basic provision of the device to enable display and inputs
        """
        # Device utility
        self.socket.clear()
        self.preset_device()
        self.set_command_echo_enabled(True)
        self.set_keybaord_enabled(True)

        # Hardware input
        self.set_nchannels_measurement(2)
        self.set_ac_input_coupling(1)
        self.set_ac_input_coupling(2)
        # self.calibrate() # TODO - this kills the socket and we have to power cycle Prologix to connect again.

        # Display
        self.set_display_format_quad()
        self.set_active_traces("ABCD")
                
    def configure(self, freq_res, fft_length):
        """
        Configuration for relative power magnitude and phase measurement. ready to read holodata.
        This configuration does not search for any signal.  It only conigures the measurements and calculations.
        Independent of received power on inputs.
        """
        self.freq_res = freq_res
        self.fft_length = fft_length
        # Enable continuous triggering to  measure signal, set markers, scale, etc.
        # For the real data recording, we will disable continuous and use manual trigger each capture in the get_holodata function.
        self.logging_socket_write("INIT:CONT ON")

        # SENS data acquisition control
        self.set_instrument_mode_FFT()
        self.set_fft_length(fft_length)
        self.set_window_function("FLAT")

        # Display power spectrum of channel 1 on trace A | 1
        self.calculate_absolute_power_spectrum(1, 1)
        self.set_axis_format(1, "MLOG")
        self.set_decibel_reference(1, "DBM")
        self.set_amplitude_unit(1, "RMS")
        # NOTE: Marker peak tracking ONLY on trace 1
        self.enable_marker_peak_tracking(1)  
        
        # Display power spectrum of channel 2 on trace B | 2
        self.calculate_absolute_power_spectrum(2, 2)
        self.set_axis_format(2, "MLOG")
        self.set_decibel_reference(2, "DBM")
        self.set_amplitude_unit(2, "RMS")
        
        # Display time series voltages of channel 1 on trace C | 3.
        # NYQ is complex I/Q constellation
        self.calculate_time_volts(3, 1)
        self.set_axis_format(3, "NYQ")
        self.set_amplitude_unit(3, "PEAK")

        # Display time series voltages of channel 2 on trace D | 4
        # NYQ is complex I/Q constellation
        self.calculate_time_volts(4, 2)
        self.set_axis_format(4, "NYQ")
        self.set_amplitude_unit(4, "PEAK")
        
    def search_and_center(self, freq_res):
        """
        Parameters:
        ----------
            freq_res: actual frequency resolution.  This function will not validate
                if the value is in the allowed list.  That must be done before using this function

        In typical operations, the user requests a frequency resolution in units of Hz / FFT bin.
        The actual resolution used by the analyzer to perform the measurements is is the nearest
        available value that is finer in resolution than requested.  For example, if the user requests
        105 Hz / bin, the software will round down to 100 Hz / bin.

        To ensure that the signal does not drift out of the decimated bandwidth ("SENS:FREQ:SPAN"), 
        we would like to center the SPAN on the detected signal of interest.  Therefore we follow the
        logic to start at the instrument full non-decimated bandwdith (51.2 kHz for 2 channel measurement),
        Then, iterate through the available values of 2**N decimation to "center and zoom" 
        the FFT analyzer span until we arrive at the user's requested frequency resolution

        The end result is a span that provides the user frequency resolution, and the signal of interest
        in the center.
        """
        # Calculate list of decimation values to iterate based on current FFT length
        # and user-selected frequency resolution.
        self.set_span(6400)
        self.set_center(20000)
        # sample_rate = self.get_span()
        # fft_length  = self.get_fft_length()
        # freq_res_no_decimation = sample_rate / fft_length
        # decimation_factor = freq_res_no_decimation / freq_res
        # self.logger.debug("sample_rate: {}, fft_length: {}, freq_res_no_decimation: {}".format(
        #     sample_rate, fft_length, freq_res_no_decimation))

        # decimation_exponent = np.log2(decimation_factor)
        # self.logger.debug("decimation_factor: {}, decimation_exponent: {}".format(decimation_factor, decimation_exponent))

        # # Iterate on decimation exponent from 0 (no decimation, maximum frequency span)
        # # to the decimation_exponent that results in the user selected frequency resolution
        # # NOTE: numpy arange function does not generate the endpoint, so we must add + 1
        # decimation_exponents = np.arange(decimation_exponent + 1).astype(int)
        # self.logger.debug("decimation_exponents: {}".format(decimation_exponents))
        # decimation_factors = 2 ** decimation_exponents
        # self.logger.debug("decimation_factors: {}".format(decimation_factors))
        # spans = sample_rate / decimation_factors

        # self.logger.debug("Iterate spans: {}".format(spans))
        # for s in spans:        
        #     self.logger.info("try span: {} kHz, (freq_res: {} Hz)".format(s/1000.0, s/fft_length))
        #     # NOTE: FFT Analyzer does not allow wrap around to negative frequencies.  Therefore,
        #     # when we set the center frequency to any value that is not exactly in the center of the
        #     # current span, the FFT analyzer will automatically reduce the span to a size that is
        #     # small enough that it always fits within the range 0 .. sample_rate without any wrap
        #     # around.
        #     self.set_center_to_marker(1)
            
        #     # If the measured signal marker was within within (+/-) 25% of center, 
        #     # but not *exactly* in the center, The FFT analyzer would have already reduced the
        #     # span by a factor of 2 to guarantee that the entire spectrum is within 0 to sample_rate
        #     # without wrap around.  In this case, the folowing fuction call to set the span 1 size smaller
        #     # has no effect, and no error.

        #     # If the measured signal marker was outside the middle (+/-) 25% of current span.
        #     # The FFT analyzer would have already reduced the span by many factors of 2 until the entire
        #     # spectrum fits within 0 to sample rate.  In this case, the function call to set_span
        #     # will have no effect, and indicate a non-zero error condition in *ESR?
        #     self.set_span(s)
        #     self.logger.debug("measurement duration after set span {} s".format(self.get_measurement_duration()))

        #     self.blocking_capture_data()
        
        # # The last value in the list of spans results in a frequency resolution equal to the user
        # # request freq_res.  If the instrument can arrive at this span,
        # # the FFT Analyzer is set to correct frequency resolution *and* centered on the signal.
        
        # # IF the loop has finished and the FFT analyzer is unable to set the span to the last value
        # # in the list of spans, this is an indicator that that the marker was too near zero or sample_rate.
        # # Likely means the signal was not detected. So, return center frequency to default 
        # # midpoint and maximum span.
        # current_span = self.get_span()
        # if current_span != spans[-1]:
        #     self.logger.warning("current_span: {} not equal to last requested span: {}. signal not found?".format(current_span, spans[-1]))
        #     self.logger.warning("Reset center: {}, span: {}".format(sample_rate / 2, sample_rate))
        #     self.set_center(sample_rate/2)
        #     self.set_span("MAX")
        # else:
        #     self.logger.info("current_span {} == last requested span {}. Signal found?".format(current_span, spans[-1]))



        self.set_voltage_range_auto(1, True)
        self.set_voltage_range_auto(1, False)
        self.set_voltage_range_auto(2, True)
        self.set_voltage_range_auto(2, False)
        
        self.wait_display_ready()
        
        for trace_id in [1,2,3,4]:
            self.autoscale_axis_once(trace_id)

    def measurement_start(self, search=True):
        """
        The purpose of this function is to find the signal somewhere in the spectrum, and re-center the axes based
        on the actual frequency of received signal.  The signal can possibly be offset from the design frequency
        of 20 kHz due to
            - Doppler shift (relative motion between satellite and moving Ku-band receiver)
            - Drift in frequency conversion (Ku band to 20 kHz) from temperature variation in electronics
        
        NOTE: This function depends on a signal present on CH1.  CH1 has the low-gain (wide beamwidth) reference antenna.
        Even if signal power is received at a low level on this channel, we should be able to detect
        and find the center frequency before we are concerned with accruate pointing of the high-gain (narrow beam from 40m reflector) 
        DUT antenna signal on CH2

        In the operational use case, the antenna should already be tracking the object before we call this function
        """
        if search is True:
            self.search_and_center(self.freq_res)

        # Now that the signal should be aligned in the center based on channel 1, set all other markers to 
        # to automatically follow the X-value (frequency) of marker 1
        
        # Page 6-56: 
        # Couples the markers on all traces with the marker of the most active trace.
        # This command moves the main marker of all traces to the same X-axis point as the marker of the most
        # active trace. This ties the movement of the main markers together.
        # The most active trace is:
        #   Trace A if all traces are active or if trace A and trace B are the only active traces.
        #   Trace C if trace C and trace D are the only active traces.
        #   The active trace if only one trace is active.
        #   The trace specifier is ignored.
        #   The position of each marker is updated, even when the trace is not displayed. You cannot move a marker
        #       beyond the maximum number of points in the most active trace.
        #   When coupled markers are used in a zoomed measurement (starting frequency > 0), the first point is
        #       assumed to be zero.
        # Note
        # This command couples the markers for each trace by X-axis position; not X-axis values.
        self.logging_socket_write("CALC:MARK:COUP:STAT ON")
            
        # Configure device to send some data in binary format instead of ASCII.  This applies to most queries where
        # we request an entire buffer of data, not only 1 simple value.  
        # Page 9-2
        # FORM:DATA only affects data transfers initiated by the following commands:
        #   CALC:DATA?
        #   CALC:CFIT:DATA?
        #   CALC:LIM:LOW:REP?
        #   CALC:LIM:LOW:SEGM
        #   CALC:LIM:UPP:REP?
        #   CALC:LIM:UPP:SEGM
        #   CALC:SYNT:DATA?
        #   CALC:WAT:DATA?
        #   PROG:SEL:NUMB
        #   TRAC[:DATA]
        #   TRAC:WAT[:DATA]
        #   TRAC:X[:DATA]?
        #   TRAC:Z[:DATA]?
        #
        # We plan to use CALC:DATA? to read complete time series data array or spectrum data array.
        # Depends on which type of data is configured by the FEED command for each CALC data array
        self.logging_socket_write("FORM:DATA REAL, 64")

        # Disable continuous triggering that was used before to continuously measure signal, set markers, scale, etc.
        # For the real data recording, we manually trigger each capture in the get_holodata function.
        self.logging_socket_write("INIT:CONT OFF")

        # Get some values ONCE at the beginning of the scan.  They will not change for each measurement point
        self.sample_capture_duration = self.get_measurement_duration()
        self.ff = self.binary_socket_query("CALC1:X:DATA?")
        self.tt = self.binary_socket_query("CALC3:X:DATA?")
        self.logger.debug(f"len(ff): {len(self.ff)}, ff: {self.ff}")
        self.logger.debug(f"len(tt): {len(self.tt)}, tt: {self.tt}")

        # Define numpy array datatype with dimensions ready to receive data as configured.
        self.data_len = len(self.ff)
        self.window = signal.windows.flattop(self.data_len)
        self.dtype_msg_holodata = generate_dtype_msg_holodata(self.data_len)

        # Check sample capture duration after all config is applied and configure socket timeout
        duration_ms = 1000 * self.get_measurement_duration()
        total_timeout = duration_ms + self.default_socket_timeout_ms
        self.logger.debug("set socket timeout expected measurement: {} ms + default_socket_timeout: {} ms = total_timeout: {} ms".format(
            duration_ms, self.default_socket_timeout_ms, total_timeout))
        self.socket.timeout = total_timeout


    def get_holodata(self):
        """
        Get one Holodata measurement assuming that configure_holodata was called and all traces are setup properly.
        """
        # Page 20-12
        # TODO (03/2024) try to run an observation without using these transition registers.  If it doesn't work
        # as expected, come back around and figure out how to use these registers for flow control. 

        # Sets bits in most enable and transition registers to their default state.
        # self.logging_socket_write("STAT:PRES")
        
        # Enable only Display Ready bit of the DeviceStateRegister
        # self.logging_socket_write("STAT:DEV:ENABLE {}".format(DeviceStateRegister.masks["DISPLAY_READY"]))

        # Toggle on positive transition of DISPLAY_READY
        # self.logging_socket_write("STAT:DEV:PTR {}".format(DeviceStateRegister.masks["DISPLAY_READY"]))

        # (page 3-14)
        # The analyzer requests service from the active controller when one of the following occurs:
        # - A bit in the Status Byte register changes from 0 to 1 while the corresponding bit of the Service
        #   Request enable register is set to 1.
        # 
        # - A bit in the Service Request enable register changes from 0 to 1 while the corresponding bit of the
        #   Status Byte register is set to 1.
        # self.logging_socket_write("*SRE {}".format(StatusRegister.masks["DEVICE_STATE_SUMMARY"]))
        timestamp_start_capture_samples = Time.now()
        
        self.blocking_capture_data()
        
        # Get the timestamp ASAP after measurement data is captured
        timestamp_end_capture_samples = Time.now()
                       
        # Subtract half of the data capture time from current timestamp to find the 
        # time stamp at the middle of the data recording time.
        self.logger.debug("timestamp_start_capture_samples: {}, timestamp_end_capture_samples: {}, measurement_duration: {}, latency: {}".format(
            timestamp_start_capture_samples,
            timestamp_end_capture_samples,
            self.sample_capture_duration,
            self.blocking_measurement_return_latency,
        ))
        
        timestamp_apy = timestamp_start_capture_samples + (self.sample_capture_duration / 2) * units.s - self.blocking_measurement_return_latency * units.s
        self.logger.debug("timestamp: {}".format(timestamp_apy.iso))
        
        # Get complex-valued time series voltages from the device
        sn_ref = self.binary_socket_query("CALC3:DATA?", complex_pairs=True)
        sn_tst = self.binary_socket_query("CALC4:DATA?", complex_pairs=True)

        # Calculate FFT spectrum
        spectrum_ref_complex = np.fft.fftshift(np.fft.fft(sn_ref * self.window)) / self.data_len
        spectrum_tst_complex = np.fft.fftshift(np.fft.fft(sn_tst * self.window)) / self.data_len
        spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
        
        # TODO (SS 12/2024): Calculate precise frequency and use to apply a frequency correction
        # to center the signal at 0 Hz. Then lowpass filter and resample (using scipy decimate) to reduce
        # bandwidth further (and reduce noise.)

        # Create an empty Holodata message structure
        msg_holodata = np.squeeze(np.zeros(1, dtype=self.dtype_msg_holodata))
        
        # Fill basic timestamp and sample duration
        msg_holodata["time_mjd"] = timestamp_apy.mjd
        msg_holodata["sample_capture_duration"] = self.sample_capture_duration
        
        # Fill time domain data
        msg_holodata["timeseries_time_axis"] = self.tt
        msg_holodata["timeseries_phase_ch1_rad"] = np.angle(sn_ref)
        msg_holodata["timeseries_phase_ch2_rad"] = np.angle(sn_tst)
        
        # Fill frequency domain data
        msg_holodata["spectrum_freq_axis"] = self.ff
        msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
        msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
        msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
        msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))
        
        # Find peak of CH1 (REF) magnitude
        marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
        # Fill interesting marker data points
        msg_holodata["marker_index"] = marker_index
        msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
        msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
        msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
        msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
        msg_holodata["marker_val_phase_ch1_deg"] = np.degrees(np.angle(spectrum_ref_complex))[marker_index]
        msg_holodata["marker_val_phase_ch2_deg"] = np.degrees(np.angle(spectrum_tst_complex))[marker_index]
        msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]
        
        return msg_holodata

def get_colored_logger(name, log_level):
    fmt_scrn = "%(asctime)s.%(msecs)03d [%(levelname)s] %(name)s.%(funcName)s(): %(message)s"

    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red"},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta"},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    # creating a handler to log on the console
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)

    logger = logging.getLogger(name)
    logger.handlers = []
    logger.addHandler(handler_screen)

    if log_level.upper() == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif log_level.upper() == "INFO":
        logger.setLevel(logging.INFO)
    elif log_level.upper() == "WARNING":
        logger.setLevel(logging.WARNING)
    elif log_level.upper() == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.DEBUG)
        logger.error("invalid parameter log-level. Select level DEBUG")

    return logger

if __name__ == "__main__":
    logger = get_colored_logger("__main__", "DEBUG")
    logger.debug("test logger in module {}".format(__name__))
    logger.info("Starting FFT35670A client")

    self = FFT35670A(holofft_host, holofft_port, True, "DEBUG")
    self.connect()
    
    #self.disconnect()
