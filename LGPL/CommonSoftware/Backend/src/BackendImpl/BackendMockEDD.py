# -*- coding: utf-8 -*-
# import standard modules
import threading
import logging
import os
import subprocess
import time
import json
from json.decoder import JSONDecodeError
import numpy as np

# import ACS modules
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Nc.Supplier import Supplier
from Acspy.Clients.SimpleClient import PySimpleClient

# import TNRT modules
import BackendMod__POA
import BackendMod
import BackendErrorImpl
import DataAggregatorError

from CentralDB import CentralDB
import cdbConstants
import BackendDefaults
import default_param_edd


class BackendMockEDD(BackendMod__POA.BackendMockEDD, ACSComponent, ContainerServices, ComponentLifecycle):
    """
    docstring
    """

    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logInfo(
            "test logger in class {}, module {}".format(self.__class__.__name__, __name__)
        )

        # Flow control events
        self.event_stop_status_notification_loop = threading.Event()
        self.fits_mock_server_proc = None

        self.cdb = CentralDB()

        self.logger.logInfo("__init__ (constructor) complete")

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.
    # ------------------------------------------------------------------------------------

    def initialize(self):
        self.logger.logInfo("initialize() start")

        self.logger.logInfo(
            "Creating Notification Channel Suppler {}".format(BackendMod.CHANNEL_NAME_STATUS)
        )

        self.sc = PySimpleClient()
        self.da = None
        self.da_name = "DataAggregator"
        self.da = self.connect_component(self.da_name)

        self.status_supplier = Supplier(BackendMod.CHANNEL_NAME_STATUS)
        self.logger.logInfo("initialize() finished")

    def execute(self):
        # Do nothing.  Use initialize() function
        pass

    def aboutToAbort(self):
        # Do nothing here.   Use cleanup() function
        pass

    def cleanUp(self):
        self.logger.logInfo("cleanup() start")

        self.logger.logInfo("Disconnect ACS Notification suppliers")

        if self.status_supplier != None:
            self.status_supplier.disconnect()

        self.logger.logInfo("Disconnect ACS Notification consumers")
        try:
            self.sc.releaseComponent(self.da_name)
            self.sc.disconnect()
            self.da = None
        except Exception as e:
            self.logger.logError("Cannot release component reference for %s" % self.da_name)

        self.logger.logInfo("cleanup() finished")

    # ------------------------------------------------------------------------------------
    # IDL interface functions for use from outside this object
    # ------------------------------------------------------------------------------------

    def client_start(self):
        self.logger.logInfo("client_start")

    def client_stop(self):
        self.logger.logInfo("client_stop")

    def provision(self, preset_config_name):
        self.preset_config_name = preset_config_name
        if "spectrometer" in self.preset_config_name.lower():
            self.nsections=2
            self.logger.logInfo("provision: {}, set nsections={}".format(self.preset_config_name, self.nsections))
        return "provision success"

    def deprovision(self):
        self.logger.logInfo("deprovision")
        return "deprovision success"

    def configure(self, config_json):
        try:
            config_dict = json.loads(config_json)
            self.logger.logInfo("config dict: {}".format(config_dict))

            if "spectrometer" in self.preset_config_name.lower():
                sampling_rate = default_param_edd.SPECTROMETER_SAMPLING_RATE_DEFAULT
                self.logger.debug("default sampling_rate: {}".format(sampling_rate))
            
                ref_channel = default_param_edd.SPECTROMETER_REF_CHANNEL_DEFAULT
                self.logger.debug("default ref_channel: {}".format(ref_channel))

                predecimation_factor = default_param_edd.preset_config_name_setting[self.preset_config_name]['predecimation_factor']
                self.logger.debug("default predecimation_factor: {}".format(predecimation_factor))
                
                decimated_sampling_rate = sampling_rate / predecimation_factor
                self.logger.debug("default decimated_sampling_rate: {}".format(decimated_sampling_rate))

                f0_nyquist_zone_2 = decimated_sampling_rate / 2
                self.logger.debug("default f0_nyquist_zone_2: {}".format(f0_nyquist_zone_2))

                # Get downconverter frequency that was set by Frontend
                down_converter_freq = self.cdb.get(cdbConstants.DOWN_CONVERTER_FREQ, float) or 0
                self.logger.debug("cdb down_converter_freq: {}".format(down_converter_freq))

                # Get fft_length and naccumulate from the first product name
                # If there are more than 1 product in the list, don't use.
                product_name = list(config_dict["products"].keys())[0]
                self.logger.logInfo("product_name: {}".format(product_name))
                fft_length = config_dict["products"][product_name]["fft_length"]
                naccumulate = config_dict["products"][product_name]["naccumulate"]

                actual_integration_time = naccumulate * (predecimation_factor / sampling_rate) * fft_length
                self.logger.debug("actual_integration_time: {}".format(actual_integration_time))
                
                actual_freq_res = decimated_sampling_rate / fft_length
                self.logger.debug("actual_freq_res: {}".format(actual_freq_res))
                
                rf_freq_at_ref_channel = f0_nyquist_zone_2 + (ref_channel * actual_freq_res) + down_converter_freq
                self.logger.debug("rf_freq_at_ref_channel: {}".format(rf_freq_at_ref_channel))

                # NOTE:  About calculation of CHANNELS for .mbfits ARRAYDATA header
                # - In the case of TCS DataSimulator, nchannels = fft_length 
                # because the simulator generates complex valued data and uses both 1st and 2nd nyquist zones.
                # - In the case of the real EDD Spectrometer, nchannels = fft_length / 2.
                # because it performs the FFT on real-valued data and sends only the second Nyquist zone data 
                # out of edd_fits_server
                nchannels = int(fft_length)
                self.logger.debug("nchannels: {}".format(nchannels))

                try:
                    ## Set metadata to cdb
                    self.cdb.set(cdbConstants.NUSEFEED, default_param_edd.preset_config_name_setting[self.preset_config_name]['nusefeed'])
                    self.cdb.set(cdbConstants.REF_CHANNEL, ref_channel)
                    self.cdb.set(cdbConstants.FREQRES, actual_freq_res)
                    self.cdb.set(cdbConstants.INTEGRATION_TIME, actual_integration_time)
                    self.cdb.set(cdbConstants.CHANNELS, nchannels)
                    self.cdb.set(cdbConstants.FREQ_AT_REF_CH, rf_freq_at_ref_channel)

                    # Get back CDB values, log some debug messages.
                    self.logger.debug("cdb NUSEFEED: {}".format(self.cdb.get(cdbConstants.NUSEFEED, int)))
                    self.logger.debug("cdb REF_CHANNEL: {}".format(self.cdb.get(cdbConstants.REF_CHANNEL, int)))
                    self.logger.debug("cdb FREQRES: {}".format(self.cdb.get(cdbConstants.FREQRES, float)))
                    self.logger.debug("cdb INTEGRATION_TIME: {}".format(self.cdb.get(cdbConstants.INTEGRATION_TIME, float)))
                    self.logger.debug("cdb CHANNELS: {}".format(self.cdb.get(cdbConstants.CHANNELS, int)))
                    self.logger.debug("cdb FREQ_AT_REF_CH: {}".format(self.cdb.get(cdbConstants.FREQ_AT_REF_CH, float)))
                    
                    # TODO (SS 7/2024): Why is this cdb setting about ACU here?  Remove this?
                    self.cdb.set(cdbConstants.ACU_IP, BackendDefaults.AcuIP)

                except Exception as e:
                    acs_corba_exception =  BackendErrorImpl.InternalErrorExImpl()
                    acs_corba_exception.addData(str(e))
                    raise acs_corba_exception
                
                # Keep some values in this instance  for use in other functions.
                self.integration_time = actual_integration_time
                self.nchannels = nchannels
                self.logger.logInfo("integration_time: {}, nsections: {}, nchannels: {}".format(
                    self.integration_time,
                    self.nsections, 
                    self.nchannels))

        except JSONDecodeError:
            self.logger.logWarning("JSONDecodeError. empty string?  Send empty config dict")
            config_dict = {}

        self.logger.logInfo("config_json: {}".format(config_json))

        return "configuration success"

    def deconfigure(self):
        self.logger.logInfo("deconfigure")
        return "reset complete"

    def capture_start(self):
        self.logger.logInfo("capture_start")
        return "capture start success"

    def capture_stop(self):
        self.logger.logInfo("capture_stop")
        return "capture stop success"

    def measurement_prepare(self, config_json):
        try:
            if "spectrometer" in self.preset_config_name.lower():
                self.logger.debug("Start fits mock server")
                # Parameters to generate spectrum arraydata_row objects.
                
                # Hard-code duration of simulation repeat
                subscan_duration = 30 
                # Hard-code interleave noise packets (Same as GatedSpectrometer EDD pipeline)
                interleave_noise_packets = True
                
                self.logger.logInfo("integration_time: {}, nsections: {}, nchannels: {}".format(self.integration_time,
                                                                                            self.nsections, 
                                                                                            self.nchannels))
                
                absolute_path = os.environ["INTROOT"] + "/lib/python/site-packages/EddFitsMockServer.py"
                
                if interleave_noise_packets == True:
                    cmd = [
                        "python",
                        absolute_path,
                        "-d",
                        "{}".format(subscan_duration),
                        "-T",
                        "{}".format(self.integration_time),
                        "-s",
                        "{}".format(self.nsections),
                        "-n",
                        "{}".format(self.nchannels),
                        "-c",
                    ]
                else:
                    cmd = [
                        "python",
                        absolute_path,
                        "-d",
                        "{}".format(subscan_duration),
                        "-T",
                        "{}".format(self.integration_time),
                        "-s",
                        "{}".format(self.nsections),
                        "-n",
                        "{}".format(self.nchannels),
                    ]

                self.logger.debug("Launching process: {}".format(cmd))
                self.fits_mock_server_proc = subprocess.Popen(cmd)
                time.sleep(1)
                return "measurement prepare success"
        except Exception as e:
            self.logger.logError(e)

    def measurement_start(self):
        self.logger.logInfo("measurement_start")
        if "spectrometer" in self.preset_config_name.lower():
            try:
                self.da.fits_client_start()
            except DataAggregatorError.ConnectionRefusedErrorEx as e:
                self.logger.logError(e)
                raise BackendErrorImpl.ConnectionErrorExImpl
            except Exception as e:
                self.logger.logError(e)
                raise BackendErrorImpl.InternalErrorExImpl
                
        return "measurement start success"

    def measurement_stop(self):
        self.logger.logInfo("measurement_stop")

        if "spectrometer" in self.preset_config_name.lower():
            self.logger.debug("Stop fits client")
            self.da.fits_client_stop()

            self.logger.debug("Stop fits mock server")
            self.fits_mock_server_proc.terminate()
            self.fits_mock_server_proc.wait()

        return "measurement stop success"

    def metadata_update(self, metadata_json):
        return "metadata update success"

    def status_loop_start(self, status_period):
        self.status_period = status_period
        self.status_message_count = 0
        self.thread_status_notification = threading.Thread(target=self._status_notification_loop)
        self.logger.logInfo("Start _status_notification_loop")
        self.event_stop_status_notification_loop.clear()
        self.thread_status_notification.start()

    def status_loop_stop(self):
        self.logger.logInfo("Stop _status_notification_loop")
        self.event_stop_status_notification_loop.set()

    # ------------------------------------------------------------------------------------
    # Internal functions (not available from IDL interface)
    # ------------------------------------------------------------------------------------

    def _status_notification_loop(self):
        # Loop through data array, create EDD fits notification messages
        while not self.event_stop_status_notification_loop.is_set():
            self.status_message_count = self.status_message_count + 1
            self.logger.logInfo(
                "publish backend status.counter = {} (TODO add useful data to status notification)".format(
                    self.status_message_count
                )
            )

            # Assemble a complete packet in the required format publish
            status_message = BackendMod.StatusNotifyBlock(int(self.status_message_count))

            self.status_supplier.publishEvent(status_message)

            # use wait instead of sleep because it can cancel immediately if anothe thread calls function stop_simulate
            self.event_stop_status_notification_loop.wait(timeout=self.status_period)

    def connect_component(self, component_name):
        try:
            self.logger.debug("Connecting to ACS component %s" % component_name)
            return self.sc.getComponent(component_name)
        except Exception as e:
            self.logger.error("Cannot get ACS component object reference for %s" % component_name)


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    s = BackendMockEDD()
