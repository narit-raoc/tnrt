# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

import socket
import numpy as np

# connection
AcuIP = socket.gethostbyname("acu")
MockAcuIP = socket.gethostbyname("mockAcu")
cmdPort = 9000
stsPort = 9001

edd_master_controller_host = socket.gethostbyname("edd_master_controller")
edd_master_controller_port = int(7147)

edd_redis_host = socket.gethostbyname("edd_redis")
edd_redis_port = int(6379)

holofft_host = socket.gethostbyname("holofft")
holofft_port = 1234

# NOTE: To create an empty message (array length 1), use syntax:
# dtype_holodata = generate_dtype_msg_holodata(fft_length)
# msg_holodata = np.squeeze(np.zeros(1, dtype=dtype_msg_holodata))
# np.zeros will create an array with length of 1 for each item in the data structure that
# does not have non-zero length `fft_length``.  This is not convenenient when we want to
# read / write these values as scalar (don"t want to always use array access[0].  So, we
# use mp.squeeze to reduce unneccesary array dimensions to salars.

def generate_dtype_msg_holodata(fft_length):
    dt = np.dtype([
        ("time_mjd", "f8"),
        ("sample_capture_duration", "f8"),
        ("timeseries_time_axis", "f8", fft_length),
        ("timeseries_phase_ch1_rad", "f8", fft_length),
        ("timeseries_phase_ch2_rad", "f8", fft_length),
        ("spectrum_freq_axis", "f8", fft_length),
        ("spectrum_absolute_magnitude_ch1_dbm", "f8", fft_length),
        ("spectrum_absolute_magnitude_ch2_dbm", "f8", fft_length),
        ("spectrum_relative_magnitude_db", "f8", fft_length),
        ("spectrum_relative_phase_deg", "f8", fft_length),
        ("marker_index", "u4"),
        ("marker_val_freq_hz", "f8"),
        ("marker_val_absolute_magnitude_ch1_dbm", "f8"),
        ("marker_val_absolute_magnitude_ch2_dbm", "f8"),
        ("marker_val_relative_magnitude_db", "f8"),
        ("marker_val_phase_ch1_deg", "f8"),
        ("marker_val_phase_ch2_deg", "f8"),
        ("marker_val_relative_phase_deg", "f8"),
        ]
        )
    return(dt)
