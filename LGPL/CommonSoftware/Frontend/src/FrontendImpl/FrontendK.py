# -*- coding: utf-8 -*-
import json
import logging

from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
import ACSErr

from .BaseFrontend import BaseFrontend

# TCS packages
import FrontendMod
import FrontendMod__POA
import FrontendErrorImpl
import FrontendError

import FrontendConfig as config


class FrontendK(BaseFrontend, FrontendMod__POA.FrontendK, ACSComponent, ContainerServices, ComponentLifecycle):

    # Constructor
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        super(FrontendK, self).__init__(config.kband_ip, config.kband_port)

        # Create a new logger for this class that is a child of logger named 'FrontendK'
        self.logger = logging.getLogger("FrontendK :" + self.__class__.__name__)

    # Destructor
    def __del__(self):
        pass

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.

    def initialize(self):
        self.logger.logInfo("INITIALIZE")

    def execute(self):
        self.logger.logInfo("EXECUTE")

    def aboutToAbort(self):
        self.logger.logInfo("ABOUT_TO_ABORT")

    def cleanUp(self):
        self.logger.logInfo("CLEANUP")

        self.event_stop_stream_monitor_data.set()

        for key in self.outputs:
            self.logger.logInfo('Destroying Notification Channel Supplier %s: ' % key)
            if self.outputs[key]['supplier'] is not None:
                try:
                    self.outputs[key]['supplier'].disconnect()
                except:
                    self.outputs[key] = None

        # stop katcp client
        self.client.stop()
        self.client.disconnect()


    def configure_frontend(self, lna_enable):
        self.auto_enable_lna_status = lna_enable
        self.operation_mode_status = self.client.get_sensor_value("rxs.rsm.opmode")
        self.digitizer_enable_status = self.client.get_sensor_value("rxs.power.digitizer") == "true"

        self.logger.logInfo(f"auto_enable_lna status : {self.auto_enable_lna_status}")
        self.logger.logInfo(f"operation_mode status : {self.operation_mode_status}")
        self.logger.logInfo(f"digitizer_enable status : {self.digitizer_enable_status}")

        # Check if the operation mode is "go-cold-operational"
        if self.operation_mode_status == "go-cold-operational":
            # Check if the digitizer is enabled (on)
            if self.digitizer_enable_status:
                # Enable MMIC Chian A and B if auto_enable is True
                if self.auto_enable_lna_status:
                    self.logger.logInfo('Start configuring MMIC Chain A to true')
                    self.set_mmic("1", True)
                    self.logger.logInfo('Start configuring MMIC Chian B to true')
                    self.set_mmic("2", True)
                else:
                    self.logger.logInfo('auto_enable_lna is Fals the operator must be control by them self')
            else:
                self.logger.logWarning('Digitizer must be anabled (on) before configuring MMIC')
        else:
            # Log a message about the incorrect mode
            self.logger.logWarning(
                f"Cannot proceed with configuration. Current mode is '{self.operation_mode_status}'."
                f"Change the mode to 'go-cold-operational' to continue."
            )

        return FrontendMod.ResponseMessage("success", "configuration updated successfully")
    

    def deconfigure_frontend(self, lna_enable):
        self.auto_enable_lna_status = lna_enable
        self.operation_mode_status = self.client.get_sensor_value("rxs.rsm.opmode")
        self.digitizer_enable_status = self.client.get_sensor_value("rxs.power.digitizer") == "true"

        if self.auto_enable_lna_status:
            # Always disable MMIC Chians A and B
            self.logger.logInfo('Disabling MMIC Chain A')
            self.set_mmic("1", False)
            self.logger.logInfo('Disabling MMIC Chian B')
            self.set_mmic("2", False)
        else:
            self.logger.logInfo("Auto enable LNA is not enabled")

        # Log the current status of operation mode and digitizer
        self.logger.logInfo(f"Current operation mode: {self.operation_mode_status}")
        self.logger.logInfo(f"Current digitizer status: {'enabled' if self.digitizer_enable_status else 'disabled'}")

        return FrontendMod.ResponseMessage("success", "deconfiguration updated successfully")

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class   
    def set_operation_mode(self, mode):
        if mode not in [
            "go-soft-off",
            "go-cold-operational",
            "go-debug",
            "do-regeneration-to-warm",
            "do-regeneration",
            "error-noncritical-stay-cold",
            "error-noncritical-soft-off",
            "error-critical",
        ]:
            raise ValueError("Value error: mode incorrect")
        
        response = super().set_operation_mode(mode)
        return response
    
    def set_mmic(self, chain, val):
        if val not in [True, False] or chain not in ["1", "2"]:
            raise ValueError("Value error: chain should be 1 or 2 and value should be boolean")
        
        response = super().set_mmic(chain, val)
        return response
    
    def set_digitizer(self, val):
        if val not in [True, False]:
            raise ValueError("Value error: value should be boolean")
        
        response = super().set_digitizer(val)
        return response

    def set_down_converter(self, val):
        try:
            self.client.set_down_converter(val)
            return FrontendMod.ResponseMessage("success", "down converter value: {}".format(val))
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    def get_data(self):
        try:
            data = self.client.get_data('K')
            return json.dumps(data)
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    # ------------------------------------------------------------------------------------
    # Internal functions not exposed in IDL
    


if __name__ == "__main__":
    t = FrontendK()
