# -*- coding: utf-8 -*-
import threading
import time
import logging
import json

# TCS packages
import FrontendMod
import FrontendMod__POA
import FrontendErrorImpl

from KatcpClient import KatcpClient
import FrontendConfig as config

from Supplier import Supplier


class BaseFrontend(FrontendMod__POA.BaseFrontend):

    # Constructor
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.logger = logging.getLogger("Frontend")
        self.client = None

        self.outputs = {
            'monitor' : {
				'channel': config.nc_channel_monitor,
				'supplier': None
			}
        }

        for key in self.outputs:
            self.logger.info('Creating Notification Channel Supplier %s: ' % key)
            try:
                self.outputs[key]['supplier'] = Supplier(self.outputs[key]['channel'])
            except Exception as e:
                self.outputs[key]['supplier'] = None
                self.logger.error('Failed to initialize NC Supplier: %s' % key)

        self.connect()

        self.event_stop_stream_monitor_data = threading.Event()
        monitor_stream_thread = threading.Thread(target=self.monitor_frontend_worker)
        monitor_stream_thread.start()


    # Destructor
    def __del__(self):
        pass

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class

    def connect(self, timeout=3):
        self.client = KatcpClient(self.host, self.port)

        connecting_event = threading.Event()
        timeout_count = 0
        TIME_OFFSET = 0.5

        while not connecting_event.is_set():
            self.logger.info("connecting...")
            time.sleep(TIME_OFFSET)
            timeout_count += TIME_OFFSET
            if self.client.is_connected():
                connecting_event.set()
            elif timeout_count >= timeout:
                connecting_event.set()

        if not self.client.is_connected():
            self.client.stop()
            raise FrontendErrorImpl.noConnectionErrorExImpl()

        return FrontendMod.ResponseMessage(
            "success",
            "client connected at {}.{}".format(config.kband_ip, config.kband_port),
        )

    def set_digitizer(self, val):
        try:
            self.client.set_digitizer(val)
            return FrontendMod.ResponseMessage("success", "digitizer status: {}".format(val and "on" or "off"))
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    def set_attenuation(self, chain, val):
        try:
            self.client.set_attenuation(chain, val)
            return FrontendMod.ResponseMessage("success", "attenuation value: {}".format(val))
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    def set_mmic(self, chain, val):
        try:
            self.client.set_mmic(chain, val)
            return FrontendMod.ResponseMessage(
                "success",
                "chain {} status: {}".format((chain == "1") and "a" or "b", val and "on" or "off"),
            )
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    def set_operation_mode(self, mode):
        try:
            self.client.set_operation_mode(mode)
            return FrontendMod.ResponseMessage("success", "operation mode: {}".format(mode))
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    def set_cryo_motor(self, speed):
        try:
            self.client.set_cryo_motor(speed)
            return FrontendMod.ResponseMessage("success", "cryo motor speed: {}".format(speed))
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    def get_data(self):
        pass

    def monitor_frontend_worker(self, update_period=5):
        self.event_stop_stream_monitor_data.clear()
        while not self.event_stop_stream_monitor_data.is_set():
            self.logger.debug('get frontend data and publish')
            data = json.loads(self.get_data())
            self.outputs['monitor']['supplier'].publish_event(data)
            self.event_stop_stream_monitor_data.wait(timeout=update_period)


if __name__ == "__main__":
    t = BaseFrontend()
