import unittest
import logging
import coloredlogs
import time
import socket
from unittest.mock import patch, Mock, MagicMock

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
import FrontendMod
import FrontendErrorImpl
import FrontendError

level_styles_scrn = {
    "critical": {"color": "red", "bold": True},
    "debug": {"color": "white", "faint": True},
    "error": {"color": "red"},
    "info": {"color": "green", "bright": True},
    "notice": {"color": "magenta"},
    "spam": {"color": "green", "faint": True},
    "success": {"color": "green", "bold": True},
    "verbose": {"color": "blue"},
    "warning": {"color": "yellow", "bright": True, "bold": True},
}
field_styles_scrn = {
    "asctime": {},
    "hostname": {"color": "magenta"},
    "levelname": {"color": "cyan", "bright": True},
    "name": {"color": "blue", "bright": True},
    "programname": {"color": "cyan"},
}


class TestFrontendL(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # Configure logger
        self.logger = logging.getLogger("Frontend")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(message)s"
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug("Started logger name %s" % self.logger.name)

        self.logger.debug("Starting PySimpleClient")
        self.sc = None
        self.sc = PySimpleClient()

        self.Lband = None
        self.component_name = "FrontendL"

        # Get reference to ComponentA.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.logger.debug("Connecting to ACS component %s" % self.component_name)

            self.Lband = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error("Cannot get ACS component object reference for %s" % self.component_name)
            self.cleanup()

    @classmethod
    def tearDownClass(self):
        try:
            self.logger.debug("Releasing ACS component %s" % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.Lband = None
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)

        if self.sc is not None:
            self.sc.disconnect()
            del self.sc

    def test_connect_fail(self):
        """[connect] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.connect()

    def test_set_digitizer_no_connection(self):
        """[set_digitizer] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.set_digitizer(True)

    def test_set_attenuation_no_connection(self):
        """[set_attenuation] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.set_attenuation("1", 15)

    def test_set_mmic_no_connection(self):
        """[set_mmic] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.set_mmic("1", True)

    def test_set_operation_mode_no_connection(self):
        """[set_operation_mode] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.set_operation_mode("go-soft-off")

    def test_set_cryo_motor_no_connection(self):
        """[set_cryo_motor] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.set_cryo_motor(60)

    def test_get_data_no_connection(self):
        """[get_data] should raise no connection error when cannot connect with in timeout"""
        with self.assertRaises(FrontendError.noConnectionErrorEx):
            self.Lband.get_data()
