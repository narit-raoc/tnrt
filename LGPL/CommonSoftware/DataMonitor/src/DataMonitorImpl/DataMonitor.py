# Import standard Python packages
import os
import threading
import subprocess

# Import ACS
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException

# Import TNRT project ACS components
import DataMonitorMod
import DataMonitorMod__POA
import DataMonitorErrorImpl
import DataMonitorError

class DataMonitor(DataMonitorMod__POA.DataMonitor, ACSComponent, ContainerServices, ComponentLifecycle):
	'''
	DataMonitor component that spawns a GUI to observe real-time data from TCS
	It opens a seaparate process for the QT GUI to avoid blocking behavior and interference
	with ACS function.
	'''
	# Constructor
	def __init__(self):
		ACSComponent.__init__(self)
		ContainerServices.__init__(self)
		self.logger = self.getLogger()
		self.gui_proc = None
		self.gui_cmd = None
		
	#------------------------------------------------------------------------------------
	# LifeCycle functions called by ACS system when we activate and deactivate components.
	# Inherited from headers in Acspy.Servants.ComponentLifecycle.  
	#------------------------------------------------------------------------------------
	def initialize(self):
		'''Create a new process for QApplication GUI.
		If QApplication is in the main thread, it will block execution of ACS Activate Component
		If QApplication is in a separate thread, it doesn't work.  QApplication *must* be in the main thread
		of a process. Therefore, we create a new process, and give the main thread to QApplication.
		This does not block execution of ACS.
		'''
		self.logger.logInfo('LifeCycle: initialize')
		try:
			absolute_path = os.environ['INTROOT']+'/lib/python/site-packages/MainWindow.py'
			cmd = ['python', absolute_path]
			self.logger.logInfo('cmd: %s' % cmd)
		except KeyError:
			self.logger.logError('Environment variable INTROOT not set')
		try:
			self.logger.logInfo('Launching process %s' % cmd)
			self.gui_proc = subprocess.Popen(cmd)
			# Start a separate thread to monitor the GUI process status.  When GUI closes,
			# this thread will stop the ACS component.  It must be in a separate thread
			# because any blocking function in this thread initialize() will not allow the
			# ACS component to activate.
			self.thread_wait_GUI_close = threading.Thread(target=self.wait_GUI_close)
			self.thread_wait_GUI_close.start()
		except:
			self.logger.logError('Failed to start process {}.  Stop ACS component'.format(cmd))

	def aboutToAbort(self):
		pass

	def cleanUp(self):
		self.logger.logInfo('LifeCycle: cleanUp')
		try:
			self.logger.logInfo("Exit GUI with SIGTERM")
			self.gui_proc.exit()
			self.gui_proc.wait()
		except:
			self.logger.logInfo("Exit GUI with SIGKILL")
			self.gui_proc.terminate()
			self.gui_proc.wait()

	#------------------------------------------------------------------------------------
	# Functions defined in IDL to interface from outside this class	
	#------------------------------------------------------------------------------------

	#------------------------------------------------------------------------------------
	# Internal functions not exposed in IDL
	#------------------------------------------------------------------------------------
	def wait_GUI_close(self):
	# Wait for process to complete : means waiting for the user to close the GUI window
		self.logger.logInfo('watchdog thread waiting for GUI close ...')
		returncode = self.gui_proc.wait()
		self.logger.logInfo('DataMonitor GUI window closed. returncode: {}'.format(returncode))
		# After user closes the GUI window, stop the ACS Component
		# Use the function inherited from ContainerServices to release the component
		name = self.getName()
		self.logger.logDebug('Release component {}'.format(name))
		self.forceReleaseComponent(name)