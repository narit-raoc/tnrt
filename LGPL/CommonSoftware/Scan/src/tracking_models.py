# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.10

import logging
import threading
from abc import ABC
from abc import abstractmethod
from abc import abstractproperty

import numpy as np
from astropy import units as units
from astropy.time import Time
from astropy.coordinates import EarthLocation

# For TrackCoordEQ
try:
    # For Python < 3.7, import erfa from astropy
    from astropy import _erfa as erfa
except:
    # Python >= 3.7, import erfa from pyerfa package
    # https://pypi.org/project/pyerfa/
    import erfa

# For TrackCoordTLE satellite
from sgp4.api import Satrec
from astropy.coordinates import CartesianRepresentation
from astropy.coordinates import AltAz
from astropy.coordinates import GCRS

# For TrackCoordSS
from astropy.coordinates import get_body

# ACS
from Acspy.Clients.SimpleClient import PySimpleClient

from Consumer import Consumer
from CentralDB import CentralDB
import cdbConstants

# TNRT
import ScanMod
import ScanDefaults
import tnrtAntennaMod

# config
import AntennaDefaults as config


class TrackCoordModelFactory:
    @classmethod
    def from_params(cls, metadata, tracking_params):
        logger = logging.getLogger(cls.__name__)
        logger.logTrace(
            "test logger in class {}, module {}".format(cls.__name__, (__name__))
        )
        logger.logDebug(
            "Construct TrackCoord object from {}".format(type(tracking_params))
        )

        if isinstance(tracking_params, ScanMod.TrackingParamsEQ):
            obj = TrackCoordEQ(
                metadata,
                tracking_params.ra,
                tracking_params.dec,
                tracking_params.pm_ra,
                tracking_params.pm_dec,
                tracking_params.parallax,
                tracking_params.radial_velocity,
                tracking_params.send_icrs_to_acu,
                tracking_params.common,
            )

        elif isinstance(tracking_params, ScanMod.TrackingParamsHO):
            obj = TrackCoordHO(
                metadata,
                tracking_params.az,
                tracking_params.el,
                tracking_params.common,
            )

        elif isinstance(tracking_params, ScanMod.TrackingParamsTLE):
            obj = TrackCoordTLE(
                metadata,
                tracking_params.tle_line0,
                tracking_params.tle_line1,
                tracking_params.tle_line2,
                tracking_params.common,
            )
        elif isinstance(tracking_params, ScanMod.TrackingParamsSS):
            obj = TrackCoordSS(
                metadata,
                tracking_params.planet,
                tracking_params.common,
            )
        else:
            obj = TrackCoordNone()

        return obj


class AbstractTrackCoord(ABC):
    """
    AbstractTrackCoord is an abstract class that cannot be instantiated.
    Some common functions and variables are implemented in this abstract class and can be
    used from child classes with the super(). functions

    Python will not allow the user to create an instance of this class because it contains
    abstract properties and functions.
    """

    # Define properties and methods that are abstract, and must be implemented by a subclass
    # before a Scan object can be created
    @abstractmethod
    def __init__(self, metadata, common):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Keep instance variables
        self.metadata = metadata
        self.user_pointing_correction_az = common.user_pointing_correction_az
        self.user_pointing_correction_el = common.user_pointing_correction_el
        self.elevation_min = common.elevation_min
        self.elevation_max = common.elevation_max
        self.north_crossing = common.north_crossing
        self.use_horizontal_tables = common.use_horizontal_tables
        self.max_tracking_errors = common.max_tracking_errors

        # Name of Antenna component to send commands for pointing and tracking.
        self.antenna_name = "AntennaTNRT"
        self.antenna = None

        # Client to access other ACS components and database
        self.logger.logInfo("Starting PySimpleClient")
        self.sc = PySimpleClient()

        # CDB instance
        self.cdb = CentralDB()

        # Read sitelocation from the CDB database:
        try:
            longitude = float(config.longitude40mE)
            latitude = float(config.latitude40mN)
            height = float(config.height40m)
            self.site_location = EarthLocation(
                EarthLocation.from_geodetic(
                    lon=longitude * units.deg,
                    lat=latitude * units.deg,
                    height=height * units.m,
                    ellipsoid="WGS84",
                )
            )
            self.logger.debug(
                "Read from element latitude %f, longitude %f, height %f"
                % (latitude, longitude, height)
            )

            self.el_low_limit = float(config.elLowerLimit)

            self.logger.logDebug("Disconnect PySimpleClient in constructor")
            self.sc.disconnect()
        except Exception as e:
            self.logger.logError("Cannot read CDB Element")

        # Use Python threading event to wait for antenna to arrive at command position
        # This is helpful if we want to block program execution while Antenna is
        # moving from old AZ, EL angle to new AZ, EL command..
        self.antenna_arrived = threading.Event()

    @abstractmethod
    def start_tracking(self, stop_event, blocking=True, update_period=5.0):
        """
        Start tracking the main program track table. Should be implemented
        in subclasses TrackCoordHO, TrackCoordTLE, and TrackCoordEQ
        """
        self.stop_event = stop_event
        self.connect_components()

    def set_starttime(self, start_mjd):
        self.logger.logInfo("Setting start time of tracking to %f [MJD]" % start_mjd)
        self.starttime_apy = Time(start_mjd, format="mjd", scale="utc")

    def connect_components(self):
        self.logger.logInfo("Starting PySimpleClient")
        self.sc = PySimpleClient()

        self.set_starttime(self.cdb.get(cdbConstants.MJD, float))

        # Connect to Antenna for access to tracking functions
        self.logger.logInfo("Connecting to component %s" % self.antenna_name)
        self.antenna = self.sc.getComponent(self.antenna_name)

        self.logger.logInfo("Creating Notification Channel Consumer: Antenna Arrived")
        consumer_ant_arrived = Consumer(tnrtAntennaMod.ANT_ARRIVED_CHANNEL_NAME)
        consumer_ant_arrived.add_subscription(self._handler_antenna_arrived)

        # Set metadata to cdb
        self.cdb.set(cdbConstants.SITE_LONG, self.site_location.lon.deg)
        self.cdb.set(cdbConstants.SITE_LAT, self.site_location.lat.deg)
        self.cdb.set(cdbConstants.SITE_ELEV, self.site_location.height.value)
        self.cdb.set(cdbConstants.EQUINOX, 2000.0)

    def cleanup(self):
        """
        Disconnect components, etc.
        """
        self.logger.logDebug("Enter %s.cleanup()" % self.__class__.__name__)

        try:
            consumer_ant_arrived.disconnect()
        except:
            consumer_ant_arrived = None

        try:
            self.sc.releaseComponent(self.antenna_name)
            self.antenna = None
        except Exception as e:
            self.logger.error(
                "Cannot release component reference for %s" % self.antenna_name
            )

        if self.sc is not None:
            self.sc.disconnect()
            self.sc = None

    def transit_timeout(self):
        """
        Simulate that Antenna / ACU transit to the start coordinate of a scan.
        """
        self.logger.logInfo("handle transit timeout. antenna_arrived.set()")
        self.antenna_arrived.set()

    def blocking_wait(self, duration, update_period):
        """
        Use threading.Event mechanism to make a blocking function call that is release by an event.set()
        """
        # clear old value of event so we must wait again until Antenna arrives.
        self.antenna_arrived.clear()
        # Wait for ACU status message to show that the Antenna has arrived at command coordinate.
        wdt = threading.Timer(duration, self.transit_timeout)
        wdt.daemon = True
        wdt.start()

        countdown = duration

        self.logger.logInfo("[Simulate] Waiting for countdown = %f [sec] " % countdown)
        while not self.antenna_arrived.wait(timeout=update_period):
            countdown = countdown - update_period
            self.logger.logInfo(
                "[Simulate] Waiting for Antenna to arrive at command coordinate. Countdown = %f"
                % countdown
            )

    def block_until_arrived_or_canceled(self, update_period):
        # clear old value of event so we must wait again until Antenna arrives.
        self.antenna_arrived.clear()

        ready_to_unblock = threading.Condition()
        # This thread must hold the reference to Mutex Lock to enable blocking
        ready_to_unblock.acquire()
        while not ready_to_unblock.wait_for(self.unblock_condition_function, timeout=update_period):
            self.logger.logDebug("arrived: {}, canceled: {} ...".format(self.antenna_arrived.is_set(), self.stop_event.is_set()))

        self.logger.logInfo("unblocked! arrived: {}, canceled: {}".format(self.antenna_arrived.is_set(), self.stop_event.is_set()))

    def unblock_condition_function(self):
        return(self.antenna_arrived.is_set() or self.stop_event.is_set())

    def _handler_antenna_arrived(self, data):
        if data.is_arrived == True:
            self.antenna_arrived.set()
        else:
            self.antenna_arrived.clear()



class TrackCoordNone(AbstractTrackCoord):
    """
    Class to keep common functions or attributes of any scan that is tracking
    in Horizontal (AZ/EL) coordinates.
    """

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.debug(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

    def cleanup(self):
        """
        Override cleanup method in TrackCoordNone.  Nothing to cleanup.
        """
        self.logger.logDebug("Empty cleanup method")

    # Define methods that are required by the parent class, but implemented in subclass
    # These methods are tagged with decorator @abstractmethod
    def start_tracking(self, stop_event, blocking=True, update_period=5.0):
        """
        Nothing to do here.
        """
        self.logger.logInfo("NOOP")
    
    def block_until_arrived_or_canceled(self, update_period):
        """
        Override parent function. Nothing to do here. return immediately
        """
        pass


class TrackCoordHO(AbstractTrackCoord):
    """
    Class to keep common functions or attributes of any scan that is tracking
    in Horizontal (AZ/EL) coordinates.
    """

    def __init__(self, metadata, az, el, common):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Use the constructor of parent classes to process common parameters
        AbstractTrackCoord.__init__(self, metadata, common)

        # Extend parent class with details of this class
        self.az = az
        self.el = el

    def connect_components(self):
        # Update the metadata structure with parameters that are available at the time of
        # object construction.
        self.cdb.set(cdbConstants.RADESYS, "AZEL")
        self.cdb.set(cdbConstants.BLNGTYPE, "AZ")
        self.cdb.set(cdbConstants.BLATTYPE, "EL")
        self.cdb.set(cdbConstants.NLNGTYPE, "AZ")
        self.cdb.set(cdbConstants.NLATTYPE, "EL")
        self.cdb.set(cdbConstants.BLONGOBJ, self.az)
        self.cdb.set(cdbConstants.BLATOBJ, self.el)

        return super().connect_components()

    # Define methods that are required by the parent class, but implemented in subclass
    # These methods are tagged with decorator @abstractmethod
    def start_tracking(self, stop_event, blocking=True, update_period=5.0):
        """
        Point the antenna to (az, el) in the Program Track table.  Not yet using
        the Program Offset table until subscans start.
        """
        # Use method of parent class for common behavior
        super().start_tracking(stop_event, blocking)

        self.logger.logInfo(
            "(az, el) = (%f, %f)" % (self.az, self.el)
        )
        try:
            # Calculate how many seconds until antenna can arrive at tracking coordinate
            slewtime = self.antenna.slewTime(
                self.az,
                self.el,
                tnrtAntennaMod.azel,
                Time.now().mjd,
                2000,
                0.0,
                0.0,
            )
            if slewtime == -1.0:
                raise NameError(
                    "Cannot get current AZ, EL from Antenna component or ACU"
                )
            self.logger.logInfo(
                "Estimate slew time to (%f, %f) = %f"
                % (self.az, self.el, slewtime)
            )
        except Exception as e:
            # Create a fake slew time so we can test the Scan software without connection to the
            # ACU and azimuth / elevation encoders.
            self.logger.logError(
                "Catch exception Antenna.slewtime() (ACU not connected?). Set fake slewtime = 10.0"
            )
            slewtime = 10.0
            self.logger.debug("For testing, run blocking_wait(%f)" % slewtime)

            # Call a blocking function in  simulates the behavior of ACU
            # taking time to transit.  log ScanC will show an update every <update_period> seconds.
            self.blocking_wait(slewtime, update_period)
            return

        # Command antenna to load AZ/EL tracking table now and go to the starting coordinate.
        # IMPORTANT: The antenna must arrive at command position before
        # we can start to run the subscans and record data.
        # Set blocking=False for the remote function that commands the ACU because we
        # must maintain the opportunity to cancel the slewing from Scan controller.

        # Set the main tracking table to run "forever" (actually 1 day) because we don't know
        # the duration of all subscans + slew times.  If the main program track table is
        # arrived at status "completed" in ACU, it ignores remaining program offset table.
        # Therefore subscans will be truncated if main Program Track stops.  TCS Scan queue
        # will overwrite this main Program Track table on the next scan, so we don't care if
        # it "ends" tomorrow becuase it will be overwritten within minutes.
        duration_1day = 86400
        
        if blocking==True:
            self.logger.logDebug("blocking=True. Cannot cancel until Antenna arrives at Tracking")

        self.antenna.trackCoordinateHO(
            Time.now().mjd,
            duration_1day,
            self.az,
            self.el,
            self.user_pointing_correction_az,
            self.user_pointing_correction_el,
            blocking,
            update_period,
        )

class TrackCoordEQ(AbstractTrackCoord):
    """
    Class to keep common functions or attributes of any scan that is tracking
    celestial sources in equatorial coordinates (RA, DEC).  .
    """

    def __init__(
        self, metadata, ra, dec, pm_ra, pm_dec, parallax, radial_velocity, send_icrs_to_acu, common
    ):
        """
        Constructor.  Keep coommon variables

        Parameters
        ----------
        ra : float
                Right ascension in ICRS (J2000) coordinate reference system. unit = deg.
        dec : float
                Declination in ICRS (J2000) coordinate reference system. unit = deg.
        pm_ra : float
                proper motion of right ascension * cos declination. [unit = mas / year], default=0

                https://docs.astropy.org/en/stable/coordinates/velocities.html
        pm_dec : float
                proper motion of right declination. [unit = mas / year], default=0

                https://docs.astropy.org/en/stable/coordinates/velocities.html
        parallax :  float
                parallax. [unit = arcsec], default=0
        radial_velocity : float
                The component of the velocity along the line-of-sight (i.e., the radial direction).
                [unit = km / s], default=0, + is moving away from observer. (-) approaching observer
        send_icrs_to_acu : {True, False}
        common : ScanMod.TrackingParamsCommon
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Use the constructor of parent classes to process common parameters
        AbstractTrackCoord.__init__(self, metadata, common)

        # Extend parent class with details of this class
        # Save parameters to member variables and use astropy units.
        # NOTE: Only save member variables of data that *does not* change with 
        # time on the timescale of a typical observation queue (1 day)
        self.ra_icrs = ra * units.deg
        self.dec_icrs = dec * units.deg
        self.pm_ra = pm_ra * units.mas / units.year
        self.pm_dec = pm_dec * units.mas / units.year
        self.parallax = parallax * units.arcsec
        self.radial_velocity = radial_velocity * units.km / units.s
        self.send_icrs_to_acu = send_icrs_to_acu

    # Define methods that are required by the parent class, but implemented in subclass
    # These methods are tagged with decorator @abstractmethod

    def connect_components(self):
        # Update the metadata structure with parameters that are available at the time of
        # object construction.
        self.cdb.set(cdbConstants.WCSNAME, "Equatorial Coordinates")
        self.cdb.set(cdbConstants.RADESYS, "ICRS")
        self.cdb.set(cdbConstants.BLNGTYPE, "RA")
        self.cdb.set(cdbConstants.BLATTYPE, "DEC")
        self.cdb.set(cdbConstants.NLNGTYPE, "RA")
        self.cdb.set(cdbConstants.NLATTYPE, "DEC")
        self.cdb.set(cdbConstants.BLONGOBJ, self.ra_icrs.value)
        self.cdb.set(cdbConstants.BLATOBJ, self.dec_icrs.value)
        return super().connect_components()

    def start_tracking(self, stop_event, blocking=True, update_period=5.0):
        """
        TODO
        """
        # Create a local/temporary Astropy time object *now* when we want to actually start tracking.
        # This will help ensure that our tracking command data is not confused by scheduled
        # start times that are not accurate when a scan runs ASAP (and is already later than scheduled)
        tracking_starttime_apy = Time.now()

        # Calculate first tracking coordinate in Horizontal AltAz coordinates (AZ, EL)
        # In the case of ScanOnSourceEQ, the the first AZ/EL coordinate to follow the celestial object
        # depends on observation time and location.  Use model and transform from IAU SOFA library,
        # access by Python wrapper erfa.
        # The spherical geometry tracking the RA/DEC is handled by ACU in real-time.
        # This coordinate calculation  is only to find the starting AZ/EL to estimate slew time before we
        # start the data recording for the scan.
        self.logger.logDebug(
            "IAU SOFA Model input ICRS (ra, dec) = ({}, {}) [deg]".format(self.ra_icrs, self.dec_icrs))
        
        # Calculate the AZ and EL from observer view @ (time, earth location)
        # to the celestial object *now* (because function start_tracking is only 
        # called when we actually want to start tracking).
        
        # Follow example http://www.iausofa.org/2019_0722_C/sofa/sofa_ast_c.pdf

        # Convert units to use wth IAU models and get the raw value from the Astropy Quantity unit objects.
        rc = self.ra_icrs.to(
            units.radian
        ).value  # ICRS [ α, δ ] at J2000.0 (radians, Note 1)
        dc = self.dec_icrs.to(units.radian).value
        pr = self.pm_ra.to(
            units.radian / units.year
        ).value  # RA proper motion (radians/year; Note 2)
        pd = self.pm_dec.to(
            units.radian / units.year
        ).value  # Dec proper motion (radians/year)

        px = self.parallax.value  # parallax (arcsec)
        rv = self.radial_velocity.value  # radial velocity (km/s, positive if receding)

        date1 = tracking_starttime_apy.jd1  # UTC as a 2-part. . .
        date2 = tracking_starttime_apy.jd2  # . . . quasi Julian Date (Notes 3,4)
        
        # Get the time difference dut1 = delta_ut1_utc = UT1-UCT (seconds) from IERS.
        dut1 = float(tracking_starttime_apy.delta_ut1_utc)
        self.logger.logDebug("DUT1 (UT1-UTC): {} s".format(dut1))

        elong = (
            self.site_location.lon.radian
        )  # longitude (radians, east-positive, Note 6)
        phi = self.site_location.lat.radian  # geodetic latitude (radians, Note 6)
        hm = (
            self.site_location.height.value
        )  # height above ellipsoid (m, geodetic Notes 6,8)

        # Note - proper motion can be read from IERS table.  However, TNRT ACU does not have an input
        # input to accept this adjustment.  Therefore, I will not include it in this function
        # so that we can use this model to compare monitor data from ACU.
        xp = 0  # polar motion coordinates (radians, Note 7)
        yp = 0  # polar motion coordinates (radians, Note 7)

        # These 4 parameters model the refraction correction due to atmoshpere effects.
        # The ACU has inputs has different parameters R0, b1, b2 which have already been confirmed
        # work correctly.  Therefore, don't include these effects in the simulation for the same reason above.
        # Set these to zero so we can compare this model to ACU monitor data (with refraction correction = 0)
        phpa = 0  # pressure at the observer (hPa ≡ mB, Note 8
        tc = 0  # ambient temperature at the observer (deg C)
        rh = 0  # relative humidity at the observer (range 0-1)
        wl = 1000  # wavelength (micrometers, Note 9)

        # Use IAU SOFA model to transform to AltAz observed
        # function name tco13 -> [transform][catalog] to [observed] model [2013]
        # Returns
        # -------
        # aob   observed azimuth (radians: N=0 ◦ , E=90 ◦ )
        # zob   observed zenith distance (radians)
        # hob   observed hour angle (radians)
        # dob   observed declination (radians)
        # rob   observed right ascension (CIO-based, radians)
        # eo    equation of the origins (ERA−GST)

        (aob, zob, hob, dob, rob, eo) = erfa.atco13(
            rc,
            dc,
            pr,
            pd,
            px,
            rv,
            date1,
            date2,
            dut1,
            elong,
            phi,
            hm,
            xp,
            yp,
            phpa,
            tc,
            rh,
            wl,
        )

        # Convert scalar result to Quantity object with units
        az_obs = (aob * units.radian).to(units.deg)
        # Result from IAU model is Zenith angle.  We want Alt/EL
        el_obs = (90 * units.deg) - (zob * units.radian).to(units.deg)

        initial_az = az_obs.value
        initial_el = el_obs.value
        
        self.logger.logDebug(
            "IAU SOFA Model output: (time_mjd, initial_az, initial_el) = ({}, {}, {}) [deg]".format(
                tracking_starttime_apy.isot, initial_az, initial_el
            )
        )

        # TODO - check if below horizon at runtime before we run the scan.  At the time we create the scan,
        # we might not yet know the actual start time (if schedule is FIFO).
        # if initial_el < self.el_low_limit:
        #   raise ScanError.sourceBelowHorizonEx(ACSErr.NameValue('errExpl', 'initial_el = %f < EL limit %f' % (initial_el, self.el_low_limit)))

        # Use method of parent class for common behavior
        super().start_tracking(stop_event, blocking)

        self.logger.logInfo(
            "Go to initial (az, el) = ({}, {})".format(initial_az, initial_el)
        )
        try:
            slewtime = self.antenna.slewTime(
                initial_az,
                initial_el,
                tnrtAntennaMod.azel,
                Time.now().mjd,
                2000,
                0.0,
                0.0,
            )
            if slewtime == -1.0:
                raise NameError(
                    "Cannot get current AZ, EL from Antenna component or ACU"
                )
            self.logger.logInfo(
                "Estimate slew time to (%f, %f) = %f"
                % (initial_az, initial_el, slewtime)
            )
        except Exception as e:
            # Create a fake slew time so we can test the Scan software without connection to the
            # ACU and azimuth / elevation encoders.
            self.logger.logError(
                "Catch exception Antenna.slewtime() (ACU not connected?). Set fake slewtime = 10.0"
            )
            slewtime = 10.0
            self.logger.logDebug("For testing, run blocking_wait(%f)" % slewtime)

            # Call a blocking function in  simulates the behavior of ACU
            # taking time to transit.  log ScanC will show an update every <update_period> seconds.
            self.blocking_wait(slewtime, update_period)
            return

        # Command antenna to load RADEC tracking table and go to the start position.
        # IMPORTANT: The antenna must arrive at command position before
        # we can start to run the subscans and record data.
        # Set blocking=False for the remote function that commands the ACU because we
        # must maintain the opportunity to cancel the slewing from Scan controller.

        # Send ICRS coordinate and space motion to Antenna Component. Reduce Astropy objects
        # to scalars to pass through IDL interface
        self.antenna.trackCoordinateEQ(
            tracking_starttime_apy.mjd,
            self.ra_icrs.value,
            self.dec_icrs.value,
            self.pm_ra.value,
            self.pm_dec.value,
            self.parallax.value,
            self.radial_velocity.value,
            self.send_icrs_to_acu,
            self.user_pointing_correction_az,
            self.user_pointing_correction_el,
            blocking,
            update_period,            
        )

class TrackCoordTLE(AbstractTrackCoord):
    """
    Class to keep common functions or attributes of any scan that is tracking
    NORAD satellite TLE orbit.
    """

    def __init__(self, metadata, tle_line0, tle_line1, tle_line2, common):
        """
        Constructor.  Keep coommon variables

        Parameters
        ----------
        tle_line0 : string
                TLE line0 - satellite name
        tle_line1 : string
                TLE line1 - satellite header info and timestamp of this TLE
        tle_line2 : string
                TLE line2 - satellite orbit data
        common : ScanMod.TrackingParamsCommon
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Use the constructor of parent classes to process common parameters
        AbstractTrackCoord.__init__(self, metadata, common)

        # Extend parent class with details of this class
        self.tle_line0 = tle_line0
        self.tle_line1 = tle_line1
        self.tle_line2 = tle_line2

    # Define methods that are required by the parent class, but implemented in subclass
    # These methods are tagged with decorator @abstractmethod

    def start_tracking(self, stop_event, blocking=True, update_period=5.0):
        """
        Point the antenna to (az + offset_az, el + offset_el)
        """
        # Use method of parent class for common behavior
        super().start_tracking(stop_event, blocking)

        # Calculate first tracking coordinate in Horizontal coordinates (AZ, EL)
        # In the case of ScanOnSourceTLE, the the first AZ/EL coordinate to follow the TLE orbit
        # parameters depends on observation time and location.  Use SGP4 model and Astropy coordinate
        # transforms to calculate the AZ/EL look angle of this satellite at the time start_mjd of this scan.
        # The detailed tracking of TLE is handled by ACU in real-time.
        # This coordiante calculation  is only to find the starting AZ/EL to estimate slew time before we
        # start the data recording for the scan.
        (initial_az, initial_el) = self.tle_to_altaz()
        self.logger.debug(
            "Calculated (initial_az, initial_el) from SGP4 = (%f, %f)"
            % (initial_az, initial_el)
        )

        try:
            slewtime = self.antenna.slewTime(
                initial_az,
                initial_el,
                tnrtAntennaMod.azel,
                Time.now().mjd,
                2000,
                0.0,
                0.0,
            )
            if slewtime == -1.0:
                raise NameError(
                    "Cannot get current AZ, EL from Antenna component or ACU"
                )
            self.logger.logInfo(
                "Estimate slew time to (%f, %f) = %f"
                % (initial_az, initial_el, slewtime)
            )
        except Exception as e:
            # Create a fake slew time so we can test the Scan software without connection to the
            # ACU and azimuth / elevation encoders.
            self.logger.logError(
                "Catch exception Antenna.slewtime() (ACU not connected?). Set fake slewtime = 10.0"
            )
            slewtime = 10.0
            self.logger.logDebug("For testing, run blocking_wait(%f)" % slewtime)

            # Call a blocking function in  simulates the behavior of ACU
            # taking time to transit.  log ScanC will show an update every <update_period> seconds.
            self.blocking_wait(slewtime, update_period)
            return


        # Command antenna to load NORAD TLE satellite tracking now and go to the starting coordinate.
        # IMPORTANT: The antenna must arrive at command position before
        # we can start to run the subscans and record data.
        # Set blocking=False for the remote function that commands the ACU because we
        # must maintain the opportunity to cancel the slewing from Scan controller.
        self.antenna.trackCoordinateTLE(
            self.tle_line0,
            self.tle_line1,
            self.tle_line2,
            self.user_pointing_correction_az,
            self.user_pointing_correction_el,
            blocking,
            update_period,
        )

    def tle_to_altaz(self):
        """
        Calculate the AZ and EL from observer view @ (time, earth location)
        to the satellite on orbit.
        """
        # Use SGP4 model to calculate current position of the satellite
        satellite = Satrec.twoline2rv(self.tle_line1, self.tle_line2)
        e, (xs, ys, zs), v = satellite.sgp4(
            self.starttime_apy.jd1, self.starttime_apy.jd2
        )

        self.logger.logInfo("transform satellite ECI->AltAz @ observer(time, location)")

        # Create a coordinate of the satellite in Geocentric Celestial Reference System
        # This might not be perfect, but it is good enough to prepare the scan with
        # initial pointing before MTM ACU starts tracking the TLE in real time
        satellite_location = CartesianRepresentation(x=xs, y=ys, z=zs, unit=units.km)
        satellite_coord_GCRS = GCRS(satellite_location, obstime=self.starttime_apy)

        # Create a reference frame for observer at Earth location while Earth is rotated
        # according to time of observation.
        observer_frame_AltAz = AltAz(
            location=self.site_location, obstime=self.starttime_apy
        )

        # Transform the coordinate of the satellite to the observer's coordinate system
        # (SS. I don't understand whey we have to specify the obstime in both the GCRS object
        # and the AltAz object.  If we don't set in both, the location
        # appears to be lost in the transform
        observed_coord_AltAz = satellite_coord_GCRS.transform_to(observer_frame_AltAz)

        return (observed_coord_AltAz.az.deg, observed_coord_AltAz.alt.deg)


class TrackCoordSS(AbstractTrackCoord):
    """
    Class to keep common functions or attributes of any scan that is tracking
    Solar System.
    """

    def __init__(self, metadata, planet, common):
        """
        Constructor.  Keep coommon variables

        Parameters
        ----------
        planet : string
                Solar System Planet
        common : ScanMod.TrackingParamsCommon
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Use the constructor of parent classes to process common parameters
        AbstractTrackCoord.__init__(self, metadata, common)

        # Extend parent class with details of this class
        self.planet = planet

    # Define methods that are required by the parent class, but implemented in subclass
    # These methods are tagged with decorator @abstractmethod

    def start_tracking(self, stop_event, blocking=True, update_period=5.0):
        """
        Point the antenna to (az + offset_az, el + offset_el)
        """
        # Use method of parent class for common behavior
        super().start_tracking(stop_event, blocking)

        # Calculate first tracking coordinate in Horizontal coordinates (AZ, EL)
        # This coordiante calculation  is only to find the starting AZ/EL to estimate slew time before we
        # start the data recording for the scan.
        (initial_az, initial_el) = self.jplephem_to_altaz()
        self.logger.debug(
            "Calculated (initial_az, initial_el) from SGP4 = (%f, %f)"
            % (initial_az, initial_el)
        )

        try:
            slewtime = self.antenna.slewTime(
                initial_az,
                initial_el,
                tnrtAntennaMod.azel,
                Time.now().mjd,
                2000,
                0.0,
                0.0,
            )
            if slewtime == -1.0:
                raise NameError("Cannot get current AZ, EL from Antenna component or ACU")
            self.logger.logInfo(
                "Estimate slew time to (%f, %f) = %f"
                % (initial_az, initial_el, slewtime)
            )
        except Exception as e:
            # Create a fake slew time so we can test the Scan software without connection to the
            # ACU and azimuth / elevation encoders.
            self.logger.logError(
                "Catch exception Antenna.slewtime() (ACU not connected?). Set fake slewtime = 10.0"
            )
            slewtime = 10.0
            self.logger.debug("For testing, run blocking_wait(%f)" % slewtime)

            # Call a blocking function in  simulates the behavior of ACU
            # taking time to transit.  log ScanC will show an update every <update_period> seconds.
            self.blocking_wait(slewtime, update_period)
            return

        duration_1day = 86400
        self.antenna.trackCoordinateSS(
            Time.now().mjd,
            duration_1day,
            self.planet,
            self.user_pointing_correction_az,
            self.user_pointing_correction_el,
            blocking,
            update_period,
        )

    # Define other methods

    def jplephem_to_altaz(self):
        """
        Calculate the AZ and EL from observer view @ (time, earth location)
        to the object in solar system.
        """

        self.logger.logInfo("transform ss skycoords->AltAz @ observer(time, location)")

        # Create a coordinate of the solar system object
        # This might not be perfect, but it is good enough to prepare the scan with
        # initial pointing before MTM ACU starts tracking the SS in real time

        obj_coord_GCRS = get_body(self.planet, self.starttime_apy, self.site_location)

        # Create a reference frame for observer at Earth location while Earth is rotated
        # according to time of observation.
        observer_frame_AltAz = AltAz(
            location=self.site_location, obstime=self.starttime_apy
        )

        # Transform the coordinate of the satellite to the observer's coordinate system
        # (SS. I don't understand why we have to specify the obstime in both the GCRS object
        # and the AltAz object.  If we don't set in both, the location
        # appears to be lost in the transform
        observed_coord_AltAz = obj_coord_GCRS.transform_to(observer_frame_AltAz)

        return (observed_coord_AltAz.az.deg, observed_coord_AltAz.alt.deg)


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    logger = logging.getLogger(__name__)

    metadata = 0
    tracking_common = 0
    try:
        t_Abstract = AbstractTrackCoord(metadata, tracking_common)
    except TypeError as e:
        logger.logError(e)
        logger.error(
            "Create object of a non-abstract child class that inherits from %s"
            % __file__
        )
        logger.error(
            "For Example: TrackCoordNone, TrackCoordHO, TrackCoordEQ, TrackCoordTLE, TrackCoordSS"
        )

    # -------------------
    # test TrackCoordNone
    # -------------------
    t_None = TrackCoordNone()

    # -------------------
    # test TrackCoordHO
    # -------------------
    md = ScanDefaults.metadata
    az = 270.0
    el = 85.0
    user_pointing_correction_az = 0
    user_pointing_correction_el = 0
    elevation_min = 15
    elevation_max = 85
    north_crossing = True
    use_horizontal_tables = False
    max_tracking_errors = 6

    tracking_common = ScanMod.TrackingParamsCommon(
        user_pointing_correction_az,
        user_pointing_correction_el,
        elevation_min,
        elevation_max,
        north_crossing,
        use_horizontal_tables,
        max_tracking_errors,
    )
    t_HO = TrackCoordHO(md, az, el, tracking_common)

    # -------------------
    # test TrackCoordEQ
    # -------------------
    md = ScanDefaults.metadata
    md.SCAN_MBFITS_HEADER.MJD = Time.now().mjd

    ra = 100.0
    dec = 50.0

    # Space motion -- affects angle to track the object
    pm_ra = (0,)
    pm_dec = 0
    parallax = 0

    # Velocity affects Doppler shift of RF frequency
    radial_velocity = 0

    user_pointing_correction_az = 0
    user_pointing_correction_el = 0
    elevation_min = 15
    elevation_max = 85
    north_crossing = True
    use_horizontal_tables = False
    max_tracking_errors = 6

    tracking_common = ScanMod.TrackingParamsCommon(
        user_pointing_correction_az,
        user_pointing_correction_el,
        elevation_min,
        elevation_max,
        north_crossing,
        use_horizontal_tables,
        max_tracking_errors,
    )
    t_EQ = TrackCoordEQ(
        md, ra, dec, pm_ra, pm_dec, parallax, radial_velocity, tracking_common
    )

    # -------------------
    # test TrackCoordTLE
    # -------------------
    md = ScanDefaults.metadata
    md.SCAN_MBFITS_HEADER.MJD = Time.now().mjd

    from ScanImpl import Scan

    scan = Scan.Scan()
    TLEData = scan.get_TLE(38098)  # INTELSAT 22
    tle_line0 = TLEData.name
    tle_line1 = TLEData.line1
    tle_line2 = TLEData.line2

    user_pointing_correction_az = 0
    user_pointing_correction_el = 0
    elevation_min = 15
    elevation_max = 85
    north_crossing = True
    use_horizontal_tables = False
    max_tracking_errors = 6

    tracking_common = ScanMod.TrackingParamsCommon(
        user_pointing_correction_az,
        user_pointing_correction_el,
        elevation_min,
        elevation_max,
        north_crossing,
        use_horizontal_tables,
        max_tracking_errors,
    )

    t_TLE = TrackCoordTLE(md, tle_line0, tle_line1, tle_line2, tracking_common)
