# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.10.30

from abc import ABC
from abc import abstractmethod
import logging

import ScanMod


class FrontendModelFactory:
    def __init__(self, frontend_params):
        if isinstance(frontend_params, ScanMod.FrontendParamsL):
            self.name = "L"
            self.component_name = "FrontendL"

        elif isinstance(frontend_params, ScanMod.FrontendParamsK):
            self.name = "K"
            self.component_name = "FrontendK"
            self.auto_enable_lna = frontend_params.auto_enable_lna

        elif isinstance(frontend_params, ScanMod.FrontendParamsCX):
            self.name = "CX"
            self.component_name = "FrontendCX"
        elif isinstance(frontend_params, ScanMod.FrontendParamsKu):
            self.name = "KU"
            self.component_name = "FrontendKu"
        elif isinstance(frontend_params, ScanMod.FrontendParamsOptical):
            self.name = "OPTICAL"
            self.component_name = "FrontendOptical"
        else:
            self.name = "NULL"
            self.component_name = ""


class AbstractFrontendModel(ABC):
    """
    ..todo:: add content
    """

    # Define properties and methods that are abstract, and must be implemented by a subclass
    # before a FrontendModel object can be created.

    @abstractmethod
    def __init__(self, metadata):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def stop(self):
        pass

    @abstractmethod
    def deconfigure(self):
        pass

    @abstractmethod
    def configure(self):
        pass


class FrontendModelL(AbstractFrontendModel):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    def start(self):
        self.logger.logDebug(">>>> TODO start client")

    def stop(self):
        self.logger.logDebug(">>>> TODO stop client")

    def configure(self):
        self.logger.logDebug(">>>> TODO configure")

    def deconfigure(self):
        self.logger.logDebug(">>>> TODO deconfigure")


class FrontendModelCX(AbstractFrontendModel):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    def start(self):
        self.logger.logDebug(">>>> TODO start client")

    def stop(self):
        self.logger.logDebug(">>>> TODO stop client")

    def configure(self):
        self.logger.logDebug(">>>> TODO configure")

    def deconfigure(self):
        self.logger.logDebug(">>>> TODO deconfigure")


class FrontendModelKu(AbstractFrontendModel):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    def start(self):
        self.logger.logDebug(">>>> TODO start client")

    def stop(self):
        self.logger.logDebug(">>>> TODO stop client")

    def configure(self):
        self.logger.logDebug(">>>> TODO configure")

    def deconfigure(self):
        self.logger.logDebug(">>>> TODO deconfigure")


class FrontendModelK(AbstractFrontendModel):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    def start(self):
        self.logger.logDebug(">>>> TODO start client")

    def stop(self):
        self.logger.logDebug(">>>> TODO stop client")

    def configure(self):
        self.logger.logDebug(">>>> TODO configure")

    def deconfigure(self):
        self.logger.logDebug(">>>> TODO deconfigure")


class FrontendModelOptical(AbstractFrontendModel):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    def start(self):
        self.logger.logDebug(">>>> TODO start client")

    def stop(self):
        self.logger.logDebug(">>>> TODO stop client")

    def configure(self):
        self.logger.logDebug(">>>> TODO configure")

    def deconfigure(self):
        self.logger.logDebug(">>>> TODO deconfigure")


class FrontendModelNone(AbstractFrontendModel):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class {}, module {}".format(self.__class__.__name__, (__name__))
        )

    def start(self):
        pass

    def stop(self):
        pass

    def configure(self):
        pass

    def deconfigure(self):
        pass


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    pass
