# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.11.10

from abc import ABC
from abc import abstractmethod
import os
import logging
import copy
import numpy as np
import threading
import time
import json

from astropy.time import Time
from astropy.io import fits

# Import Alma Common Software (ACS)
from Acspy.Clients.SimpleClient import PySimpleClient
from AcsutilPy.ACSPorts  import getIP

# Import TCS modules
from tracking_models import TrackCoordModelFactory
from frontend_models import FrontendModelFactory
from backend_models import BackendModelFactory
from Supplier import Supplier
import BackendError
import ScanErrorImpl

import ScanDefaults
import ScanMod
import tnrtAntennaMod
import TypeConverterAntenna as tc

from subscan_models import SubscanSingle
from subscan_models import SubscanLine
from subscan_models import SubscanFocus
from subscan_models import SubscanSleep

# Shortcuts for ScanFocus
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import hxp_x as hxp_x
from tnrtAntennaMod import hxp_y as hxp_y
from tnrtAntennaMod import hxp_z as hxp_z
from tnrtAntennaMod import hxp_tx as hxp_tx
from tnrtAntennaMod import hxp_ty as hxp_ty
from tnrtAntennaMod import hxp_tz as hxp_tz

from CentralDB import CentralDB
import cdbConstants

class ScanModelFactory:
    @classmethod
    def from_params(
        cls,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        logger = logging.getLogger(cls.__name__)
        logger.logTrace(
            "test logger in class {}, module {}".format(cls.__name__, (__name__))
        )
        logger.logDebug("Construct Scan object from {}".format(type(tracking_params)))

        if isinstance(scantype_params, ScanMod.ScantypeParamsCalColdload):
            obj = ScanCalColdload(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsCalHotload):
            obj = ScanCalHotload(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsCalNoise):
            obj = ScanCalNoise(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsCross):
            obj = ScanCross(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsFivePoint):
            obj = ScanFivePoint(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsFocus):
            obj = ScanFocus(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsMap):
            obj = ScanMap(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsManual):
            obj = ScanManual(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsOnSource):
            obj = ScanOnSource(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsRaster):
            obj = ScanRaster(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsSkydip):
            # Special case for ScanSkydip.
            # For skydip, the user cannot choose another type of tracking coord, therefore so we
            # create a TrackingParamsHO with az and skydip start_el. other parameters remain default.
            tracking_common = ScanMod.TrackingParamsCommon(0, 0, 0, 90, True, True, 6)
            tracking_params = ScanMod.TrackingParamsHO(
                scantype_params.az, scantype_params.el_start, tracking_common
            )
            obj = ScanSkydip(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        elif isinstance(scantype_params, ScanMod.ScantypeParamsTimeSync):
            obj = ScanTimeSync(
                schedule_params,
                scantype_params,
                tracking_params,
                frontend_params,
                backend_params,
                data_params,
            )

        else:
            raise ValueError("invalid scantype")

        return obj


class AbstractBaseScan(ABC):
    """
    AbstractBaseScan an abstract class that cannot be instantiated.
    Some common functions and variables are implemented in this abstract class and can be
    used from child classes with the super(). functions.classes.

    Python will not allow the user to create an instance of this class because it contains
    abstract properties and functions.
    """

    # Define properties and methods that are abstract, and must be implemented by a subclass
    # before a Scan object can be created.

    @abstractmethod
    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Scan Attribute
        self.scan_number = schedule_params.scan_id
        self.proj_id = schedule_params.project_id
        self.obs_id = schedule_params.obs_id
        self.source_name = schedule_params.source_name
        self.line_name = schedule_params.line_name

        self.nc_supplier = None

        self.ip = getIP()

        # List of data pipeline names
        self.pipeline_names = data_params.pipeline_name_list # list of strings

        # Dictionary of results
        self.results = {}

        # Client to connect components
        self.sc = None

        # Name of Antenna component to send commands for pointing and tracking.
        self.antenna_name = "AntennaTNRT"
        self.antenna = None

        # Name of DataAggregator component to access data pipelines.
        self.da_name = "DataAggregator"
        self.da = None

        # Name of Frontend component
        self.frontend_instance = FrontendModelFactory(frontend_params)
        self.frontend = None

        # Name of Backend component
        self.backend_instance = BackendModelFactory(backend_params)
        self.backend = None

        self.scheduler = None
        self.subscans_finished = threading.Event()
        self.cleanup_finished = threading.Event()

        # self.run() will assign an event to this object.  to interrupt scan.
        self.stop_event = None

        # Create data structure for MBFITS metadata and set default values.
        self.metadata = copy.deepcopy(ScanDefaults.metadata)

        # Create CDB instance
        self.cdb = CentralDB()

        self.schedule_params = schedule_params

        # Use the Factory method to create the correct type of TrackCoord  / FrontendModel, BackendModel objects from parameters
        self.trackcoord = TrackCoordModelFactory.from_params(
            self.metadata, tracking_params
        )

        self.duration = 0

        self.exception = None

    @abstractmethod
    def calculate_duration(self):
        pass

    def tostring(self):
        return "%s" % self.__class__.__name__

    def is_system_ready(self):
        """
        Check if all equipment is ready to work.
        """
        self.logger.logDebug("Enter %s.check_system_ready()" % self.__class__.__name__)
        # TODO - implement this
        return True

    def set_starttime(self, start_mjd):
        self.logger.logInfo("Setting start time of tracking to %f [MJD]" % start_mjd)
        self.starttime_apy = Time(start_mjd, format="mjd", scale="utc")
        self.cdb.set(cdbConstants.MJD, start_mjd)
        self.cdb.set(cdbConstants.DATE_OBS, self.starttime_apy.isot)

    def connect_components(self):
        """
        Connect to other ACS components required by this Scan.
        """
        # Client to access other ACS components
        self.logger.logInfo(
            ""
        )  # blank line to make easier for human to read log message
        self.logger.logInfo(
            "Initialize %s from %s" % (self.__class__.__name__, (__name__))
        )
        self.logger.logInfo("Starting PySimpleClient")
        self.sc = PySimpleClient()

        # Connect to DataAggregator for access to data pipelines
        try:
            self.logger.logInfo("Connecting to component %s" % self.da_name)
            self.da = self.sc.getComponent(self.da_name)
        except Exception as e:
            self.logger.logError("Cannot get component reference for %s" % self.da_name)

        # Connect to Antenna for access to pointing and tracking
        try:
            self.logger.logInfo("Connecting to component %s" % self.antenna_name)
            self.antenna = self.sc.getComponent(self.antenna_name)
        except Exception as e:
            self.logger.logError(
                "Cannot get component reference for %s" % self.antenna_name
            )

        # Connect to selected frontend component
        try:
            self.logger.logInfo(
                "Connecting to component %s" % self.frontend_instance.component_name
            )
            self.frontend = self.sc.getComponent(self.frontend_instance.component_name)
        except Exception as e:
            self.logger.logError(
                "Cannot get component reference for %s"
                % self.frontend_instance.component_name
            )

        # Connect to selected backend component
        try:
            self.logger.logInfo(
                "Connecting to component %s" % self.backend_instance.component_name
            )
            self.backend = self.sc.getComponent(self.backend_instance.component_name)
        except Exception as e:
            self.logger.logError(
                "Cannot get component reference for %s"
                % self.backend_instance.component_name
            )

        # Create Supplier for notification channel to publish metadata updates
        self.logger.logInfo(
            "Creating Notification Channel Supplier %s " % ScanMod.CHANNEL_NAME_METADATA
        )
        try:
            self.nc_supplier = Supplier(ScanMod.CHANNEL_NAME_METADATA)
            self.logger.logDebug("time.sleep 0.5 seconds waiting for Supplier to be ready on {}".format(ScanMod.CHANNEL_NAME_METADATA))
            time.sleep(0.5)
            # Send dummy metadata to "wake up" the ZMQ notification channel
            self.logger.logDebug("publish initial metadata on {}".format(ScanMod.CHANNEL_NAME_METADATA))
            self.nc_supplier.publish_event(self.metadata)

        except Exception as e:
            self.logger.logError(e)
            self.nc_supplier = None
            self.logger.logError(
                "Failed to initialize NC Supplier: %s " % ScanMod.CHANNEL_NAME_METADATA
            )

    def start_data_pipelines(self):
        """
        Open data pipelines to start recording data
        """

        # we have to skip gildas if cannot import pyclassfiller to not block the whole observation
        if "gildas" in self.pipeline_names:
            try:
                import pyclassfiller
            except ModuleNotFoundError as e:
                self.pipeline_names.remove("gildas")
                self.logger.logWarning("Unable to import pyclassfiller: Skip gildas file")

        for pname in self.pipeline_names:
            self.da.open_pipeline(pname)

    def run_subscans(self):
        """
        Run subscans immediately.  allow self.stop_event to interrupt and stop the subscan
        """
        # Antenna should already be tracking the center coordinate of the scan pattern,
        # Now that the scheduled time has arrived, start the data pipelines immediately before
        # the first subscan.
        # If the scan is stopped / canceled before this function run_subscans is called by the
        # Timer thread, the pipelines will not be started and not waste any data files.
        try:
            self.start_data_pipelines()

            # Wait for measurement prepare to finish before running measurement_start
            # EddfitsClient must start after start_data_pipelines because it needs to know 
            # which pipeline to write
            if self.backend:
                self.measurement_prepare_thread.join()
                self.logger.logInfo('Backend measurment starting...')
                self.backend.measurement_start()
                self.logger.logInfo('Backend measurment started')

            # Clear accumulated MONITOR-xxx data
            self.logger.logDebug("Clear monitor buffers")
            self.da.clear_monitor_buffers()

            # Write the scan header for all pipelines
            self.logger.logDebug("Write scan header")
            self.da.write_scan_header()

        except AttributeError as e:
            self.logger.logError("DataAggregator not connected. {}".format(e))
        except Exception as e:
            self.exception = e
            self.subscans = []
            self.da.skip_subscan()
            self.cleanup()
            

        for ss in self.subscans:

            # clear cdb data about subscan before start new subscan
            self.cdb.reset_subscan()          

            subscan_date = Time.now().isot
            self.logger.info("run subscan %d, name=%s" % (ss.subs_num, ss.name))

            self.cdb.set(cdbConstants.SUBSCAN_DATEOBS, subscan_date)
            self.cdb.set(cdbConstants.SUBSCAN_NUMBER, ss.subs_num)
            self.cdb.set(cdbConstants.SUBSTYPE, ss.subs_type)
            
            self.logger.logDebug("ss.trackMode: {}".format(ss.trackMode))
            if ss.trackMode == tnrtAntennaMod.azel:
                self.logger.logDebug("set (NLNGTYPE, NLATTYPE) = (ALON-GLS, ALAT-GLS)")
                self.cdb.set(cdbConstants.MOVEFRAM, True)
                self.cdb.set(cdbConstants.SUBS_NLNGTYPE, 'ALON-GLS')
                self.cdb.set(cdbConstants.SUBS_NLATTYPE, 'ALAT-GLS')

            elif ss.trackMode == tnrtAntennaMod.radec:
                self.logger.logDebug("set (NLNGTYPE, NLATTYPE) = (RA, DEC)")
                self.cdb.set(cdbConstants.SUBS_NLNGTYPE, 'RA')
                self.cdb.set(cdbConstants.SUBS_NLATTYPE, 'DEC')

            elif ss.trackMode == tnrtAntennaMod.radecshortcut:
                self.logger.logDebug("set (NLNGTYPE, NLATTYPE) = (RA, DEC)")
                self.cdb.set(cdbConstants.SUBS_NLNGTYPE, 'RA')
                self.cdb.set(cdbConstants.SUBS_NLATTYPE, 'DEC')
            else:
                self.logger.logDebug("Do not set (NLNGTYPE, NLATTYPE)")
            
            # Publish new values of metadata for [1] DataAggregator use the updated
            # data for file headers in pipelines.  [2] GUI preview can show correct data
            self.logger.logDebug("Publish SCAN medatata update: SCANNUM {}, SCAN DATE-OBS {}, SUBSNUM {}, SUBSCAN DATE-OBS {} ".format(
                self.scan_number,
                self.starttime_apy.isot,
                self.cdb.get(cdbConstants.SUBSCAN_NUMBER),
                self.cdb.get(cdbConstants.SUBSCAN_DATEOBS),
                )
            )

            is_finished_on_schedule = ss.run(self.stop_event, self.pipeline_names)
            # If a subscan is stopped from outside, exit the loop and don't run more subscans
            if is_finished_on_schedule:
                # Log some message and continue to the next item in the for loop
                self.logger.logDebug("subscan {} finished on schedule.".format(ss.subs_num))
            else:
                # Log some message and exit the for loop.  Do not process next subscans
                self.logger.logDebug("subscan {} stopped manually".format(ss.subs_num))
                break

        # Set the flow-control event to signal that all subscans are finished.
        # and unblock self.run() which is waiting for this signal/
        self.subscans_finished.set()

    def cleanup(self):
        """
        Disconnect components, close data pipelines, etc.
        """
        self.cleanup_finished.clear()

        self.logger.logDebug("Enter %s.cleanup()" % self.__class__.__name__)

        filename_list = []
        
        for ss in self.subscans:
            try:
                ss.cleanup_thread.join()
            except Exception as e:
                self.logger.logError(e)

        # Stop the antenna tracking before closing pipelines.  Sometimes data pipelines take
        # > 5 seconds to close.  Meanwhile, Antenna is slewing from last program offset table
        # entry back to Program Track coordinate for no reason.
        self.logger.debug("Stop antenna tracking")
        try:
            self.antenna.stopTracking()
        except AttributeError:
            self.logger.logError("Antenna not connected.  Cannot stop tracking")
        
        try:
            self.antenna.setUserPointingCorrections(0,0)
        except AttributeError:
            self.logger.logError("Antenna not connected.  Cannot clear user pointing corrections")


        # Create a None object first so we can test if exception was raised at the end of function
        da_acs_corba_exception = None
        try:    
            self.da.write_monitor()
        
            for pname in self.pipeline_names:
                self.logger.logDebug("close_pipeline {}".format(pname))
                filename_list = filename_list + [self.da.close_pipeline(pname)]

            self.results["filenames"] = filename_list
            self.logger.logDebug("Scan {} results: {}".format(self.scan_number, self.results))
        
        except AttributeError as e:
            self.logger.logError("DataAggregator not connected. {}".format(e))
        except Exception as e:
            self.logger.logError("Catch: {}".format(e))
            self.logger.logError("Prepare new ACS CORBA Exception with stack trace details")
            da_acs_corba_exception = ScanErrorImpl.datawriterExceptionExImpl(exception=e)

        # Stop backend 
        if self.backend:
            self.logger.logInfo('Backend measurment stopping...')
            self.backend.measurement_stop()
            self.logger.logInfo('Backend measurment stopped')

        # Stop frontend
        if self.frontend:
            self.logger.logInfo('Setting the mmic chains a and b to false ...')
            self.logger.logInfo('Setting the operation mode to default go-soft-off ...')
            self.logger.logInfo('Setting the digitizer to turn off ...')      
            self.frontend.deconfigure_frontend(self.frontend_instance.auto_enable_lna)
        try:
            self.sc.releaseComponent(self.da_name)
            self.da = None
        except Exception as e:
            self.logger.logError("Cannot release component reference for {}".format(self.da_name))

        try:
            self.sc.releaseComponent(self.antenna_name)
            self.antenna = None
        except Exception as e:
            self.logger.logError("Cannot stop and release component reference for {}".format(self.antenna_name))

        try:
            self.sc.releaseComponent(self.frontend_instance.component_name)
            self.frontend = None
        except Exception as e:
            self.logger.logError(
                "Cannot stop and release component reference for {}".format(self.frontend_instance.component_name)
            )

        try:
            self.sc.releaseComponent(self.backend_instance.component_name)
            self.backend = None
        except Exception as e:
            self.logger.logError(
                "Cannot stop and release component reference for {}".format(self.backend_instance.component_name)
            )

        # Run cleanup function on trackcoord to release references to antenna disconnect PySimpleClient
        self.trackcoord.cleanup()

        # disconnect Supplier for notification channel to publish metadata updates
        self.logger.logInfo("Disconnecting Notification Channel Supplier %s " % ScanMod.CHANNEL_NAME_METADATA)
        try:
            self.nc_supplier.disconnect()
        except Exception as e:
            self.nc_supplier = None
            self.logger.logError("Failed to initialize NC Supplier: %s " % ScanMod.CHANNEL_NAME_METADATA)

        try:
            self.sc.disconnect()
            del self.sc
            self.sc = None
        except Exception as e:
            self.logger.logError(e)
            self.sc = None

        self.cdb = None

        # Set the flow control event if another thread is waiting for this function to finish.

        # If run_queue() was called with blocking=True, this flow-control event has no effect
        # because the functions in the same thread already block until finished.

        # If run_queue() was called with blocking=False, this flow control event signals to a
        # waiting thread that cleanup is finished, files closed, and results are ready to read.
        self.cleanup_finished.set()

        if da_acs_corba_exception:
            raise da_acs_corba_exception

    def run(self, stop_event):
        """
        Run all subscans.  The sequence of events in this parent class is the standard.
        If a subclass must change the sequence of function calls, implement (override) this
        run() function in the sublcass.  For example.  This standard scan executor starts data
        pipelines before running all subscans.  Therefore, 1 output file includes data from all
        subscans.  If a Scan wants to create separate data files per subscan,
        implement the run and run_subscans() functions to start and stop data pipelines for each
        subscan.
        """
        self.logger.debug(
            "Enter %s.run(). Scan number = %d"
            % (self.__class__.__name__, self.scan_number)
        )

        # Keep a reference to the stop event that allows Scan ACS component to interrupt the scan
        self.stop_event = stop_event

        isready = self.is_system_ready()
        self.logger.info("System ready: %s" % isready)

        # clear cdb to prevent data from previous scan and fill current scan metadata
        self.cdb.reset_obs()
        self.update_scan_metadata()

        # If the scan does not have a start time (mjd == 0), set the start time to now
        # that it is available in the queue.  Update both the MBFITS header and the Astropy time
        # object that is used to command the tracking in ACU.
        now_mjd = Time.now().mjd
        if self.schedule_params.start_mjd < now_mjd:
            self.set_starttime(now_mjd)
            self.trackcoord.set_starttime(now_mjd)
        else:
            # If start_mjd is not set because scans enter queue as FIFO, MJD will not yet be valid (Time.now() when scan was created).
            # It will be updated when scan.run() and correct value saved in file.
            self.set_starttime(self.schedule_params.start_mjd)

        # Get access to Frontend, Backend, Antenna and DataAggregator pipelines and Notification Channel.
        self.connect_components()

        # Create ISO format string of Scan start time
        start_isot = Time(self.starttime_apy, format="mjd").isot

        # set number of subscan to header
        self.logger.logInfo('Number of subscan = {}'.format(len(self.subscans)))
        
        
        # Update the system metadata with infomration from this scan
        self.logger.logDebug("Publish SCAN medatata update: SCANNUM {}, SCAN DATE-OBS {}, NSUBS {}".format(
                self.scan_number,
                self.starttime_apy.isot,
                len(self.subscans)
            ))

        self.logger.logDebug("TODO configure frontend start of Scan")

        # Start Frontend
        if self.frontend:
            frontend_data = self.frontend.get_data()
            frontend_data_dict = json.loads(frontend_data)
            down_converter_freq = frontend_data_dict['down_converter_freq']
            self.cdb.set(cdbConstants.DOWN_CONVERTER_FREQ, down_converter_freq)
            
            self.logger.logInfo('Start configuring frontend ...')  
            self.frontend.configure_frontend(self.frontend_instance.auto_enable_lna)
            
        # Start Backend Pipeline
        if self.backend:
            self.logger.logDebug("Configuring backnd...")
            self.backend.client_start()
            self.backend.provision(self.backend_instance.preset_config_name)
            self.backend.configure(self.backend_instance.kwargs_json)
            self.backend.capture_start()

            # Measurement prepare may take a long time for some provision description.
            # so we run in another thread while waiting for antenna slewing to command 
            # coordinate
            self.measurement_prepare_thread = threading.Thread(
                target=self.backend.measurement_prepare,
                args=[""],
            )
            self.logger.logInfo('Backend measurement preparing...')
            self.measurement_prepare_thread.start()

        # Before we run subscans (on schedule), Start the main program track table.
        # to track the central tracking coordinate (HO, EQ, or TLE).
        # start_tracking will block until Antenna has arrived at coordinate.
        # Note: pass the stop event into start_tracking to allow the slewing
        # to cancel immediately if antenna has not yet arrived to command position.
        # If the timestamps in the ACU tracking table are in the future, the ACU will
        # move the Antenna to the tracking coordinate immediately so it is prepared  on track
        # before the first time stamp in the list.
        self.trackcoord.start_tracking(self.stop_event, blocking=False)
        self.trackcoord.block_until_arrived_or_canceled(update_period=5.0)

        if not self.subscans_finished.is_set():
            # Schedule the function call to run all subscans if the scan has not
            # been canceled already while Antenna is moving to tracking coordinate.
            
            # Each subscan does not have it's own start time.
            # Subscans will run immediately in sequence after Scan start time arrives on the Timer.
            SECONDS_PER_DAY = 86400.0
            countdown_seconds = SECONDS_PER_DAY * (
                self.starttime_apy.mjd - Time.now().mjd
            )

            self.logger.logDebug("Schedule scan to start: %s" % start_isot)
            self.logger.logDebug("countdown until scan start = %f [sec]" % countdown_seconds)
            self.scheduler = threading.Timer(countdown_seconds, self.run_subscans)
            self.scheduler.start()

            # Block here until subscans_finsihed event is set.  Then we can return
            # execution to Scan component and cleanup this Scan before next queue..
            self.logger.logDebug("Waiting for subscans_finished.set() ...")
            while not self.subscans_finished.wait(timeout=5):
                self.logger.logDebug("Waiting for subscans_finished.set() ...")
        
            try:
                self.scheduler.join()
                if self.exception:
                    raise self.exception
            except BackendError.ConnectionErrorEx as e: 
                self.logger.logError(e)
                raise ScanErrorImpl.backendsExceptionExImpl
            except Exception as e:
                self.logger.logError(e)
                raise e

        # Release, deconfigure, everything.
        self.cleanup()

        return self.results

    def stop(self):
        # If the scan is scheduled in the future (not started yet) and user stops the scan,
        # cancel the scheduled run.  No data, no results.
        if self.scheduler is not None:
            self.logger.logDebug("cancel scheduler threading.Timer")
            self.scheduler.cancel()

        # Set the flow-control event subscans_finished to unblock the run() function and allow
        # it to move on to cleanup()
        self.logger.logDebug("set event subscans_finished")
        self.subscans_finished.set()

        # Block and wait until cleanup is finished (write files, prepare results)
        # If the scan was canceled from the schedule before run() started, results will be empty.
        # If the scan was in progress, the cleanup() function closes data pipelines, prepares results.
        self.logger.logDebug("Waiting for cleanup_finished.set() ...")
        while not self.cleanup_finished.wait(timeout=2):
            self.logger.logDebug("Waiting for cleanup_finished.set() ...")

    def update_scan_metadata(self):
        # Update the metadata structure to redis
        self.cdb.set(cdbConstants.TELESCOPE_NAME, 'TNRO')
        self.cdb.set(cdbConstants.DIAMETER, 40)
        self.cdb.set(cdbConstants.TIMESYS, 'UTC')

        self.cdb.set(cdbConstants.PROJID, self.proj_id)
        self.cdb.set(cdbConstants.OBSID, self.obs_id)
        self.cdb.set(cdbConstants.SCANNUM, self.scan_number)
        self.cdb.set(cdbConstants.OBJECT, self.source_name)
        self.cdb.set(cdbConstants.MOLECULE, self.line_name)

        self.cdb.set(cdbConstants.FRONTEND_NAME, self.frontend_instance.name)
        self.cdb.set(cdbConstants.BACKEND_NAME, self.backend_instance.name)
        self.cdb.set(cdbConstants.PRESET_CONFIG_NAME, self.backend_instance.preset_config_name)

        self.cdb.set(cdbConstants.NSUBS, len(self.subscans))


class ScanCalColdload(AbstractBaseScan):
    """
    .. todo:: add content
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Constructor

        Parameters
        ----------
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        # Generate the offset tracking pattern.
        # This pattern includes a list of subscans.  Each subscan in the list/pattern
        # has a program offset table

        # Program offset az / el = 0.0 for On Source scan because the offset table should not
        # move away from the main tracking source.
        pgoffset_x = 0.0
        pgoffset_y = 0.0

        self.subscans = []

        self.logger.logInfo(
            "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
            % (
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )

        self.subscans.append(
            SubscanSingle(
                "single_OFF1",
                1,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_OFF_COLDLOAD",
                2,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_OFF2",
                3,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f), subs_type=%s"
                % (
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.xlim[1],
                    ss.ylim[0],
                    ss.ylim[1],
                    ss.subs_type,
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def run(self, stop_event):
        """
        Extend the stanard AbstractBaseScan run() function to post-process data immediately
        and add the result to the dictionary results{}
        """

        # Run the standard run() function from AbstractBaseScan
        super().run(stop_event)

        # Read the data file, calculate result, and return additional results to caller.
        self.logger.logDebug(
            "Process results of Scan number {} (ScanTimeSync):".format(self.scan_number)
        )

        if "filenames" in self.results.keys():
            self.logger.logDebug(
                "found file name in results dictionary: %s"
                % self.results["filenames"][0]
            )

            # Dictionary has a filename.  Open the file
            hdul = None

            if os.path.isfile(self.results["filenames"][0]) is not True:
                self.logger.logError("File not found")
                self.logger.debug(
                    "os.path.isfile(%s) = False" % self.results["filenames"][0]
                )
                hdul = None
            else:
                self.logger.logDebug(
                    "Processing file %s: " % self.results["filenames"][0]
                )
                # TODO
                # hdul = fits.open(self.results['filenames'][0])

                # Calculate the result:  Time difference between TCS and ACU in milliseconds.
                # If synchronized by NTP, the result should be better than 100ms

                # TODO
                self.results["???"] = 0

                # TODO close the file
                # hdul.close()

        return self.results


    def update_scan_metadata(self):
        super().update_scan_metadata()

        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "CAL_COLD_LOAD")
        self.cdb.set(cdbConstants.SCANMODE, "SAMPLE")
        self.cdb.set(cdbConstants.SCANGEOM, "SAMPLE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)


class ScanCalHotload(AbstractBaseScan):
    """
    Scan On Source where the source is in Horizontal Coordinates.
    Astropy calls this coordinate system 'AltAz'
    MTM ACU calls this coordinate system 'AZ-EL')
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Constructor

        Parameters
        ----------
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        # Generate the offset tracking pattern.
        # This pattern includes a list of subscans.  Each subscan in the list/pattern
        # has a program offset table

        # Program offset az / el = 0.0 for On Source scan because the offset table should not
        # move away from the main tracking source.

        pgoffset_x = 0.0
        pgoffset_y = 0.0

        # Add subscan single point on source
        self.subscans = []

        # If Reference Coordinate is defined for OFF Source, append a subscan

        self.logger.logInfo(
            "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
            % (
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )

        self.subscans.append(
            SubscanSingle(
                "single_OFF1",
                1,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_OFF_HOTLOAD",
                2,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_OFF2",
                3,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f), subs_type=%s"
                % (
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.xlim[1],
                    ss.ylim[0],
                    ss.ylim[1],
                    ss.subs_type,
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
        before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def run(self, stop_event):
        """
        Extend the stanard AbstractBaseScan run() function to post-process data immediately
        and add the result to the dictionary results{}
        """

        # Run the standard run() function from AbstractBaseScan
        super().run(stop_event)

        # Read the data file, calculate result, and return additional results to caller.
        self.logger.logDebug(
            "Process results of Scan number {} (ScanTimeSync):".format(self.scan_number)
        )

        if "filenames" in self.results.keys():
            self.logger.logDebug(
                "found file name in results dictionary: %s"
                % self.results["filenames"][0]
            )

            # Dictionary has a filename.  Open the file
            hdul = None

            if os.path.isfile(self.results["filenames"][0]) is not True:
                self.logger.logError("File not found")
                self.logger.debug(
                    "os.path.isfile(%s) = False" % self.results["filenames"][0]
                )
                hdul = None
            else:
                self.logger.logDebug(
                    "Processing file %s: " % self.results["filenames"][0]
                )
                # TODO open the file
                # hdul = fits.open(self.results['filenames'][0])

                # Calculate the result:  Time difference between TCS and ACU in milliseconds.
                # If synchronized by NTP, the result should be better than 100ms

                # TODO

                self.results["p_sky"] = 0
                self.results["p_sky_ns"] = 0
                self.results["y1"] = 0
                self.results["t_ns"] = 0
                self.results["t_sys"] = 0

                self.results["p_int"] = 0
                self.results["y5"] = 0
                self.results["t_a_star"] = 0

                self.results["p_r"] = 0
                self.results["y2"] = 0
                self.results["t_amb"] = 0
                self.results["eta_f"] = 0
                self.results["t_sys_star"] = 0

                self.results["p_r_ns"] = 0
                self.results["y3"] = 0
                self.results["t_ns"] = 0

                # TODO close the file
                # hdul.close()

        return self.results

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "CAL_HOT_LOAD")
        self.cdb.set(cdbConstants.SCANMODE, "SAMPLE")
        self.cdb.set(cdbConstants.SCANGEOM, "SAMPLE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)


class ScanCalNoise(AbstractBaseScan):
    """
    Scan On Source where the source is in Horizontal Coordinates.
    Astropy calls this coordinate system 'AltAz'
    MTM ACU calls this coordinate system 'AZ-EL')
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Constructor

        Parameters
        ----------
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        # Generate the offset tracking pattern.
        # This pattern includes a list of subscans.  Each subscan in the list/pattern
        # has a program offset table

        # Program offset az / el = 0.0 for On Source scan because the offset table should not
        # move away from the main tracking source.

        pgoffset_x = 0.0
        pgoffset_y = 0.0

        # Add subscan single point on source
        self.subscans = []

        # If Reference Coordinate is defined for OFF Source, append a subscan
        self.logger.logInfo(
            "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
            % (
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )

        self.subscans.append(
            SubscanSingle(
                "single_OFF1",
                1,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_OFF_NOISE",
                2,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
                subs_type="NOIS",
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_OFF2",
                3,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
        )
        self.subscans.append(
            SubscanSingle(
                "single_ON",
                4,
                scantype_params.cal_coord_params.duration,
                pgoffset_x,
                pgoffset_y,
                scantype_params.cal_coord_params.coord_system,
            )
        )

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f), subs_type=%s"
                % (
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.xlim[1],
                    ss.ylim[0],
                    ss.ylim[1],
                    ss.subs_type,
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
        before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def run(self, stop_event):
        """
        Extend the stanard AbstractBaseScan run() function to post-process data immediately
        and add the result to the dictionary results{}
        """

        # Run the standard run() function from AbstractBaseScan
        super().run(stop_event)

        # Read the data file, calculate result, and return additional results to caller.
        self.logger.logDebug(
            "Process results of Scan number {} (ScanTimeSync):".format(self.scan_number)
        )

        if "filenames" in self.results.keys():
            self.logger.logDebug(
                "found file name in results dictionary: %s"
                % self.results["filenames"][0]
            )

            # Dictionary has a filename.  Open the file
            hdul = None

            if os.path.isfile(self.results["filenames"][0]) is not True:
                self.logger.logError("File not found")
                self.logger.debug(
                    "os.path.isfile(%s) = False" % self.results["filenames"][0]
                )
                hdul = None
            else:
                self.logger.logDebug(
                    "Processing file %s: " % self.results["filenames"][0]
                )
                # TODO open the file
                # hdul = fits.open(self.results['filenames'][0])

                # Calculate the result:  Time difference between TCS and ACU in milliseconds.
                # If synchronized by NTP, the result should be better than 100ms

                # TODO
                self.results["p_sky"] = 0
                self.results["p_sky_ns"] = 0
                self.results["y1"] = 0
                self.results["t_ns"] = 0
                self.results["t_sys"] = 0

                self.results["p_source"] = 0
                self.results["y2"] = 0
                self.results["t_a"] = 0

                # TODO close the file
                # hdul.close()

        return self.results

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "CAL_NOISE")
        self.cdb.set(cdbConstants.SCANMODE, "SAMPLE")
        self.cdb.set(cdbConstants.SCANGEOM, "SAMPLE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)


class ScanCross(AbstractBaseScan):
    """
    Set up a cross pointing observation around a central HO (Az/EL) coordinate with with given arm
    length and time in seconds per arm.
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Set up a cross pointing observation around a central tracking coordinate with with given arm
        length and time in seconds per arm.
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        self.scantype_params = scantype_params

        # Add subscans
        self.subscans = []

        # Limits for positive and negative strokes of the cross
        lim_p = (-0.5 * self.scantype_params.arm_length, 0.5 * self.scantype_params.arm_length)
        lim_n = (0.5 * self.scantype_params.arm_length, -0.5 * self.scantype_params.arm_length)

        if scantype_params.double_cross is False:
            self.logger.info("double_cross is False.  Create a single cross scan")
            self.subscans.append(
                SubscanLine(
                    "lineHO1",
                    ScanDefaults.SUBS_NUM['CROSS']['HP'],
                    self.scantype_params.time_per_arm,
                    lim_p,
                    (0, 0),
                    tnrtAntennaMod.azel,
                )
            )
            self.subscans.append(
                SubscanLine(
                    "lineVO2",
                    ScanDefaults.SUBS_NUM['CROSS']['VP'],
                    self.scantype_params.time_per_arm,
                    (0, 0),
                    lim_p,
                    tnrtAntennaMod.azel,
                )
            )
        else:
            # double_cross is True.  Add the negative stroke in correct sequence
            self.logger.info("double_cross is True.  Create a double cross scan")
            self.subscans.append(
                SubscanLine(
                    "lineHO1",
                    ScanDefaults.SUBS_NUM['CROSS']['HP'],
                    self.scantype_params.time_per_arm,
                    lim_p,
                    (0, 0),
                    tnrtAntennaMod.azel,
                )
            )
            self.subscans.append(
                SubscanLine(
                    "lineHO2",
                    ScanDefaults.SUBS_NUM['CROSS']['HN'],
                    self.scantype_params.time_per_arm,
                    lim_n,
                    (0, 0),
                    tnrtAntennaMod.azel,
                )
            )
            self.subscans.append(
                SubscanLine(
                    "lineVO3",
                    ScanDefaults.SUBS_NUM['CROSS']['VP'],
                    self.scantype_params.time_per_arm,
                    (0, 0),
                    lim_p,
                    tnrtAntennaMod.azel,
                )
            )
            self.subscans.append(
                SubscanLine(
                    "lineVO4",
                    ScanDefaults.SUBS_NUM['CROSS']['VN'],
                    self.scantype_params.time_per_arm,
                    (0, 0),
                    lim_n,
                    tnrtAntennaMod.azel,
                )
            )

        # If Reference Coordinate is defined for OFF Source, add OFF source subscan to start and end of scan.
        if type(scantype_params.cal_coord_params) is not ScanMod.AbstractCalCoordParams:
            self.logger.logInfo(
                "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
                % (
                    scantype_params.cal_coord_params.duration,
                    scantype_params.cal_coord_params.dx,
                    scantype_params.cal_coord_params.dy,
                    scantype_params.cal_coord_params.coord_system,
                )
            )
            s_ref_before = SubscanSingle(
                "single_ref1",
                ScanDefaults.SUBS_NUM['CAL_BEFORE'],
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
            s_ref_after = SubscanSingle(
                "single_ref2",
                ScanDefaults.SUBS_NUM['CAL_AFTER'],
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )

            # Append reference OFF source subscans to beginning and end of list.
            self.subscans.insert(0, s_ref_before)
            self.subscans.append(s_ref_after)

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f)"
                % (
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.xlim[1],
                    ss.ylim[0],
                    ss.ylim[1],
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
        before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)
        
    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "CROSS")
        self.cdb.set(cdbConstants.SCANMODE, "OTF")
        self.cdb.set(cdbConstants.SCANGEOM, "LINE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)
        self.cdb.set(cdbConstants.SCANLINE, 2)
        self.cdb.set(cdbConstants.SCANTIME, self.scantype_params.time_per_arm )
        self.cdb.set(cdbConstants.SCANLEN, self.scantype_params.arm_length)


class ScanFivePoint(AbstractBaseScan):
    """
    Set up a cross pointing observation around a central HO (Az/EL) coordinate with with given arm
    length and time in seconds per arm.
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Set up a five point observation around a central tracking coordinate with with given arm
        length and time in seconds per point.
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        self.scantype_params = scantype_params

        pgoffset_az = scantype_params.arm_length / 2
        pgoffset_el = scantype_params.arm_length / 2

        # Add subscans
        self.subscans_x = []
        self.subscans_y = []

        self.subscans_x.append(
            SubscanSingle(
                "singleHO2",
                2,
                scantype_params.time_per_point,
                -1 * pgoffset_az,
                0,
                tnrtAntennaMod.azel,
            )
        )
        self.subscans_x.append(
            SubscanSingle(
                "singleHO3",
                3,
                scantype_params.time_per_point,
                0,
                0,
                tnrtAntennaMod.azel,
            )
        )
        self.subscans_x.append(
            SubscanSingle(
                "singleHO4",
                4,
                scantype_params.time_per_point,
                pgoffset_az,
                0,
                tnrtAntennaMod.azel,
            )
        )
        self.subscans_y.append(
            SubscanSingle(
                "singleHO7",
                7,
                scantype_params.time_per_point,
                0,
                -1 * pgoffset_el,
                tnrtAntennaMod.azel,
            )
        )
        self.subscans_y.append(
            SubscanSingle(
                "singleHO8",
                8,
                scantype_params.time_per_point,
                0,
                0,
                tnrtAntennaMod.azel,
            )
        )
        self.subscans_y.append(
            SubscanSingle(
                "singleHO9",
                9,
                scantype_params.time_per_point,
                0,
                pgoffset_el,
                tnrtAntennaMod.azel,
            )
        )

        # If Reference Coordinate is defined for OFF Source, add OFF source subscan to start and end of scan.
        if type(scantype_params.cal_coord_params) is not ScanMod.AbstractCalCoordParams:
            self.logger.logInfo(
                "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
                % (
                    scantype_params.cal_coord_params.duration,
                    scantype_params.cal_coord_params.dx,
                    scantype_params.cal_coord_params.dy,
                    scantype_params.cal_coord_params.coord_system,
                )
            )

            s_ref_before_x = SubscanSingle(
                "single_ref1",
                1,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
            s_ref_after_x = SubscanSingle(
                "single_ref2",
                5,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )

            s_ref_before_y = SubscanSingle(
                "single_ref3",
                6,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
            s_ref_after_y = SubscanSingle(
                "single_ref4",
                10,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )

            # Append ref coords to list of X axis subscans
            self.subscans_x.insert(0, s_ref_before_x)
            self.subscans_x.append(s_ref_after_x)

            # Append ref coords to list of Y axis subscans
            self.subscans_y.insert(0, s_ref_before_y)
            self.subscans_y.append(s_ref_after_y)

        # Concatenate the lists using '+' operator.
        # TODO (SS): is this safe in Python3?  Should we use .extend()?
        self.subscans = self.subscans_x + self.subscans_y

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f)"
                % (
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.xlim[1],
                    ss.ylim[0],
                    ss.ylim[1],
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "FIVEPOINT")
        self.cdb.set(cdbConstants.SCANMODE, "SAMPLE")
        self.cdb.set(cdbConstants.SCANGEOM, "SAMPLE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)
        self.cdb.set(cdbConstants.SCANTIME, self.scantype_params.time_per_point)


class ScanFocus(AbstractBaseScan):
    """
    Set up a .
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        TODO
        """

        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        # Initialize start and stop position range of 5 hexapod axes in the focus scan.
        position_range = {
            tnrtAntennaMod.hxp_x: [0, 0],
            tnrtAntennaMod.hxp_y: [0, 0],
            tnrtAntennaMod.hxp_z: [0, 0],
            tnrtAntennaMod.hxp_tx: [0, 0],
            tnrtAntennaMod.hxp_ty: [0, 0],
            tnrtAntennaMod.hxp_tz: [0, 0],
        }

        # Initialize velocity of 5 hexapod axes in the focus scan
        velocity = {
            tnrtAntennaMod.hxp_x: 0,
            tnrtAntennaMod.hxp_y: 0,
            tnrtAntennaMod.hxp_z: 0,
            tnrtAntennaMod.hxp_tx: 0,
            tnrtAntennaMod.hxp_ty: 0,
            tnrtAntennaMod.hxp_tz: 0,
        }

        # Set the values of position range and velocity for selected axis
        # Note:  If the user selected an axis, but did not choose a position range or velocity,
        # the value will be set to -999 temporarily until we choose the correct default for that axis..

        self.logger.logDebug(
            "position_range[scantype_params.axis][0]: {}".format(
                position_range[scantype_params.axis][0]
            )
        )
        position_range[scantype_params.axis][0] = scantype_params.start_pos
        position_range[scantype_params.axis][1] = scantype_params.end_pos
        velocity[scantype_params.axis] = np.abs(scantype_params.velocity)

        # Check if the user did not set the optional parameters.  If not set, the default
        # of start_pos, end_pos, or velocity is still 999 and we must overwrite with the
        # correct default value for the selected axis.
        if scantype_params.start_pos == 999:
            self.logger.logWarning(
                "start_pos is not set. using minimum limit {}".format(
                    tc.subsystem_properties[HXP][scantype_params.axis]["EndDn"]
                )
            )
            position_range[scantype_params.axis][0] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["EndDn"]

        if scantype_params.end_pos == 999:
            self.logger.logWarning(
                "end_pos is not set. using maximum limit {}".format(
                    tc.subsystem_properties[HXP][scantype_params.axis]["EndUp"]
                )
            )
            position_range[scantype_params.axis][1] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["EndUp"]

        if scantype_params.velocity == 999:
            self.logger.logWarning(
                "velocity is not set. using maximum limit {}".format(
                    tc.subsystem_properties[HXP][scantype_params.axis]["v_MaxSys"]
                )
            )
            velocity[scantype_params.axis] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["v_MaxSys"]

        # If the current value of the axis has been
        # selected by the user, but the value is under / over limit, set to the min / max for full length
        # focus scan of that axis.
        if (
            scantype_params.start_pos
            < tc.subsystem_properties[HXP][scantype_params.axis]["EndDn"]
        ):
            self.logger.logWarning(
                "start_pos {} is under limit. using minimum limit {}".format(
                    scantype_params.start_pos,
                    tc.subsystem_properties[HXP][scantype_params.axis]["EndDn"],
                )
            )
            position_range[scantype_params.axis][0] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["EndDn"]

        if (
            scantype_params.end_pos
            > tc.subsystem_properties[HXP][scantype_params.axis]["EndUp"]
        ):
            self.logger.logWarning(
                "end_pos {} is over limit. using maximum limit {}".format(
                    scantype_params.end_pos,
                    tc.subsystem_properties[HXP][scantype_params.axis]["EndUp"],
                )
            )
            position_range[scantype_params.axis][1] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["EndUp"]

        if (
            np.abs(scantype_params.velocity)
            > tc.subsystem_properties[HXP][scantype_params.axis]["v_MaxSys"]
        ):
            self.logger.logWarning(
                "velocity {} is over limit. using maximum limit {}".format(
                    np.abs(scantype_params.velocity),
                    tc.subsystem_properties[HXP][scantype_params.axis]["v_MaxSys"],
                )
            )
            velocity[scantype_params.axis] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["v_MaxSys"]

        if (
            np.abs(scantype_params.velocity)
            < tc.subsystem_properties[HXP][scantype_params.axis]["v_MinSys"]
        ):
            self.logger.logWarning(
                "velocity {} is under limit. using minimum limit {}".format(
                    np.abs(scantype_params.velocity),
                    tc.subsystem_properties[HXP][scantype_params.axis]["v_MinSys"],
                )
            )
            velocity[scantype_params.axis] = tc.subsystem_properties[HXP][
                scantype_params.axis
            ]["v_MinSys"]

        # Calculate duration from limits and velocity
        # This calculation should work correctly for both linear and angular motions becuase
        # The velocity unit is already linear / angular to match the position unit.
        subscan_duration = (
            position_range[scantype_params.axis][1]
            - position_range[scantype_params.axis][0]
        ) / velocity[scantype_params.axis]

        # Add subscans
        self.subscans = []
        self.subscans.append(
            SubscanFocus(
                "focus",
                1,
                subscan_duration,
                position_range[tnrtAntennaMod.hxp_x],
                position_range[tnrtAntennaMod.hxp_y],
                position_range[tnrtAntennaMod.hxp_z],
                position_range[tnrtAntennaMod.hxp_tx],
                position_range[tnrtAntennaMod.hxp_ty],
                position_range[tnrtAntennaMod.hxp_tz],
            )
        )

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num {}: {} axis={}, pos=({} -> {}), vel={})".format(
                    ss.subs_num,
                    ss.__class__.__name__,
                    scantype_params.axis,
                    position_range[scantype_params.axis][0],
                    position_range[scantype_params.axis][1],
                    velocity[scantype_params.axis],
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "FOCUS")
        self.cdb.set(cdbConstants.SCANMODE, "SAMPLE")
        self.cdb.set(cdbConstants.SCANGEOM, "SINGLE")
        self.cdb.set(cdbConstants.SCANLINE, 1)
        self.cdb.set(cdbConstants.SCANRPTS, 1)
        self.cdb.set(cdbConstants.SCANTIME, self.duration)
        self.cdb.set(cdbConstants.SCANLEN, 0)


class ScanMap(AbstractBaseScan):
    """
    Map (OTF)
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        self.scantype_params = scantype_params

        # Generate the SubscanLines for the map.  Note that we must provide all of the parameters
        # to the SubscanLine when we construct the object. If we change a value later, the internal
        # data representation of the Subscan will not be consistent.
        self.lines = []
        
        # Limits for positive and negative strokes of the map.  
        # These limits can be either X or Y depending on parameter scantype_params.axis
        
        lim_p = (-0.5 * self.scantype_params.line_length, 0.5 * self.scantype_params.line_length)
        lim_n = (0.5 * self.scantype_params.line_length, -0.5 * self.scantype_params.line_length)
        
        # Start with offset from center = 0, generate a range of offsets
        line_offsets_from_center_before_shift = np.arange(0, 
                                                          scantype_params.nlines * scantype_params.spacing, 
                                                          scantype_params.spacing)
        
        # Shift the entire list of offsets to center the pattern around on tracking coordinate 
        # NOTE: can be used for X or Y pattern
        pattern_shift = (line_offsets_from_center_before_shift[-1] - line_offsets_from_center_before_shift[0]) / 2
        line_offsets = line_offsets_from_center_before_shift - pattern_shift

        # If the number of lines in the map is odd, we expect to have the center line go through
        # the tracking center coordinate.  
        # If the number of lines is even, the pattern will not cross through the actual tracking center
        
        for k in range(len(line_offsets)):
            # Sort through the logic of pattern in X, Y and zigzag
            if scantype_params.axis == ScanMod.axis_x:
                ylim = (line_offsets[k], line_offsets[k]) # constant y position
                if (scantype_params.zigzag == True) and (k % 2 != 0):
                    xlim = lim_n # sweep line in x direction from [+] to [-]
                else:
                    xlim = lim_p # sweep line in x direction from [-] to [+]
            else:
                xlim = (line_offsets[k], line_offsets[k]) # constant x position
                if (scantype_params.zigzag == True) and (k % 2 != 0):
                    ylim = lim_n # sweep line in y direction from [+]
                else:
                    ylim = lim_p # sweep line in y direction from [-] to [+]
                
            # Construct the SubscanLine objects and append to the list
            # Subscan numbers for map lines should follow the pattern 
            self.lines.append(
                SubscanLine(
                    "line_{}".format(k),
                    1 + k,
                    self.scantype_params.time_per_line,
                    xlim,
                    ylim,
                    scantype_params.coord_system,
                    "HOLO"
                )
            )
        self.logger.logDebug("lines len: {}: data: {}".format(len(self.lines), self.lines))
        
        if scantype_params.subscans_per_cal < 1:
            # Do not create any center cal.  Simply re-assign the lines to be the complete
            # list of subscans for this map
            self.subscans = self.lines
        else:
            # Generate on-source calibration subscans and interleave into complete subscan list
            self.center_cals = []

            # Generate the first on-source calibration before the map starts
            # subscan number 0
            ss_cal0 = SubscanSingle(
                    "single_cal0",
                    0,
                    scantype_params.time_cal0,
                    0,
                    0,
                    scantype_params.coord_system,
                    "CORR"
                )
            
            self.logger.logDebug("center_cal0 data: {}".format(ss_cal0))
            
            # Generate the on-source calibrations that will be interleaved
            if (len(self.lines) % scantype_params.subscans_per_cal) != 0:
                raise ValueError("Invalid pattern. nlines={} is not a multiple of subscans_per_cal={}".format(
                    len(self.lines), scantype_params.subscans_per_cal))

            n_interleaved_cals = int(scantype_params.nlines / scantype_params.subscans_per_cal)
            
            for k in range(n_interleaved_cals):
                self.center_cals.append(
                    SubscanSingle(
                        "single_cal{}".format(k),
                        101 + k, # start subscan numbers at 100
                        scantype_params.time_per_cal,
                        0,
                        0,
                        scantype_params.coord_system,
                        "CORR"
                    )
                )

            self.logger.logDebug("center_cals len: {}: data: {}".format(len(self.center_cals), self.center_cals))
        
            # Interleave the lists  Final pattern should look like: like
            # ---------------------------------------------------------------
            # Initial center cal        subscan 0 (time_cal0. default 60 s)
            # holo map line             subscan 1 (sweep line)
            # interleaved center cal    subscan 101 (time_per_cal. default 5 s)
            # holo line                 subscan 2 (sweep line)
            # interleaved center cal    subscan 102 (time_per_cal. default 5 s)
            # ...
            # holo line                 subscan N (sweep line)
            # interleaved center cal    subscan 100+N (time_per_cal. default 5 s)

            # Use technique of numpy array: reshape, stack, reshape to interleave
            # the center_cal subscans into the map lines in the correct sequence.
            # NOTE: when we use -1 as the size of the array in any dimension, Numpy
            # uses the other dimesion (that is not -1) to specify the size of the "priority"
            # dimension.  The "-1" dimension will be filled with all available data
            # form the original array.
            lines_2d = np.reshape(self.lines, (-1, scantype_params.subscans_per_cal))
            center_cals_2d = np.reshape(self.center_cals, (-1,1))
            
            # combine the two 2D arrays together "side by side" now that lines has been reshaped
            # approprately to align the subscans_per_cal.
            stacked = np.hstack((lines_2d, center_cals_2d))

            # reshape the array to 1D and remove singleton dimensions.
            subscans_1d = np.squeeze(np.reshape(stacked, (1,-1)))
            
            # Convert ndarray and create the final list of subscans. 
            self.subscans = [ss_cal0] + subscans_1d.tolist()

            # Reset the subscan numbers
            for k in range(len(self.subscans)):
                self.logger.logDebug("k: {}, subs_num: {}".format(k, self.subscans[k].subs_num))
                self.subscans[k].subs_num = k+1
                self.logger.logDebug("k: {}, subs_num: {}".format(k, self.subscans[k].subs_num))

            # Check subscan numbers again
            for ss in self.subscans:
                self.logger.logDebug("ss.subs_num: {}".format(ss.subs_num))

        # Show results in log
        for ss in self.subscans:
            self.logger.logDebug("SUBSNUM: {}, data: {}".format(ss.subs_num, ss))

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()
    
    def calculate_duration(self):
        return(10) # TODO (SS 01/2024) calculate a real result for this

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "OTF_MAP")
        self.cdb.set(cdbConstants.SCANMODE, "OTF")
        self.cdb.set(cdbConstants.SCANGEOM, "LINE")
        self.cdb.set(cdbConstants.SCANRPTS, 1) # number of repeats of each line
        self.cdb.set(cdbConstants.SCANTIME, self.scantype_params.time_per_line)
        self.cdb.set(cdbConstants.SCANLEN, self.scantype_params.line_length/3600.0)        
        self.cdb.set(cdbConstants.SCANLINE, self.scantype_params.nlines) # number of lines
        self.cdb.set(cdbConstants.ZIGZAG, self.scantype_params.zigzag)
        # NOTE: the spacing of samples along the OTF line depends on Backend
        # integration time and velocity.  That information is not available in
        # this scope.  So, we set both spacing metadata fields to be the
        # spacing between the lines -- so at least one of them is correct.
        self.cdb.set(cdbConstants.SCANXSPC, self.scantype_params.spacing) # space between x samples
        self.cdb.set(cdbConstants.SCANYSPC, self.scantype_params.spacing) # space between y samples

        scandir_dict = {
            ScanMod.axis_x: {
                tnrtAntennaMod.azel: "ALON-GLS",
                tnrtAntennaMod.radec: "RA-SFL",
            },
            ScanMod.axis_y: {
                tnrtAntennaMod.azel: "ALAT-GLS",
                tnrtAntennaMod.radec: "DEC-SFL",
            },
        }

        self.cdb.set(
            cdbConstants.SCANDIR, 
            scandir_dict[self.scantype_params.axis][self.scantype_params.coord_system]
        )



class ScanManual(AbstractBaseScan):
    """
    Scan to record aux system data - no radio receivers
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Constructor

        Parameters
        ----------
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        # Add subscan
        self.subscans = [SubscanSleep("sleep1", 1, scantype_params.duration)]

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t duration=%f)"
                % (ss.subs_num, ss.__class__.__name__, ss.duration)
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.0
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "MANUAL")
        self.cdb.set(cdbConstants.SCANRPTS, 1)


class ScanOnSource(AbstractBaseScan):
    """
    Scan On Source where the source is in Horizontal Coordinates.
    Astropy calls this coordinate system 'AltAz'
    MTM ACU calls this coordinate system 'AZ-EL')
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Constructor

        Parameters
        ----------
        """
        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        # Generate the offset tracking pattern.
        # This pattern includes a list of subscans.  Each subscan in the list/pattern
        # has a program offset table

        # Program offset az / el = 0.0 for On Source scan because the offset table should not
        # move away from the main tracking source.

        pgoffset_az = 0.0
        pgoffset_el = 0.0

        # Add subscan single point on source
        self.subscans = []
        self.subscans.append(
            SubscanSingle(
                "singleHO1",
                2,
                scantype_params.duration,
                pgoffset_az,
                pgoffset_el,
                tnrtAntennaMod.azel,
            )
        )

        # If Reference Coordinate is defined for OFF Source, append a subscan
        if type(scantype_params.cal_coord_params) is not ScanMod.AbstractCalCoordParams:
            self.logger.logInfo(
                "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
                % (
                    scantype_params.cal_coord_params.duration,
                    scantype_params.cal_coord_params.dx,
                    scantype_params.cal_coord_params.dy,
                    scantype_params.cal_coord_params.coord_system,
                )
            )
            self.cdb.set(cdbConstants.SCANTYPE, "ON_OFF_SOURCE")

            s_ref_before = SubscanSingle(
                "singleHO_ref1",
                1,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
            s_ref_after = SubscanSingle(
                "singleHO_ref2",
                3,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )

            # Append the calibration refcoord to beginning and end of subscan list
            self.subscans.insert(0, s_ref_before)
            self.subscans.append(s_ref_after)

            # Show results in log
            for ss in self.subscans:
                self.logger.logInfo(
                    "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f)"
                    % (
                        ss.subs_num,
                        ss.__class__.__name__,
                        ss.xlim[0],
                        ss.xlim[1],
                        ss.ylim[0],
                        ss.ylim[1],
                    )
                )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

        # Duration of this pattern is the sum of durations of all subscans + some slew time
        # before, after subscans.
        # TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "ON_SOURCE")
        self.cdb.set(cdbConstants.SCANMODE, "SAMPLE")
        self.cdb.set(cdbConstants.SCANGEOM, "SAMPLE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)


class ScanRaster(AbstractBaseScan):
    """
    Set up a .
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        TODO
        """

        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )
        self.scantype_params = scantype_params
        self.subscans = []
        # Generate coordinates of the 2D map using numpy arrays

        # Generate x and y coordinates as separate 1D numpy arrays
        # coordinates will be used in a program offset table of ACU used while tracking central coordinate.
        # Therefore, these coordinates are relative to center.  center point should be 0.

        # If the step size divides perfectly into the length without remainder, add one more step to the
        # numpy arange function to include the endpoint (by default it doesn't include the endpoint)
        if scantype_params.xlen % scantype_params.xstep == 0:
            xx = np.arange(
                -1 * scantype_params.xlen / 2,
                scantype_params.xlen / 2 + scantype_params.xstep,
                scantype_params.xstep,
            )
        else:
            xx = np.arange(
                -1 * scantype_params.xlen / 2,
                scantype_params.xlen / 2,
                scantype_params.xstep,
            )

        if scantype_params.ylen % scantype_params.ystep == 0:
            yy = np.arange(
                -1 * scantype_params.ylen / 2,
                scantype_params.ylen / 2 + scantype_params.xstep,
                scantype_params.ystep,
            )
        else:
            yy = np.arange(
                -1 * scantype_params.ylen / 2,
                scantype_params.ylen / 2,
                scantype_params.xstep,
            )

        # Define a numpy data type to keep an x and y coordinate together in a numpy structured array
        # This will make easy work to create the array with vectorized numpy functions
        dtype_coord_xy = np.dtype([("x", np.float64), ("y", np.float64)])

        # Initialize an array of zeros using coordinate data type and dimensions of xx and yy generated above
        # Assume the primary axis is x. (step through all "columns" x, then move to the next "row" y)
        # keep this as an instance variable to help debugging.  Real operation will actually use the list
        # of subscans that is generated from the final array.
        self.map_coordinates = np.zeros((yy.size, xx.size), dtype=dtype_coord_xy)

        # Fill the numpy structured array by dictionary access.  generate the 2D array from meshgrid.
        self.map_coordinates["x"], self.map_coordinates["y"] = np.meshgrid(xx, yy)

        # If primary axis is Y, transpose the array.
        if scantype_params.primary_axis == ScanMod.axis_y:
            self.logger.logDebug(
                "Primary axis is Y (scan Y, then step X). Transpose the coordinate array"
            )
            self.map_coordinates = self.map_coordinates.T

        # If zigzag is true, reverse the order of odd-index rows in the 2D array.
        if scantype_params.zigzag == True:
            self.logger.logDebug(
                "zigzag is True.  Reverse the odd-numbered raster lines"
            )
            # use numpy array slice to get the odd-numbered raster lines
            # slice notation index [min_index : max_index : step] where max_index=-1 means last index of the array
            # in the first dimension.
            odd_raster_lines = self.map_coordinates[1:-1:2]

            # Flip the raster lines around the last axis (the axis we will traverse per point of the scan)
            odd_raster_lines_reversed = np.flip(odd_raster_lines, axis=1)

            # Overwrite the original map_coordinates odd-numbered raster lines with the new reversed lines.
            self.map_coordinates[1:-1:2] = odd_raster_lines_reversed

        # Traverse the numpy structured array and append SubscanSingle objects to the list of
        # subscans for for each (x,y) coordinate in the 2D array.

        # Check if we will add a calibration reference measurement before and after the raster.
        # If so, number the subscans of raster grid points starting from 2. else start at 1.
        # Then, insert and append calibration reference subscans after raster points are
        # generated (follow the same idea as other scan types)
        if type(scantype_params.cal_coord_params) is not ScanMod.AbstractCalCoordParams:
            nsubs = 2
        else:
            nsubs = 1

        for secondary_axis_raster_line in self.map_coordinates:
            for primary_axis_single_point in secondary_axis_raster_line:
                self.subscans.append(
                    SubscanSingle(
                        "SubscanSingle{}".format(nsubs),
                        nsubs,
                        scantype_params.time_per_point,
                        primary_axis_single_point["x"],
                        primary_axis_single_point["y"],
                        scantype_params.coord_system,
                    )
                )

                nsubs = nsubs + 1

        if type(scantype_params.cal_coord_params) is not ScanMod.AbstractCalCoordParams:
            self.logger.logInfo(
                "Use off-source ref coord duration=%f, x=%f, y=%f, system=%s"
                % (
                    scantype_params.cal_coord_params.duration,
                    scantype_params.cal_coord_params.dx,
                    scantype_params.cal_coord_params.dy,
                    scantype_params.cal_coord_params.coord_system,
                )
            )

            s_ref_before = SubscanSingle(
                "single_ref1",
                1,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )
            s_ref_after = SubscanSingle(
                "single_ref2",
                nsubs,
                scantype_params.cal_coord_params.duration,
                scantype_params.cal_coord_params.dx,
                scantype_params.cal_coord_params.dy,
                scantype_params.cal_coord_params.coord_system,
            )

            # Append the refcoord to beginning and end of subscan list
            self.subscans.insert(0, s_ref_before)
            self.subscans.append(s_ref_after)

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan number {:3d}: {} (x={},y={}) trackMode={}".format(
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.ylim[0],
                    ss.trackMode,
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

        scandir_dict = {
            ScanMod.axis_x: {
                tnrtAntennaMod.azel: "ALON-GLS",
                tnrtAntennaMod.radec: "RA-SFL",
            },
            ScanMod.axis_y: {
                tnrtAntennaMod.azel: "ALAT-GLS",
                tnrtAntennaMod.radec: "DEC-SFL",
            },
        }

        self.cdb.set(
            cdbConstants.SCANDIR, 
            scandir_dict[scantype_params.primary_axis][scantype_params.coord_system]
        )

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  This is not important for
        real-time flow control because each subscan will have it's own cleanup that executes
        after the subscan duration (TODO will be improved in the future by reading ACU status
        of program offset table finished).  This total duration is important for the scheduler in
        the future.  The scheuler must understand how much time is required for the total scan pattern
        to understand when the system is available to insert more into the queue..
        """
        slewtime = 0
        # This will make the scan really slow, but will give a buffer to guarantee that we don't cleanup too early
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "RASTER_MAP")
        self.cdb.set(cdbConstants.SCANMODE, "RASTER")
        self.cdb.set(cdbConstants.SCANGEOM, "SINGLE")
        self.cdb.set(cdbConstants.SCANRPTS, 1) # number of repeats of each line
        self.cdb.set(cdbConstants.SCANLINE, 1) # TODO (SS.) number of lines
        self.cdb.set(cdbConstants.SCANXSPC, self.scantype_params.xstep) # space between x samples
        self.cdb.set(cdbConstants.SCANYSPC, self.scantype_params.ystep) # space between y samples
        self.cdb.set(cdbConstants.SCANLEN, 0)
        self.cdb.set(cdbConstants.ZIGZAG, self.scantype_params.zigzag)


class ScanSkydip(AbstractBaseScan):
    """
    Set up a .
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        """
        Scan pattern at constant azimuth angle and scan from low elevation (direction of horizon)
        to high elevation (direction of zenith).  The result is one "line" type subscan that
        moves vertically offset from from the AZ/EL tracking coordinate of (az, start_el).

        Note (SS 02/2021): This implementation is different than the Yebes implementation that uses
        discrete steps in a tracking table.  I choose a continuous tracking sweep because I
        believe it makes sense for the curve fitting result processing.
        Each integration from the receiver will have an associated elevation angle in the data files
        (MBFITS DATAPAR.data['ELEVATIO'] or Gildas CLASS head.dri.apos )

        I also use the program offset table to move the antenna rather than program track table
        because it is more consistent with the implementation of other types of patterns that draw
        a pattern starting at a central tracking coordinate.
        """

        # Use the constructors of parent classes to process common parameters
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )

        self.scantype_params = scantype_params

        # Add subscans
        self.subscans = []

        # Limits for the strokes of the skydip.
        # Note that this is a program offset table.
        # The main program track table is set to track
        # the low elevation coordinate el_start.  Therefore the offset from el_start is 0 when
        # the pattern starts.
        # Azimuth always stays at offset tracking = 0 becuase program track table already tracks
        # the azimuth angle.
        # Note that SubscanLine accepts units of arcsec, so we must convert deg to arcsec.
        xlim = (0, 0)
        ylim = (0, 3600 * (scantype_params.el_end - scantype_params.el_start))

        self.subscans.append(
            SubscanLine(
                "lineHO1", 1, scantype_params.duration, xlim, ylim, tnrtAntennaMod.azel
            )
        )

        # Show results in log
        for ss in self.subscans:
            self.logger.logInfo(
                "Created subscan num %03d: %s\t x=(%+8.2f -> %+8.2f), y=(%+8.2f -> %+8.2f)"
                % (
                    ss.subs_num,
                    ss.__class__.__name__,
                    ss.xlim[0],
                    ss.xlim[1],
                    ss.ylim[0],
                    ss.ylim[1],
                )
            )

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        TODO - calculate slewtime between subscans in a smart way.  For now, hard-code a value.
        """
        slewtime = 0.5
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration + slewtime

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "SKYDIP")
        self.cdb.set(cdbConstants.SCANMODE, "OTF")
        self.cdb.set(cdbConstants.SCANGEOM, "LINE")
        self.cdb.set(cdbConstants.SCANRPTS, 1)
        self.cdb.set(cdbConstants.SCANLINE, 1)
        self.cdb.set(cdbConstants.SCANTIME, self.scantype_params.duration)
        self.cdb.set(cdbConstants.SCANLEN, self.scantype_params.el_start - self.scantype_params.el_end)


class ScanTimeSync(AbstractBaseScan):
    """
    ScanTimeSync is a simple Scan that records status data from the
    ACU (Antenna Control Unit) to compare time between ACU and TCS.
    The ACU and TCS computers should be synchronized by NTP.
    The ACU status message includes a timestamp in MJD format that is the UTC system time of ACU
    When this message arrives (via LAN) to the TCS computer, another timestamp is added
    that indicates TCS OS system time in UTC.
            duration: length of time [seconds] to record data
    """

    def __init__(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        # tracking_params, frontend_params, backend_params not used, but required for AbstractBaseScan constructor interface
        # Use the constructor of parent class to inherit all common variables
        AbstractBaseScan.__init__(
            self,
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )
        self.logger.logDebug(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Usually here, define the offset pattern.  Since ScanTimeSync only waits for timestamps,
        # we can skip the offset pattern and directly add a subscan of type SubscanSleep
        ssname = "sleep1"
        nsubs = 1
        self.subscans = [SubscanSleep(ssname, nsubs, scantype_params.duration)]

        # List of subscans constructed.  Now Calculate the Scan total duration
        self.calculate_duration()

    def calculate_duration(self):
        """
        Duration of this pattern is the sum of durations of all subscans + some slew time
         before, after subscans.
        """
        self.duration = 0
        for s in self.subscans:
            self.duration = self.duration + s.duration

        self.logger.logDebug("Calculated duration of this scan: %f [s]" % self.duration)

    def run(self, stop_event):
        """
        Extend the stanard AbstractBaseScan run() function to post-process data immediately
        and add the result to the dictionary results{}
        """

        # Run the standard run() function from AbstractBaseScan
        super().run(stop_event)

        # Read the data file, calculate result, and return additional results to caller.
        self.logger.logDebug(
            "Process results of Scan number {} (ScanTimeSync):".format(self.scan_number)
        )

        try:
            filepath = self.results["filenames"][0]
        
            # If we have not raised an exception yet, file name is in the result dict
            hdul = None

            if os.path.isfile(filepath) is not True:
                self.logger.logError("os.path.isfile({}) = False".format(filepath))
                hdul = None
            else:
                self.logger.logDebug("Processing file {}: ".format(filepath))
                hdul = fits.open(filepath)

                # Calculate the result:  Time difference between TCS and ACU in milliseconds.
                # If synchronized by NTP, the result should be better than 100ms
                SECONDS_PER_DAY = 86400
                delta_acu_tcs = SECONDS_PER_DAY * (
                    hdul["MONITOR-ANT-GENERAL"].data["actual_time"]
                    - hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"]
                )

                self.results["delta_acu_tcs_mean"] = np.mean(delta_acu_tcs)
                self.results["delta_acu_tcs_median"] = np.median(delta_acu_tcs)
                self.results["delta_acu_tcs_stdev"] = np.std(delta_acu_tcs)

                # close the file
                hdul.close()
        except (KeyError, IndexError) as e:
            msg = "{}: {}".format(e.__class__.__name__, e)
            self.logger.logError("Filename not found in results. {}. self.results={}.  Check DataAggregator and mbfits pipeline".format(msg, self.results))
        
        return self.results

    def update_scan_metadata(self):
        super().update_scan_metadata()
        # Update the metadata for each type of scan.
        self.cdb.set(cdbConstants.SCANTYPE, "TIMESYNC")


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    logger = logging.getLogger("AbstractBaseScan")
    project_id = "no_projid"
    obs_id = "no_obsid"
    scan_id = 8
    priority = 0
    start_mjd = 59000
    source_name = "no_source"
    line_name = "no_line"

    az = 20
    el_start = 5
    el_end = 85
    duration = 160

    # Tracking Common Parameters
    user_pointing_correction_az = 0.0
    user_pointing_correction_el = 0.0
    elevation_min = 5
    elevation_max = 88
    north_crossing = True
    use_horizontal_tables = False
    max_tracking_errors = 6

    tracking_common = ScanMod.TrackingParamsCommon(
        user_pointing_correction_az,
        user_pointing_correction_el,
        elevation_min,
        elevation_max,
        north_crossing,
        use_horizontal_tables,
        max_tracking_errors,
    )
    schedule_params = ScanMod.ScheduleParams(
        project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
    )
    scantype_params = ScanMod.ScantypeParamsSkydip(az, el_start, el_end, duration)
    frontend_params = ScanMod.AbstractFrontendParams()
    backend_params = ScanMod.AbstractBackendParams()
    tracking_params = ScanMod.TrackingParamsHO(az, el_start, tracking_common)

    try:
        ss1 = AbstractBaseScan(
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
        )
    except TypeError as e:
        logger.error(
            "Create object of a non-abstract child class that inherits from %s"
            % __file__
        )
        logger.error("For Example: ScanOnSource, ScanCross, ScanFocus, ...")
