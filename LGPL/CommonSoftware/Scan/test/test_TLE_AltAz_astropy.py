from sgp4.api import Satrec
import numpy as np

from astropy.time import Time
from astropy import units as units
from astropy.coordinates import CartesianRepresentation
from astropy.coordinates import EarthLocation
from astropy.coordinates import SkyCoord
from astropy.coordinates import AltAz
from astropy.coordinates import GCRS
from ScanImpl import Scan

longitude = 99.216805
latitude = 18.864348
height = 403.625

site_location = EarthLocation(EarthLocation.from_geodetic(lon=longitude * units.deg,
                                                          lat=latitude * units.deg,
                                                          height=height * units.m,
                                                          ellipsoid='WGS84'))
print('site location')
print(site_location)

# MJD from the user
obstime_mjd = Time.now().mjd
# Create an Astropy Time object
obstime = Time(obstime_mjd, format='mjd', scale='utc')

# Definitions for the satellite
scan = Scan.Scan()
TLEData = scan.get_TLE(38098) # INTELSAT 22
line0 = TLEData.name
line1 = TLEData.line1
line2 = TLEData.line2

# Use SGP4 model to calculate current position of the satellite
satellite = Satrec.twoline2rv(line1, line2)
e, (xs, ys, zs), v = satellite.sgp4(obstime.jd1, obstime.jd2)

print('option 2 - transform satellite ECI->AltAz @ observer(time, location) using Astropy GCRS transform to AltAz')

# Create a coordinate of the satellite in Geocentric Celestial Reference System
# This might not be perfect, but it is good enough to prepare the scan with
# initial pointing before MTM ACU starts tracking the TLE in real time
# obsgeoloc = CartesianRepresentation(site_location.x)
satellite_location = CartesianRepresentation(x=xs, y=ys, z=zs, unit=units.km)
satellite_coord_GCRS = GCRS(satellite_location, obstime=obstime)
observer_frame_AltAz = AltAz(location=site_location, obstime=obstime)

# Create a reference frame for observer at Earth location while Earth is rotated
# according to time of observation.
# frame_observer =

observed_coord_AltAz = satellite_coord_GCRS.transform_to(observer_frame_AltAz)

print('az = %f' % observed_coord_AltAz.az.deg)
print('el = %f' % observed_coord_AltAz.alt.deg)
