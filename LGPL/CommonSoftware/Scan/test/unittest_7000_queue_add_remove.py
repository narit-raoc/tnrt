# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import ScanMod
import tnrtAntennaMod
from scantype_models import ScanModelFactory
from scantype_models import ScanCalColdload
from scantype_models import ScanCalHotload
from scantype_models import ScanCalNoise
from scantype_models import ScanCross
from scantype_models import ScanFivePoint
from scantype_models import ScanFocus
from scantype_models import ScanManual
from scantype_models import ScanOnSource
from scantype_models import ScanRaster
from scantype_models import ScanSkydip
from scantype_models import ScanTimeSync


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

        # Prepare some parameter objects to use for generating scantypes
        project_id = "no_projid"
        obs_id = "no_obsid"
        scan_id = 8
        priority = 0
        start_mjd = 59100
        source_name = "no_source"
        line_name = "no_line"

        time_per_phase = 2.0

        ref_dx = 1.0
        ref_dy = 2.0
        ref_duration = 3.0
        ref_coord_system = tnrtAntennaMod.azel

        self.schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        cal_coord_dx = 0
        cal_coord_dy = 0
        cal_coord_duration = 1
        cal_coord_system = tnrtAntennaMod.azel

        self.cal_coord_params = ScanMod.CalCoordParams(
            cal_coord_dx, cal_coord_dy, cal_coord_duration, cal_coord_system
        )

        user_pointing_correction_az = 0
        user_pointing_correction_el = 0
        elevation_min = 15
        elevation_max = 85
        north_crossing = True
        use_horizontal_tables = False
        max_tracking_errors = 6
        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_horizontal_tables,
            max_tracking_errors,
        )

        az = -50
        el = 10
        self.tracking_params_HO = ScanMod.TrackingParamsHO(az, el, tracking_common)

        ra_catalog = 200
        dec_catalog = 30
        pm_ra = 0
        pm_dec = 0
        parallax = 0
        radial_velocity = 10.0
        send_icrs_to_acu = False
        self.tracking_params_EQ = ScanMod.TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            pm_ra,
            pm_dec,
            parallax,
            radial_velocity,
            send_icrs_to_acu,
            tracking_common,
        )

        line0 = "INTELSAT 22 (IS-22)     "
        line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
        line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"
        self.tracking_params_TLE = ScanMod.TrackingParamsTLE(
            line0, line1, line2, tracking_common
        )

        planet = "sun"
        self.tracking_params_SS = ScanMod.TrackingParamsSS(planet, tracking_common)

        self.tracking_params_NONE = ScanMod.AbstractTrackingParams()

        self.frontend_params = ScanMod.AbstractFrontendParams()
        self.backend_params = ScanMod.AbstractBackendParams()
        
        self.data_params_none = ScanMod.DataParams([])
        self.data_params_spec = ScanMod.DataParams(["mbfits", "spectrum_preview"])
        self.data_params_holo = ScanMod.DataParams(["atfits"])

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test7000_clearQueue(self):
        self.logger.debug("clearQueue()")
        self.result = self.objref.clearQueue()
        self.logger.debug(
            "result: {} items deleted from Queue".format(repr(self.result))
        )
        self.assertIsInstance(self.result, int)

    def test7002_addScan(self):
        # -----------------------------------------------------------------------------
        # Prepare data for Scan type paterns
        # -----------------------------------------------------------------------------
        arm_length = 7200  # arcsec
        time_per_arm = 3.0  # s
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True
        scantype_params_cross = ScanMod.ScantypeParamsCross(
            arm_length,
            time_per_arm,
            win_min,
            win_max,
            double_cross,
            ScanMod.AbstractCalCoordParams(),
        )

        # FivePoint
        arm_length = 7200  # arcsec
        time_per_point = 2.0  # s
        win_min = 1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True
        scantype_params_fivepoint = ScanMod.ScantypeParamsFivePoint(
            arm_length,
            time_per_point,
            win_min,
            win_max,
            ScanMod.AbstractCalCoordParams(),
        )

        # Manual TODO
        duration = 6.0  # s
        scantype_params_manual = ScanMod.ScantypeParamsManual(duration)

        # Noise Calibration
        time_per_phase = 2.0  # s
        cal_coord_dx = 0
        cal_coord_dy = 0
        cal_coord_duration = 1
        cal_coord_system = tnrtAntennaMod.azel
        cal_coord_params = ScanMod.CalCoordParams(
            cal_coord_dx, cal_coord_dy, cal_coord_duration, cal_coord_system
        )

        scantype_params_noisecal = ScanMod.ScantypeParamsCalNoise(
            time_per_phase, cal_coord_params
        )

        # On Source
        duration = 3.0  # s

        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams
        )

        # Raster
        xlen = 3600  # arcsec
        xstep = 720  # arcsec
        ylen = 3600  # arcsec
        ystep = 720  # arcsec
        time_per_point = 1
        zigzag = True
        primary_axis_enum = ScanMod.axis_x
        coord_system_enum = tnrtAntennaMod.azel
        scantype_params_raster = ScanMod.ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
            ScanMod.AbstractCalCoordParams(),
        )

        # Skydip
        az = 20
        el_start = 5
        el_end = 85
        duration = 160
        scantype_params_skydip = ScanMod.ScantypeParamsSkydip(
            az, el_start, el_end, duration
        )

        # Focus
        axis = tnrtAntennaMod.hxp_z
        start_pos = -500
        end_pos = 500
        velocity = 10.0
        scantype_params_focus = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        # Time Sync
        duration = 3.0  # s
        scantype_params_timesync = ScanMod.ScantypeParamsTimeSync(duration)

        # -----------------------------------------------------------------------------
        # Assemble scans and add to the queue
        # -----------------------------------------------------------------------------
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        self.objref.addScan(
            self.schedule_params,
            scantype_params_cross,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_cross,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_cross,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_cross,
            self.tracking_params_SS,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )

        self.objref.addScan(
            self.schedule_params,
            scantype_params_fivepoint,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_fivepoint,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_fivepoint,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_fivepoint,
            self.tracking_params_SS,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_noisecal,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_noisecal,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_noisecal,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_noisecal,
            self.tracking_params_SS,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_onsource,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_onsource,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_onsource,
            self.tracking_params_SS,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )

        self.objref.addScan(
            self.schedule_params,
            scantype_params_raster,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_raster,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_raster,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.objref.addScan(
            self.schedule_params,
            scantype_params_raster,
            self.tracking_params_SS,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )

        self.objref.addScan(
            self.schedule_params,
            scantype_params_skydip,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )

        self.objref.addScan(
            self.schedule_params,
            scantype_params_focus,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )

        self.objref.addScan(
            self.schedule_params,
            scantype_params_manual,
            self.tracking_params_NONE,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )
        
        self.objref.addScan(
            self.schedule_params,
            scantype_params_timesync,
            self.tracking_params_NONE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )

        self.result = self.objref.qsize()
        self.logger.debug("qsize: {}".format(result))
        self.assertEqual(self.result, 24)
