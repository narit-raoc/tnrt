# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import tnrtAntennaMod
import ScanMod
import subscan_models


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test4000_SubscanSleep(self):
        self.logger.debug("Create instance of SubscanSleep")
        name = "subscansleep1"
        nsubs = 1
        duration = 5.0
        self.result = subscan_models.SubscanSleep(name, nsubs, duration)
        self.assertIsInstance(self.result, subscan_models.SubscanSleep)

    def test4001_SubscanLine(self):
        self.logger.debug("Create instance of SubscanLine azel")
        nsubs = 1
        duration = 4.0
        xlim = (-3600, 3600)
        ylim = (-1800, 1800)

        # SubscanLine
        self.result = subscan_models.SubscanLine(
            "test1", nsubs, duration, xlim, ylim, tnrtAntennaMod.azel
        )
        self.assertIsInstance(self.result, subscan_models.SubscanLine)

    def test4002_SubscanLine(self):
        self.logger.debug("Create instance of SubscanLine radecshortcut")
        nsubs = 1
        duration = 4.0
        xlim = (-3600, 3600)
        ylim = (-1800, 1800)

        self.result = subscan_models.SubscanLine(
            "test2", nsubs, duration, xlim, ylim, tnrtAntennaMod.radecshortcut
        )
        self.assertIsInstance(self.result, subscan_models.SubscanLine)

    def test4003_SubscanSingle(self):
        self.logger.debug("Create instance of SubscanSingle AZEL")
        nsubs = 1
        duration = 4.0
        offset_x = -3600
        offset_y = 1800
        self.result = subscan_models.SubscanSingle(
            "test1", nsubs, duration, offset_x, offset_y, tnrtAntennaMod.azel
        )
        self.assertIsInstance(self.result, subscan_models.SubscanSingle)

    def test4004_SubscanSingle(self):
        self.logger.debug("Create instance of SubscanSingle RADEC")
        nsubs = 1
        duration = 4.0
        offset_x = -3600
        offset_y = 1800
        self.result = subscan_models.SubscanSingle(
            "test1",
            nsubs,
            duration,
            offset_x,
            offset_y,
            tnrtAntennaMod.radecshortcut,
        )
        self.assertIsInstance(self.result, subscan_models.SubscanSingle)

    def test4005_SubscanFocus(self):
        self.logger.debug("Create instance of SubscanFocus")
        nsubs = 1
        duration = 30
        hxp_xlim = (-10, 10)
        hxp_ylim = (-100, 100)
        hxp_zlim = (-1000, 1000)
        hxp_txlim = (-0.1, 0.1)
        hxp_tylim = (-0.01, 0.001)
        hxp_tzlim = (-0.001, 0.001)
        self.result = subscan_models.SubscanFocus(
            "test1",
            nsubs,
            duration,
            hxp_xlim,
            hxp_ylim,
            hxp_zlim,
            hxp_txlim,
            hxp_tylim,
            hxp_tzlim,
        )

        self.assertIsInstance(self.result, subscan_models.SubscanFocus)
