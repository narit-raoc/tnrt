# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import ScanMod
import backend_models


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.logger.info(self.result.name)
        super().tearDown()

    def test5000_BackendModelNone(self):
        self.logger.debug("create BackendModelNone")
        backend_params = None
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "NULL")

    def test5002_BackendModelEDD(self):
        self.logger.debug("create BackendModelEDD")
        backend_params = ScanMod.BackendParamsEDD(
            "preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}', False
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "EDD")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')

    def test5004_BackendModelMockEDD(self):
        self.logger.debug("create BackendModelMockEDD")
        backend_params = ScanMod.BackendParamsEDD("preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}', True)
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "MOCK_EDD")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')

    def test5006_BackendModelHoloFFT(self):
        self.logger.debug("create BackendModelHoloFFT")
        backend_params = ScanMod.BackendParamsHoloFFT(
            "preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}'
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "HOLOFFT")
        self.assertEqual(self.result.component_name, "BackendHoloFFT")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')

    def test5008_BackendModelMockHoloFFT(self):
        self.logger.debug("create BackendModelMockHoloFFT")
        backend_params = ScanMod.BackendParamsHoloFFT(
            "preset_config_fake", '{"mock": true}'
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "MOCK_HOLOFFT")
        self.assertEqual(self.result.component_name, "BackendMockHoloFFT")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"mock": true}')

    def test5010_BackendModelHoloSDR(self):
        self.logger.debug("create BackendModelHoloSDR")
        backend_params = ScanMod.BackendParamsHoloSDR(
            "preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}'
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "HOLOSDR")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')

    def test5012_BackendModelSKARAB(self):
        self.logger.debug("create BackendModelSKARAB")
        backend_params = ScanMod.BackendParamsSKARAB(
            "preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}'
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "SKARAB")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')

    def test5014_BackendModelOptical(self):
        self.logger.debug("create BackendModelOptical")
        backend_params = ScanMod.BackendParamsOptical(
            "preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}'
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "OPTICAL")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')

    def test5016_BackendModelPowerMeter(self):
        self.logger.debug("create BackendModelPowerMeter")
        backend_params = ScanMod.BackendParamsPowerMeter(
            "preset_config_fake", '{"kwarg1": 10, "kwarg2": 20.5}'
        )
        self.result = backend_models.BackendModelFactory(backend_params)
        self.assertEqual(self.result.name, "POWERMETER")
        self.assertEqual(self.result.preset_config_name, "preset_config_fake")
        self.assertEqual(self.result.kwargs_json, '{"kwarg1": 10, "kwarg2": 20.5}')
