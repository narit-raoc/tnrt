# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import ScanMod
import frontend_models


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test3001_FrontendModelNone(self):
        self.logger.debug("create FrontendModelNone")
        frontend_params = None
        self.result = frontend_models.FrontendModelFactory(frontend_params)
        self.assertEqual(self.result.name, "NULL")
        self.assertEqual(self.result.component_name, "")

    def test3002_FrontendModelL(self):
        self.logger.debug("create FrontendModelL")
        frontend_params = ScanMod.FrontendParamsL()
        self.result = frontend_models.FrontendModelFactory(frontend_params)
        self.assertEqual(self.result.name, "L")
        self.assertEqual(self.result.component_name, "FrontendL")

    def test3003_FrontendModelCX(self):
        self.logger.debug("create FrontendModelCX")
        frontend_params = ScanMod.FrontendParamsCX()
        self.result = frontend_models.FrontendModelFactory(frontend_params)
        self.assertEqual(self.result.name, "CX")
        self.assertEqual(self.result.component_name, "FrontendCX")

    def test3004_FrontendModelKu(self):
        self.logger.debug("create FrontendModelKu")
        frontend_params = ScanMod.FrontendParamsKu()
        self.result = frontend_models.FrontendModelFactory(frontend_params)
        self.assertEqual(self.result.name, "KU")
        self.assertEqual(self.result.component_name, "FrontendKu")

    def test3005_FrontendModelK(self):
        self.logger.debug("create FrontendModelK")
        frontend_params = ScanMod.FrontendParamsK()
        self.result = frontend_models.FrontendModelFactory(frontend_params)
        self.assertEqual(self.result.name, "K")
        self.assertEqual(self.result.component_name, "FrontendK")

    def test3006_FrontendModelOptical(self):
        self.logger.debug("create FrontendModelOptical")
        frontend_params = ScanMod.FrontendParamsOptical()
        self.result = frontend_models.FrontendModelFactory(frontend_params)
        self.assertEqual(self.result.name, "OPTICAL")
        self.assertEqual(self.result.component_name, "FrontendOptical")
