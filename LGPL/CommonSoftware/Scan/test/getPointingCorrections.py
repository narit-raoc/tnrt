# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.05

import sys
import logging
import coloredlogs
import argparse
import numpy as np
import pyclass

filename = '20200408_043029.40m'
scanNumber = 1
# In real situation, don't hardcode these numbers. use SubscanLine attributes lim[1], lim[-2] to grab the whole
# window of the subscan except the first and last point.  If we include the window over the entire
# subscan, Gildas complains about not enough channels. It's a bit confusing about the units, 
# but manageable with enough comments.  
v1 = -700
v2 = 700	

# Configure logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fmt_scrn = '%(asctime)s [%(levelname)s] %(module)s.%(name)s.%(funcName)s(): %(message)s'
level_styles_scrn = {'critical': {'color': 'red', 'bold': True}, 'debug': {'color': 'white', 'faint': True}, 'error': {'color': 'red'}, 'info': {'color': 'green', 'bright': True}, 'notice': {'color': 'magenta'}, 'spam': {'color': 'green', 'faint': True}, 'success': {'color': 'green', 'bold': True}, 'verbose': {'color': 'blue'}, 'warning': {'color': 'yellow', 'bright': True, 'bold': True}}
field_styles_scrn = {'asctime': {}, 'hostname': {'color': 'magenta'}, 'levelname': {'color': 'cyan', 'bright': True}, 'name': {'color': 'blue', 'bright': True}, 'programname': {'color': 'cyan'}}
formatter_screen = coloredlogs.ColoredFormatter(fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn)
handler_screen = logging.StreamHandler()
handler_screen.setFormatter(formatter_screen)
handler_screen.setLevel(logging.DEBUG)
logger.addHandler(handler_screen)
logger.debug('Started logger name %s' % logger.name)

# Read command line arguments into argparse structure
# args = parser.parse_args()

def send_class(cmd):
	'''
	Helper function to avoid copy and paste

	Parameters
	----------
	cmd : string
		command to send to pyclass.comm
	'''
	logger.info('> %s' % cmd)
	pyclass.comm(cmd)

try:
	# Open the file
	send_class('file in %s' % filename)
	
	# Set index to use continuum drift scans (not spectrum scans)
	send_class('set type cont')
	
	# Load scans that match the filter type from set type cont 
	send_class('find /all')

	# Get the Gildas dictionary of current attributes
	g = pyclass.gdict
	subscans = int(g.found)
	logger.info('I-FIND,  %d observations found' % subscans)
	
	# Set the variable GAUSS to READ mode 
	send_class('set var gauss read')

	# Set window of min max velocity to calculate the result.
	send_class('set window %f %f ' % (v1, v2))

	# Iterate through subscans and process each one separately
	for ss in range(subscans):
		# Select the next available subscan
		send_class('get next')

		# Subtract baseline of degree 2
		send_class('base 2')

		# Performs a fit of a theoretical profile
		send_class('minimize')

		# Get results of gaussian fitting from gdict in native units
		area = g.nfit[0]
		v0 = g.nfit[1]
		fwhm = g.nfit[2]
		sigma_on_base = g.sigba
		logger.info('from CLASS gdict: area: %f, v0: %f, fwhm: %f, g.sigba %f' % (area, v0, fwhm, sigma_on_base))

		ARCSEC_PER_RADIAN = 3600.0 * 180.0 / np.pi
		# Convert units
		angle0 = v0 * ARCSEC_PER_RADIAN 
		width = fwhm * ARCSEC_PER_RADIAN

		logger.info('converted units: angle0 [arcsec]: %f, width [arcsec]: %f' % (angle0, width))
		
		# Calculate addiontal results
		tamax = area / (np.sqrt(np.pi / (4 * np.log(2))) * fwhm)
		snr = tamax / sigma_on_base

		logger.info('derived results: tamax [unit?]: %f, snr [unit?]: %f' % (tamax, snr))

		
except Exception as ex:
	logger.exception('PygildasError')
	exit()
