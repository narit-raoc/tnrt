# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
import time
import threading

from unittest_common import ScanTestCase
import ScanMod
import tnrtAntennaMod
from scantype_models import ScanModelFactory
from scantype_models import ScanCalColdload
from scantype_models import ScanCalHotload
from scantype_models import ScanCalNoise
from scantype_models import ScanCross
from scantype_models import ScanFivePoint
from scantype_models import ScanFocus
from scantype_models import ScanManual
from scantype_models import ScanOnSource
from scantype_models import ScanRaster
from scantype_models import ScanSkydip
from scantype_models import ScanTimeSync

from astropy import units as units
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.time import Time


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

        # Prepare some parameter objects to use for generating scantypes
        project_id = "no_projid"
        obs_id = "no_obsid"
        scan_id = 8
        priority = 0
        start_mjd = 59100
        source_name = "no_source"
        line_name = "no_line"

        time_per_phase = 2.0

        ref_dx = 1.0
        ref_dy = 2.0
        ref_duration = 3.0
        ref_coord_system = tnrtAntennaMod.azel

        self.schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        cal_coord_dx = 0
        cal_coord_dy = 0
        cal_coord_duration = 1
        cal_coord_system = tnrtAntennaMod.azel

        self.cal_coord_params = ScanMod.CalCoordParams(
            cal_coord_dx, cal_coord_dy, cal_coord_duration, cal_coord_system
        )

        user_pointing_correction_az = 0
        user_pointing_correction_el = 0
        elevation_min = 15
        elevation_max = 85
        north_crossing = True
        use_horizontal_tables = False
        max_tracking_errors = 6
        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_horizontal_tables,
            max_tracking_errors,
        )

        az = -50
        el = 10
        self.tracking_params_HO = ScanMod.TrackingParamsHO(az, el, tracking_common)

        (ra_catalog, dec_catalog) = self._get_ra_dec_above_horizon_now(50, 30)
        pm_ra = 0
        pm_dec = 0
        parallax = 0
        radial_velocity = 10.0
        send_icrs_to_acu = False
        self.tracking_params_EQ = ScanMod.TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            pm_ra,
            pm_dec,
            parallax,
            radial_velocity,
            send_icrs_to_acu,
            tracking_common,
        )

        line0 = "INTELSAT 22 (IS-22)     "
        line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
        line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"
        self.tracking_params_TLE = ScanMod.TrackingParamsTLE(line0, line1, line2, tracking_common)

        planet = "sun"
        self.tracking_params_SS = ScanMod.TrackingParamsSS(planet, tracking_common)

        self.tracking_params_NONE = ScanMod.AbstractTrackingParams()

        self.frontend_params = ScanMod.AbstractFrontendParams()
        self.backend_params = ScanMod.AbstractBackendParams()

        self.data_params_none = ScanMod.DataParams([])
        self.data_params_spec = ScanMod.DataParams(["mbfits", "spectrum_preview"])
        self.data_params_holo = ScanMod.DataParams(["atfits"])

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def _get_ra_dec_above_horizon_now(self, az, el):
        longitude = 99.216805
        latitude = 18.864348
        height = 403.625
        az_apy = az * units.deg
        el_apy = el * units.deg
        loc_apy = EarthLocation(
            EarthLocation.from_geodetic(
                lon=longitude * units.deg,
                lat=latitude * units.deg,
                height=height * units.m,
                ellipsoid="WGS84",
            )
        )
        coord_altaz = SkyCoord(
            frame="altaz",
            az=az_apy,
            alt=el_apy,
            obstime=Time.now(),
            location=loc_apy,
        )
        coord_icrs = coord_altaz.transform_to("icrs")
        ra_catalog = coord_icrs.ra.deg
        dec_catalog = coord_icrs.dec.deg
        return (ra_catalog, dec_catalog)

    def _run_queue_helper(self):
        self.results = self.objref.run_queue()
        self.logger.debug(self.results)

    def test8000_run_queue_without_backend(self):
        # -----------------------------------------------------------------------------
        # Prepare data for Scan type paterns
        # -----------------------------------------------------------------------------
        arm_length = 7200  # arcsec
        time_per_arm = 3.0  # s
        win_min = -1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True
        scantype_params_cross = ScanMod.ScantypeParamsCross(
            arm_length,
            time_per_arm,
            win_min,
            win_max,
            double_cross,
            ScanMod.AbstractCalCoordParams(),
        )

        # FivePoint
        arm_length = 7200  # arcsec
        time_per_point = 2.0  # s
        win_min = 1000  # arcsec
        win_max = 1000  # arcsec
        double_cross = True
        scantype_params_fivepoint = ScanMod.ScantypeParamsFivePoint(
            arm_length,
            time_per_point,
            win_min,
            win_max,
            ScanMod.AbstractCalCoordParams(),
        )

        duration = 10.0  # s
        scantype_params_manual = ScanMod.ScantypeParamsManual(duration)

        # Noise Calibration
        time_per_phase = 2.0  # s
        cal_coord_dx = 0
        cal_coord_dy = 0
        cal_coord_duration = 1
        cal_coord_system = tnrtAntennaMod.azel
        cal_coord_params = ScanMod.CalCoordParams(
            cal_coord_dx, cal_coord_dy, cal_coord_duration, cal_coord_system
        )

        scantype_params_noisecal = ScanMod.ScantypeParamsCalNoise(time_per_phase, cal_coord_params)

        # On Source
        duration = 3.0  # s

        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams
        )

        # Raster
        xlen = 3600  # arcsec
        xstep = 720  # arcsec
        ylen = 3600  # arcsec
        ystep = 720  # arcsec
        time_per_point = 1
        zigzag = True
        primary_axis_enum = ScanMod.axis_x
        coord_system_enum = tnrtAntennaMod.azel
        scantype_params_raster = ScanMod.ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
            ScanMod.AbstractCalCoordParams(),
        )

        # Skydip
        az = 20
        el_start = 5
        el_end = 85
        duration = 160
        scantype_params_skydip = ScanMod.ScantypeParamsSkydip(az, el_start, el_end, duration)

        # Focus
        axis = tnrtAntennaMod.hxp_z
        start_pos = -500
        end_pos = 500
        velocity = 10.0
        scantype_params_focus = ScanMod.ScantypeParamsFocus(axis, start_pos, end_pos, velocity)

        # Time Sync
        duration = 3.0  # s
        scantype_params_timesync = ScanMod.ScantypeParamsTimeSync(duration)

        # -----------------------------------------------------------------------------
        # Assemble scans and add to the queue
        # -----------------------------------------------------------------------------
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        self.objref.addScan(
            self.schedule_params,
            scantype_params_cross,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_cross,
        #     self.tracking_params_EQ,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_cross,
        #     self.tracking_params_TLE,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_cross,
        #     self.tracking_params_SS,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_fivepoint,
        #     self.tracking_params_HO,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_fivepoint,
        #     self.tracking_params_EQ,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_fivepoint,
        #     self.tracking_params_TLE,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_fivepoint,
        #     self.tracking_params_SS,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_noisecal,
        #     self.tracking_params_HO,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_noisecal,
        #     self.tracking_params_EQ,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_noisecal,
        #     self.tracking_params_TLE,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_noisecal,
        #     self.tracking_params_SS,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_onsource,
        #     self.tracking_params_HO,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_onsource,
        #     self.tracking_params_EQ,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        self.schedule_params.scan_id += 1
        self.objref.addScan(
            self.schedule_params,
            scantype_params_onsource,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_onsource,
        #     self.tracking_params_SS,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_raster,
        #     self.tracking_params_HO,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_raster,
        #     self.tracking_params_EQ,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_raster,
        #     self.tracking_params_TLE,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_raster,
        #     self.tracking_params_SS,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_skydip,
        #     self.tracking_params_HO,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_focus,
        #     self.tracking_params_EQ,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_manual,
        #     self.tracking_params_NONE,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )
        # self.schedule_params.scan_id += 1
        # self.objref.addScan(
        #     self.schedule_params,
        #     scantype_params_timesync,
        #     self.tracking_params_NONE,
        #     self.frontend_params,
        #     self.backend_params,
        #     self.data_params_none,
        # )

        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))
        self.result = self.objref.run_queue()
        self.logger.debug("result: {}".format(repr(str)))
        self.assertIsInstance(self.result, str)

    def test8002_run_queue_with_backend(self):
        project_id = "test_with_backend"
        obs_id = "SS"

        scan_id = 2000
        priority = 0
        start_mjd = Time.now().mjd
        source_name = "fake name"
        line_name = "fake line"

        schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        duration = 5.0  # s
        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams()
        )
        tracking_params_NONE = ScanMod.AbstractTrackingParams()
        backend_params = ScanMod.BackendParamsEDD(
            'TNRT_dualpol_spectrometer_L',
            '{"products":{"gated_spectrometer_0":{"fft_length":16384,"naccumulate":65536},"gated_spectrometer_1":{"fft_length":16384,"naccumulate":65536}}}',
            True
        )
        frontend_params = ScanMod.AbstractFrontendParams()

        # -----------------------------------------------------------------------------
        # Assemble scans and add to the queue
        # -----------------------------------------------------------------------------
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            tracking_params_NONE,
            frontend_params,
            backend_params,
            self.data_params_spec,
        )

        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            tracking_params_NONE,
            frontend_params,
            backend_params,
            self.data_params_holo,
        )

        self.result = self.objref.run_queue()
        self.assertIsInstance(self.result, str)

    def test8004_run_ScanManual(self):
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        duration = 30.0  # s
        scantype_params_manual = ScanMod.ScantypeParamsManual(duration)

        self.objref.addScan(
            self.schedule_params,
            scantype_params_manual,
            self.tracking_params_NONE,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))
        self.result = self.objref.run_queue()
        self.assertIsInstance(self.result, str)

    def test8006_stop_scheduled(self):
        # Clear the queue

        project_id = "unittest"
        obs_id = "SS"

        # Optional data
        scan_id = 2000
        priority = 0
        start_mjd = Time.now().mjd
        source_name = "noname"
        line_name = "noline"

        # Create basic schedule params. Don't need optional data now.
        schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        # On Source
        duration = 10.0  # s
        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams()
        )

        # Assemble scans and add to the queue
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        schedule_params.start_mjd = Time.now().mjd + 10 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        schedule_params.scan_id += 1
        schedule_params.start_mjd = Time.now().mjd + 30 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        schedule_params.scan_id += 1
        schedule_params.start_mjd = Time.now().mjd + 50 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        # -----------------------------------------
        # Test stop scan by program command
        # -----------------------------------------
        # Run the queue in separate thread
        thread_queue = threading.Thread(target=self._run_queue_helper)
        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))
        thread_queue.start()

        # Sleep this thread for half of the duration of the first scan
        tsleep = duration / 2
        self.logger.debug("Sleep this thread for {} seconds while queue runs".format(tsleep))
        time.sleep(tsleep)

        # Stop the scan after half of duration
        self.logger.debug("Stop the scan NOW")
        self.objref.stop_queue()

        # Wait for the queue run thread to finish
        thread_queue.join()

        # Show results
        self.logger.debug("result: {}".format(repr(self.results)))

        # Confirm remaining queue
        result = self.objref.qsize()
        self.logger.debug("qsize: {}".format(result))
        self.assertEqual(result, 2)

        # Show results
        self.logger.debug("result: {}".format(repr(self.results)))

    def test8080_stop_slewing(self):
        project_id = "unittest"
        obs_id = "SS"
        
        # Optional data in user interface
        scan_id = 2000
        priority = 0
        start_mjd = Time.now().mjd
        source_name = "noname"
        line_name = "noline"

        # Create basic schedule params. Don't need optional data now.
        schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        duration = 10.0  # s
        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams()
        )

        tracking_params1 = ScanMod.TrackingParamsHO(
            self.tracking_params_HO.az + 60,
            self.tracking_params_HO.el + 30, 
            self.tracking_params_HO.common)

        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        # Send the antenna to a known coordinate defined by 
        # self.tracking_params_HO run the queue, block until arrived.  
        self.results = self.objref.run_queue()

        # Then, track a different coordinate and cancel while slewing
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            tracking_params1,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        thread_queue = threading.Thread(target=self._run_queue_helper)
        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))
        thread_queue.start()

        # Sleep this thread for half of the duration of the first scan
        self.logger.debug("Sleep this thread 10 seconds while slewing")
        time.sleep(10)

        # Stop the scan after half of duration
        self.logger.debug("Stop the scan NOW")
        self.objref.stop_queue()

        # Check the value of the Events in Scan
        self.assertFalse(self.objref.is_trackcoord_arrived())
        self.assertTrue(self.objref.is_current_scan_canceled())

    def test8010_stop_running(self):
        project_id = "unittest"
        obs_id = "SS"

        # Optional data
        scan_id = 2000
        priority = 0
        start_mjd = Time.now().mjd
        source_name = "noname"
        line_name = "noline"

        # Create basic schedule params. Don't need optional data now.
        schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        # On Source
        duration = 10.0  # s
        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams()
        )

        # Assemble scans and add to the queue
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        schedule_params.start_mjd = Time.now().mjd + 0 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        schedule_params.scan_id += 1
        schedule_params.start_mjd = Time.now().mjd + 30 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        schedule_params.scan_id += 1
        schedule_params.start_mjd = Time.now().mjd + 50 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        # -----------------------------------------
        # Test stop scan by program command
        # -----------------------------------------
        # Run the queue in separate thread
        thread_queue = threading.Thread(target=self._run_queue_helper)
        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))
        thread_queue.start()

        # Sleep this thread for half of the duration of the first scan
        tsleep = duration / 2
        self.logger.debug("Sleep this thread for {} seconds while queue runs".format(tsleep))
        time.sleep(tsleep)

        # Stop the scan after half of duration
        self.logger.debug("Stop the scan NOW")
        self.objref.stop_queue()

        # Wait for the queue run thread to finish
        thread_queue.join()

        # Show results
        self.logger.debug("result: {}".format(repr(self.results)))

        # Confirm remaining queue
        result = self.objref.qsize()
        self.logger.debug("qsize: {}".format(result))
        self.assertEqual(result, 2)

        # Show results
        self.logger.debug("result: {}".format(repr(self.results)))

    def test8012_stop_keyboard(self):
        project_id = "unittest"
        obs_id = "SS"

        # Optional data
        scan_id = 2000
        priority = 0
        start_mjd = Time.now().mjd
        source_name = "noname"
        line_name = "noline"

        # Create basic schedule params. Don't need optional data now.
        schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        # On Source
        duration = 10.0  # s
        scantype_params_onsource = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams()
        )

        # Assemble scans and add to the queue
        result = self.objref.clearQueue()
        self.logger.debug("result: {} items deleted from Queue".format(repr(result)))

        schedule_params.start_mjd = Time.now().mjd + 0 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        schedule_params.scan_id += 1
        schedule_params.start_mjd = Time.now().mjd + 30 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        schedule_params.scan_id += 1
        schedule_params.start_mjd = Time.now().mjd + 50 / 86400.0
        self.objref.addScan(
            schedule_params,
            scantype_params_onsource,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_none,
        )

        # -----------------------------------------
        # Test stop scan by KeyboardInterrupt
        # -----------------------------------------
        # Run the queue in separate thread
        thread_queue = threading.Thread(target=self._run_queue_helper)
        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))
        self.logger.debug("Press CTRL+C to cancel")

        try:
            # Wait for the queue run thread to finish
            thread_queue.start()
            thread_queue.join()
        except KeyboardInterrupt:
            self.logger.debug("Catch KeyboardInterrupt. Stop the scan NOW")
            self.objref.stop_queue()
            thread_queue.join()

        # Show results
        self.logger.debug("result: {}".format(repr(self.results)))
