# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import ScanMod
import tnrtAntennaMod
from scantype_models import ScanModelFactory
from scantype_models import ScanCalColdload
from scantype_models import ScanCalHotload
from scantype_models import ScanCalNoise
from scantype_models import ScanCross
from scantype_models import ScanFivePoint
from scantype_models import ScanFocus
from scantype_models import ScanManual
from scantype_models import ScanMap
from scantype_models import ScanOnSource
from scantype_models import ScanRaster
from scantype_models import ScanSkydip
from scantype_models import ScanTimeSync


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

        # Prepare some parameter objects to use for generating scantypes
        project_id = "no_projid"
        obs_id = "no_obsid"
        scan_id = 8
        priority = 0
        start_mjd = 59100
        source_name = "no_source"
        line_name = "no_line"

        time_per_phase = 2.0

        ref_dx = 1.0
        ref_dy = 2.0
        ref_duration = 3.0
        ref_coord_system = tnrtAntennaMod.azel

        self.schedule_params = ScanMod.ScheduleParams(
            project_id,
            obs_id,
            scan_id,
            priority,
            start_mjd,
            source_name,
            line_name,
        )

        cal_coord_dx = 0
        cal_coord_dy = 0
        cal_coord_duration = 1
        cal_coord_system = tnrtAntennaMod.azel

        self.cal_coord_params = ScanMod.CalCoordParams(
            cal_coord_dx, cal_coord_dy, cal_coord_duration, cal_coord_system
        )

        user_pointing_correction_az = 0
        user_pointing_correction_el = 0
        elevation_min = 15
        elevation_max = 85
        north_crossing = True
        use_horizontal_tables = False
        max_tracking_errors = 6
        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_horizontal_tables,
            max_tracking_errors,
        )

        az = -50
        el = 10
        self.tracking_params_HO = ScanMod.TrackingParamsHO(az, el, tracking_common)

        ra_catalog = 200
        dec_catalog = 30
        pm_ra = 0
        pm_dec = 0
        parallax = 0
        radial_velocity = 10.0
        send_icrs_to_acu = False
        self.tracking_params_EQ = ScanMod.TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            pm_ra,
            pm_dec,
            parallax,
            radial_velocity,
            send_icrs_to_acu,
            tracking_common,
        )

        line0 = "INTELSAT 22 (IS-22)     "
        line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
        line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"
        self.tracking_params_TLE = ScanMod.TrackingParamsTLE(
            line0, line1, line2, tracking_common
        )

        self.frontend_params = ScanMod.AbstractFrontendParams()
        self.backend_params = ScanMod.AbstractBackendParams()
        self.data_params_none = ScanMod.DataParams([])
        self.data_params_spec = ScanMod.DataParams(["mbfits", "spectrum_preview"])
        self.data_params_holo = ScanMod.DataParams(["atfits"])

    def tearDown(self):
        self.logger.info(self.result)
        self.logger.info(self.result.pipeline_names)
        super().tearDown()

    def test6000_ScanCalColdload(self):
        self.logger.debug("create ScanColdload")
        time_per_phase = 1
        scantype_params = ScanMod.ScantypeParamsCalColdload(
            time_per_phase, self.cal_coord_params
        )
        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanCalColdload)

    def test6002_ScanCalHotload(self):
        self.logger.debug("create ScanCalHotload")
        time_per_phase = 1
        scantype_params = ScanMod.ScantypeParamsCalHotload(
            time_per_phase, self.cal_coord_params
        )
        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanCalHotload)

    def test6004_ScanCalNoise(self):
        self.logger.debug("create ScanCalNoise")
        time_per_phase = 1
        scantype_params = ScanMod.ScantypeParamsCalNoise(
            time_per_phase, self.cal_coord_params
        )
        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanCalNoise)

    def test6006_ScanCross_no_cal_coord(self):
        self.logger.debug("create ScanCross no cal coord")
        arm_length = 7200
        time_per_arm = 30.0
        win_min = 1000
        win_max = 1000
        double_cross = True

        scantype_params = ScanMod.ScantypeParamsCross(
            arm_length,
            time_per_arm,
            win_min,
            win_max,
            double_cross,
            ScanMod.AbstractCalCoordParams(),
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanCross)

    def test6008_ScanCross_cal_coord(self):
        self.logger.debug("create ScanCross no cal coord")
        arm_length = 7200
        time_per_arm = 30.0
        win_min = 1000
        win_max = 1000
        double_cross = True

        scantype_params = ScanMod.ScantypeParamsCross(
            arm_length,
            time_per_arm,
            win_min,
            win_max,
            double_cross,
            self.cal_coord_params,
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanCross)

    def test6010_ScanFivePoit_no_cal_coord(self):
        self.logger.debug("create ScanFivePoint no cal coord")
        arm_length = 7200
        time_per_point = 5.0
        win_min = 1000
        win_max = 1000

        scantype_params = ScanMod.ScantypeParamsFivePoint(
            arm_length,
            time_per_point,
            win_min,
            win_max,
            ScanMod.AbstractCalCoordParams(),
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFivePoint)

    def test6012_ScanFivePoint_cal_coord(self):
        self.logger.debug("create ScanFivePoint no cal coord")
        arm_length = 7200
        time_per_point = 5.0
        win_min = 1000
        win_max = 1000

        scantype_params = ScanMod.ScantypeParamsFivePoint(
            arm_length,
            time_per_point,
            win_min,
            win_max,
            self.cal_coord_params,
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFivePoint)

    def test6014_ScanFocus_HO_Z(self):
        self.logger.debug("create ScanFocus on HO coordinate, Z axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_z
        start_pos = -500
        end_pos = 500
        velocity = 10.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6016_ScanFocus_EQ_Z(self):
        self.logger.debug("create ScanFocus on EQ coordinate, Z axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_z
        start_pos = -500
        end_pos = 500
        velocity = 10.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6016_ScanFocus_TLE_Z(self):
        self.logger.debug("create ScanFocus on TLE coordinate, Z axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_z
        start_pos = -500
        end_pos = 500
        velocity = 10.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_TLE,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6018_ScanFocus_HO_X(self):
        self.logger.debug("create ScanFocus on HO coordinate, X axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_x
        start_pos = -50
        end_pos = 50
        velocity = 1.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6020_ScanFocus_HO_Y(self):
        self.logger.debug("create ScanFocus on HO coordinate, Y axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_y
        start_pos = -50
        end_pos = 50
        velocity = 1.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6022_ScanFocus_HO_TX(self):
        self.logger.debug("create ScanFocus on HO coordinate, tx axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_tx
        start_pos = -50
        end_pos = 50
        velocity = 1.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6024_ScanFocus_HO_TY(self):
        self.logger.debug("create ScanFocus on HO coordinate, ty axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_ty
        start_pos = -50
        end_pos = 50
        velocity = 1.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6026_ScanFocus_HO_TZ(self):
        self.logger.debug("create ScanFocus on HO coordinate, tz axis")
        # Scantype params
        axis = tnrtAntennaMod.hxp_tz
        start_pos = -50
        end_pos = 50
        velocity = 1.0

        scantype_params = ScanMod.ScantypeParamsFocus(
            axis, start_pos, end_pos, velocity
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanFocus)

    def test6028_ScanManual(self):
        self.logger.debug("create Manual")
        # Scantype params

        duration = 3.0
        scantype_params = ScanMod.ScantypeParamsManual(duration)

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            ScanMod.AbstractTrackingParams(),
            ScanMod.AbstractFrontendParams(),
            ScanMod.AbstractBackendParams(),
            self.data_params_none,
        )
        self.assertIsInstance(self.result, ScanManual)

    def test6029_ScanMap_no_cal_coord(self):
        self.logger.debug("create ScanMap no cal coord")
        line_length = 7200
        time_per_line = 30
        nlines = 1
        spacing = 240
        axis = ScanMod.axis_x
        zigzag = False
        coord_system = tnrtAntennaMod.azel
        subscans_per_cal = 0
        time_cal0 = 60
        time_per_cal = 10

        scantype_params = ScanMod.ScantypeParamsMap(
            line_length,
		    time_per_line,
		    nlines,
		    spacing,
		    axis,
		    zigzag,
		    coord_system,
		    subscans_per_cal,
		    time_cal0,
		    time_per_cal
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanMap)

    def test6029_ScanMap_cal_coord(self):
        self.logger.debug("create ScanMap no cal coord")
        line_length = 7200
        time_per_line = 10
        nlines = 10
        spacing = 720
        axis = ScanMod.axis_x
        zigzag = False
        coord_system = tnrtAntennaMod.azel
        subscans_per_cal = 1
        time_cal0 = 60
        time_per_cal = 5

        scantype_params = ScanMod.ScantypeParamsMap(
            line_length,
		    time_per_line,
		    nlines,
		    spacing,
		    axis,
		    zigzag,
		    coord_system,
		    subscans_per_cal,
		    time_cal0,
		    time_per_cal
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanMap)
    
    def test6029_ScanMap_holo_atfits(self):
        self.logger.debug("create ScanMap no cal coord")
        line_length = 7200
        time_per_line = 10
        nlines = 10
        spacing = 720
        axis = ScanMod.axis_x
        zigzag = False
        coord_system = tnrtAntennaMod.azel
        subscans_per_cal = 1
        time_cal0 = 60
        time_per_cal = 5

        scantype_params = ScanMod.ScantypeParamsMap(
            line_length,
		    time_per_line,
		    nlines,
		    spacing,
		    axis,
		    zigzag,
		    coord_system,
		    subscans_per_cal,
		    time_cal0,
		    time_per_cal
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_HO,
            self.frontend_params,
            self.backend_params,
            self.data_params_holo,
        )
        self.assertIsInstance(self.result, ScanMap)

    def test6030_ScanOnSource_no_cal_coord(self):
        self.logger.debug("create ScanOnSource no cal coord")
        # Scantype params

        duration = 3.0
        scantype_params = ScanMod.ScantypeParamsOnSource(
            duration, ScanMod.AbstractCalCoordParams()
        )
        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanOnSource)

    def test6032_ScanOnSource_cal_coord(self):
        self.logger.debug("create ScanOnSource no cal coord")
        # Scantype params

        duration = 3.0
        scantype_params = ScanMod.ScantypeParamsOnSource(
            duration, self.cal_coord_params
        )
        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanOnSource)

    def test6032_ScanRaster_no_cal_coord(self):
        self.logger.debug("create ScanRaster no cal coord")
        # Scantype params

        xlen = 3600  # arcsec
        xstep = 720  # arcsec
        ylen = 3600  # arcsec
        ystep = 720  # arcsec
        time_per_point = 1
        zigzag = True
        primary_axis_enum = ScanMod.axis_y
        coord_system_enum = tnrtAntennaMod.azel

        scantype_params = ScanMod.ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
            ScanMod.AbstractCalCoordParams(),
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanRaster)

    def test6034_ScanRaster_cal_coord(self):
        self.logger.debug("create ScanRaster cal coord")
        # Scantype params

        xlen = 3600  # arcsec
        xstep = 720  # arcsec
        ylen = 3600  # arcsec
        ystep = 720  # arcsec
        time_per_point = 1
        zigzag = True
        primary_axis_enum = ScanMod.axis_y
        coord_system_enum = tnrtAntennaMod.azel

        scantype_params = ScanMod.ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            coord_system_enum,
            self.cal_coord_params,
        )

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanRaster)

    def test6036_ScanSkydip(self):
        self.logger.debug("create ScanSkydip")

        az = 20
        el_start = 5
        el_end = 85
        duration = 160

        scantype_params = ScanMod.ScantypeParamsSkydip(az, el_start, el_end, duration)

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            self.tracking_params_EQ,
            self.frontend_params,
            self.backend_params,
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanSkydip)

    def test6038_ScanTimeSync(self):
        self.logger.debug("create ScanTimeSync")

        duration = 10

        scantype_params = ScanMod.ScantypeParamsTimeSync(duration)

        self.result = ScanModelFactory.from_params(
            self.schedule_params,
            scantype_params,
            ScanMod.AbstractTrackingParams(),
            ScanMod.AbstractFrontendParams(),
            ScanMod.AbstractBackendParams(),
            self.data_params_spec,
        )
        self.assertIsInstance(self.result, ScanTimeSync)
